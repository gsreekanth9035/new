package keyceremony;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class KeyCeremonyWithKeyCheckValue {

	// parts in Hex
	static String transferKey1 = "63EFE72B5D18BE0B56A668B621A1A5F3A5C86D40F4140BC4CB6DE4D4D695C2A4";
	static String transferKey2 = "345C38DBEEBCF3A0C5460602F44022E478508679D2BFE17636E2C2D0781D1162";
	static String transferKey3 = "152B3626B0DDFB5346B6D77D710977D6FFBDB37B70E43E8D65D6ABE53AC8AEE5";
	static String encryptedGPKey = "E22BFD3B1183800685E4B89AE84D08F5ADE9B7098B13D7DA667696218CE4975F";
	static String tripledeskey = "0123456789ABCDEFFEDCBA9876543210";
	static String deskey = "0123456789ABCDEF";

	public static void main(String[] args) throws GeneralSecurityException, DecoderException {

		KeyCeremonyWithKeyCheckValue kckcv = new KeyCeremonyWithKeyCheckValue();
		
		//If keys are in HEX string, convert them to bytes by decoding
		System.out.println(kckcv.generateKCV(Hex.decodeHex(transferKey1.toCharArray()), "AES", "AES/ECB/NoPadding")); // KCV 2CD316
		System.out.println(kckcv.generateKCV(Hex.decodeHex(transferKey2.toCharArray()), "AES", "AES/ECB/NoPadding")); // KCV 1906CE
		System.out.println(kckcv.generateKCV(Hex.decodeHex(transferKey3.toCharArray()), "AES", "AES/ECB/NoPadding")); // KCV 4BF99B
		System.out.println(kckcv.generateKCV(Hex.decodeHex(tripledeskey.toCharArray()), "DESede", "DESede/ECB/NoPadding")); // KCV 08D7B4
		System.out.println(kckcv.generateKCV(Hex.decodeHex(deskey.toCharArray()), "DES", "DES/ECB/NoPadding")); // KCV D5D44F
		
		//Transfer Key recipient does an XOR of all transfer key parts together
		BigInteger one = new BigInteger(transferKey1, 16);
		BigInteger two = new BigInteger(transferKey2, 16);
		BigInteger three = new BigInteger(transferKey3, 16);
		BigInteger transferkey_bigint = one.xor(two).xor(three);
		String transferKey = transferkey_bigint.toString(16);
		System.out.println(transferKey); //4298e9d60379b6f8d556b9c9a4e8f0c122255842564fd43f98598de194407d23
		System.out.println(kckcv.generateKCV(Hex.decodeHex(transferKey.toCharArray()), "AES", "AES/ECB/NoPadding"));
		
		//Decrypt the "GP Key encrypted" using the AES 256 bit transfer key in ECB mode
		System.out.println(kckcv.decrypt(Hex.decodeHex(transferKey.toCharArray()), "AES", "AES/ECB/NoPadding",Hex.decodeHex(encryptedGPKey.toCharArray())));
		
		
//		
//		BigInteger transferkeyy_bigint = new BigInteger(transferKey, 16);
//		BigInteger randomKey_1_BigInteger = new BigInteger(kckcv.generateRandomAESKey(256).getEncoded());
//		System.out.println(randomKey_1_BigInteger.toString(16));
//		System.out.println(kckcv.generateKCV(Hex.decodeHex(randomKey_1_BigInteger.toString(16).toCharArray()), "AES", "AES/ECB/NoPadding")); // KCV 2CD316
//		
//		BigInteger randomKey_2_BigInteger = new BigInteger(kckcv.generateRandomAESKey(256).getEncoded());
//		System.out.println(randomKey_2_BigInteger.toString(16));
//		System.out.println(kckcv.generateKCV(Hex.decodeHex(randomKey_2_BigInteger.toString(16).toCharArray()), "AES", "AES/ECB/NoPadding")); // KCV 2CD316
//		
//		BigInteger xor = transferkeyy_bigint.xor(randomKey_1_BigInteger).xor(randomKey_2_BigInteger);
//		System.out.println(xor.toString(16));
//		System.out.println(kckcv.generateKCV(Hex.decodeHex(xor.toString(16).toCharArray()), "AES", "AES/ECB/NoPadding")); // KCV 2CD316
//		
//		System.out.println(xor.xor(randomKey_1_BigInteger).xor(randomKey_2_BigInteger).toString(16));
//	
	}

	private String generateKCV(byte[] key, String algorithm, String cipherWithPadding)
			throws GeneralSecurityException, DecoderException {
		// Add Bouncy Castle Security Provider
		Security.addProvider(new BouncyCastleProvider());
		// Key Size in bytes
		System.out.println(key.length);
		// Construct a Secret Key from the given key
		SecretKey skey = new SecretKeySpec(key, algorithm);
		// Instantiate a AES/DES/DESede Cipher
		Cipher encrypter = Cipher.getInstance(cipherWithPadding, "BC");
		// Initialize the cipher with the key in Encrypt mode
		encrypter.init(Cipher.ENCRYPT_MODE, skey);
		// Encrypt at least {key.length/2}-byte null array with the cipher and return the first 6 Hex digits of the result
		// new byte[key.length] means 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 .....
		byte[] encryptedData= encrypter.doFinal(new byte[key.length]);
		return Hex.encodeHexString(encryptedData).substring(0, 6).toUpperCase();
	}
	
	private String decrypt(byte[] key, String algorithm, String cipherWithPadding, byte[] encryptedData)
			throws GeneralSecurityException, DecoderException {
		// Add Bouncy Castle Security Provider
		Security.addProvider(new BouncyCastleProvider());
		// Key Size in bytes
		System.out.println(key.length);
		// Construct a Secret Key from the given key
		SecretKey skey = new SecretKeySpec(key, algorithm);
		// Instantiate a AES/DES/DESede Cipher
		Cipher decrypter = Cipher.getInstance(cipherWithPadding, "BC");
		// Initialize the cipher with the key in Decrypt mode
		decrypter.init(Cipher.DECRYPT_MODE, skey);
		// Decrypt at least {key.length}-byte null array with the cipher and return the first 6 Hex digits of the result
		byte[] decryptedData= decrypter.doFinal(encryptedData);
		return Hex.encodeHexString(decryptedData).toUpperCase();
	}
	
	private Key generateRandomAESKey(int keySize) throws NoSuchAlgorithmException {
		Key key;
		SecureRandom rand = new SecureRandom();
		KeyGenerator generator = KeyGenerator.getInstance("AES");
		generator.init(keySize, rand);
		key = generator.generateKey();
		System.out.println("key length: "+key.getEncoded().length);
		return key;
	}
}
