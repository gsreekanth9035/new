
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class AddTransparency
{
	static int count = 0;
	static int extcount = 0;
  AddTransparency() throws IOException
  {
    String imagePath = "D:\\unifyia_workspace\\Project_Documents\\Team\\lal\\folder";
    File inFile = new File(imagePath, "22.png");
    BufferedImage image = ImageIO.read(inFile);

    Image transpImg1 = TransformGrayToTransparency(image);
    BufferedImage resultImage1 = ImageToBufferedImage(transpImg1, image.getWidth(), image.getHeight());

    File outFile1 = new File(imagePath, "map_with_transparency1.png");
    ImageIO.write(resultImage1, "PNG", outFile1);
    int rgb = image.getRGB(25, 25);    
    int r = (rgb & 0xFF0000) >> 16;
    int g = (rgb & 0xFF00) >> 8;
    int b = rgb & 0xFF;
    System.out.println("rgb::"+r+","+g+","+b);
//    final int r1 = ((r-100)<0)?0:r-100;
//    final int g1 = ((g-50)<0)?0:g-50;
//    final int b1 = ((b-80)<0)?0:b-80;
    final int r1 = ((r-70)<0)?0:r-70;
    final int g1 = ((g-60)<0)?0:g-60;
    final int b1 = ((b-40)<0)?0:b-40;
    final int r2 = ((r+45)>255)?255:r+45;
    final int g2 = ((g+35)>255)?255:g+35;
    final int b2 = ((b+55)>255)?255:b+55;
    System.out.println("rgb1::"+r1+","+g1+","+b1+"||||||rgb2::"+r2+","+g2+","+b2);
    //System.exit(0);
    // Blue
    //Image transpImg2 = TransformColorToTransparency(image, new Color(0, 0, 102), new Color(0,153,255));
    //Image transpImg2 = TransformColorToTransparency(image, new Color(0, 90, 130), new Color(103, 200, 255)); //rgb::3,141,230
    //Image transpImg2 = TransformColorToTransparency(image, new Color(0, 70, 126), new Color(100, 170, 255)); // rgb::0,118,226
    Image transpImg2 = TransformColorToTransparency(image, new Color(r1,g1,b1), new Color(r2,g2,b2)); // rgb::95,173,91
    BufferedImage resultImage2 = ImageToBufferedImage(transpImg2, image.getWidth(), image.getHeight());
   
    File outFile2 = new File(imagePath, "map_with_transparency2.png");
    ImageIO.write(resultImage2, "PNG", outFile2);
    System.out.println(count);
    System.out.println(extcount);
  }

  private Image TransformGrayToTransparency(BufferedImage image)
  {
    ImageFilter filter = new RGBImageFilter()
    {
      public final int filterRGB(int x, int y, int rgb)
      {
        return (rgb << 8) & 0xFF000000;
      }
    };

    ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
      return Toolkit.getDefaultToolkit().createImage(ip);
  }

  private Image TransformColorToTransparency(BufferedImage image, Color c1, Color c2)
  {
    // Primitive test, just an example
    final int r1 = c1.getRed();
    final int g1 = c1.getGreen();
    final int b1 = c1.getBlue();
    final int r2 = c2.getRed();
    final int g2 = c2.getGreen();
    final int b2 = c2.getBlue();
    //System.out.println("rgb1::"+r1+","+g1+","+b1);
   // System.out.println("rgb2::"+r2+","+g2+","+b2);

    ImageFilter filter = new RGBImageFilter()
    {
    	
      public final int filterRGB(int x, int y, int rgb)
      {
    	//System.out.println("rgb:"+(rgb & 0x0000FF)+":"+(rgb & 0x00FF00)+":"+(rgb & 0xFF));
    	//  System.out.println(rgb);
    	 int R = rgb/(256^2);

    	 int G = (rgb/256) % 256;

    	 int B = rgb%256;
    	// System.out.println("RGB::"+R+","+G+","+B);
        int r = (rgb & 0xFF0000) >> 16;
        int g = (rgb & 0xFF00) >> 8;
        int b = rgb & 0xFF;
        //System.out.println("rgb1::"+r1+","+g1+","+b1+"||||||"+r+","+g+","+b+"||||||"+"rgb2::"+r2+","+g2+","+b2);
        if (r >= r1 && r <= r2 &&
            g >= g1 && g <= g2 &&
            b >= b1 && b <= b2)
        {
        	count++;
          // Set fully transparent but keep color
           // System.out.println("entered");
          return rgb & 0xFFFFFF;
        	//return rgb ;
        }
        extcount++;
        return rgb;
      }
    };

    ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
      return Toolkit.getDefaultToolkit().createImage(ip);
  }

  private BufferedImage ImageToBufferedImage(Image image, int width, int height)
  {
    BufferedImage dest = new BufferedImage(
        width, height, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g2 = dest.createGraphics();
    g2.drawImage(image, 0, 0, null);
    g2.dispose();
    return dest;
  }

	/*
	 * public static void main(String[] args) throws IOException { AddTransparency
	 * at = new AddTransparency(); }
	 */
}