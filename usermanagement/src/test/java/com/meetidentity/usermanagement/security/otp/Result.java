package com.meetidentity.usermanagement.security.otp;

public class Result{
	
    public Result() {
	}
    public boolean status;
    public boolean value;
	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
	/**
	 * @return the value
	 */
	public boolean isValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(boolean value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "status=" + status + ", value=" + value;
	}
    
}