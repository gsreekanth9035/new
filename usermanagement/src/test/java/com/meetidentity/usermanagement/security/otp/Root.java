package com.meetidentity.usermanagement.security.otp;

public class Root {

	public Root() {
	}

	public Detail detail;
	public int id;
	public String jsonrpc;
	public Result result;
	public double time;
	public String version;
	public String versionnumber;
	public String signature;

	public Detail getDetail() {
		return detail;
	}

	public void setDetail(Detail detail) {
		this.detail = detail;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJsonrpc() {
		return jsonrpc;
	}

	public void setJsonrpc(String jsonrpc) {
		this.jsonrpc = jsonrpc;
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getVersionnumber() {
		return versionnumber;
	}

	public void setVersionnumber(String versionnumber) {
		this.versionnumber = versionnumber;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	@Override
	public String toString() {
		return "detail=" + detail.toString() + ", id=" + id + ", jsonrpc=" + jsonrpc + ", result=" + result.toString()
				+ ", time=" + time + ", version=" + version + ", versionnumber=" + versionnumber;
	}

}