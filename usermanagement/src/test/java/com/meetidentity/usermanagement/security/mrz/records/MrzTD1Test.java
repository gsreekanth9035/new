package com.meetidentity.usermanagement.security.mrz.records;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.meetidentity.usermanagement.security.mrz.MrzParser;
import com.meetidentity.usermanagement.security.mrz.records.MrzTD1;
import com.meetidentity.usermanagement.security.mrz.types.MrzDate;
import com.meetidentity.usermanagement.security.mrz.types.MrzDocumentCode;
import com.meetidentity.usermanagement.security.mrz.types.MrzSex;

public class MrzTD1Test {
	    @Test
	    public void testTd1Parsing() {
	        final MrzTD1 r = (MrzTD1) MrzParser.parse("CIUTOD231458907A123X5328434D23\n3407127M9507122UTO<<<<<<<<<<<6\nSTEVENSON<<PETER<<<<<<<<<<<<<<\n");
	        assertEquals(MrzDocumentCode.TypeC, r.code);
	        assertEquals('C', r.code1);
	        assertEquals('I', r.code2);
	        assertEquals("UTO", r.issuingCountry);
	        assertEquals("UTO", r.nationality);
	        assertEquals("D23145890", r.documentNumber);
	        assertEquals("A123X5328434D23", r.optional);
	        assertEquals("", r.optional2);
	        assertEquals(new MrzDate(95, 7, 12), r.expirationDate);
	        assertEquals(new MrzDate(34, 7, 12), r.dateOfBirth);
	        assertEquals(MrzSex.Male, r.sex);
	        assertEquals("STEVENSON", r.surname);
	        assertEquals("PETER", r.givenNames);
	    }

	    @Test
	    public void testToMrz() {
	        final MrzTD1 r = new MrzTD1();
	        r.code1 = 'C';
	        r.code2 = 'I';
	        r.issuingCountry = "UTO";
	        r.nationality = "UTO";
	        r.documentNumber = "D23145890";
	        r.optional = "";
	        r.optional2 = "";
	        r.expirationDate = new MrzDate(95, 7, 12);
	        r.dateOfBirth = new MrzDate(34, 7, 12);
	        r.sex = MrzSex.Male;
	        r.surname = "Stevenson";
	        r.givenNames = "Peter";
	        System.out.println( r.toMrz_line1());
	        System.out.println( r.toMrz_line2());
	        System.out.println( r.toMrz_line3());
	        //assertEquals("CIUTOD231458907A123X5328434D23\n3407127M9507122UTO<<<<<<<<<<<6\nSTEVENSON<<PETER<<<<<<<<<<<<<<\n", r.toMrz());
	    }
	
}
