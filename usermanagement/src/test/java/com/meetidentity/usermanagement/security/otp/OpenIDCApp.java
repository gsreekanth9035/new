package com.meetidentity.usermanagement.security.otp;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.Instant;
import javax.crypto.spec.SecretKeySpec;

import com.meetidentity.usermanagement.security.otp.Base32String.DecodingException;

public class OpenIDCApp {	
    public static void main(final String[] args) throws NoSuchAlgorithmException, InvalidKeyException, DecodingException {
    	//defaults, 30sec length=6, algorithm:HmacSHA1
    	// OR3FM4SUOFZTK4LBGVXTSYSHHBZFI43V 
    	int otp = generateTOTP("IVKHO5TIPJYXQUKRJVSEIU3TMVUUGNJX");	
    	System.out.println(otp);
    			
		int _otp = generateTOTP("IVKHO5TIPJYXQUKRJVSEIU3TMVUUGNJX", 30, 6, "HmacSHA1");
		System.out.println(_otp);		 
    }    
    
    public static int generateTOTP(String seed) throws InvalidKeyException, DecodingException, NoSuchAlgorithmException {    	
    	//defaults, 30sec length=6, algorithm:HmacSHA1
        final TimeBasedOneTimePasswordGenerator totp = new TimeBasedOneTimePasswordGenerator();
        
        Key HMAC_SHA1_KEY = new SecretKeySpec(new String(Base32String.decode(seed)).getBytes(StandardCharsets.US_ASCII), "RAW");
		
        final Instant now = Instant.now();
        final Instant later = now.plus(totp.getTimeStep());

        System.out.format("Current password: %06d\n", totp.generateOneTimePassword(HMAC_SHA1_KEY, now));
        System.out.format("Future password:  %06d\n", totp.generateOneTimePassword(HMAC_SHA1_KEY, later));
        
        return totp.generateOneTimePassword(HMAC_SHA1_KEY, now);
    }
    
    public static int generateTOTP(String seed, int timestep, int passwordLength, String algorithm) throws InvalidKeyException, DecodingException, NoSuchAlgorithmException {
        final TimeBasedOneTimePasswordGenerator totp = new TimeBasedOneTimePasswordGenerator(Duration.ofSeconds(timestep), passwordLength, algorithm );
        
        Key key = new SecretKeySpec(new String(Base32String.decode(seed)).getBytes(StandardCharsets.US_ASCII), "RAW");
		
        final Instant now = Instant.now();
        final Instant later = now.plus(totp.getTimeStep());

        System.out.format("Current password: %06d\n", totp.generateOneTimePassword(key, now));
        System.out.format("Future password:  %06d\n", totp.generateOneTimePassword(key, later));
        
        return totp.generateOneTimePassword(key, now);
    }
}
