package com.meetidentity.usermanagement.security.otp;

public class Detail{
	
    public Detail() {
	}
    public String public_key;
    public String rollout_state;
    public String serial;
    public long threadid;
	public String getPublic_key() {
		return public_key;
	}
	public void setPublic_key(String public_key) {
		this.public_key = public_key;
	}
	public String getRollout_state() {
		return rollout_state;
	}
	public void setRollout_state(String rollout_state) {
		this.rollout_state = rollout_state;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public long getThreadid() {
		return threadid;
	}
	public void setThreadid(long threadid) {
		this.threadid = threadid;
	}
	@Override
	public String toString() {
		return "public_key=" + public_key + ", rollout_state=" + rollout_state + ", serial=" + serial
				+ ", threadid=" + threadid;
	}
    
}
