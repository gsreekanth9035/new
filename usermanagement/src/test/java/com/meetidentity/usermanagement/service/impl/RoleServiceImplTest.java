package com.meetidentity.usermanagement.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.meetidentity.usermanagement.UsermanagementApp;
import com.meetidentity.usermanagement.domain.RoleEntity;
import com.meetidentity.usermanagement.repository.RoleRepository;
import com.meetidentity.usermanagement.service.KeycloakService;
import com.meetidentity.usermanagement.service.RoleService;
import com.meetidentity.usermanagement.web.rest.model.Role;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UsermanagementApp.class)
public class RoleServiceImplTest {

	@Autowired
	private RoleService roleService;

	@MockBean
	private KeycloakService keycloakService;

	@MockBean
	private RoleRepository roleRepository;

	@Test
	public void testCreateRole() {
		// given
		String organization = "meetidentity";

		Role role = new Role();
		given(keycloakService.createRole(role,"")).willReturn(0);

		// when
		roleService.createRole(organization, role);

		// then
		then(keycloakService).should().createRole(role,"");

	}


	@Test
	public void testGetRoles_withEmptyRoles() {
		// given
		String organization = "meetidentity";

		given(roleRepository.findAllRolesByOrganization(organization)).willReturn(new ArrayList<>());

		// when
		List<Role> roles = roleService.getRoles(organization);

		// then
		assertThat(roles.size()).isEqualTo(0);
		then(roleRepository).should().findAllRolesByOrganization(organization);
	}

	@Test
	public void testGetRoles_withTwoRoles() {
		// given
		String organization = "meetidentity";

		List<RoleEntity> roleEntities = new ArrayList<>();

		RoleEntity roleEntity1 = new RoleEntity();
		roleEntity1.setName("ROLE_ADMIN");

		RoleEntity roleEntity2 = new RoleEntity();
		roleEntity2.setName("ROLE_OPERATOR");

		roleEntities.add(roleEntity1);
		roleEntities.add(roleEntity2);

		given(roleRepository.findAllRolesByOrganization(organization)).willReturn(roleEntities);

		// when
		List<Role> roles = roleService.getRoles(organization);

		// then
		assertThat(roles.size()).isEqualTo(2);
		assertThat(roles.get(0).getName()).isEqualTo(roleEntity1.getName());
		assertThat(roles.get(1).getName()).isEqualTo(roleEntity2.getName());

		then(roleRepository).should().findAllRolesByOrganization(organization);
	}

	@Test
	public void testGetRoleByName_withInvalidRoleName() {
		// given
		String organization = "meetidentity";

		String roleName = "Invalid";
		given(roleRepository.findRoleByOrganization(organization, roleName)).willReturn(null);

		// when
		Role role = roleService.getRoleByName(organization, roleName);

		// then
		assertThat(role).isNull();
		then(roleRepository).should().findRoleByOrganization(organization, roleName);
	}

	@Test
	public void testGetRoleByName_withValidRoleName() {
		// given
		String organization = "meetidentity";

		String roleName = "ROLE_ADMIN";

		RoleEntity roleEntity = new RoleEntity();
		roleEntity.setName(roleName);
		given(roleRepository.findRoleByOrganization(organization, roleName)).willReturn(roleEntity);

		// when
		Role role = roleService.getRoleByName(organization, roleName);

		// then
		assertThat(role).isNotNull();
		assertThat(role.getName()).isEqualTo(roleName);
		then(roleRepository).should().findRoleByOrganization(organization, roleName);
	}


}
