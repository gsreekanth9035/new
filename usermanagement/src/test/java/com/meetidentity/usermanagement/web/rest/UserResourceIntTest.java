package com.meetidentity.usermanagement.web.rest;

import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import com.meetidentity.usermanagement.UsermanagementApp;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserBiometric;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UsermanagementApp.class, webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserResourceIntTest {

	@LocalServerPort
	private int port;
	
	TestRestTemplate testRestTemplate = new TestRestTemplate();
	
	HttpHeaders httpHeaders = new HttpHeaders();
	
	@Test
	public void testRegisterUser() {
		
		User user = new User();
		user.setName("Heyy Test2");
		
		UserBiometric userBiometric = new UserBiometric();
		userBiometric.setType("Fingerprint");
		userBiometric.setPosition("Thumb");
		userBiometric.setFormat("PNG");
		
		try {
			BufferedImage image = ImageIO.read(ResourceUtils.getFile("classpath:testimages/dash.png"));
			ByteArrayOutputStream ostream = new ByteArrayOutputStream();
			ImageIO.write(image, "png", ostream);
			userBiometric.setData(ostream.toByteArray());
			
		} catch (IOException e) {
		}
		
		user.getUserBiometrics().add(userBiometric);
		
		HttpEntity<User> request = new HttpEntity<>(user, httpHeaders);
		
		ResponseEntity<String> response = testRestTemplate.exchange("http://localhost:" + port + "/api/v1/users" , HttpMethod.POST , request, String.class);
		
		assertTrue(response.getStatusCodeValue() == 201);
	
	}

}
