package com.meetidentity.usermanagement.web.rest;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.meetidentity.usermanagement.UsermanagementApp;
import com.meetidentity.usermanagement.service.RoleService;
import com.meetidentity.usermanagement.web.rest.model.Role;	

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UsermanagementApp.class)
@AutoConfigureMockMvc
public class RoleResourceTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private RoleService roleService;

    @Before
    public void setup() {
    }

	@Test
	public void testCreateRole() throws Exception {
		// given
		String organization = "meetidentity";

		String roleJson = "{ \"name\": \"ROLE_ADMIN\", \"description\": \"This is Admin Role\" }";

		willDoNothing().given(roleService).createRole(any(String.class), any(Role.class));

		// when
		mockMvc.perform(post("/api/v1/organizations/"+ organization + "/roles").contentType(MediaType.APPLICATION_JSON_UTF8).content(roleJson))
			.andExpect(status().isCreated()); // Then

		// then
		then(roleService).should().createRole(any(String.class), any(Role.class));
	}

	@Test
	public void testGetRoles_willReturnEmptyRoles() throws Exception {
		// given
		String organization = "meetidentity";

		given(roleService.getRoles(organization)).willReturn(new ArrayList<>());

		// when
		mockMvc.perform(get("/api/v1/organizations/"+ organization + "/roles"))
			.andExpect(status().isOk()) // then
			.andExpect(jsonPath("$", hasSize(0))); // then

		// then
		then(roleService).should().getRoles(organization);
	}

	@Test
	public void testGetRoles_willReturnTwoRoles() throws Exception {
		// given
		List<Role> roles = new ArrayList<>();

		String organization = "meetidentity";

		Role role1 = new Role();
		role1.setName("ROLE_ADMIN");

		Role role2 = new Role();
		role2.setName("ROLE_OPERATOR");

		roles.add(role1);
		roles.add(role2);

		given(roleService.getRoles(organization)).willReturn(roles);

		// when
		mockMvc.perform(get("/api/v1/organizations/"+ organization + "/roles"))
			.andExpect(status().isOk()) // then
			.andExpect(jsonPath("$", hasSize(2))) // then
			.andExpect(jsonPath("$[0].name", equalTo(role1.getName()))) // then
			.andExpect(jsonPath("$[1].name", equalTo(role2.getName()))); // then

		// then
		then(roleService).should().getRoles(organization);
	}

	@Test
	public void testGetRoleByName_invalidRoleName() throws Exception {
		// given
		String organization = "meetidentity";

		String roleName = "Invalid";

		given(roleService.getRoleByName(organization, roleName)).willReturn(null);

		// when
		mockMvc.perform(get("/api/v1/organizations/"+ organization + "/roles/" + roleName))
			.andExpect(status().isOk()); // then
			// TODO how to check for null

		// then
		then(roleService).should().getRoleByName(organization, roleName);
	}

	@Test
	public void testGetRoleByName_validRoleName() throws Exception {
		// given
		String organization = "meetidentity";

		String roleName = "Admin";

		Role role = new Role();
		role.setName(roleName);
		given(roleService.getRoleByName(organization, roleName)).willReturn(role);

		// when
		mockMvc.perform(get("/api/v1/organizations/"+ organization + "/roles/" + roleName))
			.andExpect(status().isOk()) // then
			.andExpect(jsonPath("$.name", equalTo(roleName))); // then

		// then
		then(roleService).should().getRoleByName(organization, roleName);
	}

}
