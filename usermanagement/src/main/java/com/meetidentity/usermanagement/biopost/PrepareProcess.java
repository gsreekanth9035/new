package com.meetidentity.usermanagement.biopost;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONObject;

public class PrepareProcess {

	static ProcessTriada processTriada = new ProcessTriada();
	static JSONObject parent = new JSONObject();

	public static void init(ProcessTriada processTriada) throws Exception {
		parent = new JSONObject(processTriada);
	}

	public static void post() throws Exception {
		URL url = new URL("https://biowsl:83/api/ProcesaTriada");
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("TLS");
		// Create a trust manager that does not validate certificate chains
		sc.init(null, new TrustManager[] { new X509TrustManager() {

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				// TODO Auto-generated method stub

			}

			@Override
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				// TODO Auto-generated method stub

			}
		} }, new java.security.SecureRandom());
		con.setSSLSocketFactory(sc.getSocketFactory());
		con.setConnectTimeout(60000);
		// add request header
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		// con.setRequestProperty("Authorization", "Bearer " +
		// accessToken.getTokenValue());
		// Send post request
		con.setDoOutput(true);

		try (OutputStream os = con.getOutputStream()) {
			System.out.println(parent.toString());
			byte[] input = parent.toString().getBytes("UTF-8");
			os.write(input, 0, input.length);

			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
			StringBuilder response = new StringBuilder();
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
			System.out.println(response.toString());
		} catch (FileNotFoundException e) {
		}
		;
	}
}
