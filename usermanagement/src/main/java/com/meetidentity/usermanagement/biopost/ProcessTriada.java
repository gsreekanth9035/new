package com.meetidentity.usermanagement.biopost;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProcessTriada {
	@JsonProperty("P_CEDULA")
	public String p_CEDULA;
	@JsonProperty("P_USUARIO_TRIADA")
	public String p_USUARIO_TRIADA;
	@JsonProperty("P_COMENTARIO")
	public String p_COMENTARIO;
	@JsonProperty("P_ESTACION_CAPTURA")
	public String p_ESTACION_CAPTURA;
	@JsonProperty("P_OFICINA_RETIRO")
	public String p_OFICINA_RETIRO;
	@JsonProperty("P_LUGAR_NACIMIENTO")
	public String p_LUGAR_NACIMIENTO;
	@JsonProperty("P_PAIS_NACIONALIDAD_TEXTO")
	public String p_PAIS_NACIONALIDAD_TEXTO;
	@JsonProperty("P_PROVINCIA_NACIMIENTO_TEXTO")
	public String p_PROVINCIA_NACIMIENTO_TEXTO;
	@JsonProperty("P_PAIS_TEXTO")
	public String p_PAIS_TEXTO;
	@JsonProperty("P_OFICINA_CAPTURA_TEXTO")
	public String p_OFICINA_CAPTURA_TEXTO;
	@JsonProperty("P_OFICINA_ENTREGA_TEXTO")
	public String p_OFICINA_ENTREGA_TEXTO;
	@JsonProperty("P_PROVINCIA_SOLICITUD_TEXTO")
	public String p_PROVINCIA_SOLICITUD_TEXTO;
	@JsonProperty("P_DISTRITO_NACIMIENTO_TEXTO")
	public String p_DISTRITO_NACIMIENTO_TEXTO;
	@JsonProperty("P_FOTO")
	public String p_FOTO;
	@JsonProperty("P_FIRMA")
	public String p_FIRMA;
	@JsonProperty("P_DEDO_HUELLA_1")
	public int p_DEDO_HUELLA_1;
	@JsonProperty("P_DEDO_HUELLA_2")
	public int p_DEDO_HUELLA_2;
	@JsonProperty("P_HUELLA_PULGAR_DERECHO")
	public String p_HUELLA_PULGAR_DERECHO;
	@JsonProperty("P_HUELLA_PULGAR_IZQUIERDO")
	public String p_HUELLA_PULGAR_IZQUIERDO;
	@JsonProperty("P_HUELLA_INDICE_DERECHO")
	public String p_HUELLA_INDICE_DERECHO;
	@JsonProperty("P_HUELLA_INDICE_IZQUIERDO")
	public String p_HUELLA_INDICE_IZQUIERDO;
	@JsonProperty("P_HUELLA_MEDIO_DERECHO")
	public String p_HUELLA_MEDIO_DERECHO;
	@JsonProperty("P_HUELLA_MEDIO_IZQUIERDO")
	public String p_HUELLA_MEDIO_IZQUIERDO;
	@JsonProperty("P_HUELLA_ANULAR_DERECHO")
	public String p_HUELLA_ANULAR_DERECHO;
	@JsonProperty("P_HUELLA_ANULAR_IZQUIERDO")
	public String p_HUELLA_ANULAR_IZQUIERDO;
	@JsonProperty("P_HUELLA_MENIQUE_DERECHO")
	public String p_HUELLA_MENIQUE_DERECHO;
	@JsonProperty("P_HUELLA_MENIQUE_IZQUIERDO")
	public String p_HUELLA_MENIQUE_IZQUIERDO;
	@JsonProperty("P_MINUTIA1")
	public String p_MINUTIA1;
	@JsonProperty("P_MINUTIA2")
	public String p_MINUTIA2;
	@JsonProperty("P_SOLICITUD_IDENTIFICADOR")
	public String p_SOLICITUD_IDENTIFICADOR;

	public String getP_CEDULA() {
		return p_CEDULA;
	}

	public void setP_CEDULA(String p_CEDULA) {
		this.p_CEDULA = p_CEDULA;
	}

	public String getP_USUARIO_TRIADA() {
		return p_USUARIO_TRIADA;
	}

	public void setP_USUARIO_TRIADA(String p_USUARIO_TRIADA) {
		this.p_USUARIO_TRIADA = p_USUARIO_TRIADA;
	}

	public String getP_COMENTARIO() {
		return p_COMENTARIO;
	}

	public void setP_COMENTARIO(String p_COMENTARIO) {
		this.p_COMENTARIO = p_COMENTARIO;
	}

	public String getP_ESTACION_CAPTURA() {
		return p_ESTACION_CAPTURA;
	}

	public void setP_ESTACION_CAPTURA(String p_ESTACION_CAPTURA) {
		this.p_ESTACION_CAPTURA = p_ESTACION_CAPTURA;
	}

	public String getP_OFICINA_RETIRO() {
		return p_OFICINA_RETIRO;
	}

	public void setP_OFICINA_RETIRO(String p_OFICINA_RETIRO) {
		this.p_OFICINA_RETIRO = p_OFICINA_RETIRO;
	}

	public String getP_LUGAR_NACIMIENTO() {
		return p_LUGAR_NACIMIENTO;
	}

	public void setP_LUGAR_NACIMIENTO(String p_LUGAR_NACIMIENTO) {
		this.p_LUGAR_NACIMIENTO = p_LUGAR_NACIMIENTO;
	}

	public String getP_PAIS_NACIONALIDAD_TEXTO() {
		return p_PAIS_NACIONALIDAD_TEXTO;
	}

	public void setP_PAIS_NACIONALIDAD_TEXTO(String p_PAIS_NACIONALIDAD_TEXTO) {
		this.p_PAIS_NACIONALIDAD_TEXTO = p_PAIS_NACIONALIDAD_TEXTO;
	}

	public String getP_PROVINCIA_NACIMIENTO_TEXTO() {
		return p_PROVINCIA_NACIMIENTO_TEXTO;
	}

	public void setP_PROVINCIA_NACIMIENTO_TEXTO(String p_PROVINCIA_NACIMIENTO_TEXTO) {
		this.p_PROVINCIA_NACIMIENTO_TEXTO = p_PROVINCIA_NACIMIENTO_TEXTO;
	}

	public String getP_PAIS_TEXTO() {
		return p_PAIS_TEXTO;
	}

	public void setP_PAIS_TEXTO(String p_PAIS_TEXTO) {
		this.p_PAIS_TEXTO = p_PAIS_TEXTO;
	}

	public String getP_OFICINA_CAPTURA_TEXTO() {
		return p_OFICINA_CAPTURA_TEXTO;
	}

	public void setP_OFICINA_CAPTURA_TEXTO(String p_OFICINA_CAPTURA_TEXTO) {
		this.p_OFICINA_CAPTURA_TEXTO = p_OFICINA_CAPTURA_TEXTO;
	}

	public String getP_OFICINA_ENTREGA_TEXTO() {
		return p_OFICINA_ENTREGA_TEXTO;
	}

	public void setP_OFICINA_ENTREGA_TEXTO(String p_OFICINA_ENTREGA_TEXTO) {
		this.p_OFICINA_ENTREGA_TEXTO = p_OFICINA_ENTREGA_TEXTO;
	}

	public String getP_PROVINCIA_SOLICITUD_TEXTO() {
		return p_PROVINCIA_SOLICITUD_TEXTO;
	}

	public void setP_PROVINCIA_SOLICITUD_TEXTO(String p_PROVINCIA_SOLICITUD_TEXTO) {
		this.p_PROVINCIA_SOLICITUD_TEXTO = p_PROVINCIA_SOLICITUD_TEXTO;
	}

	public String getP_DISTRITO_NACIMIENTO_TEXTO() {
		return p_DISTRITO_NACIMIENTO_TEXTO;
	}

	public void setP_DISTRITO_NACIMIENTO_TEXTO(String p_DISTRITO_NACIMIENTO_TEXTO) {
		this.p_DISTRITO_NACIMIENTO_TEXTO = p_DISTRITO_NACIMIENTO_TEXTO;
	}

	public String getP_FOTO() {
		return p_FOTO;
	}

	public void setP_FOTO(String p_FOTO) {
		this.p_FOTO = p_FOTO;
	}

	public String getP_FIRMA() {
		return p_FIRMA;
	}

	public void setP_FIRMA(String p_FIRMA) {
		this.p_FIRMA = p_FIRMA;
	}

	public int getP_DEDO_HUELLA_1() {
		return p_DEDO_HUELLA_1;
	}

	public void setP_DEDO_HUELLA_1(int p_DEDO_HUELLA_1) {
		this.p_DEDO_HUELLA_1 = p_DEDO_HUELLA_1;
	}

	public int getP_DEDO_HUELLA_2() {
		return p_DEDO_HUELLA_2;
	}

	public void setP_DEDO_HUELLA_2(int p_DEDO_HUELLA_2) {
		this.p_DEDO_HUELLA_2 = p_DEDO_HUELLA_2;
	}

	public String getP_HUELLA_PULGAR_DERECHO() {
		return p_HUELLA_PULGAR_DERECHO;
	}

	public void setP_HUELLA_PULGAR_DERECHO(String p_HUELLA_PULGAR_DERECHO) {
		this.p_HUELLA_PULGAR_DERECHO = p_HUELLA_PULGAR_DERECHO;
	}

	public String getP_HUELLA_PULGAR_IZQUIERDO() {
		return p_HUELLA_PULGAR_IZQUIERDO;
	}

	public void setP_HUELLA_PULGAR_IZQUIERDO(String p_HUELLA_PULGAR_IZQUIERDO) {
		this.p_HUELLA_PULGAR_IZQUIERDO = p_HUELLA_PULGAR_IZQUIERDO;
	}

	public String getP_HUELLA_INDICE_DERECHO() {
		return p_HUELLA_INDICE_DERECHO;
	}

	public void setP_HUELLA_INDICE_DERECHO(String p_HUELLA_INDICE_DERECHO) {
		this.p_HUELLA_INDICE_DERECHO = p_HUELLA_INDICE_DERECHO;
	}

	public String getP_HUELLA_INDICE_IZQUIERDO() {
		return p_HUELLA_INDICE_IZQUIERDO;
	}

	public void setP_HUELLA_INDICE_IZQUIERDO(String p_HUELLA_INDICE_IZQUIERDO) {
		this.p_HUELLA_INDICE_IZQUIERDO = p_HUELLA_INDICE_IZQUIERDO;
	}

	public String getP_HUELLA_MEDIO_DERECHO() {
		return p_HUELLA_MEDIO_DERECHO;
	}

	public void setP_HUELLA_MEDIO_DERECHO(String p_HUELLA_MEDIO_DERECHO) {
		this.p_HUELLA_MEDIO_DERECHO = p_HUELLA_MEDIO_DERECHO;
	}

	public String getP_HUELLA_MEDIO_IZQUIERDO() {
		return p_HUELLA_MEDIO_IZQUIERDO;
	}

	public void setP_HUELLA_MEDIO_IZQUIERDO(String p_HUELLA_MEDIO_IZQUIERDO) {
		this.p_HUELLA_MEDIO_IZQUIERDO = p_HUELLA_MEDIO_IZQUIERDO;
	}

	public String getP_HUELLA_ANULAR_DERECHO() {
		return p_HUELLA_ANULAR_DERECHO;
	}

	public void setP_HUELLA_ANULAR_DERECHO(String p_HUELLA_ANULAR_DERECHO) {
		this.p_HUELLA_ANULAR_DERECHO = p_HUELLA_ANULAR_DERECHO;
	}

	public String getP_HUELLA_ANULAR_IZQUIERDO() {
		return p_HUELLA_ANULAR_IZQUIERDO;
	}

	public void setP_HUELLA_ANULAR_IZQUIERDO(String p_HUELLA_ANULAR_IZQUIERDO) {
		this.p_HUELLA_ANULAR_IZQUIERDO = p_HUELLA_ANULAR_IZQUIERDO;
	}

	public String getP_HUELLA_MENIQUE_DERECHO() {
		return p_HUELLA_MENIQUE_DERECHO;
	}

	public void setP_HUELLA_MENIQUE_DERECHO(String p_HUELLA_MENIQUE_DERECHO) {
		this.p_HUELLA_MENIQUE_DERECHO = p_HUELLA_MENIQUE_DERECHO;
	}

	public String getP_HUELLA_MENIQUE_IZQUIERDO() {
		return p_HUELLA_MENIQUE_IZQUIERDO;
	}

	public void setP_HUELLA_MENIQUE_IZQUIERDO(String p_HUELLA_MENIQUE_IZQUIERDO) {
		this.p_HUELLA_MENIQUE_IZQUIERDO = p_HUELLA_MENIQUE_IZQUIERDO;
	}

	public String getP_MINUTIA1() {
		return p_MINUTIA1;
	}

	public void setP_MINUTIA1(String p_MINUTIA1) {
		this.p_MINUTIA1 = p_MINUTIA1;
	}

	public String getP_MINUTIA2() {
		return p_MINUTIA2;
	}

	public void setP_MINUTIA2(String p_MINUTIA2) {
		this.p_MINUTIA2 = p_MINUTIA2;
	}

	public String getP_SOLICITUD_IDENTIFICADOR() {
		return p_SOLICITUD_IDENTIFICADOR;
	}

	public void setP_SOLICITUD_IDENTIFICADOR(String p_SOLICITUD_IDENTIFICADOR) {
		this.p_SOLICITUD_IDENTIFICADOR = p_SOLICITUD_IDENTIFICADOR;
	}

}
