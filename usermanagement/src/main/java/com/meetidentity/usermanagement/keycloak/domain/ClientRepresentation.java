package com.meetidentity.usermanagement.keycloak.domain;

import java.util.List;

public class ClientRepresentation {

	private String id;
	private String clientId;
	private List<String> redirectUris;
	private List<String> webOrigins;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public List<String> getRedirectUris() {
		return redirectUris;
	}

	public void setRedirectUris(List<String> redirectUris) {
		this.redirectUris = redirectUris;
	}

	public List<String> getWebOrigins() {
		return webOrigins;
	}

	public void setWebOrigins(List<String> webOrigins) {
		this.webOrigins = webOrigins;
	}

}
