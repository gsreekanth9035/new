package com.meetidentity.usermanagement.keycloak.domain;

import java.util.List;

public class GroupRepresentation {

	private String id;
	private String name;
	private String path;
	private List<String> realmRoles;
	private List<GroupRepresentation> subGroups;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<String> getRealmRoles() {
		return realmRoles;
	}

	public void setRealmRoles(List<String> realmRoles) {
		this.realmRoles = realmRoles;
	}

	public List<GroupRepresentation> getSubGroups() {
		return subGroups;
	}

	public void setSubGroups(List<GroupRepresentation> subGroups) {
		this.subGroups = subGroups;
	}

}
