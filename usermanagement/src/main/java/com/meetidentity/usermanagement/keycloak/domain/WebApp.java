package com.meetidentity.usermanagement.keycloak.domain;

import java.util.List;

public class WebApp {
	private String id;
	private String client;
	private List<Mappings> mappings;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public List<Mappings> getMappings() {
		return mappings;
	}

	public void setMappings(List<Mappings> mappings) {
		this.mappings = mappings;
	}

}