package com.meetidentity.usermanagement.keycloak.domain;

public class UserStats {
	
	private String type;
	private int totalUsers;
	private int totalEnrolled;
	private int totalActive;
	private int totalSuspended;
	private int totalRevoked;
	private int totalExpired;
	
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getTotalUsers() {
		return totalUsers;
	}
	public void setTotalUsers(int totalUsers) {
		this.totalUsers = totalUsers;
	}
	public int getTotalEnrolled() {
		return totalEnrolled;
	}
	public void setTotalEnrolled(int totalEnrolled) {
		this.totalEnrolled = totalEnrolled;
	}
	public int getTotalActive() {
		return totalActive;
	}
	public void setTotalActive(int totalActive) {
		this.totalActive = totalActive;
	}
	public int getTotalSuspended() {
		return totalSuspended;
	}
	public void setTotalSuspended(int totalSuspended) {
		this.totalSuspended = totalSuspended;
	}
	public int getTotalRevoked() {
		return totalRevoked;
	}
	public void setTotalRevoked(int totalRevoked) {
		this.totalRevoked = totalRevoked;
	}
	public int getTotalExpired() {
		return totalExpired;
	}
	public void setTotalExpired(int totalExpired) {
		this.totalExpired = totalExpired;
	}
	

}
