package com.meetidentity.usermanagement.keycloak.domain;

public class ClientMappings {
	private WebApp web_app;

	public WebApp getWeb_app() {
		return web_app;
	}

	public void setWeb_app(WebApp web_app) {
		this.web_app = web_app;
	}

}