package com.meetidentity.usermanagement.keycloak.domain;

import java.util.Map;

public class UserSessionRepresentation {

	private String id;
	private String username;
	private String userId;
	private long start;
	private long lastAccess;
	private String ipAddress;
	private Map<String, String> clients;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public long getStart() {
		return start;
	}
	public void setStart(long start) {
		this.start = start;
	}
	public long getLastAccess() {
		return lastAccess;
	}
	public void setLastAccess(long lastAccess) {
		this.lastAccess = lastAccess;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Map<String, String> getClients() {
		return clients;
	}
	public void setClients(Map<String, String> clients) {
		this.clients = clients;
	}
	

	

}
