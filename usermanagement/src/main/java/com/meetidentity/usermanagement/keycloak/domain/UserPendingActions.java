package com.meetidentity.usermanagement.keycloak.domain;

import java.util.List;

public class UserPendingActions {

	private String id;
	private String firstName;
	private String lastName;
	private String email;
	private String createdDate;
	private Boolean isFederated;
	private String groupName;
	private String groupId;
	private String identityTypeName;

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	private boolean enabled;

	private String username;
	private List<CredentialRepresentation> credentials;
    private	String enrolledBy;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<CredentialRepresentation> getCredentials() {
		return credentials;
	}

	public void setCredentials(List<CredentialRepresentation> credentials) {
		this.credentials = credentials;
	}

	public Boolean getIsFederated() {
		return isFederated;
	}

	public void setIsFederated(Boolean isFederated) {
		this.isFederated = isFederated;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public void setFederated(boolean isFederated) {
		this.isFederated = isFederated;
	}

	public String getIdentityTypeName() {
		return identityTypeName;
	}

	public void setIdentityTypeName(String identityTypeName) {
		this.identityTypeName = identityTypeName;
	}
	
	public String getEnrolledBy() {
		return enrolledBy;
	}

	public void setEnrolledBy(String enrolledBy) {
		this.enrolledBy = enrolledBy;
	}


}
