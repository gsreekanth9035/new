package com.meetidentity.usermanagement.keycloak.domain;

import java.util.List;

public class KeycloakUserAttributes {
	private List<String> principalName;
	// private List<String> LDAP_ID;
	private List<String> groupID;

	private List<String> middleName;
	private List<String> gender;
	// Address
	private List<String> country;
	private List<String> city;
	private List<String> state;
	private List<String> street;
	private List<String> postalCode;

	private List<String> photo;
	private List<String> countryCode;
	private List<String> contactNumber;

	private List<String> company;
	private List<String> department;

	private List<String> dob;
	// user appearance
	private List<String> height;
	private List<String> eyeColor;
	private List<String> hairColor;
	private List<String> bloodType;
	private List<String> personalCode;
	private List<String> curp;
	private List<String> electorKey;
	private List<String> yearOfRegistration;
	private List<String> mothersMaidenName;
	
	private List<String> publicKey;
	private List<String> pushToken;

	public List<String> getGroupID() {
		return groupID;
	}

	public List<String> getPrincipalName() {
		return principalName;
	}

	public void setPrincipalName(List<String> principalName) {
		this.principalName = principalName;
	}

	public void setGroupID(List<String> groupID) {
		this.groupID = groupID;
	}

	public List<String> getMiddleName() {
		return middleName;
	}

	public void setMiddleName(List<String> middleName) {
		this.middleName = middleName;
	}

	public List<String> getGender() {
		return gender;
	}

	public void setGender(List<String> gender) {
		this.gender = gender;
	}

	public List<String> getCountry() {
		return country;
	}

	public void setCountry(List<String> country) {
		this.country = country;
	}

	public List<String> getCity() {
		return city;
	}

	public void setCity(List<String> city) {
		this.city = city;
	}

	public List<String> getState() {
		return state;
	}

	public void setState(List<String> state) {
		this.state = state;
	}

	public List<String> getStreet() {
		return street;
	}

	public void setStreet(List<String> street) {
		this.street = street;
	}

	public List<String> getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(List<String> postalCode) {
		this.postalCode = postalCode;
	}

	public List<String> getPhoto() {
		return photo;
	}

	public void setPhoto(List<String> photo) {
		this.photo = photo;
	}

	public List<String> getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(List<String> contactNumber) {
		this.contactNumber = contactNumber;
	}

	public List<String> getCompany() {
		return company;
	}

	public void setCompany(List<String> company) {
		this.company = company;
	}

	public List<String> getDepartment() {
		return department;
	}

	public void setDepartment(List<String> department) {
		this.department = department;
	}

	public List<String> getDob() {
		return dob;
	}

	public void setDob(List<String> dob) {
		this.dob = dob;
	}

	public List<String> getHeight() {
		return height;
	}

	public void setHeight(List<String> height) {
		this.height = height;
	}

	public List<String> getEyeColor() {
		return eyeColor;
	}

	public void setEyeColor(List<String> eyeColor) {
		this.eyeColor = eyeColor;
	}

	public List<String> getHairColor() {
		return hairColor;
	}

	public void setHairColor(List<String> hairColor) {
		this.hairColor = hairColor;
	}

	public List<String> getBloodType() {
		return bloodType;
	}

	public void setBloodType(List<String> bloodType) {
		this.bloodType = bloodType;
	}

	public List<String> getPersonalCode() {
		return personalCode;
	}

	public void setPersonalCode(List<String> personalCode) {
		this.personalCode = personalCode;
	}

	public List<String> getMothersMaidenName() {
		return mothersMaidenName;
	}

	public void setMothersMaidenName(List<String> mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}

	public List<String> getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(List<String> countryCode) {
		this.countryCode = countryCode;
	}

	public List<String> getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(List<String> publicKey) {
		this.publicKey = publicKey;
	}

	public List<String> getPushToken() {
		return pushToken;
	}

	public void setPushToken(List<String> pushToken) {
		this.pushToken = pushToken;
	}

	public List<String> getCurp() {
		return curp;
	}

	public void setCurp(List<String> curp) {
		this.curp = curp;
	}

	public List<String> getElectorKey() {
		return electorKey;
	}

	public void setElectorKey(List<String> electorKey) {
		this.electorKey = electorKey;
	}

	public List<String> getYearOfRegistration() {
		return yearOfRegistration;
	}

	public void setYearOfRegistration(List<String> yearOfRegistration) {
		this.yearOfRegistration = yearOfRegistration;
	}

}
