package com.meetidentity.usermanagement.keycloak.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jboss.logging.Logger;

public class WebAuthnPolicy implements Serializable {

    protected static final Logger logger = Logger.getLogger(WebAuthnPolicy.class);
    // required
    protected String rpEntityName;
    protected List<String> signatureAlgorithms;
    // optional
    protected String rpId;
    protected String attestationConveyancePreference;
    protected String authenticatorAttachment;
    protected String requireResidentKey;
    protected String userVerificationRequirement;
    protected int createTimeout = 0; // not specified as option
    protected boolean avoidSameAuthenticatorRegister = false;
    protected List<String> acceptableAaguids;

    public WebAuthnPolicy() {
    }

    public WebAuthnPolicy(List<String> signatureAlgorithms) {
        this.signatureAlgorithms = signatureAlgorithms;
    }

    // TODO : must be thread safe list
    public static WebAuthnPolicy DEFAULT_POLICY = new WebAuthnPolicy(new ArrayList<>(Arrays.asList("ES256")));

    public String getRpEntityName() {
        return rpEntityName;
    }

    public void setRpEntityName(String rpEntityName) {
        this.rpEntityName = rpEntityName;
    }

    public List<String> getSignatureAlgorithm() {
        return signatureAlgorithms;
    }

    public void setSignatureAlgorithm(List<String> signatureAlgorithms) {
        this.signatureAlgorithms = signatureAlgorithms;
    }

    public String getRpId() {
        return rpId;
    }

    public void setRpId(String rpId) {
        this.rpId = rpId;
    }

    public String getAttestationConveyancePreference() {
        return attestationConveyancePreference;
    }

    public void setAttestationConveyancePreference(String attestationConveyancePreference) {
        this.attestationConveyancePreference = attestationConveyancePreference;
    }

    public String getAuthenticatorAttachment() {
        return authenticatorAttachment;
    }

    public void setAuthenticatorAttachment(String authenticatorAttachment) {
        this.authenticatorAttachment = authenticatorAttachment;
    }

    public String getRequireResidentKey() {
        return requireResidentKey;
    }

    public void setRequireResidentKey(String requireResidentKey) {
        this.requireResidentKey = requireResidentKey;
    }

    public String getUserVerificationRequirement() {
        return userVerificationRequirement;
    }

    public void setUserVerificationRequirement(String userVerificationRequirement) {
        this.userVerificationRequirement = userVerificationRequirement;
    }

    public int getCreateTimeout() {
        return createTimeout;
    }

    public void setCreateTimeout(int createTimeout) {
        this.createTimeout = createTimeout;
    }

    public boolean isAvoidSameAuthenticatorRegister() {
        return avoidSameAuthenticatorRegister;
    }

    public void setAvoidSameAuthenticatorRegister(boolean avoidSameAuthenticatorRegister) {
        this.avoidSameAuthenticatorRegister = avoidSameAuthenticatorRegister;
    }

    public List<String> getAcceptableAaguids() {
        return acceptableAaguids;
    }

    public void setAcceptableAaguids(List<String> acceptableAaguids) {
        this.acceptableAaguids = acceptableAaguids;
    }
}
