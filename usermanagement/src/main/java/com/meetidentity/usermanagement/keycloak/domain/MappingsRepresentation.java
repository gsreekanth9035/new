package com.meetidentity.usermanagement.keycloak.domain;

public class MappingsRepresentation {
	private ClientMappings clientMappings;

	public ClientMappings getClientMappings() {
		return clientMappings;
	}

	public void setClientMappings(ClientMappings clientMappings) {
		this.clientMappings = clientMappings;
	}
}
