package com.meetidentity.usermanagement.keycloak.domain;

import java.util.List;

public class UserRepresentation {

	private String id;
	private String firstName;
	private String lastName;
	private String email;
	private boolean enabled;
	private String federationLink;
	private KeycloakUserAttributes attributes;
	private List<String> requiredActions;
	

	private String username;
	private List<CredentialRepresentation> credentials;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<CredentialRepresentation> getCredentials() {
		return credentials;
	}

	public void setCredentials(List<CredentialRepresentation> credentials) {
		this.credentials = credentials;
	}

	public String getFederationLink() {
		return federationLink;
	}

	public void setFederationLink(String federationLink) {
		this.federationLink = federationLink;
	}

	public KeycloakUserAttributes getAttributes() {
		return attributes;
	}

	public void setAttributes(KeycloakUserAttributes attributes) {
		this.attributes = attributes;
	}

	public List<String> getRequiredActions() {
		return requiredActions;
	}

	public void setRequiredActions(List<String> requiredActions) {
		this.requiredActions = requiredActions;
	}
	

}
