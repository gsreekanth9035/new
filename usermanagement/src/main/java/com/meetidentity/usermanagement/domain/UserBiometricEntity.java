package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="user_biometric")
public class UserBiometricEntity extends AbstractAuditingEntity {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
    @Column(name="type")
	private String type;
    
    @Column(name="position")
	private String position;
    
    @Column(name="format")
	private String format;

    @Lob
    @Column(name="data")
	private byte[] data;

    @Lob
    @Column(name="wsq_data")
	private byte[] wsqData;
    
    @Version
    @Column(name="version")
    private int version;
    
    @Column(name="ansi")
    private String ansi;
    
    @Column(name="image_quality")
    private int imageQuality;
    
    @ManyToOne
    @JoinColumn(name="user_id")
    private UserEntity user;
    
    @OneToMany(mappedBy="userBiometric", cascade=CascadeType.PERSIST, orphanRemoval=true)
    private List<UserBiometricAttributeEntity> userBiometricAttributes = new ArrayList<>();    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}	

	public String getAnsi() {
		return ansi;
	}

	public void setAnsi(String ansi) {
		this.ansi = ansi;
	}

	public int getImageQuality() {
		return imageQuality;
	}

	public void setImageQuality(int imageQuality) {
		this.imageQuality = imageQuality;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}
	
	public List<UserBiometricAttributeEntity> getUserBiometricAttributes() {
		return userBiometricAttributes;
	}
	
	public void addUserBiometricAttribute(UserBiometricAttributeEntity userBiometricAttribute) {
		this.userBiometricAttributes.add(userBiometricAttribute);
		userBiometricAttribute.setUserBiometric(this);
	}
	
	public void removeUserAttribute(UserBiometricAttributeEntity userBiometricAttribute) {
		this.userBiometricAttributes.remove(userBiometricAttribute);
		userBiometricAttribute.setUserBiometric(null);
	}	

	public byte[] getWsqData() {
		return wsqData;
	}

	public void setWsqData(byte[] wsqData) {
		this.wsqData = wsqData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserBiometricEntity other = (UserBiometricEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

    
}
