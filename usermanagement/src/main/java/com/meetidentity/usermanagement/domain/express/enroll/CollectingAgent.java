
package com.meetidentity.usermanagement.domain.express.enroll;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "$type",
    "x500DistinguishedName",
    "EmailAddress",
    "PrintedName",
    "CardUUID",
    "CardHolderUUID",
    "CHUID",
    "CertificateSN",
    "x509CertificateThumbprint",
    "X509Certificate",
    "Role",
    "Classification",
    "DataElementType",
    "PolicyClass",
    "Id",
    "CollectedOn",
    "LastModified",
    "CollectionDevice"
})
public class CollectingAgent {

    @JsonProperty("$type")
    private String $type;
    @JsonProperty("x500DistinguishedName")
    private String x500DistinguishedName;
    @JsonProperty("EmailAddress")
    private String emailAddress;
    @JsonProperty("PrintedName")
    private String printedName;
    @JsonProperty("CardUUID")
    private String cardUUID;
    @JsonProperty("CardHolderUUID")
    private String cardHolderUUID;
    @JsonProperty("CHUID")
    private String cHUID;
    @JsonProperty("CertificateSN")
    private String certificateSN;
    @JsonProperty("x509CertificateThumbprint")
    private String x509CertificateThumbprint;
    @JsonProperty("X509Certificate")
    private Object x509Certificate;
    @JsonProperty("Role")
    private String role;
    @JsonProperty("Classification")
    private String classification;
    @JsonProperty("DataElementType")
    private String dataElementType;
    @JsonProperty("PolicyClass")
    private String policyClass;
    @JsonProperty("Id")
    private String id;
    @JsonProperty("CollectedOn")
    private String collectedOn;
    @JsonProperty("LastModified")
    private String lastModified;
    @JsonProperty("CollectionDevice")
    private String collectionDevice;
    @JsonIgnore
    private Map<String, String> additionalProperties = new HashMap<String, String>();

    @JsonProperty("$type")
    public String get$type() {
        return $type;
    }

    @JsonProperty("$type")
    public void set$type(String $type) {
        this.$type = $type;
    }

    @JsonProperty("x500DistinguishedName")
    public String getX500DistinguishedName() {
        return x500DistinguishedName;
    }

    @JsonProperty("x500DistinguishedName")
    public void setX500DistinguishedName(String x500DistinguishedName) {
        this.x500DistinguishedName = x500DistinguishedName;
    }

    @JsonProperty("EmailAddress")
    public String getEmailAddress() {
        return emailAddress;
    }

    @JsonProperty("EmailAddress")
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @JsonProperty("PrintedName")
    public String getPrintedName() {
        return printedName;
    }

    @JsonProperty("PrintedName")
    public void setPrintedName(String printedName) {
        this.printedName = printedName;
    }

    @JsonProperty("CardUUID")
    public String getCardUUID() {
        return cardUUID;
    }

    @JsonProperty("CardUUID")
    public void setCardUUID(String cardUUID) {
        this.cardUUID = cardUUID;
    }

    @JsonProperty("CardHolderUUID")
    public String getCardHolderUUID() {
        return cardHolderUUID;
    }

    @JsonProperty("CardHolderUUID")
    public void setCardHolderUUID(String cardHolderUUID) {
        this.cardHolderUUID = cardHolderUUID;
    }

    @JsonProperty("CHUID")
    public String getCHUID() {
        return cHUID;
    }

    @JsonProperty("CHUID")
    public void setCHUID(String cHUID) {
        this.cHUID = cHUID;
    }

    @JsonProperty("CertificateSN")
    public String getCertificateSN() {
        return certificateSN;
    }

    @JsonProperty("CertificateSN")
    public void setCertificateSN(String certificateSN) {
        this.certificateSN = certificateSN;
    }

    @JsonProperty("x509CertificateThumbprint")
    public String getX509CertificateThumbprint() {
        return x509CertificateThumbprint;
    }

    @JsonProperty("x509CertificateThumbprint")
    public void setX509CertificateThumbprint(String x509CertificateThumbprint) {
        this.x509CertificateThumbprint = x509CertificateThumbprint;
    }

    @JsonProperty("X509Certificate")
    public Object getX509Certificate() {
        return x509Certificate;
    }

    @JsonProperty("X509Certificate")
    public void setX509Certificate(Object x509Certificate) {
        this.x509Certificate = x509Certificate;
    }

    @JsonProperty("Role")
    public String getRole() {
        return role;
    }

    @JsonProperty("Role")
    public void setRole(String role) {
        this.role = role;
    }

    @JsonProperty("Classification")
    public String getClassification() {
        return classification;
    }

    @JsonProperty("Classification")
    public void setClassification(String classification) {
        this.classification = classification;
    }

    @JsonProperty("DataElementType")
    public String getDataElementType() {
        return dataElementType;
    }

    @JsonProperty("DataElementType")
    public void setDataElementType(String dataElementType) {
        this.dataElementType = dataElementType;
    }

    @JsonProperty("PolicyClass")
    public String getPolicyClass() {
        return policyClass;
    }

    @JsonProperty("PolicyClass")
    public void setPolicyClass(String policyClass) {
        this.policyClass = policyClass;
    }

    @JsonProperty("Id")
    public String getId() {
        return id;
    }

    @JsonProperty("Id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("CollectedOn")
    public String getCollectedOn() {
        return collectedOn;
    }

    @JsonProperty("CollectedOn")
    public void setCollectedOn(String collectedOn) {
        this.collectedOn = collectedOn;
    }

    @JsonProperty("LastModified")
    public String getLastModified() {
        return lastModified;
    }

    @JsonProperty("LastModified")
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    @JsonProperty("CollectionDevice")
    public String getCollectionDevice() {
        return collectionDevice;
    }

    @JsonProperty("CollectionDevice")
    public void setCollectionDevice(String collectionDevice) {
        this.collectionDevice = collectionDevice;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, String> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, String value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append($type).append(x500DistinguishedName).append(emailAddress).append(printedName).append(cardUUID).append(cardHolderUUID).append(cHUID).append(certificateSN).append(x509CertificateThumbprint).append(x509Certificate).append(role).append(classification).append(dataElementType).append(policyClass).append(id).append(collectedOn).append(lastModified).append(collectionDevice).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CollectingAgent) == false) {
            return false;
        }
        CollectingAgent rhs = ((CollectingAgent) other);
        return new EqualsBuilder().append($type, rhs.$type).append(x500DistinguishedName, rhs.x500DistinguishedName).append(emailAddress, rhs.emailAddress).append(printedName, rhs.printedName).append(cardUUID, rhs.cardUUID).append(cardHolderUUID, rhs.cardHolderUUID).append(cHUID, rhs.cHUID).append(certificateSN, rhs.certificateSN).append(x509CertificateThumbprint, rhs.x509CertificateThumbprint).append(x509Certificate, rhs.x509Certificate).append(role, rhs.role).append(classification, rhs.classification).append(dataElementType, rhs.dataElementType).append(policyClass, rhs.policyClass).append(id, rhs.id).append(collectedOn, rhs.collectedOn).append(lastModified, rhs.lastModified).append(collectionDevice, rhs.collectionDevice).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
