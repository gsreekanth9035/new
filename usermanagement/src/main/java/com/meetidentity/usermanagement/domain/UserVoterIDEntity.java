package com.meetidentity.usermanagement.domain;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.meetidentity.usermanagement.annotations.SequenceValue;

@Entity
@Table(name = "user_voter_id")
public class UserVoterIDEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "uims_id", nullable = false)
	private String uimsId;

	@Column(name = "date_of_issuance")
	private Instant dateOfIssuance;

	@Column(name = "date_of_expiry")
	private Instant dateOfExpiry;

	@SequenceValue
	@Column(name = "curp")
	private String curp;

	@Column(name = "mrz_td1_line1")
	private String mrzTd1Line1;

	@Column(name = "mrz_td1_line2")
	private String mrzTd1Line2;

	@Column(name = "mrz_td1_line3")
	private String mrzTd1Line3;

	@Column(name = "registration_year")
	private String registrationYear;

	@Column(name = "elector_key")
	private String electorKey;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public Instant getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Instant instant) {
		this.dateOfIssuance = instant;
	}

	public Instant getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Instant dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getMrzTd1Line1() {
		return mrzTd1Line1;
	}

	public void setMrzTd1Line1(String mrzTd1Line1) {
		this.mrzTd1Line1 = mrzTd1Line1;
	}

	public String getMrzTd1Line2() {
		return mrzTd1Line2;
	}

	public void setMrzTd1Line2(String mrzTd1Line2) {
		this.mrzTd1Line2 = mrzTd1Line2;
	}

	public String getMrzTd1Line3() {
		return mrzTd1Line3;
	}

	public void setMrzTd1Line3(String mrzTd1Line3) {
		this.mrzTd1Line3 = mrzTd1Line3;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getRegistrationYear() {
		return registrationYear;
	}

	public void setRegistrationYear(String registrationYear) {
		this.registrationYear = registrationYear;
	}

	public String getElectorKey() {
		return electorKey;
	}

	public void setElectorKey(String electorKey) {
		this.electorKey = electorKey;
	}

}
