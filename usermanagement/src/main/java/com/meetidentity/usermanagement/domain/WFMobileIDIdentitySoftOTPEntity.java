package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "wf_mobile_id_identity_soft_otp_issuance")
public class WFMobileIDIdentitySoftOTPEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "soft_otp_type")
	private String softOtpType;

	@Column(name = "secret_key")
	private String secretKey;

	@Column(name = "algorithm")
	private String algorithm;

	@Column(name = "digits")
	private int digits;

	@Column(name = "period")
	private int period;

	@Column(name = "counter")
	private long counter;	

	@Column(name = "status")
	private String status;

	@OneToOne
	@JoinColumn(name = "wf_mobile_identity_issuance_id")
	private WFMobileIDIdentityIssuanceEntity wfMobileIDIdentityIssuance;
	
	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSoftOtpType() {
		return softOtpType;
	}

	public void setSoftOtpType(String softOtpType) {
		this.softOtpType = softOtpType;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public int getDigits() {
		return digits;
	}

	public void setDigits(int digits) {
		this.digits = digits;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public long getCounter() {
		return counter;
	}

	public void setCounter(long counter) {
		this.counter = counter;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public WFMobileIDIdentityIssuanceEntity getWfMobileIDIdentityIssuance() {
		return wfMobileIDIdentityIssuance;
	}

	public void setWfMobileIDIdentityIssuance(WFMobileIDIdentityIssuanceEntity wfMobileIDIdentityIssuance) {
		this.wfMobileIDIdentityIssuance = wfMobileIDIdentityIssuance;
	}

	
	
}
