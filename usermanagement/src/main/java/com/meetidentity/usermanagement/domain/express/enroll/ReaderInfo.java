
package com.meetidentity.usermanagement.domain.express.enroll;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "$type",
    "DeviceName",
    "DeviceClass",
    "DeviceVersion",
    "SerialNumber",
    "Calibrated",
    "CalibrationDate"
})
public class ReaderInfo {

    @JsonProperty("$type")
    private String $type;
    @JsonProperty("DeviceName")
    private String deviceName;
    @JsonProperty("DeviceClass")
    private Object deviceClass;
    @JsonProperty("DeviceVersion")
    private Object deviceVersion;
    @JsonProperty("SerialNumber")
    private String serialNumber;
    @JsonProperty("Calibrated")
    private Object calibrated;
    @JsonProperty("CalibrationDate")
    private Object calibrationDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("$type")
    public String get$type() {
        return $type;
    }

    @JsonProperty("$type")
    public void set$type(String $type) {
        this.$type = $type;
    }

    @JsonProperty("DeviceName")
    public String getDeviceName() {
        return deviceName;
    }

    @JsonProperty("DeviceName")
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @JsonProperty("DeviceClass")
    public Object getDeviceClass() {
        return deviceClass;
    }

    @JsonProperty("DeviceClass")
    public void setDeviceClass(Object deviceClass) {
        this.deviceClass = deviceClass;
    }

    @JsonProperty("DeviceVersion")
    public Object getDeviceVersion() {
        return deviceVersion;
    }

    @JsonProperty("DeviceVersion")
    public void setDeviceVersion(Object deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    @JsonProperty("SerialNumber")
    public String getSerialNumber() {
        return serialNumber;
    }

    @JsonProperty("SerialNumber")
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @JsonProperty("Calibrated")
    public Object getCalibrated() {
        return calibrated;
    }

    @JsonProperty("Calibrated")
    public void setCalibrated(Object calibrated) {
        this.calibrated = calibrated;
    }

    @JsonProperty("CalibrationDate")
    public Object getCalibrationDate() {
        return calibrationDate;
    }

    @JsonProperty("CalibrationDate")
    public void setCalibrationDate(Object calibrationDate) {
        this.calibrationDate = calibrationDate;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append($type).append(deviceName).append(deviceClass).append(deviceVersion).append(serialNumber).append(calibrated).append(calibrationDate).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ReaderInfo) == false) {
            return false;
        }
        ReaderInfo rhs = ((ReaderInfo) other);
        return new EqualsBuilder().append($type, rhs.$type).append(deviceName, rhs.deviceName).append(deviceClass, rhs.deviceClass).append(deviceVersion, rhs.deviceVersion).append(serialNumber, rhs.serialNumber).append(calibrated, rhs.calibrated).append(calibrationDate, rhs.calibrationDate).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
