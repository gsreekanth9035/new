package com.meetidentity.usermanagement.domain.visual.credentials.sid;

import java.io.Serializable;

public class UserSIDback implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String Zone0BS_Background;
	private String Zone1BS_Org_CardSN;
	private String Zone2BS_IssuerIN;
	private String Zone4BS_ReturnAddress;
	private String Zone6BS_Ainfo;
	private String Zone1BV_Org_CardSN;
	private String Zone2BV_IssuerIN;
	private String Zone6BV_Qrcode;
	public String getZone0BS_Background() {
		return Zone0BS_Background;
	}
	public void setZone0BS_Background(String zone0bs_Background) {
		Zone0BS_Background = zone0bs_Background;
	}
	public String getZone1BS_Org_CardSN() {
		return Zone1BS_Org_CardSN;
	}
	public void setZone1BS_Org_CardSN(String zone1bs_Org_CardSN) {
		Zone1BS_Org_CardSN = zone1bs_Org_CardSN;
	}
	public String getZone2BS_IssuerIN() {
		return Zone2BS_IssuerIN;
	}
	public void setZone2BS_IssuerIN(String zone2bs_IssuerIN) {
		Zone2BS_IssuerIN = zone2bs_IssuerIN;
	}
	public String getZone4BS_ReturnAddress() {
		return Zone4BS_ReturnAddress;
	}
	public void setZone4BS_ReturnAddress(String zone4bs_ReturnAddress) {
		Zone4BS_ReturnAddress = zone4bs_ReturnAddress;
	}
	public String getZone6BS_Ainfo() {
		return Zone6BS_Ainfo;
	}
	public void setZone6BS_Ainfo(String zone6bs_Ainfo) {
		Zone6BS_Ainfo = zone6bs_Ainfo;
	}
	public String getZone1BV_Org_CardSN() {
		return Zone1BV_Org_CardSN;
	}
	public void setZone1BV_Org_CardSN(String zone1bv_Org_CardSN) {
		Zone1BV_Org_CardSN = zone1bv_Org_CardSN;
	}
	public String getZone2BV_IssuerIN() {
		return Zone2BV_IssuerIN;
	}
	public void setZone2BV_IssuerIN(String zone2bv_IssuerIN) {
		Zone2BV_IssuerIN = zone2bv_IssuerIN;
	}
	public String getZone6BV_Qrcode() {
		return Zone6BV_Qrcode;
	}
	public void setZone6BV_Qrcode(String zone6bv_Qrcode) {
		Zone6BV_Qrcode = zone6bv_Qrcode;
	}

	
}
