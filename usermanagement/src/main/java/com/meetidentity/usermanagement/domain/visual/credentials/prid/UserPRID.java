package com.meetidentity.usermanagement.domain.visual.credentials.prid;

import java.io.Serializable;

public class UserPRID implements Serializable {

	private static final long serialVersionUID = 1L;

	private UserPRIDBack userPRCBack;

	private UserPRIDFront userPRCFront;

	public void setUserPRCBack(UserPRIDBack userPRCBack) {
		this.userPRCBack = userPRCBack;
	}

	public UserPRIDBack getUserPRCBack() {
		return this.userPRCBack;
	}

	public void setUserPRCFront(UserPRIDFront userPRCFront) {
		this.userPRCFront = userPRCFront;
	}

	public UserPRIDFront getUserPRCFront() {
		return this.userPRCFront;
	}

}
