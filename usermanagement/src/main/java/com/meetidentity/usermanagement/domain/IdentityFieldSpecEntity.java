package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="identity_field_spec")
public class IdentityFieldSpecEntity  extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
    @Column(name="format")
	private String format;
    
    @Column(name="length_min")
	private int lengthMin;

    @Column(name="length_max")
	private int lengthMax;

    @Column(name="length_type")
	private String lengthType;

    @Column(name="font_size_min")
	private int fontSizeMin;

    @Column(name="font_size_max")
	private int fontSizeMax;

    @Column(name="font_size_type")
	private String fontSizeType;

    @Column(name="font_weight")
	private String fontWeight;

    @Column(name="font_type")
	private String fontType;

    @Column(name="full_spec")
	private String fullSpec;

    @Column(name="usage")
	private String usage;
    
    @Version
    @Column(name="version")
    private int version;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="identity_field_id")
    private IdentityFieldEntity identityField;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public int getLengthMin() {
		return lengthMin;
	}

	public void setLengthMin(int lengthMin) {
		this.lengthMin = lengthMin;
	}

	public int getLengthMax() {
		return lengthMax;
	}

	public void setLengthMax(int lengthMax) {
		this.lengthMax = lengthMax;
	}

	public String getLengthType() {
		return lengthType;
	}

	public void setLengthType(String lengthType) {
		this.lengthType = lengthType;
	}

	public int getFontSizeMin() {
		return fontSizeMin;
	}

	public void setFontSizeMin(int fontSizeMin) {
		this.fontSizeMin = fontSizeMin;
	}

	public int getFontSizeMax() {
		return fontSizeMax;
	}

	public void setFontSizeMax(int fontSizeMax) {
		this.fontSizeMax = fontSizeMax;
	}

	public String getFontSizeType() {
		return fontSizeType;
	}

	public void setFontSizeType(String fontSizeType) {
		this.fontSizeType = fontSizeType;
	}

	public String getFontWeight() {
		return fontWeight;
	}

	public void setFontWeight(String fontWeight) {
		this.fontWeight = fontWeight;
	}

	public String getFontType() {
		return fontType;
	}

	public void setFontType(String fontType) {
		this.fontType = fontType;
	}

	public String getFullSpec() {
		return fullSpec;
	}

	public void setFullSpec(String fullSpec) {
		this.fullSpec = fullSpec;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public IdentityFieldEntity getIdentityField() {
		return identityField;
	}

	public void setIdentityField(IdentityFieldEntity identityField) {
		this.identityField = identityField;
	}
       
}
