package com.meetidentity.usermanagement.domain.visual.credentials.dl;

import java.io.Serializable;

public class UserDLBack implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String Zone0_BS_Background;
	private String Zone5_BV_Photoghost;
	private String Zone2_8BS_CHAddress_Alt;
	private String Zone4_9BS_VClass_Alt;
	private String Zone4_5BS_DD_Alt;
	private String Zone4_9aBS_End_Alt;
	private String Zone4_18BS_Eyes_Alt;
	private String Zone4_16BS_Height_Alt;
	private String Zone4_12BS_Restrictions_Alt;

	private String Zone2_8BS_CHAddress_Eng;
	private String Zone4_18BS_Eyes_Eng;
	private String Zone4_9aBS_End_Eng;
	private String Zone4_16BS_Height_Eng;
	private String Zone4_12BS_Restrictions_Eng;
	private String Zone4_9BS_VClass_Eng;
	private String Zone4_5BS_DD_Eng;


	private String Zone2_8BV_CHAddress_1;
	private String Zone2_8BV_CHAddress_2;
	private String Zone4_5BV_DD;
	private String Zone4_9aBV_End;
	private String Zone4_18BV_Eyes;
	private String Zone4_16BV_Height;
	private String Zone4_12BV_Restrictions;
	private String Zone4_9BV_VClass;
	private String Zone4_5BV_QR;

	private String Zone5_1BV_ICN;

	public String getZone0_BS_Background() {
		return Zone0_BS_Background;
	}

	public void setZone0_BS_Background(String zone0_BS_Background) {
		Zone0_BS_Background = zone0_BS_Background;
	}

	public String getZone5_BV_Photoghost() {
		return Zone5_BV_Photoghost;
	}

	public void setZone5_BV_Photoghost(String zone5_BV_Photoghost) {
		Zone5_BV_Photoghost = zone5_BV_Photoghost;
	}

	public String getZone2_8BS_CHAddress_Alt() {
		return Zone2_8BS_CHAddress_Alt;
	}

	public void setZone2_8BS_CHAddress_Alt(String zone2_8bs_CHAddress_Alt) {
		Zone2_8BS_CHAddress_Alt = zone2_8bs_CHAddress_Alt;
	}

	public String getZone4_9BS_VClass_Alt() {
		return Zone4_9BS_VClass_Alt;
	}

	public void setZone4_9BS_VClass_Alt(String zone4_9bs_VClass_Alt) {
		Zone4_9BS_VClass_Alt = zone4_9bs_VClass_Alt;
	}

	public String getZone4_5BS_DD_Alt() {
		return Zone4_5BS_DD_Alt;
	}

	public void setZone4_5BS_DD_Alt(String zone4_5bs_DD_Alt) {
		Zone4_5BS_DD_Alt = zone4_5bs_DD_Alt;
	}

	public String getZone4_9aBS_End_Alt() {
		return Zone4_9aBS_End_Alt;
	}

	public void setZone4_9aBS_End_Alt(String zone4_9aBS_End_Alt) {
		Zone4_9aBS_End_Alt = zone4_9aBS_End_Alt;
	}

	public String getZone4_18BS_Eyes_Alt() {
		return Zone4_18BS_Eyes_Alt;
	}

	public void setZone4_18BS_Eyes_Alt(String zone4_18bs_Eyes_Alt) {
		Zone4_18BS_Eyes_Alt = zone4_18bs_Eyes_Alt;
	}

	public String getZone4_16BS_Height_Alt() {
		return Zone4_16BS_Height_Alt;
	}

	public void setZone4_16BS_Height_Alt(String zone4_16bs_Height_Alt) {
		Zone4_16BS_Height_Alt = zone4_16bs_Height_Alt;
	}

	public String getZone4_12BS_Restrictions_Alt() {
		return Zone4_12BS_Restrictions_Alt;
	}

	public void setZone4_12BS_Restrictions_Alt(String zone4_12bs_Restrictions_Alt) {
		Zone4_12BS_Restrictions_Alt = zone4_12bs_Restrictions_Alt;
	}

	public String getZone2_8BS_CHAddress_Eng() {
		return Zone2_8BS_CHAddress_Eng;
	}

	public void setZone2_8BS_CHAddress_Eng(String zone2_8bs_CHAddress_Eng) {
		Zone2_8BS_CHAddress_Eng = zone2_8bs_CHAddress_Eng;
	}

	public String getZone4_18BS_Eyes_Eng() {
		return Zone4_18BS_Eyes_Eng;
	}

	public void setZone4_18BS_Eyes_Eng(String zone4_18bs_Eyes_Eng) {
		Zone4_18BS_Eyes_Eng = zone4_18bs_Eyes_Eng;
	}

	public String getZone4_9aBS_End_Eng() {
		return Zone4_9aBS_End_Eng;
	}

	public void setZone4_9aBS_End_Eng(String zone4_9aBS_End_Eng) {
		Zone4_9aBS_End_Eng = zone4_9aBS_End_Eng;
	}

	public String getZone4_16BS_Height_Eng() {
		return Zone4_16BS_Height_Eng;
	}

	public void setZone4_16BS_Height_Eng(String zone4_16bs_Height_Eng) {
		Zone4_16BS_Height_Eng = zone4_16bs_Height_Eng;
	}

	public String getZone4_12BS_Restrictions_Eng() {
		return Zone4_12BS_Restrictions_Eng;
	}

	public void setZone4_12BS_Restrictions_Eng(String zone4_12bs_Restrictions_Eng) {
		Zone4_12BS_Restrictions_Eng = zone4_12bs_Restrictions_Eng;
	}

	public String getZone4_9BS_VClass_Eng() {
		return Zone4_9BS_VClass_Eng;
	}

	public void setZone4_9BS_VClass_Eng(String zone4_9bs_VClass_Eng) {
		Zone4_9BS_VClass_Eng = zone4_9bs_VClass_Eng;
	}

	public String getZone4_5BS_DD_Eng() {
		return Zone4_5BS_DD_Eng;
	}

	public void setZone4_5BS_DD_Eng(String zone4_5bs_DD_Eng) {
		Zone4_5BS_DD_Eng = zone4_5bs_DD_Eng;
	}

	public String getZone2_8BV_CHAddress_1() {
		return Zone2_8BV_CHAddress_1;
	}

	public void setZone2_8BV_CHAddress_1(String zone2_8bv_CHAddress_1) {
		Zone2_8BV_CHAddress_1 = zone2_8bv_CHAddress_1;
	}

	public String getZone2_8BV_CHAddress_2() {
		return Zone2_8BV_CHAddress_2;
	}

	public void setZone2_8BV_CHAddress_2(String zone2_8bv_CHAddress_2) {
		Zone2_8BV_CHAddress_2 = zone2_8bv_CHAddress_2;
	}

	public String getZone4_5BV_DD() {
		return Zone4_5BV_DD;
	}

	public void setZone4_5BV_DD(String zone4_5bv_DD) {
		Zone4_5BV_DD = zone4_5bv_DD;
	}

	public String getZone4_9aBV_End() {
		return Zone4_9aBV_End;
	}

	public void setZone4_9aBV_End(String zone4_9aBV_End) {
		Zone4_9aBV_End = zone4_9aBV_End;
	}

	public String getZone4_18BV_Eyes() {
		return Zone4_18BV_Eyes;
	}

	public void setZone4_18BV_Eyes(String zone4_18bv_Eyes) {
		Zone4_18BV_Eyes = zone4_18bv_Eyes;
	}

	public String getZone4_16BV_Height() {
		return Zone4_16BV_Height;
	}

	public void setZone4_16BV_Height(String zone4_16bv_Height) {
		Zone4_16BV_Height = zone4_16bv_Height;
	}

	public String getZone4_12BV_Restrictions() {
		return Zone4_12BV_Restrictions;
	}

	public void setZone4_12BV_Restrictions(String zone4_12bv_Restrictions) {
		Zone4_12BV_Restrictions = zone4_12bv_Restrictions;
	}

	public String getZone4_9BV_VClass() {
		return Zone4_9BV_VClass;
	}

	public void setZone4_9BV_VClass(String zone4_9bv_VClass) {
		Zone4_9BV_VClass = zone4_9bv_VClass;
	}

	public String getZone4_5BV_QR() {
		return Zone4_5BV_QR;
	}

	public void setZone4_5BV_QR(String zone4_5bv_QR) {
		Zone4_5BV_QR = zone4_5bv_QR;
	}

	public String getZone5_1BV_ICN() {
		return Zone5_1BV_ICN;
	}

	public void setZone5_1BV_ICN(String zone5_1bv_ICN) {
		Zone5_1BV_ICN = zone5_1bv_ICN;
	}
	
	
}
