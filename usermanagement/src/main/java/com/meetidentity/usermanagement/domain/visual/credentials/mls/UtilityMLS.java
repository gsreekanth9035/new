package com.meetidentity.usermanagement.domain.visual.credentials.mls;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.constants.XMLConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.User;

@Component
public class UtilityMLS {
	
	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;
	
	private final Logger log = LoggerFactory.getLogger(UtilityMLS.class);
	
	private static String XLINK_NS = XMLConstants.XLINK_NAMESPACE_URI; 
	private static String XLINK_HREF = "xlink:href"; 
	
	
	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @param string 
	 * @return
	 * @throws Exception 
	 */
	public Document parseMLSFront(SAXSVGDocumentFactory  sAXSVGDocumentFactory,User user,byte[] svgFile, byte[] jsonFile,String credentialTemplateID, String visualCredentialType) throws VisualCredentialException {
		try {
			
			Gson gson = new Gson();
			MLSTemplate mlsTemplate = gson.fromJson(new String(jsonFile), MLSTemplate.class);

			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));	
		    //Static variables 
			 //Text variables 
			log.debug("parse MLS Front: Started");
			log.debug("MLS Front Text Fields: Started");
		    doc.getElementById("TEAM").setTextContent(mlsTemplate.getMlsTemplateFront().getTEAM());
		    doc.getElementById("DATE").setTextContent(mlsTemplate.getMlsTemplateFront().getDATE());
		    doc.getElementById("EVENT").setTextContent(mlsTemplate.getMlsTemplateFront().getEVENT());
		    doc.getElementById("Zone2FV_Last_Name").setTextContent(mlsTemplate.getMlsTemplateFront().getZone2FV_Last_Name());
		    doc.getElementById("Zone8FV_Team").setTextContent(mlsTemplate.getMlsTemplateFront().getZone8FV_Team());
		    doc.getElementById("Zone13FV_Date").setTextContent(mlsTemplate.getMlsTemplateFront().getZone13FV_Date());
		    doc.getElementById("Zone10FV_Event").setTextContent(mlsTemplate.getMlsTemplateFront().getZone10FV_Event());
		    doc.getElementById("Zone13FV_IDesignation").setTextContent(mlsTemplate.getMlsTemplateFront().getZone13FV_IDesignation());
		   
		    log.debug("MLS Front Image Fields: Started");
		    doc.getElementById("Background").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64,"+mlsTemplate.getMlsTemplateFront().getBackground());
		    doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64,"+mlsTemplate.getMlsTemplateFront().getZone1FV_Photograph());
		    doc.getElementById("Zone11FS_AgencySeal_copy").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64,"+mlsTemplate.getMlsTemplateFront().getZone11FS_AgencySeal_copy());
		    doc.getElementById("Zone6FV_QR").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64,"+ Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(user.getOrganisationName()+"#"+user.getId() + "#" + credentialTemplateID, BarcodeFormat.QR_CODE,300, 300))));
		    
		    log.debug("MLS Front Dynamic  Fields: Started");
		    
		    //Dynamic User Variables
		    if (user != null) {
		    	
		    	// FirstName
				doc.getElementById("Zone2FV_First__Middle_Name")
						.setTextContent(user.getFirstName() + " " + ((user.getMiddleName() == null)?"":user.getMiddleName()));
				// Last Name
				doc.getElementById("Zone2FV_Last_Name").setTextContent(((user.getLastName() == null)?"":user.getLastName()));
				
				user.getUserBiometrics().stream().forEach(userBio->{
					//FACE 
					if(userBio.getType().equalsIgnoreCase("Face")) {
						int width = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("width"));
						int height = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("height"));
						try {
							Image image = Util
									.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
							BufferedImage bufferedImage = Util.resizeImage(image, width, height);
							if(user.getIsTransparentPhotoRequired()) {
								bufferedImage = backgroundColorTransparency.generate(bufferedImage, user.getOrganisationName());
							}
							String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
							doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + base64Data);

						} catch (IOException e) {
							doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
						}
					}
				});
			}
		    
		   
		    log.debug("parse MLS Front: Completed");
		    return doc;
	
		
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType);
		}
	}
	
	
	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @return
	 * @throws Exception 
	 */
	public Document parseMLSBack(SAXSVGDocumentFactory  sAXSVGDocumentFactory,User user, String credentialTemplateID, byte[] svgFile, byte[] jsonFile, String visualCredentialType) throws VisualCredentialException {
		try {
			
			Gson gson = new Gson();
		    MLSTemplate mlsTemplate = gson.fromJson(new String(jsonFile), MLSTemplate.class);
			
		    Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));	
		    
			//Static variables 
		    log.debug("parse MLS Back: Started");
			log.debug("MLS Back Text Fields: Started");
		    doc.getElementById("Zone9FS_Header").setTextContent(mlsTemplate.getMlsTemplateBack().getZone9FS_Header());
		    doc.getElementById("Zone5FS_EN").setTextContent(mlsTemplate.getMlsTemplateBack().getZone5FS_EN());
		    doc.getElementById("Zone10FS_Department").setTextContent(mlsTemplate.getMlsTemplateBack().getZone10FS_Department());
		    doc.getElementById("Zone13FS_Issued").setTextContent(mlsTemplate.getMlsTemplateBack().getZone13FS_Issued());
		    doc.getElementById("Zone14FS_Expires").setTextContent(mlsTemplate.getMlsTemplateBack().getZone14FS_Expires());
		    doc.getElementById("Zone_6BS_Ainfo_L1").setTextContent(mlsTemplate.getMlsTemplateBack().getZone_6BS_Ainfo_L1());
		    doc.getElementById("Zone_6BS_Ainfo_L2").setTextContent(mlsTemplate.getMlsTemplateBack().getZone_6BS_Ainfo_L2());
		    doc.getElementById("Zone_6BS_Ainfo_L3").setTextContent(mlsTemplate.getMlsTemplateBack().getZone_6BS_Ainfo_L3());
		    doc.getElementById("Zone_6BS_Ainfo_L4").setTextContent(mlsTemplate.getMlsTemplateBack().getZone_6BS_Ainfo_L4());
		    doc.getElementById("Zone4BS_ReturnAddress_L1").setTextContent(mlsTemplate.getMlsTemplateBack().getZone4BS_ReturnAddress_L1());
		    doc.getElementById("Zone4BS_ReturnAddress_L2").setTextContent(mlsTemplate.getMlsTemplateBack().getZone4BS_ReturnAddress_L2());
		    doc.getElementById("Zone4BS_ReturnAddress_L3").setTextContent(mlsTemplate.getMlsTemplateBack().getZone4BS_ReturnAddress_L3());
		    doc.getElementById("Zone2BS_IssuerIN").setTextContent(mlsTemplate.getMlsTemplateBack().getZone2BS_IssuerIN());
		    doc.getElementById("Zone1BS_Agency_CardSN").setTextContent(mlsTemplate.getMlsTemplateBack().getZone1BS_Agency_CardSN());
		    
		    doc.getElementById("Zone14FV_Expires").setTextContent(mlsTemplate.getMlsTemplateBack().getZone14FV_Expires());
		    doc.getElementById("Zone5FV__EN").setTextContent(mlsTemplate.getMlsTemplateBack().getZone5FV__EN());
		    doc.getElementById("Zone8FV_Affiliation").setTextContent(mlsTemplate.getMlsTemplateBack().getZone8FV_Affiliation());
		    doc.getElementById("Zone10FV_Department").setTextContent(mlsTemplate.getMlsTemplateBack().getZone10FV_Department());
		    doc.getElementById("Zone2BV_IssuerIN").setTextContent(mlsTemplate.getMlsTemplateBack().getZone2BV_IssuerIN());
		    doc.getElementById("Zone1BV_Agency_CardSN").setTextContent(mlsTemplate.getMlsTemplateBack().getZone1BV_Agency_CardSN());
		    
		    log.debug("MLS Back Image Fields: Started");
		    doc.getElementById("Zone11FS_OrgSeal").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64,"+ Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(user.getOrganisationName()+"#"+user.getId() + "#" + credentialTemplateID, BarcodeFormat.QR_CODE,300, 300))));
		    doc.getElementById("Zone6FV_Qrcode").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64,"+ Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(user.getOrganisationName()+"#"+user.getId() + "#" + credentialTemplateID, BarcodeFormat.QR_CODE,300, 300))));
		    
		    log.debug("MLS Back Dynamic  Fields: Started");
		    user.getUserBiometrics().stream().forEach(userBio->{
		    	
		    	// FirstName
				doc.getElementById("Zone2FV_First__Middle_Name").setTextContent(user.getFirstName() + " " + user.getMiddleName());
				// Last Name
				doc.getElementById("Zone2FV_Last_Name").setTextContent(user.getLastName());
				
		    	
		    	
				//FACE 
				if(userBio.getType().equalsIgnoreCase("Face")) {
					if(userBio.getData() != null) {
						int width = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("width"));
						int height = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("height"));
						try {
							Image image = Util
									.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
							BufferedImage bufferedImage = Util.resizeImage(image, width, height);
							if(user.getIsTransparentPhotoRequired()) {
								bufferedImage = backgroundColorTransparency.generate(bufferedImage, user.getOrganisationName());
							}
							String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
							doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + base64Data);

						} catch (IOException e) {
							doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
						}
					}
					  
				}
			});
		    
		    log.debug("parse MLS Back: Completed");
		   
		    return doc;
		
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType);
		}
	}
	
}
