package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "device_status")
public class DeviceStatusEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@OneToMany(mappedBy = "deviceStatus", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<DeviceActionStatusRelationEntity> deviceActionsByStatus = new ArrayList<>();

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<DeviceActionStatusRelationEntity> getDeviceActionsByStatus() {
		return deviceActionsByStatus;
	}

	public void setDeviceActionsByStatus(List<DeviceActionStatusRelationEntity> deviceActionsByStatus) {
		this.deviceActionsByStatus = deviceActionsByStatus;
	}

}
