package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "org_pair_mobile_device_config")
public class PairMobileDeviceConfigEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "google_play_store_app_url")
	private String googlePlayStoreAppUrl;

	@Column(name = "apple_app_store_app_url")
	private String appleAppStoreAppUrl;

	@Column(name = "pair_mobile_device_secret_key_length")
	private int pairMobileDeviceSecretKeyLength;

	@Column(name = "pair_mobile_device_url")
	private String pairMobileDeviceUrl;

	@Column(name = "qr_code_width")
	private int qrCodeWidth;

	@Column(name = "qr_code_height")
	private int qrCodeHeight;

	@Column(name = "qr_code_expiry_in_mins")
	private int qrCodeExpiryInMins;
	
	@Column(name = "disable_qr_code_expiry")
	private boolean disableQRCodeExpiry;
	

	@Column(name = "enable_demo_video")
	private boolean enableDemoVideo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
	private OrganizationEntity organization;

	@Column(name = "aes_enccipher_algorithm")
	private String aesEnccipherAlgorithm;
	
	@Column(name = "status")
	private boolean status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

	public String getGooglePlayStoreAppUrl() {
		return googlePlayStoreAppUrl;
	}

	public void setGooglePlayStoreAppUrl(String googlePlayStoreAppUrl) {
		this.googlePlayStoreAppUrl = googlePlayStoreAppUrl;
	}

	public String getAppleAppStoreAppUrl() {
		return appleAppStoreAppUrl;
	}

	public void setAppleAppStoreAppUrl(String appleAppStoreAppUrl) {
		this.appleAppStoreAppUrl = appleAppStoreAppUrl;
	}

	public int getPairMobileDeviceSecretKeyLength() {
		return pairMobileDeviceSecretKeyLength;
	}

	public void setPairMobileDeviceSecretKeyLength(int pairMobileDeviceSecretKeyLength) {
		this.pairMobileDeviceSecretKeyLength = pairMobileDeviceSecretKeyLength;
	}

	public String getPairMobileDeviceUrl() {
		return pairMobileDeviceUrl;
	}

	public void setPairMobileDeviceUrl(String pairMobileDeviceUrl) {
		this.pairMobileDeviceUrl = pairMobileDeviceUrl;
	}

	public int getQrCodeWidth() {
		return qrCodeWidth;
	}

	public void setQrCodeWidth(int qrCodeWidth) {
		this.qrCodeWidth = qrCodeWidth;
	}

	public int getQrCodeHeight() {
		return qrCodeHeight;
	}

	public void setQrCodeHeight(int qrCodeHeight) {
		this.qrCodeHeight = qrCodeHeight;
	}

	public int getQrCodeExpiryInMins() {
		return qrCodeExpiryInMins;
	}

	public void setQrCodeExpiryInMins(int qrCodeExpiryInMins) {
		this.qrCodeExpiryInMins = qrCodeExpiryInMins;
	}

	public boolean isDisableQRCodeExpiry() {
		return disableQRCodeExpiry;
	}

	public void setDisableQRCodeExpiry(boolean disableQRCodeExpiry) {
		this.disableQRCodeExpiry = disableQRCodeExpiry;
	}

	public boolean isEnableDemoVideo() {
		return enableDemoVideo;
	}

	public void setEnableDemoVideo(boolean enableDemoVideo) {
		this.enableDemoVideo = enableDemoVideo;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getAesEnccipherAlgorithm() {
		return aesEnccipherAlgorithm;
	}

	public void setAesEnccipherAlgorithm(String aesEnccipherAlgorithm) {
		this.aesEnccipherAlgorithm = aesEnccipherAlgorithm;
	}

	
}
