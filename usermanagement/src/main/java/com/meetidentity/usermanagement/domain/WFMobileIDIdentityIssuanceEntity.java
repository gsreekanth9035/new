package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "wf_mobile_id_identity_issuance")
public class WFMobileIDIdentityIssuanceEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "friendly_name")
	private String friendlyName;

	@Column(name = "push_verify")
	private Boolean pushVerify;

	@Column(name = "soft_otp")
	private Boolean softOTP;
	
	@Column(name = "fido2")
	private Boolean fido2;
	
	@Column(name = "is_content_signing")
	private Boolean isContentSigning;
	
	@Version
	@Column(name = "version")
	private int version;

	@ManyToOne
	@JoinColumn(name = "wf_step_id")
	private WFStepEntity wfStep;

	@OneToMany(mappedBy = "wfMobileIDIdentitiesIssuance", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WFMobileIdStepVisualIDGroupConfigEntity> wfMobileIdStepVisualIDGroupConfigs = new ArrayList<>();

	@OneToMany(mappedBy = "wfMobileIDIdentitiesIssuance", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WFMobileIdStepCertificatesEntity> wfMobileIdStepCertificates = new ArrayList<>();

	@OneToOne(mappedBy = "wfMobileIDIdentityIssuance", cascade = CascadeType.ALL)
	private WFMobileIDStepCertConfigEntity wfMobileIDStepCertConfig;

	@OneToOne(mappedBy = "wfMobileIDIdentityIssuance", cascade = CascadeType.ALL)
	private WFMobileIDIdentitySoftOTPEntity wfMobileIDIdentitySoftOTP;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFriendlyName() {
		return friendlyName;
	}

	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}

	public Boolean getPushVerify() {
		return pushVerify;
	}

	public void setPushVerify(Boolean pushVerify) {
		this.pushVerify = pushVerify;
	}

	public Boolean getSoftOTP() {
		return softOTP;
	}

	public void setSoftOTP(Boolean softOTP) {
		this.softOTP = softOTP;
	}

	public Boolean getFido2() {
		return fido2;
	}

	public void setFido2(Boolean fido2) {
		this.fido2 = fido2;
	}

	public Boolean getIsContentSigning() {
		return isContentSigning;
	}

	public void setIsContentSigning(Boolean isContentSigning) {
		this.isContentSigning = isContentSigning;
	}
	
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public WFStepEntity getWfStep() {
		return wfStep;
	}

	public void setWfStep(WFStepEntity wfStep) {
		this.wfStep = wfStep;
	}

	public List<WFMobileIdStepVisualIDGroupConfigEntity> getWfMobileIdStepVisualIDGroupConfigs() {
		return wfMobileIdStepVisualIDGroupConfigs;
	}

	public void setWfMobileIdStepVisualIDGroupConfigs(
			List<WFMobileIdStepVisualIDGroupConfigEntity> wfMobileIdStepVisualIDGroupConfigs) {
		this.wfMobileIdStepVisualIDGroupConfigs = wfMobileIdStepVisualIDGroupConfigs;
	}

	public void addWFMobileIdStepVisualIDGroupConfig(
			WFMobileIdStepVisualIDGroupConfigEntity wfMobileIdStepVisualIDGroupConfigEntity) {
		wfMobileIdStepVisualIDGroupConfigEntity.setWfMobileIDIdentitiesIssuance(this);
		this.wfMobileIdStepVisualIDGroupConfigs.add(wfMobileIdStepVisualIDGroupConfigEntity);
	}

	public void removeWFMobileIDStepIdentityConfig(
			WFMobileIdStepVisualIDGroupConfigEntity wfMobileIdStepVisualIDGroupConfigEntity) {
		wfMobileIdStepVisualIDGroupConfigEntity.setWfMobileIDIdentitiesIssuance(null);
		this.wfMobileIdStepVisualIDGroupConfigs.remove(wfMobileIdStepVisualIDGroupConfigEntity);
	}

	public List<WFMobileIdStepCertificatesEntity> getWfMobileIdStepCertificates() {
		return wfMobileIdStepCertificates;
	}

	public void setWfMobileIdStepCertificates(List<WFMobileIdStepCertificatesEntity> wfMobileIdStepCertificates) {
		this.wfMobileIdStepCertificates = wfMobileIdStepCertificates;
	}

	public void addWfMobileIdStepCertificate(WFMobileIdStepCertificatesEntity wfMobileIdStepCertConfigEntity) {
		wfMobileIdStepCertConfigEntity.setWfMobileIDIdentitiesIssuance(this);
		this.wfMobileIdStepCertificates.add(wfMobileIdStepCertConfigEntity);
	}

	public void removeWfMobileIdStepCertificate(WFMobileIdStepCertificatesEntity wfMobileIdStepCertConfigEntity) {
		wfMobileIdStepCertConfigEntity.setWfMobileIDIdentitiesIssuance(null);
		this.wfMobileIdStepCertificates.remove(wfMobileIdStepCertConfigEntity);
	}

	public WFMobileIDStepCertConfigEntity getWfMobileIDStepCertConfig() {
		return wfMobileIDStepCertConfig;
	}

	public void setWfMobileIDStepCertConfig(WFMobileIDStepCertConfigEntity wfMobileIDStepCertConfig) {
		this.wfMobileIDStepCertConfig = wfMobileIDStepCertConfig;
	}

	public WFMobileIDIdentitySoftOTPEntity getWFMobileIDIdentitySoftOTPEntity() {
		return wfMobileIDIdentitySoftOTP;
	}

	public void setWFMobileIDIdentitySoftOTPEntity(WFMobileIDIdentitySoftOTPEntity wfMobileIDIdentitySoftOTP) {
		this.wfMobileIDIdentitySoftOTP = wfMobileIDIdentitySoftOTP;
	}

}
