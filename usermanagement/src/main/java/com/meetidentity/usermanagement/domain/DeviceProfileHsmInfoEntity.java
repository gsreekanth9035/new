package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "device_profile_hsm_info")
public class DeviceProfileHsmInfoEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "hsm_type_id")
	private Long hsmTypeId;

	@Column(name = "partition_name")
	private String partitionName;

	@Column(name = "partition_serial_number")
	private String partitionSerialNumber;

	@Column(name = "partition_password")
	private String partitionPassword;

	@Column(name = "master_key_label")
	private String masterKeyLabel;

	@Column(name = "admin_key_label")
	private String adminKeyLabel;

	@Column(name = "customer_master_key_label")
	private String customerMasterKeyLabel;

	@OneToOne
	@JoinColumn(name = "dev_prof_id")
	private DeviceProfileEntity deviceProfile;

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getHsmTypeId() {
		return hsmTypeId;
	}

	public void setHsmTypeId(Long hsmTypeId) {
		this.hsmTypeId = hsmTypeId;
	}

	public String getPartitionName() {
		return partitionName;
	}

	public void setPartitionName(String partitionName) {
		this.partitionName = partitionName;
	}

	public String getPartitionSerialNumber() {
		return partitionSerialNumber;
	}

	public void setPartitionSerialNumber(String partitionSerialNumber) {
		this.partitionSerialNumber = partitionSerialNumber;
	}

	public String getPartitionPassword() {
		return partitionPassword;
	}

	public void setPartitionPassword(String partitionPassword) {
		this.partitionPassword = partitionPassword;
	}

	public String getMasterKeyLabel() {
		return masterKeyLabel;
	}

	public void setMasterKeyLabel(String masterKeyLabel) {
		this.masterKeyLabel = masterKeyLabel;
	}

	public String getAdminKeyLabel() {
		return adminKeyLabel;
	}

	public void setAdminKeyLabel(String adminKeyLabel) {
		this.adminKeyLabel = adminKeyLabel;
	}

	public String getCustomerMasterKeyLabel() {
		return customerMasterKeyLabel;
	}

	public void setCustomerMasterKeyLabel(String customerMasterKeyLabel) {
		this.customerMasterKeyLabel = customerMasterKeyLabel;
	}

	public DeviceProfileEntity getDeviceProfile() {
		return deviceProfile;
	}

	public void setDeviceProfile(DeviceProfileEntity deviceProfile) {
		this.deviceProfile = deviceProfile;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
