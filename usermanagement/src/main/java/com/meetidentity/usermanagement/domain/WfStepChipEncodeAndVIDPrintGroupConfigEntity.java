package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "wf_step_chip_encode_vid_print_group_config")
public class WfStepChipEncodeAndVIDPrintGroupConfigEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "wf_step_id")
	private WFStepEntity wfStep;

	@Column(name = "reference_chip_encode_vid_print_group_id")
	private String referenceChipEncodeVIDPrintGroupId;

	@Column(name = "reference_chip_encode_vid_print_group_name")
	private String referenceChipEncodeVIDPrintGroupName;

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WFStepEntity getWfStep() {
		return wfStep;
	}

	public void setWfStep(WFStepEntity wfStep) {
		this.wfStep = wfStep;
	}

	public String getReferenceChipEncodeVIDPrintGroupId() {
		return referenceChipEncodeVIDPrintGroupId;
	}

	public void setReferenceChipEncodeVIDPrintGroupId(String referenceChipEncodeVIDPrintGroupId) {
		this.referenceChipEncodeVIDPrintGroupId = referenceChipEncodeVIDPrintGroupId;
	}

	public String getReferenceChipEncodeVIDPrintGroupName() {
		return referenceChipEncodeVIDPrintGroupName;
	}

	public void setReferenceChipEncodeVIDPrintGroupName(String referenceChipEncodeVIDPrintGroupName) {
		this.referenceChipEncodeVIDPrintGroupName = referenceChipEncodeVIDPrintGroupName;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
