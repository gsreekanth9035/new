package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "org_branding_policy_config")
public class OrganizationBrandingPolicyConfigEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "powered_by")
	private String poweredBy;

	@Column(name = "login_page_title")
	private String loginPageTitle;

	@Column(name = "mobile_app_name")
	private String mobileAppName;

	@Column(name = "mobile_services_app_name")
	private String mobileServicesAppName;

	@Column(name = "contact_us_support_email")
	private String contactUsSupportEmail;
	
	@Version
	@Column(name = "version")
	private int version;

	@OneToMany(mappedBy = "organizationBrandingPolicyConfig")
	private List<OrganizationEntity> organizations = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPoweredBy() {
		return poweredBy;
	}

	public void setPoweredBy(String poweredBy) {
		this.poweredBy = poweredBy;
	}

	public String getLoginPageTitle() {
		return loginPageTitle;
	}

	public void setLoginPageTitle(String loginPageTitle) {
		this.loginPageTitle = loginPageTitle;
	}

	public String getMobileAppName() {
		return mobileAppName;
	}

	public void setMobileAppName(String mobileAppName) {
		this.mobileAppName = mobileAppName;
	}

	public String getMobileServicesAppName() {
		return mobileServicesAppName;
	}

	public void setMobileServicesAppName(String mobileServicesAppName) {
		this.mobileServicesAppName = mobileServicesAppName;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<OrganizationEntity> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(List<OrganizationEntity> organizations) {
		this.organizations = organizations;
	}

	public String getContactUsSupportEmail() {
		return contactUsSupportEmail;
	}

	public void setContactUsSupportEmail(String contactUsSupportEmail) {
		this.contactUsSupportEmail = contactUsSupportEmail;
	}

}
