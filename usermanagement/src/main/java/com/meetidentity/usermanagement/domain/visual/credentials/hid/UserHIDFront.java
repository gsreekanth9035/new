package com.meetidentity.usermanagement.domain.visual.credentials.hid;

import java.io.Serializable;

public class UserHIDFront implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String ZoneXX_Background;
	private String ZoneXX_Logo;
	private String ZoneXX_ORG_HEALTH_ID;
	
	private String ZoneXX_Name;
	
	private String ZoneXX_DOB;
	private String ZoneXX_IssuedDate;
	private String ZoneXX_ExpirationDate;

	private String ZoneXX_PRMRY_SUB;
	private String ZoneXX_PRMRY_SUB_ID;
	private String ZoneXX_IDN;
	
	private String ZoneXX_PCP;
	private String ZoneXX_PCP_PHONE;

	private String ZoneXX_POLICY_NUMBER;
	private String ZoneXX_GROUP_PLAN;
	private String ZoneXX_HEALTH_PLAN_NUMBER;

	private String ZoneXXV_Signature;
	private String ZoneXXV_Name;
	private String ZoneXXV_DOB;
	private String ZoneXXV_IssuedDate;
	private String ZoneXXV_ExpirationDate;

	private String ZoneXXV_PRMRY_SUB;
	private String ZoneXXV_PRMRY_SUB_ID;
	private String ZoneXXV_IDN;
	
	private String ZoneXXV_PCP;
	private String ZoneXXV_PCP_PHONE;

	private String ZoneXXV_POLICY_NUMBER;
	private String ZoneXXV_GROUP_PLAN;
	private String ZoneXXV_HEALTH_PLAN_NUMBER; 
	
	private String ZoneXXV_Photograph;

	public String getZoneXX_Background() {
		return ZoneXX_Background;
	}

	public void setZoneXX_Background(String zoneXX_Background) {
		ZoneXX_Background = zoneXX_Background;
	}

	public String getZoneXX_Logo() {
		return ZoneXX_Logo;
	}

	public void setZoneXX_Logo(String zoneXX_Logo) {
		ZoneXX_Logo = zoneXX_Logo;
	}

	public String getZoneXX_ORG_HEALTH_ID() {
		return ZoneXX_ORG_HEALTH_ID;
	}

	public void setZoneXX_ORG_HEALTH_ID(String zoneXX_ORG_HEALTH_ID) {
		ZoneXX_ORG_HEALTH_ID = zoneXX_ORG_HEALTH_ID;
	}

	public String getZoneXX_Name() {
		return ZoneXX_Name;
	}

	public void setZoneXX_Name(String zoneXX_Name) {
		ZoneXX_Name = zoneXX_Name;
	}

	public String getZoneXX_DOB() {
		return ZoneXX_DOB;
	}

	public void setZoneXX_DOB(String zoneXX_DOB) {
		ZoneXX_DOB = zoneXX_DOB;
	}

	public String getZoneXX_IssuedDate() {
		return ZoneXX_IssuedDate;
	}

	public void setZoneXX_IssuedDate(String zoneXX_IssuedDate) {
		ZoneXX_IssuedDate = zoneXX_IssuedDate;
	}

	public String getZoneXX_ExpirationDate() {
		return ZoneXX_ExpirationDate;
	}

	public void setZoneXX_ExpirationDate(String zoneXX_ExpirationDate) {
		ZoneXX_ExpirationDate = zoneXX_ExpirationDate;
	}

	public String getZoneXX_PRMRY_SUB() {
		return ZoneXX_PRMRY_SUB;
	}

	public void setZoneXX_PRMRY_SUB(String zoneXX_PRMRY_SUB) {
		ZoneXX_PRMRY_SUB = zoneXX_PRMRY_SUB;
	}

	public String getZoneXX_PRMRY_SUB_ID() {
		return ZoneXX_PRMRY_SUB_ID;
	}

	public void setZoneXX_PRMRY_SUB_ID(String zoneXX_PRMRY_SUB_ID) {
		ZoneXX_PRMRY_SUB_ID = zoneXX_PRMRY_SUB_ID;
	}

	public String getZoneXX_IDN() {
		return ZoneXX_IDN;
	}

	public void setZoneXX_IDN(String zoneXX_IDN) {
		ZoneXX_IDN = zoneXX_IDN;
	}

	public String getZoneXX_PCP() {
		return ZoneXX_PCP;
	}

	public void setZoneXX_PCP(String zoneXX_PCP) {
		ZoneXX_PCP = zoneXX_PCP;
	}

	public String getZoneXX_PCP_PHONE() {
		return ZoneXX_PCP_PHONE;
	}

	public void setZoneXX_PCP_PHONE(String zoneXX_PCP_PHONE) {
		ZoneXX_PCP_PHONE = zoneXX_PCP_PHONE;
	}

	public String getZoneXX_POLICY_NUMBER() {
		return ZoneXX_POLICY_NUMBER;
	}

	public void setZoneXX_POLICY_NUMBER(String zoneXX_POLICY_NUMBER) {
		ZoneXX_POLICY_NUMBER = zoneXX_POLICY_NUMBER;
	}

	public String getZoneXX_GROUP_PLAN() {
		return ZoneXX_GROUP_PLAN;
	}

	public void setZoneXX_GROUP_PLAN(String zoneXX_GROUP_PLAN) {
		ZoneXX_GROUP_PLAN = zoneXX_GROUP_PLAN;
	}

	public String getZoneXX_HEALTH_PLAN_NUMBER() {
		return ZoneXX_HEALTH_PLAN_NUMBER;
	}

	public void setZoneXX_HEALTH_PLAN_NUMBER(String zoneXX_HEALTH_PLAN_NUMBER) {
		ZoneXX_HEALTH_PLAN_NUMBER = zoneXX_HEALTH_PLAN_NUMBER;
	}

	public String getZoneXXV_Signature() {
		return ZoneXXV_Signature;
	}

	public void setZoneXXV_Signature(String zoneXXV_Signature) {
		ZoneXXV_Signature = zoneXXV_Signature;
	}

	public String getZoneXXV_Name() {
		return ZoneXXV_Name;
	}

	public void setZoneXXV_Name(String zoneXXV_Name) {
		ZoneXXV_Name = zoneXXV_Name;
	}

	public String getZoneXXV_DOB() {
		return ZoneXXV_DOB;
	}

	public void setZoneXXV_DOB(String zoneXXV_DOB) {
		ZoneXXV_DOB = zoneXXV_DOB;
	}

	public String getZoneXXV_IssuedDate() {
		return ZoneXXV_IssuedDate;
	}

	public void setZoneXXV_IssuedDate(String zoneXXV_IssuedDate) {
		ZoneXXV_IssuedDate = zoneXXV_IssuedDate;
	}

	public String getZoneXXV_ExpirationDate() {
		return ZoneXXV_ExpirationDate;
	}

	public void setZoneXXV_ExpirationDate(String zoneXXV_ExpirationDate) {
		ZoneXXV_ExpirationDate = zoneXXV_ExpirationDate;
	}

	public String getZoneXXV_PRMRY_SUB() {
		return ZoneXXV_PRMRY_SUB;
	}

	public void setZoneXXV_PRMRY_SUB(String zoneXXV_PRMRY_SUB) {
		ZoneXXV_PRMRY_SUB = zoneXXV_PRMRY_SUB;
	}

	public String getZoneXXV_PRMRY_SUB_ID() {
		return ZoneXXV_PRMRY_SUB_ID;
	}

	public void setZoneXXV_PRMRY_SUB_ID(String zoneXXV_PRMRY_SUB_ID) {
		ZoneXXV_PRMRY_SUB_ID = zoneXXV_PRMRY_SUB_ID;
	}

	public String getZoneXXV_IDN() {
		return ZoneXXV_IDN;
	}

	public void setZoneXXV_IDN(String zoneXXV_IDN) {
		ZoneXXV_IDN = zoneXXV_IDN;
	}

	public String getZoneXXV_PCP() {
		return ZoneXXV_PCP;
	}

	public void setZoneXXV_PCP(String zoneXXV_PCP) {
		ZoneXXV_PCP = zoneXXV_PCP;
	}

	public String getZoneXXV_PCP_PHONE() {
		return ZoneXXV_PCP_PHONE;
	}

	public void setZoneXXV_PCP_PHONE(String zoneXXV_PCP_PHONE) {
		ZoneXXV_PCP_PHONE = zoneXXV_PCP_PHONE;
	}

	public String getZoneXXV_POLICY_NUMBER() {
		return ZoneXXV_POLICY_NUMBER;
	}

	public void setZoneXXV_POLICY_NUMBER(String zoneXXV_POLICY_NUMBER) {
		ZoneXXV_POLICY_NUMBER = zoneXXV_POLICY_NUMBER;
	}

	public String getZoneXXV_GROUP_PLAN() {
		return ZoneXXV_GROUP_PLAN;
	}

	public void setZoneXXV_GROUP_PLAN(String zoneXXV_GROUP_PLAN) {
		ZoneXXV_GROUP_PLAN = zoneXXV_GROUP_PLAN;
	}

	public String getZoneXXV_HEALTH_PLAN_NUMBER() {
		return ZoneXXV_HEALTH_PLAN_NUMBER;
	}

	public void setZoneXXV_HEALTH_PLAN_NUMBER(String zoneXXV_HEALTH_PLAN_NUMBER) {
		ZoneXXV_HEALTH_PLAN_NUMBER = zoneXXV_HEALTH_PLAN_NUMBER;
	}

	public String getZoneXXV_Photograph() {
		return ZoneXXV_Photograph;
	}

	public void setZoneXXV_Photograph(String zoneXXV_Photograph) {
		ZoneXXV_Photograph = zoneXXV_Photograph;
	}
	
	

	
}
