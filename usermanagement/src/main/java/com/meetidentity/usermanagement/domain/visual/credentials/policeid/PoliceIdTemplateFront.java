package com.meetidentity.usermanagement.domain.visual.credentials.policeid;

import java.io.Serializable;

public class PoliceIdTemplateFront implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String Zone2FV_Last_Name;
	private String Zone2FV_First__Middle_Name;
	private String Zone13FV_IDesignation;
	private String Zone14FV_ID;
	private String Zone9FS_Header;
	private String Zone12FSV_Footer;
	private String Identification_Number;
	private String Background;
	private String Zone1FV_Photograph;
	private String Zone11FS_AgencySeal;
	private String Zone11FS_AgencySeal_copy;
	private String Zone6FV_QR;
	
	public String getZone2FV_Last_Name() {
		return Zone2FV_Last_Name;
	}
	public void setZone2FV_Last_Name(String zone2fv_Last_Name) {
		Zone2FV_Last_Name = zone2fv_Last_Name;
	}
	public String getZone2FV_First__Middle_Name() {
		return Zone2FV_First__Middle_Name;
	}
	public void setZone2FV_First__Middle_Name(String zone2fv_First__Middle_Name) {
		Zone2FV_First__Middle_Name = zone2fv_First__Middle_Name;
	}
	public String getZone13FV_IDesignation() {
		return Zone13FV_IDesignation;
	}
	public void setZone13FV_IDesignation(String zone13fv_IDesignation) {
		Zone13FV_IDesignation = zone13fv_IDesignation;
	}
	public String getZone14FV_ID() {
		return Zone14FV_ID;
	}
	public void setZone14FV_ID(String zone14fv_ID) {
		Zone14FV_ID = zone14fv_ID;
	}
	public String getZone9FS_Header() {
		return Zone9FS_Header;
	}
	public void setZone9FS_Header(String zone9fs_Header) {
		Zone9FS_Header = zone9fs_Header;
	}
	public String getZone12FSV_Footer() {
		return Zone12FSV_Footer;
	}
	public void setZone12FSV_Footer(String zone12fsv_Footer) {
		Zone12FSV_Footer = zone12fsv_Footer;
	}
	public String getIdentification_Number() {
		return Identification_Number;
	}
	public void setIdentification_Number(String identification_Number) {
		Identification_Number = identification_Number;
	}
	public String getBackground() {
		return Background;
	}
	public void setBackground(String background) {
		Background = background;
	}
	public String getZone1FV_Photograph() {
		return Zone1FV_Photograph;
	}
	public void setZone1FV_Photograph(String zone1fv_Photograph) {
		Zone1FV_Photograph = zone1fv_Photograph;
	}
	public String getZone11FS_AgencySeal() {
		return Zone11FS_AgencySeal;
	}
	public void setZone11FS_AgencySeal(String zone11fs_AgencySeal) {
		Zone11FS_AgencySeal = zone11fs_AgencySeal;
	}
	public String getZone11FS_AgencySeal_copy() {
		return Zone11FS_AgencySeal_copy;
	}
	public void setZone11FS_AgencySeal_copy(String zone11fs_AgencySeal_copy) {
		Zone11FS_AgencySeal_copy = zone11fs_AgencySeal_copy;
	}
	public String getZone6FV_QR() {
		return Zone6FV_QR;
	}
	public void setZone6FV_QR(String zone6fv_QR) {
		Zone6FV_QR = zone6fv_QR;
	}

}
