package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "wf_step_chip_encode_group_config")
public class WfStepChipEncodeGroupConfigEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "wf_step_id")
	private WFStepEntity wfStep;

	@Column(name = "reference_chip_encode_group_id")
	private String referenceChipEncodeGroupId;

	@Column(name = "reference_chip_encode_group_name")
	private String referenceChipEncodeGroupName;

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WFStepEntity getWfStep() {
		return wfStep;
	}

	public void setWfStep(WFStepEntity wfStep) {
		this.wfStep = wfStep;
	}

	public String getReferenceChipEncodeGroupId() {
		return referenceChipEncodeGroupId;
	}

	public void setReferenceChipEncodeGroupId(String referenceChipEncodeGroupId) {
		this.referenceChipEncodeGroupId = referenceChipEncodeGroupId;
	}

	public String getReferenceChipEncodeGroupName() {
		return referenceChipEncodeGroupName;
	}

	public void setReferenceChipEncodeGroupName(String referenceChipEncodeGroupName) {
		this.referenceChipEncodeGroupName = referenceChipEncodeGroupName;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
