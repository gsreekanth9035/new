package com.meetidentity.usermanagement.domain;

import java.time.Instant;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.meetidentity.usermanagement.annotations.SequenceValue;

@Entity
@Table(name = "user_health_id")
public class UserHealthIDEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;

	@Column(name = "uims_id")
	private String uimsId;

	@SequenceValue
	@Column(name = "id_number")
	private String idNumber;

	@Column(name = "primary_subscriber")
	private String primarySubscriber;
	
	@Column(name = "primary_subscriber_id")
	private String primarySubscriberID;
	
	@Column(name = "pcp")
	private String pcp;
	
	@Column(name = "pcp_phone")
	private String pcpPhone;
	
	@Column(name = "date_of_issuance")
	private Instant dateOfIssuance;
	
	@Column(name = "date_of_expiry")
	private Instant dateOfExpiry;

	@Column(name = "policy_number")
	private String policyNumber;

	@Column(name = "group_plan")
	private String groupPlan;
	
	@Column(name = "health_plan_number")
	private String healthPlanNumber;	

	@Column(name = "agency_card_sn")
	private String agencyCardSN;
	
	@Column(name = "issuer_in")
	private String issuerIN;	
	   
	public UserHealthIDEntity() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getPrimarySubscriber() {
		return primarySubscriber;
	}

	public void setPrimarySubscriber(String primarySubscriber) {
		this.primarySubscriber = primarySubscriber;
	}

	public String getPrimarySubscriberID() {
		return primarySubscriberID;
	}

	public void setPrimarySubscriberID(String primarySubscriberID) {
		this.primarySubscriberID = primarySubscriberID;
	}

	public String getPcp() {
		return pcp;
	}

	public void setPcp(String pcp) {
		this.pcp = pcp;
	}

	public String getPcpPhone() {
		return pcpPhone;
	}

	public void setPcpPhone(String pcpPhone) {
		this.pcpPhone = pcpPhone;
	}

	public Instant getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Instant dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public Instant getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Instant dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getGroupPlan() {
		return groupPlan;
	}

	public void setGroupPlan(String groupPlan) {
		this.groupPlan = groupPlan;
	}

	public String getHealthPlanNumber() {
		return healthPlanNumber;
	}

	public void setHealthPlanNumber(String healthPlanNumber) {
		this.healthPlanNumber = healthPlanNumber;
	}
	
	public String getAgencyCardSN() {
		return agencyCardSN;
	}

	public void setAgencyCardSN(String agencyCardSN) {
		this.agencyCardSN = agencyCardSN;
	}

	public String getIssuerIN() {
		return issuerIN;
	}

	public void setIssuerIN(String issuerIN) {
		this.issuerIN = issuerIN;
	}


}
