
package com.meetidentity.usermanagement.domain.express.enroll;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "$type",
    "Name",
    "Description",
    "Analysis",
    "Action",
    "Result",
    "Disposition"
})
public class Analysi {

    @JsonProperty("$type")
    private String $type;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("Analysis")
    private String analysis;
    @JsonProperty("Action")
    private String action;
    @JsonProperty("Result")
    private String result;
    @JsonProperty("Disposition")
    private String disposition;
    @JsonIgnore
    private Map<String, String> additionalProperties = new HashMap<String, String>();

    @JsonProperty("$type")
    public String get$type() {
        return $type;
    }

    @JsonProperty("$type")
    public void set$type(String $type) {
        this.$type = $type;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("Description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("Description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("Analysis")
    public String getAnalysis() {
        return analysis;
    }

    @JsonProperty("Analysis")
    public void setAnalysis(String analysis) {
        this.analysis = analysis;
    }

    @JsonProperty("Action")
    public String getAction() {
        return action;
    }

    @JsonProperty("Action")
    public void setAction(String action) {
        this.action = action;
    }

    @JsonProperty("Result")
    public String getResult() {
        return result;
    }

    @JsonProperty("Result")
    public void setResult(String result) {
        this.result = result;
    }

    @JsonProperty("Disposition")
    public String getDisposition() {
        return disposition;
    }

    @JsonProperty("Disposition")
    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, String> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, String value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append($type).append(name).append(description).append(analysis).append(action).append(result).append(disposition).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Analysi) == false) {
            return false;
        }
        Analysi rhs = ((Analysi) other);
        return new EqualsBuilder().append($type, rhs.$type).append(name, rhs.name).append(description, rhs.description).append(analysis, rhs.analysis).append(action, rhs.action).append(result, rhs.result).append(disposition, rhs.disposition).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
