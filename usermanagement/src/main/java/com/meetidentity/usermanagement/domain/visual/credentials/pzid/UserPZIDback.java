package com.meetidentity.usermanagement.domain.visual.credentials.pzid;

import java.io.Serializable;

public class UserPZIDback implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String ZoneXFV_DOE;

	public String getZoneXFV_DOE() {
		return ZoneXFV_DOE;
	}

	public void setZoneXFV_DOE(String zoneXFV_DOE) {
		ZoneXFV_DOE = zoneXFV_DOE;
	}

}
