package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "org_identity_field_config")
public class OrgIdentityFieldConfigEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
    @JoinColumn(name="org_identities_id")
	private OrganizationIdentitiesEntity OrganizationIdentities;
	
	@ManyToOne
    @JoinColumn(name="identity_field_id")
	private IdentityFieldEntity identityField;

	@ManyToOne
    @JoinColumn(name="config_value_id")
    private ConfigValueEntity configValue;
	
	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrganizationIdentitiesEntity getOrganizationIdentities() {
		return OrganizationIdentities;
	}

	public void setOrganizationIdentities(OrganizationIdentitiesEntity organizationIdentities) {
		OrganizationIdentities = organizationIdentities;
	}

	public IdentityFieldEntity getIdentityField() {
		return identityField;
	}

	public void setIdentityField(IdentityFieldEntity identityField) {
		this.identityField = identityField;
	}

	public ConfigValueEntity getConfigValue() {
		return configValue;
	}

	public void setConfigValue(ConfigValueEntity configValue) {
		this.configValue = configValue;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
