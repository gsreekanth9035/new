package com.meetidentity.usermanagement.domain.visual.credentials.policeid;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.constants.XMLConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.User;

@Component
public class UtilityPoliceId {

	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;

	private final Logger log = LoggerFactory.getLogger(UtilityPoliceId.class);

	private static String XLINK_NS = XMLConstants.XLINK_NAMESPACE_URI;
	private static String XLINK_HREF = "xlink:href";

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @param string
	 * @return
	 * @throws Exception
	 */
	public Document parsePoliceIdFront(SAXSVGDocumentFactory sAXSVGDocumentFactory, User user, byte[] svgFile,
			byte[] jsonFile, String credentialTemplateID, String visualCredentialType)
			throws VisualCredentialException {
		try {
			Gson gson = new Gson();
			PoliceIDTemplate policeIDTemplate = gson.fromJson(new String(jsonFile), PoliceIDTemplate.class);

			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			// Text variables
			log.debug("parse Police ID Front: Started");
			log.debug("Police ID Front Text Fields: Started");

			doc.getElementById("Zone2FV_Last_Name")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateFront().getZone2FV_Last_Name());
			doc.getElementById("Zone2FV_First__Middle_Name")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateFront().getZone2FV_First__Middle_Name());
			doc.getElementById("Zone13FV_IDesignation")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateFront().getZone13FV_IDesignation());
			doc.getElementById("Zone14FV_ID")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateFront().getZone14FV_ID());
			doc.getElementById("Zone9FS_Header")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateFront().getZone9FS_Header());
			doc.getElementById("Zone12FSV_Footer")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateFront().getZone12FSV_Footer());
			doc.getElementById("Identification_Number")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateFront().getIdentification_Number());

			log.debug("Police ID Front Image Fields: Started");

			doc.getElementById("Background").setAttributeNS(XLINK_NS, XLINK_HREF,
					"data:;base64," + policeIDTemplate.getPoliceIdTemplateFront().getBackground());
			doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
					"data:;base64," + policeIDTemplate.getPoliceIdTemplateFront().getZone1FV_Photograph());
			doc.getElementById("Zone11FS_AgencySeal").setAttributeNS(XLINK_NS, XLINK_HREF,
					"data:;base64," + policeIDTemplate.getPoliceIdTemplateFront().getZone11FS_AgencySeal());
			doc.getElementById("Zone11FS_AgencySeal_copy").setAttributeNS(XLINK_NS, XLINK_HREF,
					"data:;base64," + policeIDTemplate.getPoliceIdTemplateFront().getZone11FS_AgencySeal_copy());
			doc.getElementById("Zone6FV_QR")
					.setAttributeNS(XLINK_NS, XLINK_HREF,
							"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
									user.getOrganisationName() + "#" + user.getId() + "#" + credentialTemplateID,
									BarcodeFormat.QR_CODE, 300, 300))));

			if (user != null) {

				// FirstName
				doc.getElementById("Zone2FV_First__Middle_Name")
						.setTextContent(user.getFirstName() + " " + user.getMiddleName());
				// Last Name
				doc.getElementById("Zone2FV_Last_Name").setTextContent(user.getLastName());

				user.getUserBiometrics().stream().forEach(userBio -> {
					// FACE
					if (userBio.getType().equalsIgnoreCase("Face")) {
						int width = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("width"));
						int height = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("height"));
						try {
							Image image = Util
									.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
							BufferedImage bufferedImage = Util.resizeImage(image, width, height);
							if(user.getIsTransparentPhotoRequired()) {
								bufferedImage = backgroundColorTransparency.generate(bufferedImage,
										user.getOrganisationName());
							}
							String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
							doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + base64Data);

						} catch (IOException e) {
							doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
						}
					}
				});
			}
			log.debug("parse Police ID Front: Completed");
			return doc;

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @return
	 * @throws Exception
	 */
	public Document parsePoliceIdBack(SAXSVGDocumentFactory sAXSVGDocumentFactory, User user, byte[] svgFile,
			byte[] jsonFile, String visualCredentialType) throws VisualCredentialException {

		try {
			Gson gson = new Gson();
			PoliceIDTemplate policeIDTemplate = gson.fromJson(new String(jsonFile), PoliceIDTemplate.class);

			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			// Text variables
			log.debug("parse Police ID Back: Started");
			log.debug("Police ID Back Text Fields: Started");

			doc.getElementById("Zone2BV_IssuerIN")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getZone2BV_IssuerIN());
			doc.getElementById("Zone1BV_Agency_CardSN")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getZone1BV_Agency_CardSN());
			doc.getElementById("Zone7BS_Section499")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getZone7BS_Section499());
			doc.getElementById("Zone7BS_Section499_l2")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getZone7BS_Section499_l2());
			doc.getElementById("Zone_6BS_Ainfo")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getZone_6BS_Ainfo());
			doc.getElementById("Zone_6BS_Ainfo_l2")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getZone_6BS_Ainfo_l2());
			doc.getElementById("Height").setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getHeight());
			doc.getElementById("Return_to").setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getReturn_to());
			doc.getElementById("Zone4BS_ReturnAddress_l1")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getZone4BS_ReturnAddress_l1());
			doc.getElementById("Zone4BS_ReturnAddress_l2")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getZone4BS_ReturnAddress_l2());
			doc.getElementById("Zone4BS_ReturnAddress_l3")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getZone4BS_ReturnAddress_l3());
			doc.getElementById("Zone5BS_PhysicalCC_Height")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getZone5BS_PhysicalCC_Height());
			doc.getElementById("Eyes").setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getEyes());
			doc.getElementById("Zone5BS_PhysicalCC_Eyes")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getZone5BS_PhysicalCC_Eyes());
			doc.getElementById("Hairs").setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getHairs());
			doc.getElementById("Zone5BS_PhysicalCC_Hairs")
					.setTextContent(policeIDTemplate.getPoliceIdTemplateBack().getZone5BS_PhysicalCC_Hairs());

			log.debug("Police ID Back Image Fields: Started");

			doc.getElementById("Layer_0").setAttributeNS(XLINK_NS, XLINK_HREF,
					"data:;base64," + policeIDTemplate.getPoliceIdTemplateBack().getLayer_0());
			doc.getElementById("Zone8BV_LinearBC").setAttributeNS(XLINK_NS, XLINK_HREF,
					"data:;base64," + policeIDTemplate.getPoliceIdTemplateBack().getZone8BV_LinearBC());

			log.debug("parse Police ID Back: Completed");

			return doc;

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}
}
