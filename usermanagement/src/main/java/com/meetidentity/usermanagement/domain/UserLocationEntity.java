package com.meetidentity.usermanagement.domain;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.vividsolutions.jts.geom.Point;


@Entity
@Table(name="user_location")
public class UserLocationEntity  extends AbstractAuditingEntity {
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    
    @ManyToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;

	@Column(name="name")
    private String name;
    
    @Column(name = "location")
	private Point location;

    @Column(name = "date")
    private Date date;
    
    @Column(name = "status")
    private int status;
    
	public UserLocationEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserLocationEntity(  UserEntity user, String name, Point location, Date date) {
		super(); 
		this.user = user;
		this.name = name;
		this.location = location;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
    
    
 
    // standard getters and setters
}
