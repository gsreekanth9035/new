package com.meetidentity.usermanagement.domain.visual.credentials.hid;

import java.io.Serializable;

public class UserHID implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserHIDFront userHIDFront;
	private UserHIDBack userHIDBack;
	
	public UserHIDFront getUserHIDFront() {
		return userHIDFront;
	}
	public void setUserHIDFront(UserHIDFront userHIDFront) {
		this.userHIDFront = userHIDFront;
	}
	public UserHIDBack getUserHIDBack() {
		return userHIDBack;
	}
	public void setUserHIDBack(UserHIDBack userHIDBack) {
		this.userHIDBack = userHIDBack;
	}

	
	
	

}
