package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "device_profile")
public class DeviceProfileEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "description")
	private String description;

	@ManyToOne
	@JoinColumn(name = "category_id")
	private ConfigValueEntity configValueCategory;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
	private OrganizationEntity organization;

	@ManyToOne
	@JoinColumn(name = "supplier_id")
	private ConfigValueEntity configValueSupplier;

	@ManyToOne
	@JoinColumn(name = "product_name_id")
	private ConfigValueEntity configValueProductName;

	@Column(name = "key_storage_type")
	private String keyStorageType;

	@Version
	@Column(name = "version")
	private int version;

	@OneToOne(mappedBy = "deviceProfile", cascade = CascadeType.ALL)
	private DeviceProfileKeyManagerEntity devProfKeyMgr;

	@OneToOne(mappedBy = "deviceProfile", cascade = CascadeType.ALL)
	private DeviceProfileHsmInfoEntity devProfHsmInfo;

	@OneToMany(mappedBy = "deviceProfile", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<WFDeviceProfileEntity> wfDeviceProfileEntity = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getVersion() {
		return version;
	}

	public ConfigValueEntity getConfigValueCategory() {
		return configValueCategory;
	}

	public void setConfigValueCategory(ConfigValueEntity configValueCategory) {
		this.configValueCategory = configValueCategory;
	}

	public ConfigValueEntity getConfigValueSupplier() {
		return configValueSupplier;
	}

	public void setConfigValueSupplier(ConfigValueEntity configValueSupplier) {
		this.configValueSupplier = configValueSupplier;
	}

	public ConfigValueEntity getConfigValueProductName() {
		return configValueProductName;
	}

	public void setConfigValueProductName(ConfigValueEntity configValueProductName) {
		this.configValueProductName = configValueProductName;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getKeyStorageType() {
		return keyStorageType;
	}

	public void setKeyStorageType(String keyStorageType) {
		this.keyStorageType = keyStorageType;
	}

	public DeviceProfileKeyManagerEntity getDevProfKeyMgr() {
		return devProfKeyMgr;
	}

	public void setDevProfKeyMgr(DeviceProfileKeyManagerEntity devProfKeyMgr) {
		this.devProfKeyMgr = devProfKeyMgr;
	}

	public DeviceProfileHsmInfoEntity getDevProfHsmInfo() {
		return devProfHsmInfo;
	}

	public void setDevProfHsmInfo(DeviceProfileHsmInfoEntity devProfHsmInfo) {
		this.devProfHsmInfo = devProfHsmInfo;
	}

	public List<WFDeviceProfileEntity> getWfDeviceProfileEntity() {
		return wfDeviceProfileEntity;
	}

	public void setWfDeviceProfileEntity(List<WFDeviceProfileEntity> wfDeviceProfileEntity) {
		this.wfDeviceProfileEntity = wfDeviceProfileEntity;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

}
