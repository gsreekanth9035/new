package com.meetidentity.usermanagement.domain.visual.credentials.vid;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import javax.imageio.ImageIO;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.constants.XMLConstants;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserAddressEntity;
import com.meetidentity.usermanagement.domain.UserAttributeEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserVoterIDEntity;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.security.mrz.records.MrzTD1;
import com.meetidentity.usermanagement.security.mrz.types.MrzDate;
import com.meetidentity.usermanagement.security.mrz.types.MrzSex;
import com.meetidentity.usermanagement.service.ZxingBarcodeGeneratorService;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.service.helper.UserCredentialsServiceHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAddress;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;


@Component
public class UtilityVID {

	@Autowired
	private Util util = new Util();
	
	@Autowired
	private UserCredentialsServiceHelper userCredentialsServiceHelper;
	
	@Autowired
	private ZxingBarcodeGeneratorService zxingBarcodeGeneratorService;	
	
	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;
	
	private final Logger log = LoggerFactory.getLogger(UtilityVID.class);

	private final Logger logger = LoggerFactory.getLogger(UtilityVID.class);
	private static String XLINK_NS = XMLConstants.XLINK_NAMESPACE_URI;
	private static String XLINK_HREF = "xlink:href";

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @return
	 * @throws Exception
	 */
	public Document parseVIDFront(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity,User user, byte[] svgFile,
			byte[] jsonFile, UserVoterIDEntity userVoterIDEntity,  String credentialTemplateID, String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			
			if(simplyParse) {
				JSONObject vidJsonObject = new JSONObject(new String(jsonFile));
				JSONObject vidFrontJsonObject = vidJsonObject.getJSONObject("userVIDFront");
				Iterator<String> keys = vidFrontJsonObject.keys();
				log.debug("parseVIDFront: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				// UserPRID userNID = gson.fromJson(new String(jsonFile), UserPRID.class);				
				// Dynamic User Variables
				
				switch (requestFrom) {

					case "web_portal_enroll_summary":
						UserAttribute userAttribute = user.getUserAttribute();
						if(userVoterIDEntity != null) {
							doc.getElementById("Zone2_07FV_IDNumber").setTextContent(userVoterIDEntity.getCurp() != null ? userVoterIDEntity.getCurp().toUpperCase() : "XXXXXXXXXXXXXXXXXX");
							doc.getElementById("Zone2_06FV_Elector_Key").setTextContent(userVoterIDEntity.getElectorKey() != null ? userVoterIDEntity.getElectorKey().toUpperCase() : "XXXXXXXXXXXXXXXXXX");
							doc.getElementById("Zone2_09FV_Registration_Year").setTextContent(userVoterIDEntity.getRegistrationYear());
						}else {
							doc.getElementById("Zone2_07FV_IDNumber").setTextContent("XXXXXXXXXXXXXXXXXX");
							doc.getElementById("Zone2_06FV_Elector_Key").setTextContent("XXXXXXXXXXXXXXXXXX");
							doc.getElementById("Zone2_09FV_Registration_Year").setTextContent(Calendar.getInstance().get(Calendar.YEAR)+"");
						}
						doc.getElementById("Zone2_03FV_DOB").setTextContent(userAttribute.getDateOfBirth()== null? "XX/XX/XXXX" : Util.formatDate(userAttribute.getDateOfBirth(), Util.MMDDYYYY_format));
						
						doc.getElementById("Zone2_01FV_Family_Name").setTextContent(userAttribute.getLastName());
						doc.getElementById("Zone2_02FV_Given_Name").setTextContent(userAttribute.getFirstName());
						doc.getElementById("Zone2_04FV_Sex").setTextContent(userAttribute.getGender().equalsIgnoreCase("m")?"H":"M");
						UserAddress userAddress = user.getUserAddress();
						String addressL1 = userCredentialsServiceHelper.getAddressFromAddressObj_addressLine1(userAddress);
						String addressL2 = userCredentialsServiceHelper.getAddressFromAddressObj_addressLine2(userAddress);
						if(addressL1 != null && addressL2 != null) {
							doc.getElementById("Zone2_05BV_CHAddress_1").setTextContent(addressL1);	
							doc.getElementById("Zone2_05BV_CHAddress_2").setTextContent(addressL2);	
						} else {
							//TODO need to add few more conditional setters in future
							doc.getElementById("Zone2_05BV_CHAddress_1").setTextContent("");	
							doc.getElementById("Zone2_05BV_CHAddress_2").setTextContent("");	
						}
						doc.getElementById("Zone2_08FV_Birthplace").setTextContent(userAttribute.getPlaceOfBirth());						
						doc.getElementById("Zone2_10FV_State").setTextContent("32");
						doc.getElementById("Zone2_11FV_DOI").setTextContent("XX/XX/XXXX");
						doc.getElementById("Zone2_12FV_DOE").setTextContent("XX/XX/XXXX");						
						user.getUserBiometrics().stream().forEach(userBio -> {
							if (userBio.getType().equalsIgnoreCase("FACE")) {
								
								int width = Integer.parseInt(doc.getElementById("Zone3_01FV_Photograph").getAttribute("width"));
								int height = Integer.parseInt(doc.getElementById("Zone3_01FV_Photograph").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
									doc.getElementById("Zone3_01FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);

								} catch (IOException e) {
									doc.getElementById("Zone3_01FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
								
								width = Integer.parseInt(doc.getElementById("Zone2_13FV_Photoghost").getAttribute("width"));
								height = Integer.parseInt(doc.getElementById("Zone2_13FV_Photoghost").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate_photoghost(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);

									doc.getElementById("Zone2_13FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);


								} catch (IOException e) {
									doc.getElementById("Zone2_13FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
							if(doc.getElementById("Zone3_02FV_Signature") != null) {
								if(userBio.getType().equalsIgnoreCase("SIGNATURE")) {
										doc.getElementById("Zone3_02FV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
						});
						
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":
						UserAttributeEntity userAttributee = userEntity.getUserAttribute();
						if(userVoterIDEntity != null) {
							doc.getElementById("Zone2_07FV_IDNumber").setTextContent(userVoterIDEntity.getCurp() != null ? userVoterIDEntity.getCurp().toUpperCase() : "XXXXXXXXXXXXXXXXXX");
							doc.getElementById("Zone2_06FV_Elector_Key").setTextContent(userVoterIDEntity.getElectorKey() != null ? userVoterIDEntity.getElectorKey().toUpperCase() : "XXXXXXXXXXXXXXXXXX");
							doc.getElementById("Zone2_09FV_Registration_Year").setTextContent(userVoterIDEntity.getRegistrationYear());
						}else {
							doc.getElementById("Zone2_07FV_IDNumber").setTextContent("XXXXXXXXXXXXXXXXXX");
							doc.getElementById("Zone2_06FV_Elector_Key").setTextContent("XXXXXXXXXXXXXXXXXX");
							doc.getElementById("Zone2_09FV_Registration_Year").setTextContent(Calendar.getInstance().get(Calendar.YEAR)+"");
						}
						doc.getElementById("Zone2_03FV_DOB").setTextContent(userAttributee.getDateOfBirth()== null? "XX/XX/XXXX" : Util.formatDate(userAttributee.getDateOfBirth(), Util.MMDDYYYY_format));
						doc.getElementById("Zone2_01FV_Family_Name").setTextContent(userAttributee.getLastName());
						doc.getElementById("Zone2_02FV_Given_Name").setTextContent(userAttributee.getFirstName());
						doc.getElementById("Zone2_04FV_Sex").setTextContent(userAttributee.getGender().equalsIgnoreCase("m")?"H":"M");
						UserAddressEntity userAddressEntity = userEntity.getUserAddress();
						String addressL1_ = userCredentialsServiceHelper.getAddressFromAddressObj_addressLine1(userAddressEntity);
						String addressL2_ = userCredentialsServiceHelper.getAddressFromAddressObj_addressLine2(userAddressEntity);
						if(addressL1_ != null && addressL2_ != null) {
							doc.getElementById("Zone2_05BV_CHAddress_1").setTextContent(addressL1_);	
							doc.getElementById("Zone2_05BV_CHAddress_2").setTextContent(addressL2_);	
						} else {
							//TODO need to add few more conditional setters in future
							doc.getElementById("Zone2_05BV_CHAddress_1").setTextContent("");	
							doc.getElementById("Zone2_05BV_CHAddress_2").setTextContent("");	
						}
						doc.getElementById("Zone2_08FV_Birthplace").setTextContent(userAttributee.getPlaceOfBirth());						
						doc.getElementById("Zone2_10FV_State").setTextContent("32");						
						doc.getElementById("Zone2_11FV_DOI").setTextContent(userVoterIDEntity.getDateOfIssuance() == null? "XX/XX/XXXX" : Util.formatDate(userVoterIDEntity.getDateOfIssuance(), Util.MMDDYYYY_format));
						doc.getElementById("Zone2_12FV_DOE").setTextContent(userVoterIDEntity.getDateOfExpiry() == null? "XX/XX/XXXX" : Util.formatDate(userVoterIDEntity.getDateOfExpiry(), Util.MMDDYYYY_format));
						
						userEntity.getUserBiometrics().stream().forEach(userBio -> {
							if (userBio.getType().equalsIgnoreCase("FACE")) {
								
								int width = Integer.parseInt(doc.getElementById("Zone3_01FV_Photograph").getAttribute("width"));
								int height = Integer.parseInt(doc.getElementById("Zone3_01FV_Photograph").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
									doc.getElementById("Zone3_01FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);

								} catch (IOException e) {
									doc.getElementById("Zone3_01FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
								
								width = Integer.parseInt(doc.getElementById("Zone2_13FV_Photoghost").getAttribute("width"));
								height = Integer.parseInt(doc.getElementById("Zone2_13FV_Photoghost").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate_photoghost(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);

									doc.getElementById("Zone2_13FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);


								} catch (IOException e) {
									doc.getElementById("Zone2_13FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
							if(doc.getElementById("Zone3_02FV_Signature") != null) {
								if(userBio.getType().equalsIgnoreCase("SIGNATURE")) {
										doc.getElementById("Zone3_02FV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
						});
						
						break;
					default :
				
				}
								
				logger.debug("parseVIDFront: Ended Dynamic Variable");
			}
			return doc;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}

	public Document parseVIDBack(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity,User user, byte[] svgFile,
			byte[] jsonFile, UserVoterIDEntity userVoterIDEntity,  String credentialTemplateID,
			String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {

				JSONObject vidJsonObject = new JSONObject(new String(jsonFile));
				JSONObject vidBacktJsonObject = vidJsonObject.getJSONObject("userVIDBack");
				Iterator<String> keys = vidBacktJsonObject.keys();
				log.debug("parseVIDBack: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				StringBuilder pdf417_text = new StringBuilder();
				// UserPRID userNID = gson.fromJson(new String(jsonFile), UserPRID.class);
				// Dynamic User Variables
				switch (requestFrom) {

					case "web_portal_enroll_summary":
						UserAttribute userAttribute = user.getUserAttribute();
						
						 final MrzTD1 r = new MrzTD1();
					        r.code2 = 'I';
					        r.issuingCountry = userAttribute.getNationality().toUpperCase();
					        r.nationality = userAttribute.getNationality().toUpperCase();
					        r.optional = "";   r.optional2 = "";
					        Calendar cal_dob = Calendar.getInstance();
					        cal_dob.setTime(userAttribute.getDateOfBirth());
					        Calendar cal_exp = Calendar.getInstance();
					        
							r.documentNumber = "XXXXXXXX";
							cal_exp.setTime(new Date());
							
							r.expirationDate = new MrzDate(cal_exp.get(Calendar.YEAR)%100, cal_exp.get(Calendar.MONTH)+1,cal_exp.get(Calendar.DAY_OF_MONTH) );
					        r.dateOfBirth = new MrzDate(cal_dob.get(Calendar.YEAR)%100, cal_dob.get(Calendar.MONTH)+1,cal_dob.get(Calendar.DAY_OF_MONTH) );
					        if(userAttribute.getGender().toLowerCase().contains("m")) {
					        	r.sex = MrzSex.Male;
					        }else 
					        if(userAttribute.getGender().toLowerCase().contains("f")) {
					        	r.sex = MrzSex.Female;
					        }else {
					        	r.sex = MrzSex.Male;
					        }
					        r.surname = userAttribute.getLastName();
					        r.givenNames = userAttribute.getFirstName();
					        doc.getElementById("Zone3_MRZ_TD1_L1").setTextContent(r.toMrz_line1());
					        doc.getElementById("Zone3_MRZ_TD1_L2").setTextContent(r.toMrz_line2());
					        doc.getElementById("Zone3_MRZ_TD1_L3").setTextContent(r.toMrz_line3());
					        
					        doc.getElementById("Zone2_02BV_QR")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + user.getId() + "#" + credentialTemplateID + "#" + "XXXXXXXXXX",
											BarcodeFormat.QR_CODE, 300, 300))));

					        user.getUserBiometrics().stream().forEach(userBio -> {

								if(userBio.getType().equalsIgnoreCase("SIGNATURE")) {
										doc.getElementById("Zone2_04BV_Citizen_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
					        });
					        if(doc.getElementById("Zone2_05BV_Citizen_Fingerprint") != null ) {
						        user.getUserBiometrics().stream().forEach(userBio -> {
						        	
									if(userBio.getType().equalsIgnoreCase("FINGERPRINT")) {
										doc.getElementById("Zone2_05BV_Citizen_Fingerprint").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
										//TODO add break statement;
									}
						        });
					        }
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":
						UserAttributeEntity userAttributee = userEntity.getUserAttribute();						
						final MrzTD1 rr = new MrzTD1();
				        rr.code2 = 'I';
				        rr.issuingCountry = userAttributee.getNationality().toUpperCase();
				        rr.nationality = userAttributee.getNationality().toUpperCase();
				        rr.optional = "";   rr.optional2 = "";
				        Calendar cal_dobb = Calendar.getInstance();
				        cal_dobb.setTime(Date.from(userAttributee.getDateOfBirth()));
				        Calendar cal_expp = Calendar.getInstance();
				        
						rr.documentNumber = userVoterIDEntity.getCurp();
						cal_expp.setTime(userVoterIDEntity.getDateOfExpiry() == null? new Date() : Date.from(userVoterIDEntity.getDateOfExpiry()));
						
						rr.expirationDate = new MrzDate(cal_expp.get(Calendar.YEAR)%100, cal_expp.get(Calendar.MONTH)+1,cal_expp.get(Calendar.DAY_OF_MONTH) );
				        rr.dateOfBirth = new MrzDate(cal_dobb.get(Calendar.YEAR)%100, cal_dobb.get(Calendar.MONTH)+1,cal_dobb.get(Calendar.DAY_OF_MONTH) );
				        if(userAttributee.getGender().toLowerCase().contains("m")) {
				        	rr.sex = MrzSex.Male;
				        }else 
				        if(userAttributee.getGender().toLowerCase().contains("f")) {
				        	rr.sex = MrzSex.Female;
				        }else {
				        	rr.sex = MrzSex.Male;
				        }
				        rr.surname = userAttributee.getLastName();
				        rr.givenNames = userAttributee.getFirstName();
				        doc.getElementById("Zone3_MRZ_TD1_L1").setTextContent(rr.toMrz_line1());
				        doc.getElementById("Zone3_MRZ_TD1_L2").setTextContent(rr.toMrz_line2());
				        doc.getElementById("Zone3_MRZ_TD1_L3").setTextContent(rr.toMrz_line3());
				        String idNumber = "XXXXXXXXXXXX";
						if(userVoterIDEntity.getCurp() != null) {
							idNumber = userVoterIDEntity.getCurp();
						}
						pdf417_text.append("Name: "+userAttributee.getLastName()+" "+userAttributee.getFirstName());
						pdf417_text.append(", Curp: "+ userVoterIDEntity.getCurp());
						BufferedImage pdf417 = zxingBarcodeGeneratorService.generatePDF417BarcodeImage(pdf417_text.toString(),Integer.valueOf(doc.getElementById("Zone2_01BV_PDF417").getAttribute("width")),Integer.valueOf(doc.getElementById("Zone2_01BV_PDF417").getAttribute("height")));
						ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
						ImageIO.write(pdf417, "PNG", pngOutputStream);					
						byte[] pdf417_bytes = pngOutputStream.toByteArray(); 
						pngOutputStream.close();
						
						doc.getElementById("Zone2_01BV_PDF417").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder()
								.encodeToString(pdf417_bytes));
				        doc.getElementById("Zone2_02BV_QR")
						.setAttributeNS(XLINK_NS, XLINK_HREF,
								"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
										organizationEntity.getName() + "#" + userEntity.getId() + "#" + credentialTemplateID + "#" + idNumber,
										BarcodeFormat.QR_CODE, 300, 300))));

				        userEntity.getUserBiometrics().stream().forEach(userBio -> {

							if(userBio.getType().equalsIgnoreCase("SIGNATURE")) {
									doc.getElementById("Zone2_04BV_Citizen_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
							}
				        });
				        if(doc.getElementById("Zone2_05BV_Citizen_Fingerprint") != null ) {
					        userEntity.getUserBiometrics().stream().forEach(userBio -> {
					        	
								if(userBio.getType().equalsIgnoreCase("FINGERPRINT")) {
									doc.getElementById("Zone2_05BV_Citizen_Fingerprint").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									
									//TODO add break statement;
								}
					        });
				        }
						break;
					default :
				
				}
				
//				doc.getElementById("background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + userNID.getUserNIDBack().getBackground());
							
				logger.debug("parseVIDBack: Ended dynamic Variable");
				
				return doc;
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}
	
}
