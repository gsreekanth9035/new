package com.meetidentity.usermanagement.domain.visual.credentials.policeid;

import java.io.Serializable;

public class PoliceIDTemplate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PoliceIdTemplateFront policeIdTemplateFront;
	private PoliceIdTemplateBack policeIdTemplateBack;
	
	
	public PoliceIdTemplateFront getPoliceIdTemplateFront() {
		return policeIdTemplateFront;
	}
	public void setPoliceIdTemplateFront(PoliceIdTemplateFront policeIdTemplateFront) {
		this.policeIdTemplateFront = policeIdTemplateFront;
	}
	public PoliceIdTemplateBack getPoliceIdTemplateBack() {
		return policeIdTemplateBack;
	}
	public void setPoliceIdTemplateBack(PoliceIdTemplateBack policeIdTemplateBack) {
		this.policeIdTemplateBack = policeIdTemplateBack;
	}
	
	
}
