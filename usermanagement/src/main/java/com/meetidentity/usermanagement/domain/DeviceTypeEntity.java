package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "device_type")
public class DeviceTypeEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "active")
	private Boolean active;
	
	@Column(name = "visual_applicable")
	private Boolean visualApplicable;
	
	@OneToMany(mappedBy = "deviceType", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<DeviceTypeActionsMappingEntity> deviceTypeActions = new ArrayList<>();

	@OneToMany(mappedBy = "deviceType", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<RoleDeviceTypeEntity> roleDeviceTypes = new ArrayList<>();

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<DeviceTypeActionsMappingEntity> getDeviceTypeActions() {
		return deviceTypeActions;
	}

	public void setDeviceTypeActions(List<DeviceTypeActionsMappingEntity> deviceTypeActions) {
		this.deviceTypeActions = deviceTypeActions;
	}

	public List<RoleDeviceTypeEntity> getRoleDeviceTypes() {
		return roleDeviceTypes;
	}

	public void setRoleDeviceTypes(List<RoleDeviceTypeEntity> roleDeviceTypes) {
		this.roleDeviceTypes = roleDeviceTypes;
	}

	public Boolean getVisualApplicable() {
		return visualApplicable;
	}

	public void setVisualApplicable(Boolean visualApplicable) {
		this.visualApplicable = visualApplicable;
	}

}
