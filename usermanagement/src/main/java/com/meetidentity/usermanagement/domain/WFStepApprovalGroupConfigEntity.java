package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "wf_step_approval_group_config")
public class WFStepApprovalGroupConfigEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "wf_step_id")
	private WFStepEntity wfStep;

    @Column(name="reference_approval_group_id")
	private String referenceApprovalGroupId;
    
    @Column(name="reference_approval_group_name")
	private String referenceApprovalGroupName;
    
	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WFStepEntity getWfStep() {
		return wfStep;
	}

	public void setWfStep(WFStepEntity wfStep) {
		this.wfStep = wfStep;
	}

	public String getReferenceApprovalGroupId() {
		return referenceApprovalGroupId;
	}

	public void setReferenceApprovalGroupId(String referenceApprovalGroupId) {
		this.referenceApprovalGroupId = referenceApprovalGroupId;
	}

	public String getReferenceApprovalGroupName() {
		return referenceApprovalGroupName;
	}

	public void setReferenceApprovalGroupName(String referenceApprovalGroupName) {
		this.referenceApprovalGroupName = referenceApprovalGroupName;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
