package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "workflow")
public class WorkflowEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "allowed_devices")
	private Integer allowedDevices;

	@Column(name = "expiration_in_months")
	private Integer expirationInMonths;

	@Column(name = "expiration_in_days")
	private Integer expirationInDays;

	@Column(name = "status")
	private String status;

	@Column(name = "disable_expiration_enforcement")
	private Boolean disableExpirationEnforcement;

	@Column(name = "revoke_encryption_certificate")
	private Boolean revokeEncryptionCertificate;

	@Column(name = "required_enrollment")
	private Boolean requiredEnrollment;

	@Column(name = "required_token_card_issuance")
	private Boolean requiredTokenCardIssuance;

	@Column(name = "required_mobile_issuance")
	private Boolean requiredMobileIssuance;

	@Version
	@Column(name = "version")
	private int version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
	private OrganizationEntity organization;

	@OneToOne(mappedBy = "workflow")
	private RoleEntity role;

	@OneToMany(mappedBy = "workflow", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WFDeviceProfileEntity> wfDeviceProfiles = new ArrayList<>();

	@OneToMany(mappedBy = "workflow", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WFGroupEntity> wfGroups = new ArrayList<>();

	@OneToMany(mappedBy = "workflow", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WFStepEntity> wfSteps = new ArrayList<>();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "org_identity_type_id")
	private OrganizationIdentitiesEntity orgIdentityType;

	@OneToMany(mappedBy = "workflow", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WorkflowAttributeEntity> workflowAttributes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getAllowedDevices() {
		return allowedDevices;
	}

	public void setAllowedDevices(Integer allowedDevices) {
		this.allowedDevices = allowedDevices;
	}

	public Integer getExpirationInMonths() {
		return expirationInMonths;
	}

	public void setExpirationInMonths(Integer expirationInMonths) {
		this.expirationInMonths = expirationInMonths;
	}

	public Integer getExpirationInDays() {
		return expirationInDays;
	}

	public void setExpirationInDays(Integer expirationInDays) {
		this.expirationInDays = expirationInDays;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

	public RoleEntity getRole() {
		return role;
	}

	public void setRole(RoleEntity role) {
		this.role = role;
	}

	public List<WFDeviceProfileEntity> getWfDeviceProfiles() {
		return wfDeviceProfiles;
	}

	public void setWfDeviceProfiles(List<WFDeviceProfileEntity> wfDeviceProfiles) {
		this.wfDeviceProfiles = wfDeviceProfiles;
	}

	public List<WFGroupEntity> getWfGroups() {
		return wfGroups;
	}

	public void setWfGroups(List<WFGroupEntity> wfGroups) {
		this.wfGroups = wfGroups;
	}

	public List<WFStepEntity> getWfSteps() {
		return wfSteps;
	}

	public void setWfSteps(List<WFStepEntity> wfSteps) {
		this.wfSteps = wfSteps;
	}

	public void addWfDeviceProfile(WFDeviceProfileEntity wfDeviceProfileEntity) {
		wfDeviceProfileEntity.setWorkflow(this);
		this.wfDeviceProfiles.add(wfDeviceProfileEntity);
	}

	public void removeWfDeviceProfile(WFDeviceProfileEntity wfDeviceProfileEntity) {
		wfDeviceProfileEntity.setWorkflow(null);
		this.wfDeviceProfiles.remove(wfDeviceProfileEntity);
	}

	public void addWfGroup(WFGroupEntity wfGroupEntity) {
		wfGroupEntity.setWorkflow(this);
		this.wfGroups.add(wfGroupEntity);
	}

	public void removeWfGroup(WFGroupEntity wfGroupEntity) {
		wfGroupEntity.setWorkflow(null);
		this.wfGroups.remove(wfGroupEntity);
	}

	public void addWfStep(WFStepEntity wfStepEntity) {
		wfStepEntity.setWorkflow(this);
		this.wfSteps.add(wfStepEntity);
	}

	public void removeWfStep(WFStepEntity wfStepEntity) {
		wfStepEntity.setWorkflow(null);
		this.wfSteps.remove(wfStepEntity);
	}

	public Boolean getDisableExpirationEnforcement() {
		return disableExpirationEnforcement;
	}

	public void setDisableExpirationEnforcement(Boolean disableExpirationEnforcement) {
		this.disableExpirationEnforcement = disableExpirationEnforcement;
	}

	public Boolean getRevokeEncryptionCertificate() {
		return revokeEncryptionCertificate;
	}

	public void setRevokeEncryptionCertificate(Boolean revokeEncryptionCertificate) {
		this.revokeEncryptionCertificate = revokeEncryptionCertificate;
	}

	public OrganizationIdentitiesEntity getOrgIdentityType() {
		return orgIdentityType;
	}

	public void setOrgIdentityType(OrganizationIdentitiesEntity orgIdentityType) {
		this.orgIdentityType = orgIdentityType;
	}

	public Boolean getRequiredEnrollment() {
		return requiredEnrollment;
	}

	public void setRequiredEnrollment(Boolean requiredEnrollment) {
		this.requiredEnrollment = requiredEnrollment;
	}

	public Boolean getRequiredTokenCardIssuance() {
		return requiredTokenCardIssuance;
	}

	public void setRequiredTokenCardIssuance(Boolean requiredTokenCardIssuance) {
		this.requiredTokenCardIssuance = requiredTokenCardIssuance;
	}

	public Boolean getRequiredMobileIssuance() {
		return requiredMobileIssuance;
	}

	public void setRequiredMobileIssuance(Boolean requiredMobileIssuance) {
		this.requiredMobileIssuance = requiredMobileIssuance;
	}

	public List<WorkflowAttributeEntity> getWorkflowAttributes() {
		return workflowAttributes;
	}

	public void setWorkflowAttributes(List<WorkflowAttributeEntity> workflowAttributes) {
		this.workflowAttributes = workflowAttributes;
	}

}
