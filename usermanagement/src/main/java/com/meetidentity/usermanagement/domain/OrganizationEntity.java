package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.meetidentity.usermanagement.annotations.SequenceValue;

@Entity
@Table(name="organization")
public class OrganizationEntity  extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
	@SequenceValue
	@Column(name="uims_id")
	private String uimsId;
	
    @Column(name="name")
	private String name;
    
    @Column(name="description")
	private String description;
    
    @Column(name="abbreviation")
	private String abbreviation;
    
    @Column(name="display_name")
	private String displayName;
    
    @Column(name="industry", nullable = false) //Organization Category
    private String industry; //mandatory 
    
    @Column(name="issuer_identification_number") // orgIdentifier 
    private String issuerIdentificationNumber; //mandatory
    
    @Column(name="agency_code") //orgDeptCd
    private String agencyCode;  //optional

    @Column(name="system_code") //
    private String systemCode;  //
    
    @Column(name="issuing_authority") //issuingOrg 
    private String issuingAuthority;  //optional
    
    @Column(name="address_line1") // optional
    private String addressLine1;
    
    @Column(name="address_line2") // optional
    private String addressLine2 ;
    
    /*    @Column(name="solution") 
    private String solution; //mandatory
   
    @Column(name="document_type") 
    private String  documentType; //mandatory    
     
    @Column(name="agency_card_serial_number") 
    private String agencyCardSerialNumber; // optional
     
    @Column(name="employee_affiliation")  //Groups 
    private String employeeAffiliation; // optional
    
    @Column(name="employee_status")  //Rank/Grade 
    private String employeeStatus; // optional 
    
    @Column(name="color_coding_for_employee_affiliation") //mandatory
    private String colorCodingForEmployeeAffiliation;
    
    @Column(name="organization") //mandatory
    private String organization;
    
     @Column(name="organizational_affiliation_abbreviation") // optional
    private String organizationalAffiliationAbbreviation;
    
    @Column(name="site_cd")
    private String siteCd;
    
   */
    @Column(name="alt_name")
  	private String altName;
     
    @Lob
    @Column(name="login_logo")
  	private byte[] loginLogo;     
   
    @Column(name="login_logo_width")
  	private String loginLogoWidth;  
    
    @Column(name="login_logo_height")
  	private String loginLogoHeight;  
    
    @Lob
    @Column(name="sidebar_logo")
 	private byte[] sidebarLogo;    
   
    @Column(name="sidebar_logo_width")
  	private String sidebarLogoWidth;     
   
    @Column(name="sidebar_logo_height")
  	private String sidebarLogoHeight;     
   
    @Lob
    @Column(name="fav_icon")
 	private byte[] favIcon; 
    
    @Column(name="primary_color_code")
  	private String primaryColorCode;    
    
    @Column(name="secondary_color_code")
  	private String secondaryColorCode;    

    @Column(name="tertiary_color_code")
  	private String tertiaryColorCode;   

    @Column(name="quaternary_color_code")
  	private String quaternaryColorCode;       

    @Column(name="quinary_color_code")
  	private String quinaryColorCode;   
    
    @Column(name="privacy_policy")
  	private String privacyPolicy;
    
    @Column(name="hide_service_name")
  	private boolean hideServiceName;
   
    @Column(name="enable_scheduler_in_login_screen")
  	private Boolean enableSchedulerInLoginScreen;

    @Version
    @Column(name="version")
    private int version;
    
    @OneToMany(mappedBy="organization", fetch = FetchType.LAZY)
    private List<UserEntity> users = new ArrayList<>();
    
    @OneToMany(mappedBy="organization", fetch = FetchType.LAZY)
    private List<RoleEntity> roles = new ArrayList<>();

    @OneToMany(mappedBy="organization", fetch = FetchType.LAZY)
    private List<ConfigEntity> configs = new ArrayList<>();
    
    @OneToMany(mappedBy="organization", fetch = FetchType.LAZY)
    private List<WorkflowEntity> workflows = new ArrayList<>();
    
    @OneToMany(mappedBy="organization", fetch = FetchType.LAZY)
    private List<OrganizationIdentitiesEntity> organizationIdentities = new ArrayList<>();
    
    @OneToMany(mappedBy="organization", fetch = FetchType.LAZY)
    private List<FingerTypeEntity> FingerTypes = new ArrayList<>();
    
    @OneToMany(mappedBy="organization")
    private List<CardholderRankEntity> cardholderRanks = new ArrayList<>();
    
    @OneToMany(mappedBy="organization")
    private List<EmployeeAffiliationEntity> employeeAffiliationTypes = new ArrayList<>();
    
    @OneToMany(mappedBy="organization")
    private List<PhysicalCCEyeEntity> physicalccEyeColors = new ArrayList<>();
    
    @OneToMany(mappedBy="organization")
    private List<PhysicalCCHairEntity> physicalccHairColors = new ArrayList<>();
    
    @OneToMany(mappedBy="organization")
    private List<PhysicalCCHeightEntity> physicalccHeight = new ArrayList<>();

    @OneToOne(mappedBy="organization")
    private OrganizationEmailConfigEntity emailConfig = new OrganizationEmailConfigEntity();
    
    @ManyToOne()
	@JoinColumn(name = "organization_branding_policy_config_id")
    private OrganizationBrandingPolicyConfigEntity organizationBrandingPolicyConfig;
    
	@OneToMany(mappedBy = "organization", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WorkflowAttributeEntity> workflowAttributes = new ArrayList<>();
	
	@OneToMany(mappedBy = "organization", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<RoleAttributeEntity> roleAttributes = new ArrayList<>();
	
	@OneToMany(mappedBy = "organization", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<GroupAttributeEntity> groupAttributes = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getAltName() {
		return altName;
	}

	public void setAltName(String altName) {
		this.altName = altName;
	}

	public byte[] getLoginLogo() {
		return loginLogo;
	}

	public void setLoginLogo(byte[] loginLogo) {
		this.loginLogo = loginLogo;
	}

	public void setSidebarLogo(byte[] sidebarLogo) {
		this.sidebarLogo = sidebarLogo;
	}
	
	public byte[] getSidebarLogo() {
		return sidebarLogo;
	}	

	public String getLoginLogoWidth() {
		return loginLogoWidth;
	}

	public void setLoginLogoWidth(String loginLogoWidth) {
		this.loginLogoWidth = loginLogoWidth;
	}

	public String getLoginLogoHeight() {
		return loginLogoHeight;
	}

	public void setLoginLogoHeight(String loginLogoHeight) {
		this.loginLogoHeight = loginLogoHeight;
	}

	public String getSidebarLogoWidth() {
		return sidebarLogoWidth;
	}

	public void setSidebarLogoWidth(String sidebarLogoWidth) {
		this.sidebarLogoWidth = sidebarLogoWidth;
	}

	public String getSidebarLogoHeight() {
		return sidebarLogoHeight;
	}

	public void setSidebarLogoHeight(String sidebarLogoHeight) {
		this.sidebarLogoHeight = sidebarLogoHeight;
	}

	public String getPrimaryColorCode() {
		return primaryColorCode;
	}

	public void setPrimaryColorCode(String primaryColorCode) {
		this.primaryColorCode = primaryColorCode;
	}

	public String getSecondaryColorCode() {
		return secondaryColorCode;
	}

	public void setSecondaryColorCode(String secondaryColorCode) {
		this.secondaryColorCode = secondaryColorCode;
	}

	public String getTertiaryColorCode() {
		return tertiaryColorCode;
	}

	public void setTertiaryColorCode(String tertiaryColorCode) {
		this.tertiaryColorCode = tertiaryColorCode;
	}

	public String getQuaternaryColorCode() {
		return quaternaryColorCode;
	}

	public void setQuaternaryColorCode(String quaternaryColorCode) {
		this.quaternaryColorCode = quaternaryColorCode;
	}

	public String getQuinaryColorCode() {
		return quinaryColorCode;
	}

	public void setQuinaryColorCode(String quinaryColorCode) {
		this.quinaryColorCode = quinaryColorCode;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<UserEntity> getUsers() {
		return users;
	}

	public void setUsers(List<UserEntity> users) {
		this.users = users;
	}

	public List<RoleEntity> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleEntity> roles) {
		this.roles = roles;
	}

	public List<ConfigEntity> getConfigs() {
		return configs;
	}

	public void setConfigs(List<ConfigEntity> configs) {
		this.configs = configs;
	}

	public List<WorkflowEntity> getWorkflows() {
		return workflows;
	}

	public void setWorkflows(List<WorkflowEntity> workflows) {
		this.workflows = workflows;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getIssuerIdentificationNumber() {
		return issuerIdentificationNumber;
	}

	public void setIssuerIdentificationNumber(String issuerIdentificationNumber) {
		this.issuerIdentificationNumber = issuerIdentificationNumber;
	}

	public String getAgencyCode() {
		return agencyCode;
	}

	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}

	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public List<OrganizationIdentitiesEntity> getOrganizationIdentities() {
		return organizationIdentities;
	}

	public void setOrganizationIdentities(List<OrganizationIdentitiesEntity> organizationIdentities) {
		this.organizationIdentities = organizationIdentities;
	}

	public List<FingerTypeEntity> getFingerTypes() {
		return FingerTypes;
	}

	public void setFingerTypes(List<FingerTypeEntity> fingerTypes) {
		FingerTypes = fingerTypes;
	}

	public List<CardholderRankEntity> getCardholderRanks() {
		return cardholderRanks;
	}

	public void setCardholderRanks(List<CardholderRankEntity> cardholderRanks) {
		this.cardholderRanks = cardholderRanks;
	}

	public List<EmployeeAffiliationEntity> getEmployeeAffiliationTypes() {
		return employeeAffiliationTypes;
	}

	public void setEmployeeAffiliationTypes(List<EmployeeAffiliationEntity> employeeAffiliationTypes) {
		this.employeeAffiliationTypes = employeeAffiliationTypes;
	}

	public List<PhysicalCCEyeEntity> getPhysicalccEyeColors() {
		return physicalccEyeColors;
	}

	public void setPhysicalccEyeColors(List<PhysicalCCEyeEntity> physicalccEyeColors) {
		this.physicalccEyeColors = physicalccEyeColors;
	}

	public List<PhysicalCCHairEntity> getPhysicalccHairColors() {
		return physicalccHairColors;
	}

	public void setPhysicalccHairColors(List<PhysicalCCHairEntity> physicalccHairColors) {
		this.physicalccHairColors = physicalccHairColors;
	}

	public List<PhysicalCCHeightEntity> getPhysicalccHeight() {
		return physicalccHeight;
	}

	public void setPhysicalccHeight(List<PhysicalCCHeightEntity> physicalccHeight) {
		this.physicalccHeight = physicalccHeight;
	}



	public String getPrivacyPolicy() {
		return privacyPolicy;
	}

	public void setPrivacyPolicy(String privacyPolicy) {
		this.privacyPolicy = privacyPolicy;
	}

	public OrganizationEmailConfigEntity getEmailConfig() {
		return emailConfig;
	}

	public void setEmailConfig(OrganizationEmailConfigEntity emailConfig) {
		this.emailConfig = emailConfig;
	}

	public boolean isHideServiceName() {
		return hideServiceName;
	}

	public void setHideServiceName(boolean hideServiceName) {
		this.hideServiceName = hideServiceName;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public Boolean getEnableSchedulerInLoginScreen() {
		return enableSchedulerInLoginScreen;
	}

	public void setEnableSchedulerInLoginScreen(Boolean enableSchedulerInLoginScreen) {
		this.enableSchedulerInLoginScreen = enableSchedulerInLoginScreen;
	}

	public byte[] getFavIcon() {
		return favIcon;
	}

	public void setFavIcon(byte[] favIcon) {
		this.favIcon = favIcon;
	}

	public OrganizationBrandingPolicyConfigEntity getOrganizationBrandingPolicyConfig() {
		return organizationBrandingPolicyConfig;
	}

	public void setOrganizationBrandingPolicyConfig(
			OrganizationBrandingPolicyConfigEntity organizationBrandingPolicyConfig) {
		this.organizationBrandingPolicyConfig = organizationBrandingPolicyConfig;
	}

	public List<WorkflowAttributeEntity> getWorkflowAttributes() {
		return workflowAttributes;
	}

	public void setWorkflowAttributes(List<WorkflowAttributeEntity> workflowAttributes) {
		this.workflowAttributes = workflowAttributes;
	}

	public List<RoleAttributeEntity> getRoleAttributes() {
		return roleAttributes;
	}

	public void setRoleAttributes(List<RoleAttributeEntity> roleAttributes) {
		this.roleAttributes = roleAttributes;
	}

	public List<GroupAttributeEntity> getGroupAttributes() {
		return groupAttributes;
	}

	public void setGroupAttributes(List<GroupAttributeEntity> groupAttributes) {
		this.groupAttributes = groupAttributes;
	}

}
