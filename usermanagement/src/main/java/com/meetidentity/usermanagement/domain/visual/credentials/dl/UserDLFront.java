package com.meetidentity.usermanagement.domain.visual.credentials.dl;

import java.io.Serializable;

public class UserDLFront implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String Zone0_FS_Background;
	private String Zone1_FS_Agency_seal;
	private String Zone3_FV_Photograph;
	
	private String Zone2_10FS_Sex_Alt;
	private String Zone2_9FS_VClass_Alt;
	private String Zone2_5FS_DD_Alt;
	private String Zone2_4dFS_DLNumber_Alt;
	private String Zone2_4bFS_DOE_Alt;
	private String Zone2_4aFS_DOI_Alt;
	private String Zone2_3aFS_POB_Alt;
	private String Zone2_3FS_DOB_Alt;
	private String Zone1_FS_Doc_Type_Alt;

	private String Zone2_5FS_DD_Eng;
	private String Zone2_10FS_Sex_Eng;
	private String Zone2_9FS_VClass_Eng;
	private String Zone2_4dFS_DLNumber_Eng;
	private String Zone2_4bFS_DOE_Eng;
	private String Zone2_4aFS_DOI_Eng;
	private String Zone2_3aFS_POB_Eng;
	private String Zone2_3FS_DOB_Eng;
	private String Zone1_FS_Doc_Type_Eng;


	private String Zone2_FS_Name	;
	private String Zone1_FV_Issuing_Org;


	private String Zone2_4dFV_DLNumber;
	private String Zone2_1FV_First_Middle_Name;
	private String Zone2_2FV_Last_Name;
	private String Zone2_9FV_VClass;
	private String Zone2_3aFV_POB;
	private String Zone2_15FV_Sex;
	private String Zone2_4aFV_DOI;
	private String Zone2_4bFV_DOE;
	private String Zone2_3FV_DOB;
	private String Zone2_5FV_DD	;

	private String Zone2_3FV_DOB_2	;
	private String Zone2_4dFV_DLNumber_2;
	private String Zone3_FV_Signature;
	private String Zone3_FV_Signature_2;
	public String getZone0_FS_Background() {
		return Zone0_FS_Background;
	}
	public void setZone0_FS_Background(String zone0_FS_Background) {
		Zone0_FS_Background = zone0_FS_Background;
	}
	public String getZone1_FS_Agency_seal() {
		return Zone1_FS_Agency_seal;
	}
	public void setZone1_FS_Agency_seal(String zone1_FS_Agency_seal) {
		Zone1_FS_Agency_seal = zone1_FS_Agency_seal;
	}
	public String getZone3_FV_Photograph() {
		return Zone3_FV_Photograph;
	}
	public void setZone3_FV_Photograph(String zone3_FV_Photograph) {
		Zone3_FV_Photograph = zone3_FV_Photograph;
	}
	public String getZone2_10FS_Sex_Alt() {
		return Zone2_10FS_Sex_Alt;
	}
	public void setZone2_10FS_Sex_Alt(String zone2_10fs_Sex_Alt) {
		Zone2_10FS_Sex_Alt = zone2_10fs_Sex_Alt;
	}
	public String getZone2_9FS_VClass_Alt() {
		return Zone2_9FS_VClass_Alt;
	}
	public void setZone2_9FS_VClass_Alt(String zone2_9fs_VClass_Alt) {
		Zone2_9FS_VClass_Alt = zone2_9fs_VClass_Alt;
	}
	public String getZone2_5FS_DD_Alt() {
		return Zone2_5FS_DD_Alt;
	}
	public void setZone2_5FS_DD_Alt(String zone2_5fs_DD_Alt) {
		Zone2_5FS_DD_Alt = zone2_5fs_DD_Alt;
	}
	public String getZone2_4dFS_DLNumber_Alt() {
		return Zone2_4dFS_DLNumber_Alt;
	}
	public void setZone2_4dFS_DLNumber_Alt(String zone2_4dFS_DLNumber_Alt) {
		Zone2_4dFS_DLNumber_Alt = zone2_4dFS_DLNumber_Alt;
	}
	public String getZone2_4bFS_DOE_Alt() {
		return Zone2_4bFS_DOE_Alt;
	}
	public void setZone2_4bFS_DOE_Alt(String zone2_4bFS_DOE_Alt) {
		Zone2_4bFS_DOE_Alt = zone2_4bFS_DOE_Alt;
	}
	public String getZone2_4aFS_DOI_Alt() {
		return Zone2_4aFS_DOI_Alt;
	}
	public void setZone2_4aFS_DOI_Alt(String zone2_4aFS_DOI_Alt) {
		Zone2_4aFS_DOI_Alt = zone2_4aFS_DOI_Alt;
	}
	public String getZone2_3aFS_POB_Alt() {
		return Zone2_3aFS_POB_Alt;
	}
	public void setZone2_3aFS_POB_Alt(String zone2_3aFS_POB_Alt) {
		Zone2_3aFS_POB_Alt = zone2_3aFS_POB_Alt;
	}
	public String getZone2_3FS_DOB_Alt() {
		return Zone2_3FS_DOB_Alt;
	}
	public void setZone2_3FS_DOB_Alt(String zone2_3fs_DOB_Alt) {
		Zone2_3FS_DOB_Alt = zone2_3fs_DOB_Alt;
	}
	public String getZone1_FS_Doc_Type_Alt() {
		return Zone1_FS_Doc_Type_Alt;
	}
	public void setZone1_FS_Doc_Type_Alt(String zone1_FS_Doc_Type_Alt) {
		Zone1_FS_Doc_Type_Alt = zone1_FS_Doc_Type_Alt;
	}
	public String getZone2_5FS_DD_Eng() {
		return Zone2_5FS_DD_Eng;
	}
	public void setZone2_5FS_DD_Eng(String zone2_5fs_DD_Eng) {
		Zone2_5FS_DD_Eng = zone2_5fs_DD_Eng;
	}
	public String getZone2_10FS_Sex_Eng() {
		return Zone2_10FS_Sex_Eng;
	}
	public void setZone2_10FS_Sex_Eng(String zone2_10fs_Sex_Eng) {
		Zone2_10FS_Sex_Eng = zone2_10fs_Sex_Eng;
	}
	public String getZone2_9FS_VClass_Eng() {
		return Zone2_9FS_VClass_Eng;
	}
	public void setZone2_9FS_VClass_Eng(String zone2_9fs_VClass_Eng) {
		Zone2_9FS_VClass_Eng = zone2_9fs_VClass_Eng;
	}
	public String getZone2_4dFS_DLNumber_Eng() {
		return Zone2_4dFS_DLNumber_Eng;
	}
	public void setZone2_4dFS_DLNumber_Eng(String zone2_4dFS_DLNumber_Eng) {
		Zone2_4dFS_DLNumber_Eng = zone2_4dFS_DLNumber_Eng;
	}
	public String getZone2_4bFS_DOE_Eng() {
		return Zone2_4bFS_DOE_Eng;
	}
	public void setZone2_4bFS_DOE_Eng(String zone2_4bFS_DOE_Eng) {
		Zone2_4bFS_DOE_Eng = zone2_4bFS_DOE_Eng;
	}
	public String getZone2_4aFS_DOI_Eng() {
		return Zone2_4aFS_DOI_Eng;
	}
	public void setZone2_4aFS_DOI_Eng(String zone2_4aFS_DOI_Eng) {
		Zone2_4aFS_DOI_Eng = zone2_4aFS_DOI_Eng;
	}
	public String getZone2_3aFS_POB_Eng() {
		return Zone2_3aFS_POB_Eng;
	}
	public void setZone2_3aFS_POB_Eng(String zone2_3aFS_POB_Eng) {
		Zone2_3aFS_POB_Eng = zone2_3aFS_POB_Eng;
	}
	public String getZone2_3FS_DOB_Eng() {
		return Zone2_3FS_DOB_Eng;
	}
	public void setZone2_3FS_DOB_Eng(String zone2_3fs_DOB_Eng) {
		Zone2_3FS_DOB_Eng = zone2_3fs_DOB_Eng;
	}
	public String getZone1_FS_Doc_Type_Eng() {
		return Zone1_FS_Doc_Type_Eng;
	}
	public void setZone1_FS_Doc_Type_Eng(String zone1_FS_Doc_Type_Eng) {
		Zone1_FS_Doc_Type_Eng = zone1_FS_Doc_Type_Eng;
	}
	public String getZone2_FS_Name() {
		return Zone2_FS_Name;
	}
	public void setZone2_FS_Name(String zone2_FS_Name) {
		Zone2_FS_Name = zone2_FS_Name;
	}
	public String getZone1_FV_Issuing_Org() {
		return Zone1_FV_Issuing_Org;
	}
	public void setZone1_FV_Issuing_Org(String zone1_FV_Issuing_Org) {
		Zone1_FV_Issuing_Org = zone1_FV_Issuing_Org;
	}
	public String getZone2_4dFV_DLNumber() {
		return Zone2_4dFV_DLNumber;
	}
	public void setZone2_4dFV_DLNumber(String zone2_4dFV_DLNumber) {
		Zone2_4dFV_DLNumber = zone2_4dFV_DLNumber;
	}
	public String getZone2_1FV_First_Middle_Name() {
		return Zone2_1FV_First_Middle_Name;
	}
	public void setZone2_1FV_First_Middle_Name(String zone2_1fv_First_Middle_Name) {
		Zone2_1FV_First_Middle_Name = zone2_1fv_First_Middle_Name;
	}
	public String getZone2_2FV_Last_Name() {
		return Zone2_2FV_Last_Name;
	}
	public void setZone2_2FV_Last_Name(String zone2_2fv_Last_Name) {
		Zone2_2FV_Last_Name = zone2_2fv_Last_Name;
	}
	public String getZone2_9FV_VClass() {
		return Zone2_9FV_VClass;
	}
	public void setZone2_9FV_VClass(String zone2_9fv_VClass) {
		Zone2_9FV_VClass = zone2_9fv_VClass;
	}
	public String getZone2_3aFV_POB() {
		return Zone2_3aFV_POB;
	}
	public void setZone2_3aFV_POB(String zone2_3aFV_POB) {
		Zone2_3aFV_POB = zone2_3aFV_POB;
	}
	public String getZone2_15FV_Sex() {
		return Zone2_15FV_Sex;
	}
	public void setZone2_15FV_Sex(String zone2_15fv_Sex) {
		Zone2_15FV_Sex = zone2_15fv_Sex;
	}
	public String getZone2_4aFV_DOI() {
		return Zone2_4aFV_DOI;
	}
	public void setZone2_4aFV_DOI(String zone2_4aFV_DOI) {
		Zone2_4aFV_DOI = zone2_4aFV_DOI;
	}
	public String getZone2_4bFV_DOE() {
		return Zone2_4bFV_DOE;
	}
	public void setZone2_4bFV_DOE(String zone2_4bFV_DOE) {
		Zone2_4bFV_DOE = zone2_4bFV_DOE;
	}
	public String getZone2_3FV_DOB() {
		return Zone2_3FV_DOB;
	}
	public void setZone2_3FV_DOB(String zone2_3fv_DOB) {
		Zone2_3FV_DOB = zone2_3fv_DOB;
	}
	public String getZone2_5FV_DD() {
		return Zone2_5FV_DD;
	}
	public void setZone2_5FV_DD(String zone2_5fv_DD) {
		Zone2_5FV_DD = zone2_5fv_DD;
	}
	public String getZone2_3FV_DOB_2() {
		return Zone2_3FV_DOB_2;
	}
	public void setZone2_3FV_DOB_2(String zone2_3fv_DOB_2) {
		Zone2_3FV_DOB_2 = zone2_3fv_DOB_2;
	}
	public String getZone2_4dFV_DLNumber_2() {
		return Zone2_4dFV_DLNumber_2;
	}
	public void setZone2_4dFV_DLNumber_2(String zone2_4dFV_DLNumber_2) {
		Zone2_4dFV_DLNumber_2 = zone2_4dFV_DLNumber_2;
	}
	public String getZone3_FV_Signature() {
		return Zone3_FV_Signature;
	}
	public void setZone3_FV_Signature(String zone3_FV_Signature) {
		Zone3_FV_Signature = zone3_FV_Signature;
	}
	public String getZone3_FV_Signature_2() {
		return Zone3_FV_Signature_2;
	}
	public void setZone3_FV_Signature_2(String zone3_FV_Signature_2) {
		Zone3_FV_Signature_2 = zone3_FV_Signature_2;
	}
	
	
}
