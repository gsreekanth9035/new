package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "wf_step_applet_loading_info")
public class WfStepAppletLoadingInfoEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne
	@JoinColumn(name = "wf_step_id")
	private WFStepEntity wfStep;

	@Column(name = "piv_applet_enabled")
	private Boolean pivAppletEnabled;

	@Column(name = "fido2_applet_enabled")
	private Boolean fido2AppletEnabled;

	@Column(name = "attestation_cert")
	private String attestationCert;

	@Column(name = "attestation_cert_private_key")
	private String attestationCertPrivateKey;

	@Column(name = "aaguid")
	private String aaguid;

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WFStepEntity getWfStep() {
		return wfStep;
	}

	public void setWfStep(WFStepEntity wfStep) {
		this.wfStep = wfStep;
	}

	public Boolean getPivAppletEnabled() {
		return pivAppletEnabled;
	}

	public void setPivAppletEnabled(Boolean pivAppletEnabled) {
		this.pivAppletEnabled = pivAppletEnabled;
	}

	public Boolean getFido2AppletEnabled() {
		return fido2AppletEnabled;
	}

	public void setFido2AppletEnabled(Boolean fido2AppletEnabled) {
		this.fido2AppletEnabled = fido2AppletEnabled;
	}

	public String getAttestationCert() {
		return attestationCert;
	}

	public void setAttestationCert(String attestationCert) {
		this.attestationCert = attestationCert;
	}

	public String getAttestationCertPrivateKey() {
		return attestationCertPrivateKey;
	}

	public void setAttestationCertPrivateKey(String attestationCertPrivateKey) {
		this.attestationCertPrivateKey = attestationCertPrivateKey;
	}

	public String getAaguid() {
		return aaguid;
	}

	public void setAaguid(String aaguid) {
		this.aaguid = aaguid;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
