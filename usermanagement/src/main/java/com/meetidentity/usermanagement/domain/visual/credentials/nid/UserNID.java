package com.meetidentity.usermanagement.domain.visual.credentials.nid;

import java.io.Serializable;

public class UserNID implements Serializable {

	private static final long serialVersionUID = 1L;

	private UserNIDBack userNIDBack;

	private UserNIDFront userNIDFront;

	public void setUserNIDBack(UserNIDBack userNIDBack) {
		this.userNIDBack = userNIDBack;
	}

	public UserNIDBack getUserNIDBack() {
		return this.userNIDBack;
	}

	public void setUserNIDFront(UserNIDFront userNIDFront) {
		this.userNIDFront = userNIDFront;
	}

	public UserNIDFront getUserNIDFront() {
		return this.userNIDFront;
	}

}
