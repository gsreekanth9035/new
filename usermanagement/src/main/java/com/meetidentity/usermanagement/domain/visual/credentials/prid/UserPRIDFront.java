package com.meetidentity.usermanagement.domain.visual.credentials.prid;

import java.io.Serializable;

public class UserPRIDFront implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String Zone0_FS_Background;
	private String Zone1_FS_Agency_seal;
	private String Zone1_FS_Issuing_Org;

	private String Zone2_01FS_Name_Eng;
	private String Zone2_02FS_Nationality_Eng;
	private String Zone2_03FS_BirthPlace_Eng;
	private String Zone2_04FS_DocNum_Eng;
	private String Zone2_05FS_IDNum_Eng;
	private String Zone2_06FS_DOI_Eng;
	private String Zone2_07FS_DOE_Eng;
	private String Zone2_08FS_Passport_Eng;
	private String Zone2_09FS_Sex_Eng;
	private String Zone2_10FS_Residentcard_Type_Eng;
	private String Zone2_13FS_Designation_Eng;
	private String Zone3_01FS_Signature_Eng;
	private String Zone3_02FS_DOB_Eng;

	private String Zone2_01FS_Name_Alt;
	private String Zone2_02FS_Nationality_Alt;
	private String Zone2_03FS_BirthPlace_Alt;
	private String Zone2_04FS_DocNum_Alt;
	private String Zone2_05FS_IDNum_Alt;
	private String Zone2_06FS_DOI_Alt;
	private String Zone2_07FS_DOE_Alt;
	private String Zone2_08FS_Passport_Alt;
	private String Zone2_09FS_Sex_Alt;
	private String Zone2_10FS_Residentcard_Type_Alt;
	private String Zone2_13FS_Designation_Alt;
	private String Zone3_01FS_Signature_Alt;
	private String Zone3_02FS_DOB_Alt;


	private String Zone2_01FV_Firstname_MiddleName;
	private String Zone2_02FV_Fatherlastname_Motherlastname;
	private String Zone2_03FV_Nationality;
	private String Zone2_04FV_BirthPlace;
	private String Zone2_05FV_DocNum;
	private String Zone2_06FV_IDNum;
	private String Zone2_07FV_DOI;
	private String Zone2_08FV_DOE;
	private String Zone2_09FV_Passport;
	private String Zone2_10FV_Sex;
	private String Zone2_15FV_Photoghost;
	private String Zone2_16FV_Director_Signature;

	private String Zone3_01FV_Signature;
	private String Zone3_02FV_DOB;
	private String Zone3_03FV_Photograph;
	public String getZone0_FS_Background() {
		return Zone0_FS_Background;
	}
	public void setZone0_FS_Background(String zone0_FS_Background) {
		Zone0_FS_Background = zone0_FS_Background;
	}
	public String getZone1_FS_Agency_seal() {
		return Zone1_FS_Agency_seal;
	}
	public void setZone1_FS_Agency_seal(String zone1_FS_Agency_seal) {
		Zone1_FS_Agency_seal = zone1_FS_Agency_seal;
	}
	public String getZone1_FS_Issuing_Org() {
		return Zone1_FS_Issuing_Org;
	}
	public void setZone1_FS_Issuing_Org(String zone1_FS_Issuing_Org) {
		Zone1_FS_Issuing_Org = zone1_FS_Issuing_Org;
	}
	public String getZone2_01FS_Name_Eng() {
		return Zone2_01FS_Name_Eng;
	}
	public void setZone2_01FS_Name_Eng(String zone2_01fs_Name_Eng) {
		Zone2_01FS_Name_Eng = zone2_01fs_Name_Eng;
	}
	public String getZone2_02FS_Nationality_Eng() {
		return Zone2_02FS_Nationality_Eng;
	}
	public void setZone2_02FS_Nationality_Eng(String zone2_02fs_Nationality_Eng) {
		Zone2_02FS_Nationality_Eng = zone2_02fs_Nationality_Eng;
	}
	public String getZone2_03FS_BirthPlace_Eng() {
		return Zone2_03FS_BirthPlace_Eng;
	}
	public void setZone2_03FS_BirthPlace_Eng(String zone2_03fs_BirthPlace_Eng) {
		Zone2_03FS_BirthPlace_Eng = zone2_03fs_BirthPlace_Eng;
	}
	public String getZone2_04FS_DocNum_Eng() {
		return Zone2_04FS_DocNum_Eng;
	}
	public void setZone2_04FS_DocNum_Eng(String zone2_04fs_DocNum_Eng) {
		Zone2_04FS_DocNum_Eng = zone2_04fs_DocNum_Eng;
	}
	public String getZone2_05FS_IDNum_Eng() {
		return Zone2_05FS_IDNum_Eng;
	}
	public void setZone2_05FS_IDNum_Eng(String zone2_05fs_IDNum_Eng) {
		Zone2_05FS_IDNum_Eng = zone2_05fs_IDNum_Eng;
	}
	public String getZone2_06FS_DOI_Eng() {
		return Zone2_06FS_DOI_Eng;
	}
	public void setZone2_06FS_DOI_Eng(String zone2_06fs_DOI_Eng) {
		Zone2_06FS_DOI_Eng = zone2_06fs_DOI_Eng;
	}
	public String getZone2_07FS_DOE_Eng() {
		return Zone2_07FS_DOE_Eng;
	}
	public void setZone2_07FS_DOE_Eng(String zone2_07fs_DOE_Eng) {
		Zone2_07FS_DOE_Eng = zone2_07fs_DOE_Eng;
	}
	public String getZone2_08FS_Passport_Eng() {
		return Zone2_08FS_Passport_Eng;
	}
	public void setZone2_08FS_Passport_Eng(String zone2_08fs_Passport_Eng) {
		Zone2_08FS_Passport_Eng = zone2_08fs_Passport_Eng;
	}
	public String getZone2_09FS_Sex_Eng() {
		return Zone2_09FS_Sex_Eng;
	}
	public void setZone2_09FS_Sex_Eng(String zone2_09fs_Sex_Eng) {
		Zone2_09FS_Sex_Eng = zone2_09fs_Sex_Eng;
	}
	public String getZone2_10FS_Residentcard_Type_Eng() {
		return Zone2_10FS_Residentcard_Type_Eng;
	}
	public void setZone2_10FS_Residentcard_Type_Eng(String zone2_10fs_Residentcard_Type_Eng) {
		Zone2_10FS_Residentcard_Type_Eng = zone2_10fs_Residentcard_Type_Eng;
	}
	public String getZone2_13FS_Designation_Eng() {
		return Zone2_13FS_Designation_Eng;
	}
	public void setZone2_13FS_Designation_Eng(String zone2_13fs_Designation_Eng) {
		Zone2_13FS_Designation_Eng = zone2_13fs_Designation_Eng;
	}
	public String getZone3_01FS_Signature_Eng() {
		return Zone3_01FS_Signature_Eng;
	}
	public void setZone3_01FS_Signature_Eng(String zone3_01fs_Signature_Eng) {
		Zone3_01FS_Signature_Eng = zone3_01fs_Signature_Eng;
	}
	public String getZone3_02FS_DOB_Eng() {
		return Zone3_02FS_DOB_Eng;
	}
	public void setZone3_02FS_DOB_Eng(String zone3_02fs_DOB_Eng) {
		Zone3_02FS_DOB_Eng = zone3_02fs_DOB_Eng;
	}
	public String getZone2_01FS_Name_Alt() {
		return Zone2_01FS_Name_Alt;
	}
	public void setZone2_01FS_Name_Alt(String zone2_01fs_Name_Alt) {
		Zone2_01FS_Name_Alt = zone2_01fs_Name_Alt;
	}
	public String getZone2_02FS_Nationality_Alt() {
		return Zone2_02FS_Nationality_Alt;
	}
	public void setZone2_02FS_Nationality_Alt(String zone2_02fs_Nationality_Alt) {
		Zone2_02FS_Nationality_Alt = zone2_02fs_Nationality_Alt;
	}
	public String getZone2_03FS_BirthPlace_Alt() {
		return Zone2_03FS_BirthPlace_Alt;
	}
	public void setZone2_03FS_BirthPlace_Alt(String zone2_03fs_BirthPlace_Alt) {
		Zone2_03FS_BirthPlace_Alt = zone2_03fs_BirthPlace_Alt;
	}
	public String getZone2_04FS_DocNum_Alt() {
		return Zone2_04FS_DocNum_Alt;
	}
	public void setZone2_04FS_DocNum_Alt(String zone2_04fs_DocNum_Alt) {
		Zone2_04FS_DocNum_Alt = zone2_04fs_DocNum_Alt;
	}
	public String getZone2_05FS_IDNum_Alt() {
		return Zone2_05FS_IDNum_Alt;
	}
	public void setZone2_05FS_IDNum_Alt(String zone2_05fs_IDNum_Alt) {
		Zone2_05FS_IDNum_Alt = zone2_05fs_IDNum_Alt;
	}
	public String getZone2_06FS_DOI_Alt() {
		return Zone2_06FS_DOI_Alt;
	}
	public void setZone2_06FS_DOI_Alt(String zone2_06fs_DOI_Alt) {
		Zone2_06FS_DOI_Alt = zone2_06fs_DOI_Alt;
	}
	public String getZone2_07FS_DOE_Alt() {
		return Zone2_07FS_DOE_Alt;
	}
	public void setZone2_07FS_DOE_Alt(String zone2_07fs_DOE_Alt) {
		Zone2_07FS_DOE_Alt = zone2_07fs_DOE_Alt;
	}
	public String getZone2_08FS_Passport_Alt() {
		return Zone2_08FS_Passport_Alt;
	}
	public void setZone2_08FS_Passport_Alt(String zone2_08fs_Passport_Alt) {
		Zone2_08FS_Passport_Alt = zone2_08fs_Passport_Alt;
	}
	public String getZone2_09FS_Sex_Alt() {
		return Zone2_09FS_Sex_Alt;
	}
	public void setZone2_09FS_Sex_Alt(String zone2_09fs_Sex_Alt) {
		Zone2_09FS_Sex_Alt = zone2_09fs_Sex_Alt;
	}
	public String getZone2_10FS_Residentcard_Type_Alt() {
		return Zone2_10FS_Residentcard_Type_Alt;
	}
	public void setZone2_10FS_Residentcard_Type_Alt(String zone2_10fs_Residentcard_Type_Alt) {
		Zone2_10FS_Residentcard_Type_Alt = zone2_10fs_Residentcard_Type_Alt;
	}
	public String getZone2_13FS_Designation_Alt() {
		return Zone2_13FS_Designation_Alt;
	}
	public void setZone2_13FS_Designation_Alt(String zone2_13fs_Designation_Alt) {
		Zone2_13FS_Designation_Alt = zone2_13fs_Designation_Alt;
	}
	public String getZone3_01FS_Signature_Alt() {
		return Zone3_01FS_Signature_Alt;
	}
	public void setZone3_01FS_Signature_Alt(String zone3_01fs_Signature_Alt) {
		Zone3_01FS_Signature_Alt = zone3_01fs_Signature_Alt;
	}
	public String getZone3_02FS_DOB_Alt() {
		return Zone3_02FS_DOB_Alt;
	}
	public void setZone3_02FS_DOB_Alt(String zone3_02fs_DOB_Alt) {
		Zone3_02FS_DOB_Alt = zone3_02fs_DOB_Alt;
	}
	public String getZone2_01FV_Firstname_MiddleName() {
		return Zone2_01FV_Firstname_MiddleName;
	}
	public void setZone2_01FV_Firstname_MiddleName(String zone2_01fv_Firstname_MiddleName) {
		Zone2_01FV_Firstname_MiddleName = zone2_01fv_Firstname_MiddleName;
	}
	public String getZone2_02FV_Fatherlastname_Motherlastname() {
		return Zone2_02FV_Fatherlastname_Motherlastname;
	}
	public void setZone2_02FV_Fatherlastname_Motherlastname(String zone2_02fv_Fatherlastname_Motherlastname) {
		Zone2_02FV_Fatherlastname_Motherlastname = zone2_02fv_Fatherlastname_Motherlastname;
	}
	public String getZone2_03FV_Nationality() {
		return Zone2_03FV_Nationality;
	}
	public void setZone2_03FV_Nationality(String zone2_03fv_Nationality) {
		Zone2_03FV_Nationality = zone2_03fv_Nationality;
	}
	public String getZone2_04FV_BirthPlace() {
		return Zone2_04FV_BirthPlace;
	}
	public void setZone2_04FV_BirthPlace(String zone2_04fv_BirthPlace) {
		Zone2_04FV_BirthPlace = zone2_04fv_BirthPlace;
	}
	public String getZone2_05FV_DocNum() {
		return Zone2_05FV_DocNum;
	}
	public void setZone2_05FV_DocNum(String zone2_05fv_DocNum) {
		Zone2_05FV_DocNum = zone2_05fv_DocNum;
	}
	public String getZone2_06FV_IDNum() {
		return Zone2_06FV_IDNum;
	}
	public void setZone2_06FV_IDNum(String zone2_06fv_IDNum) {
		Zone2_06FV_IDNum = zone2_06fv_IDNum;
	}
	public String getZone2_07FV_DOI() {
		return Zone2_07FV_DOI;
	}
	public void setZone2_07FV_DOI(String zone2_07fv_DOI) {
		Zone2_07FV_DOI = zone2_07fv_DOI;
	}
	public String getZone2_08FV_DOE() {
		return Zone2_08FV_DOE;
	}
	public void setZone2_08FV_DOE(String zone2_08fv_DOE) {
		Zone2_08FV_DOE = zone2_08fv_DOE;
	}
	public String getZone2_09FV_Passport() {
		return Zone2_09FV_Passport;
	}
	public void setZone2_09FV_Passport(String zone2_09fv_Passport) {
		Zone2_09FV_Passport = zone2_09fv_Passport;
	}
	public String getZone2_10FV_Sex() {
		return Zone2_10FV_Sex;
	}
	public void setZone2_10FV_Sex(String zone2_10fv_Sex) {
		Zone2_10FV_Sex = zone2_10fv_Sex;
	}
	public String getZone2_15FV_Photoghost() {
		return Zone2_15FV_Photoghost;
	}
	public void setZone2_15FV_Photoghost(String zone2_15fv_Photoghost) {
		Zone2_15FV_Photoghost = zone2_15fv_Photoghost;
	}
	public String getZone2_16FV_Director_Signature() {
		return Zone2_16FV_Director_Signature;
	}
	public void setZone2_16FV_Director_Signature(String zone2_16fv_Director_Signature) {
		Zone2_16FV_Director_Signature = zone2_16fv_Director_Signature;
	}
	public String getZone3_01FV_Signature() {
		return Zone3_01FV_Signature;
	}
	public void setZone3_01FV_Signature(String zone3_01fv_Signature) {
		Zone3_01FV_Signature = zone3_01fv_Signature;
	}
	public String getZone3_02FV_DOB() {
		return Zone3_02FV_DOB;
	}
	public void setZone3_02FV_DOB(String zone3_02fv_DOB) {
		Zone3_02FV_DOB = zone3_02fv_DOB;
	}
	public String getZone3_03FV_Photograph() {
		return Zone3_03FV_Photograph;
	}
	public void setZone3_03FV_Photograph(String zone3_03fv_Photograph) {
		Zone3_03FV_Photograph = zone3_03fv_Photograph;
	}
	
	
}
