
package com.meetidentity.usermanagement.domain.express.enroll;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "$type",
    "ExternalId",
    "Name",
    "Dateofbirth",
    "Placeofbirth",
    "Nationality",
    "SSN",
    "Addresses",
    "Emails",
    "PhoneNumbers",
    "X509Certificates",
    "Entitlements",
    "Adjudications",
    "Roles",
    "Groups",
    "profileUrl",
    "Affiliation",
    "CitizenShipAlpha2",
    "CitizenShipAlpha3",
    "CitizenshipName",
    "GenderCode",
    "Height",
    "HeightMM",
    "HeightFeet",
    "HeightInches",
    "Weight",
    "WeightLbs",
    "EyeColorCode",
    "HairColorCode",
    "EthnicityCode",
    "EyeColorString",
    "EthnicityString",
    "HairColorString",
    "Classification",
    "DataElementType",
    "Id",
    "CollectedOn",
    "LastModified",
    "CollectionDevice",
    "SampleType",
    "SampleStatus",
    "SampleEncoding",
    "SampleData",
    "DocumentType",
    "Info",
    "Fields",
    "Analysis",
    "Images",
    "SignatureImage",
    "SignatureImageEncoding",
    "PolicyClass"
})
public class Item {

    @JsonProperty("$type")
    private String $type;
    @JsonProperty("ExternalId")
    private String externalId;
    @JsonProperty("Name")
    private Name name;
    @JsonProperty("Dateofbirth")
    private String dateofbirth;
    @JsonProperty("Placeofbirth")
    private String placeofbirth;
    @JsonProperty("Nationality")
    private String nationality;
    @JsonProperty("SSN")
    private String sSN;
    @JsonProperty("Addresses")
    private List<Address> addresses = new ArrayList<Address>();
    @JsonProperty("Emails")
    private String emails;
    @JsonProperty("PhoneNumbers")
    private String phoneNumbers;
    @JsonProperty("X509Certificates")
    private Object x509Certificates;
    @JsonProperty("Entitlements")
    private String entitlements;
    @JsonProperty("Adjudications")
    private String adjudications;
    @JsonProperty("Roles")
    private String roles;
    @JsonProperty("Groups")
    private String groups;
    @JsonProperty("profileUrl")
    private String profileUrl;
    @JsonProperty("Affiliation")
    private String affiliation;
    @JsonProperty("CitizenShipAlpha2")
    private String citizenShipAlpha2;
    @JsonProperty("CitizenShipAlpha3")
    private String citizenShipAlpha3;
    @JsonProperty("CitizenshipName")
    private String citizenshipName;
    @JsonProperty("GenderCode")
    private String genderCode;
    @JsonProperty("Height")
    private String height;
    @JsonProperty("HeightMM")
    private String heightMM;
    @JsonProperty("HeightFeet")
    private String heightFeet;
    @JsonProperty("HeightInches")
    private String heightInches;
    @JsonProperty("Weight")
    private String weight;
    @JsonProperty("WeightLbs")
    private String weightLbs;
    @JsonProperty("EyeColorCode")
    private String eyeColorCode;
    @JsonProperty("HairColorCode")
    private String hairColorCode;
    @JsonProperty("EthnicityCode")
    private String ethnicityCode;
    @JsonProperty("EyeColorString")
    private String eyeColorString;
    @JsonProperty("EthnicityString")
    private String ethnicityString;
    @JsonProperty("HairColorString")
    private String hairColorString;
    @JsonProperty("Classification")
    private String classification;
    @JsonProperty("DataElementType")
    private String dataElementType;
    @JsonProperty("Id")
    private String id;
    @JsonProperty("CollectedOn")
    private String collectedOn;
    @JsonProperty("LastModified")
    private String lastModified;
    @JsonProperty("CollectionDevice")
    private String collectionDevice;
    @JsonProperty("SampleType")
    private String sampleType;
    @JsonProperty("SampleStatus")
    private String sampleStatus;
    @JsonProperty("SampleEncoding")
    private String sampleEncoding;
    @JsonProperty("SampleData")
    private String sampleData;
    @JsonProperty("DocumentType")
    private String documentType;
    @JsonProperty("Info")
    private Info info;
    @JsonProperty("Fields")
    private List<Field> fields = new ArrayList<Field>();
    @JsonProperty("Analysis")
    private List<Analysi> analysis = new ArrayList<Analysi>();
    @JsonProperty("Images")
    private List<Image> images = new ArrayList<Image>();
    @JsonProperty("SignatureImage")
    private String signatureImage;
    @JsonProperty("SignatureImageEncoding")
    private String signatureImageEncoding;
    @JsonProperty("PolicyClass")
    private String policyClass;
    @JsonIgnore
    private Map<String, String> additionalProperties = new HashMap<String, String>();

    @JsonProperty("$type")
    public String get$type() {
        return $type;
    }

    @JsonProperty("$type")
    public void set$type(String $type) {
        this.$type = $type;
    }

    @JsonProperty("ExternalId")
    public String getExternalId() {
        return externalId;
    }

    @JsonProperty("ExternalId")
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @JsonProperty("Name")
    public Name getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(Name name) {
        this.name = name;
    }

    @JsonProperty("Dateofbirth")
    public String getDateofbirth() {
        return dateofbirth;
    }

    @JsonProperty("Dateofbirth")
    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    @JsonProperty("Placeofbirth")
    public String getPlaceofbirth() {
        return placeofbirth;
    }

    @JsonProperty("Placeofbirth")
    public void setPlaceofbirth(String placeofbirth) {
        this.placeofbirth = placeofbirth;
    }

    @JsonProperty("Nationality")
    public String getNationality() {
        return nationality;
    }

    @JsonProperty("Nationality")
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @JsonProperty("SSN")
    public String getSSN() {
        return sSN;
    }

    @JsonProperty("SSN")
    public void setSSN(String sSN) {
        this.sSN = sSN;
    }

    @JsonProperty("Addresses")
    public List<Address> getAddresses() {
        return addresses;
    }

    @JsonProperty("Addresses")
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @JsonProperty("Emails")
    public String getEmails() {
        return emails;
    }

    @JsonProperty("Emails")
    public void setEmails(String emails) {
        this.emails = emails;
    }

    @JsonProperty("PhoneNumbers")
    public String getPhoneNumbers() {
        return phoneNumbers;
    }

    @JsonProperty("PhoneNumbers")
    public void setPhoneNumbers(String phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    @JsonProperty("X509Certificates")
    public Object getX509Certificates() {
        return x509Certificates;
    }

    @JsonProperty("X509Certificates")
    public void setX509Certificates(Object x509Certificates) {
        this.x509Certificates = x509Certificates;
    }

    @JsonProperty("Entitlements")
    public String getEntitlements() {
        return entitlements;
    }

    @JsonProperty("Entitlements")
    public void setEntitlements(String entitlements) {
        this.entitlements = entitlements;
    }

    @JsonProperty("Adjudications")
    public String getAdjudications() {
        return adjudications;
    }

    @JsonProperty("Adjudications")
    public void setAdjudications(String adjudications) {
        this.adjudications = adjudications;
    }

    @JsonProperty("Roles")
    public String getRoles() {
        return roles;
    }

    @JsonProperty("Roles")
    public void setRoles(String roles) {
        this.roles = roles;
    }

    @JsonProperty("Groups")
    public String getGroups() {
        return groups;
    }

    @JsonProperty("Groups")
    public void setGroups(String groups) {
        this.groups = groups;
    }

    @JsonProperty("profileUrl")
    public String getProfileUrl() {
        return profileUrl;
    }

    @JsonProperty("profileUrl")
    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    @JsonProperty("Affiliation")
    public String getAffiliation() {
        return affiliation;
    }

    @JsonProperty("Affiliation")
    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    @JsonProperty("CitizenShipAlpha2")
    public String getCitizenShipAlpha2() {
        return citizenShipAlpha2;
    }

    @JsonProperty("CitizenShipAlpha2")
    public void setCitizenShipAlpha2(String citizenShipAlpha2) {
        this.citizenShipAlpha2 = citizenShipAlpha2;
    }

    @JsonProperty("CitizenShipAlpha3")
    public String getCitizenShipAlpha3() {
        return citizenShipAlpha3;
    }

    @JsonProperty("CitizenShipAlpha3")
    public void setCitizenShipAlpha3(String citizenShipAlpha3) {
        this.citizenShipAlpha3 = citizenShipAlpha3;
    }

    @JsonProperty("CitizenshipName")
    public String getCitizenshipName() {
        return citizenshipName;
    }

    @JsonProperty("CitizenshipName")
    public void setCitizenshipName(String citizenshipName) {
        this.citizenshipName = citizenshipName;
    }

    @JsonProperty("GenderCode")
    public String getGenderCode() {
        return genderCode;
    }

    @JsonProperty("GenderCode")
    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

    @JsonProperty("Height")
    public String getHeight() {
        return height;
    }

    @JsonProperty("Height")
    public void setHeight(String height) {
        this.height = height;
    }

    @JsonProperty("HeightMM")
    public String getHeightMM() {
        return heightMM;
    }

    @JsonProperty("HeightMM")
    public void setHeightMM(String heightMM) {
        this.heightMM = heightMM;
    }

    @JsonProperty("HeightFeet")
    public String getHeightFeet() {
        return heightFeet;
    }

    @JsonProperty("HeightFeet")
    public void setHeightFeet(String heightFeet) {
        this.heightFeet = heightFeet;
    }

    @JsonProperty("HeightInches")
    public String getHeightInches() {
        return heightInches;
    }

    @JsonProperty("HeightInches")
    public void setHeightInches(String heightInches) {
        this.heightInches = heightInches;
    }

    @JsonProperty("Weight")
    public String getWeight() {
        return weight;
    }

    @JsonProperty("Weight")
    public void setWeight(String weight) {
        this.weight = weight;
    }

    @JsonProperty("WeightLbs")
    public String getWeightLbs() {
        return weightLbs;
    }

    @JsonProperty("WeightLbs")
    public void setWeightLbs(String weightLbs) {
        this.weightLbs = weightLbs;
    }

    @JsonProperty("EyeColorCode")
    public String getEyeColorCode() {
        return eyeColorCode;
    }

    @JsonProperty("EyeColorCode")
    public void setEyeColorCode(String eyeColorCode) {
        this.eyeColorCode = eyeColorCode;
    }

    @JsonProperty("HairColorCode")
    public String getHairColorCode() {
        return hairColorCode;
    }

    @JsonProperty("HairColorCode")
    public void setHairColorCode(String hairColorCode) {
        this.hairColorCode = hairColorCode;
    }

    @JsonProperty("EthnicityCode")
    public String getEthnicityCode() {
        return ethnicityCode;
    }

    @JsonProperty("EthnicityCode")
    public void setEthnicityCode(String ethnicityCode) {
        this.ethnicityCode = ethnicityCode;
    }

    @JsonProperty("EyeColorString")
    public String getEyeColorString() {
        return eyeColorString;
    }

    @JsonProperty("EyeColorString")
    public void setEyeColorString(String eyeColorString) {
        this.eyeColorString = eyeColorString;
    }

    @JsonProperty("EthnicityString")
    public String getEthnicityString() {
        return ethnicityString;
    }

    @JsonProperty("EthnicityString")
    public void setEthnicityString(String ethnicityString) {
        this.ethnicityString = ethnicityString;
    }

    @JsonProperty("HairColorString")
    public String getHairColorString() {
        return hairColorString;
    }

    @JsonProperty("HairColorString")
    public void setHairColorString(String hairColorString) {
        this.hairColorString = hairColorString;
    }

    @JsonProperty("Classification")
    public String getClassification() {
        return classification;
    }

    @JsonProperty("Classification")
    public void setClassification(String classification) {
        this.classification = classification;
    }

    @JsonProperty("DataElementType")
    public String getDataElementType() {
        return dataElementType;
    }

    @JsonProperty("DataElementType")
    public void setDataElementType(String dataElementType) {
        this.dataElementType = dataElementType;
    }

    @JsonProperty("Id")
    public String getId() {
        return id;
    }

    @JsonProperty("Id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("CollectedOn")
    public String getCollectedOn() {
        return collectedOn;
    }

    @JsonProperty("CollectedOn")
    public void setCollectedOn(String collectedOn) {
        this.collectedOn = collectedOn;
    }

    @JsonProperty("LastModified")
    public String getLastModified() {
        return lastModified;
    }

    @JsonProperty("LastModified")
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    @JsonProperty("CollectionDevice")
    public String getCollectionDevice() {
        return collectionDevice;
    }

    @JsonProperty("CollectionDevice")
    public void setCollectionDevice(String collectionDevice) {
        this.collectionDevice = collectionDevice;
    }

    @JsonProperty("SampleType")
    public String getSampleType() {
        return sampleType;
    }

    @JsonProperty("SampleType")
    public void setSampleType(String sampleType) {
        this.sampleType = sampleType;
    }

    @JsonProperty("SampleStatus")
    public String getSampleStatus() {
        return sampleStatus;
    }

    @JsonProperty("SampleStatus")
    public void setSampleStatus(String sampleStatus) {
        this.sampleStatus = sampleStatus;
    }

    @JsonProperty("SampleEncoding")
    public String getSampleEncoding() {
        return sampleEncoding;
    }

    @JsonProperty("SampleEncoding")
    public void setSampleEncoding(String sampleEncoding) {
        this.sampleEncoding = sampleEncoding;
    }

    @JsonProperty("SampleData")
    public String getSampleData() {
        return sampleData;
    }

    @JsonProperty("SampleData")
    public void setSampleData(String sampleData) {
        this.sampleData = sampleData;
    }

    @JsonProperty("DocumentType")
    public String getDocumentType() {
        return documentType;
    }

    @JsonProperty("DocumentType")
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    @JsonProperty("Info")
    public Info getInfo() {
        return info;
    }

    @JsonProperty("Info")
    public void setInfo(Info info) {
        this.info = info;
    }

    @JsonProperty("Fields")
    public List<Field> getFields() {
        return fields;
    }

    @JsonProperty("Fields")
    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    @JsonProperty("Analysis")
    public List<Analysi> getAnalysis() {
        return analysis;
    }

    @JsonProperty("Analysis")
    public void setAnalysis(List<Analysi> analysis) {
        this.analysis = analysis;
    }

    @JsonProperty("Images")
    public List<Image> getImages() {
        return images;
    }

    @JsonProperty("Images")
    public void setImages(List<Image> images) {
        this.images = images;
    }

    @JsonProperty("SignatureImage")
    public String getSignatureImage() {
        return signatureImage;
    }

    @JsonProperty("SignatureImage")
    public void setSignatureImage(String signatureImage) {
        this.signatureImage = signatureImage;
    }

    @JsonProperty("SignatureImageEncoding")
    public String getSignatureImageEncoding() {
        return signatureImageEncoding;
    }

    @JsonProperty("SignatureImageEncoding")
    public void setSignatureImageEncoding(String signatureImageEncoding) {
        this.signatureImageEncoding = signatureImageEncoding;
    }

    @JsonProperty("PolicyClass")
    public String getPolicyClass() {
        return policyClass;
    }

    @JsonProperty("PolicyClass")
    public void setPolicyClass(String policyClass) {
        this.policyClass = policyClass;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, String> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, String value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append($type).append(externalId).append(name).append(dateofbirth).append(placeofbirth).append(nationality).append(sSN).append(addresses).append(emails).append(phoneNumbers).append(x509Certificates).append(entitlements).append(adjudications).append(roles).append(groups).append(profileUrl).append(affiliation).append(citizenShipAlpha2).append(citizenShipAlpha3).append(citizenshipName).append(genderCode).append(height).append(heightMM).append(heightFeet).append(heightInches).append(weight).append(weightLbs).append(eyeColorCode).append(hairColorCode).append(ethnicityCode).append(eyeColorString).append(ethnicityString).append(hairColorString).append(classification).append(dataElementType).append(id).append(collectedOn).append(lastModified).append(collectionDevice).append(sampleType).append(sampleStatus).append(sampleEncoding).append(sampleData).append(documentType).append(info).append(fields).append(analysis).append(images).append(signatureImage).append(signatureImageEncoding).append(policyClass).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Item) == false) {
            return false;
        }
        Item rhs = ((Item) other);
        return new EqualsBuilder().append($type, rhs.$type).append(externalId, rhs.externalId).append(name, rhs.name).append(dateofbirth, rhs.dateofbirth).append(placeofbirth, rhs.placeofbirth).append(nationality, rhs.nationality).append(sSN, rhs.sSN).append(addresses, rhs.addresses).append(emails, rhs.emails).append(phoneNumbers, rhs.phoneNumbers).append(x509Certificates, rhs.x509Certificates).append(entitlements, rhs.entitlements).append(adjudications, rhs.adjudications).append(roles, rhs.roles).append(groups, rhs.groups).append(profileUrl, rhs.profileUrl).append(affiliation, rhs.affiliation).append(citizenShipAlpha2, rhs.citizenShipAlpha2).append(citizenShipAlpha3, rhs.citizenShipAlpha3).append(citizenshipName, rhs.citizenshipName).append(genderCode, rhs.genderCode).append(height, rhs.height).append(heightMM, rhs.heightMM).append(heightFeet, rhs.heightFeet).append(heightInches, rhs.heightInches).append(weight, rhs.weight).append(weightLbs, rhs.weightLbs).append(eyeColorCode, rhs.eyeColorCode).append(hairColorCode, rhs.hairColorCode).append(ethnicityCode, rhs.ethnicityCode).append(eyeColorString, rhs.eyeColorString).append(ethnicityString, rhs.ethnicityString).append(hairColorString, rhs.hairColorString).append(classification, rhs.classification).append(dataElementType, rhs.dataElementType).append(id, rhs.id).append(collectedOn, rhs.collectedOn).append(lastModified, rhs.lastModified).append(collectionDevice, rhs.collectionDevice).append(sampleType, rhs.sampleType).append(sampleStatus, rhs.sampleStatus).append(sampleEncoding, rhs.sampleEncoding).append(sampleData, rhs.sampleData).append(documentType, rhs.documentType).append(info, rhs.info).append(fields, rhs.fields).append(analysis, rhs.analysis).append(images, rhs.images).append(signatureImage, rhs.signatureImage).append(signatureImageEncoding, rhs.signatureImageEncoding).append(policyClass, rhs.policyClass).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
