package com.meetidentity.usermanagement.domain.visual.credentials.piv;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Iterator;

import javax.imageio.ImageIO;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.constants.XMLConstants;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserAppearanceEntity;
import com.meetidentity.usermanagement.domain.UserAttributeEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserPivEntity;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.service.ZxingBarcodeGeneratorService;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.UserResource;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAppearance;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserPIVModel;

@Component
public class UtilityPIV {

	@Autowired
	private Util util = new Util();
	
	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;
	
	@Autowired
	private ZxingBarcodeGeneratorService zxingBarcodeGeneratorService;	
	
	@Autowired
	private UserResource userResource;


	private final Logger log = LoggerFactory.getLogger(UtilityPIV.class);
	
	private static String XLINK_NS = XMLConstants.XLINK_NAMESPACE_URI;
	private static String XLINK_HREF = "xlink:href";

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @param userEntity 
	 * @param visualCredentialType 
	 * @return
	 * @throws Exception
	 */
	public Document parsePIVFront(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user, byte[] svgFile,
			byte[] jsonFile, UserPivEntity userPivEntity, String credentialTemplateID, String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		
		try {
				Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
				Gson gson = new Gson();
				// log.debug(new String(jsonFile));
				
				if(simplyParse) {
					JSONObject pivJsonObject = new JSONObject(new String(jsonFile));
					JSONObject pivFrontJsonObject = pivJsonObject.getJSONObject("pivFront");
					Iterator<String> keys = pivFrontJsonObject.keys();
					log.debug("parsePIVFront: Started");
					
					while(keys.hasNext()) {
						String key = keys.next();
						System.out.println(key);
					      if(doc.getElementById(key) == null) {
					    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS_WITH_KEY, visualCredentialType, key));
					    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS_WITH_KEY, visualCredentialType, key));
					      };
					}
					return doc ;
				}else {

					// UserPIVID userPiv = gson.fromJson(new String(jsonFile), UserPIVID.class);
					
					StringBuilder pdf417_text = new StringBuilder();
					// Dynamic User Variables
					switch (requestFrom) {

						case "web_portal_enroll_summary":
							UserPIVModel userPIV = user.getUserPIV();
							UserAttribute userAttribute = user.getUserAttribute();
							doc.getElementById("Zone2FV_First_Middle_Name").setTextContent(userAttribute.getFirstName());
							doc.getElementById("Zone2FV_Last_Name").setTextContent(userAttribute.getLastName());						
							doc.getElementById("Zone5FV_Rank").setTextContent(userPIV.getRank());
							doc.getElementById("Zone8FV_Affiliation").setTextContent(userPIV.getEmployeeAffiliation());
							doc.getElementById("Zone10FV_Agency").setTextContent(organizationEntity.getDisplayName());
							doc.getElementById("Zone18FV_Affiliation_Color_code").setTextContent(userPIV.getEmployeeAffiliationColorCode());
							doc.getElementById("Zone15FS_Color_Affiliation").setAttribute("class", userPIV.getEmployeeAffiliationColorCode());
							doc.getElementById("Zone4FV_Agency_Specific").setTextContent(userPIV.getAgencySpecificText()== null?"":userPIV.getAgencySpecificText());
							doc.getElementById("Zone20FV_Agency_Abbreviation").setTextContent(userPIV.getAgencyAbbreviation()== null?"":userPIV.getAgencyAbbreviation());							
							doc.getElementById("Zone13FV_Issued").setTextContent("XX/XX/XXXX" );
							doc.getElementById("Zone14FV_Expires").setTextContent("XX/XX/XXXX");
							doc.getElementById("Zone19FV_Card_Expiration").setTextContent("XX/XX/XXXX");
							pdf417_text.append("Card SN: "+ userPIV.getAgencyCardSerialNumber()== null?"XXXXXXX":userPIV.getAgencyCardSerialNumber()+", ");
							user.getUserBiometrics().stream().forEach(userBio -> {
								if (userBio.getType().equalsIgnoreCase("FACE")) {
									
									int width = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("width"));
									int height = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("height"));
									try {
										Image image = Util
												.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
										BufferedImage bufferedImage = Util.resizeImage(image, width, height);
										if(user.getIsTransparentPhotoRequired()) {
											bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
										}
										String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
										doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
												"data:;base64," + base64Data);
		
									} catch (IOException e) {
										doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									}
								}
								if(userBio.getType().equalsIgnoreCase("SIGNATURE")) {
										doc.getElementById("Zone3FV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							});
							doc.getElementById("Zone23FV_QRCode")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + user.getId() + "#" + credentialTemplateID + "#" + "XXXXXXXXXX",
											BarcodeFormat.QR_CODE, 300, 300))));
							break;
						case "web_portal_summary":
						case "web_portal_approval_summary":				
						case "web_portal_print":
						case "Request_From_Mobile":
							UserAttributeEntity userAttributee = userEntity.getUserAttribute();
							doc.getElementById("Zone2FV_First_Middle_Name").setTextContent(userAttributee.getFirstName());
							doc.getElementById("Zone2FV_Last_Name").setTextContent(userAttributee.getLastName());					
							doc.getElementById("Zone5FV_Rank").setTextContent(userPivEntity.getRank());
							//Zone4FV_Agency_Specific
							doc.getElementById("Zone8FV_Affiliation").setTextContent(userPivEntity.getEmployeeAffiliation());
							doc.getElementById("Zone10FV_Agency").setTextContent(organizationEntity.getDisplayName());
							doc.getElementById("Zone18FV_Affiliation_Color_code").setTextContent(userPivEntity.getEmployeeAffiliationColorCode());
							doc.getElementById("Zone15FS_Color_Affiliation").setAttribute("class", userPivEntity.getEmployeeAffiliationColorCode());
							doc.getElementById("Zone4FV_Agency_Specific").setTextContent(userPivEntity.getAgencySpecificText()== null?"":userPivEntity.getAgencySpecificText());
							doc.getElementById("Zone20FV_Agency_Abbreviation").setTextContent(userPivEntity.getAgencyAbbreviation()== null?"":userPivEntity.getAgencyAbbreviation());
							doc.getElementById("Zone13FV_Issued").setTextContent(userPivEntity.getDateOfIssuance() == null? "XX/XX/XXXX" : Util.formatDate(userPivEntity.getDateOfIssuance(), Util.MMDDYYYY_format));
							doc.getElementById("Zone14FV_Expires").setTextContent(userPivEntity.getDateOfExpiry() == null? "XX/XX/XXXX" : Util.formatDate(userPivEntity.getDateOfExpiry(), Util.MMDDYYYY_format));
							doc.getElementById("Zone19FV_Card_Expiration").setTextContent(userPivEntity.getCardExpiry() == null ? "XXXXXXX" : userPivEntity.getCardExpiry());
							pdf417_text.append("Card SN: "+ userPivEntity.getAgencyCardSerialNumber()== null?"XXXXXXXXX":userPivEntity.getAgencyCardSerialNumber()+", ");
							userEntity.getUserBiometrics().stream().forEach(userBio -> {
								if (userBio.getType().equalsIgnoreCase("FACE")) {
									
									int width = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("width"));
									int height = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("height"));
									try {
										Image image = Util
												.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
										BufferedImage bufferedImage = Util.resizeImage(image, width, height);
										if(user.getIsTransparentPhotoRequired()) {
											bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
										}
										String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
										doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
												"data:;base64," + base64Data);
		
									} catch (IOException e) {
										doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									}
								}
								if(userBio.getType().equalsIgnoreCase("SIGNATURE")) {
										doc.getElementById("Zone3FV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							});
							String idNumber = "XXXXXXXXXXXX";
							if(userPivEntity.getPivIdNumber() != null) {
								idNumber = userPivEntity.getPivIdNumber();
							}
							doc.getElementById("Zone23FV_QRCode")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + userEntity.getId() + "#" + credentialTemplateID + "#" + idNumber,
											BarcodeFormat.QR_CODE, 300, 300))));
							break;	
						default :
							break;
					}
					
//					switch (requestFrom) {
//
//						case "web_portal_enroll_summary":
//						case "web_portal_summary":
//						case "web_portal_approval_summary":
//						case "Request_From_Mobile":
////							doc.getElementById("Zone7FS_CircuitChip").setAttributeNS(XLINK_NS, XLINK_HREF,
////									"data:;base64," + userPiv.getPivFront().getZone7FS_CircuitChip());
//							break;
//						case "web_portal_print":
//							Element element = doc.getElementById("Zone7FS_CircuitChip");
//							element.getParentNode().removeChild(element);
//							doc.normalize();
//							break;
//						default :
//							break;
//					}
					
					
					pdf417_text.append("Name: "+doc.getElementById("Zone2FV_First_Middle_Name").getTextContent());
					pdf417_text.append(", Affiliation: "+ doc.getElementById("Zone8FV_Affiliation").getTextContent());
					pdf417_text.append(", Agency: "+ organizationEntity.getDisplayName());
					BufferedImage pdf417 = zxingBarcodeGeneratorService.generatePDF417BarcodeImage(pdf417_text.toString(),Integer.valueOf(doc.getElementById("Zone6FV_PDF").getAttribute("width")),Integer.valueOf(doc.getElementById("Zone6FV_PDF").getAttribute("height")));
					ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
					ImageIO.write(pdf417, "PNG", pngOutputStream);					
					byte[] pdf417_bytes = pngOutputStream.toByteArray(); 
					pngOutputStream.close();
					
					doc.getElementById("Zone6FV_PDF").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder()
							.encodeToString(pdf417_bytes));
//					doc.getElementById("Zone11FS_AgencySeal").setAttributeNS(XLINK_NS, XLINK_HREF,
//							"data:;base64," + userPiv.getPivFront().getZone11FS_AgencySeal());
//					doc.getElementById("background").setAttributeNS(XLINK_NS, XLINK_HREF,
//							"data:;base64," + userPiv.getPivFront().getBackground());
					
					log.debug("parsePIVFront: Ended Dynamic Images loading");
					return doc;
				}
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType);
		}
	}

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @param userEntity 
	 * @return
	 * @throws Exception
	 */
	public Document parsePIVBack(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user, byte[] svgFile,
			byte[] jsonFile,UserPivEntity userPivEntity, String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			
			if(simplyParse) {
				JSONObject pivJsonObject = new JSONObject(new String(jsonFile));
				JSONObject pivBackJsonObject = pivJsonObject.getJSONObject("pivBack");
				Iterator<String> keys = pivBackJsonObject.keys();
				log.debug("parsePIVBack: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS_WITH_KEY, visualCredentialType,key));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS_WITH_KEY, visualCredentialType,key));
				      };
				}
				return doc ;
			}else {
				
				// UserPIVID userPiv = gson.fromJson(new String(jsonFile), UserPIVID.class);
				
				// Dynamic User Variables
				StringBuilder linearBC_text = new StringBuilder();
				
				
				switch (requestFrom) {

					case "web_portal_enroll_summary":
						UserAttribute userAttribute = user.getUserAttribute();			
						UserAppearance userAppearance = user.getUserAppearance();
						if(userAppearance != null) {
							doc.getElementById("Zone5BV_PhysicalCC_HGT").setTextContent(userAppearance.getHeight());
							doc.getElementById("Zone5BV_PhysicalCC_Eye").setTextContent(userAppearance.getEyeColor());
							doc.getElementById("Zone5BV_PhysicalCC_Hair").setTextContent(userAppearance.getHairColor());		
						}
						doc.getElementById("Zone1BV_Agency_CardSN").setTextContent("XXXXXXXXXXXX");
						doc.getElementById("Zone2BV_IssuerIN").setTextContent("XXXXXXXXXXXX");
						
						linearBC_text.append("Card SN: XXXXXXXXXXXX, ");
						linearBC_text.append(", Name: "+ userAttribute.getFirstName());
						linearBC_text.append(" "+userAttribute.getLastName());
						linearBC_text.append(", Agency: "+ organizationEntity.getDisplayName());
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":				
					case "web_portal_print":
					case "Request_From_Mobile":
						UserAppearanceEntity userAppearancee = userEntity.getUserAppearance();
						UserAttributeEntity userAttributeEntity2 = userPivEntity.getUser().getUserAttribute();						
						doc.getElementById("Zone1BV_Agency_CardSN").setTextContent(userPivEntity.getAgencyCardSerialNumber()== null?"XXXXXXXXX":userPivEntity.getAgencyCardSerialNumber());
						doc.getElementById("Zone2BV_IssuerIN").setTextContent(userPivEntity.getIssuerIdentificationNumber());
						if(userAppearancee != null) {					
								doc.getElementById("Zone5BV_PhysicalCC_HGT").setTextContent(userAppearancee.getHeight());
								doc.getElementById("Zone5BV_PhysicalCC_Eye").setTextContent(userAppearancee.getEyeColor());
								doc.getElementById("Zone5BV_PhysicalCC_Hair").setTextContent(userAppearancee.getHairColor());
						}
						linearBC_text.append("Card SN: "+userPivEntity.getAgencyCardSerialNumber()== null?"XXXXXXXXX":userPivEntity.getAgencyCardSerialNumber()+", ");
						linearBC_text.append(", Name: "+ userAttributeEntity2.getFirstName());
						linearBC_text.append(" "+userAttributeEntity2.getLastName());
						linearBC_text.append(", Agency: "+ organizationEntity.getDisplayName());			
						break;	
					default :
						break;				
				}
				
				//TODO Contents:linearBC_text length should be between 1 and 80 characters, but got 93 else ava.lang.IllegalArgumentException: is thrown
//				BufferedImage linearBC = zxingBarcodeGeneratorService.generateCode128BarcodeImage(linearBC_text.toString());
//				ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
//				ImageIO.write(linearBC, "PNG", pngOutputStream);					
//				byte[] linearBC_bytes = pngOutputStream.toByteArray(); 
//				pngOutputStream.close();
//				
//				doc.getElementById("Zone8BV_LinearBC").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder()
//						.encodeToString(linearBC_bytes));
	

				log.debug("parseDLBack: Ended Dynamic text Variable");
		
				return doc;
			}
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType);
		}
	}

}
