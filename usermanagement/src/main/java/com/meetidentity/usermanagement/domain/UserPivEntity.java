package com.meetidentity.usermanagement.domain;


import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.meetidentity.usermanagement.annotations.SequenceValue;

@Entity
@Table(name = "user_piv")
public class UserPivEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "uims_id", nullable = false)
	private String uimsID;

	@SequenceValue
	@Column(name = "piv_id_number")
	private String pivIdNumber;
	
	@Column(name = "employee_affiliation")
	private String employeeAffiliation;
	
	@Column(name = "affiliation_optional_line")
	private String affiliationOptionalLine;
	
	@Column(name = "employee_affiliation_color_code")
	private String employeeAffiliationColorCode;
	
	@Column(name = "employee_photo_border_color")
	private String employeePhotoBorderColor;
	
	@Column(name = "rank")
	private String rank;

	@Column(name = "agency")
	private String agency;

	@Column(name = "agency_abbreviation")
	private String agencyAbbreviation;
	
	@Column(name = "agency_card_serial_number")
	private String agencyCardSerialNumber;

	@Column(name = "agency_specific_data")
	private String agencySpecificData;

	@Column(name = "agency_specific_text")
	private String agencySpecificText;

	@Column(name = "date_of_issuance")
	private Instant dateOfIssuance;

	@Column(name = "date_of_expiry")
	private Instant dateOfExpiry;
	
	@Column(name = "card_expiry")
	private String cardExpiry;

	@Lob
	@Column(name = "pdf417_bar_code")
	private byte[] pdf417BarCode;

	@Lob
	@Column(name = "linear_bar_code")
	private byte[] linearBarCode;	

	@Column(name = "issuer_identification_number")
	private String issuerIdentificationNumber;	

	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUimsID() {
		return uimsID;
	}

	public void setUimsID(String uimsID) {
		this.uimsID = uimsID;
	}	
	
	public String getPivIdNumber() {
		return pivIdNumber;
	}

	public void setPivIdNumber(String pivIdNumber) {
		this.pivIdNumber = pivIdNumber;
	}

	public String getEmployeeAffiliation() {
		return employeeAffiliation;
	}

	public void setEmployeeAffiliation(String employeeAffiliation) {
		this.employeeAffiliation = employeeAffiliation;
	}

	public String getAffiliationOptionalLine() {
		return affiliationOptionalLine;
	}

	public void setAffiliationOptionalLine(String affiliationOptionalLine) {
		this.affiliationOptionalLine = affiliationOptionalLine;
	}

	public String getEmployeeAffiliationColorCode() {
		return employeeAffiliationColorCode;
	}

	public void setEmployeeAffiliationColorCode(String employeeAffiliationColorCode) {
		this.employeeAffiliationColorCode = employeeAffiliationColorCode;
	}

	public String getAgencyCardSerialNumber() {
		return agencyCardSerialNumber;
	}

	public void setAgencyCardSerialNumber(String agencyCardSerialNumber) {
		this.agencyCardSerialNumber = agencyCardSerialNumber;
	}

	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public String getAgencySpecificData() {
		return agencySpecificData;
	}

	public void setAgencySpecificData(String agencySpecificData) {
		this.agencySpecificData = agencySpecificData;
	}

	public String getAgencySpecificText() {
		return agencySpecificText;
	}

	public void setAgencySpecificText(String agencySpecificText) {
		this.agencySpecificText = agencySpecificText;
	}

	public Instant getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Instant dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public Instant getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Instant dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getCardExpiry() {
		return cardExpiry;
	}

	public void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public byte[] getPdf417BarCode() {
		return pdf417BarCode;
	}

	public void setPdf417BarCode(byte[] pdf417BarCode) {
		this.pdf417BarCode = pdf417BarCode;
	}	

	public byte[] getLinearBarCode() {
		return linearBarCode;
	}

	public void setLinearBarCode(byte[] linearBarCode) {
		this.linearBarCode = linearBarCode;
	}
	
	public String getEmployeePhotoBorderColor() {
		return employeePhotoBorderColor;
	}

	public void setEmployeePhotoBorderColor(String employeePhotoBorderColor) {
		this.employeePhotoBorderColor = employeePhotoBorderColor;
	}

	public String getAgencyAbbreviation() {
		return agencyAbbreviation;
	}

	public void setAgencyAbbreviation(String agencyAbbreviation) {
		this.agencyAbbreviation = agencyAbbreviation;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getIssuerIdentificationNumber() {
		return issuerIdentificationNumber;
	}

	public void setIssuerIdentificationNumber(String issuerIdentificationNumber) {
		this.issuerIdentificationNumber = issuerIdentificationNumber;
	}

	
	
	
	
}
