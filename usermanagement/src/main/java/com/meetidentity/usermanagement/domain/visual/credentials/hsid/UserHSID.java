package com.meetidentity.usermanagement.domain.visual.credentials.hsid;

import java.io.Serializable;

public class UserHSID implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserHSIDFront hsIDFront;
	private UserHSIDBack hsIDBack;
	public UserHSIDFront getHsIDFront() {
		return hsIDFront;
	}
	public void setHsIDFront(UserHSIDFront hsIDFront) {
		this.hsIDFront = hsIDFront;
	}
	public UserHSIDBack getHsIDBack() {
		return hsIDBack;
	}
	public void setHsIDBack(UserHSIDBack hsIDBack) {
		this.hsIDBack = hsIDBack;
	}
	
	
	

}
