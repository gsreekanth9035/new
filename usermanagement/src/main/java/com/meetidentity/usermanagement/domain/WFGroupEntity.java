package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "wf_group")
public class WFGroupEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "workflow_id")
	private WorkflowEntity workflow;

    @Column(name="reference_group_id")
	private String referenceGroupId;
    
    @ManyToOne
    @JoinColumn(name="smart_card_visual_template_id") 
   	private VisualTemplateEntity visualTemplate;
    
    @Column(name="reference_group_name")
	private String referenceGroupName;
    
	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WorkflowEntity getWorkflow() {
		return workflow;
	}

	public void setWorkflow(WorkflowEntity workflow) {
		this.workflow = workflow;
	}

	public String getReferenceGroupId() {
		return referenceGroupId;
	}

	public void setReferenceGroupId(String referenceGroupId) {
		this.referenceGroupId = referenceGroupId;
	}

	public String getReferenceGroupName() {
		return referenceGroupName;
	}

	public void setReferenceGroupName(String referenceGroupName) {
		this.referenceGroupName = referenceGroupName;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public VisualTemplateEntity getVisualTemplate() {
		return visualTemplate;
	}

	public void setVisualTemplate(VisualTemplateEntity visualTemplate) {
		this.visualTemplate = visualTemplate;
	}

}
