package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "wf_step")
public class WFStepEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "workflow_id")
	private WorkflowEntity workflow;

	@ManyToOne
	@JoinColumn(name = "wf_step_def_id")
	private WFStepDefEntity wfStepDef;

	@Column(name = "step_order")
	private int stepOrder;
	
	@Column(name = "status")
	private String status;
	
	@Version
	@Column(name = "version")
	private int version;

	@OneToMany(mappedBy = "wfStep", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WFStepConfigEntity> wfStepConfigs = new ArrayList<>();

	@OneToMany(mappedBy = "wfStep", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WFStepRegistrationConfigEntity> wfStepRegistrationConfigs = new ArrayList<>();

	@OneToMany(mappedBy = "wfStep", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WFStepApprovalGroupConfigEntity> wfStepApprovalGroupConfigs = new ArrayList<>();

	@OneToMany(mappedBy = "wfStep", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WFStepCertConfigEntity> wfStepCertConfigs = new ArrayList<>();

	@OneToMany(mappedBy = "wfStep", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WfStepChipEncodeGroupConfigEntity> wfStepChipEncodeGroupConfigs = new ArrayList<>();
	
	@OneToMany(mappedBy = "wfStep", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WfStepChipEncodeAndVIDPrintGroupConfigEntity> wfStepChipEncodeVIDPrintGroupConfigs = new ArrayList<>();

	@OneToMany(mappedBy = "wfStep", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WfStepPrintGroupConfigEntity> wfStepPrintGroupConfigs = new ArrayList<>();
	
	@OneToMany(mappedBy = "wfStep", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WFMobileIDIdentityIssuanceEntity> wfMobileIDStepIdentityConfigs = new ArrayList<>();

	@OneToMany(mappedBy = "wfStep", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WFStepAdjudicationGroupConfigEntity> wfStepAdjudicationGroupConfigs = new ArrayList<>();

	@OneToMany(mappedBy = "wfStep", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<WFMobileIDStepTrustedIdentityEntity> wfMobileIDStepTrustedIdentities = new ArrayList<>();

//	@OneToOne(mappedBy = "wfStep", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	@OneToOne(mappedBy = "wfStep", cascade = CascadeType.ALL)
	private WfStepAppletLoadingInfoEntity wfAppletLoadingInfo;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WorkflowEntity getWorkflow() {
		return workflow;
	}

	public void setWorkflow(WorkflowEntity workflow) {
		this.workflow = workflow;
	}

	public WFStepDefEntity getWfStepDef() {
		return wfStepDef;
	}

	public void setWfStepDef(WFStepDefEntity wfStepDef) {
		this.wfStepDef = wfStepDef;
	}

	public int getStepOrder() {
		return stepOrder;
	}

	public void setStepOrder(int stepOrder) {
		this.stepOrder = stepOrder;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<WFStepConfigEntity> getWfStepConfigs() {
		return wfStepConfigs;
	}

	public void setWfStepConfigs(List<WFStepConfigEntity> wfStepConfigs) {
		this.wfStepConfigs = wfStepConfigs;
	}

	public List<WFStepRegistrationConfigEntity> getWfStepRegistrationConfigs() {
		return wfStepRegistrationConfigs;
	}

	public void setWfStepRegistrationConfigs(List<WFStepRegistrationConfigEntity> wfStepRegistrationConfigs) {
		this.wfStepRegistrationConfigs = wfStepRegistrationConfigs;
	}

	public List<WFStepApprovalGroupConfigEntity> getWfStepApprovalGroupConfigs() {
		return wfStepApprovalGroupConfigs;
	}

	public void setWfStepApprovalGroupConfigs(List<WFStepApprovalGroupConfigEntity> wfStepApprovalGroupConfigs) {
		this.wfStepApprovalGroupConfigs = wfStepApprovalGroupConfigs;
	}

	public List<WFStepCertConfigEntity> getWfStepCertConfigs() {
		return wfStepCertConfigs;
	}

	public void setWfStepCertConfigs(List<WFStepCertConfigEntity> wfStepCertConfigs) {
		this.wfStepCertConfigs = wfStepCertConfigs;
	}

	public void addWfStepConfig(WFStepConfigEntity wfStepConfigEntity) {
		wfStepConfigEntity.setWfStep(this);
		this.wfStepConfigs.add(wfStepConfigEntity);
	}

	public void removeWfStepConfig(WFStepConfigEntity wfStepConfigEntity) {
		wfStepConfigEntity.setWfStep(null);
		this.wfStepConfigs.remove(wfStepConfigEntity);
	}

	public void addWfStepRegistrationConfig(WFStepRegistrationConfigEntity wfStepRegistrationConfigEntity) {
		wfStepRegistrationConfigEntity.setWfStep(this);
		this.wfStepRegistrationConfigs.add(wfStepRegistrationConfigEntity);
	}

	public void removeWfStepRegistrationConfig(WFStepRegistrationConfigEntity wfStepRegistrationConfigEntity) {
		wfStepRegistrationConfigEntity.setWfStep(null);
		this.wfStepRegistrationConfigs.remove(wfStepRegistrationConfigEntity);
	}

	public void addWfStepApprovalGroupConfig(WFStepApprovalGroupConfigEntity wfStepApprovalGroupConfigEntity) {
		wfStepApprovalGroupConfigEntity.setWfStep(this);
		this.wfStepApprovalGroupConfigs.add(wfStepApprovalGroupConfigEntity);
	}

	public void removeWfStepApprovalGroupConfig(WFStepApprovalGroupConfigEntity wfStepApprovalGroupConfigEntity) {
		wfStepApprovalGroupConfigEntity.setWfStep(null);
		this.wfStepApprovalGroupConfigs.remove(wfStepApprovalGroupConfigEntity);
	}

	public void addWfStepCertConfig(WFStepCertConfigEntity wfStepCertConfigEntity) {
		wfStepCertConfigEntity.setWfStep(this);
		this.wfStepCertConfigs.add(wfStepCertConfigEntity);
	}

	public void removeWfStepCertConfig(WFStepCertConfigEntity wfStepCertConfigEntity) {
		wfStepCertConfigEntity.setWfStep(null);
		this.wfStepCertConfigs.remove(wfStepCertConfigEntity);
	}

	public List<WfStepPrintGroupConfigEntity> getWfStepPrintGroupConfigs() {
		return wfStepPrintGroupConfigs;
	}

	public void setWfStepPrintGroupConfigs(List<WfStepPrintGroupConfigEntity> wfStepPrintGroupConfigs) {
		this.wfStepPrintGroupConfigs = wfStepPrintGroupConfigs;
	}

	public void addWfStepPrintGroupConfigs(WfStepPrintGroupConfigEntity wfStepPrintGroupConfigEntity) {
		wfStepPrintGroupConfigEntity.setWfStep(this);
		this.wfStepPrintGroupConfigs.add(wfStepPrintGroupConfigEntity);
	}

	public void removeWfStepPrintGroupConfigs(WfStepPrintGroupConfigEntity wfStepPrintGroupConfigEntity) {
		wfStepPrintGroupConfigEntity.setWfStep(null);
		this.wfStepPrintGroupConfigs.remove(wfStepPrintGroupConfigEntity);
	}

	public List<WFMobileIDIdentityIssuanceEntity> getWfMobileIDStepIdentityConfigs() {
		return wfMobileIDStepIdentityConfigs;
	}

	public void setWfMobileIDStepIdentityConfigs(
			List<WFMobileIDIdentityIssuanceEntity> wfMobileIDStepIdentityConfigs) {
		this.wfMobileIDStepIdentityConfigs = wfMobileIDStepIdentityConfigs;
	}

	public void addWFMobileIDStepIdentityConfigs(
			WFMobileIDIdentityIssuanceEntity wfMobileIDStepIdentityConfigEntity) {
		wfMobileIDStepIdentityConfigEntity.setWfStep(this);
		this.wfMobileIDStepIdentityConfigs.add(wfMobileIDStepIdentityConfigEntity);
	}

	public void removeWFMobileIDStepIdentityConfigs(
			WFMobileIDIdentityIssuanceEntity wfMobileIDStepIdentityConfigEntity) {
		wfMobileIDStepIdentityConfigEntity.setWfStep(null);
		this.wfMobileIDStepIdentityConfigs.remove(wfMobileIDStepIdentityConfigEntity);
	}

	public List<WFStepAdjudicationGroupConfigEntity> getWfStepAdjudicationGroupConfigs() {
		return wfStepAdjudicationGroupConfigs;
	}

	public void setWfStepAdjudicationGroupConfigs(
			List<WFStepAdjudicationGroupConfigEntity> wfStepAdjudicationGroupConfigs) {
		this.wfStepAdjudicationGroupConfigs = wfStepAdjudicationGroupConfigs;
	}

	public void addWFStepAdjudicationGroupConfigEntity(
			WFStepAdjudicationGroupConfigEntity wfStepAdjudicationGroupConfigEntity) {
		wfStepAdjudicationGroupConfigEntity.setWfStep(this);
		this.wfStepAdjudicationGroupConfigs.add(wfStepAdjudicationGroupConfigEntity);
	}

	public void removeWFStepAdjudicationGroupConfigEntity(
			WFStepAdjudicationGroupConfigEntity wfStepAdjudicationGroupConfigEntity) {
		wfStepAdjudicationGroupConfigEntity.setWfStep(null);
		this.wfStepAdjudicationGroupConfigs.remove(wfStepAdjudicationGroupConfigEntity);
	}

	public List<WFMobileIDStepTrustedIdentityEntity> getWfMobileIDStepTrustedIdentities() {
		return wfMobileIDStepTrustedIdentities;
	}

	public void setWfMobileIDStepTrustedIdentities(
			List<WFMobileIDStepTrustedIdentityEntity> wfMobileIDStepTrustedIdentities) {
		this.wfMobileIDStepTrustedIdentities = wfMobileIDStepTrustedIdentities;
	}

	public void addWFMobileIDStepTrustedIdentityEntity(
			WFMobileIDStepTrustedIdentityEntity wfMobileIDStepTrustedIdentityEntity) {
		wfMobileIDStepTrustedIdentityEntity.setWfStep(this);
		this.wfMobileIDStepTrustedIdentities.add(wfMobileIDStepTrustedIdentityEntity);
	}

	public void removeWFMobileIDStepTrustedIdentityEntity(
			WFMobileIDStepTrustedIdentityEntity wfMobileIDStepTrustedIdentityEntity) {
		wfMobileIDStepTrustedIdentityEntity.setWfStep(null);
		this.wfMobileIDStepTrustedIdentities.remove(wfMobileIDStepTrustedIdentityEntity);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<WfStepChipEncodeGroupConfigEntity> getWfStepChipEncodeGroupConfigs() {
		return wfStepChipEncodeGroupConfigs;
	}

	public void setWfStepChipEncodeGroupConfigs(List<WfStepChipEncodeGroupConfigEntity> wfStepChipEncodeGroupConfigs) {
		this.wfStepChipEncodeGroupConfigs = wfStepChipEncodeGroupConfigs;
	}

	public void addWfStepChipEncodeGroupConfigEntity(WfStepChipEncodeGroupConfigEntity wfStepChipEncodeGroupConfigEntity) {
		wfStepChipEncodeGroupConfigEntity.setWfStep(this);
		this.wfStepChipEncodeGroupConfigs.add(wfStepChipEncodeGroupConfigEntity);
	}

	public void removeWfStepChipEncodeGroupConfigEntity(WfStepChipEncodeGroupConfigEntity wfStepChipEncodeGroupConfigEntity) {
		wfStepChipEncodeGroupConfigEntity.setWfStep(null);
		this.wfStepChipEncodeGroupConfigs.remove(wfStepChipEncodeGroupConfigEntity);
	}

	public List<WfStepChipEncodeAndVIDPrintGroupConfigEntity> getWfStepChipEncodeVIDPrintGroupConfigs() {
		return wfStepChipEncodeVIDPrintGroupConfigs;
	}

	public void setWfStepChipEncodeVIDPrintGroupConfigs(
			List<WfStepChipEncodeAndVIDPrintGroupConfigEntity> wfStepChipEncodeVIDPrintGroupConfigs) {
		this.wfStepChipEncodeVIDPrintGroupConfigs = wfStepChipEncodeVIDPrintGroupConfigs;
	}

	public void addWfStepChipEncodeAndVIDPrintGroupConfigEntity(WfStepChipEncodeAndVIDPrintGroupConfigEntity wfStepChipEncodeAndVIDPrintGroupConfigEntity) {
		wfStepChipEncodeAndVIDPrintGroupConfigEntity.setWfStep(this);
		this.wfStepChipEncodeVIDPrintGroupConfigs.add(wfStepChipEncodeAndVIDPrintGroupConfigEntity);
	}

	public void removeWfStepChipEncodeAndVIDPrintGroupConfigEntity(WfStepChipEncodeAndVIDPrintGroupConfigEntity wfStepChipEncodeAndVIDPrintGroupConfigEntity) {
		wfStepChipEncodeAndVIDPrintGroupConfigEntity.setWfStep(null);
		this.wfStepChipEncodeVIDPrintGroupConfigs.remove(wfStepChipEncodeAndVIDPrintGroupConfigEntity);
	}

	public WfStepAppletLoadingInfoEntity getWfAppletLoadingInfo() {
		return wfAppletLoadingInfo;
	}

	public void setWfAppletLoadingInfo(WfStepAppletLoadingInfoEntity wfAppletLoadingInfo) {
		this.wfAppletLoadingInfo = wfAppletLoadingInfo;
	}
	
}
