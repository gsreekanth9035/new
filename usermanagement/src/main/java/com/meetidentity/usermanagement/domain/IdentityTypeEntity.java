package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="identity_type")
public class IdentityTypeEntity  extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
    @Column(name="name")
	private String name;
    
    @Column(name="description")
	private String description;
    
    @Column(name="ref_standard")
	private String refStandard;
    
    @Column(name="ref_standard_desc")
	private String refStandardDesc;
    
    @Column(name="notes")
	private String notes;
    
    @Column(name="file_formats")
    private String fileFormats;
    
    @OneToMany(mappedBy="identityType", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserPersonalizedIDEntity> userPersonalizedIDs = new ArrayList<>();
    
    @OneToMany(mappedBy="identityTpeId")
    private List<EmployeeAffiliationEntity> employeeAffiliations = new ArrayList<>();

    @Version
    @Column(name="version")
    private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRefStandard() {
		return refStandard;
	}

	public void setRefStandard(String refStandard) {
		this.refStandard = refStandard;
	}

	public String getRefStandardDesc() {
		return refStandardDesc;
	}

	public void setRefStandardDesc(String refStandardDesc) {
		this.refStandardDesc = refStandardDesc;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<EmployeeAffiliationEntity> getEmployeeAffiliations() {
		return employeeAffiliations;
	}

	public void setEmployeeAffiliations(List<EmployeeAffiliationEntity> employeeAffiliations) {
		this.employeeAffiliations = employeeAffiliations;
	}

	public List<UserPersonalizedIDEntity> getUserPersonalizedIDs() {
		return userPersonalizedIDs;
	}

	public void setUserPersonalizedIDs(List<UserPersonalizedIDEntity> userPersonalizedIDs) {
		this.userPersonalizedIDs = userPersonalizedIDs;
	}

	public String getFileFormats() {
		return fileFormats;
	}

	public void setFileFormats(String fileFormats) {
		this.fileFormats = fileFormats;
	}
	
}
