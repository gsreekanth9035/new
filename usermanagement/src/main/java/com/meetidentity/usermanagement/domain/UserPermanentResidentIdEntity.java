package com.meetidentity.usermanagement.domain;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.meetidentity.usermanagement.annotations.SequenceValue;

@Entity
@Table(name = "user_permanent_resident_id")
public class UserPermanentResidentIdEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "uims_id", nullable = false)
	private String uimsId;
	
	//TODO this should also be a 8-10 digit sequence number
	//@SequenceValue
	@Column(name = "document_number")
	private String documentNumber;

	@Column(name = "date_of_issuance")
	private Instant dateOfIssuance;

	@Column(name = "date_of_expiry")
	private Instant dateOfExpiry;
	
	@SequenceValue
	@Column(name = "personal_code")
	private String personalCode;
	
	@Column(name = "residentcard_type_code")
	private String residentcardTypeCode;
	
	@Column(name = "residentcard_type_number")
	private String residentcardTypeNumber;
	
	@Column(name = "passport_number")
	private String passportNumber;
	
	@Column(name = "mrz_td1_line1")
	private String mrzTd1Line1;
	
	@Column(name = "mrz_td1_line2")
	private String mrzTd1Line2;
	
	@Column(name = "mrz_td1_line3")
	private String mrzTd1Line3;	
    	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public Instant getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Instant instant) {
		this.dateOfIssuance = instant;
	}

	public Instant getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Instant dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	public String getPersonalCode() {
		return personalCode;
	}

	public void setPersonalCode(String personalCode) {
		this.personalCode = personalCode;
	}

	public String getMrzTd1Line1() {
		return mrzTd1Line1;
	}

	public void setMrzTd1Line1(String mrzTd1Line1) {
		this.mrzTd1Line1 = mrzTd1Line1;
	}

	public String getMrzTd1Line2() {
		return mrzTd1Line2;
	}

	public void setMrzTd1Line2(String mrzTd1Line2) {
		this.mrzTd1Line2 = mrzTd1Line2;
	}

	public String getMrzTd1Line3() {
		return mrzTd1Line3;
	}

	public void setMrzTd1Line3(String mrzTd1Line3) {
		this.mrzTd1Line3 = mrzTd1Line3;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getResidentcardTypeCode() {
		return residentcardTypeCode;
	}

	public void setResidentcardTypeCode(String residentcardTypeCode) {
		this.residentcardTypeCode = residentcardTypeCode;
	}

	public String getResidentcardTypeNumber() {
		return residentcardTypeNumber;
	}

	public void setResidentcardTypeNumber(String residentcardTypeNumber) {
		this.residentcardTypeNumber = residentcardTypeNumber;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

		

}
