package com.meetidentity.usermanagement.domain.visual.credentials.mls;

import java.io.Serializable;

public class MLSTemplateFront implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String TEAM;
	private String DATE;
	private String EVENT;
	private String Zone2FV_Last_Name;
	private String Zone8FV_Team;
	private String Zone13FV_Date;
	private String Zone10FV_Event;
	private String Zone13FV_IDesignation;
	private String Zone6FV_QR;
	private String Background;
	private String Zone1FV_Photograph;
	private String Zone11FS_AgencySeal_copy;
	
	public String getBackground() {
		return Background;
	}
	public void setBackground(String background) {
		Background = background;
	}
	public String getTEAM() {
		return TEAM;
	}
	public void setTEAM(String tEAM) {
		TEAM = tEAM;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getEVENT() {
		return EVENT;
	}
	public void setEVENT(String eVENT) {
		EVENT = eVENT;
	}
	public String getZone1FV_Photograph() {
		return Zone1FV_Photograph;
	}
	public void setZone1FV_Photograph(String zone1fv_Photograph) {
		Zone1FV_Photograph = zone1fv_Photograph;
	}
	public String getZone11FS_AgencySeal_copy() {
		return Zone11FS_AgencySeal_copy;
	}
	public void setZone11FS_AgencySeal_copy(String zone11fs_AgencySeal_copy) {
		Zone11FS_AgencySeal_copy = zone11fs_AgencySeal_copy;
	}
	public String getZone2FV_Last_Name() {
		return Zone2FV_Last_Name;
	}
	public void setZone2FV_Last_Name(String zone2fv_Last_Name) {
		Zone2FV_Last_Name = zone2fv_Last_Name;
	}
	public String getZone8FV_Team() {
		return Zone8FV_Team;
	}
	public void setZone8FV_Team(String zone8fv_Team) {
		Zone8FV_Team = zone8fv_Team;
	}
	public String getZone13FV_Date() {
		return Zone13FV_Date;
	}
	public void setZone13FV_Date(String zone13fv_Date) {
		Zone13FV_Date = zone13fv_Date;
	}
	public String getZone10FV_Event() {
		return Zone10FV_Event;
	}
	public void setZone10FV_Event(String zone10fv_Event) {
		Zone10FV_Event = zone10fv_Event;
	}
	public String getZone13FV_IDesignation() {
		return Zone13FV_IDesignation;
	}
	public void setZone13FV_IDesignation(String zone13fv_IDesignation) {
		Zone13FV_IDesignation = zone13fv_IDesignation;
	}
	public String getZone6FV_QR() {
		return Zone6FV_QR;
	}
	public void setZone6FV_QR(String zone6fv_QR) {
		Zone6FV_QR = zone6fv_QR;
	}
	
	
	
	
	

}
