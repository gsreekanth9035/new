package com.meetidentity.usermanagement.domain.visual.credentials.prid;

import java.io.Serializable;

public class UserPRIDBack implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String Zone0_BS_background;
	private String Zone4_BS_Agency_seal;

	private String Zone4_01BS_Phone_Eng;
	private String Zone4_02BS_DOB_Eng;
	private String Zone4_03BS_CHAddress_Eng;
	private String Zone4_04BS_DocNum_Eng;
	private String Zone4_05BS_IDNum_Eng;

	private String Zone4_01BS_Phone_Alt;
	private String Zone4_02BS_DOB_Alt;
	private String Zone4_03BS_CHAddress_Alt;
	private String Zone4_04BS_DocNum_Alt;
	private String Zone4_05BS_IDNum_Alt;

	private String Zone4_01BV_Phone;
	private String Zone4_02BV_Birthdate;
	private String Zone4_03BV_CHAddress1;
	private String Zone4_03BV_CHAddress2;
	private String Zone4_04BV_DocNum;
	private String Zone4_05BV_IDNum;

	private String Zone5_MRZ_TD1_L1;
	private String Zone5_MRZ_TD1_L2;
	private String Zone5_MRZ_TD1_L3;

	private String Zone4_06BV_QR;

	public String getZone0_BS_background() {
		return Zone0_BS_background;
	}

	public void setZone0_BS_background(String zone0_BS_background) {
		Zone0_BS_background = zone0_BS_background;
	}

	public String getZone4_BS_Agency_seal() {
		return Zone4_BS_Agency_seal;
	}

	public void setZone4_BS_Agency_seal(String zone4_BS_Agency_seal) {
		Zone4_BS_Agency_seal = zone4_BS_Agency_seal;
	}

	public String getZone4_01BS_Phone_Eng() {
		return Zone4_01BS_Phone_Eng;
	}

	public void setZone4_01BS_Phone_Eng(String zone4_01bs_Phone_Eng) {
		Zone4_01BS_Phone_Eng = zone4_01bs_Phone_Eng;
	}

	public String getZone4_02BS_DOB_Eng() {
		return Zone4_02BS_DOB_Eng;
	}

	public void setZone4_02BS_DOB_Eng(String zone4_02bs_DOB_Eng) {
		Zone4_02BS_DOB_Eng = zone4_02bs_DOB_Eng;
	}

	public String getZone4_03BS_CHAddress_Eng() {
		return Zone4_03BS_CHAddress_Eng;
	}

	public void setZone4_03BS_CHAddress_Eng(String zone4_03bs_CHAddress_Eng) {
		Zone4_03BS_CHAddress_Eng = zone4_03bs_CHAddress_Eng;
	}

	public String getZone4_04BS_DocNum_Eng() {
		return Zone4_04BS_DocNum_Eng;
	}

	public void setZone4_04BS_DocNum_Eng(String zone4_04bs_DocNum_Eng) {
		Zone4_04BS_DocNum_Eng = zone4_04bs_DocNum_Eng;
	}

	public String getZone4_05BS_IDNum_Eng() {
		return Zone4_05BS_IDNum_Eng;
	}

	public void setZone4_05BS_IDNum_Eng(String zone4_05bs_IDNum_Eng) {
		Zone4_05BS_IDNum_Eng = zone4_05bs_IDNum_Eng;
	}

	public String getZone4_01BS_Phone_Alt() {
		return Zone4_01BS_Phone_Alt;
	}

	public void setZone4_01BS_Phone_Alt(String zone4_01bs_Phone_Alt) {
		Zone4_01BS_Phone_Alt = zone4_01bs_Phone_Alt;
	}

	public String getZone4_02BS_DOB_Alt() {
		return Zone4_02BS_DOB_Alt;
	}

	public void setZone4_02BS_DOB_Alt(String zone4_02bs_DOB_Alt) {
		Zone4_02BS_DOB_Alt = zone4_02bs_DOB_Alt;
	}

	public String getZone4_03BS_CHAddress_Alt() {
		return Zone4_03BS_CHAddress_Alt;
	}

	public void setZone4_03BS_CHAddress_Alt(String zone4_03bs_CHAddress_Alt) {
		Zone4_03BS_CHAddress_Alt = zone4_03bs_CHAddress_Alt;
	}

	public String getZone4_04BS_DocNum_Alt() {
		return Zone4_04BS_DocNum_Alt;
	}

	public void setZone4_04BS_DocNum_Alt(String zone4_04bs_DocNum_Alt) {
		Zone4_04BS_DocNum_Alt = zone4_04bs_DocNum_Alt;
	}

	public String getZone4_05BS_IDNum_Alt() {
		return Zone4_05BS_IDNum_Alt;
	}

	public void setZone4_05BS_IDNum_Alt(String zone4_05bs_IDNum_Alt) {
		Zone4_05BS_IDNum_Alt = zone4_05bs_IDNum_Alt;
	}

	public String getZone4_01BV_Phone() {
		return Zone4_01BV_Phone;
	}

	public void setZone4_01BV_Phone(String zone4_01bv_Phone) {
		Zone4_01BV_Phone = zone4_01bv_Phone;
	}

	public String getZone4_02BV_Birthdate() {
		return Zone4_02BV_Birthdate;
	}

	public void setZone4_02BV_Birthdate(String zone4_02bv_Birthdate) {
		Zone4_02BV_Birthdate = zone4_02bv_Birthdate;
	}

	public String getZone4_03BV_CHAddress1() {
		return Zone4_03BV_CHAddress1;
	}

	public void setZone4_03BV_CHAddress1(String zone4_03bv_CHAddress1) {
		Zone4_03BV_CHAddress1 = zone4_03bv_CHAddress1;
	}

	public String getZone4_03BV_CHAddress2() {
		return Zone4_03BV_CHAddress2;
	}

	public void setZone4_03BV_CHAddress2(String zone4_03bv_CHAddress2) {
		Zone4_03BV_CHAddress2 = zone4_03bv_CHAddress2;
	}

	public String getZone4_04BV_DocNum() {
		return Zone4_04BV_DocNum;
	}

	public void setZone4_04BV_DocNum(String zone4_04bv_DocNum) {
		Zone4_04BV_DocNum = zone4_04bv_DocNum;
	}

	public String getZone4_05BV_IDNum() {
		return Zone4_05BV_IDNum;
	}

	public void setZone4_05BV_IDNum(String zone4_05bv_IDNum) {
		Zone4_05BV_IDNum = zone4_05bv_IDNum;
	}

	public String getZone5_MRZ_TD1_L1() {
		return Zone5_MRZ_TD1_L1;
	}

	public void setZone5_MRZ_TD1_L1(String zone5_MRZ_TD1_L1) {
		Zone5_MRZ_TD1_L1 = zone5_MRZ_TD1_L1;
	}

	public String getZone5_MRZ_TD1_L2() {
		return Zone5_MRZ_TD1_L2;
	}

	public void setZone5_MRZ_TD1_L2(String zone5_MRZ_TD1_L2) {
		Zone5_MRZ_TD1_L2 = zone5_MRZ_TD1_L2;
	}

	public String getZone5_MRZ_TD1_L3() {
		return Zone5_MRZ_TD1_L3;
	}

	public void setZone5_MRZ_TD1_L3(String zone5_MRZ_TD1_L3) {
		Zone5_MRZ_TD1_L3 = zone5_MRZ_TD1_L3;
	}

	public String getZone4_06BV_QR() {
		return Zone4_06BV_QR;
	}

	public void setZone4_06BV_QR(String zone4_06bv_QR) {
		Zone4_06BV_QR = zone4_06bv_QR;
	}
	
}
