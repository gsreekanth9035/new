package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "device_action_status_relation")
public class DeviceActionStatusRelationEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "device_status_id")
	private DeviceStatusEntity deviceStatus;

	@ManyToOne
	@JoinColumn(name = "device_actions_id")
	private DeviceActionsEntity deviceActions;

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DeviceStatusEntity getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(DeviceStatusEntity deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public DeviceActionsEntity getDeviceActions() {
		return deviceActions;
	}

	public void setDeviceActions(DeviceActionsEntity deviceActions) {
		this.deviceActions = deviceActions;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
