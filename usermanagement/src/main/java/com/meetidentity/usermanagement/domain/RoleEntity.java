package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "role")
public class RoleEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "type")
	private String type;
	
	@Column(name = "color")
	private String color;

	@Version
	@Column(name = "version")
	private int version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
	private OrganizationEntity organization;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "workflow_id")
	private WorkflowEntity workflow;

	@OneToMany(mappedBy = "role", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<RolePermissionEntity> rolePermissions = new ArrayList<>();

	@OneToMany(mappedBy = "role", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<RoleDeviceActionsEntity> roleDeviceTypeActions = new ArrayList<>();

	@OneToMany(mappedBy = "role", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<RoleDeviceTypeEntity> roleDeviceTypes = new ArrayList<>();

	@OneToMany(mappedBy = "role", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<RoleAttributeEntity> roleAttributes = new ArrayList<>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

	public WorkflowEntity getWorkflow() {
		return workflow;
	}

	public void setWorkflow(WorkflowEntity workflow) {
		this.workflow = workflow;
	}

	public List<RolePermissionEntity> getRolePermissions() {
		return rolePermissions;
	}

	public void setRolePermissions(List<RolePermissionEntity> rolePermissions) {
		this.rolePermissions = rolePermissions;
	}

	public List<RoleDeviceActionsEntity> getRoleDeviceTypeActions() {
		return roleDeviceTypeActions;
	}

	public void setRoleDeviceTypeActions(List<RoleDeviceActionsEntity> roleDeviceTypeActions) {
		this.roleDeviceTypeActions = roleDeviceTypeActions;
	}

	public List<RoleDeviceTypeEntity> getRoleDeviceTypes() {
		return roleDeviceTypes;
	}

	public void setRoleDeviceTypes(List<RoleDeviceTypeEntity> roleDeviceTypes) {
		this.roleDeviceTypes = roleDeviceTypes;
	}
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public List<RoleAttributeEntity> getRoleAttributes() {
		return roleAttributes;
	}

	public void setRoleAttributes(List<RoleAttributeEntity> roleAttributes) {
		this.roleAttributes = roleAttributes;
	}

}
