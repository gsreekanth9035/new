package com.meetidentity.usermanagement.domain.visual.credentials.eid;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Iterator;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.constants.XMLConstants;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserAttributeEntity;
import com.meetidentity.usermanagement.domain.UserEmployeeIDEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserEmployeeID;

@Component
public class UtilityEid {

	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;

	private final Logger log = LoggerFactory.getLogger(UtilityEid.class);

	@Autowired
	private Util util;
	
	
	private final Logger logger = LoggerFactory.getLogger(UtilityEid.class);
	private static String XLINK_NS = XMLConstants.XLINK_NAMESPACE_URI;
	private static String XLINK_HREF = "xlink:href";

	/**
	 * 
	 * @param requestFrom 
	 * @param sAXSVGDocumentFactory
	 * @param b 
	 * @param userEntity 
	 * @param organizationName 
	 * @return
	 * @throws Exception
	 */
	public Document parseEidFront(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user, byte[] svgFile,
			byte[] jsonFile, String credentialTemplateID, UserEmployeeIDEntity userEmployeeIDEntity, String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {

				JSONObject eidJsonObject = new JSONObject(new String(jsonFile));
				JSONObject eidFrontJsonObject = eidJsonObject.getJSONObject("userEIDfront");
				Iterator<String> keys = eidFrontJsonObject.keys();
				log.debug("parseEIDFront: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				// UserEid usereid = gson.fromJson(new String(jsonFile), UserEid.class);
				// Dynamic User Variables
				switch (requestFrom) {

					case "web_portal_enroll_summary":		
						UserAttribute userAttribute = user.getUserAttribute();
						UserEmployeeID userEmployeeID = user.getUserEmployeeID();
						doc.getElementById("Zone2_01FV_FirstName").setTextContent(userAttribute.getFirstName());
						doc.getElementById("Zone2_01FV_LastName").setTextContent(userAttribute.getLastName());
						doc.getElementById("Zone2_03FV_IDNumber").setTextContent("XXXXXXXXXX");
						if(userEmployeeID != null) {
							doc.getElementById("Zone2_02FV_Designation").setTextContent(userEmployeeID.getAffiliation());
							doc.getElementById("Zone2_04FV_Department_Name").setTextContent(userEmployeeID.getDepartment());
						}
						doc.getElementById("Zone2_05FV_DOI").setTextContent("XX/XX/XXXX");
						doc.getElementById("Zone2_06FV_DOE").setTextContent("XX/XX/XXXX");
						 doc.getElementById("Zone2_FV_QR")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + user.getId() + "#" + credentialTemplateID + "#" + "XXXXXXXXXX",
											BarcodeFormat.QR_CODE, 300, 300))));
						 user.getUserBiometrics().stream().forEach(userBio -> {
								if (userBio.getType().equalsIgnoreCase("FACE")) {
									
									int width = Integer.parseInt(doc.getElementById("Zone3_01FV_Photograph").getAttribute("width"));
									int height = Integer.parseInt(doc.getElementById("Zone3_01FV_Photograph").getAttribute("height"));
									try {
										Image image = Util
												.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
										BufferedImage bufferedImage = Util.resizeImage(image, width, height);
										if(user.getIsTransparentPhotoRequired()) {
											bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
										}
										String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
										doc.getElementById("Zone3_01FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
												"data:;base64," + base64Data);

									} catch (IOException e) {
										doc.getElementById("Zone3_01FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									}
								}
							});
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":						
						UserAttributeEntity userAttributee = userEntity.getUserAttribute();		

						doc.getElementById("Zone2_01FV_FirstName").setTextContent(userAttributee.getFirstName());
						doc.getElementById("Zone2_01FV_LastName").setTextContent(userAttributee.getLastName());
						doc.getElementById("Zone2_03FV_IDNumber").setTextContent(userEmployeeIDEntity.getEmployeeIDNumber()==null?"XXXXXXXXXX":userEmployeeIDEntity.getEmployeeIDNumber());
						doc.getElementById("Zone2_02FV_Designation").setTextContent(userEmployeeIDEntity.getAffiliation()==null?"XXXXXXXXXX":userEmployeeIDEntity.getAffiliation());
						doc.getElementById("Zone2_04FV_Department_Name").setTextContent(userEmployeeIDEntity.getDepartment()==null?"XXXXXXXXXX":userEmployeeIDEntity.getDepartment());
						doc.getElementById("Zone2_05FV_DOI").setTextContent(userEmployeeIDEntity.getDateOfIssuance() == null? "XX/XX/XXXX" : Util.formatDate(userEmployeeIDEntity.getDateOfIssuance(), Util.DDMMYYYY_format));
						doc.getElementById("Zone2_06FV_DOE").setTextContent(userEmployeeIDEntity.getDateOfExpiry() == null? "XX/XX/XXXX" : Util.formatDate(userEmployeeIDEntity.getDateOfExpiry(), Util.DDMMYYYY_format));
						String employeeIDNumber = "XXXXXXXXXXXX";
						if(userEmployeeIDEntity.getEmployeeIDNumber()!=null) {
							employeeIDNumber = userEmployeeIDEntity.getEmployeeIDNumber();
						}
						doc.getElementById("Zone2_FV_QR")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + userEntity.getId() + "#" + credentialTemplateID +"#"+ employeeIDNumber,
											BarcodeFormat.QR_CODE, 300, 300))));
						 userEntity.getUserBiometrics().stream().forEach(userBio -> {
								if (userBio.getType().equalsIgnoreCase("FACE")) {
									
									int width = Integer.parseInt(doc.getElementById("Zone3_01FV_Photograph").getAttribute("width"));
									int height = Integer.parseInt(doc.getElementById("Zone3_01FV_Photograph").getAttribute("height"));
									try {
										Image image = Util
												.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
										BufferedImage bufferedImage = Util.resizeImage(image, width, height);
										if(user.getIsTransparentPhotoRequired()) {
											bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
										}
										String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
										doc.getElementById("Zone3_01FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
												"data:;base64," + base64Data);

									} catch (IOException e) {
										doc.getElementById("Zone3_01FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									}
								}
							});
						break;
					default :
				
				}
				
//				doc.getElementById("Zone0FS_Background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + usereid.getUserEIDfront().getZone0FS_Background());
//				doc.getElementById("Zone11FS_OrgSeal").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + usereid.getUserEIDfront().getZone11FS_OrgSeal());
		
	
				logger.debug("parseEidFront: Ended Dynamic text Variable");
	
				return doc;
			}
		
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType);
		}
	}

	/**
	 * 
	 * @param requestFrom 
	 * @param sAXSVGDocumentFactory
	 * @param b 
	 * @param userEntity 
	 * @param organizationName 
	 * @return
	 * @throws Exception
	 */
	public Document parseEidBack(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user, String credentialTemplateID,
			byte[] svgFile, byte[] jsonFile, UserEmployeeIDEntity userEmployeeIDEntity, String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {

				JSONObject eidJsonObject = new JSONObject(new String(jsonFile));
				JSONObject eidBacktJsonObject = eidJsonObject.getJSONObject("userEIDback");
				Iterator<String> keys = eidBacktJsonObject.keys();
				log.debug("parseEIDBack: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				// UserEid usereid = gson.fromJson(new String(jsonFile), UserEid.class);
				// Dynamic User Variables
				switch (requestFrom) {

					case "web_portal_enroll_summary":
						doc.getElementById("Zone4_02BV_CardNumber").setTextContent("XXXXXXXXXXXX");
						doc.getElementById("Zone4_01BV_IssuerID").setTextContent("XXXXXXXXXXXX");						
						user.getUserBiometrics().stream().forEach(userBio -> {
							if (userBio.getType().equalsIgnoreCase("FACE")) {
								
								int width = Integer.parseInt(doc.getElementById("Zone4_BV_Photoghost").getAttribute("width"));
								int height = Integer.parseInt(doc.getElementById("Zone4_BV_Photoghost").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
									doc.getElementById("Zone4_BV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);

								} catch (IOException e) {
									doc.getElementById("Zone4_BV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
						});
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":

						doc.getElementById("Zone4_02BV_CardNumber").setTextContent(userEmployeeIDEntity.getAgencyCardSn()==null?"XXXXXXXXXXXX":userEmployeeIDEntity.getAgencyCardSn());
						doc.getElementById("Zone4_01BV_IssuerID").setTextContent(userEmployeeIDEntity.getIssuerIN()==null?"XXXXXXXXXXXX":userEmployeeIDEntity.getIssuerIN());
						userEntity.getUserBiometrics().stream().forEach(userBio -> {
							if (userBio.getType().equalsIgnoreCase("FACE")) {
								
								int width = Integer.parseInt(doc.getElementById("Zone4_BV_Photoghost").getAttribute("width"));
								int height = Integer.parseInt(doc.getElementById("Zone4_BV_Photoghost").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
									doc.getElementById("Zone4_BV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);

								} catch (IOException e) {
									doc.getElementById("Zone4_BV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
						});
						break;
					default :
				
				}
				
//				doc.getElementById("Zone0BS_Background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + usereid.getUserEIDback().getZone0BS_Background());

		
	
				logger.debug("parseEidBack: Ended Dynamic text Variable");
	
				return doc;
			}
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType);
		}
	}
}
