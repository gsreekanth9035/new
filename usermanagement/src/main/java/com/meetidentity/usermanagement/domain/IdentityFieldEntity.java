package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="identity_field")
public class IdentityFieldEntity  extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
    @Column(name="name")
	private String name;
    
    @Column(name="display_name")
	private String displayName;

    @Column(name="description")
	private String description;
    
    @Column(name="type")
	private String type;
    
    @Column(name="mandatory")
	private boolean mandatory;

    @Version
    @Column(name="version")
    private int version;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="identity_type_id")
    private IdentityTypeEntity identityType;    

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="stake_holder_id")
    private ConfigValueEntity stakeHolder;
    
    @OneToMany(mappedBy="identityField", cascade=CascadeType.REMOVE, orphanRemoval=true)
    private List<IdentityFieldDropdownOptionEntity> identityFieldDropdownOptions = new ArrayList<>();    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public IdentityTypeEntity getIdentityType() {
		return identityType;
	}

	public void setIdentityType(IdentityTypeEntity identityType) {
		this.identityType = identityType;
	}

	public ConfigValueEntity getStakeHolder() {
		return stakeHolder;
	}

	public void setStakeHolder(ConfigValueEntity stakeHolder) {
		this.stakeHolder = stakeHolder;
	}

	public List<IdentityFieldDropdownOptionEntity> getIdentityFieldDropdownOptions() {
		return identityFieldDropdownOptions;
	}

	public void setIdentityFieldDropdownOptions(List<IdentityFieldDropdownOptionEntity> identityFieldDropdownOptions) {
		this.identityFieldDropdownOptions = identityFieldDropdownOptions;
	}    
       
}
