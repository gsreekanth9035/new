package com.meetidentity.usermanagement.domain.visual.credentials.mls;

import java.io.Serializable;

public class MLSTemplateBack  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String Zone9FS_Header;
	private String Zone5FS_EN;
	private String Zone10FS_Department;
	private String Zone13FS_Issued;
	private String Zone14FS_Expires;
	private String Zone11FS_OrgSeal;
	private String Zone1FV_Photograph;
	private String Zone14FV_Expires;
	private String Zone2FV_Last_Name;
	private String Zone5FV__EN;
	private String Zone2FV_First__Middle_Name;
	private String Zone6FV_Qrcode;
	private String Zone8FV_Affiliation;
	private String Zone10FV_Department;
	private String Zone1BS_Agency_CardSN;
	private String Zone2BS_IssuerIN;
	private String Zone1BV_Agency_CardSN;
	private String Zone2BV_IssuerIN;
	private String Zone_6BS_Ainfo_L1;
	private String Zone_6BS_Ainfo_L2;
	private String Zone_6BS_Ainfo_L3;
	private String Zone_6BS_Ainfo_L4;
	private String Zone4BS_ReturnAddress_L1;
	private String Zone4BS_ReturnAddress_L2;
	private String Zone4BS_ReturnAddress_L3;
	public String getZone9FS_Header() {
		return Zone9FS_Header;
	}
	public void setZone9FS_Header(String zone9fs_Header) {
		Zone9FS_Header = zone9fs_Header;
	}
	public String getZone5FS_EN() {
		return Zone5FS_EN;
	}
	public void setZone5FS_EN(String zone5fs_EN) {
		Zone5FS_EN = zone5fs_EN;
	}
	public String getZone10FS_Department() {
		return Zone10FS_Department;
	}
	public void setZone10FS_Department(String zone10fs_Department) {
		Zone10FS_Department = zone10fs_Department;
	}
	public String getZone13FS_Issued() {
		return Zone13FS_Issued;
	}
	public void setZone13FS_Issued(String zone13fs_Issued) {
		Zone13FS_Issued = zone13fs_Issued;
	}
	public String getZone14FS_Expires() {
		return Zone14FS_Expires;
	}
	public void setZone14FS_Expires(String zone14fs_Expires) {
		Zone14FS_Expires = zone14fs_Expires;
	}
	public String getZone11FS_OrgSeal() {
		return Zone11FS_OrgSeal;
	}
	public void setZone11FS_OrgSeal(String zone11fs_OrgSeal) {
		Zone11FS_OrgSeal = zone11fs_OrgSeal;
	}
	public String getZone1FV_Photograph() {
		return Zone1FV_Photograph;
	}
	public void setZone1FV_Photograph(String zone1fv_Photograph) {
		Zone1FV_Photograph = zone1fv_Photograph;
	}
	public String getZone14FV_Expires() {
		return Zone14FV_Expires;
	}
	public void setZone14FV_Expires(String zone14fv_Expires) {
		Zone14FV_Expires = zone14fv_Expires;
	}
	public String getZone2FV_Last_Name() {
		return Zone2FV_Last_Name;
	}
	public void setZone2FV_Last_Name(String zone2fv_Last_Name) {
		Zone2FV_Last_Name = zone2fv_Last_Name;
	}
	public String getZone5FV__EN() {
		return Zone5FV__EN;
	}
	public void setZone5FV__EN(String zone5fv__EN) {
		Zone5FV__EN = zone5fv__EN;
	}
	public String getZone2FV_First__Middle_Name() {
		return Zone2FV_First__Middle_Name;
	}
	public void setZone2FV_First__Middle_Name(String zone2fv_First__Middle_Name) {
		Zone2FV_First__Middle_Name = zone2fv_First__Middle_Name;
	}
	public String getZone6FV_Qrcode() {
		return Zone6FV_Qrcode;
	}
	public void setZone6FV_Qrcode(String zone6fv_Qrcode) {
		Zone6FV_Qrcode = zone6fv_Qrcode;
	}
	public String getZone8FV_Affiliation() {
		return Zone8FV_Affiliation;
	}
	public void setZone8FV_Affiliation(String zone8fv_Affiliation) {
		Zone8FV_Affiliation = zone8fv_Affiliation;
	}
	public String getZone10FV_Department() {
		return Zone10FV_Department;
	}
	public void setZone10FV_Department(String zone10fv_Department) {
		Zone10FV_Department = zone10fv_Department;
	}
	public String getZone1BS_Agency_CardSN() {
		return Zone1BS_Agency_CardSN;
	}
	public void setZone1BS_Agency_CardSN(String zone1bs_Agency_CardSN) {
		Zone1BS_Agency_CardSN = zone1bs_Agency_CardSN;
	}
	public String getZone2BS_IssuerIN() {
		return Zone2BS_IssuerIN;
	}
	public void setZone2BS_IssuerIN(String zone2bs_IssuerIN) {
		Zone2BS_IssuerIN = zone2bs_IssuerIN;
	}
	public String getZone1BV_Agency_CardSN() {
		return Zone1BV_Agency_CardSN;
	}
	public void setZone1BV_Agency_CardSN(String zone1bv_Agency_CardSN) {
		Zone1BV_Agency_CardSN = zone1bv_Agency_CardSN;
	}
	public String getZone2BV_IssuerIN() {
		return Zone2BV_IssuerIN;
	}
	public void setZone2BV_IssuerIN(String zone2bv_IssuerIN) {
		Zone2BV_IssuerIN = zone2bv_IssuerIN;
	}
	public String getZone_6BS_Ainfo_L1() {
		return Zone_6BS_Ainfo_L1;
	}
	public void setZone_6BS_Ainfo_L1(String zone_6bs_Ainfo_L1) {
		Zone_6BS_Ainfo_L1 = zone_6bs_Ainfo_L1;
	}
	public String getZone_6BS_Ainfo_L2() {
		return Zone_6BS_Ainfo_L2;
	}
	public void setZone_6BS_Ainfo_L2(String zone_6bs_Ainfo_L2) {
		Zone_6BS_Ainfo_L2 = zone_6bs_Ainfo_L2;
	}
	public String getZone_6BS_Ainfo_L3() {
		return Zone_6BS_Ainfo_L3;
	}
	public void setZone_6BS_Ainfo_L3(String zone_6bs_Ainfo_L3) {
		Zone_6BS_Ainfo_L3 = zone_6bs_Ainfo_L3;
	}
	public String getZone_6BS_Ainfo_L4() {
		return Zone_6BS_Ainfo_L4;
	}
	public void setZone_6BS_Ainfo_L4(String zone_6bs_Ainfo_L4) {
		Zone_6BS_Ainfo_L4 = zone_6bs_Ainfo_L4;
	}
	public String getZone4BS_ReturnAddress_L1() {
		return Zone4BS_ReturnAddress_L1;
	}
	public void setZone4BS_ReturnAddress_L1(String zone4bs_ReturnAddress_L1) {
		Zone4BS_ReturnAddress_L1 = zone4bs_ReturnAddress_L1;
	}
	public String getZone4BS_ReturnAddress_L2() {
		return Zone4BS_ReturnAddress_L2;
	}
	public void setZone4BS_ReturnAddress_L2(String zone4bs_ReturnAddress_L2) {
		Zone4BS_ReturnAddress_L2 = zone4bs_ReturnAddress_L2;
	}
	public String getZone4BS_ReturnAddress_L3() {
		return Zone4BS_ReturnAddress_L3;
	}
	public void setZone4BS_ReturnAddress_L3(String zone4bs_ReturnAddress_L3) {
		Zone4BS_ReturnAddress_L3 = zone4bs_ReturnAddress_L3;
	}
}
