package com.meetidentity.usermanagement.domain.visual.credentials.dl;

import java.io.Serializable;

public class UserDL implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserDLFront dlFront;
	private UserDLBack dlBack;
	public UserDLFront getDlFront() {
		return dlFront;
	}
	public void setDlFront(UserDLFront dlFront) {
		this.dlFront = dlFront;
	}
	public UserDLBack getDlBack() {
		return dlBack;
	}
	public void setDlBack(UserDLBack dlBack) {
		this.dlBack = dlBack;
	}
	
	

}
