package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="org_identity_field")
public class OrgIdentityFieldEntity  extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
    @Column(name="ldap_mapping_attribute")
	private String ldapMappingAttribute;
    
    @Version
    @Column(name="version")
    private int version;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="identity_field_id")
    private IdentityFieldEntity identityField;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="organization_id")
    private OrganizationEntity organization;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLdapMappingAttribute() {
		return ldapMappingAttribute;
	}

	public void setLdapMappingAttribute(String ldapMappingAttribute) {
		this.ldapMappingAttribute = ldapMappingAttribute;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public IdentityFieldEntity getIdentityField() {
		return identityField;
	}

	public void setIdentityField(IdentityFieldEntity identityField) {
		this.identityField = identityField;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}
       
}
