package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "wf_mobile_id_step_trusted_identity")
public class WFMobileIDStepTrustedIdentityEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "wf_step_id")
	private WFStepEntity wfStep;

	@ManyToOne
	@JoinColumn(name = "org_identity_type_id")
	private OrganizationIdentitiesEntity organizationIdentities;

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WFStepEntity getWfStep() {
		return wfStep;
	}

	public void setWfStep(WFStepEntity wfStep) {
		this.wfStep = wfStep;
	}

	public OrganizationIdentitiesEntity getOrganizationIdentities() {
		return organizationIdentities;
	}

	public void setOrganizationIdentities(OrganizationIdentitiesEntity organizationIdentities) {
		this.organizationIdentities = organizationIdentities;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
