package com.meetidentity.usermanagement.domain.visual.credentials.dl;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.constants.XMLConstants;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserAddressEntity;
import com.meetidentity.usermanagement.domain.UserAppearanceEntity;
import com.meetidentity.usermanagement.domain.UserAttributeEntity;
import com.meetidentity.usermanagement.domain.UserDrivingLicenseEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.VehicleCategoryEntity;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.service.helper.UserCredentialsServiceHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAddress;
import com.meetidentity.usermanagement.web.rest.model.UserAppearance;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserDrivingLicense;

@Component
public class UtilityDL {

	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;

	private final Logger log = LoggerFactory.getLogger(UtilityDL.class);

	@Autowired
	private Util util;
	
	@Autowired
	private UserCredentialsServiceHelper userCredentialsServiceHelper;

	private static String XLINK_NS = XMLConstants.XLINK_NAMESPACE_URI;
	private static String XLINK_HREF = "xlink:href";

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @param dl
	 * @param visualTemplateType
	 * @param vc
	 * @return
	 * @throws Exception
	 */
	public Document parseDLFront(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user,
			byte[] svgFile, byte[] jsonFile, UserDrivingLicenseEntity userDrivingLicensee, List<VehicleCategoryEntity> list,
			String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {
				JSONObject dlJsonObject = new JSONObject(new String(jsonFile));
				JSONObject dlFrontJsonObject = dlJsonObject.getJSONObject("dlFront");
				Iterator<String> keys = dlFrontJsonObject.keys();
				log.debug("parseDLFront: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				// UserDL userDL = gson.fromJson(new String(jsonFile), UserDL.class);				
				// Dynamic User Variables
				doc.getElementById("Zone1_FV_Issuing_Org").setTextContent(organizationEntity.getIssuingAuthority());
				
				switch (requestFrom) {

					case "web_portal_enroll_summary":
						UserAttribute userAttribute = user.getUserAttribute();
						UserAddress userAddress = user.getUserAddress();
						UserDrivingLicense userDrivingLicense = user.getUserDrivingLicense();
						doc.getElementById("Zone2_2FV_Last_Name").setTextContent(userAttribute.getLastName());
						doc.getElementById("Zone2_1FV_First_Middle_Name").setTextContent(userAttribute.getFirstName());
						doc.getElementById("Zone2_4dFV_DLNumber").setTextContent("XXXXXXXXX" );
						doc.getElementById("Zone2_4dFV_DLNumber_2").setTextContent("XXXXXXXXX");
						
						//TODO DO not Delete this
//						Calendar c = Calendar.getInstance();
//						  c.setTime(userAttribute.getDateOfBirth());
//						  int year = c.get(Calendar.YEAR);
//						  int month = c.get(Calendar.MONTH) + 1;
//						  int date = c.get(Calendar.DATE);
//						int age = this.calculateAgeInYears(LocalDate.of(year, month, date) ,LocalDate.now());
//						if(age < 21) {
//							Instant dob = userAttribute.getDateOfBirth().toInstant();
//							
//							doc.getElementById("ZoneFV_UAge").setTextContent(Util.formatDate(ZonedDateTime.ofInstant(dob, ZoneOffset.UTC).plusYears(21).toInstant(),Util.DD_MMM_YYYY_format));
//						}else {
//							Element element = doc.getElementById("ZoneFS_UAge");
//							element.getParentNode().removeChild(element);
//							doc.normalize();
//							element = doc.getElementById("ZoneFV_UAge");
//							element.getParentNode().removeChild(element);
//							doc.normalize();
//						}

						doc.getElementById("Zone2_9FV_VClass").setTextContent(userDrivingLicense.getVehicleClass());
						doc.getElementById("Zone2_3aFV_POB").setTextContent(userAttribute.getPlaceOfBirth());
						doc.getElementById("Zone2_15FV_Sex").setTextContent(userAttribute.getGender());
						
						doc.getElementById("Zone2_4aFV_DOI").setTextContent("XX/XX/XXXX");
						doc.getElementById("Zone2_4bFV_DOE").setTextContent("XX/XX/XXXX");
						doc.getElementById("Zone2_3FV_DOB").setTextContent(Util.formatDate(userAttribute.getDateOfBirth(),Util.MMDDYYYY_format));
						doc.getElementById("Zone2_3FV_DOB_2").setTextContent(Util.formatDate(userAttribute.getDateOfBirth(),Util.MMDDYYYY_format));
						
						doc.getElementById("Zone2_5FV_DD").setTextContent("XXXXXXXXX");

						//TODO DO not Delete this						
//						if(userAttribute.isOrganDonor()) {							
//						}else {
//							Element element = doc.getElementById("ZoneFS_ODonor");
//							element.getParentNode().removeChild(element);
//							doc.normalize();
//							element = doc.getElementById("ZoneFV_ODonor");
//							element.getParentNode().removeChild(element);
//							doc.normalize();
//						}if(userAttribute.isVeteran()) {							
//						}else {
//							Element element = doc.getElementById("ZoneFV_VIndicator");
//							element.getParentNode().removeChild(element);
//							doc.normalize();
//						}
						
						user.getUserBiometrics().stream().forEach(userBio -> {
							if (userBio.getType().equalsIgnoreCase("FACE")) {
								
								int width = Integer.parseInt(doc.getElementById("Zone3_FV_Photograph").getAttribute("width"));
								int height = Integer.parseInt(doc.getElementById("Zone3_FV_Photograph").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
									doc.getElementById("Zone3_FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);

								} catch (IOException e) {
									doc.getElementById("Zone3_FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
								
								width = Integer.parseInt(doc.getElementById("Zone2_FV_Photoghost").getAttribute("width"));
								height = Integer.parseInt(doc.getElementById("Zone2_FV_Photoghost").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate_photoghost(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);

									doc.getElementById("Zone2_FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);


								} catch (IOException e) {
									doc.getElementById("Zone2_FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
							if(userBio.getType().equalsIgnoreCase("SIGNATURE")) {
									doc.getElementById("Zone3_FV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									doc.getElementById("Zone3_FV_Signature_2").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
							}
						});
						
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":
						UserAttributeEntity userAttributee = userEntity.getUserAttribute();
						UserAppearanceEntity userAppearancee = userEntity.getUserAppearance();
						UserAddressEntity userAddressEntity = userEntity.getUserAddress();
						String address1 = userCredentialsServiceHelper.getAddressFromAddressEntity_addressLine1(userAddressEntity)+userCredentialsServiceHelper.getAddressFromAddressEntity_addressLine2(userAddressEntity);
						doc.getElementById("Zone2_2FV_Last_Name").setTextContent(userAttributee.getLastName());
						doc.getElementById("Zone2_1FV_First_Middle_Name").setTextContent(userAttributee.getFirstName());
						//TODO DO not Delete this
//						Calendar c1 = Calendar.getInstance();
//						  c1.setTime(Date.from(userAttributee.getDateOfBirth()));
//						  int year1 = c1.get(Calendar.YEAR);
//						  int month1 = c1.get(Calendar.MONTH) + 1;
//						  int date1 = c1.get(Calendar.DATE);
//						int age1 = this.calculateAgeInYears(LocalDate.of(year1, month1, date1) ,LocalDate.now());
//						if(age1 < 21) {
//							Instant dob = userAttributee.getDateOfBirth();
//							
//							doc.getElementById("ZoneFV_UAge").setTextContent(Util.formatDate(ZonedDateTime.ofInstant(dob, ZoneOffset.UTC).plusYears(21).toInstant(),Util.DD_MMM_YYYY_format));
//						}else {
//							Element element = doc.getElementById("ZoneFS_UAge");
//							element.getParentNode().removeChild(element);
//							doc.normalize();
//							element = doc.getElementById("ZoneFV_UAge");
//							element.getParentNode().removeChild(element);
//							doc.normalize();
//						}
						doc.getElementById("Zone2_4aFV_DOI").setTextContent(userDrivingLicensee.getDateOfIssuance() == null? "XX/XX/XXXX" : Util.formatDate(userDrivingLicensee.getDateOfIssuance(), Util.MMDDYYYY_format));
						doc.getElementById("Zone2_4bFV_DOE").setTextContent(userDrivingLicensee.getDateOfExpiry() == null? "XX/XX/XXXX" : Util.formatDate(userDrivingLicensee.getDateOfExpiry(), Util.MMDDYYYY_format));
						doc.getElementById("Zone2_4dFV_DLNumber").setTextContent(userDrivingLicensee.getLicenseNumber() == null? "XXXXXXXXX" : userDrivingLicensee.getLicenseNumber());
						doc.getElementById("Zone2_4dFV_DLNumber_2").setTextContent(userDrivingLicensee.getLicenseNumber() == null? "XXXXXXXXX" : userDrivingLicensee.getLicenseNumber());

						doc.getElementById("Zone2_3FV_DOB").setTextContent(Util.formatDate(userAttributee.getDateOfBirth(),Util.MMDDYYYY_format));
						doc.getElementById("Zone2_3FV_DOB_2").setTextContent(Util.formatDate(userAttributee.getDateOfBirth(),Util.MMDDYYYY_format));
						doc.getElementById("Zone2_5FV_DD").setTextContent(userDrivingLicensee.getDocumentDescriminator() == null? "XXXXXXXXX" : userDrivingLicensee.getDocumentDescriminator());

						doc.getElementById("Zone2_9FV_VClass").setTextContent(userDrivingLicensee.getVehicleClass());
						doc.getElementById("Zone2_15FV_Sex").setTextContent(userAttributee.getGender());
						doc.getElementById("Zone2_3aFV_POB").setTextContent(userAttributee.getPlaceOfBirth());

						//TODO DO not Delete this						
//						if(userAttributee.isOrganDonor()) {							
//						}else {
//							Element element = doc.getElementById("ZoneFV_ODonor");
//							element.getParentNode().removeChild(element);
//							doc.normalize();
//							element = doc.getElementById("ZoneFS_ODonor");
//							element.getParentNode().removeChild(element);
//							doc.normalize();
//						}if(userAttributee.isVeteran()) {							
//						}else {
//							Element element = doc.getElementById("ZoneFV_VIndicator");
//							element.getParentNode().removeChild(element);
//							doc.normalize();
//						}
//						
						userEntity.getUserBiometrics().stream().forEach(userBio -> {
							if (userBio.getType().equalsIgnoreCase("FACE")) {
								
								int width = Integer.parseInt(doc.getElementById("Zone3_FV_Photograph").getAttribute("width"));
								int height = Integer.parseInt(doc.getElementById("Zone3_FV_Photograph").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
									doc.getElementById("Zone3_FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);

								} catch (IOException e) {
									doc.getElementById("Zone3_FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
								
								width = Integer.parseInt(doc.getElementById("Zone2_FV_Photoghost").getAttribute("width"));
								height = Integer.parseInt(doc.getElementById("Zone2_FV_Photoghost").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate_photoghost(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);

									doc.getElementById("Zone2_FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);


								} catch (IOException e) {
									doc.getElementById("Zone2_FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
							if(userBio.getType().equalsIgnoreCase("SIGNATURE")) {
									doc.getElementById("Zone3_FV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									doc.getElementById("Zone3_FV_Signature_2").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
							}
						});
						
						break;
					default :
				
				}
			
//				UserDLFront dlfront = userDL.getDlFront();
//				doc.getElementById("Zone0FS_Background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + dlfront.getZone0FS_Background());
//				doc.getElementById("Zone0_ORG").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + dlfront.getZone0_ORG());
//				doc.getElementById("Zone0_Agency_seal").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + dlfront.getZone0_Agency_seal());
					
				log.debug("parseDLFront: Ended Dynamic Variable");
			}
			return doc;			
		
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @param dl
	 * @param vc
	 * @return
	 * @throws Exception
	 */
	public Document parseDLBack(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user,
			String credentialTemplateID, byte[] svgFile, byte[] jsonFile, UserDrivingLicenseEntity userDrivingLicensee,
			List<VehicleCategoryEntity> list, String visualCredentialType,  boolean simplyParse) throws Exception {
		
		
		try {
			
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {
				JSONObject dlJsonObject = new JSONObject(new String(jsonFile));
				JSONObject dlBackJsonObject = dlJsonObject.getJSONObject("dlBack");
				Iterator<String> keys = dlBackJsonObject.keys();
				log.debug("parseDLBack: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {
				QRCodeHelper obj = new QRCodeHelper();
				
				// UserDL userDL = gson.fromJson(new String(jsonFile), UserDL.class);				
				// Dynamic User Variables
				switch (requestFrom) {

					case "web_portal_enroll_summary":
						UserDrivingLicense userDrivingLicense = user.getUserDrivingLicense();	
						UserAddress userAddress = user.getUserAddress();						
						UserAppearance userAppearance = user.getUserAppearance();				
						doc.getElementById("Zone4_5BV_DD").setTextContent("XXXXXXXXXXXXX");
						doc.getElementById("Zone4_9aBV_End").setTextContent(userDrivingLicense.getEndorsements());
						doc.getElementById("Zone4_18BV_Eyes").setTextContent(userAppearance.getEyeColor());
						doc.getElementById("Zone4_16BV_Height").setTextContent(userAppearance.getHeight());
						doc.getElementById("Zone4_12BV_Restrictions").setTextContent(userDrivingLicense.getRestrictions());
						doc.getElementById("Zone4_9BV_VClass").setTextContent(userDrivingLicense.getVehicleClass());
						
						String addressL1 = userCredentialsServiceHelper.getAddressFromAddressObj_addressLine1(userAddress);
						String addressL2 = userCredentialsServiceHelper.getAddressFromAddressObj_addressLine2(userAddress);
						if(addressL1 != null && addressL2 != null) {
							doc.getElementById("Zone2_8BV_CHAddress_1").setTextContent(addressL1);	
							doc.getElementById("Zone2_8BV_CHAddress_2").setTextContent(addressL2);	
						} else {
							//TODO need to add few more conditional setters in future
							doc.getElementById("Zone2_8BV_CHAddress_1").setTextContent("");	
							doc.getElementById("Zone2_8BV_CHAddress_2").setTextContent("");	
						}
						try {
							int width = Integer.parseInt(doc.getElementById("Zone4_5BV_QR").getAttribute("width"));
							int height = Integer.parseInt(doc.getElementById("Zone4_5BV_QR").getAttribute("height"));
							 doc.getElementById("Zone4_5BV_QR")
								.setAttributeNS(XLINK_NS, XLINK_HREF,
										"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
												organizationEntity.getName() + "#" + user.getId() + "#" + credentialTemplateID,
												BarcodeFormat.QR_CODE, width, height))));
							 
							width = Integer.parseInt(doc.getElementById("Zone5_1BV_ICN").getAttribute("width"));
							height = Integer.parseInt(doc.getElementById("Zone5_1BV_ICN").getAttribute("height"));						
							
							byte[] data = obj.generatePDF417(
									organizationEntity.getName() + "#" + user.getId() + "#" + credentialTemplateID + "#" + "XXXXXXXXXX", width, height);
							ByteArrayInputStream bis = new ByteArrayInputStream(data);
							Image image = ImageIO.read(bis);
							bis.close();
							BufferedImage bufferedImage = Util.resizeImage(image, width, height);
							String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
							doc.getElementById("Zone5_1BV_ICN").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + base64Data);

						} catch (IOException e) {
							// throw new RuntimeException(e);
						}
						user.getUserBiometrics().stream().forEach(userBio -> {
							if (userBio.getType().equalsIgnoreCase("FACE")) {
								
								int width = Integer.parseInt(doc.getElementById("Zone5_BV_Photoghost").getAttribute("width"));
								int height = Integer.parseInt(doc.getElementById("Zone5_BV_Photoghost").getAttribute("height"));

								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate_photoghost(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);

									doc.getElementById("Zone5_BV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);


								} catch (IOException e) {
									doc.getElementById("Zone5_BV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
						});
						
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":		
						UserDrivingLicenseEntity userDrivingLicenseEntity = userEntity.getUserDrivingLicenses().get(0);
						doc.getElementById("Zone4_5BV_DD").setTextContent(userDrivingLicenseEntity.getDocumentDescriminator() == null? "XXXXXXXXX" : userDrivingLicenseEntity.getDocumentDescriminator());
						doc.getElementById("Zone4_9aBV_End").setTextContent(userDrivingLicensee.getEndorsements());
						doc.getElementById("Zone4_18BV_Eyes").setTextContent(userEntity.getUserAppearance().getEyeColor());
						doc.getElementById("Zone4_16BV_Height").setTextContent(userEntity.getUserAppearance().getHeight());
						doc.getElementById("Zone4_12BV_Restrictions").setTextContent(userDrivingLicensee.getRestrictions().getDescription());
						doc.getElementById("Zone4_9BV_VClass").setTextContent(userDrivingLicensee.getVehicleClass());
						String addressL1_ = userCredentialsServiceHelper.getAddressFromAddressEntity_addressLine1(userEntity.getUserAddress());
						String addressL2_ = userCredentialsServiceHelper.getAddressFromAddressEntity_addressLine2(userEntity.getUserAddress());
						if(addressL1_ != null && addressL2_ != null) {
							doc.getElementById("Zone2_8BV_CHAddress_1").setTextContent(addressL1_);	
							doc.getElementById("Zone2_8BV_CHAddress_2").setTextContent(addressL2_);		
						} else {
							//TODO need to add few more conditional setters in future
							doc.getElementById("Zone2_8BV_CHAddress_1").setTextContent("");	
							doc.getElementById("Zone2_8BV_CHAddress_2").setTextContent("");	
						}
						try {
							int width = Integer.parseInt(doc.getElementById("Zone4_5BV_QR").getAttribute("width"));
							int height = Integer.parseInt(doc.getElementById("Zone4_5BV_QR").getAttribute("height"));
							String licenseNumber = "XXXXXXXXXXXX";
							if(userDrivingLicensee.getLicenseNumber() != null) {
								licenseNumber = userDrivingLicensee.getLicenseNumber();
							}
							 doc.getElementById("Zone4_5BV_QR")
								.setAttributeNS(XLINK_NS, XLINK_HREF,
										"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
												organizationEntity.getName() + "#" + userEntity.getId() + "#" + credentialTemplateID + "#" + licenseNumber,
												BarcodeFormat.QR_CODE, width, height))));
							 
							width = Integer.parseInt(doc.getElementById("Zone5_1BV_ICN").getAttribute("width"));
							height = Integer.parseInt(doc.getElementById("Zone5_1BV_ICN").getAttribute("height"));						
							
							byte[] data = obj.generatePDF417(
									organizationEntity.getName() + "#" + userEntity.getId() + "#" + credentialTemplateID, width, height);
							ByteArrayInputStream bis = new ByteArrayInputStream(data);
							Image image = ImageIO.read(bis);
							bis.close();
							BufferedImage bufferedImage = Util.resizeImage(image, width, height);
							String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
							doc.getElementById("Zone5_1BV_ICN").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + base64Data);

						} catch (IOException e) {
							// throw new RuntimeException(e);
						}
						
						userEntity.getUserBiometrics().stream().forEach(userBio -> {
							if (userBio.getType().equalsIgnoreCase("FACE")) {
								
								int width = Integer.parseInt(doc.getElementById("Zone5_BV_Photoghost").getAttribute("width"));
								int height = Integer.parseInt(doc.getElementById("Zone5_BV_Photoghost").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate_photoghost(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);

									doc.getElementById("Zone5_BV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);


								} catch (IOException e) {
									doc.getElementById("Zone5_BV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
						});
						
						break;
					default :
				
				}
			
//				UserDLBack dlback = userDL.getDlBack();
//				doc.getElementById("background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + dlback.getBackground());
					
				


				log.debug("parseDLBack: Ended Dynamic text Variable");
				return doc;		
			}		

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}

	public int calculateAgeInYears(LocalDate birthDate, LocalDate currentDate) {
		// validate inputs ...
		return Period.between(birthDate, currentDate).getYears();
	}
	
}
