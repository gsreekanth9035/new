package com.meetidentity.usermanagement.domain.visual.credentials.nid;

import java.io.Serializable;

public class UserNIDFront implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// static starts

	private String Zone0_FS_Background;

	private String Zone1_FS_Issuing_Org;

	private String Zone1_FS_Agency_seal;

	private String Zone1_FS_Doc_Type_Eng;

	// english
	private String Zone2_01FS_IDNumber_Eng;

	private String Zone2_02FS_Family_Name_Eng;

	private String Zone2_03FS_Given_Name_Eng;

	private String Zone2_05FS_Sex_Eng;

	private String Zone2_06FS_Nationality_Eng;

	private String Zone2_07FS_DOB_Eng;

	private String Zone2_08FS_Birthplace_Eng;

	private String Zone2_09FS_DocNum_Eng;

	private String Zone2_10FS_DOI_Eng;

	private String Zone2_11FS_DOE_Eng;

	private String Zone2_12FS_IssuingAuthority_Eng;

	// non english
	private String Zone2_01FS_IDNumber_Alt;

	private String Zone2_02FS_Family_Name_Alt;

	private String Zone2_03FS_Given_Name_Alt;

	private String Zone2_05FS_Sex_Alt;

	private String Zone2_06FS_Nationality_Alt;

	private String Zone2_07FS_DOB_Alt;

	private String Zone2_08FS_Birthplace_Alt;

	private String Zone2_09FS_DocNum_Alt;

	private String Zone2_10FS_DOI_Alt;

	private String Zone2_11FS_DOE_Alt;

	private String Zone2_12FS_IssuingAuthority_Alt;

	// static ends

	// variable starts
	private String Zone2_10FV_IDNumber;

	private String Zone2_02FV_Family_Name;

	private String Zone2_03FV_Given_Name;

	private String Zone2_05FV_Sex;

	private String Zone2_06FV_Nationality;

	private String Zone2_07FV_DOB;

	private String Zone2_08FV_Birthplace;

	private String Zone2_09FV_DocNum;

	private String Zone2_10FV_DOI;

	private String Zone2_11FV_DOE;

	private String Zone2_12FV_IssuingAuthority;

	private String Zone2_13FV_Photoghost;

	private String Zone2_14FV_Fingerprint_image;

	private String Zone3_01FV_Photograph;

	private String Zone3_02FV_Signature;

	private String Zone2_10FV_IDNumber_2;

	private String Zone3_02FV_Signature_2;

	private String Zone2_07FV_DOB_2;
	

	// variable ends

	public String getZone0_FS_Background() {
		return Zone0_FS_Background;
	}

	public void setZone0_FS_Background(String zone0_FS_Background) {
		Zone0_FS_Background = zone0_FS_Background;
	}

	public String getZone1_FS_Issuing_Org() {
		return Zone1_FS_Issuing_Org;
	}

	public void setZone1_FS_Issuing_Org(String zone1_FS_Issuing_Org) {
		Zone1_FS_Issuing_Org = zone1_FS_Issuing_Org;
	}

	public String getZone1_FS_Agency_seal() {
		return Zone1_FS_Agency_seal;
	}

	public void setZone1_FS_Agency_seal(String zone1_FS_Agency_seal) {
		Zone1_FS_Agency_seal = zone1_FS_Agency_seal;
	}

	public String getZone1_FS_Doc_Type_Eng() {
		return Zone1_FS_Doc_Type_Eng;
	}

	public void setZone1_FS_Doc_Type_Eng(String zone1_FS_Doc_Type_Eng) {
		Zone1_FS_Doc_Type_Eng = zone1_FS_Doc_Type_Eng;
	}

	public String getZone2_01FS_IDNumber_Eng() {
		return Zone2_01FS_IDNumber_Eng;
	}

	public void setZone2_01FS_IDNumber_Eng(String zone2_01fs_IDNumber_Eng) {
		Zone2_01FS_IDNumber_Eng = zone2_01fs_IDNumber_Eng;
	}

	public String getZone2_02FS_Family_Name_Eng() {
		return Zone2_02FS_Family_Name_Eng;
	}

	public void setZone2_02FS_Family_Name_Eng(String zone2_02fs_Family_Name_Eng) {
		Zone2_02FS_Family_Name_Eng = zone2_02fs_Family_Name_Eng;
	}

	public String getZone2_03FS_Given_Name_Eng() {
		return Zone2_03FS_Given_Name_Eng;
	}

	public void setZone2_03FS_Given_Name_Eng(String zone2_03fs_Given_Name_Eng) {
		Zone2_03FS_Given_Name_Eng = zone2_03fs_Given_Name_Eng;
	}

	public String getZone2_05FS_Sex_Eng() {
		return Zone2_05FS_Sex_Eng;
	}

	public void setZone2_05FS_Sex_Eng(String zone2_05fs_Sex_Eng) {
		Zone2_05FS_Sex_Eng = zone2_05fs_Sex_Eng;
	}

	public String getZone2_06FS_Nationality_Eng() {
		return Zone2_06FS_Nationality_Eng;
	}

	public void setZone2_06FS_Nationality_Eng(String zone2_06fs_Nationality_Eng) {
		Zone2_06FS_Nationality_Eng = zone2_06fs_Nationality_Eng;
	}

	public String getZone2_07FS_DOB_Eng() {
		return Zone2_07FS_DOB_Eng;
	}

	public void setZone2_07FS_DOB_Eng(String zone2_07fs_DOB_Eng) {
		Zone2_07FS_DOB_Eng = zone2_07fs_DOB_Eng;
	}

	public String getZone2_08FS_Birthplace_Eng() {
		return Zone2_08FS_Birthplace_Eng;
	}

	public void setZone2_08FS_Birthplace_Eng(String zone2_08fs_Birthplace_Eng) {
		Zone2_08FS_Birthplace_Eng = zone2_08fs_Birthplace_Eng;
	}

	public String getZone2_09FS_DocNum_Eng() {
		return Zone2_09FS_DocNum_Eng;
	}

	public void setZone2_09FS_DocNum_Eng(String zone2_09fs_DocNum_Eng) {
		Zone2_09FS_DocNum_Eng = zone2_09fs_DocNum_Eng;
	}

	public String getZone2_10FS_DOI_Eng() {
		return Zone2_10FS_DOI_Eng;
	}

	public void setZone2_10FS_DOI_Eng(String zone2_10fs_DOI_Eng) {
		Zone2_10FS_DOI_Eng = zone2_10fs_DOI_Eng;
	}

	public String getZone2_11FS_DOE_Eng() {
		return Zone2_11FS_DOE_Eng;
	}

	public void setZone2_11FS_DOE_Eng(String zone2_11fs_DOE_Eng) {
		Zone2_11FS_DOE_Eng = zone2_11fs_DOE_Eng;
	}

	public String getZone2_12FS_IssuingAuthority_Eng() {
		return Zone2_12FS_IssuingAuthority_Eng;
	}

	public void setZone2_12FS_IssuingAuthority_Eng(String zone2_12fs_IssuingAuthority_Eng) {
		Zone2_12FS_IssuingAuthority_Eng = zone2_12fs_IssuingAuthority_Eng;
	}

	public String getZone2_01FS_IDNumber_Alt() {
		return Zone2_01FS_IDNumber_Alt;
	}

	public void setZone2_01FS_IDNumber_Alt(String zone2_01fs_IDNumber_Alt) {
		Zone2_01FS_IDNumber_Alt = zone2_01fs_IDNumber_Alt;
	}

	public String getZone2_02FS_Family_Name_Alt() {
		return Zone2_02FS_Family_Name_Alt;
	}

	public void setZone2_02FS_Family_Name_Alt(String zone2_02fs_Family_Name_Alt) {
		Zone2_02FS_Family_Name_Alt = zone2_02fs_Family_Name_Alt;
	}

	public String getZone2_03FS_Given_Name_Alt() {
		return Zone2_03FS_Given_Name_Alt;
	}

	public void setZone2_03FS_Given_Name_Alt(String zone2_03fs_Given_Name_Alt) {
		Zone2_03FS_Given_Name_Alt = zone2_03fs_Given_Name_Alt;
	}
	
	public String getZone2_05FS_Sex_Alt() {
		return Zone2_05FS_Sex_Alt;
	}

	public void setZone2_05FS_Sex_Alt(String zone2_05fs_Sex_Alt) {
		Zone2_05FS_Sex_Alt = zone2_05fs_Sex_Alt;
	}

	public String getZone2_06FS_Nationality_Alt() {
		return Zone2_06FS_Nationality_Alt;
	}

	public void setZone2_06FS_Nationality_Alt(String zone2_06fs_Nationality_Alt) {
		Zone2_06FS_Nationality_Alt = zone2_06fs_Nationality_Alt;
	}

	public String getZone2_07FS_DOB_Alt() {
		return Zone2_07FS_DOB_Alt;
	}

	public void setZone2_07FS_DOB_Alt(String zone2_07fs_DOB_Alt) {
		Zone2_07FS_DOB_Alt = zone2_07fs_DOB_Alt;
	}

	public String getZone2_08FS_Birthplace_Alt() {
		return Zone2_08FS_Birthplace_Alt;
	}

	public void setZone2_08FS_Birthplace_Alt(String zone2_08fs_Birthplace_Alt) {
		Zone2_08FS_Birthplace_Alt = zone2_08fs_Birthplace_Alt;
	}

	public String getZone2_09FS_DocNum_Alt() {
		return Zone2_09FS_DocNum_Alt;
	}

	public void setZone2_09FS_DocNum_Alt(String zone2_09fs_DocNum_Alt) {
		Zone2_09FS_DocNum_Alt = zone2_09fs_DocNum_Alt;
	}

	public String getZone2_10FS_DOI_Alt() {
		return Zone2_10FS_DOI_Alt;
	}

	public void setZone2_10FS_DOI_Alt(String zone2_10fs_DOI_Alt) {
		Zone2_10FS_DOI_Alt = zone2_10fs_DOI_Alt;
	}

	public String getZone2_11FS_DOE_Alt() {
		return Zone2_11FS_DOE_Alt;
	}

	public void setZone2_11FS_DOE_Alt(String zone2_11fs_DOE_Alt) {
		Zone2_11FS_DOE_Alt = zone2_11fs_DOE_Alt;
	}

	public String getZone2_12FS_IssuingAuthority_Alt() {
		return Zone2_12FS_IssuingAuthority_Alt;
	}

	public void setZone2_12FS_IssuingAuthority_Alt(String zone2_12fs_IssuingAuthority_Alt) {
		Zone2_12FS_IssuingAuthority_Alt = zone2_12fs_IssuingAuthority_Alt;
	}

	public String getZone2_10FV_IDNumber() {
		return Zone2_10FV_IDNumber;
	}

	public void setZone2_10FV_IDNumber(String zone2_10fv_IDNumber) {
		Zone2_10FV_IDNumber = zone2_10fv_IDNumber;
	}

	public String getZone2_02FV_Family_Name() {
		return Zone2_02FV_Family_Name;
	}

	public void setZone2_02FV_Family_Name(String zone2_02fv_Family_Name) {
		Zone2_02FV_Family_Name = zone2_02fv_Family_Name;
	}

	public String getZone2_03FV_Given_Name() {
		return Zone2_03FV_Given_Name;
	}

	public void setZone2_03FV_Given_Name(String zone2_03fv_Given_Name) {
		Zone2_03FV_Given_Name = zone2_03fv_Given_Name;
	}

	public String getZone2_05FV_Sex() {
		return Zone2_05FV_Sex;
	}

	public void setZone2_05FV_Sex(String zone2_05fv_Sex) {
		Zone2_05FV_Sex = zone2_05fv_Sex;
	}

	public String getZone2_06FV_Nationality() {
		return Zone2_06FV_Nationality;
	}

	public void setZone2_06FV_Nationality(String zone2_06fv_Nationality) {
		Zone2_06FV_Nationality = zone2_06fv_Nationality;
	}

	public String getZone2_07FV_DOB() {
		return Zone2_07FV_DOB;
	}

	public void setZone2_07FV_DOB(String zone2_07fv_DOB) {
		Zone2_07FV_DOB = zone2_07fv_DOB;
	}

	public String getZone2_08FV_Birthplace() {
		return Zone2_08FV_Birthplace;
	}

	public void setZone2_08FV_Birthplace(String zone2_08fv_Birthplace) {
		Zone2_08FV_Birthplace = zone2_08fv_Birthplace;
	}

	public String getZone2_09FV_DocNum() {
		return Zone2_09FV_DocNum;
	}

	public void setZone2_09FV_DocNum(String zone2_09fv_DocNum) {
		Zone2_09FV_DocNum = zone2_09fv_DocNum;
	}

	public String getZone2_10FV_DOI() {
		return Zone2_10FV_DOI;
	}

	public void setZone2_10FV_DOI(String zone2_10fv_DOI) {
		Zone2_10FV_DOI = zone2_10fv_DOI;
	}

	public String getZone2_11FV_DOE() {
		return Zone2_11FV_DOE;
	}

	public void setZone2_11FV_DOE(String zone2_11fv_DOE) {
		Zone2_11FV_DOE = zone2_11fv_DOE;
	}

	public String getZone2_12FV_IssuingAuthority() {
		return Zone2_12FV_IssuingAuthority;
	}

	public void setZone2_12FV_IssuingAuthority(String zone2_12fv_IssuingAuthority) {
		Zone2_12FV_IssuingAuthority = zone2_12fv_IssuingAuthority;
	}

	public String getZone2_13FV_Photoghost() {
		return Zone2_13FV_Photoghost;
	}

	public void setZone2_13FV_Photoghost(String zone2_13fv_Photoghost) {
		Zone2_13FV_Photoghost = zone2_13fv_Photoghost;
	}

	public String getZone2_14FV_Fingerprint_image() {
		return Zone2_14FV_Fingerprint_image;
	}

	public void setZone2_14FV_Fingerprint_image(String zone2_14fv_Fingerprint_image) {
		Zone2_14FV_Fingerprint_image = zone2_14fv_Fingerprint_image;
	}

	public String getZone3_01FV_Photograph() {
		return Zone3_01FV_Photograph;
	}

	public void setZone3_01FV_Photograph(String zone3_01fv_Photograph) {
		Zone3_01FV_Photograph = zone3_01fv_Photograph;
	}

	public String getZone3_02FV_Signature() {
		return Zone3_02FV_Signature;
	}

	public void setZone3_02FV_Signature(String zone3_02fv_Signature) {
		Zone3_02FV_Signature = zone3_02fv_Signature;
	}

	public String getZone2_10FV_IDNumber_2() {
		return Zone2_10FV_IDNumber_2;
	}

	public void setZone2_10FV_IDNumber_2(String zone2_10fv_IDNumber_2) {
		Zone2_10FV_IDNumber_2 = zone2_10fv_IDNumber_2;
	}

	public String getZone3_02FV_Signature_2() {
		return Zone3_02FV_Signature_2;
	}

	public void setZone3_02FV_Signature_2(String zone3_02fv_Signature_2) {
		Zone3_02FV_Signature_2 = zone3_02fv_Signature_2;
	}

	public String getZone2_07FV_DOB_2() {
		return Zone2_07FV_DOB_2;
	}

	public void setZone2_07FV_DOB_2(String zone2_07fv_DOB_2) {
		Zone2_07FV_DOB_2 = zone2_07fv_DOB_2;
	}
	
	

}
