package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="identity_field_dropdown_option")
public class IdentityFieldDropdownOptionEntity  extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
    @Column(name="value")
	private String value;
    
    @Column(name="display_name")
	private String displayName;

    @Column(name="active")
	private boolean active;

    @Version
    @Column(name="version")
    private int version;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="identity_field_id")
    private IdentityFieldEntity identityField;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public IdentityFieldEntity getIdentityField() {
		return identityField;
	}

	public void setIdentityField(IdentityFieldEntity identityField) {
		this.identityField = identityField;
	}    
       
}
