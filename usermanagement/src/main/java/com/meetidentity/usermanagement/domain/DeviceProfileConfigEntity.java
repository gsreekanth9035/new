package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "device_profile_config")
public class DeviceProfileConfigEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne
	@JoinColumn(name = "config_value_id")
	private ConfigValueEntity configValue;

	@Column(name = "enable_key_manager")
	private Boolean enableKeyManager;

	@Column(name = "enable_diversify")
	private Boolean enableDiversify;

	@Column(name = "enable_master_key")
	private Boolean enableMasterKey;

	@Column(name = "enable_ak_is_identical_to_mk_check")
	private Boolean enableAkIsIdenticalToMkCheck;

	@Column(name = "enable_admin_key")
	private Boolean enableAdminKey;

	@Column(name = "enable_customer_master_key")
	private Boolean enableCustomerMasterKey;
	
	@Column(name = "enable_customer_admin_key")
	private Boolean enableCustomerAdminKey;

	@Column(name = "enable_factory_management_key")
	private Boolean enableFactoryManagementKey;
	
	@Column(name = "enable_customer_management_key")
	private Boolean enableCustomerManagementKey;

	@Column(name = "key_length")
	private Integer keyLength;
	
	@Column(name = "master_key_length")
	private Integer masterKeyLength;
	
	@Column(name = "is_mobile_id")
	private Boolean isMobileId;

	@Column(name = "is_plastic_id")
	private Boolean isPlasticId;
	
	@Column(name = "is_rfid_card")
	private Boolean isRfidCard;
	
	@Column(name = "is_applet_loading_required")
	private Boolean isAppletLoadingRequired;
	
	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ConfigValueEntity getConfigValue() {
		return configValue;
	}

	public void setConfigValue(ConfigValueEntity configValue) {
		this.configValue = configValue;
	}

	public Boolean getEnableKeyManager() {
		return enableKeyManager;
	}

	public void setEnableKeyManager(Boolean enableKeyManager) {
		this.enableKeyManager = enableKeyManager;
	}

	public Boolean getEnableDiversify() {
		return enableDiversify;
	}

	public void setEnableDiversify(Boolean enableDiversify) {
		this.enableDiversify = enableDiversify;
	}

	public Boolean getEnableMasterKey() {
		return enableMasterKey;
	}

	public void setEnableMasterKey(Boolean enableMasterKey) {
		this.enableMasterKey = enableMasterKey;
	}

	public Boolean getEnableAkIsIdenticalToMkCheck() {
		return enableAkIsIdenticalToMkCheck;
	}

	public void setEnableAkIsIdenticalToMkCheck(Boolean enableAkIsIdenticalToMkCheck) {
		this.enableAkIsIdenticalToMkCheck = enableAkIsIdenticalToMkCheck;
	}

	public Boolean getEnableAdminKey() {
		return enableAdminKey;
	}

	public void setEnableAdminKey(Boolean enableAdminKey) {
		this.enableAdminKey = enableAdminKey;
	}

	public Boolean getEnableCustomerMasterKey() {
		return enableCustomerMasterKey;
	}

	public void setEnableCustomerMasterKey(Boolean enableCustomerMasterKey) {
		this.enableCustomerMasterKey = enableCustomerMasterKey;
	}

	public Boolean getIsMobileId() {
		return isMobileId;
	}

	public void setIsMobileId(Boolean isMobileId) {
		this.isMobileId = isMobileId;
	}

	public Boolean getIsPlasticId() {
		return isPlasticId;
	}

	public void setIsPlasticId(Boolean isPlasticId) {
		this.isPlasticId = isPlasticId;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Integer getKeyLength() {
		return keyLength;
	}

	public void setKeyLength(Integer keyLength) {
		this.keyLength = keyLength;
	}
	
	public Boolean getEnableCustomerAdminKey() {
		return enableCustomerAdminKey;
	}

	public void setEnableCustomerAdminKey(Boolean enableCustomerAdminKey) {
		this.enableCustomerAdminKey = enableCustomerAdminKey;
	}

	public Boolean getEnableFactoryManagementKey() {
		return enableFactoryManagementKey;
	}

	public void setEnableFactoryManagementKey(Boolean enableFactoryManagementKey) {
		this.enableFactoryManagementKey = enableFactoryManagementKey;
	}

	public Boolean getEnableCustomerManagementKey() {
		return enableCustomerManagementKey;
	}

	public void setEnableCustomerManagementKey(Boolean enableCustomerManagementKey) {
		this.enableCustomerManagementKey = enableCustomerManagementKey;
	}

	public Integer getMasterKeyLength() {
		return masterKeyLength;
	}

	public void setMasterKeyLength(Integer masterKeyLength) {
		this.masterKeyLength = masterKeyLength;
	}

	public Boolean getIsRfidCard() {
		return isRfidCard;
	}

	public void setIsRfidCard(Boolean isRfidCard) {
		this.isRfidCard = isRfidCard;
	}

	public Boolean getIsAppletLoadingRequired() {
		return isAppletLoadingRequired;
	}

	public void setIsAppletLoadingRequired(Boolean isAppletLoadingRequired) {
		this.isAppletLoadingRequired = isAppletLoadingRequired;
	}

}
