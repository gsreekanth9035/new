package com.meetidentity.usermanagement.domain.visual.credentials.eid;

import java.io.Serializable;

public class UserEid implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public UserEIDfront userEIDfront;
	public UserEIDback userEIDback;
	public UserEIDfront getUserEIDfront() {
		return userEIDfront;
	}
	public void setUserEIDfront(UserEIDfront userEIDfront) {
		this.userEIDfront = userEIDfront;
	}
	public UserEIDback getUserEIDback() {
		return userEIDback;
	}
	public void setUserEIDback(UserEIDback userEIDback) {
		this.userEIDback = userEIDback;
	}
	
	

}
