package com.meetidentity.usermanagement.domain.visual.credentials.hid;

import java.io.Serializable;

public class UserHIDBack implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String ZoneXX_Background;
	private String ZoneXX_Org_CardSN;
	private String ZoneXX_IssuerIN;
	private String ZoneXX_ReturnAddress;
	private String ZoneXX_Ainfo;
	private String ZoneXXV_Org_CardSN;
	private String ZoneXXV_IssuerIN;
	private String ZoneXXV_Qrcode;
	
	public String getZoneXX_Background() {
		return ZoneXX_Background;
	}
	public void setZoneXX_Background(String zoneXX_Background) {
		ZoneXX_Background = zoneXX_Background;
	}
	public String getZoneXX_Org_CardSN() {
		return ZoneXX_Org_CardSN;
	}
	public void setZoneXX_Org_CardSN(String zoneXX_Org_CardSN) {
		ZoneXX_Org_CardSN = zoneXX_Org_CardSN;
	}
	public String getZoneXX_IssuerIN() {
		return ZoneXX_IssuerIN;
	}
	public void setZoneXX_IssuerIN(String zoneXX_IssuerIN) {
		ZoneXX_IssuerIN = zoneXX_IssuerIN;
	}
	public String getZoneXX_ReturnAddress() {
		return ZoneXX_ReturnAddress;
	}
	public void setZoneXX_ReturnAddress(String zoneXX_ReturnAddress) {
		ZoneXX_ReturnAddress = zoneXX_ReturnAddress;
	}
	public String getZoneXX_Ainfo() {
		return ZoneXX_Ainfo;
	}
	public void setZoneXX_Ainfo(String zoneXX_Ainfo) {
		ZoneXX_Ainfo = zoneXX_Ainfo;
	}
	public String getZoneXXV_Org_CardSN() {
		return ZoneXXV_Org_CardSN;
	}
	public void setZoneXXV_Org_CardSN(String zoneXXV_Org_CardSN) {
		ZoneXXV_Org_CardSN = zoneXXV_Org_CardSN;
	}
	public String getZoneXXV_IssuerIN() {
		return ZoneXXV_IssuerIN;
	}
	public void setZoneXXV_IssuerIN(String zoneXXV_IssuerIN) {
		ZoneXXV_IssuerIN = zoneXXV_IssuerIN;
	}
	public String getZoneXXV_Qrcode() {
		return ZoneXXV_Qrcode;
	}
	public void setZoneXXV_Qrcode(String zoneXXV_Qrcode) {
		ZoneXXV_Qrcode = zoneXXV_Qrcode;
	}
	
	
	
}
