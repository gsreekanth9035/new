package com.meetidentity.usermanagement.domain.visual.credentials.piv;

import java.io.Serializable;

public class UserPIVID implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserPivFront pivFront;
	private UserPivBack pivBack;
	
	
	public UserPivFront getPivFront() {
		return pivFront;
	}
	public void setPivFront(UserPivFront pivFront) {
		this.pivFront = pivFront;
	}
	public UserPivBack getPivBack() {
		return pivBack;
	}
	public void setPivBack(UserPivBack pivBack) {
		this.pivBack = pivBack;
	} 
	
	

}
