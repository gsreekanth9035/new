package com.meetidentity.usermanagement.domain.visual.credentials.sid;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Iterator;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.constants.XMLConstants;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserAttributeEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserStudentIDEntity;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserStudentID;

@Component
public class UtilitySID {

	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;

	private final Logger log = LoggerFactory.getLogger(UtilitySID.class);

	@Autowired
	private Util util;
	
	
	private final Logger logger = LoggerFactory.getLogger(UtilitySID.class);
	private static String XLINK_NS = XMLConstants.XLINK_NAMESPACE_URI;
	private static String XLINK_HREF = "xlink:href";

	/**
	 * 
	 * @param requestFrom 
	 * @param sAXSVGDocumentFactory
	 * @param b 
	 * @param userEntity 
	 * @param organizationName 
	 * @return
	 * @throws Exception
	 */
	public Document parseSidFront(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user, byte[] svgFile,
			byte[] jsonFile, String credentialTemplateID, UserStudentIDEntity userStudentIDEntity, String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {

				JSONObject sidJsonObject = new JSONObject(new String(jsonFile));
				JSONObject sidFrontJsonObject = sidJsonObject.getJSONObject("userSIDfront");
				Iterator<String> keys = sidFrontJsonObject.keys();
				log.debug("parseSIDFront: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					//log.debug(key);
				      if(doc.getElementById(key) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS_WITH_KEY, visualCredentialType,key));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS_WITH_KEY, visualCredentialType,key));
				      };
				}
				return doc ;
			}else {

				// UserSID userSID = gson.fromJson(new String(jsonFile), UserSID.class);
				// Dynamic User Variables
				switch (requestFrom) {

					case "web_portal_enroll_summary":		
						UserAttribute userAttribute = user.getUserAttribute();
						UserStudentID userStudentID = user.getUserStudentID();		
						
						doc.getElementById("Zone_FV_Country").setTextContent(userAttribute.getNationality());						
						doc.getElementById("Zone_FV_Name").setTextContent(userAttribute.getLastName()+" "+userAttribute.getFirstName());						
						doc.getElementById("Zone5FV_EN").setTextContent("XXXXXXXXXX");
						doc.getElementById("Zone8FV_Institution").setTextContent(organizationEntity.getDescription());
						doc.getElementById("Zone10FV_Department").setTextContent(userStudentID.getDepartment());
						doc.getElementById("Zone13aFV_DOB").setTextContent(userAttribute.getDateOfBirth() == null? "XX/XX/XXXX" : Util.formatDate(userAttribute.getDateOfBirth(), Util.DDMMYYYY_format));
						doc.getElementById("Zone13FV_Issued").setTextContent("XX/XX/XXXX");
						doc.getElementById("Zone14FV_Expires").setTextContent("XX/XX/XXXX");
						 user.getUserBiometrics().stream().forEach(userBio -> {
								if (userBio.getType().equalsIgnoreCase("FACE")) {
									
									int width = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("width"));
									int height = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("height"));
									try {
										Image image = Util
												.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
										BufferedImage bufferedImage = Util.resizeImage(image, width, height);
										if(user.getIsTransparentPhotoRequired()) {
											bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
										}
										String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
										doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
												"data:;base64," + base64Data);

									} catch (IOException e) {
										doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									}
								}
								if (userBio.getType().equalsIgnoreCase("SIGNATURE")) {
									doc.getElementById("Zone15FV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							});
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":						
						UserAttributeEntity userAttributee = userEntity.getUserAttribute();		
						
						doc.getElementById("Zone_FV_Country").setTextContent(userAttributee.getNationality());						
						doc.getElementById("Zone_FV_Name").setTextContent(userAttributee.getLastName()+" "+userAttributee.getFirstName());
						doc.getElementById("Zone5FV_EN").setTextContent(userStudentIDEntity.getStudentIdNumber() ==null?"XXXXXXXXXX":userStudentIDEntity.getStudentIdNumber());
						doc.getElementById("Zone8FV_Institution").setTextContent(organizationEntity.getDescription());
						doc.getElementById("Zone10FV_Department").setTextContent(userStudentIDEntity.getDepartment()==null?"XXXXXXXXXX":userStudentIDEntity.getDepartment());
						doc.getElementById("Zone13aFV_DOB").setTextContent(userAttributee.getDateOfBirth() == null? "XX/XX/XXXX" : Util.formatDate(userAttributee.getDateOfBirth(), Util.DDMMYYYY_format));
						doc.getElementById("Zone13FV_Issued").setTextContent(userStudentIDEntity.getDateOfIssuance() == null? "XX/XX/XXXX" : Util.formatDate(userStudentIDEntity.getDateOfIssuance(), Util.DDMMYYYY_format));
						doc.getElementById("Zone14FV_Expires").setTextContent(userStudentIDEntity.getDateOfExpiry() == null? "XX/XX/XXXX" : Util.formatDate(userStudentIDEntity.getDateOfExpiry(), Util.DDMMYYYY_format));
						
						 userEntity.getUserBiometrics().stream().forEach(userBio -> {
								if (userBio.getType().equalsIgnoreCase("FACE")) {
									
									int width = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("width"));
									int height = Integer.parseInt(doc.getElementById("Zone1FV_Photograph").getAttribute("height"));
									try {
										Image image = Util
												.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
										BufferedImage bufferedImage = Util.resizeImage(image, width, height);
										if(user.getIsTransparentPhotoRequired()) {
											bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
										}
										String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
										doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
												"data:;base64," + base64Data);

									} catch (IOException e) {
										doc.getElementById("Zone1FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									}
								}
								if (userBio.getType().equalsIgnoreCase("SIGNATURE")) {
									doc.getElementById("Zone15FV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							});
						break;
					default :
				
				}
				
//				doc.getElementById("Zone0FS_Background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + userSID.getUserSIDfront().getZone0FS_Background());
//				doc.getElementById("Zone11FS_OrgSeal").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + userSID.getUserSIDfront().getZone11FS_OrgSeal());
		
	
				logger.debug("parseEidFront: Ended Dynamic text Variable");
	
				return doc;
			}
		
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType);
		}
	}

	/**
	 * 
	 * @param requestFrom 
	 * @param sAXSVGDocumentFactory
	 * @param b 
	 * @param userEntity 
	 * @param organizationName 
	 * @return
	 * @throws Exception
	 */
	public Document parseSidBack(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user, String credentialTemplateID,
			byte[] svgFile, byte[] jsonFile, UserStudentIDEntity userStudentIDEntity, String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {

				JSONObject sidJsonObject = new JSONObject(new String(jsonFile));
				JSONObject sidBacktJsonObject = sidJsonObject.getJSONObject("userSIDback");
				Iterator<String> keys = sidBacktJsonObject.keys();
				log.debug("parseSIDBack: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					//log.debug(key);
				      if(doc.getElementById(key) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS_WITH_KEY, visualCredentialType,key));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS_WITH_KEY, visualCredentialType,key));
				      };
				}
				return doc ;
			}else {

				// UserSID userSID = gson.fromJson(new String(jsonFile), UserSID.class);
				// Dynamic User Variables
				switch (requestFrom) {

					case "web_portal_enroll_summary":
						doc.getElementById("Zone1BV_Org_CardSN").setTextContent("XXXXXXXXXXXX");
						doc.getElementById("Zone2BV_IssuerIN").setTextContent("XXXXXXXXXXXX");						
						 doc.getElementById("Zone6BV_Qrcode")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + user.getId() + "#" + credentialTemplateID + "#" +  "XXXXXXXXXX",
											BarcodeFormat.QR_CODE, 300, 300))));
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":
						doc.getElementById("Zone1BV_Org_CardSN").setTextContent(userStudentIDEntity.getAgencyCardSn()==null?"XXXXXXXXXXXX":userStudentIDEntity.getAgencyCardSn());
						doc.getElementById("Zone2BV_IssuerIN").setTextContent(userStudentIDEntity.getIssuerIN()==null?"XXXXXXXXXXXX":userStudentIDEntity.getIssuerIN());
						String studentNumber = "XXXXXXXXXXXX";
						if(userStudentIDEntity.getStudentIdNumber() != null) {
							studentNumber = userStudentIDEntity.getStudentIdNumber();
						}
							
						doc.getElementById("Zone6BV_Qrcode")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + userEntity.getId() + "#" + credentialTemplateID+ "#" + studentNumber,
											BarcodeFormat.QR_CODE, 300, 300))));
						break;
					default :
				
				}
				
//				doc.getElementById("Zone0BS_Background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + userSID.getUserSIDback().getZone0BS_Background());

		
	
				logger.debug("parseEidBack: Ended Dynamic text Variable");
	
				return doc;
			}
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType);
		}
	}
}
