package com.meetidentity.usermanagement.domain.visual.credentials.mls;

import java.io.Serializable;

public class MLSTemplate implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private MLSTemplateFront mlsTemplateFront;
	private MLSTemplateBack mlsTemplateBack;
	
	public MLSTemplateFront getMlsTemplateFront() {
		return mlsTemplateFront;
	}
	public void setMlsTemplateFront(MLSTemplateFront mlsTemplateFront) {
		this.mlsTemplateFront = mlsTemplateFront;
	}
	public MLSTemplateBack getMlsTemplateBack() {
		return mlsTemplateBack;
	}
	public void setMlsTemplateBack(MLSTemplateBack mlsTemplateBack) {
		this.mlsTemplateBack = mlsTemplateBack;
	}
	
	

}
