package com.meetidentity.usermanagement.domain;


import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.meetidentity.usermanagement.annotations.SequenceValue;

@Entity
@Table(name = "user_employee_id")
public class UserEmployeeIDEntity extends AbstractAuditingEntity {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "uims_id", nullable = false)
	private String uimsId;

	@SequenceValue
	@Column(name = "employee_id_number")
	private String employeeIDNumber;

	@Column(name = "date_of_issuance")
	private Instant dateOfIssuance;

	@Column(name = "date_of_expiry")
	private Instant dateOfExpiry;	
	
	@Column(name = "affiliation")
	private String affiliation;
	
	@Column(name = "department")
	private String department;
	
	@Column(name = "issuer_in")
	private String issuerIN;
	
	@Column(name = "agency_card_sn")
	private String agencyCardSn;
	
	

	public UserEmployeeIDEntity(Long id, String uimsId, String employeeIDNumber, Instant dateOfIssuance,
			Instant dateOfExpiry, String affiliation, String department, String issuerIN, String agencyCardSn,
			UserEntity user) {
		super();
		this.id = id;
		this.uimsId = uimsId;
		this.employeeIDNumber = employeeIDNumber;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
		this.affiliation = affiliation;
		this.department = department;
		this.issuerIN = issuerIN;
		this.agencyCardSn = agencyCardSn;
		this.user = user;
	}

	public UserEmployeeIDEntity() {
		// TODO Auto-generated constructor stub
	}

	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	

	public Instant getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Instant dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public Instant getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Instant dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getEmployeeIDNumber() {
		return employeeIDNumber;
	}

	public void setEmployeeIDNumber(String employeeIDNumber) {
		this.employeeIDNumber = employeeIDNumber;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getIssuerIN() {
		return issuerIN;
	}

	public void setIssuerIN(String issuerIN) {
		this.issuerIN = issuerIN;
	}

	public String getAgencyCardSn() {
		return agencyCardSn;
	}

	public void setAgencyCardSn(String agencyCardSn) {
		this.agencyCardSn = agencyCardSn;
	}
	
	
}
