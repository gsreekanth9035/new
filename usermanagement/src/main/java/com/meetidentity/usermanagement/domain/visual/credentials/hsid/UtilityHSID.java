package com.meetidentity.usermanagement.domain.visual.credentials.hsid;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.constants.XMLConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserHealthServiceEntity;
import com.meetidentity.usermanagement.domain.UserHealthServiceVaccineInfoEntity;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserHealthService;
import com.meetidentity.usermanagement.web.rest.model.UserHealthServiceVaccineInfo;

@Component
public class UtilityHSID {

	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;
	
	private final Logger log = LoggerFactory.getLogger(UtilityHSID.class);

	private Util util = new Util();

	private static String XLINK_NS = XMLConstants.XLINK_NAMESPACE_URI;
	private static String XLINK_HREF = "xlink:href";

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @param userEntity 
	 * @param organizationName 
	 * @param status 
	 * @param hs
	 * @param b 
	 * @param visualTemplateType
	 * @param vc
	 * @return
	 * @throws Exception
	 */
	public Document parseHSIDFront(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user,
			int status, String credentialTemplateID, byte[] svgFile, byte[] jsonFile, UserHealthServiceEntity hs,
			String visualCredentialType, boolean b) throws VisualCredentialException {
		try {
			Gson gson = new Gson();
			UserHSID userHS = gson.fromJson(new String(jsonFile), UserHSID.class);

			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));

		
			doc.getElementById("background").setAttributeNS(XLINK_NS, XLINK_HREF,
					"data:;base64," + userHS.getHsIDFront().getBackground());
			doc.getElementById("ZoneXX_Logo").setAttributeNS(XLINK_NS, XLINK_HREF,
					"data:;base64," + userHS.getHsIDFront().getZoneXX_Logo());
			doc.getElementById("ZoneXX_Issuer_ID_Icon").setAttributeNS(XLINK_NS, XLINK_HREF,
					"data:;base64," + userHS.getHsIDFront().getZoneXX_Issuer_ID_Icon());
			
	
			// Dynamic User Variables
			if (user != null) {
				UserAttribute userAttribute = user.getUserAttribute();
				UserHealthService userHealthService = user.getUserHealthService();
				// FirstName
				// Last Name
				 doc.getElementById("ZoneXX_First_Middle_Name").setTextContent((userAttribute.getFirstName()));

				doc.getElementById("ZoneXX_Last_Name")
						.setTextContent(((userAttribute.getLastName() == null) ? "" : userAttribute.getLastName()));
				// DOB
				 doc.getElementById("ZoneXXV_DOB").setTextContent((userAttribute.getDateOfBirth()
				== null)?"":Util.formatDate(userAttribute.getDateOfBirth(),
				 Util.MMDDYYYY_format));

				
				
				log.debug("parseHSIDFront: Ended Dynamic text Variable");

				if (hs != null) {
					doc.getElementById("ZoneXXV_IssuedDate")
							.setTextContent(Util.formatDate(hs.getDateOfIssuance(), Util.MMDDYYYY_format));
					doc.getElementById("ZoneXXV_ExpirationDate")
							.setTextContent(Util.formatDate(hs.getDateOfExpiry(), Util.MMDDYYYY_format));
					doc.getElementById("ZoneXXV_IDN").setTextContent("" + hs.getIdNumber());
					doc.getElementById("ZoneXXV_PRMRY_SUB").setTextContent(hs.getPrimarySubscriber());
					doc.getElementById("ZoneXXV_SUB_ID").setTextContent("" + hs.getPrimarySubscriberID());

					doc.getElementById("ZoneXXV_PRMRY_DR").setTextContent(hs.getPrimaryDoctor());
					doc.getElementById("ZoneXXV_PRMRY_DR_PHONE").setTextContent("" + hs.getPrimaryDoctorPhone());
					
				} else if (userHealthService != null && userHealthService.getIdNumber()!= 0) {
					doc.getElementById("ZoneXXV_IssuedDate").setTextContent(
							Util.formatDate(userHealthService.getDateOfIssuance(), Util.MMDDYYYY_format));
					doc.getElementById("ZoneXXV_ExpirationDate").setTextContent(
							Util.formatDate(userHealthService.getDateOfExpiry(), Util.MMDDYYYY_format));
					doc.getElementById("ZoneXXV_IDN").setTextContent("" + userHealthService.getIdNumber());
					doc.getElementById("ZoneXXV_PRMRY_SUB").setTextContent(userHealthService.getPrimarySubscriber());
					doc.getElementById("ZoneXXV_SUB_ID").setTextContent("" + userHealthService.getPrimarySubscriberID());

					doc.getElementById("ZoneXXV_PRMRY_DR").setTextContent(userHealthService.getPrimaryDoctor());
					doc.getElementById("ZoneXXV_PRMRY_DR_PHONE").setTextContent("" + userHealthService.getPrimaryDoctorPhone());
				}
				
				
				
				log.debug("parseHSIDFront: Ended Dynamic Hard coded text Variable");

				user.getUserBiometrics().stream().forEach(userBio -> {
					// FACE
					if (userBio.getType().equalsIgnoreCase("Face")) {
						
						int width = Integer.parseInt(doc.getElementById("ZoneXX_Photograph").getAttribute("width"));
						int height = Integer.parseInt(doc.getElementById("ZoneXX_Photograph").getAttribute("height"));

						
						try {
							Image image = Util
									.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
							BufferedImage bufferedImage = Util.resizeImage(image, width, height);
							if(user.getIsTransparentPhotoRequired()) {
								bufferedImage = backgroundColorTransparency.generate(bufferedImage, user.getOrganisationName());
							}
							String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
							doc.getElementById("ZoneXX_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + base64Data);

						} catch (IOException e) {
							throw new RuntimeException(e);
						}
						
					}

					// SIGNATURE
					if (userBio.getType().equalsIgnoreCase("SIGNATURE")) {
						doc.getElementById("ZoneXX_Signature").setAttributeNS(XLINK_NS, XLINK_HREF,
								"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
					}
					// TODO Review Else block
					else {
						doc.getElementById("ZoneXX_Signature").setAttributeNS(XLINK_NS, XLINK_HREF,
								"data:;base64," + userHS.getHsIDFront().getZoneXX_Signature());
					}
				});
				
				if(doc.getElementById("ZoneXX_QR") != null) {
					
					doc.getElementById("ZoneXX_QR")
						.setAttributeNS(XLINK_NS, XLINK_HREF,
								"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
										user.getOrganisationName() + "#" + user.getId() + "#" + credentialTemplateID,
										BarcodeFormat.QR_CODE, 300, 300, status))));
				}
			}

			return doc;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @param userEntity 
	 * @param organizationName 
	 * @param hs
	 * @param b 
	 * @param vc
	 * @return
	 * @throws Exception
	 */
	public Document parseHSIDBack(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user,int status, 
			String credentialTemplateID, byte[] svgFile, byte[] jsonFile, UserHealthServiceEntity hs, String visualCredentialType, boolean b) throws Exception {
		try {
			Gson gson = new Gson();
			UserHSID userHS = gson.fromJson(new String(jsonFile), UserHSID.class);

			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			// Static Variables
			
//			doc.getElementById("ZoneXX_QR").setAttributeNS(XLINK_NS, XLINK_HREF,
//					"data:;base64," + userHS.getHsIDBack().getZoneXX_QR());
			doc.getElementById("background").setAttributeNS(XLINK_NS, XLINK_HREF,
					"data:;base64," + userHS.getHsIDBack().getBackground());
			
			UserHealthService userHealthService = user.getUserHealthService();
			
			
			log.debug("parseDLBack: Ended Static Variable");
			
			
			// Dynamic Variables

			if (hs != null) {
				 List<UserHealthServiceVaccineInfoEntity> vacchineInfos = hs.getUserHealthServiceVaccineInfos();
				 int size  = vacchineInfos.size();
				 for (int i=1; size>0 && i <= size ; i++) {
					 UserHealthServiceVaccineInfoEntity userHealthServiceVaccineInfoEntity  = vacchineInfos.get(i-1);
				 
					 doc.getElementById("ZoneXX_Vaccine"+(i)).setTextContent(userHealthServiceVaccineInfoEntity.getVaccine());
					 doc.getElementById("ZoneXX_Route"+(i)).setTextContent(userHealthServiceVaccineInfoEntity.getRoute());
					 doc.getElementById("ZoneXX_Site"+(i)).setTextContent(userHealthServiceVaccineInfoEntity.getSite());
					 doc.getElementById("ZoneXX_Date"+(i)).setTextContent(util.formatDate(userHealthServiceVaccineInfoEntity.getDate(), Util.MMDDYYYY_format));
					 doc.getElementById("ZoneXX_AdministeredBy"+(i)).setTextContent(userHealthServiceVaccineInfoEntity.getAdministeredBy());

				}				
			} else if (userHealthService != null && userHealthService.getIdNumber()!= 0) {
				List<UserHealthServiceVaccineInfo> vacchineInfos = userHealthService.getUserHealthServiceVaccineInfos();
				 int size  = vacchineInfos.size();
				 for (int i=1; size>0 && i <= size ; i++) {
					 UserHealthServiceVaccineInfo userHealthServiceVaccineInfo  = vacchineInfos.get(i-1);
				 
					 doc.getElementById("ZoneXX_Vaccine"+(i)).setTextContent(userHealthServiceVaccineInfo.getVaccine());
					 doc.getElementById("ZoneXX_Route"+(i)).setTextContent(userHealthServiceVaccineInfo.getRoute());
					 doc.getElementById("ZoneXX_Site"+(i)).setTextContent(userHealthServiceVaccineInfo.getSite());
					 doc.getElementById("ZoneXX_Date"+(i)).setTextContent(util.formatDate(userHealthServiceVaccineInfo.getDate(), Util.MMDDYYYY_format));
					 doc.getElementById("ZoneXX_AdministeredBy"+(i)).setTextContent(userHealthServiceVaccineInfo.getAdministeredBy());

				}		
			}

		
			if(doc.getElementById("ZoneXX_QR") != null) {
				doc.getElementById("ZoneXX_QR")
					.setAttributeNS(XLINK_NS, XLINK_HREF,
							"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
									user.getOrganisationName() + "#" + user.getId() + "#" + credentialTemplateID,
									BarcodeFormat.QR_CODE, 300, 300, status))));
			}
				
				
			log.debug("parseHSIDBack: Ended Dynamic text Variable");

			return doc;

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}

}
