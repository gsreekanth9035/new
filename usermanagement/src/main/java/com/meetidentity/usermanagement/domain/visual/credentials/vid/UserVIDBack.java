package com.meetidentity.usermanagement.domain.visual.credentials.vid;

import java.io.Serializable;

public class UserVIDBack implements Serializable {

	private static final long serialVersionUID = 1L;

	private String Zone0_BS_Background;

	private String Zone4_01BS_Height_Eng;

	private String Zone4_02BS_Eyes_Eng;

	private String Zone2_03BS_DocNum_Eng;

	private String Zone4_04BS_CHAddress_Eng;

	private String Zone4_01BS_Height_Alt;

	private String Zone4_02BS_Eyes_Alt;

	private String Zone2_03BS_DocNum_Alt;

	private String Zone4_04BS_CHAddress_Alt;

	private String Zone4_01BV_Height;

	private String Zone4_02BV_Eyes;

	private String Zone2_03BV_DocNum;

	private String Zone4_04BV_CHAddress_1;

	private String Zone4_04BV_CHAddress_2;

	private String Zone4_05BV_QR;

	private String Zone5_MRZ_TD1_L1;

	private String Zone5_MRZ_TD1_L2;

	private String Zone5_MRZ_TD1_L3;

	public String getZone0_BS_Background() {
		return Zone0_BS_Background;
	}

	public void setZone0_BS_Background(String zone0_BS_Background) {
		Zone0_BS_Background = zone0_BS_Background;
	}

	public String getZone4_01BS_Height_Eng() {
		return Zone4_01BS_Height_Eng;
	}

	public void setZone4_01BS_Height_Eng(String zone4_01bs_Height_Eng) {
		Zone4_01BS_Height_Eng = zone4_01bs_Height_Eng;
	}

	public String getZone4_02BS_Eyes_Eng() {
		return Zone4_02BS_Eyes_Eng;
	}

	public void setZone4_02BS_Eyes_Eng(String zone4_02bs_Eyes_Eng) {
		Zone4_02BS_Eyes_Eng = zone4_02bs_Eyes_Eng;
	}

	public String getZone2_03BS_DocNum_Eng() {
		return Zone2_03BS_DocNum_Eng;
	}

	public void setZone2_03BS_DocNum_Eng(String zone2_03bs_DocNum_Eng) {
		Zone2_03BS_DocNum_Eng = zone2_03bs_DocNum_Eng;
	}

	public String getZone2_03BS_DocNum_Alt() {
		return Zone2_03BS_DocNum_Alt;
	}

	public void setZone2_03BS_DocNum_Alt(String zone2_03bs_DocNum_Alt) {
		Zone2_03BS_DocNum_Alt = zone2_03bs_DocNum_Alt;
	}

	public String getZone4_01BS_Height_Alt() {
		return Zone4_01BS_Height_Alt;
	}

	public void setZone4_01BS_Height_Alt(String zone4_01bs_Height_Alt) {
		Zone4_01BS_Height_Alt = zone4_01bs_Height_Alt;
	}

	public String getZone4_02BS_Eyes_Alt() {
		return Zone4_02BS_Eyes_Alt;
	}

	public void setZone4_02BS_Eyes_Alt(String zone4_02bs_Eyes_Alt) {
		Zone4_02BS_Eyes_Alt = zone4_02bs_Eyes_Alt;
	}

	public String getZone4_04BS_CHAddress_Eng() {
		return Zone4_04BS_CHAddress_Eng;
	}

	public void setZone4_04BS_CHAddress_Eng(String zone4_04bs_CHAddress_Eng) {
		Zone4_04BS_CHAddress_Eng = zone4_04bs_CHAddress_Eng;
	}

	public String getZone4_04BS_CHAddress_Alt() {
		return Zone4_04BS_CHAddress_Alt;
	}

	public void setZone4_04BS_CHAddress_Alt(String zone4_04bs_CHAddress_Alt) {
		Zone4_04BS_CHAddress_Alt = zone4_04bs_CHAddress_Alt;
	}

	public String getZone4_04BV_CHAddress_1() {
		return Zone4_04BV_CHAddress_1;
	}

	public void setZone4_04BV_CHAddress_1(String zone4_04bv_CHAddress_1) {
		Zone4_04BV_CHAddress_1 = zone4_04bv_CHAddress_1;
	}

	public String getZone4_04BV_CHAddress_2() {
		return Zone4_04BV_CHAddress_2;
	}

	public void setZone4_04BV_CHAddress_2(String zone4_04bv_CHAddress_2) {
		Zone4_04BV_CHAddress_2 = zone4_04bv_CHAddress_2;
	}

	public String getZone4_01BV_Height() {
		return Zone4_01BV_Height;
	}

	public void setZone4_01BV_Height(String zone4_01bv_Height) {
		Zone4_01BV_Height = zone4_01bv_Height;
	}

	public String getZone4_02BV_Eyes() {
		return Zone4_02BV_Eyes;
	}

	public void setZone4_02BV_Eyes(String zone4_02bv_Eyes) {
		Zone4_02BV_Eyes = zone4_02bv_Eyes;
	}

	public String getZone2_03BV_DocNum() {
		return Zone2_03BV_DocNum;
	}

	public void setZone2_03BV_DocNum(String zone2_03bv_DocNum) {
		Zone2_03BV_DocNum = zone2_03bv_DocNum;
	}

	public String getZone4_05BV_QR() {
		return Zone4_05BV_QR;
	}

	public void setZone4_05BV_QR(String zone4_05bv_QR) {
		Zone4_05BV_QR = zone4_05bv_QR;
	}

	public String getZone5_MRZ_TD1_L1() {
		return Zone5_MRZ_TD1_L1;
	}

	public void setZone5_MRZ_TD1_L1(String zone5_MRZ_TD1_L1) {
		Zone5_MRZ_TD1_L1 = zone5_MRZ_TD1_L1;
	}

	public String getZone5_MRZ_TD1_L2() {
		return Zone5_MRZ_TD1_L2;
	}

	public void setZone5_MRZ_TD1_L2(String zone5_MRZ_TD1_L2) {
		Zone5_MRZ_TD1_L2 = zone5_MRZ_TD1_L2;
	}

	public String getZone5_MRZ_TD1_L3() {
		return Zone5_MRZ_TD1_L3;
	}

	public void setZone5_MRZ_TD1_L3(String zone5_MRZ_TD1_L3) {
		Zone5_MRZ_TD1_L3 = zone5_MRZ_TD1_L3;
	}

}
