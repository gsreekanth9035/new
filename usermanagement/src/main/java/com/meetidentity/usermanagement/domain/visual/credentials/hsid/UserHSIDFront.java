package com.meetidentity.usermanagement.domain.visual.credentials.hsid;

import java.io.Serializable;

public class UserHSIDFront implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String background;
	private String ZoneXX_Logo;
	private String ZoneXX_Card_Name;
	private String ZoneXX_First_Middle_Name;
	private String ZoneXX_Last_Name;
	private String ZoneXX_DOB;
	private String ZoneXX_IssuedDate;
	private String ZoneXX_ExpirationDate;
	private String ZoneXX_IDN;
	private String ZoneXX_PRMRY_SUB;
	private String ZoneXX_SUB_ID;
	private String ZoneXX_PRMRY_DR;
	private String ZoneXX_PRMRY_DR_PHONE;
	private String ZoneXX_Photograph;
	private String ZoneXX_Issuer_ID_Icon;
	private String ZoneXX_Signature;
	private String ZoneXXV_DOB;
	private String ZoneXXV_IssuedDate;
	private String ZoneXXV_ExpirationDate;
	private String ZoneXXV_IDN;
	private String ZoneXXV_PRMRY_SUB;
	private String ZoneXXV_SUB_ID;
	private String ZoneXXV_PRMRY_DR;
	private String ZoneXXV_PRMRY_DR_PHONE;
	
	
	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	public String getZoneXX_Logo() {
		return ZoneXX_Logo;
	}
	public void setZoneXX_Logo(String zoneXX_Logo) {
		ZoneXX_Logo = zoneXX_Logo;
	}
	public String getZoneXX_Card_Name() {
		return ZoneXX_Card_Name;
	}
	public void setZoneXX_Card_Name(String zoneXX_Card_Name) {
		ZoneXX_Card_Name = zoneXX_Card_Name;
	}
	public String getZoneXX_First_Middle_Name() {
		return ZoneXX_First_Middle_Name;
	}
	public void setZoneXX_First_Middle_Name(String zoneXX_First_Middle_Name) {
		ZoneXX_First_Middle_Name = zoneXX_First_Middle_Name;
	}
	public String getZoneXX_Last_Name() {
		return ZoneXX_Last_Name;
	}
	public void setZoneXX_Last_Name(String zoneXX_Last_Name) {
		ZoneXX_Last_Name = zoneXX_Last_Name;
	}
	public String getZoneXX_DOB() {
		return ZoneXX_DOB;
	}
	public void setZoneXX_DOB(String zoneXX_DOB) {
		ZoneXX_DOB = zoneXX_DOB;
	}
	public String getZoneXX_IssuedDate() {
		return ZoneXX_IssuedDate;
	}
	public void setZoneXX_IssuedDate(String zoneXX_IssuedDate) {
		ZoneXX_IssuedDate = zoneXX_IssuedDate;
	}
	public String getZoneXX_ExpirationDate() {
		return ZoneXX_ExpirationDate;
	}
	public void setZoneXX_ExpirationDate(String zoneXX_ExpirationDate) {
		ZoneXX_ExpirationDate = zoneXX_ExpirationDate;
	}
	public String getZoneXX_IDN() {
		return ZoneXX_IDN;
	}
	public void setZoneXX_IDN(String zoneXX_IDN) {
		ZoneXX_IDN = zoneXX_IDN;
	}
	public String getZoneXX_PRMRY_SUB() {
		return ZoneXX_PRMRY_SUB;
	}
	public void setZoneXX_PRMRY_SUB(String zoneXX_PRMRY_SUB) {
		ZoneXX_PRMRY_SUB = zoneXX_PRMRY_SUB;
	}
	public String getZoneXX_SUB_ID() {
		return ZoneXX_SUB_ID;
	}
	public void setZoneXX_SUB_ID(String zoneXX_SUB_ID) {
		ZoneXX_SUB_ID = zoneXX_SUB_ID;
	}
	public String getZoneXX_PRMRY_DR() {
		return ZoneXX_PRMRY_DR;
	}
	public void setZoneXX_PRMRY_DR(String zoneXX_PRMRY_DR) {
		ZoneXX_PRMRY_DR = zoneXX_PRMRY_DR;
	}
	public String getZoneXX_PRMRY_DR_PHONE() {
		return ZoneXX_PRMRY_DR_PHONE;
	}
	public void setZoneXX_PRMRY_DR_PHONE(String zoneXX_PRMRY_DR_PHONE) {
		ZoneXX_PRMRY_DR_PHONE = zoneXX_PRMRY_DR_PHONE;
	}
	public String getZoneXX_Photograph() {
		return ZoneXX_Photograph;
	}
	public void setZoneXX_Photograph(String zoneXX_Photograph) {
		ZoneXX_Photograph = zoneXX_Photograph;
	}
	public String getZoneXX_Issuer_ID_Icon() {
		return ZoneXX_Issuer_ID_Icon;
	}
	public void setZoneXX_Issuer_ID_Icon(String zoneXX_Issuer_ID_Icon) {
		ZoneXX_Issuer_ID_Icon = zoneXX_Issuer_ID_Icon;
	}
	public String getZoneXX_Signature() {
		return ZoneXX_Signature;
	}
	public void setZoneXX_Signature(String zoneXX_Signature) {
		ZoneXX_Signature = zoneXX_Signature;
	}
	public String getZoneXXV_DOB() {
		return ZoneXXV_DOB;
	}
	public void setZoneXXV_DOB(String zoneXXV_DOB) {
		ZoneXXV_DOB = zoneXXV_DOB;
	}
	public String getZoneXXV_IssuedDate() {
		return ZoneXXV_IssuedDate;
	}
	public void setZoneXXV_IssuedDate(String zoneXXV_IssuedDate) {
		ZoneXXV_IssuedDate = zoneXXV_IssuedDate;
	}
	public String getZoneXXV_ExpirationDate() {
		return ZoneXXV_ExpirationDate;
	}
	public void setZoneXXV_ExpirationDate(String zoneXXV_ExpirationDate) {
		ZoneXXV_ExpirationDate = zoneXXV_ExpirationDate;
	}
	public String getZoneXXV_IDN() {
		return ZoneXXV_IDN;
	}
	public void setZoneXXV_IDN(String zoneXXV_IDN) {
		ZoneXXV_IDN = zoneXXV_IDN;
	}
	public String getZoneXXV_PRMRY_SUB() {
		return ZoneXXV_PRMRY_SUB;
	}
	public void setZoneXXV_PRMRY_SUB(String zoneXXV_PRMRY_SUB) {
		ZoneXXV_PRMRY_SUB = zoneXXV_PRMRY_SUB;
	}
	public String getZoneXXV_SUB_ID() {
		return ZoneXXV_SUB_ID;
	}
	public void setZoneXXV_SUB_ID(String zoneXXV_SUB_ID) {
		ZoneXXV_SUB_ID = zoneXXV_SUB_ID;
	}
	public String getZoneXXV_PRMRY_DR() {
		return ZoneXXV_PRMRY_DR;
	}
	public void setZoneXXV_PRMRY_DR(String zoneXXV_PRMRY_DR) {
		ZoneXXV_PRMRY_DR = zoneXXV_PRMRY_DR;
	}
	public String getZoneXXV_PRMRY_DR_PHONE() {
		return ZoneXXV_PRMRY_DR_PHONE;
	}
	public void setZoneXXV_PRMRY_DR_PHONE(String zoneXXV_PRMRY_DR_PHONE) {
		ZoneXXV_PRMRY_DR_PHONE = zoneXXV_PRMRY_DR_PHONE;
	}
	
	

	
}
