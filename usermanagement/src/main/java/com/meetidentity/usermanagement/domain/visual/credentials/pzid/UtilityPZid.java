package com.meetidentity.usermanagement.domain.visual.credentials.pzid;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Iterator;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.constants.XMLConstants;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserAttributeEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserPersonalizedIDEntity;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;

@Component
public class UtilityPZid {

	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;

	private final Logger log = LoggerFactory.getLogger(UtilityPZid.class);

	@Autowired
	private Util util;
	
	
	private final Logger logger = LoggerFactory.getLogger(UtilityPZid.class);
	private static String XLINK_NS = XMLConstants.XLINK_NAMESPACE_URI;
	private static String XLINK_HREF = "xlink:href";

	/**
	 * 
	 * @param requestFrom 
	 * @param sAXSVGDocumentFactory
	 * @param b 
	 * @param userEntity 
	 * @param organizationName 
	 * @return
	 * @throws Exception
	 */
	public Document parsePZidFront(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user, byte[] svgFile,
			byte[] jsonFile, String credentialTemplateID, UserPersonalizedIDEntity userPersonalizedIDEntity, String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {

				JSONObject pzidJsonObject = new JSONObject(new String(jsonFile));
				JSONObject pzidFrontJsonObject = pzidJsonObject.getJSONObject("userPZIDfront");
				Iterator<String> keys = pzidFrontJsonObject.keys();
				log.debug("parsePZIDFront: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				// userPZID userpzid = gson.fromJson(new String(jsonFile), userPZID.class);
				// Dynamic User Variables
				switch (requestFrom) {

					case "web_portal_enroll_summary":		
						UserAttribute userAttribute = user.getUserAttribute();
						if(doc.getElementById("ZoneXFV_Given_Last_Name") != null) {
							if((userAttribute.getFirstName()+" "+userAttribute.getLastName()).length()<=20) {
								doc.getElementById("ZoneXFV_Given_Last_Name").setTextContent(userAttribute.getFirstName()+" "+userAttribute.getLastName());
							}else {
								doc.getElementById("ZoneXFV_Given_Last_Name").setTextContent(userAttribute.getFirstName()+" "+userAttribute.getLastName().charAt(0));
							}
						}
						if (doc.getElementById("ZoneXFV_FirstName") != null) {
							doc.getElementById("ZoneXFV_FirstName").setTextContent(userAttribute.getFirstName());
						}
						if (doc.getElementById("ZoneXFV_LastName") != null) {
							doc.getElementById("ZoneXFV_LastName").setTextContent(userAttribute.getLastName());
						}
						if(doc.getElementById("ZoneXFV_Id_Number") != null) {
							doc.getElementById("ZoneXFV_Id_Number").setTextContent("XXXXXXXXXX");
							
						}
						if(doc.getElementById("ZoneXFV_YY") != null) {
							doc.getElementById("ZoneXFV_YY").setTextContent("XX");							
						}
						if(doc.getElementById("ZoneXFV_Time") != null) {
							doc.getElementById("ZoneXFV_Time").setTextContent("HH:MM, EEE, MMM dd YYYY");							
						}
						 doc.getElementById("ZoneXFV_QRCode")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + user.getId() + "#" + credentialTemplateID + "#" + "XXXXXXXXXX",
											BarcodeFormat.QR_CODE, 300, 300))));
						 if(doc.getElementById("ZoneXFV_Photograph") != null) {
							 user.getUserBiometrics().stream().forEach(userBio -> {
									if (userBio.getType().equalsIgnoreCase("FACE")) {
										
										int width = Integer.parseInt(doc.getElementById("ZoneXFV_Photograph").getAttribute("width"));
										int height = Integer.parseInt(doc.getElementById("ZoneXFV_Photograph").getAttribute("height"));
										try {
											Image image = Util
													.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
											BufferedImage bufferedImage = Util.resizeImage(image, width, height);
											if(user.getIsTransparentPhotoRequired()) {
												bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
											}
											String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
											doc.getElementById("ZoneXFV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
													"data:;base64," + base64Data);
	
										} catch (IOException e) {
											doc.getElementById("ZoneXFV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
										}
									}
								});
						 }
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":						
						UserAttributeEntity userAttributee = userEntity.getUserAttribute();		
						
						if(doc.getElementById("ZoneXFV_Given_Last_Name") != null) {
							if((userAttributee.getFirstName()+" "+userAttributee.getLastName()).length()<=20) {
								doc.getElementById("ZoneXFV_Given_Last_Name").setTextContent(userAttributee.getFirstName()+" "+userAttributee.getLastName());
							}else {
								doc.getElementById("ZoneXFV_Given_Last_Name").setTextContent(userAttributee.getFirstName()+" "+userAttributee.getLastName().charAt(0));
							}
						}
						
						if(doc.getElementById("ZoneXFV_Given_Last_Name") != null) {
							if((userAttributee.getFirstName()+" "+userAttributee.getLastName()).length()<=20) {
								doc.getElementById("ZoneXFV_Given_Last_Name").setTextContent(userAttributee.getFirstName()+" "+userAttributee.getLastName());
							}else {
								doc.getElementById("ZoneXFV_Given_Last_Name").setTextContent(userAttributee.getFirstName()+" "+userAttributee.getLastName().charAt(0));
							}
						}
						if (doc.getElementById("ZoneXFV_FirstName") != null) {
							doc.getElementById("ZoneXFV_FirstName").setTextContent(userAttributee.getFirstName());
						}
						if (doc.getElementById("ZoneXFV_LastName") != null) {
							doc.getElementById("ZoneXFV_LastName").setTextContent(userAttributee.getLastName());
						}
						
						if(doc.getElementById("ZoneXFV_YY") != null) {
							doc.getElementById("ZoneXFV_YY").setTextContent(userPersonalizedIDEntity.getDateOfIssuance() == null? "XX" : Util.formatDate(userPersonalizedIDEntity.getDateOfIssuance(), Util.YY_format));							
						}
						if(doc.getElementById("ZoneXFV_Time") != null) {
							doc.getElementById("ZoneXFV_Time").setTextContent(userPersonalizedIDEntity.getDateOfExpiry() == null? "XX" : Util.formatDate(userPersonalizedIDEntity.getDateOfExpiry(), Util.HH_MM_EEE_MMM_DD_YYYY_format));							
						}
						
						String idNumber = "XXXXXXXXXXXX";
						if(userPersonalizedIDEntity.getIdNumber()!=null) {
							idNumber = userPersonalizedIDEntity.getIdNumber();
						}
						if(doc.getElementById("ZoneXFV_Id_Number") != null) {
							doc.getElementById("ZoneXFV_Id_Number").setTextContent(idNumber);
						}
						
						doc.getElementById("ZoneXFV_QRCode")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + userEntity.getId() + "#" + credentialTemplateID +"#"+ idNumber,
											BarcodeFormat.QR_CODE, 300, 300))));
						 if(doc.getElementById("ZoneXFV_Photograph") != null) {
							 userEntity.getUserBiometrics().stream().forEach(userBio -> {
									if (userBio.getType().equalsIgnoreCase("FACE")) {
										
										int width = Integer.parseInt(doc.getElementById("ZoneXFV_Photograph").getAttribute("width"));
										int height = Integer.parseInt(doc.getElementById("ZoneXFV_Photograph").getAttribute("height"));
										try {
											Image image = Util
													.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
											BufferedImage bufferedImage = Util.resizeImage(image, width, height);
											if(user.getIsTransparentPhotoRequired()) {
												bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
											}
											String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
											doc.getElementById("ZoneXFV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
													"data:;base64," + base64Data);
	
										} catch (IOException e) {
											doc.getElementById("ZoneXFV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
										}
									}
								});
						 }
						break;
					default :
				
				}
				
//				doc.getElementById("Zone0FS_Background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + usereid.getUserEIDfront().getZone0FS_Background());
//				doc.getElementById("Zone11FS_OrgSeal").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + usereid.getUserEIDfront().getZone11FS_OrgSeal());
		
	
				logger.debug("parsePZidFront: Ended Dynamic text Variable");
	
				return doc;
			}
		
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType);
		}
	}

	/**
	 * 
	 * @param requestFrom 
	 * @param sAXSVGDocumentFactory
	 * @param b 
	 * @param userEntity 
	 * @param organizationName 
	 * @return
	 * @throws Exception
	 */
	public Document parsePZidBack(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user, String credentialTemplateID,
			byte[] svgFile, byte[] jsonFile, UserPersonalizedIDEntity userPersonalizedIDEntity, String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {

				JSONObject pzidJsonObject = new JSONObject(new String(jsonFile));
				JSONObject pzidBacktJsonObject = pzidJsonObject.getJSONObject("userPZIDback");
				Iterator<String> keys = pzidBacktJsonObject.keys();
				log.debug("parsePZIDBack: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				// UserEid usereid = gson.fromJson(new String(jsonFile), UserEid.class);
				// Dynamic User Variables
				switch (requestFrom) {

					case "web_portal_enroll_summary":
						if(doc.getElementById("ZoneXFV_DOE") != null) {
							doc.getElementById("ZoneXFV_DOE").setTextContent("XXX. XX, XXXX");	
						}						
						if(doc.getElementById("ZoneXFV_DOI") != null) {
							doc.getElementById("ZoneXFV_DOI").setTextContent("XXX. XX, XXXX");	
						}				
//						user.getUserBiometrics().stream().forEach(userBio -> {
//							if (userBio.getType().equalsIgnoreCase("FACE")) {
//								
//								int width = Integer.parseInt(doc.getElementById("Zone4_BV_Photoghost").getAttribute("width"));
//								int height = Integer.parseInt(doc.getElementById("Zone4_BV_Photoghost").getAttribute("height"));
//								try {
//									Image image = Util
//											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
//									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
//									if(user.getIsTransparentPhotoRequired()) {
//										bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
//									}
//									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
//									doc.getElementById("Zone4_BV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
//											"data:;base64," + base64Data);
//
//								} catch (IOException e) {
//									doc.getElementById("Zone4_BV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
//								}
//							}
//						});
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":

						if(doc.getElementById("ZoneXFV_DOE") != null) {
							doc.getElementById("ZoneXFV_DOE").setTextContent(userPersonalizedIDEntity.getDateOfExpiry() == null? "XXX. XX, XXXX" : Util.formatDate(userPersonalizedIDEntity.getDateOfExpiry(), Util.MMM_DD_YYYY_format));
						}	
						if(doc.getElementById("ZoneXFV_DOI") != null) {
							doc.getElementById("ZoneXFV_DOI").setTextContent(userPersonalizedIDEntity.getDateOfIssuance() == null? "XXX. XX, XXXX" : Util.formatDate(userPersonalizedIDEntity.getDateOfIssuance(), Util.MMM_DD_YYYY_format));
						}	
						//						userEntity.getUserBiometrics().stream().forEach(userBio -> {
//							if (userBio.getType().equalsIgnoreCase("FACE")) {
//								
//								int width = Integer.parseInt(doc.getElementById("Zone4_BV_Photoghost").getAttribute("width"));
//								int height = Integer.parseInt(doc.getElementById("Zone4_BV_Photoghost").getAttribute("height"));
//								try {
//									Image image = Util
//											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
//									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
//									if(user.getIsTransparentPhotoRequired()) {
//										bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
//									}
//									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
//									doc.getElementById("Zone4_BV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
//											"data:;base64," + base64Data);
//
//								} catch (IOException e) {
//									doc.getElementById("Zone4_BV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
//								}
//							}
//						});
						break;
					default :
				
				}
				
//				doc.getElementById("Zone0BS_Background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + usereid.getUserEIDback().getZone0BS_Background());

		
	
				logger.debug("parseEidBack: Ended Dynamic text Variable");
	
				return doc;
			}
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType);
		}
	}
}
