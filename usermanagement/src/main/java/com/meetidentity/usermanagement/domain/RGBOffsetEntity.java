package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "rgb_offset")
public class RGBOffsetEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "red_onset")
	private int redOnset;

	@Column(name = "green_onset")
	private int greenOnset;

	@Column(name = "blue_onset")
	private int blueOnset;

	@Column(name = "red_end")
	private int redEnd;

	@Column(name = "green_end")
	private int greenEnd;

	@Column(name = "blue_end")
	private int blueEnd;

	@Column(name = "status")
	private boolean status;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
	private OrganizationEntity organization;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getRedOnset() {
		return redOnset;
	}

	public void setRedOnset(int redOnset) {
		this.redOnset = redOnset;
	}

	public int getGreenOnset() {
		return greenOnset;
	}

	public void setGreenOnset(int greenOnset) {
		this.greenOnset = greenOnset;
	}

	public int getBlueOnset() {
		return blueOnset;
	}

	public void setBlueOnset(int blueOnset) {
		this.blueOnset = blueOnset;
	}

	public int getRedEnd() {
		return redEnd;
	}

	public void setRedEnd(int redEnd) {
		this.redEnd = redEnd;
	}

	public int getGreenEnd() {
		return greenEnd;
	}

	public void setGreenEnd(int greenEnd) {
		this.greenEnd = greenEnd;
	}

	public int getBlueEnd() {
		return blueEnd;
	}

	public void setBlueEnd(int blueEnd) {
		this.blueEnd = blueEnd;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

}
