package com.meetidentity.usermanagement.domain;

import java.time.Instant;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user_healthservice")
public class UserHealthServiceEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;

	@Column(name = "uims_id")
	private String uimsId;

	@Column(name = "id_number")
	private long idNumber;

	@Column(name = "issuing_authority")
	private String issuingAuthority;

	@Column(name = "primary_subscriber")
	private String primarySubscriber;
	
	@Column(name = "primary_subscriber_id")
	private long primarySubscriberID;
	
	@Column(name = "primary_doctor")
	private String primaryDoctor;
	
	@Column(name = "primary_doctor_phone")
	private long primaryDoctorPhone;
	
	@Column(name = "date_of_issuance")
	private Instant dateOfIssuance;
	
	@Column(name = "date_of_expiry")
	private Instant dateOfExpiry;
	
	 @OneToMany(mappedBy = "userhs", cascade=CascadeType.PERSIST, orphanRemoval=true, fetch=FetchType.LAZY)
	 private List<UserHealthServiceVaccineInfoEntity> userHealthServiceVaccineInfos;
	   
	public UserHealthServiceEntity() {
		// TODO Auto-generated constructor stub
	}

	
	
	public UserHealthServiceEntity(Long id, UserEntity user, String uimsId, long idNumber, String issuingAuthority,
			String primarySubscriber, long primarySubscriberID, String primaryDoctor, long primaryDoctorPhone,
			Instant dateOfIssuance, Instant dateOfExpiry,
			List<UserHealthServiceVaccineInfoEntity> userHealthServiceVaccineInfos) {
		super();
		this.id = id;
		this.user = user;
		this.uimsId = uimsId;
		this.idNumber = idNumber;
		this.issuingAuthority = issuingAuthority;
		this.primarySubscriber = primarySubscriber;
		this.primarySubscriberID = primarySubscriberID;
		this.primaryDoctor = primaryDoctor;
		this.primaryDoctorPhone = primaryDoctorPhone;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
		this.userHealthServiceVaccineInfos = userHealthServiceVaccineInfos;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public long getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(long idNumber) {
		this.idNumber = idNumber;
	}

	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public String getPrimarySubscriber() {
		return primarySubscriber;
	}

	public void setPrimarySubscriber(String primarySubscriber) {
		this.primarySubscriber = primarySubscriber;
	}

	public long getPrimarySubscriberID() {
		return primarySubscriberID;
	}

	public void setPrimarySubscriberID(long primarySubscriberID) {
		this.primarySubscriberID = primarySubscriberID;
	}

	public String getPrimaryDoctor() {
		return primaryDoctor;
	}

	public void setPrimaryDoctor(String primaryDoctor) {
		this.primaryDoctor = primaryDoctor;
	}

	public long getPrimaryDoctorPhone() {
		return primaryDoctorPhone;
	}

	public void setPrimaryDoctorPhone(long primaryDoctorPhone) {
		this.primaryDoctorPhone = primaryDoctorPhone;
	}

	public Instant getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Instant  dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public Instant getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Instant  dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public List<UserHealthServiceVaccineInfoEntity> getUserHealthServiceVaccineInfos() {
		return userHealthServiceVaccineInfos;
	}

	public void setUserHealthServiceVaccineInfos(List<UserHealthServiceVaccineInfoEntity> userHealthServiceVaccineInfos) {
		this.userHealthServiceVaccineInfos = userHealthServiceVaccineInfos;
	}

	

}
