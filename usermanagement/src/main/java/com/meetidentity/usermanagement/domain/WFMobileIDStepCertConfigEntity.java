package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "wf_mobile_id_step_cert_config")
public class WFMobileIDStepCertConfigEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "no_of_days_before_cert_expiry")
	private ConfigValueEntity noOfDaysBeforeCertExpiry;

	@ManyToOne
	@JoinColumn(name = "email_notification_freq")
	private ConfigValueEntity emailNotificationFreq;

	@OneToOne
	@JoinColumn(name = "wf_mobile_identity_issuance_id")
	private WFMobileIDIdentityIssuanceEntity wfMobileIDIdentityIssuance;

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ConfigValueEntity getNoOfDaysBeforeCertExpiry() {
		return noOfDaysBeforeCertExpiry;
	}

	public void setNoOfDaysBeforeCertExpiry(ConfigValueEntity noOfDaysBeforeCertExpiry) {
		this.noOfDaysBeforeCertExpiry = noOfDaysBeforeCertExpiry;
	}

	public ConfigValueEntity getEmailNotificationFreq() {
		return emailNotificationFreq;
	}

	public void setEmailNotificationFreq(ConfigValueEntity emailNotificationFreq) {
		this.emailNotificationFreq = emailNotificationFreq;
	}

	public int getVersion() {
		return version;
	}

	public WFMobileIDIdentityIssuanceEntity getWfMobileIDIdentityIssuance() {
		return wfMobileIDIdentityIssuance;
	}

	public void setWfMobileIDIdentityIssuance(WFMobileIDIdentityIssuanceEntity wfMobileIDIdentityIssuance) {
		this.wfMobileIDIdentityIssuance = wfMobileIDIdentityIssuance;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
