package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="config")
public class ConfigEntity  extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
    @Column(name="name")
	private String name;
    
    @Column(name="description")
	private String description;
    
    @Column(name="display_type")
	private String displayType;
    
    @Column(name="active")
    private Boolean active;

    @Version
    @Column(name="version")
    private int version;
    
    @ManyToOne
    @JoinColumn(name="organization_id")
    private OrganizationEntity organization;
    
    @ManyToOne
    @JoinColumn(name="config_type_id")
    private ConfigTypeEntity configType;
    
    @OneToMany(mappedBy="config")
    private List<ConfigValueEntity> configValues = new ArrayList<>();    
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDisplayType() {
		return displayType;
	}

	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

	public ConfigTypeEntity getConfigType() {
		return configType;
	}

	public void setConfigType(ConfigTypeEntity configType) {
		this.configType = configType;
	}

	public List<ConfigValueEntity> getConfigValues() {
		return configValues;
	}

	public void setConfigValues(List<ConfigValueEntity> configValues) {
		this.configValues = configValues;
	}
	
}
