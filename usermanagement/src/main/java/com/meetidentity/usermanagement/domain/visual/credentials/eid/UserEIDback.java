package com.meetidentity.usermanagement.domain.visual.credentials.eid;

import java.io.Serializable;

public class UserEIDback implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 private String Zone0BS_Background;
	 private String Zone1BS_Agency_CardSN;
	 private String Zone2BS_IssuerIN;
	 private String Zone4BS_ReturnAddress;
	 private String Zone6BS_Ainfo;
	 private String Zone1BV_Agency_CardSN;
	public String getZone0BS_Background() {
		return Zone0BS_Background;
	}
	public void setZone0BS_Background(String zone0bs_Background) {
		Zone0BS_Background = zone0bs_Background;
	}
	public String getZone1BS_Agency_CardSN() {
		return Zone1BS_Agency_CardSN;
	}
	public void setZone1BS_Agency_CardSN(String zone1bs_Agency_CardSN) {
		Zone1BS_Agency_CardSN = zone1bs_Agency_CardSN;
	}
	public String getZone2BS_IssuerIN() {
		return Zone2BS_IssuerIN;
	}
	public void setZone2BS_IssuerIN(String zone2bs_IssuerIN) {
		Zone2BS_IssuerIN = zone2bs_IssuerIN;
	}
	public String getZone4BS_ReturnAddress() {
		return Zone4BS_ReturnAddress;
	}
	public void setZone4BS_ReturnAddress(String zone4bs_ReturnAddress) {
		Zone4BS_ReturnAddress = zone4bs_ReturnAddress;
	}
	public String getZone6BS_Ainfo() {
		return Zone6BS_Ainfo;
	}
	public void setZone6BS_Ainfo(String zone6bs_Ainfo) {
		Zone6BS_Ainfo = zone6bs_Ainfo;
	}
	public String getZone1BV_Agency_CardSN() {
		return Zone1BV_Agency_CardSN;
	}
	public void setZone1BV_Agency_CardSN(String zone1bv_Agency_CardSN) {
		Zone1BV_Agency_CardSN = zone1bv_Agency_CardSN;
	}
	 
	 
}
