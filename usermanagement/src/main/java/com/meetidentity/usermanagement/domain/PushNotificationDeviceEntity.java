package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.meetidentity.usermanagement.annotations.SequenceValue;

@Entity
@Table(name="push_notification_device")
public class PushNotificationDeviceEntity extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
	
	@SequenceValue
	@Column(name = "uims_id", nullable = false)
	private String uimsId;
    
    @Column(name="user_name")
	private String userName;
    
    @Column(name="custom_data")
	private String customData;
    
    @Column(name="type")
	private String type;
    
    @Column(name="platform_arn")
	private String platformArn;
    
    @Column(name="principle")
	private String principle;
    
    @Column(name="gcm_registration_id")
	private String gcmRegistrationId;
    
    
    @Column(name="platform_app_arn")
   	private String platformAppArn;
       
       @Column(name="app_end_point_arn")
   	private String appEndPointArn;
    
    @Version
    @Column(name="version")
    private int version;
    
    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCustomData() {
		return customData;
	}

	public void setCustomData(String customData) {
		this.customData = customData;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPlatformArn() {
		return platformArn;
	}

	public void setPlatformArn(String platformArn) {
		this.platformArn = platformArn;
	}

	public String getPrinciple() {
		return principle;
	}

	public void setPrinciple(String principle) {
		this.principle = principle;
	}

	public String getGcmRegistrationId() {
		return gcmRegistrationId;
	}

	public void setGcmRegistrationId(String gcmRegistrationId) {
		this.gcmRegistrationId = gcmRegistrationId;
	}

	public String getPlatformAppArn() {
		return platformAppArn;
	}

	public void setPlatformAppArn(String platformAppArn) {
		this.platformAppArn = platformAppArn;
	}

	public String getAppEndPointArn() {
		return appEndPointArn;
	}

	public void setAppEndPointArn(String appEndPointArn) {
		this.appEndPointArn = appEndPointArn;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}
	
    
    

}
