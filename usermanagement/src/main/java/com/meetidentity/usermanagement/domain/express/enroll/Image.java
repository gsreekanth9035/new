
package com.meetidentity.usermanagement.domain.express.enroll;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "$type",
    "Name",
    "Side",
    "Lighting",
    "ImageDataEncoding",
    "ImageData"
})
public class Image {

    @JsonProperty("$type")
    private String $type;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Side")
    private String side;
    @JsonProperty("Lighting")
    private String lighting;
    @JsonProperty("ImageDataEncoding")
    private String imageDataEncoding;
    @JsonProperty("ImageData")
    private String imageData;
    @JsonIgnore
    private Map<String, String> additionalProperties = new HashMap<String, String>();

    @JsonProperty("$type")
    public String get$type() {
        return $type;
    }

    @JsonProperty("$type")
    public void set$type(String $type) {
        this.$type = $type;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("Side")
    public String getSide() {
        return side;
    }

    @JsonProperty("Side")
    public void setSide(String side) {
        this.side = side;
    }

    @JsonProperty("Lighting")
    public String getLighting() {
        return lighting;
    }

    @JsonProperty("Lighting")
    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    @JsonProperty("ImageDataEncoding")
    public String getImageDataEncoding() {
        return imageDataEncoding;
    }

    @JsonProperty("ImageDataEncoding")
    public void setImageDataEncoding(String imageDataEncoding) {
        this.imageDataEncoding = imageDataEncoding;
    }

    @JsonProperty("ImageData")
    public String getImageData() {
        return imageData;
    }

    @JsonProperty("ImageData")
    public void setImageData(String imageData) {
        this.imageData = imageData;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, String> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, String value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append($type).append(name).append(side).append(lighting).append(imageDataEncoding).append(imageData).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Image) == false) {
            return false;
        }
        Image rhs = ((Image) other);
        return new EqualsBuilder().append($type, rhs.$type).append(name, rhs.name).append(side, rhs.side).append(lighting, rhs.lighting).append(imageDataEncoding, rhs.imageDataEncoding).append(imageData, rhs.imageData).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
