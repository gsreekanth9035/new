package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "enrollment_steps_status")
public class EnrollmentStepsStatusEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;

	@Column(name = "idproof_status")
	private int idproofStatus;

	@Column(name = "registration_form_status")
	private int registrationFormStatus;

	@Column(name = "face_status")
	private int faceStatus;

	@Column(name = "iris_status")
	private int irisStatus;

	@Column(name = "roll_fingerprint_status")
	private int rollFingerprintStatus;
	
	@Column(name = "fingerprint_status")
	private int fingerprintStatus;

	@Column(name = "signature_status")
	private int signatureStatus;

	@Column(name = "summary_status")
	private int summaryStatus;
	
	@Column(name = "comments")
	private String comments;

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public int getIdproofStatus() {
		return idproofStatus;
	}

	public void setIdproofStatus(int idproofStatus) {
		this.idproofStatus = idproofStatus;
	}

	public int getRegistrationFormStatus() {
		return registrationFormStatus;
	}

	public void setRegistrationFormStatus(int registrationFormStatus) {
		this.registrationFormStatus = registrationFormStatus;
	}

	public int getFaceStatus() {
		return faceStatus;
	}

	public void setFaceStatus(int faceStatus) {
		this.faceStatus = faceStatus;
	}

	public int getIrisStatus() {
		return irisStatus;
	}

	public void setIrisStatus(int irisStatus) {
		this.irisStatus = irisStatus;
	}

	public int getFingerprintStatus() {
		return fingerprintStatus;
	}

	public void setFingerprintStatus(int fingerprintStatus) {
		this.fingerprintStatus = fingerprintStatus;
	}

	public int getSignatureStatus() {
		return signatureStatus;
	}

	public void setSignatureStatus(int signatureStatus) {
		this.signatureStatus = signatureStatus;
	}

	public int getSummaryStatus() {
		return summaryStatus;
	}

	public void setSummaryStatus(int summaryStatus) {
		this.summaryStatus = summaryStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getRollFingerprintStatus() {
		return rollFingerprintStatus;
	}

	public void setRollFingerprintStatus(int rollFingerprintStatus) {
		this.rollFingerprintStatus = rollFingerprintStatus;
	}

}
