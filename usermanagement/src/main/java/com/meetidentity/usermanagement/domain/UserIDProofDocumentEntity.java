package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "user_id_proof_document")
public class UserIDProofDocumentEntity extends AbstractAuditingEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "uims_id", nullable = false)
	private String uimsId;

	@Column(name = "name", nullable = false)
	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "org_identities_id")
	private OrganizationIdentitiesEntity organizationIdentity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private UserEntity user;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "issuing_authority_id")
	private IDProofIssuingAuthorityEntity idProofIssuingAuthority;

	@Lob
	@Column(name = "front_img")
	private byte[] frontImg;

	@Lob
	@Column(name = "back_img")
	private byte[] backImg;

	@Lob
	@Column(name = "file_data")
	private byte[] fileData;

	@Version
	@Column(name = "version")
	private int version;

	@Column(name = "front_file_type")
	private String frontFileType;

	@Column(name = "back_file_type")
	private String backFileType;

	@Column(name = "front_file_name")
	private String frontFileName;

	@Column(name = "back_file_name")
	private String backFileName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserEntity getUser() {
		return user;
	}

	public OrganizationIdentitiesEntity getOrganizationIdentity() {
		return organizationIdentity;
	}

	public void setOrganizationIdentity(OrganizationIdentitiesEntity organizationIdentity) {
		this.organizationIdentity = organizationIdentity;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public IDProofIssuingAuthorityEntity getIdProofIssuingAuthority() {
		return idProofIssuingAuthority;
	}

	public void setIdProofIssuingAuthority(IDProofIssuingAuthorityEntity idProofIssuingAuthority) {
		this.idProofIssuingAuthority = idProofIssuingAuthority;
	}

	public byte[] getFrontImg() {
		return frontImg;
	}

	public void setFrontImg(byte[] frontImg) {
		this.frontImg = frontImg;
	}

	public byte[] getBackImg() {
		return backImg;
	}

	public void setBackImg(byte[] backImg) {
		this.backImg = backImg;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public byte[] getFileData() {
		return fileData;
	}

	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}

	public String getFrontFileType() {
		return frontFileType;
	}

	public void setFrontFileType(String frontFileType) {
		this.frontFileType = frontFileType;
	}

	public String getBackFileType() {
		return backFileType;
	}

	public void setBackFileType(String backFileType) {
		this.backFileType = backFileType;
	}

	public String getFrontFileName() {
		return frontFileName;
	}

	public void setFrontFileName(String frontFileName) {
		this.frontFileName = frontFileName;
	}

	public String getBackFileName() {
		return backFileName;
	}

	public void setBackFileName(String backFileName) {
		this.backFileName = backFileName;
	}

}
