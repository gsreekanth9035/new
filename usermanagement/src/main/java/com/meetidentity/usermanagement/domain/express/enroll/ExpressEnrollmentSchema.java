
package com.meetidentity.usermanagement.domain.express.enroll;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "$type",
    "Id",
    "ExternalId",
    "CreatedOn",
    "AssemblyVersion",
    "DataEncoding",
    "LocationCollected",
    "CollectionMethod",
    "CollectingAgent",
    "Notes",
    "Items"
})
public class ExpressEnrollmentSchema {

    @JsonProperty("$type")
    private String $type;
    @JsonProperty("Id")
    private String id;
    @JsonProperty("ExternalId")
    private String externalId;
    @JsonProperty("CreatedOn")
    private String createdOn;
    @JsonProperty("AssemblyVersion")
    private String assemblyVersion;
    @JsonProperty("DataEncoding")
    private String dataEncoding;
    @JsonProperty("LocationCollected")
    private String locationCollected;
    @JsonProperty("CollectionMethod")
    private String collectionMethod;
    @JsonProperty("CollectingAgent")
    private CollectingAgent collectingAgent;
    @JsonProperty("Notes")
    private String notes;
    @JsonProperty("Items")
    private List<Item> items = new ArrayList<Item>();
    @JsonIgnore
    private Map<String, String> additionalProperties = new HashMap<String, String>();

    @JsonProperty("$type")
    public String get$type() {
        return $type;
    }

    @JsonProperty("$type")
    public void set$type(String $type) {
        this.$type = $type;
    }

    @JsonProperty("Id")
    public String getId() {
        return id;
    }

    @JsonProperty("Id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("ExternalId")
    public String getExternalId() {
        return externalId;
    }

    @JsonProperty("ExternalId")
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @JsonProperty("CreatedOn")
    public String getCreatedOn() {
        return createdOn;
    }

    @JsonProperty("CreatedOn")
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @JsonProperty("AssemblyVersion")
    public String getAssemblyVersion() {
        return assemblyVersion;
    }

    @JsonProperty("AssemblyVersion")
    public void setAssemblyVersion(String assemblyVersion) {
        this.assemblyVersion = assemblyVersion;
    }

    @JsonProperty("DataEncoding")
    public String getDataEncoding() {
        return dataEncoding;
    }

    @JsonProperty("DataEncoding")
    public void setDataEncoding(String dataEncoding) {
        this.dataEncoding = dataEncoding;
    }

    @JsonProperty("LocationCollected")
    public String getLocationCollected() {
        return locationCollected;
    }

    @JsonProperty("LocationCollected")
    public void setLocationCollected(String locationCollected) {
        this.locationCollected = locationCollected;
    }

    @JsonProperty("CollectionMethod")
    public String getCollectionMethod() {
        return collectionMethod;
    }

    @JsonProperty("CollectionMethod")
    public void setCollectionMethod(String collectionMethod) {
        this.collectionMethod = collectionMethod;
    }

    @JsonProperty("CollectingAgent")
    public CollectingAgent getCollectingAgent() {
        return collectingAgent;
    }

    @JsonProperty("CollectingAgent")
    public void setCollectingAgent(CollectingAgent collectingAgent) {
        this.collectingAgent = collectingAgent;
    }

    @JsonProperty("Notes")
    public String getNotes() {
        return notes;
    }

    @JsonProperty("Notes")
    public void setNotes(String notes) {
        this.notes = notes;
    }

    @JsonProperty("Items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("Items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, String> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, String value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append($type).append(id).append(externalId).append(createdOn).append(assemblyVersion).append(dataEncoding).append(locationCollected).append(collectionMethod).append(collectingAgent).append(notes).append(items).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ExpressEnrollmentSchema) == false) {
            return false;
        }
        ExpressEnrollmentSchema rhs = ((ExpressEnrollmentSchema) other);
        return new EqualsBuilder().append($type, rhs.$type).append(id, rhs.id).append(externalId, rhs.externalId).append(createdOn, rhs.createdOn).append(assemblyVersion, rhs.assemblyVersion).append(dataEncoding, rhs.dataEncoding).append(locationCollected, rhs.locationCollected).append(collectionMethod, rhs.collectionMethod).append(collectingAgent, rhs.collectingAgent).append(notes, rhs.notes).append(items, rhs.items).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
