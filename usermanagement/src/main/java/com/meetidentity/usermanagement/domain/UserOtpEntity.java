package com.meetidentity.usermanagement.domain;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="user_otp_code")
public class UserOtpEntity extends AbstractAuditingEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
	  
	@Column(name = "uims_id", nullable = false)
	private String uimsId;
	
	@Column(name="otp")
	private String otp;
	
	@Column(name="mobile_number")
	private String mobileNumber;
	    
	@Column(name="validity_from")
	private Instant validityFrom;
	
	@Column(name="validity_till")
	private Instant validityTill;
	
	@Column(name="status")
	private boolean status;   

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private UserEntity user;

	 @Version
	 @Column(name="version")
	 private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Instant getValidityFrom() {
		return validityFrom;
	}

	public void setValidityFrom(Instant validityFrom) {
		this.validityFrom = validityFrom;
	}

	public Instant getValidityTill() {
		return validityTill;
	}

	public void setValidityTill(Instant validityTill) {
		this.validityTill = validityTill;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}
	
	 
}
