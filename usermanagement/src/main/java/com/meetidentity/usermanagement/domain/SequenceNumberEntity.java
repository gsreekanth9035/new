package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sequence_number")
public class SequenceNumberEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
	
	@Column(name = "org_name")
	private String orgName;
    
    @Column(name = "class_name")
    private String className;

	@Column(name = "next_value")
    private long nextValue = 1;

    @Column(name = "increment_value")
    private Integer incrementValue = 1;
    
    @Column(name = "seq_prefix")
    private String seqPrefix;
    
    @Column(name = "seq_suffix")
    private String seqSuffix;
    
    @Column(name = "id_length")
    private int idLength;
    
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getNextValue() {
		return nextValue;
	}

	public void setNextValue(long nextValue) {
		this.nextValue = nextValue;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getSeqPrefix() {
		return seqPrefix;
	}

	public void setSeqPrefix(String seqPrefix) {
		this.seqPrefix = seqPrefix;
	}

	public String getSeqSuffix() {
		return seqSuffix;
	}

	public void setSeqSuffix(String seqSuffix) {
		this.seqSuffix = seqSuffix;
	}


	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Integer getIncrementValue() {
		return incrementValue;
	}

	public void setIncrementValue(Integer incrementValue) {
		this.incrementValue = incrementValue;
	}

	public int getIdLength() {
		return idLength;
	}

	public void setIdLength(int idLength) {
		this.idLength = idLength;
	}

}
