package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "device_profile_key_manager")
public class DeviceProfileKeyManagerEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "diversify")
	private String diversify;

	@Column(name = "master_key")
	private String masterKey;

	@Column(name = "is_admin_key_same_as_master_key")
	private Boolean isAdminKeySameAsMasterKey;

	@Column(name = "admin_key")
	private String adminKey;

	@Column(name = "customer_master_key")
	private String customerMasterKey;
	
	@Column(name = "customer_admin_key")
	private String customerAdminKey;

	@Column(name = "factory_management_key")
	private String factoryManagementKey;
	
	@Column(name = "customer_Management_Key")
	private String customerManagementKey;

	@OneToOne
	@JoinColumn(name = "dev_prof_id")
	private DeviceProfileEntity deviceProfile;

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDiversify() {
		return diversify;
	}

	public void setDiversify(String diversify) {
		this.diversify = diversify;
	}

	public String getMasterKey() {
		return masterKey;
	}

	public void setMasterKey(String masterKey) {
		this.masterKey = masterKey;
	}

	public Boolean getIsAdminKeySameAsMasterKey() {
		return isAdminKeySameAsMasterKey;
	}

	public void setIsAdminKeySameAsMasterKey(Boolean isAdminKeySameAsMasterKey) {
		this.isAdminKeySameAsMasterKey = isAdminKeySameAsMasterKey;
	}

	public String getAdminKey() {
		return adminKey;
	}

	public void setAdminKey(String adminKey) {
		this.adminKey = adminKey;
	}

	public String getCustomerMasterKey() {
		return customerMasterKey;
	}

	public void setCustomerMasterKey(String customerMasterKey) {
		this.customerMasterKey = customerMasterKey;
	}

	public DeviceProfileEntity getDeviceProfile() {
		return deviceProfile;
	}

	public void setDeviceProfile(DeviceProfileEntity deviceProfile) {
		this.deviceProfile = deviceProfile;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getCustomerAdminKey() {
		return customerAdminKey;
	}

	public void setCustomerAdminKey(String customerAdminKey) {
		this.customerAdminKey = customerAdminKey;
	}

	public String getFactoryManagementKey() {
		return factoryManagementKey;
	}

	public void setFactoryManagementKey(String factoryManagementKey) {
		this.factoryManagementKey = factoryManagementKey;
	}

	public String getCustomerManagementKey() {
		return customerManagementKey;
	}

	public void setCustomerManagementKey(String customerManagementKey) {
		this.customerManagementKey = customerManagementKey;
	}
	
}
