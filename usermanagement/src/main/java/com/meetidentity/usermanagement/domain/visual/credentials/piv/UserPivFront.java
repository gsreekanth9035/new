package com.meetidentity.usermanagement.domain.visual.credentials.piv;

import java.io.Serializable;

public class UserPivFront implements Serializable{
	
	private static final long serialVersionUID = 1L;
	

	public String background;

	public String Zone4FS_Agency_Specific;
	public String Zone5FS_Rank;
	public String Zone8FS_Affiliation;
	public String Zone9FS_Header;
	public String Zone10FS_Agency;
	public String Zone11FS_AgencySeal;
	public String Zone12FS_Footer;
	public String Zone13FS_Issued;
	public String Zone14FS_Expires;
	public String Zone15FS_Color_Affiliation;
	public String Zone16FS_Photo_Border;
	public String Zone18FS_Affiliation_Color_Code;
	public String Zone7FS_CircuitChip;

	public String Zone1FV_Photograph;
	public String Zone2FV_Last_Name;
	public String Zone2FV_First_Middle_Name;
	public String Zone3FV_Signature;
	public String Zone4FV_Agency_Specific;
	public String Zone5FV_Rank;
	public String Zone6FV_PDF;
	public String Zone8FV_Affiliation;
	public String Zone10FV_Agency;
	public String Zone12FV_Footer;
	public String Zone13FV_Issued;
	public String Zone14FV_Expires;
	public String Zone18FV_Affiliation_Color_code;
	public String Zone19FV_Card_Expiration;
	public String Zone20FV_Agency_Abbreviation;
	public String Zone23FV_QRCode;
	
	public String getZone5FS_Rank() {
		return Zone5FS_Rank;
	}
	public void setZone5FS_Rank(String zone5fs_Rank) {
		Zone5FS_Rank = zone5fs_Rank;
	}
	public String getZone5FV_Rank() {
		return Zone5FV_Rank;
	}
	public void setZone5FV_Rank(String zone5fv_Rank) {
		Zone5FV_Rank = zone5fv_Rank;
	}
	public String getZone8FS_Affiliation() {
		return Zone8FS_Affiliation;
	}
	public void setZone8FS_Affiliation(String zone8fs_Affiliation) {
		Zone8FS_Affiliation = zone8fs_Affiliation;
	}
	public String getZone8FV_Affiliation() {
		return Zone8FV_Affiliation;
	}
	public void setZone8FV_Affiliation(String zone8fv_Affiliation) {
		Zone8FV_Affiliation = zone8fv_Affiliation;
	}
	public String getZone9FS_Header() {
		return Zone9FS_Header;
	}
	public void setZone9FS_Header(String zone9fs_Header) {
		Zone9FS_Header = zone9fs_Header;
	}
	public String getZone10FS_Agency() {
		return Zone10FS_Agency;
	}
	public void setZone10FS_Agency(String zone10fs_Agency) {
		Zone10FS_Agency = zone10fs_Agency;
	}
	public String getZone10FV_Agency() {
		return Zone10FV_Agency;
	}
	public void setZone10FV_Agency(String zone10fv_Agency) {
		Zone10FV_Agency = zone10fv_Agency;
	}
	public String getZone13FS_Issued() {
		return Zone13FS_Issued;
	}
	public void setZone13FS_Issued(String zone13fs_Issued) {
		Zone13FS_Issued = zone13fs_Issued;
	}
	public String getZone13FV_Issued() {
		return Zone13FV_Issued;
	}
	public void setZone13FV_Issued(String zone13fv_Issued) {
		Zone13FV_Issued = zone13fv_Issued;
	}
	public String getZone14FS_Expires() {
		return Zone14FS_Expires;
	}
	public void setZone14FS_Expires(String zone14fs_Expires) {
		Zone14FS_Expires = zone14fs_Expires;
	}
	public String getZone14FV_Expires() {
		return Zone14FV_Expires;
	}
	public void setZone14FV_Expires(String zone14fv_Expires) {
		Zone14FV_Expires = zone14fv_Expires;
	}

	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	public String getZone1FV_Photograph() {
		return Zone1FV_Photograph;
	}
	public void setZone1FV_Photograph(String zone1fv_Photograph) {
		Zone1FV_Photograph = zone1fv_Photograph;
	}

	public String getZone11FS_AgencySeal() {
		return Zone11FS_AgencySeal;
	}
	public void setZone11FS_AgencySeal(String zone11fs_AgencySeal) {
		Zone11FS_AgencySeal = zone11fs_AgencySeal;
	}

	public String getZone2FV_Last_Name() {
		return Zone2FV_Last_Name;
	}
	public void setZone2FV_Last_Name(String zone2fv_Last_Name) {
		Zone2FV_Last_Name = zone2fv_Last_Name;
	}

	
	public String getZone6FV_PDF() {
		return Zone6FV_PDF;
	}
	public void setZone6FV_PDF(String zone6fv_PDF) {
		Zone6FV_PDF = zone6fv_PDF;
	}
	public String getZone12FS_Footer() {
		return Zone12FS_Footer;
	}
	public void setZone12FS_Footer(String zone12fs_Footer) {
		Zone12FS_Footer = zone12fs_Footer;
	}
	public String getZone15FS_Color_Affiliation() {
		return Zone15FS_Color_Affiliation;
	}
	public void setZone15FS_Color_Affiliation(String zone15fs_Color_Affiliation) {
		Zone15FS_Color_Affiliation = zone15fs_Color_Affiliation;
	}
	public String getZone16FS_Photo_Border() {
		return Zone16FS_Photo_Border;
	}
	public void setZone16FS_Photo_Border(String zone16fs_Photo_Border) {
		Zone16FS_Photo_Border = zone16fs_Photo_Border;
	}
	public String getZone18FS_Affiliation_Color_Code() {
		return Zone18FS_Affiliation_Color_Code;
	}
	public void setZone18FS_Affiliation_Color_Code(String zone18fs_Affiliation_Color_Code) {
		Zone18FS_Affiliation_Color_Code = zone18fs_Affiliation_Color_Code;
	}
	public String getZone2FV_First_Middle_Name() {
		return Zone2FV_First_Middle_Name;
	}
	public void setZone2FV_First_Middle_Name(String zone2fv_First_Middle_Name) {
		Zone2FV_First_Middle_Name = zone2fv_First_Middle_Name;
	}
	public String getZone3FV_Signature() {
		return Zone3FV_Signature;
	}
	public void setZone3FV_Signature(String zone3fv_Signature) {
		Zone3FV_Signature = zone3fv_Signature;
	}
	public String getZone12FV_Footer() {
		return Zone12FV_Footer;
	}
	public void setZone12FV_Footer(String zone12fv_Footer) {
		Zone12FV_Footer = zone12fv_Footer;
	}
	public String getZone18FV_Affiliation_Color_code() {
		return Zone18FV_Affiliation_Color_code;
	}
	public void setZone18FV_Affiliation_Color_code(String zone18fv_Affiliation_Color_code) {
		Zone18FV_Affiliation_Color_code = zone18fv_Affiliation_Color_code;
	}
	public String getZone19FV_Card_Expiration() {
		return Zone19FV_Card_Expiration;
	}
	public void setZone19FV_Card_Expiration(String zone19fv_Card_Expiration) {
		Zone19FV_Card_Expiration = zone19fv_Card_Expiration;
	}
	public String getZone20FV_Agency_Abbreviation() {
		return Zone20FV_Agency_Abbreviation;
	}
	public void setZone20FV_Agency_Abbreviation(String zone20fv_Agency_Abbreviation) {
		Zone20FV_Agency_Abbreviation = zone20fv_Agency_Abbreviation;
	}
	public String getZone7FS_CircuitChip() {
		return Zone7FS_CircuitChip;
	}
	public void setZone7FS_CircuitChip(String zone7fs_CircuitChip) {
		Zone7FS_CircuitChip = zone7fs_CircuitChip;
	}
	public String getZone4FS_Agency_Specific() {
		return Zone4FS_Agency_Specific;
	}
	public void setZone4FS_Agency_Specific(String zone4fs_Agency_Specific) {
		Zone4FS_Agency_Specific = zone4fs_Agency_Specific;
	}
	public String getZone4FV_Agency_Specific() {
		return Zone4FV_Agency_Specific;
	}
	public void setZone4FV_Agency_Specific(String zone4fv_Agency_Specific) {
		Zone4FV_Agency_Specific = zone4fv_Agency_Specific;
	}
	public String getZone23FV_QRCode() {
		return Zone23FV_QRCode;
	}
	public void setZone23FV_QRCode(String zone23fv_QRCode) {
		Zone23FV_QRCode = zone23fv_QRCode;
	}

	
}
