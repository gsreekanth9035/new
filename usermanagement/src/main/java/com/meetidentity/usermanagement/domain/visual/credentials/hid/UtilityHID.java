package com.meetidentity.usermanagement.domain.visual.credentials.hid;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Iterator;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.constants.XMLConstants;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserAttributeEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserHealthIDEntity;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAppearance;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserHealthID;

@Component
public class UtilityHID {

	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;
	
	private final Logger log = LoggerFactory.getLogger(UtilityHID.class);

	private Util util = new Util();

	private static String XLINK_NS = XMLConstants.XLINK_NAMESPACE_URI;
	private static String XLINK_HREF = "xlink:href";

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @param userEntity 
	 * @param organizationName 
	 * @param status 
	 * @param userHealthIDEntity
	 * @param b 
	 * @param visualTemplateType
	 * @param vc
	 * @return
	 * @throws Exception
	 */
	public Document parseHIDFront(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user,
			byte[] svgFile, byte[] jsonFile, String credentialTemplateID, UserHealthIDEntity userHealthIDEntity,
			String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
						
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {

				JSONObject hidJsonObject = new JSONObject(new String(jsonFile));
				JSONObject hidFrontJsonObject = hidJsonObject.getJSONObject("userHIDFront");
				Iterator<String> keys = hidFrontJsonObject.keys();
				log.debug("parseHIDFront: Started");
				
				while(keys.hasNext()) {
				      if(doc.getElementById(keys.next()) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				// UserHID userHID = gson.fromJson(new String(jsonFile), UserHID.class);
				// Dynamic User Variables
				switch (requestFrom) {

					case "web_portal_enroll_summary":		
						UserAttribute userAttribute = user.getUserAttribute();
						UserHealthID userHealthID = user.getUserHealthID();
						UserAppearance userAppearance = user.getUserAppearance();			
						
						doc.getElementById("ZoneXXV_Name").setTextContent(userAttribute.getFirstName()+ " " + userAttribute.getLastName());
						doc.getElementById("ZoneXXV_DOB").setTextContent(Util.formatDate(userAttribute.getDateOfBirth(), Util.DDMMYYYY_format));
						doc.getElementById("ZoneXXV_IDN").setTextContent("XXXXXXXXXX");
						doc.getElementById("ZoneXXV_IssuedDate").setTextContent("XX/XX/XXXX");
						doc.getElementById("ZoneXXV_ExpirationDate").setTextContent("XX/XX/XXXX");
						doc.getElementById("ZoneXXV_PRMRY_SUB").setTextContent(userHealthID.getPrimarySubscriber());
						doc.getElementById("ZoneXXV_PRMRY_SUB_ID").setTextContent(userHealthID.getPrimarySubscriberID());
						doc.getElementById("ZoneXXV_PCP").setTextContent(userHealthID.getPcp());
						doc.getElementById("ZoneXXV_PCP_PHONE").setTextContent(userHealthID.getPcpPhone());
						doc.getElementById("ZoneXXV_POLICY_NUMBER").setTextContent(userHealthID.getPolicyNumber());
						doc.getElementById("ZoneXXV_GROUP_PLAN").setTextContent(userHealthID.getGroupPlan());
						doc.getElementById("ZoneXXV_HEALTH_PLAN_NUMBER").setTextContent(userHealthID.getHealthPlanNumber());
						 user.getUserBiometrics().stream().forEach(userBio -> {
								if (userBio.getType().equalsIgnoreCase("FACE")) {
									
									int width = Integer.parseInt(doc.getElementById("ZoneXXV_Photograph").getAttribute("width"));
									int height = Integer.parseInt(doc.getElementById("ZoneXXV_Photograph").getAttribute("height"));
									try {
										Image image = Util
												.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
										BufferedImage bufferedImage = Util.resizeImage(image, width, height);
										if(user.getIsTransparentPhotoRequired()) {
											bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
										}
										String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
										doc.getElementById("ZoneXXV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
												"data:;base64," + base64Data);

									} catch (IOException e) {
										doc.getElementById("ZoneXXV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									}
								}
								if (userBio.getType().equalsIgnoreCase("SIGNATURE")) {
									doc.getElementById("ZoneXXV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));									
								}
							});
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":						
						UserAttributeEntity userAttributee = userEntity.getUserAttribute();	
						doc.getElementById("ZoneXXV_Name").setTextContent(userAttributee.getFirstName() + " " + userAttributee.getLastName());
						doc.getElementById("ZoneXXV_DOB").setTextContent(Util.formatDate(userAttributee.getDateOfBirth(), Util.DDMMYYYY_format));
						doc.getElementById("ZoneXXV_IDN").setTextContent(userHealthIDEntity.getIdNumber()==null?"XXXXXXXXXX":userHealthIDEntity.getIdNumber());
						doc.getElementById("ZoneXXV_IssuedDate").setTextContent(userHealthIDEntity.getDateOfIssuance() == null? "XX/XX/XXXX" : Util.formatDate(userHealthIDEntity.getDateOfIssuance(), Util.DDMMYYYY_format));
						doc.getElementById("ZoneXXV_ExpirationDate").setTextContent(userHealthIDEntity.getDateOfExpiry() == null? "XX/XX/XXXX" : Util.formatDate(userHealthIDEntity.getDateOfExpiry(), Util.DDMMYYYY_format));
						doc.getElementById("ZoneXXV_PRMRY_SUB").setTextContent(userHealthIDEntity.getPrimarySubscriber());
						doc.getElementById("ZoneXXV_PRMRY_SUB_ID").setTextContent(userHealthIDEntity.getPrimarySubscriberID());
						doc.getElementById("ZoneXXV_PCP").setTextContent(userHealthIDEntity.getPcp());
						doc.getElementById("ZoneXXV_PCP_PHONE").setTextContent(userHealthIDEntity.getPcpPhone());
						doc.getElementById("ZoneXXV_POLICY_NUMBER").setTextContent(userHealthIDEntity.getPolicyNumber());
						doc.getElementById("ZoneXXV_GROUP_PLAN").setTextContent(userHealthIDEntity.getGroupPlan());
						doc.getElementById("ZoneXXV_HEALTH_PLAN_NUMBER").setTextContent(userHealthIDEntity.getHealthPlanNumber());
						
						 userEntity.getUserBiometrics().stream().forEach(userBio -> {
								if (userBio.getType().equalsIgnoreCase("FACE")) {
									
									int width = Integer.parseInt(doc.getElementById("ZoneXXV_Photograph").getAttribute("width"));
									int height = Integer.parseInt(doc.getElementById("ZoneXXV_Photograph").getAttribute("height"));
									try {
										Image image = Util
												.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
										BufferedImage bufferedImage = Util.resizeImage(image, width, height);
										if(user.getIsTransparentPhotoRequired()) {
											bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
										}
										String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
										doc.getElementById("ZoneXXV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
												"data:;base64," + base64Data);

									} catch (IOException e) {
										doc.getElementById("ZoneXXV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									}
								}
								if (userBio.getType().equalsIgnoreCase("SIGNATURE")) {
									doc.getElementById("ZoneXXV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));									
								}
							});
						break;
					default :
				
				}
				
//				doc.getElementById("ZoneXX_Background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + userHID.getUserHIDFront().getZoneXX_Background());
//				doc.getElementById("ZoneXX_Logo").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + userHID.getUserHIDFront().getZoneXX_Logo());
		
	
				log.debug("parseHidFront: Ended Dynamic text Variable");
	
				return doc;
			}
						
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @param userEntity 
	 * @param organizationName 
	 * @param userHealthIDEntity
	 * @param b 
	 * @param vc
	 * @return
	 * @throws Exception
	 */
	public Document parseHIDBack(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity, User user,
			String credentialTemplateID, byte[] svgFile, byte[] jsonFile, UserHealthIDEntity userHealthIDEntity, String visualCredentialType, boolean simplyParse) throws Exception {
		try {
			
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {

				JSONObject hidJsonObject = new JSONObject(new String(jsonFile));
				JSONObject hidBackJsonObject = hidJsonObject.getJSONObject("userHIDBack");
				Iterator<String> keys = hidBackJsonObject.keys();
				log.debug("parseHIDBack: Started");
				
				while(keys.hasNext()) {
				      if(doc.getElementById(keys.next()) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				// UserHID userHID = gson.fromJson(new String(jsonFile), UserHID.class);
				// Dynamic User Variables
				switch (requestFrom) {

					case "web_portal_enroll_summary":		
						UserAttribute userAttribute = user.getUserAttribute();
						UserHealthID userHealthID = user.getUserHealthID();
						UserAppearance userAppearance = user.getUserAppearance();			
						
						doc.getElementById("ZoneXXV_Org_CardSN").setTextContent("XXXXXXXXXX");
						doc.getElementById("ZoneXXV_IssuerIN").setTextContent("XXXXXXXXXX");
						 doc.getElementById("ZoneXXV_Qrcode")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + user.getId() + "#" + credentialTemplateID + "#" + "XXXXXXXXXX",
											BarcodeFormat.QR_CODE, 300, 300))));						
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":						

						doc.getElementById("ZoneXXV_Org_CardSN").setTextContent(userHealthIDEntity.getAgencyCardSN()==null?"XXXXXXXXXXXX":userHealthIDEntity.getAgencyCardSN());
						doc.getElementById("ZoneXXV_IssuerIN").setTextContent(userHealthIDEntity.getIssuerIN()==null?"XXXXXXXXXXXX":userHealthIDEntity.getIssuerIN());
						String idNumber = "XXXXXXXXXXXX";
						if(userHealthIDEntity.getIdNumber() != null) {
							idNumber = userHealthIDEntity.getIdNumber();
						}
						 doc.getElementById("ZoneXXV_Qrcode")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + userEntity.getId() + "#" + credentialTemplateID + "#" + idNumber,
											BarcodeFormat.QR_CODE, 300, 300))));
						break;
					default :
				
				}
				
//				doc.getElementById("ZoneXX_Background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + userHID.getUserHIDBack().getZoneXX_Background());
		
	
				log.debug("parseHidFront: Ended Dynamic text Variable");
	
				return doc;
			}

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}

}
