package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.meetidentity.usermanagement.annotations.SequenceValue;

@Entity
@Table(name = "user")
public class UserEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@SequenceValue
	@Column(name = "uims_id", nullable = false)
	private String uimsId;

	@Column(name = "name")
	private String name;

	@Column(name = "reference_name")
	private String referenceName;

	@Version
	@Column(name = "version")
	private int version;

	@Column(name = "status")
	private String status;

	@Column(name = "group_id")
	private String groupId;

	@Column(name = "group_name")
	private String groupName;

	@Column(name = "ref_approval_group_id")
	private String refApprovalGroupId;

	@Column(name = "is_enrollment_updated")
	private Boolean isEnrollmentUpdated;

	@Column(name = "user_status")
	private String userStatus;

	@Column(name = "is_federated")
	private Boolean isFederated;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
	private OrganizationEntity organization;

	@OneToOne(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private UserAttributeEntity userAttribute;

	@OneToOne(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private UserAppearanceEntity userAppearance;

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserBiometricEntity> userBiometrics;

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserBiometricSkipEntity> userBiometricSkips;

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserIDProofDocumentEntity> userIDProofDocuments;

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserPivEntity> userPivCredentials;

	@OneToOne(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private UserNationalIdEntity userNationalId;

	@OneToOne(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private UserPermanentResidentIdEntity userPermanentResidentId;

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserEmployeeIDEntity> userEmployeeIDs;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserPersonalizedIDEntity> userPersonalizedIDs;
	
	@OneToOne(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private UserVoterIDEntity userVoterID;	

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserStudentIDEntity> userStudentIDs;

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserHealthIDEntity> userHealthIDs;

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserDrivingLicenseEntity> userDrivingLicenses;

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserIdentificationCodeEntity> userIdentificationCodes;

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserVehicleEntity> userVehicles;

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserOtpEntity> userOtps;

	@OneToOne(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private UserHealthServiceEntity userHealthService;

	@OneToOne(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private UserLocationEntity userLocation;

	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserEnrollmentHistoryEntity> userEnrollmentsHistory;
	
	@OneToOne(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private EnrollmentStepsStatusEntity enrollmentStepsStatusEntity;
	
	@OneToOne(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private UserAddressEntity userAddress;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserGroupsEntity> userGroups;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReferenceName() {
		return referenceName;
	}

	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getRefApprovalGroupId() {
		return refApprovalGroupId;
	}

	public void setRefApprovalGroupId(String refApprovalGroupId) {
		this.refApprovalGroupId = refApprovalGroupId;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

	public UserAttributeEntity getUserAttribute() {
		return userAttribute;
	}

	public void setUserAttribute(UserAttributeEntity userAttribute) {
		this.userAttribute = userAttribute;
		userAttribute.setUser(this);
	}

	public UserAppearanceEntity getUserAppearance() {
		return userAppearance;
	}

	public void setUserAppearance(UserAppearanceEntity userAppearance) {
		this.userAppearance = userAppearance;
		userAppearance.setUser(this);
	}

	public List<UserBiometricEntity> getUserBiometrics() {
		return userBiometrics;
	}

	public List<UserBiometricSkipEntity> getUserBiometricSkips() {
		return userBiometricSkips;
	}

	public void addUserBiometric(UserBiometricEntity userBiometric) {
		if (this.userBiometrics == null) {
			this.userBiometrics = new ArrayList<UserBiometricEntity>();
		}
		this.userBiometrics.add(userBiometric);
		userBiometric.setUser(this);
	}

	public void removeBiometric(UserBiometricEntity userBiometric) {
		if (this.userBiometrics != null) {
			this.userBiometrics.remove(userBiometric);
			userBiometric.setUser(null);
		}
	}

	public void addUserBiometricSkip(UserBiometricSkipEntity userBiometricSkip) {
		if (this.userBiometricSkips == null) {
			this.userBiometricSkips = new ArrayList<UserBiometricSkipEntity>();
		}
		this.userBiometricSkips.add(userBiometricSkip);
		userBiometricSkip.setUser(this);
	}

	public void removeUserBiometricSkip(UserBiometricSkipEntity userBiometricSkip) {
		if (this.userBiometricSkips != null) {
			this.userBiometricSkips.remove(userBiometricSkip);
			userBiometricSkip.setUser(null);
		}
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public List<UserIDProofDocumentEntity> getUserIDProofDocuments() {
		return userIDProofDocuments;
	}

	public void addUserIDProofDocuments(UserIDProofDocumentEntity userIDProofDocument) {
		if (this.userIDProofDocuments == null) {
			this.userIDProofDocuments = new ArrayList<UserIDProofDocumentEntity>();
		}
		this.userIDProofDocuments.add(userIDProofDocument);
		userIDProofDocument.setUser(this);

	}

	public void removeUserIDProofDocuments(UserIDProofDocumentEntity userIDProofDocumentEntity) {
		userIDProofDocumentEntity.setUser(null);
		this.userIDProofDocuments.remove(userIDProofDocumentEntity);
	}

	public List<UserPivEntity> getUserPivCredentials() {
		return userPivCredentials;
	}

	public void setUserPivCredentials(List<UserPivEntity> userPivCredentials) {
		this.userPivCredentials = userPivCredentials;
		for (UserPivEntity userPivEntity : userPivCredentials) {
			userPivEntity.setUser(this);
		}
	}

	public UserNationalIdEntity getUserNationalId() {
		return userNationalId;
	}

	public void setUserNationalId(UserNationalIdEntity userNationalId) {
		this.userNationalId = userNationalId;
		userNationalId.setUser(this);
	}	
	
	public UserVoterIDEntity getUserVoterID() {
		return userVoterID;
	}

	public void setUserVoterID(UserVoterIDEntity userVoterID) {
		this.userVoterID = userVoterID;
		userVoterID.setUser(this);
	}	

	public UserPermanentResidentIdEntity getUserPermanentResidentId() {
		return userPermanentResidentId;
	}

	public void setUserPermanentResidentId(UserPermanentResidentIdEntity userPermanentResidentId) {
		this.userPermanentResidentId = userPermanentResidentId;
		userPermanentResidentId.setUser(this);
	}

	public List<UserEmployeeIDEntity> getUserEmployeeIDs() {
		return userEmployeeIDs;
	}

	public void setUserEmployeeIDs(List<UserEmployeeIDEntity> userEmployeeIDs) {
		this.userEmployeeIDs = userEmployeeIDs;
		for (UserEmployeeIDEntity userEmployeeIDEntity : userEmployeeIDs) {
			userEmployeeIDEntity.setUser(this);
		}
	}
	public void addUserEmployeeIDs(List<UserEmployeeIDEntity> userEmployeeIDs) {
		if (this.userEmployeeIDs == null) {
			this.userEmployeeIDs = new ArrayList<UserEmployeeIDEntity>();
		}
		this.userEmployeeIDs.addAll(userEmployeeIDs);
	}

	public List<UserPersonalizedIDEntity> getUserPersonalizedIDs() {
		return userPersonalizedIDs;
	}

	public void setUserPersonalizedIDs(List<UserPersonalizedIDEntity> userPersonalizedIDs) {
		this.userPersonalizedIDs = userPersonalizedIDs;
		for (UserPersonalizedIDEntity userPersonalizedIDEntity : userPersonalizedIDs) {
			userPersonalizedIDEntity.setUser(this);
		}
	}
	
	public void addUserPersonalizedIDs(List<UserPersonalizedIDEntity> userPersonalizedIDs) {
		if (this.userPersonalizedIDs == null) {
			this.userPersonalizedIDs = new ArrayList<UserPersonalizedIDEntity>();
		}
		this.userPersonalizedIDs.addAll(userPersonalizedIDs);
	}

	public List<UserStudentIDEntity> getUserStudentIDs() {
		return userStudentIDs;
	}

	public void setUserStudentIDs(List<UserStudentIDEntity> userStudentIDs) {
		this.userStudentIDs = userStudentIDs;
		for (UserStudentIDEntity userStudentIDEntity : userStudentIDs) {
			userStudentIDEntity.setUser(this);
		}
	}

	public List<UserHealthIDEntity> getUserHealthIDs() {
		return userHealthIDs;
	}

	public void setUserHealthIDs(List<UserHealthIDEntity> userHealthIDs) {
		this.userHealthIDs = userHealthIDs;
		for (UserHealthIDEntity userHealthIDEntity : userHealthIDs) {
			userHealthIDEntity.setUser(this);
		}
	}

	public List<UserDrivingLicenseEntity> getUserDrivingLicenses() {
		return userDrivingLicenses;
	}

	public void setUserDrivingLicenses(List<UserDrivingLicenseEntity> userDrivingLicenses) {
		this.userDrivingLicenses = userDrivingLicenses;
		for (UserDrivingLicenseEntity userDrivingLicenseEntity : userDrivingLicenses) {
			userDrivingLicenseEntity.setUser(this);
		}
	}

	public void setUserHealthService(UserHealthServiceEntity userHealthServiceEntity) {

		this.userHealthService = userHealthServiceEntity;
		userHealthService.setUser(this);

	}

	public void addUserDrivingLicenses(UserDrivingLicenseEntity userDrivingLicense) {
		if (this.userDrivingLicenses == null) {
			this.userDrivingLicenses = new ArrayList<UserDrivingLicenseEntity>();
		}
		this.userDrivingLicenses.add(userDrivingLicense);
		userDrivingLicense.setUser(this);

	}

	public void setUserBiometrics(List<UserBiometricEntity> userBiometrics) {
		this.userBiometrics = userBiometrics;
		for (UserBiometricEntity userBiometricEntity : userBiometrics) {
			userBiometricEntity.setUser(this);
		}
	}

	public void setUserBiometricSkips(List<UserBiometricSkipEntity> userBiometricSkips) {
		this.userBiometricSkips = userBiometricSkips;
		for (UserBiometricSkipEntity userBiometricSkipEntity : userBiometricSkips) {
			userBiometricSkipEntity.setUser(this);
		}
	}

	public void setUserIDProofDocuments(List<UserIDProofDocumentEntity> userIDProofDocuments) {
		this.userIDProofDocuments = userIDProofDocuments;
		for (UserIDProofDocumentEntity userIDProofDocumentEntity : userIDProofDocuments) {
			userIDProofDocumentEntity.setUser(this);
		}
	}

	public List<UserIdentificationCodeEntity> getUserIdentificationCodes() {
		return userIdentificationCodes;
	}

	public void setUserIdentificationCodes(List<UserIdentificationCodeEntity> userIdentificationCodes) {
		this.userIdentificationCodes = userIdentificationCodes;
		for (UserIdentificationCodeEntity userIdentificationCodeEntity : userIdentificationCodes) {
			userIdentificationCodeEntity.setUser(this);
		}
	}

	public List<UserVehicleEntity> getUserVehicles() {
		return userVehicles;
	}

	public void setUserVehicles(List<UserVehicleEntity> userVehicles) {
		this.userVehicles = userVehicles;
		for (UserVehicleEntity userVehicleEntity : userVehicles) {
			userVehicleEntity.setUser(this);
		}
	}

	public List<UserOtpEntity> getUserOtps() {
		return userOtps;
	}

	public void setUserOtps(List<UserOtpEntity> userOtps) {
		this.userOtps = userOtps;
		for (UserOtpEntity userOtpEntity : userOtps) {
			userOtpEntity.setUser(this);
		}
	}

	public UserHealthServiceEntity getUserHealthService() {
		return userHealthService;
	}

	public UserLocationEntity getUserLocation() {
		return userLocation;
	}

	public void setUserLocation(UserLocationEntity userLocation) {
		this.userLocation = userLocation;
		userLocation.setUser(this);
	}

	public List<UserEnrollmentHistoryEntity> getUserEnrollmentsHistory() {
		return userEnrollmentsHistory;
	}

	public void setUserEnrollmentsHistory(List<UserEnrollmentHistoryEntity> userEnrollmentsHistory) {
		this.userEnrollmentsHistory = userEnrollmentsHistory;
		for (UserEnrollmentHistoryEntity userEnrollmentHistory : userEnrollmentsHistory) {
			userEnrollmentHistory.setUser(this);
		}
	}

	public void addUserEnrollmentHistory(UserEnrollmentHistoryEntity userEnrollmentsHistoryEntity) {

		if (this.userEnrollmentsHistory == null) {
			this.userEnrollmentsHistory = new ArrayList<UserEnrollmentHistoryEntity>();
		}
		this.userEnrollmentsHistory.add(userEnrollmentsHistoryEntity);
		userEnrollmentsHistoryEntity.setUser(this);
	}
		

	public EnrollmentStepsStatusEntity getEnrollmentStepsStatusEntity() {
		return enrollmentStepsStatusEntity;
	}

	public void setEnrollmentStepsStatusEntity(EnrollmentStepsStatusEntity enrollmentStepsStatusEntity) {
		this.enrollmentStepsStatusEntity = enrollmentStepsStatusEntity;
	}

	public Boolean getIsEnrollmentUpdated() {
		return isEnrollmentUpdated;
	}

	public void setIsEnrollmentUpdated(Boolean isEnrollmentUpdated) {
		this.isEnrollmentUpdated = isEnrollmentUpdated;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public Boolean getIsFederated() {
		return isFederated;
	}

	public void setIsFederated(Boolean isFederated) {
		this.isFederated = isFederated;
	}

	public UserAddressEntity getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(UserAddressEntity userAddress) {
		this.userAddress = userAddress;
	}

	public List<UserGroupsEntity> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(List<UserGroupsEntity> userGroups) {
		this.userGroups = userGroups;
	}
	
	public void addUserGroups(UserGroupsEntity userGroup) {

		if (this.userGroups == null) {
			this.userGroups = new ArrayList<UserGroupsEntity>();
		}
		this.userGroups.add(userGroup);
		userGroup.setUser(this);
	}
}
