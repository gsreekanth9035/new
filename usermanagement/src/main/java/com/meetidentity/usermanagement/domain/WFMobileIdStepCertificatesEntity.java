package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "wf_mobile_id_step_certificates")
public class WFMobileIdStepCertificatesEntity extends AbstractAuditingEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "cert_type")
	private String certType;

	@Column(name = "ca_server")
	private String caServer;

	@Column(name = "cert_template")
	private String certTemplate;

	@Column(name = "key_escrow")
	private Boolean keyEscrow;

	@Column(name = "disable_revoke")
	private Boolean disableRevoke;

	@Column(name = "algorithm")
	private String algorithm;

	@Column(name = "key_size")
	private int keySize;

	@Column(name = "subject_an")
	private String subjectAN;

	@Column(name = "subject_dn")
	private String subjectDN;

	@Column(name = "status")
	private String status;

	@Version
	@Column(name = "version")
	private int version;

	@ManyToOne
	@JoinColumn(name = "wf_mobile_identity_issuance_id")
	private WFMobileIDIdentityIssuanceEntity wfMobileIDIdentitiesIssuance;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCaServer() {
		return caServer;
	}

	public void setCaServer(String caServer) {
		this.caServer = caServer;
	}

	public String getCertTemplate() {
		return certTemplate;
	}

	public void setCertTemplate(String certTemplate) {
		this.certTemplate = certTemplate;
	}

	public Boolean getKeyEscrow() {
		return keyEscrow;
	}

	public void setKeyEscrow(Boolean keyEscrow) {
		this.keyEscrow = keyEscrow;
	}

	public Boolean getDisableRevoke() {
		return disableRevoke;
	}

	public void setDisableRevoke(Boolean disableRevoke) {
		this.disableRevoke = disableRevoke;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public int getKeySize() {
		return keySize;
	}

	public void setKeySize(int keySize) {
		this.keySize = keySize;
	}

	public String getSubjectDN() {
		return subjectDN;
	}

	public void setSubjectDN(String subjectDN) {
		this.subjectDN = subjectDN;
	}

	public String getSubjectAN() {
		return subjectAN;
	}

	public void setSubjectAN(String subjectAN) {
		this.subjectAN = subjectAN;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public WFMobileIDIdentityIssuanceEntity getWfMobileIDIdentitiesIssuance() {
		return wfMobileIDIdentitiesIssuance;
	}

	public void setWfMobileIDIdentitiesIssuance(WFMobileIDIdentityIssuanceEntity wfMobileIDIdentitiesIssuance) {
		this.wfMobileIDIdentitiesIssuance = wfMobileIDIdentitiesIssuance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

}
