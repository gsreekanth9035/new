package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "fingerprint_device_config")
public class FingerprintDeviceConfigEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "org_id")
	private OrganizationEntity organization;

	@Column(name = "device_name")
	private String deviceName;

	@Column(name = "device_manufacturer")
	private String deviceManufacturer;

	@Column(name = "capture_type")
	private String captureType;
	
	@Column(name="roll_support")
	private Boolean rollSupport;
	
	@Column(name="flat_support")
	private Boolean flatSupport;

	@Column(name = "ansi_template_support")
	private Boolean ansiTemplateSupport;

	@Column(name = "iso_template_support")
	private Boolean isoTemplateSupport;

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceManufacturer() {
		return deviceManufacturer;
	}

	public void setDeviceManufacturer(String deviceManufacturer) {
		this.deviceManufacturer = deviceManufacturer;
	}

	public String getCaptureType() {
		return captureType;
	}

	public void setCaptureType(String captureType) {
		this.captureType = captureType;
	}
	
	public Boolean getRollSupport() {
		return rollSupport;
	}

	public void setRollSupport(Boolean rollSupport) {
		this.rollSupport = rollSupport;
	}

	public Boolean getFlatSupport() {
		return flatSupport;
	}

	public void setFlatSupport(Boolean flatSupport) {
		this.flatSupport = flatSupport;
	}
	
	public Boolean getAnsiTemplateSupport() {
		return ansiTemplateSupport;
	}

	public void setAnsiTemplateSupport(Boolean ansiTemplateSupport) {
		this.ansiTemplateSupport = ansiTemplateSupport;
	}

	public Boolean getIsoTemplateSupport() {
		return isoTemplateSupport;
	}

	public void setIsoTemplateSupport(Boolean isoTemplateSupport) {
		this.isoTemplateSupport = isoTemplateSupport;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
