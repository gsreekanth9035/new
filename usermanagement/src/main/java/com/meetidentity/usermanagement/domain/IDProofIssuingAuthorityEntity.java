package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "id_proof_issuing_authority")
public class IDProofIssuingAuthorityEntity extends AbstractAuditingEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "issuing_authority")
	private String issuingAuthority;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "org_identities_id")
	private OrganizationIdentitiesEntity organizationIdentity;

	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public OrganizationIdentitiesEntity getOrganizationIdentity() {
		return organizationIdentity;
	}

	public void setOrganizationIdentity(OrganizationIdentitiesEntity organizationIdentity) {
		this.organizationIdentity = organizationIdentity;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
