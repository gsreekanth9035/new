package com.meetidentity.usermanagement.domain.visual.credentials.piv;

import java.io.Serializable;

public class UserPivBack implements Serializable {

	private static final long serialVersionUID = 1L;

	private String background;

	private String Zone1BS_Agency_CardSN;
	private String Zone2BS_IssuerIN;
	private String Zone4BS_ReturnTo;
	private String Zone4BS_ReturnAddress_l1;
	private String Zone4BS_ReturnAddress_l2;
	private String Zone5BS_PhysicalCC_HGT;
	private String Zone5BS_PhysicalCC_Eye;
	private String Zone5BS_PhysicalCC_Hair;
	private String Zone6BS_Ainfo;
	private String Zone7BS_Section499_1;
	private String Zone7BS_Section499_2;

	private String Zone1BV_Agency_CardSN;
	private String Zone2BV_IssuerIN;
	private String Zone5BV_PhysicalCC_HGT;
	private String Zone5BV_PhysicalCC_Eye;
	private String Zone5BV_PhysicalCC_Hair;
	private String Zone8BV_LinearBC;

	public String getZone2BV_IssuerIN() {
		return Zone2BV_IssuerIN;
	}

	public void setZone2BV_IssuerIN(String zone2bv_IssuerIN) {
		Zone2BV_IssuerIN = zone2bv_IssuerIN;
	}

	public String getZone1BV_Agency_CardSN() {
		return Zone1BV_Agency_CardSN;
	}

	public void setZone1BV_Agency_CardSN(String zone1bv_Agency_CardSN) {
		Zone1BV_Agency_CardSN = zone1bv_Agency_CardSN;
	}

	public String getZone5BS_PhysicalCC_HGT() {
		return Zone5BS_PhysicalCC_HGT;
	}

	public void setZone5BS_PhysicalCC_HGT(String zone5bs_PhysicalCC_HGT) {
		Zone5BS_PhysicalCC_HGT = zone5bs_PhysicalCC_HGT;
	}

	public String getZone5BS_PhysicalCC_Eye() {
		return Zone5BS_PhysicalCC_Eye;
	}

	public void setZone5BS_PhysicalCC_Eye(String zone5bs_PhysicalCC_Eye) {
		Zone5BS_PhysicalCC_Eye = zone5bs_PhysicalCC_Eye;
	}

	public String getZone5BS_PhysicalCC_Hair() {
		return Zone5BS_PhysicalCC_Hair;
	}

	public void setZone5BS_PhysicalCC_Hair(String zone5bs_PhysicalCC_Hair) {
		Zone5BS_PhysicalCC_Hair = zone5bs_PhysicalCC_Hair;
	}

	public String getZone4BS_ReturnAddress_l1() {
		return Zone4BS_ReturnAddress_l1;
	}

	public void setZone4BS_ReturnAddress_l1(String zone4bs_ReturnAddress_l1) {
		Zone4BS_ReturnAddress_l1 = zone4bs_ReturnAddress_l1;
	}

	public String getZone4BS_ReturnAddress_l2() {
		return Zone4BS_ReturnAddress_l2;
	}

	public void setZone4BS_ReturnAddress_l2(String zone4bs_ReturnAddress_l2) {
		Zone4BS_ReturnAddress_l2 = zone4bs_ReturnAddress_l2;
	}

	public String getZone7BS_Section499_1() {
		return Zone7BS_Section499_1;
	}

	public void setZone7BS_Section499_1(String zone7bs_Section499_1) {
		Zone7BS_Section499_1 = zone7bs_Section499_1;
	}

	public String getZone7BS_Section499_2() {
		return Zone7BS_Section499_2;
	}

	public void setZone7BS_Section499_2(String zone7bs_Section499_2) {
		Zone7BS_Section499_2 = zone7bs_Section499_2;
	}

	public String getZone5BV_PhysicalCC_HGT() {
		return Zone5BV_PhysicalCC_HGT;
	}

	public void setZone5BV_PhysicalCC_HGT(String zone5bv_PhysicalCC_HGT) {
		Zone5BV_PhysicalCC_HGT = zone5bv_PhysicalCC_HGT;
	}

	public String getZone5BV_PhysicalCC_Eye() {
		return Zone5BV_PhysicalCC_Eye;
	}

	public void setZone5BV_PhysicalCC_Eye(String zone5bv_PhysicalCC_Eye) {
		Zone5BV_PhysicalCC_Eye = zone5bv_PhysicalCC_Eye;
	}

	public String getZone5BV_PhysicalCC_Hair() {
		return Zone5BV_PhysicalCC_Hair;
	}

	public void setZone5BV_PhysicalCC_Hair(String zone5bv_PhysicalCC_Hair) {
		Zone5BV_PhysicalCC_Hair = zone5bv_PhysicalCC_Hair;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getZone8BV_LinearBC() {
		return Zone8BV_LinearBC;
	}

	public void setZone8BV_LinearBC(String zone8bv_LinearBC) {
		Zone8BV_LinearBC = zone8bv_LinearBC;
	}

	public String getZone1BS_Agency_CardSN() {
		return Zone1BS_Agency_CardSN;
	}

	public void setZone1BS_Agency_CardSN(String zone1bs_Agency_CardSN) {
		Zone1BS_Agency_CardSN = zone1bs_Agency_CardSN;
	}

	public String getZone2BS_IssuerIN() {
		return Zone2BS_IssuerIN;
	}

	public void setZone2BS_IssuerIN(String zone2bs_IssuerIN) {
		Zone2BS_IssuerIN = zone2bs_IssuerIN;
	}

	public String getZone4BS_ReturnTo() {
		return Zone4BS_ReturnTo;
	}

	public void setZone4BS_ReturnTo(String zone4bs_ReturnTo) {
		Zone4BS_ReturnTo = zone4bs_ReturnTo;
	}

	public String getZone6BS_Ainfo() {
		return Zone6BS_Ainfo;
	}

	public void setZone6BS_Ainfo(String zone6bs_Ainfo) {
		Zone6BS_Ainfo = zone6bs_Ainfo;
	}

}
