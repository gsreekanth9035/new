package com.meetidentity.usermanagement.domain.visual.credentials.sid;

import java.io.Serializable;

public class UserSIDfront implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	 private String Zone0FS_Background;
	 private String Zone5FS_EN;
	 private String Zone8FS_Institution;
	 private String Zone9FS_Header;
	 private String Zone10FS_Department;
	 private String Zone11FS_OrgSeal;
	 private String Zone13aFS_DOB;
	 private String Zone13FS_Issued;
	 private String Zone14FS_Expires;

	 private String Zone1FV_Photograph;
	 private String Zone_FV_Country;
	 private String Zone_FV_Name;
	 private String Zone5FV_EN;
	 private String Zone8FV_Institution;
	 private String Zone10FV_Department;
	 private String Zone13aFV_DOB;
	 private String Zone13FV_Issued;
	 private String Zone14FV_Expires;
	 private String Zone15FV_Signature;
	public String getZone0FS_Background() {
		return Zone0FS_Background;
	}
	public void setZone0FS_Background(String zone0fs_Background) {
		Zone0FS_Background = zone0fs_Background;
	}
	public String getZone5FS_EN() {
		return Zone5FS_EN;
	}
	public void setZone5FS_EN(String zone5fs_EN) {
		Zone5FS_EN = zone5fs_EN;
	}
	public String getZone8FS_Institution() {
		return Zone8FS_Institution;
	}
	public void setZone8FS_Institution(String zone8fs_Institution) {
		Zone8FS_Institution = zone8fs_Institution;
	}
	public String getZone9FS_Header() {
		return Zone9FS_Header;
	}
	public void setZone9FS_Header(String zone9fs_Header) {
		Zone9FS_Header = zone9fs_Header;
	}
	public String getZone10FS_Department() {
		return Zone10FS_Department;
	}
	public void setZone10FS_Department(String zone10fs_Department) {
		Zone10FS_Department = zone10fs_Department;
	}
	public String getZone11FS_OrgSeal() {
		return Zone11FS_OrgSeal;
	}
	public void setZone11FS_OrgSeal(String zone11fs_OrgSeal) {
		Zone11FS_OrgSeal = zone11fs_OrgSeal;
	}
	public String getZone13aFS_DOB() {
		return Zone13aFS_DOB;
	}
	public void setZone13aFS_DOB(String zone13aFS_DOB) {
		Zone13aFS_DOB = zone13aFS_DOB;
	}
	public String getZone13FS_Issued() {
		return Zone13FS_Issued;
	}
	public void setZone13FS_Issued(String zone13fs_Issued) {
		Zone13FS_Issued = zone13fs_Issued;
	}
	public String getZone14FS_Expires() {
		return Zone14FS_Expires;
	}
	public void setZone14FS_Expires(String zone14fs_Expires) {
		Zone14FS_Expires = zone14fs_Expires;
	}
	public String getZone1FV_Photograph() {
		return Zone1FV_Photograph;
	}
	public void setZone1FV_Photograph(String zone1fv_Photograph) {
		Zone1FV_Photograph = zone1fv_Photograph;
	}
	public String getZone_FV_Country() {
		return Zone_FV_Country;
	}
	public void setZone_FV_Country(String zone_FV_Country) {
		Zone_FV_Country = zone_FV_Country;
	}
	public String getZone_FV_Name() {
		return Zone_FV_Name;
	}
	public void setZone_FV_Name(String zone_FV_Name) {
		Zone_FV_Name = zone_FV_Name;
	}
	public String getZone5FV_EN() {
		return Zone5FV_EN;
	}
	public void setZone5FV_EN(String zone5fv_EN) {
		Zone5FV_EN = zone5fv_EN;
	}
	public String getZone8FV_Institution() {
		return Zone8FV_Institution;
	}
	public void setZone8FV_Institution(String zone8fv_Institution) {
		Zone8FV_Institution = zone8fv_Institution;
	}
	public String getZone10FV_Department() {
		return Zone10FV_Department;
	}
	public void setZone10FV_Department(String zone10fv_Department) {
		Zone10FV_Department = zone10fv_Department;
	}
	public String getZone13aFV_DOB() {
		return Zone13aFV_DOB;
	}
	public void setZone13aFV_DOB(String zone13aFV_DOB) {
		Zone13aFV_DOB = zone13aFV_DOB;
	}
	public String getZone13FV_Issued() {
		return Zone13FV_Issued;
	}
	public void setZone13FV_Issued(String zone13fv_Issued) {
		Zone13FV_Issued = zone13fv_Issued;
	}
	public String getZone14FV_Expires() {
		return Zone14FV_Expires;
	}
	public void setZone14FV_Expires(String zone14fv_Expires) {
		Zone14FV_Expires = zone14fv_Expires;
	}
	public String getZone15FV_Signature() {
		return Zone15FV_Signature;
	}
	public void setZone15FV_Signature(String zone15fv_Signature) {
		Zone15FV_Signature = zone15fv_Signature;
	}
	 
	 
	 
	
}
