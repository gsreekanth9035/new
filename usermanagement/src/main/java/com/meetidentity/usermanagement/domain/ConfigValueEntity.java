package com.meetidentity.usermanagement.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="config_value")
public class ConfigValueEntity  extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
    @Column(name="value")
	private String value;
    
    @Column(name="active")
    private Boolean active;

    @Version
    @Column(name="version")
    private int version;
    
    @ManyToOne
    @JoinColumn(name="config_id")
    private ConfigEntity config;
 
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="parent_config_value_id", referencedColumnName="id", nullable = true) 
    private ConfigValueEntity parentConfigValueEntity;
    
    @OneToOne(mappedBy = "configValue", cascade = CascadeType.ALL)
	private DeviceProfileConfigEntity devProfileConfig;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public ConfigEntity getConfig() {
		return config;
	}

	public void setConfig(ConfigEntity config) {
		this.config = config;
	}

	public ConfigValueEntity getParentConfigValueEntity() {
		return parentConfigValueEntity;
	}

	public void setParentConfigValueEntity(ConfigValueEntity parentConfigValueEntity) {
		this.parentConfigValueEntity = parentConfigValueEntity;
	}

	public DeviceProfileConfigEntity getDevProfileConfig() {
		return devProfileConfig;
	}

	public void setDevProfileConfig(DeviceProfileConfigEntity devProfileConfig) {
		this.devProfileConfig = devProfileConfig;
	}


}
