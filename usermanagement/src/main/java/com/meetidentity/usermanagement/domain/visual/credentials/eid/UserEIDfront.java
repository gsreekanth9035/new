package com.meetidentity.usermanagement.domain.visual.credentials.eid;

import java.io.Serializable;

public class UserEIDfront implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	 private String Zone0FS_Background;
	 private String Zone5FS_EN;
	 private String Zone9FS_Header;
	 private String Zone10FS_Department;
	 private String Zone11FS_OrgSeal;
	 private String Zone13FS_Issued;
	 private String Zone14FS_Expires;
	 private String Zone1FV_Photograph; 
	 private String Zone2FV_Name;
	 private String Zone5FV_EN;
	 private String Zone6FV_Qrcode;
	 private String Zone8FV_Affiliation;
	 private String Zone10FV_Department;
	 private String Zone13FV_Issued;
	 private String Zone14FV_Expires;
	public String getZone0FS_Background() {
		return Zone0FS_Background;
	}
	public void setZone0FS_Background(String zone0fs_Background) {
		Zone0FS_Background = zone0fs_Background;
	}
	public String getZone5FS_EN() {
		return Zone5FS_EN;
	}
	public void setZone5FS_EN(String zone5fs_EN) {
		Zone5FS_EN = zone5fs_EN;
	}
	public String getZone9FS_Header() {
		return Zone9FS_Header;
	}
	public void setZone9FS_Header(String zone9fs_Header) {
		Zone9FS_Header = zone9fs_Header;
	}
	public String getZone10FS_Department() {
		return Zone10FS_Department;
	}
	public void setZone10FS_Department(String zone10fs_Department) {
		Zone10FS_Department = zone10fs_Department;
	}
	public String getZone11FS_OrgSeal() {
		return Zone11FS_OrgSeal;
	}
	public void setZone11FS_OrgSeal(String zone11fs_OrgSeal) {
		Zone11FS_OrgSeal = zone11fs_OrgSeal;
	}
	public String getZone13FS_Issued() {
		return Zone13FS_Issued;
	}
	public void setZone13FS_Issued(String zone13fs_Issued) {
		Zone13FS_Issued = zone13fs_Issued;
	}
	public String getZone14FS_Expires() {
		return Zone14FS_Expires;
	}
	public void setZone14FS_Expires(String zone14fs_Expires) {
		Zone14FS_Expires = zone14fs_Expires;
	}
	public String getZone1FV_Photograph() {
		return Zone1FV_Photograph;
	}
	public void setZone1FV_Photograph(String zone1fv_Photograph) {
		Zone1FV_Photograph = zone1fv_Photograph;
	}
	public String getZone2FV_Name() {
		return Zone2FV_Name;
	}
	public void setZone2FV_Name(String zone2fv_Name) {
		Zone2FV_Name = zone2fv_Name;
	}
	public String getZone5FV_EN() {
		return Zone5FV_EN;
	}
	public void setZone5FV_EN(String zone5fv_EN) {
		Zone5FV_EN = zone5fv_EN;
	}
	public String getZone6FV_Qrcode() {
		return Zone6FV_Qrcode;
	}
	public void setZone6FV_Qrcode(String zone6fv_Qrcode) {
		Zone6FV_Qrcode = zone6fv_Qrcode;
	}
	public String getZone8FV_Affiliation() {
		return Zone8FV_Affiliation;
	}
	public void setZone8FV_Affiliation(String zone8fv_Affiliation) {
		Zone8FV_Affiliation = zone8fv_Affiliation;
	}
	public String getZone10FV_Department() {
		return Zone10FV_Department;
	}
	public void setZone10FV_Department(String zone10fv_Department) {
		Zone10FV_Department = zone10fv_Department;
	}
	public String getZone13FV_Issued() {
		return Zone13FV_Issued;
	}
	public void setZone13FV_Issued(String zone13fv_Issued) {
		Zone13FV_Issued = zone13fv_Issued;
	}
	public String getZone14FV_Expires() {
		return Zone14FV_Expires;
	}
	public void setZone14FV_Expires(String zone14fv_Expires) {
		Zone14FV_Expires = zone14fv_Expires;
	}
	 
	 
	
}
