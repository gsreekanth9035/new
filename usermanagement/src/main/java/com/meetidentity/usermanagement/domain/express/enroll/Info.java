
package com.meetidentity.usermanagement.domain.express.enroll;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "$type",
    "SampleID",
    "DocumentType",
    "ClassType",
    "ClassCode",
    "ClassName",
    "IsGenericType",
    "Issue",
    "IssuerCode",
    "IssuerName",
    "DocumentNumber",
    "KeesingCode",
    "Name",
    "TypeOfIssue",
    "ReaderInfo",
    "Result",
    "HeightMM",
    "WidthMM",
    "Source",
    "StandardSize",
    "Timestamp"
})
public class Info {

    @JsonProperty("$type")
    private String $type;
    @JsonProperty("SampleID")
    private String sampleID;
    @JsonProperty("DocumentType")
    private String documentType;
    @JsonProperty("ClassType")
    private String classType;
    @JsonProperty("ClassCode")
    private String classCode;
    @JsonProperty("ClassName")
    private String className;
    @JsonProperty("IsGenericType")
    private Boolean isGenericType;
    @JsonProperty("Issue")
    private String issue;
    @JsonProperty("IssuerCode")
    private String issuerCode;
    @JsonProperty("IssuerName")
    private String issuerName;
    @JsonProperty("DocumentNumber")
    private String documentNumber;
    @JsonProperty("KeesingCode")
    private String keesingCode;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("TypeOfIssue")
    private String typeOfIssue;
    @JsonProperty("ReaderInfo")
    private ReaderInfo readerInfo;
    @JsonProperty("Result")
    private String result;
    @JsonProperty("HeightMM")
    private Double heightMM;
    @JsonProperty("WidthMM")
    private Double widthMM;
    @JsonProperty("Source")
    private String source;
    @JsonProperty("StandardSize")
    private String standardSize;
    @JsonProperty("Timestamp")
    private String timestamp;
    @JsonIgnore
    private Map<String, String> additionalProperties = new HashMap<String, String>();

    @JsonProperty("$type")
    public String get$type() {
        return $type;
    }

    @JsonProperty("$type")
    public void set$type(String $type) {
        this.$type = $type;
    }

    @JsonProperty("SampleID")
    public String getSampleID() {
        return sampleID;
    }

    @JsonProperty("SampleID")
    public void setSampleID(String sampleID) {
        this.sampleID = sampleID;
    }

    @JsonProperty("DocumentType")
    public String getDocumentType() {
        return documentType;
    }

    @JsonProperty("DocumentType")
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    @JsonProperty("ClassType")
    public String getClassType() {
        return classType;
    }

    @JsonProperty("ClassType")
    public void setClassType(String classType) {
        this.classType = classType;
    }

    @JsonProperty("ClassCode")
    public String getClassCode() {
        return classCode;
    }

    @JsonProperty("ClassCode")
    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    @JsonProperty("ClassName")
    public String getClassName() {
        return className;
    }

    @JsonProperty("ClassName")
    public void setClassName(String className) {
        this.className = className;
    }

    @JsonProperty("IsGenericType")
    public Boolean getIsGenericType() {
        return isGenericType;
    }

    @JsonProperty("IsGenericType")
    public void setIsGenericType(Boolean isGenericType) {
        this.isGenericType = isGenericType;
    }

    @JsonProperty("Issue")
    public String getIssue() {
        return issue;
    }

    @JsonProperty("Issue")
    public void setIssue(String issue) {
        this.issue = issue;
    }

    @JsonProperty("IssuerCode")
    public String getIssuerCode() {
        return issuerCode;
    }

    @JsonProperty("IssuerCode")
    public void setIssuerCode(String issuerCode) {
        this.issuerCode = issuerCode;
    }

    @JsonProperty("IssuerName")
    public String getIssuerName() {
        return issuerName;
    }

    @JsonProperty("IssuerName")
    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    @JsonProperty("DocumentNumber")
    public String getDocumentNumber() {
        return documentNumber;
    }

    @JsonProperty("DocumentNumber")
    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    @JsonProperty("KeesingCode")
    public String getKeesingCode() {
        return keesingCode;
    }

    @JsonProperty("KeesingCode")
    public void setKeesingCode(String keesingCode) {
        this.keesingCode = keesingCode;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("TypeOfIssue")
    public String getTypeOfIssue() {
        return typeOfIssue;
    }

    @JsonProperty("TypeOfIssue")
    public void setTypeOfIssue(String typeOfIssue) {
        this.typeOfIssue = typeOfIssue;
    }

    @JsonProperty("ReaderInfo")
    public ReaderInfo getReaderInfo() {
        return readerInfo;
    }

    @JsonProperty("ReaderInfo")
    public void setReaderInfo(ReaderInfo readerInfo) {
        this.readerInfo = readerInfo;
    }

    @JsonProperty("Result")
    public String getResult() {
        return result;
    }

    @JsonProperty("Result")
    public void setResult(String result) {
        this.result = result;
    }

    @JsonProperty("HeightMM")
    public Double getHeightMM() {
        return heightMM;
    }

    @JsonProperty("HeightMM")
    public void setHeightMM(Double heightMM) {
        this.heightMM = heightMM;
    }

    @JsonProperty("WidthMM")
    public Double getWidthMM() {
        return widthMM;
    }

    @JsonProperty("WidthMM")
    public void setWidthMM(Double widthMM) {
        this.widthMM = widthMM;
    }

    @JsonProperty("Source")
    public String getSource() {
        return source;
    }

    @JsonProperty("Source")
    public void setSource(String source) {
        this.source = source;
    }

    @JsonProperty("StandardSize")
    public String getStandardSize() {
        return standardSize;
    }

    @JsonProperty("StandardSize")
    public void setStandardSize(String standardSize) {
        this.standardSize = standardSize;
    }

    @JsonProperty("Timestamp")
    public String getTimestamp() {
        return timestamp;
    }

    @JsonProperty("Timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, String> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, String value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append($type).append(sampleID).append(documentType).append(classType).append(classCode).append(className).append(isGenericType).append(issue).append(issuerCode).append(issuerName).append(documentNumber).append(keesingCode).append(name).append(typeOfIssue).append(readerInfo).append(result).append(heightMM).append(widthMM).append(source).append(standardSize).append(timestamp).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Info) == false) {
            return false;
        }
        Info rhs = ((Info) other);
        return new EqualsBuilder().append($type, rhs.$type).append(sampleID, rhs.sampleID).append(documentType, rhs.documentType).append(classType, rhs.classType).append(classCode, rhs.classCode).append(className, rhs.className).append(isGenericType, rhs.isGenericType).append(issue, rhs.issue).append(issuerCode, rhs.issuerCode).append(issuerName, rhs.issuerName).append(documentNumber, rhs.documentNumber).append(keesingCode, rhs.keesingCode).append(name, rhs.name).append(typeOfIssue, rhs.typeOfIssue).append(readerInfo, rhs.readerInfo).append(result, rhs.result).append(heightMM, rhs.heightMM).append(widthMM, rhs.widthMM).append(source, rhs.source).append(standardSize, rhs.standardSize).append(timestamp, rhs.timestamp).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
