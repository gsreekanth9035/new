package com.meetidentity.usermanagement.domain.visual.credentials.pzid;

import java.io.Serializable;

public class UserPZid implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public UserPZIDfront userPZIDfront;
	public UserPZIDback userPZIDback;
	public UserPZIDfront getUserEIDfront() {
		return userPZIDfront;
	}
	public void setUserEIDfront(UserPZIDfront userEIDfront) {
		this.userPZIDfront = userEIDfront;
	}
	public UserPZIDback getUserEIDback() {
		return userPZIDback;
	}
	public void setUserEIDback(UserPZIDback userEIDback) {
		this.userPZIDback = userEIDback;
	}
	
	

}
