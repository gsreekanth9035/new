package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="wf_step_def")
public class WFStepDefEntity  extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
    @Column(name="name")
	private String name;
    
    @Column(name="description")
	private String description;
    
    @Column(name="display_name")
	private String displayName;
    
    @Column(name="active")
    private Boolean active;
    
    @Column(name="step_def_order")
    private int stepDefOrder;
    
    @Column(name="ref_ui_page")
	private String refUIPage;
    
    @Column(name="ref_table_name")
	private String refTableName;

    @Version
    @Column(name="version")
    private int version;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public int getStepDefOrder() {
		return stepDefOrder;
	}

	public void setStepDefOrder(int stepDefOrder) {
		this.stepDefOrder = stepDefOrder;
	}

	public String getRefUIPage() {
		return refUIPage;
	}

	public void setRefUIPage(String refUIPage) {
		this.refUIPage = refUIPage;
	}

	public String getRefTableName() {
		return refTableName;
	}

	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
   
}
