package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "organization_advertisements")
public class OrganizationAdvertisementEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "uims_id")
	private String uimsId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
	private OrganizationEntity organizationEntity;

	@Column(name = "advertisement_name")
	private String advertisementName;

	@Lob
	@Column(name = "advertisement_image")
	private byte[] advertisementImage;

	@Column(name = "advertisement_text")
	private String advertisementText;

	@Column(name = "advertisement_hyper_link")
	private String advertisementHyperLink;

	@Column(name = "advertisement_condition")
	private String advertisementCondition;

	@Column(name = "status")
	private boolean status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public OrganizationEntity getOrganizationEntity() {
		return organizationEntity;
	}

	public void setOrganizationEntity(OrganizationEntity organizationEntity) {
		this.organizationEntity = organizationEntity;
	}

	public String getAdvertisementName() {
		return advertisementName;
	}

	public void setAdvertisementName(String advertisementName) {
		this.advertisementName = advertisementName;
	}

	public byte[] getAdvertisementImage() {
		return advertisementImage;
	}

	public void setAdvertisementImage(byte[] advertisementImage) {
		this.advertisementImage = advertisementImage;
	}

	public String getAdvertisementText() {
		return advertisementText;
	}

	public void setAdvertisementText(String advertisementText) {
		this.advertisementText = advertisementText;
	}

	public String getAdvertisementHyperLink() {
		return advertisementHyperLink;
	}

	public void setAdvertisementHyperLink(String advertisementHyperLink) {
		this.advertisementHyperLink = advertisementHyperLink;
	}

	public String getAdvertisementCondition() {
		return advertisementCondition;
	}

	public void setAdvertisementCondition(String advertisementCondition) {
		this.advertisementCondition = advertisementCondition;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
