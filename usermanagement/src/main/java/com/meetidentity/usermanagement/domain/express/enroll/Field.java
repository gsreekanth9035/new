
package com.meetidentity.usermanagement.domain.express.enroll;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "$type",
    "Name",
    "Value",
    "ValueEncoding",
    "DisplayName",
    "Description",
    "Source",
    "Reliability"
})
public class Field {

    @JsonProperty("$type")
    private String $type;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Value")
    private String value;
    @JsonProperty("ValueEncoding")
    private String valueEncoding;
    @JsonProperty("DisplayName")
    private String displayName;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("Source")
    private String source;
    @JsonProperty("Reliability")
    private Double reliability;
    @JsonIgnore
    private Map<String, String> additionalProperties = new HashMap<String, String>();

    @JsonProperty("$type")
    public String get$type() {
        return $type;
    }

    @JsonProperty("$type")
    public void set$type(String $type) {
        this.$type = $type;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("Value")
    public String getValue() {
        return value;
    }

    @JsonProperty("Value")
    public void setValue(String value) {
        this.value = value;
    }

    @JsonProperty("ValueEncoding")
    public String getValueEncoding() {
        return valueEncoding;
    }

    @JsonProperty("ValueEncoding")
    public void setValueEncoding(String valueEncoding) {
        this.valueEncoding = valueEncoding;
    }

    @JsonProperty("DisplayName")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("DisplayName")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonProperty("Description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("Description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("Source")
    public String getSource() {
        return source;
    }

    @JsonProperty("Source")
    public void setSource(String source) {
        this.source = source;
    }

    @JsonProperty("Reliability")
    public Double getReliability() {
        return reliability;
    }

    @JsonProperty("Reliability")
    public void setReliability(Double reliability) {
        this.reliability = reliability;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, String> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, String value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append($type).append(name).append(value).append(valueEncoding).append(displayName).append(description).append(source).append(reliability).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Field) == false) {
            return false;
        }
        Field rhs = ((Field) other);
        return new EqualsBuilder().append($type, rhs.$type).append(name, rhs.name).append(value, rhs.value).append(valueEncoding, rhs.valueEncoding).append(displayName, rhs.displayName).append(description, rhs.description).append(source, rhs.source).append(reliability, rhs.reliability).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
