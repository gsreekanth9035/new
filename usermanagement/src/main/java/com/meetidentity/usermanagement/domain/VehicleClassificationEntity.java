package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "vehicle_classification")
public class VehicleClassificationEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "type")
	private String type;

	@Column(name = "description")
	private String description;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
	private OrganizationEntity organization;

	@OneToMany(mappedBy = "vehicleClassification", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<VehicleCategoryEntity> vehicleCategoryEntity = new ArrayList<>();

	@OneToMany(mappedBy = "vehicleClassification", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<UserDrivingLicenseEntity> userDrivingLicenseEntity = new ArrayList<>();

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

	public List<VehicleCategoryEntity> getVehicleCategoryEntity() {
		return vehicleCategoryEntity;
	}

	public void setVehicleCategoryEntity(List<VehicleCategoryEntity> vehicleCategoryEntity) {
		this.vehicleCategoryEntity = vehicleCategoryEntity;
	}

	public List<UserDrivingLicenseEntity> getUserDrivingLicenseEntity() {
		return userDrivingLicenseEntity;
	}

	public void setUserDrivingLicenseEntity(List<UserDrivingLicenseEntity> userDrivingLicenseEntity) {
		this.userDrivingLicenseEntity = userDrivingLicenseEntity;
	}

	

}
