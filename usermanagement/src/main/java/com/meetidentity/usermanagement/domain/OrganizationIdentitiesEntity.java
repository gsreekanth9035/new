package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "org_identities")
public class OrganizationIdentitiesEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "org_id")
	private OrganizationEntity organization;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "identity_type_id")
	private IdentityTypeEntity identityType;

	@Column(name = "is_trusted")
	private Boolean isTrusted;

	@Column(name = "can_be_issued")
	private Boolean canBeIssued;

	@Column(name = "default_identity")
	private Boolean defaultIdentity;
	
	@Column(name = "issued_identity_expiry")
	private Long issuedIdentityExpiry;

	@OneToMany(mappedBy = "organizationIdentity", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<IDProofIssuingAuthorityEntity> idProofIssuingAuthorities = new ArrayList<>();
	
	@Version
	@Column(name = "version")
	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

	public IdentityTypeEntity getIdentityType() {
		return identityType;
	}

	public void setIdentityType(IdentityTypeEntity identityType) {
		this.identityType = identityType;
	}

	public Boolean getIsTrusted() {
		return isTrusted;
	}

	public void setIsTrusted(Boolean isTrusted) {
		this.isTrusted = isTrusted;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Boolean getCanBeIssued() {
		return canBeIssued;
	}

	public void setCanBeIssued(Boolean canBeIssued) {
		this.canBeIssued = canBeIssued;
	}

	public Boolean getDefaultIdentity() {
		return defaultIdentity;
	}

	public void setDefaultIdentity(Boolean defaultIdentity) {
		this.defaultIdentity = defaultIdentity;
	}

	public Long getIssuedIdentityExpiry() {
		return issuedIdentityExpiry;
	}

	public void setIssuedIdentityExpiry(Long issuedIdentityExpiry) {
		this.issuedIdentityExpiry = issuedIdentityExpiry;
	}

	public List<IDProofIssuingAuthorityEntity> getIdProofIssuingAuthorities() {
		return idProofIssuingAuthorities;
	}

	public void setIdProofIssuingAuthorities(List<IDProofIssuingAuthorityEntity> idProofIssuingAuthorities) {
		this.idProofIssuingAuthorities = idProofIssuingAuthorities;
	}

}
