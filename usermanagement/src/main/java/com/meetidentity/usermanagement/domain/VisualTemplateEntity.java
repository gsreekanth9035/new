package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "visual_template")
public class VisualTemplateEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "org_identity_type_id")
	private OrganizationIdentitiesEntity orgIdentityType;

	@Lob
	@Column(name = "front_design")
	private byte[] frontDesign;

	@Lob
	@Column(name = "back_design")
	private byte[] backDesign;

	@Column(name = "front_orientation")
	private String frontOrientation;

	@Column(name = "back_orientation")
	private String backOrientation;

	@Lob
	@Column(name = "attribute_mapping")
	private byte[] attributeMapping;

	@Version
	@Column(name = "version")
	private int version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
	private OrganizationEntity organization;

	@OneToMany(mappedBy = "visualTemplate", cascade = CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<VisualTemplateDeviceTypeMappingEntity> visualTemplateDevices = new ArrayList<>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public OrganizationIdentitiesEntity getOrgIdentityType() {
		return orgIdentityType;
	}

	public void setOrgIdentityType(OrganizationIdentitiesEntity orgIdentityType) {
		this.orgIdentityType = orgIdentityType;
	}

	public byte[] getFrontDesign() {
		return frontDesign;
	}

	public void setFrontDesign(byte[] frontDesign) {
		this.frontDesign = frontDesign;
	}

	public byte[] getBackDesign() {
		return backDesign;
	}

	public void setBackDesign(byte[] backDesign) {
		this.backDesign = backDesign;
	}

	public String getFrontOrientation() {
		return frontOrientation;
	}

	public void setFrontOrientation(String frontOrientation) {
		this.frontOrientation = frontOrientation;
	}

	public String getBackOrientation() {
		return backOrientation;
	}

	public void setBackOrientation(String backOrientation) {
		this.backOrientation = backOrientation;
	}

	public byte[] getAttributeMapping() {
		return attributeMapping;
	}

	public void setAttributeMapping(byte[] attributeMapping) {
		this.attributeMapping = attributeMapping;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

	public List<VisualTemplateDeviceTypeMappingEntity> getVisualTemplateDevices() {
		return visualTemplateDevices;
	}

	public void setVisualTemplateDevices(List<VisualTemplateDeviceTypeMappingEntity> visualTemplateDevices) {
		this.visualTemplateDevices = visualTemplateDevices;
	}

	public void addVTDeviceTypeMapping(VisualTemplateDeviceTypeMappingEntity visualTemplateDevice) {
		visualTemplateDevice.setVisualTemplate(this);
		this.visualTemplateDevices.add(visualTemplateDevice);
	}
	
	public void removeVTDeviceTypeMapping(VisualTemplateDeviceTypeMappingEntity visualTemplateDevice) {
		visualTemplateDevice.setVisualTemplate(null);
		this.visualTemplateDevices.remove(visualTemplateDevice);
	}
}
