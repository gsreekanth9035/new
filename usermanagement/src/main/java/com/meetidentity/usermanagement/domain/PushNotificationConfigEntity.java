package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="push_notification_config")
public class PushNotificationConfigEntity  extends AbstractAuditingEntity {

      
      /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
      @GeneratedValue(strategy = GenerationType.IDENTITY)
      @Column(name="id")
  	private Long id;
      
      @Column(name="type")
  	private String type;
      
      @Column(name="service_provider_name")
  	private String serviceProviderName;
      
      @Column(name="app_name")
  	private String appName;
      
      @Column(name="api_key")
  	private String apiKey;
      
      @Column(name="api_security_key")
  	private String apiSecurityKey;
      
      @Column(name="server_key")
  	private String serverKey;
      
      
     @Column(name="version")
     private int version;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getServiceProviderName() {
		return serviceProviderName;
	}


	public void setServiceProviderName(String serviceProviderName) {
		this.serviceProviderName = serviceProviderName;
	}


	public String getAppName() {
		return appName;
	}


	public void setAppName(String appName) {
		this.appName = appName;
	}


	public String getApiKey() {
		return apiKey;
	}


	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}


	public String getApiSecurityKey() {
		return apiSecurityKey;
	}


	public void setApiSecurityKey(String apiSecurityKey) {
		this.apiSecurityKey = apiSecurityKey;
	}


	public String getServerKey() {
		return serverKey;
	}


	public void setServerKey(String serverKey) {
		this.serverKey = serverKey;
	}


	public int getVersion() {
		return version;
	}


	public void setVersion(int version) {
		this.version = version;
	}
         
       
      
      
      
}
