package com.meetidentity.usermanagement.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="config_type")
public class ConfigTypeEntity  extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
    @Column(name="name")
	private String name;
    
    @Column(name="description")
	private String description;

    @Version
    @Column(name="version")
    private int version;
    
    @OneToMany(mappedBy="configType")
    private List<ConfigEntity> configs = new ArrayList<>();
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<ConfigEntity> getConfigs() {
		return configs;
	}

	public void setConfigs(List<ConfigEntity> configs) {
		this.configs = configs;
	}
	
}
