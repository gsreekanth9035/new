package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="wf_step_registration_config")
public class WFStepRegistrationConfigEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
    
    @ManyToOne
    @JoinColumn(name="wf_step_id")
    private WFStepEntity wfStep;
    
    @ManyToOne
    @JoinColumn(name="org_field_id")
    private OrgIdentityFieldEntity orgIdentityField;
    
    @Column(name="field_label")
	private String fieldLabel;
    
    @Column(name="required")
    private Boolean required;
	
    @Version
    @Column(name="version")
    private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WFStepEntity getWfStep() {
		return wfStep;
	}

	public void setWfStep(WFStepEntity wfStep) {
		this.wfStep = wfStep;
	}

	public OrgIdentityFieldEntity getOrgIdentityField() {
		return orgIdentityField;
	}

	public void setOrgIdentityField(OrgIdentityFieldEntity orgIdentityField) {
		this.orgIdentityField = orgIdentityField;
	}

	public String getFieldLabel() {
		return fieldLabel;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
