package com.meetidentity.usermanagement.domain.visual.credentials.prid;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.constants.XMLConstants;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserAttributeEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserPermanentResidentIdEntity;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.security.mrz.records.MrzTD1;
import com.meetidentity.usermanagement.security.mrz.types.MrzDate;
import com.meetidentity.usermanagement.security.mrz.types.MrzSex;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.service.helper.UserCredentialsServiceHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAddress;
import com.meetidentity.usermanagement.web.rest.model.UserAppearance;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserPermanentResidentID;

@Component
public class UtilityPRID {

	@Autowired
	private Util util = new Util();
	
	@Autowired
	private UserCredentialsServiceHelper userCredentialsServiceHelper;
	
	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;
	
	private final Logger log = LoggerFactory.getLogger(UtilityPRID.class);

	private final Logger logger = LoggerFactory.getLogger(UtilityPRID.class);
	private static String XLINK_NS = XMLConstants.XLINK_NAMESPACE_URI;
	private static String XLINK_HREF = "xlink:href";

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @return
	 * @throws Exception
	 */
	public Document parsePRIDFront(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity,User user, byte[] svgFile,
			byte[] jsonFile, UserPermanentResidentIdEntity userPermanentResidentIdEntity,  String credentialTemplateID, String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			
			if(simplyParse) {
				JSONObject pridJsonObject = new JSONObject(new String(jsonFile));
				JSONObject pridFrontJsonObject = pridJsonObject.getJSONObject("userPRIDFront");
				Iterator<String> keys = pridFrontJsonObject.keys();
				log.debug("parsePRIDFront: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				// UserPRID userPRID = gson.fromJson(new String(jsonFile), UserPRID.class);				
				// Dynamic User Variables
				

				switch (requestFrom) {

					case "web_portal_enroll_summary":
						UserAttribute userAttribute = user.getUserAttribute();
						UserPermanentResidentID userPermanentResidentID = user.getUserPermanentResidentID();
						doc.getElementById("Zone2_02FV_Fatherlastname_Motherlastname").setTextContent(userAttribute.getLastName());
						doc.getElementById("Zone2_01FV_Firstname_MiddleName").setTextContent(userAttribute.getFirstName());
						doc.getElementById("Zone2_03FV_Nationality").setTextContent(userAttribute.getNationality());
						doc.getElementById("Zone2_04FV_BirthPlace").setTextContent(userAttribute.getPlaceOfBirth());						
						doc.getElementById("Zone2_05FV_DocNum").setTextContent(userPermanentResidentIdEntity.getDocumentNumber() == null? "XXXXXXXXX" : userPermanentResidentIdEntity.getDocumentNumber());
						doc.getElementById("Zone2_06FV_IDNum").setTextContent(userPermanentResidentIdEntity.getPersonalCode() == null? "XXXXXXXXX" : userPermanentResidentIdEntity.getPersonalCode());
						doc.getElementById("Zone2_07FV_DOI").setTextContent(userPermanentResidentIdEntity.getDateOfIssuance() == null? "XX/XX/XXXX" : Util.formatDate(userPermanentResidentIdEntity.getDateOfIssuance(), Util.MMDDYYYY_format));
						doc.getElementById("Zone2_08FV_DOE").setTextContent(userPermanentResidentIdEntity.getDateOfExpiry() == null? "XX/XX/XXXX" : Util.formatDate(userPermanentResidentIdEntity.getDateOfExpiry(), Util.MMDDYYYY_format));					
														
						doc.getElementById("Zone2_10FV_Sex").setTextContent(userAttribute.getGender());
						doc.getElementById("Zone3_02FV_DOB").setTextContent(Util.formatDate(userAttribute.getDateOfBirth(),Util.MMDDYYYY_format));
						if(userPermanentResidentID != null) {
							doc.getElementById("Zone2_09FV_Passport").setTextContent(userPermanentResidentID.getPassportNumber());
							doc.getElementById("Zone2_11FV_Residentcard_Type_Number").setTextContent(userPermanentResidentID.getResidentcardTypeNumber());
							doc.getElementById("Zone2_12FV_Residentcard_Type_Code").setTextContent(userPermanentResidentID.getResidentcardTypeCode());
						}
						user.getUserBiometrics().stream().forEach(userBio -> {
							if (userBio.getType().equalsIgnoreCase("FACE")) {
								
								int width = Integer.parseInt(doc.getElementById("Zone3_03FV_Photograph").getAttribute("width"));
								int height = Integer.parseInt(doc.getElementById("Zone3_03FV_Photograph").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
									doc.getElementById("Zone3_03FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);

								} catch (IOException e) {
									doc.getElementById("Zone3_03FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
								
								width = Integer.parseInt(doc.getElementById("Zone2_15FV_Photoghost").getAttribute("width"));
								height = Integer.parseInt(doc.getElementById("Zone2_15FV_Photoghost").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate_photoghost(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);

									doc.getElementById("Zone2_15FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);


								} catch (IOException e) {
									doc.getElementById("Zone2_15FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
							if(userBio.getType().equalsIgnoreCase("SIGNATURE")) {
									doc.getElementById("Zone3_01FV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
							}
						});
						
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":
						UserAttributeEntity userAttributee = userEntity.getUserAttribute();
						doc.getElementById("Zone2_02FV_Fatherlastname_Motherlastname").setTextContent(userAttributee.getLastName());
						doc.getElementById("Zone2_01FV_Firstname_MiddleName").setTextContent(userAttributee.getFirstName());
						doc.getElementById("Zone2_03FV_Nationality").setTextContent(userAttributee.getNationality());
						doc.getElementById("Zone2_04FV_BirthPlace").setTextContent(userAttributee.getPlaceOfBirth());						
						doc.getElementById("Zone2_05FV_DocNum").setTextContent(userPermanentResidentIdEntity.getDocumentNumber() == null? "XXXXXXXXX" : userPermanentResidentIdEntity.getDocumentNumber());
						doc.getElementById("Zone2_06FV_IDNum").setTextContent(userPermanentResidentIdEntity.getPersonalCode() == null? "XXXXXXXXX" : userPermanentResidentIdEntity.getPersonalCode());
						doc.getElementById("Zone2_07FV_DOI").setTextContent(userPermanentResidentIdEntity.getDateOfIssuance() == null? "XX/XX/XXXX" : Util.formatDate(userPermanentResidentIdEntity.getDateOfIssuance(), Util.MMDDYYYY_format));
						doc.getElementById("Zone2_08FV_DOE").setTextContent(userPermanentResidentIdEntity.getDateOfExpiry() == null? "XX/XX/XXXX" : Util.formatDate(userPermanentResidentIdEntity.getDateOfExpiry(), Util.MMDDYYYY_format));					
						doc.getElementById("Zone2_09FV_Passport").setTextContent(userPermanentResidentIdEntity.getPassportNumber());						
						doc.getElementById("Zone2_10FV_Sex").setTextContent(userAttributee.getGender());
						doc.getElementById("Zone3_02FV_DOB").setTextContent(Util.formatDate(userAttributee.getDateOfBirth(),Util.MMDDYYYY_format));
						doc.getElementById("Zone2_11FV_Residentcard_Type_Number").setTextContent(userPermanentResidentIdEntity.getResidentcardTypeNumber());
						doc.getElementById("Zone2_12FV_Residentcard_Type_Code").setTextContent(userPermanentResidentIdEntity.getResidentcardTypeCode());
													
						userEntity.getUserBiometrics().stream().forEach(userBio -> {
							if (userBio.getType().equalsIgnoreCase("FACE")) {
								
								int width = Integer.parseInt(doc.getElementById("Zone3_03FV_Photograph").getAttribute("width"));
								int height = Integer.parseInt(doc.getElementById("Zone3_03FV_Photograph").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
									doc.getElementById("Zone3_03FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);

								} catch (IOException e) {
									doc.getElementById("Zone3_03FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
								
								width = Integer.parseInt(doc.getElementById("Zone2_15FV_Photoghost").getAttribute("width"));
								height = Integer.parseInt(doc.getElementById("Zone2_15FV_Photoghost").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate_photoghost(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);

									doc.getElementById("Zone2_15FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);


								} catch (IOException e) {
									doc.getElementById("Zone2_15FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
							if(userBio.getType().equalsIgnoreCase("SIGNATURE")) {
									doc.getElementById("Zone3_01FV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
							}
						});
						
						break;
					default :
				
				}
								
				logger.debug("parsePRIDFront: Ended Dynamic Variable");
			}
			return doc;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}

	public Document parsePRIDBack(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity,User user, byte[] svgFile,
			byte[] jsonFile, UserPermanentResidentIdEntity userPermanentResidentIdEntity,  String credentialTemplateID,
			String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {

				JSONObject pridJsonObject = new JSONObject(new String(jsonFile));
				JSONObject pridBacktJsonObject = pridJsonObject.getJSONObject("userPRIDBack");
				Iterator<String> keys = pridBacktJsonObject.keys();
				log.debug("parsePRIDBack: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				// UserPRID userPRID = gson.fromJson(new String(jsonFile), UserPRID.class);
				// Dynamic User Variables
				switch (requestFrom) {

					case "web_portal_enroll_summary":
						UserAttribute userAttribute = user.getUserAttribute();
						UserAppearance uerAppearance = user.getUserAppearance();
						UserPermanentResidentID userPermanentResidentID = user.getUserPermanentResidentID();
						UserAddress userAddress = user.getUserAddress();
						doc.getElementById("Zone4_01BV_Phone").setTextContent(userAttribute.getContact());	
						doc.getElementById("Zone4_02BV_Birthdate").setTextContent(Util.formatDate(userAttribute.getDateOfBirth(),Util.MMDDYYYY_format));	
						
						String addressL1 = userCredentialsServiceHelper.getAddressFromAddressObj_addressLine1(userAddress);
						String addressL2 = userCredentialsServiceHelper.getAddressFromAddressObj_addressLine2(userAddress);
						if(addressL1 != null && addressL2 != null) {
							doc.getElementById("Zone4_03BV_CHAddress1").setTextContent(addressL1);	
							doc.getElementById("Zone4_03BV_CHAddress2").setTextContent(addressL2);	
						} else {
							//TODO need to add few more conditional setters in future
							doc.getElementById("Zone4_03BV_CHAddress1").setTextContent("");	
							doc.getElementById("Zone4_03BV_CHAddress2").setTextContent("");	
						}
						doc.getElementById("Zone4_04BV_DocNum").setTextContent(userPermanentResidentIdEntity.getDocumentNumber() == null? "XXXXXXXXX" : userPermanentResidentIdEntity.getDocumentNumber());
						doc.getElementById("Zone4_05BV_IDNum").setTextContent(userPermanentResidentIdEntity.getPersonalCode()== null? "XXXXXXXXX" : userPermanentResidentIdEntity.getPersonalCode());
						
						try {
//							doc.getElementById("Zone6BV_VaccinationName").setTextContent(userPermanentResidentID.getVaccineName());	
//							doc.getElementById("Zone6BV_VaccinationDate").setTextContent(userPermanentResidentID.getVaccinationDate() == null? "XX-XX-XXXX" : Util.formatDate(userPermanentResidentID.getVaccinationDate(), Util.MMDDYYYY_format));	
//							doc.getElementById("Zone6BV_VaccinationLocationName").setTextContent(userPermanentResidentID.getVaccinationLocationName());	
						}catch (Exception e) {
							log.info("Vaccine info is not supported");
						}
						
						 final MrzTD1 r = new MrzTD1();
					        r.code2 = 'I';
					        r.issuingCountry = userAttribute.getNationality().toUpperCase();
					        r.nationality = userAttribute.getNationality().toUpperCase();
					        r.optional = "";   r.optional2 = "";
					        Calendar cal_dob = Calendar.getInstance();
					        cal_dob.setTime(userAttribute.getDateOfBirth());
					        Calendar cal_exp = Calendar.getInstance();
					        
							r.documentNumber = "XXXXXXXX";
							cal_exp.setTime(new Date());
							
							r.expirationDate = new MrzDate(cal_exp.get(Calendar.YEAR)%100, cal_exp.get(Calendar.MONTH)+1,cal_exp.get(Calendar.DAY_OF_MONTH) );
					        r.dateOfBirth = new MrzDate(cal_dob.get(Calendar.YEAR)%100, cal_dob.get(Calendar.MONTH)+1,cal_dob.get(Calendar.DAY_OF_MONTH) );
					        if(userAttribute.getGender().toLowerCase().contains("m")) {
					        	r.sex = MrzSex.Male;
					        }else 
					        if(userAttribute.getGender().toLowerCase().contains("f")) {
					        	r.sex = MrzSex.Female;
					        }else {
					        	r.sex = MrzSex.Male;
					        }
					        r.surname = userAttribute.getLastName();
					        r.givenNames = userAttribute.getFirstName();
					        doc.getElementById("Zone5_MRZ_TD1_L1").setTextContent(r.toMrz_line1());
					        doc.getElementById("Zone5_MRZ_TD1_L2").setTextContent(r.toMrz_line2());
					        doc.getElementById("Zone5_MRZ_TD1_L3").setTextContent(r.toMrz_line3());
					        doc.getElementById("Zone4_06BV_QR")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + user.getId() + "#" + credentialTemplateID + "#" + "XXXXXXXXXX",
											BarcodeFormat.QR_CODE, 300, 300))));
						
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":
						UserAttributeEntity userAttributee = userEntity.getUserAttribute();
						UserPermanentResidentIdEntity userPermanentResidentIdEntityy = userEntity.getUserPermanentResidentId();
						
						doc.getElementById("Zone4_01BV_Phone").setTextContent(userAttributee.getContact());	
						doc.getElementById("Zone4_02BV_Birthdate").setTextContent(Util.formatDate(userAttributee.getDateOfBirth(),Util.MMDDYYYY_format));	
						
						String addressL1_ = userCredentialsServiceHelper.getAddressFromAddressEntity_addressLine1(userEntity.getUserAddress());
						String addressL2_ = userCredentialsServiceHelper.getAddressFromAddressEntity_addressLine2(userEntity.getUserAddress());
						if(addressL1_ != null && addressL2_ != null) {
							doc.getElementById("Zone4_03BV_CHAddress1").setTextContent(addressL1_);	
							doc.getElementById("Zone4_03BV_CHAddress2").setTextContent(addressL2_);	
						} else {
							//TODO need to add few more conditional setters in future
							doc.getElementById("Zone4_03BV_CHAddress1").setTextContent("");	
							doc.getElementById("Zone4_03BV_CHAddress2").setTextContent("");	
						}
						doc.getElementById("Zone4_04BV_DocNum").setTextContent(userPermanentResidentIdEntity.getDocumentNumber() == null? "XXXXXXXXX" : userPermanentResidentIdEntity.getDocumentNumber());
						doc.getElementById("Zone4_05BV_IDNum").setTextContent(userPermanentResidentIdEntity.getPersonalCode()== null? "XXXXXXXXX" : userPermanentResidentIdEntity.getPersonalCode());
						
						try {
//							doc.getElementById("Zone6BV_VaccinationName").setTextContent(userPermanentResidentIdEntityy.getVaccineName());	
//							doc.getElementById("Zone6BV_VaccinationDate").setTextContent(userPermanentResidentIdEntityy.getVaccinationDate() == null? "XX-XX-XXXX" : Util.formatDate(userPermanentResidentIdEntityy.getVaccinationDate(), Util.MMDDYYYY_format));	
//							doc.getElementById("Zone6BV_VaccinationLocationName").setTextContent(userPermanentResidentIdEntityy.getVaccinationLocationName());	
						}catch (Exception e) {
							log.info("Vaccine info is not supported");
						}
						
						final MrzTD1 rr = new MrzTD1();
				        rr.code2 = 'I';
				        rr.issuingCountry = userAttributee.getNationality().toUpperCase();
				        rr.nationality = userAttributee.getNationality().toUpperCase();
				        rr.optional = "";   rr.optional2 = "";
				        Calendar cal_dobb = Calendar.getInstance();
				        cal_dobb.setTime(Date.from(userAttributee.getDateOfBirth()));
				        Calendar cal_expp = Calendar.getInstance();
				        
						rr.documentNumber = userPermanentResidentIdEntity.getDocumentNumber();
						cal_expp.setTime(userPermanentResidentIdEntityy.getDateOfExpiry() == null? new Date() : Date.from(userPermanentResidentIdEntityy.getDateOfExpiry()));
						
						rr.expirationDate = new MrzDate(cal_expp.get(Calendar.YEAR)%100, cal_expp.get(Calendar.MONTH)+1,cal_expp.get(Calendar.DAY_OF_MONTH) );
				        rr.dateOfBirth = new MrzDate(cal_dobb.get(Calendar.YEAR)%100, cal_dobb.get(Calendar.MONTH)+1,cal_dobb.get(Calendar.DAY_OF_MONTH) );
				        if(userAttributee.getGender().toLowerCase().contains("m")) {
				        	rr.sex = MrzSex.Male;
				        }else 
				        if(userAttributee.getGender().toLowerCase().contains("f")) {
				        	rr.sex = MrzSex.Female;
				        }else {
				        	rr.sex = MrzSex.Male;
				        }
				        rr.surname = userAttributee.getLastName();
				        rr.givenNames = userAttributee.getFirstName();
				        doc.getElementById("Zone5_MRZ_TD1_L1").setTextContent(rr.toMrz_line1());
				        doc.getElementById("Zone5_MRZ_TD1_L2").setTextContent(rr.toMrz_line2());
				        doc.getElementById("Zone5_MRZ_TD1_L3").setTextContent(rr.toMrz_line3());
				        String idNumber = "XXXXXXXXXXXX";
						if(userPermanentResidentIdEntityy.getPersonalCode()!= null) {
							idNumber = userPermanentResidentIdEntityy.getPersonalCode();
						}
												
				        doc.getElementById("Zone4_06BV_QR")
						.setAttributeNS(XLINK_NS, XLINK_HREF,
								"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
										organizationEntity.getName() + "#" + userEntity.getId() + "#" + credentialTemplateID + "#" + idNumber,
										BarcodeFormat.QR_CODE, 300, 300))));
					
						break;
					default :
				
				}
				
//				doc.getElementById("background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + userPRID.getUserPRIDBack().getBackground());
							
				logger.debug("parsePRIDBack: Ended dynamic Variable");
				
				return doc;
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}
	
}
