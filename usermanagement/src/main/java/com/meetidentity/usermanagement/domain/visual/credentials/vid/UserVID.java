package com.meetidentity.usermanagement.domain.visual.credentials.vid;

import java.io.Serializable;

public class UserVID implements Serializable {

	private static final long serialVersionUID = 1L;

	private UserVIDBack userVIDBack;

	private UserVIDFront userVIDFront;

	public UserVIDBack getUserVIDBack() {
		return userVIDBack;
	}

	public void setUserVIDBack(UserVIDBack userVIDBack) {
		this.userVIDBack = userVIDBack;
	}

	public UserVIDFront getUserVIDFront() {
		return userVIDFront;
	}

	public void setUserVIDFront(UserVIDFront userVIDFront) {
		this.userVIDFront = userVIDFront;
	}


}
