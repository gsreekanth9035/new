package com.meetidentity.usermanagement.domain.visual.credentials.pzid;

import java.io.Serializable;

public class UserPZIDfront implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String ZoneXFV_Given_Last_Name;
	private String ZoneXFV_Id_Number;
	private String ZoneXFV_YY;
	private String ZoneXFV_Time;
	private String ZoneXFV_QRCode;

	public String getZoneXFV_Given_Last_Name() {
		return ZoneXFV_Given_Last_Name;
	}

	public void setZoneXFV_Given_Last_Name(String zoneXFV_Given_Last_Name) {
		ZoneXFV_Given_Last_Name = zoneXFV_Given_Last_Name;
	}

	public String getZoneXFV_Id_Number() {
		return ZoneXFV_Id_Number;
	}

	public void setZoneXFV_Id_Number(String zoneXFV_Id_Number) {
		ZoneXFV_Id_Number = zoneXFV_Id_Number;
	}

	public String getZoneXFV_YY() {
		return ZoneXFV_YY;
	}

	public void setZoneXFV_YY(String zoneXFV_YY) {
		ZoneXFV_YY = zoneXFV_YY;
	}

	public String getZoneXFV_Time() {
		return ZoneXFV_Time;
	}

	public void setZoneXFV_Time(String zoneXFV_Time) {
		ZoneXFV_Time = zoneXFV_Time;
	}

	public String getZoneXFV_QRCode() {
		return ZoneXFV_QRCode;
	}

	public void setZoneXFV_QRCode(String zoneXFV_QRCode) {
		ZoneXFV_QRCode = zoneXFV_QRCode;
	}

}
