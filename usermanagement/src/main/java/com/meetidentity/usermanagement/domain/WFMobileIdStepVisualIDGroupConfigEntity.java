package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "wf_mobile_id_step_visual_id_group_config")
public class WFMobileIdStepVisualIDGroupConfigEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "reference_group_id")
	private String referenceGroupId;

	@Column(name = "reference_group_name")
	private String referenceGroupName;

	@ManyToOne
	@JoinColumn(name = "visual_template_id")
	private VisualTemplateEntity visualTemplate;

	@Version
	@Column(name = "version")
	private int version;

	@ManyToOne
	@JoinColumn(name = "wf_mobile_identity_issuance_id")
	private WFMobileIDIdentityIssuanceEntity wfMobileIDIdentitiesIssuance;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReferenceGroupId() {
		return referenceGroupId;
	}

	public void setReferenceGroupId(String referenceGroupId) {
		this.referenceGroupId = referenceGroupId;
	}

	public String getReferenceGroupName() {
		return referenceGroupName;
	}

	public void setReferenceGroupName(String referenceGroupName) {
		this.referenceGroupName = referenceGroupName;
	}

	public VisualTemplateEntity getVisualTemplate() {
		return visualTemplate;
	}

	public void setVisualTemplate(VisualTemplateEntity visualTemplate) {
		this.visualTemplate = visualTemplate;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public WFMobileIDIdentityIssuanceEntity getWfMobileIDIdentitiesIssuance() {
		return wfMobileIDIdentitiesIssuance;
	}

	public void setWfMobileIDIdentitiesIssuance(WFMobileIDIdentityIssuanceEntity wfMobileIDIdentitiesIssuance) {
		this.wfMobileIDIdentitiesIssuance = wfMobileIDIdentitiesIssuance;
	}

}
