package com.meetidentity.usermanagement.domain;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="vehicle")
public class VehicleEntity  extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
	
	@Column(name = "uims_id", nullable = false)
	private String uimsId;
    
    @Column(name="vin")
	private String vin;
    
    @Column(name="make")
	private String make;
    
    @Column(name="year")
    private int year;    
    
    @Column(name="model")
	private String model;

    @Column(name="color")
	private String color;    
    
    @Column(name="license_plate_number")
	private String licensePlateNumber;    

    @Column(name="capacity")
    private int capacity;    
    
    @Column(name = "date_of_sale")
    private Instant dateOfSale;    

    @Column(name="dealer_name")
	private String dealerName;    
    
    @Column(name="dealer_license_number")
	private String dealerLicenseNumber;    
    
    @Column(name="dealer_invoice_number")
	private String dealerInvoiceNumber;    

    @Version
    @Column(name="version")
    private int version;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="organization_id")
    private OrganizationEntity organization;    
    
    @OneToMany(mappedBy="vehicle", cascade=CascadeType.PERSIST, orphanRemoval=true, fetch=FetchType.LAZY)
    private List<VehicleInsuranceEntity> vehicleInsurances = new ArrayList<>();    

    @OneToMany(mappedBy="vehicle", cascade=CascadeType.PERSIST, orphanRemoval=true, fetch=FetchType.LAZY)
    private List<VehicleOwnerEntity> vehicleOwners = new ArrayList<>();    

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getLicensePlateNumber() {
		return licensePlateNumber;
	}

	public void setLicensePlateNumber(String licensePlateNumber) {
		this.licensePlateNumber = licensePlateNumber;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public Instant getDateOfSale() {
		return dateOfSale;
	}

	public void setDateOfSale(Instant dateOfSale) {
		this.dateOfSale = dateOfSale;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getDealerLicenseNumber() {
		return dealerLicenseNumber;
	}

	public void setDealerLicenseNumber(String dealerLicenseNumber) {
		this.dealerLicenseNumber = dealerLicenseNumber;
	}

	public String getDealerInvoiceNumber() {
		return dealerInvoiceNumber;
	}

	public void setDealerInvoiceNumber(String dealerInvoiceNumber) {
		this.dealerInvoiceNumber = dealerInvoiceNumber;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

	public List<VehicleInsuranceEntity> getVehicleInsurances() {
		return vehicleInsurances;
	}

	public void setVehicleInsurances(List<VehicleInsuranceEntity> vehicleInsurances) {
		this.vehicleInsurances = vehicleInsurances;
	}

	public List<VehicleOwnerEntity> getVehicleOwners() {
		return vehicleOwners;
	}

	public void setVehicleOwners(List<VehicleOwnerEntity> vehicleOwners) {
		this.vehicleOwners = vehicleOwners;
	}
	
	public void addVehicleInsurance(VehicleInsuranceEntity vehicleInsuranceEntity) {
		vehicleInsuranceEntity.setVehicle(this);
		this.vehicleInsurances.add(vehicleInsuranceEntity);
	}
	
	public void removeVehicleInsurance(VehicleInsuranceEntity vehicleInsuranceEntity) {
		vehicleInsuranceEntity.setVehicle(null);
		this.vehicleInsurances.remove(vehicleInsuranceEntity);
	}	

	public void addVehicleOwner(VehicleOwnerEntity vehicleOwnerEntity) {
		vehicleOwnerEntity.setVehicle(this);
		this.vehicleOwners.add(vehicleOwnerEntity);
	}
	
	public void removeVehicleOwner(VehicleOwnerEntity vehicleOwnerEntity) {
		vehicleOwnerEntity.setVehicle(null);
		this.vehicleOwners.remove(vehicleOwnerEntity);
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}	
	
	
   
}
