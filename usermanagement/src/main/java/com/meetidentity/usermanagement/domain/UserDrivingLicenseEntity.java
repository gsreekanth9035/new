package com.meetidentity.usermanagement.domain;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.meetidentity.usermanagement.annotations.SequenceValue;

@Entity
@Table(name = "user_drivinglicense")
public class UserDrivingLicenseEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;

	@Column(name = "uims_id")
	private String uimsId;

	@SequenceValue
	@Column(name = "license_number")
	private String licenseNumber;

	@Column(name = "document_descriminator")
	private String documentDescriminator;

	@Column(name = "issuing_authority")
	private String issuingAuthority;

	@Column(name = "license_category")
	private String licenseCategory;
	
	@Column(name = "date_of_issuance")
	private Instant dateOfIssuance;
	
	@Column(name = "date_of_expiry")
	private Instant dateOfExpiry;
	
	@Column(name = "vehicle_class")
	private String vehicleClass;

	@OneToOne
	@JoinColumn(name = "restriction_id")
	private DrivingLicenseRestrictionEntity restrictions;
	
	@Column(name = "endorsements")
	private String endorsements;
	
	@ManyToOne
	@JoinColumn(name = "vehicle_classification_id")
	private VehicleClassificationEntity vehicleClassification;
	
	@Column(name = "under_18_until")
	private Instant under18Until;
	
	@Column(name = "under_19_until")
	private Instant under19Until;
	
	@Column(name = "under_21_until")
	private Instant under21Until;
	
	@Column(name = "audit_information")
	private String auditInformation;
	
	
	public UserDrivingLicenseEntity(UserEntity user, String uimsId, String licenseNumber, String documentDescriminator,
			String issuingAuthority, String licenseCategory, Instant dateOfIssuance, Instant dateOfExpiry, String vehicleClass,
			DrivingLicenseRestrictionEntity restrictions, String endorsements, VehicleClassificationEntity vehicleClassification, Instant under18Until,
			Instant under19Until, Instant under21Until, String auditInformation) {
		super();
		this.user = user;
		this.uimsId = uimsId;
		this.licenseNumber = licenseNumber;
		this.documentDescriminator = documentDescriminator;
		this.issuingAuthority = issuingAuthority;
		this.licenseCategory = licenseCategory;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
		this.vehicleClass = vehicleClass;
		this.restrictions = restrictions;
		this.endorsements = endorsements;
		this.vehicleClassification = vehicleClassification;
		this.under18Until = under18Until;
		this.under19Until = under19Until;
		this.under21Until = under21Until;
		this.auditInformation = auditInformation;
	}

	public UserDrivingLicenseEntity() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getDocumentDescriminator() {
		return documentDescriminator;
	}

	public void setDocumentDescriminator(String documentDescriminator) {
		this.documentDescriminator = documentDescriminator;
	}

	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public String getLicenseCategory() {
		return licenseCategory;
	}

	public void setLicenseCategory(String licenseCategory) {
		this.licenseCategory = licenseCategory;
	}

	public Instant getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Instant dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public Instant getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Instant dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public DrivingLicenseRestrictionEntity getRestrictions() {
		return restrictions;
	}

	public void setRestrictions(DrivingLicenseRestrictionEntity restrictions) {
		this.restrictions = restrictions;
	}

	public String getEndorsements() {
		return endorsements;
	}

	public void setEndorsements(String endorsements) {
		this.endorsements = endorsements;
	}

	public VehicleClassificationEntity getVehicleClassification() {
		return vehicleClassification;
	}

	public void setVehicleClassification(VehicleClassificationEntity vehicleClassification) {
		this.vehicleClassification = vehicleClassification;
	}

	public Instant getUnder18Until() {
		return under18Until;
	}

	public void setUnder18Until(Instant under18Until) {
		this.under18Until = under18Until;
	}

	public Instant getUnder19Until() {
		return under19Until;
	}

	public void setUnder19Until(Instant under19Until) {
		this.under19Until = under19Until;
	}

	public Instant getUnder21Until() {
		return under21Until;
	}

	public void setUnder21Until(Instant under21Until) {
		this.under21Until = under21Until;
	}

	public String getAuditInformation() {
		return auditInformation;
	}

	public void setAuditInformation(String auditInformation) {
		this.auditInformation = auditInformation;
	}

	public String getVehicleClass() {
		return vehicleClass;
	}

	public void setVehicleClass(String vehicleClass) {
		this.vehicleClass = vehicleClass;
	}

	

}
