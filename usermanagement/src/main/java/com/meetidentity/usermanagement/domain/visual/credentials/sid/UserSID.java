package com.meetidentity.usermanagement.domain.visual.credentials.sid;

import java.io.Serializable;

public class UserSID implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public UserSIDfront userSIDfront;
	public UserSIDback userSIDback;
	public UserSIDfront getUserSIDfront() {
		return userSIDfront;
	}
	public void setUserSIDfront(UserSIDfront userSIDfront) {
		this.userSIDfront = userSIDfront;
	}
	public UserSIDback getUserSIDback() {
		return userSIDback;
	}
	public void setUserSIDback(UserSIDback userSIDback) {
		this.userSIDback = userSIDback;
	}

	

}
