package com.meetidentity.usermanagement.domain.visual.credentials.policeid;

import java.io.Serializable;

public class PoliceIdTemplateBack implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String Zone2BV_IssuerIN;
	private String Zone1BV_Agency_CardSN;
	private String Zone7BS_Section499;
	private String Zone7BS_Section499_l2;
	private String Zone_6BS_Ainfo;
	private String Zone_6BS_Ainfo_l2;
	private String Height;
	private String Return_to;
	private String Zone4BS_ReturnAddress_l1;
	private String Zone4BS_ReturnAddress_l2;
	private String Zone4BS_ReturnAddress_l3;
	private String Zone5BS_PhysicalCC_Height;
	private String Eyes;
	private String Zone5BS_PhysicalCC_Eyes;
	private String Hairs;
	private String Zone5BS_PhysicalCC_Hairs;
	private String Layer_0;
	private String Zone8BV_LinearBC;
	
	public String getZone2BV_IssuerIN() {
		return Zone2BV_IssuerIN;
	}
	public void setZone2BV_IssuerIN(String zone2bv_IssuerIN) {
		Zone2BV_IssuerIN = zone2bv_IssuerIN;
	}
	public String getZone1BV_Agency_CardSN() {
		return Zone1BV_Agency_CardSN;
	}
	public void setZone1BV_Agency_CardSN(String zone1bv_Agency_CardSN) {
		Zone1BV_Agency_CardSN = zone1bv_Agency_CardSN;
	}
	public String getZone7BS_Section499() {
		return Zone7BS_Section499;
	}
	public void setZone7BS_Section499(String zone7bs_Section499) {
		Zone7BS_Section499 = zone7bs_Section499;
	}
	public String getZone7BS_Section499_l2() {
		return Zone7BS_Section499_l2;
	}
	public void setZone7BS_Section499_l2(String zone7bs_Section499_l2) {
		Zone7BS_Section499_l2 = zone7bs_Section499_l2;
	}
	public String getZone_6BS_Ainfo() {
		return Zone_6BS_Ainfo;
	}
	public void setZone_6BS_Ainfo(String zone_6bs_Ainfo) {
		Zone_6BS_Ainfo = zone_6bs_Ainfo;
	}
	public String getZone_6BS_Ainfo_l2() {
		return Zone_6BS_Ainfo_l2;
	}
	public void setZone_6BS_Ainfo_l2(String zone_6bs_Ainfo_l2) {
		Zone_6BS_Ainfo_l2 = zone_6bs_Ainfo_l2;
	}
	public String getHeight() {
		return Height;
	}
	public void setHeight(String height) {
		Height = height;
	}
	public String getReturn_to() {
		return Return_to;
	}
	public void setReturn_to(String return_to) {
		Return_to = return_to;
	}
	public String getZone4BS_ReturnAddress_l1() {
		return Zone4BS_ReturnAddress_l1;
	}
	public void setZone4BS_ReturnAddress_l1(String zone4bs_ReturnAddress_l1) {
		Zone4BS_ReturnAddress_l1 = zone4bs_ReturnAddress_l1;
	}
	public String getZone4BS_ReturnAddress_l2() {
		return Zone4BS_ReturnAddress_l2;
	}
	public void setZone4BS_ReturnAddress_l2(String zone4bs_ReturnAddress_l2) {
		Zone4BS_ReturnAddress_l2 = zone4bs_ReturnAddress_l2;
	}
	public String getZone4BS_ReturnAddress_l3() {
		return Zone4BS_ReturnAddress_l3;
	}
	public void setZone4BS_ReturnAddress_l3(String zone4bs_ReturnAddress_l3) {
		Zone4BS_ReturnAddress_l3 = zone4bs_ReturnAddress_l3;
	}
	public String getZone5BS_PhysicalCC_Height() {
		return Zone5BS_PhysicalCC_Height;
	}
	public void setZone5BS_PhysicalCC_Height(String zone5bs_PhysicalCC_Height) {
		Zone5BS_PhysicalCC_Height = zone5bs_PhysicalCC_Height;
	}
	public String getEyes() {
		return Eyes;
	}
	public void setEyes(String eyes) {
		Eyes = eyes;
	}
	public String getZone5BS_PhysicalCC_Eyes() {
		return Zone5BS_PhysicalCC_Eyes;
	}
	public void setZone5BS_PhysicalCC_Eyes(String zone5bs_PhysicalCC_Eyes) {
		Zone5BS_PhysicalCC_Eyes = zone5bs_PhysicalCC_Eyes;
	}
	public String getHairs() {
		return Hairs;
	}
	public void setHairs(String hairs) {
		Hairs = hairs;
	}
	public String getZone5BS_PhysicalCC_Hairs() {
		return Zone5BS_PhysicalCC_Hairs;
	}
	public void setZone5BS_PhysicalCC_Hairs(String zone5bs_PhysicalCC_Hairs) {
		Zone5BS_PhysicalCC_Hairs = zone5bs_PhysicalCC_Hairs;
	}
	public String getLayer_0() {
		return Layer_0;
	}
	public void setLayer_0(String layer_0) {
		Layer_0 = layer_0;
	}
	public String getZone8BV_LinearBC() {
		return Zone8BV_LinearBC;
	}
	public void setZone8BV_LinearBC(String zone8bv_LinearBC) {
		Zone8BV_LinearBC = zone8bv_LinearBC;
	}
	
	

}
