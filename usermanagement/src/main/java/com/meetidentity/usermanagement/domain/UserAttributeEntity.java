package com.meetidentity.usermanagement.domain;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "user_attribute")
public class UserAttributeEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserAttributeEntity(Instant dateOfBirth, String nationality, String placeOfBirth, String gender, String contact,
			String address, boolean veteran, boolean organDonor, String bloodType) {
		super();
		this.dateOfBirth = dateOfBirth;
		this.nationality = nationality;
		this.placeOfBirth = placeOfBirth;
		this.gender = gender;
		this.contact = contact;
		this.address = address;
		this.veteran = veteran;
		this.organDonor = organDonor;
		this.bloodType = bloodType;
	}

	public UserAttributeEntity() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "mothers_maiden_name")
	private String mothersMaidenName;

	@Column(name = "email")
	private String email;

	@Column(name = "date_of_birth")
	private Instant dateOfBirth;

	@Column(name = "nationality")
	private String nationality;

	@Column(name = "place_of_birth")
	private String placeOfBirth;

	@Column(name = "gender")
	private String gender;

	@Column(name = "country_code")
	private String countryCode;
	
	@Column(name = "contact")
	private String contact;

	@Column(name = "address")
	private String address;

	@Column(name = "veteran")
	private boolean veteran;

	@Column(name = "organ_donor")
	private boolean organDonor;

	@Column(name = "blood_type")
	private String bloodType;

	@Version
	@Column(name = "version")
	private int version;

	@OneToOne
	@JoinColumn(name = "user_id")
	private UserEntity user;

	@ManyToOne
	@JoinColumn(name = "wf_reg_field_id")
	private WFStepRegistrationConfigEntity wfStepRegistrationConfig;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WFStepRegistrationConfigEntity getWfStepRegistrationConfig() {
		return wfStepRegistrationConfig;
	}

	public void setWfStepRegistrationConfig(WFStepRegistrationConfigEntity wfStepRegistrationConfig) {
		this.wfStepRegistrationConfig = wfStepRegistrationConfig;
	}

	public Instant getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Instant dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isVeteran() {
		return veteran;
	}

	public void setVeteran(boolean veteran) {
		this.veteran = veteran;
	}

	public boolean isOrganDonor() {
		return organDonor;
	}

	public void setOrganDonor(boolean organDonor) {
		this.organDonor = organDonor;
	}

	public String getBloodType() {
		return bloodType;
	}

	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMothersMaidenName() {
		return mothersMaidenName;
	}

	public void setMothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserAttributeEntity other = (UserAttributeEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
