package com.meetidentity.usermanagement.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_healthservicevaccineinfo")
public class UserHealthServiceVaccineInfoEntity extends AbstractAuditingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "userhs_id")
	private UserHealthServiceEntity userhs;

	@Column(name = "uims_id")
	private String uimsId;

	@Column(name = "vaccine")
	private String vaccine;

	@Column(name = "route")
	private String route;

	@Column(name = "site")
	private String site;

	@Column(name = "date")
	private Date date;

	@Column(name = "administered_by")
	private String administeredBy;

	public UserHealthServiceVaccineInfoEntity() {
		// TODO Auto-generated constructor stub
	}

	public UserHealthServiceVaccineInfoEntity(Long id, UserHealthServiceEntity userhs, String uimsId, String vaccine, String route,
			String site, Date date, String administeredBy) {
		super();
		this.id = id;
		this.userhs = userhs;
		this.uimsId = uimsId;
		this.vaccine = vaccine;
		this.route = route;
		this.site = site;
		this.date = date;
		this.administeredBy = administeredBy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserHealthServiceEntity getUserhs() {
		return userhs;
	}

	public void setUserhs(UserHealthServiceEntity userhs) {
		this.userhs = userhs;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public String getVaccine() {
		return vaccine;
	}

	public void setVaccine(String vaccine) {
		this.vaccine = vaccine;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAdministeredBy() {
		return administeredBy;
	}

	public void setAdministeredBy(String administeredBy) {
		this.administeredBy = administeredBy;
	}

}
