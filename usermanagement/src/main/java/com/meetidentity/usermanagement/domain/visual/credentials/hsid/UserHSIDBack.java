package com.meetidentity.usermanagement.domain.visual.credentials.hsid;

import java.io.Serializable;

public class UserHSIDBack implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String background;
	private String ZoneXX_Vaccine;
	private String ZoneXX_Route;
	private String ZoneXX_Site;
	private String ZoneXX_Date;
	private String ZoneXX_AdministeredBy;
	private String ZoneXX_Attention;
	private String ZoneXX_VisitOrCall;
	private String ZoneXX_QR;
	
	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	public String getZoneXX_Vaccine() {
		return ZoneXX_Vaccine;
	}
	public void setZoneXX_Vaccine(String zoneXX_Vaccine) {
		ZoneXX_Vaccine = zoneXX_Vaccine;
	}
	public String getZoneXX_Route() {
		return ZoneXX_Route;
	}
	public void setZoneXX_Route(String zoneXX_Route) {
		ZoneXX_Route = zoneXX_Route;
	}
	public String getZoneXX_Site() {
		return ZoneXX_Site;
	}
	public void setZoneXX_Site(String zoneXX_Site) {
		ZoneXX_Site = zoneXX_Site;
	}
	public String getZoneXX_Date() {
		return ZoneXX_Date;
	}
	public void setZoneXX_Date(String zoneXX_Date) {
		ZoneXX_Date = zoneXX_Date;
	}
	public String getZoneXX_AdministeredBy() {
		return ZoneXX_AdministeredBy;
	}
	public void setZoneXX_AdministeredBy(String zoneXX_AdministeredBy) {
		ZoneXX_AdministeredBy = zoneXX_AdministeredBy;
	}
	public String getZoneXX_Attention() {
		return ZoneXX_Attention;
	}
	public void setZoneXX_Attention(String zoneXX_Attention) {
		ZoneXX_Attention = zoneXX_Attention;
	}
	public String getZoneXX_VisitOrCall() {
		return ZoneXX_VisitOrCall;
	}
	public void setZoneXX_VisitOrCall(String zoneXX_VisitOrCall) {
		ZoneXX_VisitOrCall = zoneXX_VisitOrCall;
	}
	public String getZoneXX_QR() {
		return ZoneXX_QR;
	}
	public void setZoneXX_QR(String zoneXX_QR) {
		ZoneXX_QR = zoneXX_QR;
	}
	
	
	
	
}
