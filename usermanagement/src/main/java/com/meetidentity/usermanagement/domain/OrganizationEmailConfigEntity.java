package com.meetidentity.usermanagement.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.meetidentity.usermanagement.annotations.SequenceValue;

@Entity
@Table(name="org_email_config")
public class OrganizationEmailConfigEntity  extends AbstractAuditingEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
	
    @Column(name="host")
	private String host;
    
    @Column(name="port")
	private int port;
    
    @Column(name="from_display_name")
	private String fromDisplayName;
    
    @Column(name="from")
	private String from;
    
    @Column(name="reply_to_display_name") 
    private String replyToDisplayName;
    
    @Column(name="reply_to")  
    private String replyTo; 
    
    @Column(name="enable_ssl") 
    private boolean enableSsl; 
    
    @Column(name="enable_start_tls") 
    private boolean enableStartTls; 
    
    @Column(name="enable_authentication")
    private boolean enableAuthentication;
    
    @Column(name="username") 
    private String username ;

    @Column(name="password") 
    private String password ;

    @OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "organization_id")
    private OrganizationEntity organization ;     

    @Column(name="status")
    private boolean status;    
   
    @Version
    @Column(name="version")
    private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getFromDisplayName() {
		return fromDisplayName;
	}

	public void setFromDisplayName(String fromDisplayName) {
		this.fromDisplayName = fromDisplayName;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getReplyToDisplayName() {
		return replyToDisplayName;
	}

	public void setReplyToDisplayName(String replyToDisplayName) {
		this.replyToDisplayName = replyToDisplayName;
	}

	public String getReplyTo() {
		return replyTo;
	}

	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}

	public boolean isEnableSsl() {
		return enableSsl;
	}

	public void setEnableSsl(boolean enableSsl) {
		this.enableSsl = enableSsl;
	}

	public boolean isEnableStartTls() {
		return enableStartTls;
	}

	public void setEnableStartTls(boolean enableStartTls) {
		this.enableStartTls = enableStartTls;
	}

	public boolean isEnableAuthentication() {
		return enableAuthentication;
	}

	public void setEnableAuthentication(boolean enableAuthentication) {
		this.enableAuthentication = enableAuthentication;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public OrganizationEntity getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
    
    
    
}
