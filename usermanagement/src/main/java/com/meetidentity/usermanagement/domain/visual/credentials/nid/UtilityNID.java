package com.meetidentity.usermanagement.domain.visual.credentials.nid;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.constants.XMLConstants;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserAppearanceEntity;
import com.meetidentity.usermanagement.domain.UserAttributeEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserNationalIdEntity;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.security.mrz.records.MrzTD1;
import com.meetidentity.usermanagement.security.mrz.types.MrzDate;
import com.meetidentity.usermanagement.security.mrz.types.MrzSex;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.service.helper.UserCredentialsServiceHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAddress;
import com.meetidentity.usermanagement.web.rest.model.UserAppearance;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserNationalID;

@Component
public class UtilityNID {

	@Autowired
	private Util util = new Util();
	
	@Autowired
	private UserCredentialsServiceHelper userCredentialsServiceHelper;
	
	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;
	
	private final Logger log = LoggerFactory.getLogger(UtilityNID.class);

	private final Logger logger = LoggerFactory.getLogger(UtilityNID.class);
	private static String XLINK_NS = XMLConstants.XLINK_NAMESPACE_URI;
	private static String XLINK_HREF = "xlink:href";

	/**
	 * 
	 * @param sAXSVGDocumentFactory
	 * @return
	 * @throws Exception
	 */
	public Document parseNIDFront(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity,User user, byte[] svgFile,
			byte[] jsonFile, UserNationalIdEntity userNationalIdEntity,  String credentialTemplateID, String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			
			if(simplyParse) {
				JSONObject nidJsonObject = new JSONObject(new String(jsonFile));
				JSONObject nidFrontJsonObject = nidJsonObject.getJSONObject("userNIDFront");
				Iterator<String> keys = nidFrontJsonObject.keys();
				log.debug("parseNIDFront: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				// UserPRID userNID = gson.fromJson(new String(jsonFile), UserPRID.class);				
				// Dynamic User Variables
				

				doc.getElementById("Zone2_12FV_IssuingAuthority").setTextContent(organizationEntity.getIssuingAuthority());
				switch (requestFrom) {

					case "web_portal_enroll_summary":
						UserAttribute userAttribute = user.getUserAttribute();
						UserNationalID userNationalID = user.getUserNationalID();
						if(userNationalIdEntity != null) {
							doc.getElementById("Zone2_10FV_IDNumber").setTextContent(userNationalIdEntity.getPersonalCode());
						}
						
						if(doc.getElementById("ZoneXFV_Given_Last_Name")!= null) {
							doc.getElementById("ZoneXFV_Given_Last_Name").setTextContent(userAttribute.getFirstName()+" "+userAttribute.getLastName());
						}
						doc.getElementById("Zone2_02FV_Family_Name").setTextContent(userAttribute.getLastName());
						doc.getElementById("Zone2_03FV_Given_Name").setTextContent(userAttribute.getFirstName());
						doc.getElementById("Zone2_05FV_Sex").setTextContent(userAttribute.getGender());
						doc.getElementById("Zone2_06FV_Nationality").setTextContent(userAttribute.getNationality());
						doc.getElementById("Zone2_07FV_DOB").setTextContent(Util.formatDate(userAttribute.getDateOfBirth(),Util.MMDDYYYY_format));
						doc.getElementById("Zone2_08FV_Birthplace").setTextContent(userAttribute.getPlaceOfBirth());						
						doc.getElementById("Zone2_09FV_DocNum").setTextContent("XXXXXXXXXXX");
						doc.getElementById("Zone2_10FV_DOI").setTextContent("XX/XX/XXXX");
						//doc.getElementById("Covid19_Vaccinated_Date").setTextContent("XX/XX/XXXX");	

						if(userNationalID != null) {
							doc.getElementById("Covid19_Vaccinated_Name").setTextContent(userNationalID.getVaccineName() == null? "XXXXXXXXXX" : userNationalID.getVaccineName());	
							doc.getElementById("Covid19_Vaccinated_Date").setTextContent(userNationalID.getVaccinationDate() == null? "XX/XX/XXXX" : Util.formatDate(userNationalID.getVaccinationDate(), Util.MMDDYYYY_format));	
						}
						doc.getElementById("Zone2_11FV_DOE").setTextContent("XX/XX/XXXX");						
						doc.getElementById("Zone2_10FV_IDNumber_2").setTextContent(userEntity.getUimsId());
						doc.getElementById("Zone2_07FV_DOB_2").setTextContent(Util.formatDate(userAttribute.getDateOfBirth(),Util.MMDDYYYY_format));
						user.getUserBiometrics().stream().forEach(userBio -> {
							if (userBio.getType().equalsIgnoreCase("FACE")) {
								
								int width = Integer.parseInt(doc.getElementById("Zone3_01FV_Photograph").getAttribute("width"));
								int height = Integer.parseInt(doc.getElementById("Zone3_01FV_Photograph").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
									doc.getElementById("Zone3_01FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);

								} catch (IOException e) {
									doc.getElementById("Zone3_01FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
								
								width = Integer.parseInt(doc.getElementById("Zone2_13FV_Photoghost").getAttribute("width"));
								height = Integer.parseInt(doc.getElementById("Zone2_13FV_Photoghost").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate_photoghost(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);

									doc.getElementById("Zone2_13FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);


								} catch (IOException e) {
									doc.getElementById("Zone2_13FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
							if(userBio.getType().equalsIgnoreCase("SIGNATURE")) {
									doc.getElementById("Zone3_02FV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									doc.getElementById("Zone3_02FV_Signature_2").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
							}
						});
						if(doc.getElementById("ZoneXFV_QRCode")!=null) {
							doc.getElementById("ZoneXFV_QRCode")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + userEntity.getId() + "#" + credentialTemplateID +"#"+ "XXXXXXXXXXXX",
											BarcodeFormat.QR_CODE, 300, 300))));
							}
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":
						UserAttributeEntity userAttributee = userEntity.getUserAttribute();
						doc.getElementById("Zone2_10FV_IDNumber").setTextContent(userNationalIdEntity.getPersonalCode() == null? "XXXXXXXXX" : userNationalIdEntity.getPersonalCode());

						if(doc.getElementById("ZoneXFV_Given_Last_Name")!= null) {
							doc.getElementById("ZoneXFV_Given_Last_Name").setTextContent(userAttributee.getFirstName()+" "+userAttributee.getLastName());
						}
						doc.getElementById("Zone2_02FV_Family_Name").setTextContent(userAttributee.getLastName());
						doc.getElementById("Zone2_03FV_Given_Name").setTextContent(userAttributee.getFirstName());
						doc.getElementById("Zone2_05FV_Sex").setTextContent(userAttributee.getGender());
						doc.getElementById("Zone2_06FV_Nationality").setTextContent(userAttributee.getNationality());
						doc.getElementById("Zone2_07FV_DOB").setTextContent(Util.formatDate(userAttributee.getDateOfBirth(),Util.MMDDYYYY_format));
						doc.getElementById("Zone2_08FV_Birthplace").setTextContent(userAttributee.getPlaceOfBirth());						
						doc.getElementById("Zone2_09FV_DocNum").setTextContent(userNationalIdEntity.getDocumentNumber() == null? "XXXXXXXXX" : userNationalIdEntity.getDocumentNumber());
						doc.getElementById("Zone2_10FV_DOI").setTextContent(userNationalIdEntity.getDateOfIssuance() == null? "XX/XX/XXXX" : Util.formatDate(userNationalIdEntity.getDateOfIssuance(), Util.MMDDYYYY_format));
						doc.getElementById("Zone2_11FV_DOE").setTextContent(userNationalIdEntity.getDateOfExpiry() == null? "XX/XX/XXXX" : Util.formatDate(userNationalIdEntity.getDateOfExpiry(), Util.MMDDYYYY_format));
						
						doc.getElementById("Zone2_10FV_IDNumber_2").setTextContent(userEntity.getUimsId());
						doc.getElementById("Zone2_07FV_DOB_2").setTextContent(Util.formatDate(userAttributee.getDateOfBirth(),Util.MMDDYYYY_format));

						doc.getElementById("Covid19_Vaccinated_Date").setTextContent(userNationalIdEntity.getVaccinationDate() == null? "XX/XX/XXXX" : Util.formatDate(userNationalIdEntity.getVaccinationDate(), Util.MMDDYYYY_format));	
						doc.getElementById("Covid19_Vaccinated_Name").setTextContent(userNationalIdEntity.getVaccineName() == null? "XXXXXXXXXX" : userNationalIdEntity.getVaccineName());	
						
						userEntity.getUserBiometrics().stream().forEach(userBio -> {
							if (userBio.getType().equalsIgnoreCase("FACE")) {
								
								int width = Integer.parseInt(doc.getElementById("Zone3_01FV_Photograph").getAttribute("width"));
								int height = Integer.parseInt(doc.getElementById("Zone3_01FV_Photograph").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
									doc.getElementById("Zone3_01FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);

								} catch (IOException e) {
									doc.getElementById("Zone3_01FV_Photograph").setAttributeNS(XLINK_NS, XLINK_HREF,"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
								
								width = Integer.parseInt(doc.getElementById("Zone2_13FV_Photoghost").getAttribute("width"));
								height = Integer.parseInt(doc.getElementById("Zone2_13FV_Photoghost").getAttribute("height"));
								try {
									Image image = Util
											.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));
									BufferedImage bufferedImage = Util.resizeImage(image, width, height);
									if(user.getIsTransparentPhotoRequired()) {
										bufferedImage = backgroundColorTransparency.generate_photoghost(bufferedImage, organizationEntity.getName());
									}
									String base64Data = Util.convertBufferedImageToBase64(bufferedImage);

									doc.getElementById("Zone2_13FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + base64Data);


								} catch (IOException e) {
									doc.getElementById("Zone2_13FV_Photoghost").setAttributeNS(XLINK_NS, XLINK_HREF,
											"data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
								}
							}
							if(userBio.getType().equalsIgnoreCase("SIGNATURE")) {
									doc.getElementById("Zone3_02FV_Signature").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
									doc.getElementById("Zone3_02FV_Signature_2").setAttributeNS(XLINK_NS, XLINK_HREF, "data:;base64," + Base64.getEncoder().encodeToString(userBio.getData()));
							}
						});
						if(doc.getElementById("ZoneXFV_QRCode")!=null) {
						doc.getElementById("ZoneXFV_QRCode")
						.setAttributeNS(XLINK_NS, XLINK_HREF,
								"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
										organizationEntity.getName() + "#" + userEntity.getId() + "#" + credentialTemplateID +"#"+ userNationalIdEntity.getPersonalCode(),
										BarcodeFormat.QR_CODE, 300, 300))));
						}
						break;
					default :
				
				}
								
				logger.debug("parseNIDFront: Ended Dynamic Variable");
			}
			return doc;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}

	public Document parseNIDBack(String requestFrom, SAXSVGDocumentFactory sAXSVGDocumentFactory, OrganizationEntity organizationEntity, UserEntity userEntity,User user, byte[] svgFile,
			byte[] jsonFile, UserNationalIdEntity userNationalIdEntity,  String credentialTemplateID,
			String visualCredentialType, boolean simplyParse) throws VisualCredentialException {
		try {
			Document doc = sAXSVGDocumentFactory.createDocument(null, new ByteArrayInputStream(svgFile));
			Gson gson = new Gson();
			// log.debug(new String(jsonFile));
			if(simplyParse) {

				JSONObject nidJsonObject = new JSONObject(new String(jsonFile));
				JSONObject nidBacktJsonObject = nidJsonObject.getJSONObject("userNIDBack");
				Iterator<String> keys = nidBacktJsonObject.keys();
				log.debug("parseNIDBack: Started");
				
				while(keys.hasNext()) {
					String key = keys.next();
					System.out.println(key);
				      if(doc.getElementById(key) == null) {
				    	  log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				    	  throw new VisualCredentialException( Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType));
				      };
				}
				return doc ;
			}else {

				// UserPRID userNID = gson.fromJson(new String(jsonFile), UserPRID.class);
				// Dynamic User Variables
				switch (requestFrom) {

					case "web_portal_enroll_summary":
						UserAttribute userAttribute = user.getUserAttribute();
						UserAppearance uerAppearance = user.getUserAppearance();
						UserNationalID userNationalID = user.getUserNationalID();
						UserAddress userAddress = user.getUserAddress();
						if(uerAppearance != null) {
							doc.getElementById("Zone4_01BV_Height").setTextContent(uerAppearance.getHeight()!= null?uerAppearance.getHeight():"");
							doc.getElementById("Zone4_02BV_Eyes").setTextContent(uerAppearance.getEyeColor()!= null?uerAppearance.getEyeColor():"");
						}
						doc.getElementById("Zone2_03BV_DocNum").setTextContent("XXXXXXXXXX");
						String addressL1 = userCredentialsServiceHelper.getAddressFromAddressObj_addressLine1(userAddress);
						String addressL2 = userCredentialsServiceHelper.getAddressFromAddressObj_addressLine2(userAddress);
						if(addressL1 != null && addressL2 != null) {
							doc.getElementById("Zone4_04BV_CHAddress_1").setTextContent(addressL1);	
							doc.getElementById("Zone4_04BV_CHAddress_2").setTextContent(addressL2);	
						} else {
							//TODO need to add few more conditional setters in future
							doc.getElementById("Zone4_04BV_CHAddress_1").setTextContent("");	
							doc.getElementById("Zone4_04BV_CHAddress_2").setTextContent("");	
						}
						try {
						}catch (Exception e) {
							log.info("Vaccine info is not supported");
						}
						
						 final MrzTD1 r = new MrzTD1();
					        r.code2 = 'I';
					        r.issuingCountry = userAttribute.getNationality().toUpperCase();
					        r.nationality = userAttribute.getNationality().toUpperCase();
					        r.optional = "";   r.optional2 = "";
					        Calendar cal_dob = Calendar.getInstance();
					        cal_dob.setTime(userAttribute.getDateOfBirth());
					        Calendar cal_exp = Calendar.getInstance();
					        
							r.documentNumber = "XXXXXXXX";
							cal_exp.setTime(new Date());
							
							r.expirationDate = new MrzDate(cal_exp.get(Calendar.YEAR)%100, cal_exp.get(Calendar.MONTH)+1,cal_exp.get(Calendar.DAY_OF_MONTH) );
					        r.dateOfBirth = new MrzDate(cal_dob.get(Calendar.YEAR)%100, cal_dob.get(Calendar.MONTH)+1,cal_dob.get(Calendar.DAY_OF_MONTH) );
					        if(userAttribute.getGender().toLowerCase().contains("m")) {
					        	r.sex = MrzSex.Male;
					        }else 
					        if(userAttribute.getGender().toLowerCase().contains("f")) {
					        	r.sex = MrzSex.Female;
					        }else {
					        	r.sex = MrzSex.Male;
					        }
					        r.surname = userAttribute.getLastName();
					        r.givenNames = userAttribute.getFirstName();
					        doc.getElementById("Zone5_MRZ_TD1_L1").setTextContent(r.toMrz_line1());
					        doc.getElementById("Zone5_MRZ_TD1_L2").setTextContent(r.toMrz_line2());
					        doc.getElementById("Zone5_MRZ_TD1_L3").setTextContent(r.toMrz_line3());
					        doc.getElementById("Zone4_05BV_QR")
							.setAttributeNS(XLINK_NS, XLINK_HREF,
									"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
											organizationEntity.getName() + "#" + user.getId() + "#" + credentialTemplateID + "#" + "XXXXXXXXXX",
											BarcodeFormat.QR_CODE, 300, 300))));
						
						break;
					case "web_portal_summary":
					case "web_portal_approval_summary":
					case "web_portal_print":
					case "Request_From_Mobile":
						UserAttributeEntity userAttributee = userEntity.getUserAttribute();
						UserAppearanceEntity uerAppearancee = userEntity.getUserAppearance();
						UserNationalIdEntity userNationalIdEntityy = userEntity.getUserNationalId();
						String addressL1_ = userCredentialsServiceHelper.getAddressFromAddressEntity_addressLine1(userEntity.getUserAddress());
						String addressL2_ = userCredentialsServiceHelper.getAddressFromAddressEntity_addressLine2(userEntity.getUserAddress());
						if(addressL1_ != null && addressL2_ != null) {
							doc.getElementById("Zone4_04BV_CHAddress_1").setTextContent(addressL1_);	
							doc.getElementById("Zone4_04BV_CHAddress_2").setTextContent(addressL2_);	
						} else {
							//TODO need to add few more conditional setters in future
							doc.getElementById("Zone4_04BV_CHAddress_1").setTextContent("");	
							doc.getElementById("Zone4_04BV_CHAddress_2").setTextContent("");	
						}

						if(uerAppearancee != null) {
							doc.getElementById("Zone4_01BV_Height").setTextContent(uerAppearancee.getHeight()!= null?uerAppearancee.getHeight():"");
							doc.getElementById("Zone4_02BV_Eyes").setTextContent(uerAppearancee.getEyeColor()!= null?uerAppearancee.getEyeColor():"");
						}
						doc.getElementById("Zone2_03BV_DocNum").setTextContent(userNationalIdEntity.getDocumentNumber() == null? "XXXXXXXXX" : userNationalIdEntity.getDocumentNumber());

						try {
//							doc.getElementById("Zone6BV_VaccinationName").setTextContent(userNationalIdEntityy.getVaccineName());	
//							doc.getElementById("Zone6BV_VaccinationDate").setTextContent(userNationalIdEntityy.getVaccinationDate() == null? "XX-XX-XXXX" : Util.formatDate(userNationalIdEntityy.getVaccinationDate(), Util.MMDDYYYY_format));	
//							doc.getElementById("Zone6BV_VaccinationLocationName").setTextContent(userNationalIdEntityy.getVaccinationLocationName());	
						}catch (Exception e) {
							log.info("Vaccine info is not supported");
						}
						
						final MrzTD1 rr = new MrzTD1();
				        rr.code2 = 'I';
				        rr.issuingCountry = userAttributee.getNationality().toUpperCase();
				        rr.nationality = userAttributee.getNationality().toUpperCase();
				        rr.optional = "";   rr.optional2 = "";
				        Calendar cal_dobb = Calendar.getInstance();
				        cal_dobb.setTime(Date.from(userAttributee.getDateOfBirth()));
				        Calendar cal_expp = Calendar.getInstance();
				        
						rr.documentNumber = userNationalIdEntity.getDocumentNumber();
						cal_expp.setTime(userNationalIdEntityy.getDateOfExpiry() == null? new Date() : Date.from(userNationalIdEntityy.getDateOfExpiry()));
						
						rr.expirationDate = new MrzDate(cal_expp.get(Calendar.YEAR)%100, cal_expp.get(Calendar.MONTH)+1,cal_expp.get(Calendar.DAY_OF_MONTH) );
				        rr.dateOfBirth = new MrzDate(cal_dobb.get(Calendar.YEAR)%100, cal_dobb.get(Calendar.MONTH)+1,cal_dobb.get(Calendar.DAY_OF_MONTH) );
				        if(userAttributee.getGender().toLowerCase().contains("m")) {
				        	rr.sex = MrzSex.Male;
				        }else 
				        if(userAttributee.getGender().toLowerCase().contains("f")) {
				        	rr.sex = MrzSex.Female;
				        }else {
				        	rr.sex = MrzSex.Male;
				        }
				        rr.surname = userAttributee.getLastName();
				        rr.givenNames = userAttributee.getFirstName();
				        doc.getElementById("Zone5_MRZ_TD1_L1").setTextContent(rr.toMrz_line1());
				        doc.getElementById("Zone5_MRZ_TD1_L2").setTextContent(rr.toMrz_line2());
				        doc.getElementById("Zone5_MRZ_TD1_L3").setTextContent(rr.toMrz_line3());
				        String idNumber = "XXXXXXXXXXXX";
						if(userNationalIdEntityy.getPersonalCode() != null) {
							idNumber = userNationalIdEntityy.getPersonalCode();
						}
						
				        doc.getElementById("Zone4_05BV_QR")
						.setAttributeNS(XLINK_NS, XLINK_HREF,
								"data:;base64," + Base64.getEncoder().encodeToString((new QRCodeHelper().getQRCodeImage(
										organizationEntity.getName() + "#" + userEntity.getId() + "#" + credentialTemplateID + "#" + idNumber,
										BarcodeFormat.QR_CODE, 300, 300))));
					
						break;
					default :
				
				}
				
//				doc.getElementById("background").setAttributeNS(XLINK_NS, XLINK_HREF,
//						"data:;base64," + userNID.getUserNIDBack().getBackground());
							
				logger.debug("parseNIDBack: Ended dynamic Variable");
				
				return doc;
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS, visualCredentialType),
					cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS,
					visualCredentialType);
		}
	}
	
}
