package com.meetidentity.usermanagement.config;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Define Constants for User management service
 *
 */
public interface ApplicationConstants {

	// User Statuses

	// Workflow Steps

	// Mail
	public static int SC_EMAIL_SENT = 0;
	public static int ER_EMAIL_SENT = 1;

	// Retrun Values
	public static int SUCCESS = 0;
	public static int FAILURE = 1;

	public static Marker notifyAdmin = MarkerFactory.getMarker("NOTIFY_ADMIN");
	public static Marker notifyCreate = MarkerFactory.getMarker("CREATE");
	public static Marker notifySearch = MarkerFactory.getMarker("SEARCH");
	public static Marker notifyUpdate = MarkerFactory.getMarker("UPDATE");
	public static Marker notifyDelete = MarkerFactory.getMarker("DELETE");
	public static Marker notifyValidate = MarkerFactory.getMarker("VALIDATE");
	public static Marker notifySend = MarkerFactory.getMarker("SEND");

}
