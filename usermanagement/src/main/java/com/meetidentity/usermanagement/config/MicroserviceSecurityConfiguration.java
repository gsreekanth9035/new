package com.meetidentity.usermanagement.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

import com.meetidentity.usermanagement.security.AuthoritiesConstants;
import com.meetidentity.usermanagement.security.jwt.JWTConfigurer;
import com.meetidentity.usermanagement.security.jwt.TokenProvider;

@Configuration
@Import(SecurityProblemSupport.class)
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class MicroserviceSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final TokenProvider tokenProvider;

    private final SecurityProblemSupport problemSupport;

    public MicroserviceSecurityConfiguration(TokenProvider tokenProvider, SecurityProblemSupport problemSupport) {
        this.tokenProvider = tokenProvider;
        this.problemSupport = problemSupport;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .antMatchers("/app/**/*.{js,html}")
            .antMatchers("/bower_components/**")
            .antMatchers("/i18n/**")
            .antMatchers("/content/**")
            .antMatchers("/swagger-ui/index.html")
            .antMatchers("/test/**")
            .antMatchers("/h2-console/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf()
            .disable()
            .exceptionHandling()
            .authenticationEntryPoint(problemSupport)
            .accessDeniedHandler(problemSupport)
        .and()
            .headers()
            .frameOptions()
            .disable()
        .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
            .authorizeRequests()
            .antMatchers("/api/v1/organizations").permitAll()
            .antMatchers("/api/v1/organizations/*/idp-users").permitAll()
            .antMatchers("/api/v1/organizations/*/idp-users/local-register").permitAll()
            .antMatchers("/api/v1/organizations/*/idp-users/*/user-credentials").permitAll()
            .antMatchers("/api/v1/organizations/*/idp-users/*/user-credentials/*/moveAfter/*").permitAll()
            .antMatchers("/api/v1/organizations/*/idp-users/*/user-credentials/*/moveToFirst").permitAll()
            .antMatchers("/api/v1/organizations/*/idp-users/*/user-credentials/config").permitAll()
            .antMatchers("/api/v1/organizations/*/idp-users/*/user-credentials/list").permitAll()
            .antMatchers("/api/v1/organizations/*/idp-users/*/user-credentials/update").permitAll()
            .antMatchers("/api/v1/organizations/*/idp-users/*/user-credentials/delete").permitAll()
            .antMatchers("/api/v1/organizations/*/idp-users/*/execute-webauthn").permitAll()  
            .antMatchers("/api/v1/organizations/*/idp-users/*/webauthn-policy-details").permitAll()  
            .antMatchers("/api/v1/organizations/*/idp-users/*/webauthn-register-authenticator").permitAll()            
            .antMatchers("/api/v1/organizations/*/idp-users/*/onboardUserDetails").permitAll()
            .antMatchers("/api/v1/organizations/*/idp-users/roles").permitAll()
            .antMatchers("/api/v1/organizations/*/idp-client/redirect-uri").permitAll()
            .antMatchers("/api/v1/organizations/*/transparent-image-enabled-check").permitAll()
            .antMatchers("/api/v1/getOrganizationBrandingInfo/**").permitAll()           
            .antMatchers("/api/v1/organizations/*/getApprovalRejectionReasons").permitAll()
            .antMatchers("/api/v1/organizations/*/registrationConfigDetails/identitymodel/*").permitAll()
            .antMatchers("/api/v1/organizations/*/users/search/invite").permitAll()   
            .antMatchers("/api/v1/organizations/*/users/*/userIdentity/*/credentials/*").permitAll()
            .antMatchers("/api/v1/organizations/*/users/search/*").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/userAge").permitAll()
            .antMatchers("/api/v1/organizations/*/users").permitAll()
            .antMatchers("/api/v1/organizations/*/users/register").permitAll()
            .antMatchers("/api/v1/organizations/*/users/register/via-mobile-device").permitAll()
            .antMatchers("/api/v1/organizations/*/users/groups").permitAll()
            .antMatchers("/api/v1/organizations/*/users/updateUserStatusAfterDeleteCred").permitAll()            
            .antMatchers("/api/v1/organizations/*/users/*/workflow").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/workflowGeneralDetails").permitAll()            
            .antMatchers("/api/v1/organizations/*/users/*/workflow-credential-templates").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/visual-credentials/*").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/visual-credentials-on-summary/*").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/validateUserSession").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/enrollmentStepsStatus").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/uuid").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/uniqueuid").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/issuanceStatus").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/approveEnrollment").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/rejectEnrollment").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/resetMobileOTPCredential").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/orgUserIds").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/user-details").permitAll()   
            .antMatchers("/api/v1/organizations/*/users/*/access/*").permitAll()           
            .antMatchers("/api/v1/fingerprint/providers/*/**").permitAll()
            .antMatchers("/api/v1/organizations/*/roles").permitAll()
            .antMatchers("/api/v1/organizations/*/roles/*").permitAll()
            .antMatchers("/api/v1/organizations/*/roles/*/workflow").permitAll()
            .antMatchers("/api/v1/organizations/*/roles/*/permissions").permitAll()
            .antMatchers("/api/v1/organizations/*/roles/*/permissionMode").permitAll()
            .antMatchers("/api/v1/organizations/*/groups").permitAll()
            .antMatchers("/api/v1/organizations/*/groups/*").permitAll()
            .antMatchers("/api/v1/organizations/*/groups/users/*").permitAll()
            .antMatchers("/api/v1/user-cards/*").permitAll()
            .antMatchers("/api/v1/push-notification-devices").permitAll()
            .antMatchers("/api/v1/push-notification-devices/payLoad").permitAll()
            .antMatchers("/api/v1/workflow-step-defs").permitAll()
            .antMatchers("/api/v1/organizations/*/workflows").permitAll()
            .antMatchers("/api/v1/organizations/*/workflows/*").permitAll()
            .antMatchers("/api/v1/organizations/*/workflows/*/steps").permitAll()
            .antMatchers("/api/v1/organizations/*/workflows/*/steps/perso-and-issuance-step/applet-loading-info").permitAll()
            .antMatchers("/api/v1/organizations/*/workflows/*/steps/*/details").permitAll()
            .antMatchers("/api/v1/organizations/*/workflows/*/steps/*/genericConfigs").permitAll()
            .antMatchers("/api/v1/organizations/*/workflows/*/device-profiles").permitAll()
            .antMatchers("/api/v1/organizations/*/workflows/*/device-profiles-names").permitAll()
            .antMatchers("/api/v1/organizations/*/workflows/*/mobile-issuance-details").permitAll()
            .antMatchers("/api/v1/organizations/*/workflows/*/steps/*").permitAll()
            .antMatchers("/api/v1/organizations/*/workflows/*/steps/*/*/orgIdentityFieldIdNames").permitAll()
            .antMatchers("/api/v1/organizations/*/*/*/configs").permitAll()
            .antMatchers("/api/v1/workflowCertConfigId/*").permitAll()
            .antMatchers("/api/v1/workflowMobileIdCertConfigId/*").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*").permitAll()
            .antMatchers("/api/v1/organizations/*/users/updateUserStatus/toReissuance").permitAll()            
            .antMatchers("/api/v1/organizations/*/users/groups/*/*").permitAll()   
            .antMatchers("/api/v1/organizations/*/users/searchByLocation").permitAll()    
            .antMatchers("/api/v1/organizations/*/users/updateUsersLocationStatus/*").permitAll()
            .antMatchers("/api/v1/organizations/*/users/enablePush/*").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/details").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/pairDevice").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/smartCardPrinters").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/mobileIdentities").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/uuid").permitAll()
            .antMatchers("/api/v1/organizations/*/users/getByID/*").permitAll()
            .antMatchers("/api/v1/organizations/*/users/getByID/*/details").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/biometrics").permitAll()
            .antMatchers("/api/v1/organizations/*/users/biometrics/face/transparent").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/enableSoftOTP2FA").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/issuedUserDevices").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/biometrics/*").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/wf-mobileIdStep-details").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/enrollment-details").permitAll()
            .antMatchers("/api/v1/organizations/*/users/*/user-status").permitAll()
	        .antMatchers("/api/v1/organizations/*/users/express-enroll").permitAll()
	        .antMatchers("/api/v1/organizations/*/users/express-enroll/rp").permitAll()
	        .antMatchers("/api/v1/organizations/*/users/express-enroll/groups/*").permitAll()
	        .antMatchers("/api/v1/organizations/*/users/express-enroll/groups/*/roles/*").permitAll()
            .antMatchers("/api/v1/*/users/*/credentials/*").permitAll()
            .antMatchers("/api/v1/organizations/*/deviceManufacturer/*/deviceName/*").permitAll()
            .antMatchers("/api/v1/organizations/*/deviceManufacturer/*").permitAll()
			.antMatchers("/api/v1/organizations/*/device-profiles").permitAll()
	        .antMatchers("/api/v1/organizations/*/device-profiles/*").permitAll()
	        .antMatchers("/api/v1/organizations/*/device-profiles/*/*").permitAll()
	        .antMatchers("/api/v1/organizations/*/brandingInfo").permitAll()
	        .antMatchers("/api/v1/organizations/*/advertisement-info").permitAll()
	        .antMatchers("/api/v1/organizations/*/pair-mobile-service").permitAll()
	        .antMatchers("/api/v1/organizations/*/issuingIdentityTypes").permitAll()
	        .antMatchers("/api/v1/organizations/*/trustedExistingIdentityTypes").permitAll()
	        .antMatchers("/api/v1/organizations/*/*/visual-templates").permitAll()
	        .antMatchers("/api/v1/organizations/*/config/*/values").permitAll()
	        .antMatchers("/api/v1/organizations/*").permitAll()
	        .antMatchers("/api/v1/organizations/*/vehicle/vehicleClassifications").permitAll()
	        .antMatchers("/api/v1/organizations/*/drivingLicense/restrictions").permitAll()
	        .antMatchers("/api/v1/organizations/*/visualtemplates").permitAll()
	        .antMatchers("/api/v1/organizations/*/visualtemplates/*").permitAll()
	        .antMatchers("/api/v1/organizations/*/vehicle-registrations").permitAll()
	        .antMatchers("/api/v1/organizations/*/vehicle-registrations/*").permitAll()
	        .antMatchers("/api/v1/organizations/*/users/*/vehicles").permitAll()
	        .antMatchers("/api/v1/organizations/*/users/*/vehicles/*").permitAll()
	        .antMatchers("/api/v1/organizations/*/users/*").permitAll()
            .antMatchers("/api/v1/organizations/*/users/**").permitAll()
	        .antMatchers("/api/v1/organizations/*/fingersByCount/*").permitAll()
	        .antMatchers("/api/v1/organizations/*/mobile-device/token").permitAll()
	        .antMatchers("/api/v1/organizations/*/mobile-device/pn-login").permitAll()
	        .antMatchers("/api/v1/devices").permitAll()
	        .antMatchers("/api/v1/devices/login").permitAll()
	        .antMatchers("/api/v1/devices/getUserByUUID").permitAll()
	        .antMatchers("/api/v1/devices/getUserAndClientInfoByUUID").permitAll()
	        .antMatchers("/api/v1/devices/getUserIDByName").permitAll()
	        .antMatchers("/api/v1/devices/mobile-service/organizations/*/client-credentials").permitAll()
	        .antMatchers("/api/v1/devices/organizations/*/getUserIDByName/*").permitAll()
	        .antMatchers("/api/v1/devices/organizations/*/syncUserDetails/*").permitAll()
	        .antMatchers("/api/v1/devices/organizations/*/actionsByRole/**").permitAll()
	        .antMatchers("/api/v1/organizations/*/roles/*/color").permitAll()
	        .antMatchers("/api/v1/device-types").permitAll()
	        .antMatchers("/api/v1/device-types/visual-applicable").permitAll()
	        .antMatchers("/api/v1/branding").permitAll()
	        .antMatchers("/api/v1/organizations/*/idProof/idProofTypes").permitAll()
	        .antMatchers("/api/v1/organizations/*/idProof/idProofTypes/*/issuingAuthorities").permitAll()
            .antMatchers("/api/**").authenticated()
            .antMatchers("/management/health").permitAll()
            .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
            .antMatchers("/swagger-resources/configuration/ui").permitAll()   

        .and()
            .apply(securityConfigurerAdapter());
    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }

    @Bean
    public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
        return new SecurityEvaluationContextExtension();
    }
}
