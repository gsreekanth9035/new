package com.meetidentity.usermanagement.config.message;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

public class MessagingService {
	private final Logger logger = LoggerFactory.getLogger(MessagingService.class);
	private static MessagingService messaging_service_instance = null;
	
	private MessagingService() {
	}
	public static MessagingService getInstance() {
		if (messaging_service_instance == null)
			messaging_service_instance = new MessagingService();

		return messaging_service_instance;
	}

	@SuppressWarnings("deprecation")
	public AmazonSNSClient getAwsSNSClient(String awsAccessKey, String awsSecretKey) {
		AWSCredentials awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
		logger.debug("aws client generated ");
		return new AmazonSNSClient(awsCredentials);
	}

	@SuppressWarnings("deprecation")
	public PublishResult  sendSMSFromAWS(AmazonSNSClient awsAmazonSNSClient,String mobileNumber,
			String messageBody) {
		PublishResult result = null;
		try {
			logger.debug("send SMS from AWS started  ");
			awsAmazonSNSClient.setRegion(Region.getRegion(Regions.US_WEST_1));
			Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();
			result = awsAmazonSNSClient.publish(new PublishRequest().withMessage(messageBody)
					.withPhoneNumber(mobileNumber).withMessageAttributes(smsAttributes));
			logger.debug("send SMS from AWS Ended and sent SMS to registered device  ");
		} catch (Exception e) {
			logger.debug("Error : "+e.getMessage());
			throw e;
		}
		return result;
	}

}
