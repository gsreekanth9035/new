package com.meetidentity.usermanagement.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * Properties specific to Usermanagement.
 * <p>
 * Properties are configured in the application.yml file.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
@Configuration
@Component
public class ApplicationProperties {
	
	private Map<String,KeycloakConfiguration> map_keycloakConfigurations = new HashMap<String,KeycloakConfiguration>();
	
	private List<KeycloakConfiguration> keycloakConfigurationList = new ArrayList<KeycloakConfiguration>();
	private QrCode qrcode = new QrCode();
	private NeuroLicense neuro = new NeuroLicense();
	private SMS sms = new SMS();
	
	
	
	public static class NeuroLicense {
		private String isTrail;

		public String getIsTrail() {
			return isTrail;
		}

		public void setIsTrail(String isTrail) {
			this.isTrail = isTrail;
		}

		
		
	}
	
	public static class SMS {
		private String id;
		private String secrete;
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getSecrete() {
			return secrete;
		}
		public void setSecrete(String secrete) {
			this.secrete = secrete;
		}
	}
	
	public static class QrCode {
		
		private int qrCodeExpiryInMints;
		
		private String aesKey;
		
		private String ivParam;
		
		

		public String getAesKey() {
			return aesKey;
		}

		public void setAesKey(String aesKey) {
			this.aesKey = aesKey;
		}

		public String getIvParam() {
			return ivParam;
		}

		public void setIvParam(String ivParam) {
			this.ivParam = ivParam;
		}

		public int getQrCodeExpiryInMints() {
			return qrCodeExpiryInMints;
		}

		public void setQrCodeExpiryInMints(int qrCodeExpiryInMints) {
			this.qrCodeExpiryInMints = qrCodeExpiryInMints;
		}
		
	}

	@Component
	public static class KeycloakConfiguration {
		
		private String basePath;
		private String realm;
		
		private String clientId;
		private String clientSecret;
		
		private String username;
		private String password;
		
		private String mobileUserName;
		private String mobileUserPassword;
		
		private String grantType;
		


		public String getBasePath() {
			return basePath;
		}

		public void setBasePath(String basePath) {
			this.basePath = basePath;
		}

		public String getRealm() {
			return realm;
		}

		public void setRealm(String realm) {
			this.realm = realm;
		}

		public String getClientId() {
			return clientId;
		}

		public void setClientId(String clientId) {
			this.clientId = clientId;
		}

		public String getClientSecret() {
			return clientSecret;
		}

		public void setClientSecret(String clientSecret) {
			this.clientSecret = clientSecret;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getGrantType() {
			return grantType;
		}

		public void setGrantType(String grantType) {
			this.grantType = grantType;
		}

		public String getMobileUserName() {
			return mobileUserName;
		}

		public void setMobileUserName(String mobileUserName) {
			this.mobileUserName = mobileUserName;
		}

		public String getMobileUserPassword() {
			return mobileUserPassword;
		}

		public void setMobileUserPassword(String mobileUserPassword) {
			this.mobileUserPassword = mobileUserPassword;
		}
		
		
		
		
	}


	
	
	public List<KeycloakConfiguration> getKeycloakConfigurationList() {
		return keycloakConfigurationList;
	}


	public void setKeycloakConfigurationList(List<KeycloakConfiguration> keycloakConfigurationList) {
		this.keycloakConfigurationList = keycloakConfigurationList;
	}


	public QrCode getQrcode() {
		return qrcode;
	}


	public void setQrcode(QrCode qrcode) {
		this.qrcode = qrcode;
	}


	public NeuroLicense getNeuro() {
		return neuro;
	}


	public void setNeuro(NeuroLicense neuro) {
		this.neuro = neuro;
	}


	public SMS getSms() {
		return sms;
	}


	public void setSms(SMS sms) {
		this.sms = sms;
	}


	public KeycloakConfiguration getKeycloak(String organizationName) {
		if(map_keycloakConfigurations.isEmpty()) {
			for (KeycloakConfiguration keycloakConfiguration : keycloakConfigurationList) {

				map_keycloakConfigurations.put(keycloakConfiguration.getRealm().toLowerCase(), keycloakConfiguration);

			}
		}
				
		return map_keycloakConfigurations.get(organizationName.toLowerCase());
		
	}
	
	
	

	
	
}
