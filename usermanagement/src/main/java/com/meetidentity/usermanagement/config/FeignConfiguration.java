package com.meetidentity.usermanagement.config;

import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "com.meetidentity.usermanagement")
public class FeignConfiguration {

}
