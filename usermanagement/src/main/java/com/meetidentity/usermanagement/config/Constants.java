package com.meetidentity.usermanagement.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String DEFAULT_LANGUAGE = "en";
    
    // Request Headers
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_VALUE_PREFIX_BEARER = "Bearer ";
    
    // TODO Move to Keycloak constants if required
    // Keycloak
    public static final String ADMIN_REALM = "master";
    public static final String ADMIN_CLIENT_ID = "admin-cli";
    
    public static final String GRANT_TYPE = "grant_type";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String REFRESH_TOKEN = "refresh_token";

    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET = "client_secret";

    // identity types
    public static final String PIV_and_PIV_1 = "Government";
    public static final String Employee_ID = "Workforce";
    public static final String National_ID = "National ID";
    public static final String Driving_License = "Driving License";
    public static final String Vehicle_Identification_ID = "Vehicle Identification ID";
    public static final String Health_ID = "Health ID";
    public static final String Voter_ID = "Voter ID";
    public static final String Student_ID = "Student ID";
    public static final String Health_Service_ID = "Health Service ID";
    public static final String Permanent_Resident_ID = "Permanent Resident ID";
    public static final String police_id = "police_id";
    public static final String mls_id = "mls_id";
    
    // User identity data fields for mobile API
    public static final String Nationality = "Nationality";
    public static final String Date_Of_Birth = "Date Of Birth";
    public static final String Email = "Email";
    public static final String License_Category = " License Category";
    public static final String Gender = "Gender";
    public static final String Address = "Address";
    public static final String UserStatus = "User Status";
    public static final String Photo = "Photo";
    public static final String Verify_Age_For_Text = "verify_age_by_number";
    public static final String Verify_Age_For_Number = "verify_age_by_text";
    public static final String Under18 = "Under18";
    public static final String Under19 = "Under19";
    public static final String Under21 = "Under21";
    

    public static final String Identity_Type = "Identity Type";
    
    // Driving License
    public static final String License_Number = "License Number";
    public static final String Document_Descriminator = "Document Descriminator";
    public static final String Issuing_Authority = "Issuing Authority";
    public static final String Date_Of_Issuance = "Date Of Issuance";
    public static final String Date_Of_Expiry = "Date Of Expiry";
    public static final String Restrictions = "Restrictions";
    public static final String Endorsements = "Endorsements";
    public static final String Vehicle_Class = "Vehicle Class";
    public static final String Under18Until = "Under18Until";
    public static final String Under19Until = "Under19Until";
    public static final String Under21Until = "Under21Until";
    public static final String Audit_Information = "Audit Information";
    // PIV 
    public static final String Piv_Number = "Workforce ID";
    public static final String Affiliation = "Affiliation";
    public static final String Affiliation_Optional_Line = "Affiliation Optional Line";
    public static final String Affiliation_Color = "Affiliation Color";
    public static final String Agency_Card_Serial_Number = "Agency Card Serial Number";
    public static final String Rank = "Rank";
    public static final String Fs_Footer = "Fs Footer";
    public static final String Fv_Header = "Fv Header";
    // NID
    public static final String NationalId_Number = "National ID";
    public static final String Document_Number = "Document Number";
    public static final String Security_Number = "Security Number";
    public static final String Personal_Code = "Personal Code";
    public static final String Vaccine_Name = "Vaccine Name";
    public static final String Vaccination_Date = "Vaccination Date";
    public static final String Vaccination_Location_Name = "Vaccination Location Name";
    
    // VID
//    public static final String Elector_Key = "Clave De Elector";
//    public static final String Year_Of_Registration = "Año de Registro";
//    public static final String State = "Estado";
    public static final String Elector_Key = "Elector Key";
    public static final String Year_Of_Registration = "Year Of Registration";
    public static final String State = "State";
    public static final String Curp = "Curp";

    // PRC
    public static final String PRCId_Number = "Permanent Resident ID";
    public static final String Passport_Number = "Passport Number";
    public static final String Residentcard_Type_Number = "Residentcard Type Number";
    public static final String Residentcard_Type_Code = "Residentcard Type Code";
    // Health Service ID
    public static final String Primary_Subscriber = "Primary Subscriber";
    public static final String Id_Number = "Id Number";
    public static final String Primary_Subscriber_ID = "Primary Subscriber ID";
    public static final String Primary_Doctor = "Primary Doctor";
    public static final String Primary_Doctor_Phone = "Primary Doctor Phone";
    public static final String userHealthServiceVaccineInfos = "userHealthServiceVaccineInfos";
    public static final String Vaccine = "Vaccine";
    public static final String Route = "Route";
    public static final String Site = "Site";
    public static final String Date = "Date";
    public static final String Administered_By = "Administered By";
    
    // employee ID
    public static final String Employee_ID_Number = "Employee ID";
    public static final String Department = "Department";
    public static final String Issuer_IN = "Issuer IN";
    public static final String Agency_Card_Sn = "Agency Card Sn";
    // employee ID
    public static final String Student_ID_Number = "Student ID";
    public static final String Institution = "Institution";
    // health ID
    public static final String Health_ID_Number = "Health ID";
    public static final String Subscriber = "Subscriber";
    public static final String Subscriber_ID = "Subscriber ID";
    public static final String Pcp = "PCP";
    public static final String Pcp_Phone = "PCP Phone";
    public static final String Policy_Number = "Policy Number";
    public static final String Group_Plan = "Group Plan";
    public static final String Health_Plan_Number = "Health Plan Number";
    
    public static final String Vehicle_Classification = "Vehicle Classification";
    public static final String Veteran = "Veteran";
    public static final String Height ="Height";
    public static final String Hair_Color ="Hair Color";
    public static final String Eye_Color ="Eye Color";
    public static final String Blood_Type = "Blood Type";
    public static final String Rank_Grade_Employee_Status = "Rank/Grade/Employee Status";
    public static final String Employee_Affiliation = "Employee Affiliation";
    public static final String Employee_Affiliation_Color_Code = "Employee Affiliation Color Code";
    public static final String  Donor = "Donor";
    // Enrollment fields
    public static final String ID_PROOFING = "ID_PROOFING";
    public static final String MOBILE_ID_ONBOARDING_CONFIG = "MOBILE_ID_ONBOARDING_CONFIG";
    public static final String REGISTRATION = "REGISTRATION";
    public static final String FACE_CAPTURE = "FACE_CAPTURE";
    public static final String FINGERPRINT_CAPTURE = "FINGERPRINT_CAPTURE";
    public static final String APPROVAL_BEFORE_ISSUANCE = "APPROVAL_BEFORE_ISSUANCE";
    
    public static final String dropdown = "dropdown";
    public static final String date = "date";
    public static final String input = "input";
    public static final String checkbox = "checkbox";
    
    // Rest Constants
    public static final String LOGGED_IN_USER = "loggedInUser";
    public static final String UIMS_URL = "uimsUrl";
    public static final String EXPRESS_ENROLLMENT = "expressEnrollment";
    public static final String LOGGED_IN_USER_ID = "loggedInUserId";
    
    // Config Values
    public static final String TRANSPARENT_PHOTO_ALLOWED = "TRANSPARENT_PHOTO_ALLOWED";
     
    public static final String cropWidth = "width";
    public static final String cropHeight = "height";
    
    public static final String MOBILE_WALLET = "mobile_wallet";
    public static final String MOBILE_SERVICE = "mobile_service";
    
    public static final String portal = "PORTAL";
    public static final String server = "SERVER";    
    
    private Constants() {
    }
}
