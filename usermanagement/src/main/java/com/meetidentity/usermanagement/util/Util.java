package com.meetidentity.usermanagement.util;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.util.Base64;
import java.util.Date;
import java.util.Random;

import javax.imageio.ImageIO;

import org.springframework.stereotype.Component;

@Component
public class Util {

	public static SimpleDateFormat ddmmYYYY_format = new SimpleDateFormat("dd/MM/YYYY");
	public static SimpleDateFormat ddmmYYYY_format_HH_mm_ss = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
	public static SimpleDateFormat yyyy_MM_dd_HH_mm_ss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat DD_MMM_YYYY_format = new SimpleDateFormat("dd-MMM-YYYY");
	public static SimpleDateFormat DDMMYYYY_format = new SimpleDateFormat("dd/MM/YYYY");
	public static SimpleDateFormat YY_format = new SimpleDateFormat("YY");
	public static SimpleDateFormat MMDDYYYY_format = new SimpleDateFormat("MM/dd/YYYY");
	public static SimpleDateFormat YYYYMMMDD_format = new SimpleDateFormat("YYYYMMMdd");
	public static SimpleDateFormat MMMYYYY_format = new SimpleDateFormat("MMMYYYY");
	public static SimpleDateFormat MMM_YYYY_format = new SimpleDateFormat("MMM YYYY");
	public static SimpleDateFormat MMM_DD_YYYY_format = new SimpleDateFormat("MMM. dd, YYYY");
	public static SimpleDateFormat HH_MM_EEE_MMM_DD_YYYY_format = new SimpleDateFormat("HH:MM, EEE, MMM dd YYYY");
	
	// public static SimpleDateFormat ddmmCCYY_format = new
	// SimpleDateFormat("dd/MM/CCYY");

	public static String formatDate(Date date, SimpleDateFormat dateFormater) {

		if(date != null) {
			return dateFormater.format(date);
		}else {
			return "";
		}

	}

	public static String formatDate(Instant date, SimpleDateFormat dateFormater) {

		if(date != null) {
			return dateFormater.format(Date.from(date)).toUpperCase();
		}else {
			return "";
		}

	}
	
	public static String format(String message, Object... inserts) {

		for (Object obj : inserts) {
			if(obj != null)
			message = message.replaceFirst("<>", obj.toString());
		}

		return message;
	}
	
	public static int calculateAge(LocalDate birthDate, LocalDate currentDate) {
		return Period.between(birthDate, currentDate).getYears();
	}

	public static BufferedImage resizeImage(final Image image, int width, int height) {
        final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        final Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setComposite(AlphaComposite.Src);
        //below three lines are for RenderingHints for better image quality at cost of higher processing time
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.drawImage(image, 0, 0, width, height, null);
        graphics2D.dispose();
        return bufferedImage;
    }
	
	public static Image convertBase64ToImage(String base64Img) throws IOException {
		byte[] imageByte;
		imageByte = Base64.getDecoder().decode(base64Img.getBytes());
		ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
		Image img =  ImageIO.read(bis);
		bis.close();
		
		return img;

	}
	
	public static String convertBufferedImageToBase64(BufferedImage bufferedImage) throws IOException {
		 ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "png", bos);
		byte[] imageBytes = bos.toByteArray();
		bos.close();
		return Base64.getEncoder().encodeToString(imageBytes);
	}
	
	public static int get5DigitRandomNumber() {
	    // It will generate 5 digit random Number.
	    // from 0 to 99999
	    Random rnd = new Random();
	    int number = rnd.nextInt(99999);
	    return number;
	    // this will convert any number sequence into 5 character.
	   // return String.format("%05d", number);
	}
	
	public static int get8DigitRandomNumber() {
	    // It will generate 8 digit random Number.
	    // from 0 to 99999999
	    Random rnd = new Random();
	    int number = rnd.nextInt(99999999);
	    return number;
	    // this will convert any number sequence into 8 character.
	   // return String.format("%08d", number);
	}

	public static int get6DigitRandomNumber() {
	    // It will generate 6 digit random Number.
	    // from 0 to 999999
	    Random rnd = new Random();
	    int number = rnd.nextInt(999999);
	    return number;
	}
	
	public static String getAlphaNumericString(int n) {

		// length is bounded by 256 Character
		byte[] array = new byte[256];
		new Random().nextBytes(array);

		String randomString = new String(array, Charset.forName("UTF-8"));

		// Create a StringBuffer to store the result
		StringBuffer r = new StringBuffer();

		// Append first n alphanumeric characters
		// from the generated random String into the result
		for (int k = 0; k < randomString.length(); k++) {

			char ch = randomString.charAt(k);

			if (((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9')) && (n > 0)) {

				r.append(ch);
				n--;
			}
		}

		// return the resultant string
		return r.toString();
	}
}
