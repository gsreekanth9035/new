package com.meetidentity.usermanagement;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.annotation.PostConstruct;
	
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.MetricFilterAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.MetricRepositoryAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;

import com.meetidentity.usermanagement.config.ApplicationProperties;
import com.meetidentity.usermanagement.config.DefaultProfileUtil;

import io.github.jhipster.config.JHipsterConstants;
@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration(exclude = {MetricFilterAutoConfiguration.class, MetricRepositoryAutoConfiguration.class})
@EnableConfigurationProperties({LiquibaseProperties.class, ApplicationProperties.class})
@EnableDiscoveryClient
@EnableCaching
@EnableAsync
public class UsermanagementApp {

    private static final Logger log = LoggerFactory.getLogger(UsermanagementApp.class);

    private final Environment env;

    public UsermanagementApp(Environment env) {
        this.env = env;
    }

    /**
     * Initializes usermanagement.
     * <p>
     * Spring profiles can be configured with a program arguments --spring.profiles.active=your-active-profile
     * <p>
     * You can find more information on how profiles work with JHipster on <a href="http://www.jhipster.tech/profiles/">http://www.jhipster.tech/profiles/</a>.
     */
    @PostConstruct
    public void initApplication() {
        Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
        if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_PRODUCTION)) {
            log.error("You have misconfigured your application! It should not run " +
                "with both the 'dev' and 'prod' profiles at the same time.");
        }
        if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_CLOUD)) {
            log.error("You have misconfigured your application! It should not " +
                "run with both the 'dev' and 'cloud' profiles at the same time.");
        }
    }

    /**
     * Main method, used to run the application.
     *
     * @param args the command line arguments
     * @throws UnknownHostException if the local host name could not be resolved into an address
     */
    public static void main(String[] args) throws UnknownHostException {
        SpringApplication app = new SpringApplication(UsermanagementApp.class);
        DefaultProfileUtil.addDefaultProfile(app);
        Environment env = app.run(args).getEnvironment();
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        log.info("\n----------------------------------------------------------\n\t" +
                "Application '{}' is running! Access URLs:\n\t" +
                "Local: \t\t{}://localhost:{}\n\t" +
                "External: \t{}://{}:{}\n\t" +
                "Profile(s): \t{}\n----------------------------------------------------------",
            env.getProperty("spring.application.name"),
            protocol,
            env.getProperty("server.port"),
            protocol,
            InetAddress.getLocalHost().getHostAddress(),
            env.getProperty("server.port"),
            env.getActiveProfiles());
        log.info("test properties "+ env.getProperty("application.keycloakConfigurationList[0].client-secret"));

        String configServerStatus = env.getProperty("configserver.status");
        log.info("\n----------------------------------------------------------\n\t" +
                "Config Server: \t{}\n----------------------------------------------------------",
            configServerStatus == null ? "Not found or not setup for this application" : configServerStatus);
        
        log.info("Fetching Build information from Manifest Metadata...");
        InputStream manifestStream =  UsermanagementApp.class.getClassLoader().getResourceAsStream("META-INF/MANIFEST.MF");
        if (manifestStream != null) {
            Manifest manifest = null;
			try {
				manifest = new Manifest(manifestStream);
				Attributes attributes = manifest.getMainAttributes();
				log.info(
						"************************************ Build Information from Manifest Metadata ************************************");
				log.info("artifactId: " + attributes.getValue("artifactId"));
				log.info("version: " + attributes.getValue("version"));
				log.info("buildNumber: " + attributes.getValue("buildNumber"));
				log.info("commitId: " + attributes.getValue("commitId"));
				log.info("branch: " + attributes.getValue("branch"));
				log.info("prId: " + attributes.getValue("prId"));
				log.info("prDestinationBranch: " + attributes.getValue("prDestinationBranch"));
			} catch (IOException e) {
				log.error("Exception occured while fetching build information from Manifest Metadata: {}", e.getMessage());
			}
        }
    }
}
