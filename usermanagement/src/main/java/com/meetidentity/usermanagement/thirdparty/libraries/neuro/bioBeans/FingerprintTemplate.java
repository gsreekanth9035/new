package com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans;

public class FingerprintTemplate {
	
	public String fingerPosition;
	public String ansi;
	
	public String getFingerPosition() {
		return fingerPosition;
	}
	public void setFingerPosition(String fingerPosition) {
		this.fingerPosition = fingerPosition;
	}
	public String getAnsi() {
		return ansi;
	}
	public void setAnsi(String ansi) {
		this.ansi = ansi;
	}
}
