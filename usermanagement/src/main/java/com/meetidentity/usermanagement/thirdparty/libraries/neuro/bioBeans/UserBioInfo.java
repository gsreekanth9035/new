package com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans;

import java.io.Serializable;
import java.util.List;

public class UserBioInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long userID;
	
	private FaceModality faceModality;
    private List<FingerprintModality> fingerprintModalities;
    private List<IRISModality> irisModalities;
    
    private String requestIsFrom; //Is Web or Mobile Better to set Enum Value for this, Later we can modify based on requirements 
    
    boolean deDuplicationCheck=false;
    
	public Long getUserID() {
		return userID;
	}
	public void setUserID(Long userID) {
		this.userID = userID;
	}
	public List<FingerprintModality> getFingerprintModalities() {
		return fingerprintModalities;
	}
	public void setFingerprintModalities(List<FingerprintModality> fingerprintModalities) {
		this.fingerprintModalities = fingerprintModalities;
	}
	public boolean isDeDuplicationCheck() {
		return deDuplicationCheck;
	}
	public void setDeDuplicationCheck(boolean deDuplicationCheck) {
		this.deDuplicationCheck = deDuplicationCheck;
	}
	public FaceModality getFaceModality() {
		return faceModality;
	}
	public void setFaceModality(FaceModality faceModality) {
		this.faceModality = faceModality;
	}
	public String getRequestIsFrom() {
		return requestIsFrom;
	}
	public void setRequestIsFrom(String requestIsFrom) {
		this.requestIsFrom = requestIsFrom;
	}
	public List<IRISModality> getIrisModalities() {
		return irisModalities;
	}
	public void setIrisModalities(List<IRISModality> irisModalities) {
		this.irisModalities = irisModalities;
	} 	
}
