package com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans;

import java.io.Serializable;

public class IRISModality implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String irisData;
	private String irisPosition ;
	
	public String getIrisData() {
		return irisData;
	}
	public void setIrisData(String irisData) {
		this.irisData = irisData;
	}
	public String getIrisPosition() {
		return irisPosition;
	}
	public void setIrisPosition(String irisPosition) {
		this.irisPosition = irisPosition;
	} 	
}
