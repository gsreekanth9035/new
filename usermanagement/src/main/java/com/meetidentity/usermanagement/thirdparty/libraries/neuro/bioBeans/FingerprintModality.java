package com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans;

import java.io.Serializable;

public class FingerprintModality implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fingerInputData;
	private String fingerPosition ; // Ex NFPosition.LEFT_INDEX_FINGER
	private String imageType;
   
	public String getFingerInputData() {
		return fingerInputData;
	}
	public void setFingerInputData(String fingerInputData) {
		this.fingerInputData = fingerInputData;
	}
	
	public String getFingerPosition() {
		return fingerPosition;
	}
	public void setFingerPosition(String fingerPosition) {
		this.fingerPosition = fingerPosition;
	}
	public String getImageType() {
		return imageType;
	}
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}    
	
}
