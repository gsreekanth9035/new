package com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans;

import java.io.Serializable;

public class FaceModality implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//In DB its in Base64 format 
	private String faceImg;

	public String getFaceImg() {
		return faceImg;
	}

	public void setFaceImg(String faceImg) {
		this.faceImg = faceImg;
	}
	
	
	

	private String faceInputData;

	public String getFaceInputData() {
		return faceInputData;
	}

	public void setFaceInputData(String faceInputData) {
		this.faceInputData = faceInputData;
	}	
}
