package com.meetidentity.usermanagement.aop;

import java.lang.reflect.Field;

import javax.persistence.Entity;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.meetidentity.usermanagement.annotations.SequenceValue;
import com.meetidentity.usermanagement.domain.PushNotificationDeviceEntity;
import com.meetidentity.usermanagement.domain.SequenceNumberEntity;
import com.meetidentity.usermanagement.domain.UserDrivingLicenseEntity;
import com.meetidentity.usermanagement.domain.UserEmployeeIDEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserHealthIDEntity;
import com.meetidentity.usermanagement.domain.UserNationalIdEntity;
import com.meetidentity.usermanagement.domain.UserPermanentResidentIdEntity;
import com.meetidentity.usermanagement.domain.UserPersonalizedIDEntity;
import com.meetidentity.usermanagement.domain.UserPivEntity;
import com.meetidentity.usermanagement.domain.UserStudentIDEntity;
import com.meetidentity.usermanagement.domain.UserVoterIDEntity;
import com.meetidentity.usermanagement.repository.SequenceNumberRepository;
import com.meetidentity.usermanagement.web.rest.model.SequenceNumberModel;

@Configuration
@Aspect
public class SequenceAspect {
	Logger logger = LoggerFactory.getLogger(SequenceAspect.class);

	@Autowired
	private SequenceNumberRepository sequenceNumberRepository;
	
	@Autowired
	private HttpServletRequest request;

	@Before("execution(* org.springframework.data.jpa.repository.JpaRepository.save(..)) && args(entity)") // This is for JPA.
	public void generateSequence(JoinPoint joinPoint, Object entity) {

		logger.debug("Entered SequenceAspect.generateSequence()");
		try {

			if (entity instanceof UserEntity) {
				if (((UserEntity) entity).getUimsId() != null) {
					return;
				}
			} else if (entity instanceof UserDrivingLicenseEntity) {
				if (((UserDrivingLicenseEntity) entity).getLicenseNumber() != null) {
					return;
				}
			} else if (entity instanceof UserPivEntity) {
				if (((UserPivEntity) entity).getPivIdNumber() != null) {
					return;
				}
			} else if (entity instanceof UserNationalIdEntity) {
				if (((UserNationalIdEntity) entity).getPersonalCode() != null) {
					return;
				}
			} else if (entity instanceof UserVoterIDEntity) {
				if (((UserVoterIDEntity) entity).getCurp() != null) {
					return;
				}
			} else if (entity instanceof UserPermanentResidentIdEntity) {
				if (((UserPermanentResidentIdEntity) entity).getPersonalCode() != null) {
					return;
				}
			} else if (entity instanceof UserEmployeeIDEntity) {
				if (((UserEmployeeIDEntity) entity).getEmployeeIDNumber() != null) {
					return;
				}
			} else if (entity instanceof UserPersonalizedIDEntity) {
				if (((UserPersonalizedIDEntity) entity).getIdNumber() != null) {
					return;
				}
			} else if (entity instanceof UserStudentIDEntity) {
				if (((UserStudentIDEntity) entity).getStudentIdNumber() != null) {
					return;
				}
			} else if (entity instanceof UserHealthIDEntity) {
				if (((UserHealthIDEntity) entity).getIdNumber() != null) {
					return;
				}
			} else if (entity instanceof PushNotificationDeviceEntity) {
				if (((PushNotificationDeviceEntity) entity).getUimsId() != null) {
					return;
				}
			}

			updateSequenceValue(joinPoint);

		} finally {
			logger.debug("Exited SequenceAspect.generateSequence()");
		}
	}

	private void updateSequenceValue(JoinPoint joinPoint) {
		logger.info("Entered SequenceAspect.updateSequenceValue()");

		String organizationName = request.getHeader("organizationName");
		String orgName = organizationName != null ? organizationName : "demo";
		// TODO- get orgName from the context. default is "demo"

		Object[] aragumentList = joinPoint.getArgs(); // Getting all arguments of the save
		for (Object arg : aragumentList) {
			if (arg.getClass().isAnnotationPresent(Entity.class)) { // getting the Entity class
				Field[] fields = arg.getClass().getDeclaredFields();

				for (Field field : fields) {

					if (field.isAnnotationPresent(SequenceValue.class)) {
						field.setAccessible(true);

						SequenceNumberModel sequenceNumberModel = getSequenceNumber(arg.getClass().getSimpleName(),
								orgName);
						try {
							String prefix = sequenceNumberModel.getSeqPrefix() != null
									? sequenceNumberModel.getSeqPrefix()
									: StringUtils.EMPTY;
							String suffix = sequenceNumberModel.getSeqSuffix() != null
									? sequenceNumberModel.getSeqSuffix()
									: StringUtils.EMPTY;

							long seqCurrentValue = sequenceNumberModel.getCurrentValue();
							String updatedSequenceValue = String.valueOf(seqCurrentValue);

							int idLength = sequenceNumberModel.getIdLength();

							if ((prefix + seqCurrentValue + suffix).length() < idLength) {
								
								int leadingCharsLength = idLength - (prefix + suffix).length();

								updatedSequenceValue = String.format("%0" + leadingCharsLength + "d", seqCurrentValue);

							}

							field.set(arg, prefix + updatedSequenceValue + suffix);

						} catch (IllegalArgumentException e) {
							logger.debug(
									"Error while Setting the sequence value field, continue with the exisitng field value. field Name: "
											+ field.getName());
						} catch (IllegalAccessException e) {
							logger.debug(
									"Error while Setting the sequence value field, continue with the exisitng field value. field Name: "
											+ field.getName());
						}
					}

				}

			}

		}

		logger.info("Exited SequenceAspect.updateSequenceValue()");
	}

	private SequenceNumberModel getSequenceNumber(String p_className, String orgName) {

		logger.info("Entered SequenceAspect.getSequenceNumber()");

		SequenceNumberEntity sequenceNumberEntity = sequenceNumberRepository.findByOrgNameIgnoreCaseAndClassNameIgnoreCase(orgName,
				p_className);

		if (sequenceNumberEntity == null) {
			sequenceNumberEntity = new SequenceNumberEntity();
			sequenceNumberEntity.setClassName(p_className);
			sequenceNumberEntity.setOrgName(orgName);
			sequenceNumberEntity.setIdLength(11);
		}

		SequenceNumberModel sequenceNumberModel = new SequenceNumberModel();
		sequenceNumberModel.setCurrentValue(sequenceNumberEntity.getNextValue());
		sequenceNumberModel.setSeqPrefix(sequenceNumberEntity.getSeqPrefix());
		sequenceNumberModel.setSeqSuffix(sequenceNumberEntity.getSeqSuffix());
		sequenceNumberModel.setIdLength(sequenceNumberEntity.getIdLength());

		sequenceNumberEntity.setNextValue(sequenceNumberEntity.getNextValue() + sequenceNumberEntity.getIncrementValue());

		sequenceNumberRepository.save(sequenceNumberEntity);

		logger.info("Exited SequenceAspect.getSequenceNumber()");

		return sequenceNumberModel;
	}

}
