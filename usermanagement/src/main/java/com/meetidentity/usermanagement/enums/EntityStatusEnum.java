package com.meetidentity.usermanagement.enums;

public enum EntityStatusEnum {
		
	ACTIVE("ACTIVE"), 
	SUSPENDED("SUSPENDED"), 
	REVOKED("REVOKED"), 
	INACTIVE("INACTIVE"), 
	EXPIRED("EXPIRED");

	private String userStatus;

	EntityStatusEnum(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getUserStatus() {
		return userStatus;
	}
}
