package com.meetidentity.usermanagement.enums;

public enum RoleTypeEnum {

	ADMIN("ROLE_ADMIN"), OPERATOR("ROLE_OPERATOR"), ADJUDICATOR("ROLE_ADJUDICATOR"),USER("ROLE_USER"),MOBILEIDUSER("ROLE_MOBILEID_USER"),ANONYMOUS("ROLE_ANONYMOUS");
	
	
	private String userRole;

	RoleTypeEnum(String userRole) {
		this.userRole = userRole;
	}

	public String getUserRole() {
		return userRole;
	}	
}
