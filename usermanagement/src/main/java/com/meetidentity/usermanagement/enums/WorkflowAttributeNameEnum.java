package com.meetidentity.usermanagement.enums;

public enum WorkflowAttributeNameEnum {

	DEFAULT_WORKFLOW("DEFAULT_WORKFLOW");

	private String name;

	private WorkflowAttributeNameEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
