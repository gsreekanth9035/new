package com.meetidentity.usermanagement.enums;

public enum MobilePairPolicyConfigEnum {

	ENROLLMENT_TOKEN_EXPIRY("ENROLLMENT_TOKEN_EXPIRY"),
	PAIR_MOBILE_WALLET_INVITE_TOKEN_EXPIRY("PAIR_MOBILE_WALLET_INVITE_TOKEN_EXPIRY"),
	PAIR_MOBILE_SERVICE("PAIR_MOBILE_SERVICE");

	private String configName;

	MobilePairPolicyConfigEnum(String configName) {
		this.configName = configName;
	}

	public String getConfigName() {
		return configName;
	}
}
