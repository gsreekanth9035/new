package com.meetidentity.usermanagement.enums;

public enum DirectionEnum {
	ASC("asc"), DESC("desc");
	private final String directionCode;

	private DirectionEnum(String direction) {
		this.directionCode = direction;
	}

	public String getDirectionCode() {
		return this.directionCode;
	}
}