package com.meetidentity.usermanagement.enums;

public enum RoleAttributeNameEnum {

	DEFAULT_ROLE("DEFAULT_ROLE");

	private String name;

	private RoleAttributeNameEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
