package com.meetidentity.usermanagement.enums;

public enum UserStatusEnum {
	
	ENROLLMENT_IN_PROGRESS("Enrollment In Progress"), 
	PENDING_ENROLLMENT("Ready For Enrollment"), 
	READY_FOR_ISSUANCE("Ready For Issuance"),
	PENDING_APPROVAL("Pending Approval"), 
	IDENTITY_ISSUED("Identity Issued"),
	READY_FOR_REISSUANCE("Ready For Reissuance"), 
	ACTIVE("ACTIVE"), 
	SUSPENDED("SUSPENDED"), 
	REVOKED("REVOKED"), 
	INACTIVE("INACTIVE"), 
	EXPIRED("EXPIRED");

	private String userStatus;

	UserStatusEnum(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getUserStatus() {
		return userStatus;
	}
}
