package com.meetidentity.usermanagement.enums;

public enum CardTypeEnum {
	v8_1("ID-One PIV v 2.4.1 on Cosmo V8.1", "PIV"), v7("ID-One PIV v 2.3.4 on Cosmo V7", "PIV"), GnD_SEC7(
			"G+D SCE 7.0 with PIV Applet V 1.0",
			"PIV"), UnifyiaJcop3("NXP JCOP 3 with Unifyia PIV Applet v2.0", "PIV"), YubiKey5("YubiKey 5", "PIV"), MifareClassic1k(
					"Mifare Classic 1k", "Identity"),MifareClassic4k("Mifare Classic 4k", "Identity"), DesfireEV1_EV2("Desfire EV1/EV2", "Identity"), SmartAuth4("NXP JCOP 4 with Unifyia PIV Applet V 4.0", "PIV");
	private String productName;
	private String category;

	private CardTypeEnum(String productName, String category) {
		this.productName = productName;
		this.category = category;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
