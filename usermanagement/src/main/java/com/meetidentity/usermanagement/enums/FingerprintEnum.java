package com.meetidentity.usermanagement.enums;

public enum FingerprintEnum {
	LEFT_THUMB("Left Thumb"), LEFT_INDEX_FINGER("Left Index Finger"), LEFT_MIDDLE_FINGER("Left Middle Finger"), LEFT_RING_FINGER("Left Ring Finger"), LEFT_LITTLE_FINGER(
			"Left Little Finger"), RIGHT_THUMB("Right Thumb"), RIGHT_INDEX_FINGER("Right Index Finger"), RIGHT_MIDDLE_FINGER("Right Middle Finger"), RIGHT_RING_FINGER("Right Ring Finger"),RIGHT_LITTLE_FINGER("Right Little Finger");
	private String fingerPosition;

	FingerprintEnum(String fingerPosition) {
		this.fingerPosition = fingerPosition;
	}
	
	public String getFingerPosition() {
		return fingerPosition;
	}
}
