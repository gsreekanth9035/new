package com.meetidentity.usermanagement.enums;

public enum GroupAttributeNameEnum {

	DEFAULT_GROUP("DEFAULT_GROUP");

	private String name;

	private GroupAttributeNameEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
