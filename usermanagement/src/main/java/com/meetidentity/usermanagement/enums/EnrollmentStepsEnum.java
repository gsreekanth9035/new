package com.meetidentity.usermanagement.enums;

public enum EnrollmentStepsEnum {

	ID_PROOFING("ID_PROOFING"), INFORMATION_CAPTURE("INFORMATION_CAPTURE"), FACE_CAPTURE("FACE_CAPTURE"),
	IRIS_CAPTURE("IRIS_CAPTURE"), FINGERPRINT_CAPTURE("FINGERPRINT_CAPTURE"), FINGERPRINT_ROLL_CAPTURE("FINGERPRINT_ROLL_CAPTURE"),SIGNATURE_CAPTURE("SIGNATURE_CAPTURE"),
	ENROLL_SUMMARY("ENROLL_SUMMARY");

	private String enrollmentStep;

	private EnrollmentStepsEnum(String enrollmentStep) {
		this.enrollmentStep = enrollmentStep;
	}

	public String getEnrollmentStep() {
		return enrollmentStep;
	}

	public void setEnrollmentStep(String enrollmentStep) {
		this.enrollmentStep = enrollmentStep;
	}

}
