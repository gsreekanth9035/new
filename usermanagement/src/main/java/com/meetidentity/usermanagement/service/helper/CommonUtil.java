package com.meetidentity.usermanagement.service.helper;

/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */
// import com.sun.media.imageio.plugins.jpeg2000.J2KImageReadParam;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class CommonUtil {

    private final static char[] HEX =
            {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public String arrayToHex(byte[] data) {
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < data.length; i++) {
            String bs = Integer.toHexString(data[i] & 0xFF);
            if (bs.length() == 1) {
                sb.append(0);
            }
            sb.append(bs);
        }

        return sb.toString();
    }

    public String stringToHex(String str) {
        char[] chars = str.toCharArray();
        StringBuffer strBuffer = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            strBuffer.append(Integer.toHexString((int) chars[i]));

        }
        return strBuffer.toString();
    }

    public String stringPairToHex(String str) {
        String strpair;
        StringBuilder strBuffer = new StringBuilder();
        for (int i = 0; i < str.length(); i = i + 2) {
            strpair = Integer.toHexString(Integer.parseInt(str.substring(i, i + 2)));

            if (strpair.length() == 1) {
                strBuffer.append('0').append(strpair);
            } else {
                strBuffer.append(strpair);
            }
        }
        return strBuffer.toString();
    }

    public String intToHex(int intValue) {
        String strHex;
        StringBuilder strBuffer = new StringBuilder();

        strHex = Integer.toHexString(intValue);

        if (strHex.length() == 1) {
            strBuffer.append('0').append(strHex);
        } else {
            strBuffer.append(strHex);
        }

        return strBuffer.toString();
    }



    public byte[] hexStringToByteArray(String s, int pinLen) {
        int len = s.length();
        byte[] data;
        data = new byte[pinLen];


        for (int k = 0; k < pinLen; k++) {
            data[k] = (byte) 0xFF;
        }

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }



    /*
     * public byte[] hexStringToByteArray(String s) { int len = s.length(); byte[] data; data = new
     * byte[len/2];
     * 
     * 
     * for (int k = 0; k < len/2; k++) { data[k] = (byte) 0xFF; }
     * 
     * for (int i = 0; i < len; i += 2) { data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) <<
     * 4) + Character .digit(s.charAt(i + 1), 16)); }
     * 
     * return data; }
     * 
     */
    public byte[] hex1StringToByteArray(String s) {
        int len = s.length();
        byte[] data;
        data = new byte[len / 2];


        for (int k = 0; k < len / 2; k++) {
            data[k] = (byte) 0xFF;
        }

        for (int i = 0; i < len - 1; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }

    public byte[] hexToByteArray(String s) {
        int len = s.length();
        byte[] data;
        data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }

    public byte[] hex1ToByteArray(String s) {
        int len = s.length();
        byte[] data;
        data = new byte[len / 2];

        for (int i = 0; i < len - 1; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }

    public void writeByteArrayToFile(String sFileName, byte[] inByte) {

        String strFilePath = sFileName;
        try {
            FileOutputStream fos = new FileOutputStream(strFilePath);
            // String strContent = "Write File using Java FileOutputStream example !";

            // fos.write(strContent.getBytes());
            fos.write(inByte);
            fos.close();

        } catch (FileNotFoundException ex) {

            System.out.println("FileNotFoundException : " + ex);

        } catch (IOException ioe) {


            System.out.println("IOException : " + ioe);

        }
    }

    public byte[] combine_data(byte[] array1, byte[] array2) {

        byte[] combined = new byte[array1.length + array2.length];

        System.arraycopy(array1, 0, combined, 0, array1.length);
        System.arraycopy(array2, 0, combined, array1.length, array2.length);

        return combined;
    }


    public byte[] getEndBytes(byte[] array1, int len) {

        byte[] strip_byte = new byte[len];

        System.arraycopy(array1, array1.length - len, strip_byte, 0, len);

        return strip_byte;
    }



    public byte[] strip_data(byte[] array1, int len) {

        byte[] strip_byte = new byte[array1.length - len];
        System.arraycopy(array1, len, strip_byte, 0, array1.length - len);

        return strip_byte;
    }

    public byte[] strip_data_end(byte[] array1, int len) {

        // byte[] strip_byte = new byte[array1.length - len];
        byte[] strip_byte = new byte[len];

        System.arraycopy(array1, 0, strip_byte, 0, len);

        return strip_byte;
    }

    public byte[] extract_data(byte[] array1, int strt, int len) {

        byte[] extract_byte = new byte[len];
        System.arraycopy(array1, strt, extract_byte, 0, len);

        return extract_byte;
    }



    public int indexOf(byte[] data, byte[] pattern) {
        int[] failure = computeFailure(pattern);

        int j = 0;
        if (data.length == 0) {
            return -1;
        }

        for (int i = 0; i < data.length; i++) {
            while (j > 0 && pattern[j] != data[i]) {
                j = failure[j - 1];
            }
            if (pattern[j] == data[i]) {
                j++;
            }
            if (j == pattern.length) {
                return i - pattern.length + 1;
            }
        }
        return -1;
    }

    /**
     * Computes the failure function using a boot-strapping process, where the pattern is matched
     * against itself.
     */
    private int[] computeFailure(byte[] pattern) {
        int[] failure = new int[pattern.length];

        int j = 0;
        for (int i = 1; i < pattern.length; i++) {
            while (j > 0 && pattern[j] != pattern[i]) {
                j = failure[j - 1];
            }
            if (pattern[j] == pattern[i]) {
                j++;
            }
            failure[i] = j;
        }

        return failure;
    }

    public String formatHexString(int len, String input) {

        StringBuffer sb = new StringBuffer();
        sb = sb.append(input);

        int input_len = input.length();

        for (int i = input_len + 1; i <= len; i += 2) {
            sb = sb.append("20");
        }

        return sb.toString();

    }

    public String formatHexStringBefore(int len, String input) {

        StringBuffer sb = new StringBuffer();


        int input_len = input.length();

        for (int i = 1; i <= len - input_len; i += 2) {
            sb = sb.append("20");
        }
        sb = sb.append(input);
        return sb.toString();

    }

    public String hexToString(String hex) {

        StringBuilder sb = new StringBuilder();
        // StringBuilder temp = new StringBuilder();

        // 49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for (int i = 0; i < hex.length() - 1; i += 2) {

            // grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            // convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            // convert the decimal to character
            sb.append((char) decimal);

            // temp.append(decimal);
        }
        // System.out.println("Decimal : " + temp.toString());

        return sb.toString();
    }

    // public static long toLong(String hexadecimal) throws
    // NumberFormatException{
    // char[] chars;
    // char c;
    // long value;
    // int i;
    // byte b;
    // if (hexadecimal == null)
    // throw new IllegalArgumentException();
    // chars = hexadecimal.toUpperCase().toCharArray();
    // if (chars.length != 16)
    // throw new HexValueException("Incomplete hex value");
    // value = 0;
    // b = 0;
    // for (i = 0; i < 16; i++) {
    // c = chars[i];
    // if (c >= '0' && c <= '9') {
    // value = ((value << 4) | (0xff & (c - '0')));
    // } else if (c >= 'A' && c <= 'F') {
    // value = ((value << 4) | (0xff & (c - 'A' + 10)));
    // } else {
    // throw new NumberFormatException("Invalid hex character: " + c);
    // }
    // }
    // return value;
    // }
    public static String longToHex(long value) {
        char[] hexs;
        int i;
        int c;

        hexs = new char[16];
        for (i = 0; i < 16; i++) {
            c = (int) (value & 0xf);
            hexs[16 - i - 1] = HEX[c];
            value = value >> 4;
        }
        return new String(hexs);
    }

    public static int byteToInt(byte[] b) {
        int val = 0;
        for (int i = b.length - 1, j = 0; i >= 0; i--, j++) {
            val += (b[i] & 0xff) << (8 * j);
        }
        return val;
    }

    public static byte[] getBytes(Long val) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        dos.writeLong(val);
        return baos.toByteArray();
    }

    public int findtagLocation(int startloc, String ss, String tag) {
        int loca = -1;
        for (int i = startloc; i < ss.length(); i += 2) {
            if ((ss.substring(i, i + 2)).equalsIgnoreCase(tag)) {

                loca = i;
                break;
            }
        }
        return loca;
    }

    public int findtagLocation(int startloc, String ss, String tag, int offset) {
        // System.out.println("SS "+ss);
        // System.out.println("tag "+tag);
        int loca = -1;
        String subst;
        for (int i = startloc; i < ss.length(); i += 2) {
            if (ss.length() > i + offset) {
                subst = ss.substring(i, i + offset);
            } else {
                subst = ss.substring(i);
            }
            // System.out.println("substr "+subst+" tag "+tag);
            if (subst.equals(tag)) {

                loca = i;
                break;
            }
        }
        return loca;
    }

    public int findfldtagLocation(int startloc, String ss, String tag) {
        // System.out.println("SS "+ss);
        // System.out.println("tag "+tag);
        int loca = -1;
        String subst;
        for (int i = startloc; i < ss.length(); i += 2) {
            if (ss.length() > i + 12) {
                subst = ss.substring(i, i + 12);
            } else {
                subst = ss.substring(i);
            }
            // System.out.println("substr "+subst+" tag "+tag);
            if (subst.equals(tag)) {

                loca = i;
                break;
            }
        }
        return loca;
    }

    public int findrectagLocation(int startloc, String ss, String tag) {

        String subst;
        int loca = -1;
        for (int i = startloc; i < ss.length(); i += 2) {
            if (ss.length() > i + 16) {
                subst = ss.substring(i, i + 16);
            } else {
                subst = ss.substring(i);
            }
            if (subst.equals(tag)) {

                loca = i;
                break;
            }
        }
        return loca;
    }

    public String subStrVal(String stag, String etag, String shex) {
        String srtn = "";

        int spos = this.findtagLocation(0, shex, stag) + 2;

        int epos = this.findtagLocation(0, shex, etag);

        if (spos >= 2) {
            srtn = (epos < 0) ? shex.substring(spos) : shex.substring(spos, epos);
            srtn = this.hexToString(srtn);
        }
        return srtn;
    }

    public String subfldStrVal(String stag, String etag, String shex) {
        String srtn = "";

        int spos = this.findfldtagLocation(0, shex, stag) + 2;

        int epos = this.findfldtagLocation(0, shex, etag);
        // System.out.println(stag+" "+etag);
        // System.out.println("endo "+epos+"spos "+spos);
        // if(spos>=2 && spos<epos)
        if (spos >= 2) {
            srtn = (epos < 0) ? shex.substring(spos) : shex.substring(spos, epos);
            srtn = this.hexToString(srtn);
        }
        return srtn;
    }

    public String subStrVal(String stag, String etag, String shex, int offset) {
        String srtn = "";

        int spos = this.findrectagLocation(0, shex, stag) + 2;
        // int spos = this.findtagLocation(0,shex,stag,offset);
        int epos = this.findtagLocation(0, shex, etag, offset);
        // System.out.println("start tag "+stag);
        // System.out.println("End tag "+etag);
        // System.out.println("search string "+shex);
        // if(spos>=2 && spos<epos)
        if (spos >= 2) {
            srtn = (epos < 0) ? shex.substring(spos) : shex.substring(spos, epos);
            // srtn = this.hexToString(srtn);
        }
        return srtn;
    }

    public String subrecStrVal(String stag, String etag, String shex) {
        String srtn = "";

        // int spos = this.findrectagLocation(0,shex,stag)+2;
        int spos = this.findrectagLocation(0, shex, stag);
        int epos = this.findrectagLocation(0, shex, etag);

        // if(spos>=2 && spos<epos)
        if (spos >= 0) {
            srtn = (epos < 0) ? shex.substring(spos) : shex.substring(spos, epos);
            // srtn = this.hexToString(srtn);
        }
        return srtn;
    }

    public static ImageIcon addPic(byte[] bytearray, int width, int height) throws IOException {
        // BufferedImage img = ImageIO.read(new
        // File("c:\\RValidemo\\RValideDemo\\images\\personal.jpg"));

        // old code
        InputStream in = new ByteArrayInputStream(bytearray);
        BufferedImage image = ImageIO.read(in);
        // old code

        // InputStream in = new ByteArrayInputStream(bytearray);
        // ImageInputStream in = ImageIO.createImageInputStream(bytearray);
        // ImageReader reader = ImageIO.getImageReaders(in).next();

        // ImageInputStream in = ImageIO.createImageInputStream(new
        // File("c:\\Rvalide\\alka.jpg"));
        // ImageReader reader = ImageIO.getImageReaders(in).next();

        // System.out.println("Test "+ reader);

        // Image image = null;

        // BufferedImage image = ImageIO.read("c:\\Rvalide\\alka.jpg");
        // File file = new File("c:\\Rvalide\\alka.jpg");
        // BufferedImage oldimage = ImageIO.read(file);

        // System.out.println("Test_2 " );

        // File outputFile = new File("c:\\Rvalide\\abc");

        // ImageIO.write(oldimage, "Jpeg2000",outputFile);

        // J2KImageReadParam param = new J2KImageReadParam();
        // param.setResolution(5);

        // System.out.println("Test_2 " + param);

        // BufferedImage image = reader.read(0, param);

        // System.out.println("Test_3 " + oldimage);

        // BufferedImage image = ImageIO.read(outputFile);

        // ImageIcon myIcon=new ImageIcon(image);

        /*
         * File file = new File("image.gif"); image = ImageIO.read(file);
         *
         *
         *
         * ImageInputStream in = ImageIO.createImageInputStream(new File("test.jp2")); ImageReader
         * reader = ImageIO.getImageReaders(in).next();
         *
         *
         * BufferedImage scaledImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
         *
         * Graphics2D graphics2D = scaledImage.createGraphics();
         * graphics2D.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION,
         * RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED); graphics2D.drawImage(image, 0, 0, width,
         * height, null); graphics2D.dispose();
         */
        ImageIcon myIcon = CommonUtil.addPic(image, width, height);
        return myIcon;

    }

    public static ImageIcon addPic(BufferedImage image, int width, int height) throws IOException {
        // BufferedImage img = ImageIO.read(new
        // File("c:\\RValidemo\\RValideDemo\\images\\personal.jpg"));

        // InputStream in = new ByteArrayInputStream(bytearray);

        BufferedImage scaledImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        Graphics2D graphics2D = scaledImage.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION,
                RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
        graphics2D.drawImage(image, 0, 0, width, height, null);
        graphics2D.dispose();

        ImageIcon myIcon = new ImageIcon(scaledImage);
        return myIcon;

    }

    public byte[] getBytesFromFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        // Get the size of the file
        long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        System.out.println("MAX Size " + Integer.MAX_VALUE);
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        // Close the input stream and return bytes
        is.close();
        return bytes;

    }

    public String findfldhextag(String hexstr, String stag, int startloc, int offset) {
        String[] sfldytype = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09"};
        String[] reclevels = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09"};
        String rtntag = "";
        int foundloc = -1;
        String hexsty;
        String hexreclev;
        // for(String sty:sfldytype )
        for (String reclev : reclevels) {

            if (foundloc == -1) {
                // for(String reclev:reclevels)
                for (String sty : sfldytype) {
                    hexreclev = this.stringToHex(reclev);
                    hexsty = this.stringToHex(sty);
                    // System.out.println("TESTio");
                    rtntag = hexreclev + stag + hexsty;

                    foundloc = findfldtagLocation(startloc, hexstr, rtntag);
                    if (foundloc != -1) {
                        break;
                    }

                }
            }
            if (foundloc != -1) {
                break;
            }

        }
        return rtntag;
    }

    // /////////////////////
    public String findrechextag(String hexstr, String stag, int startloc, int offset) {
        String[] sfldytype = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09"};

        String rtntag = "";
        int foundloc = -1;
        String hexsty;

        for (String sty : sfldytype) {
            hexsty = this.stringToHex(sty);
            rtntag = stag + hexsty;
            if (offset == 16) {
                foundloc = findrectagLocation(startloc, hexstr, rtntag);
            } else {
                foundloc = findfldtagLocation(startloc, hexstr, rtntag);
            }
            if (foundloc != -1) {
                break;
            }

        }

        return rtntag;
    }

    // /////////////////////
    // ////////////////////////////////
    public byte[] converttoBCD(int num_input) {

        byte[] result = new byte[5];

        switch (num_input) {
            case 0:
                result[0] = 0;
                result[1] = 0;
                result[2] = 0;
                result[3] = 0;
                result[4] = 1;
                break;
            case 1:
                result[0] = 1;
                result[1] = 0;
                result[2] = 0;
                result[3] = 0;
                result[4] = 0;
                break;
            case 2:
                result[0] = 0;
                result[1] = 1;
                result[2] = 0;
                result[3] = 0;
                result[4] = 0;
                break;
            case 3:
                result[0] = 1;
                result[1] = 1;
                result[2] = 0;
                result[3] = 0;
                result[4] = 1;
                break;
            case 4:
                result[0] = 0;
                result[1] = 0;
                result[2] = 1;
                result[3] = 0;
                result[4] = 0;
                break;
            case 5:
                result[0] = 1;
                result[1] = 0;
                result[2] = 1;
                result[3] = 0;
                result[4] = 1;
                break;
            case 6:
                result[0] = 0;
                result[1] = 1;
                result[2] = 1;
                result[3] = 0;
                result[4] = 1;
                break;
            case 7:
                result[0] = 1;
                result[1] = 1;
                result[2] = 1;
                result[3] = 0;
                result[4] = 0;
                break;
            case 8:
                result[0] = 0;
                result[1] = 0;
                result[2] = 0;
                result[3] = 1;
                result[4] = 0;
                break;
            case 9:
                result[0] = 1;
                result[1] = 0;
                result[2] = 0;
                result[3] = 1;
                result[4] = 1;
                break;
            case 10: // Starting FASCAN
                result[0] = 1;
                result[1] = 1;
                result[2] = 0;
                result[3] = 1;
                result[4] = 0;
                break;
            case 11: // Field Separator
                result[0] = 1;
                result[1] = 0;
                result[2] = 1;
                result[3] = 1;
                result[4] = 0;
                break;
            case 12: // End of FASCAN
                result[0] = 1;
                result[1] = 1;
                result[2] = 1;
                result[3] = 1;
                result[4] = 1;
                break;
        }
        return result;


    }

    public String getFASCN(String agency_cde, String system_cde, String cred_nbr, String str_cs,
            String str_ici, String pi_iden) {
        String str_hex_ret = null;

        // StringBuffer result = new StringBuffer();

        byte[] result;
        byte[] lrc = new byte[5];
        byte[] temp_result = new byte[5];

        String str_temp = null;
        // First of all get the FASCAN Starting BCD CODE.
        result = converttoBCD(10);

        // Store the result in for lrc computation.
        for (int i = 0; i < result.length; i++) {

            lrc[i] = result[i];
        }


        for (int i = 0; i < 6; i++) {

            if (i == 0) {
                str_temp = agency_cde;
            }
            if (i == 1) {
                str_temp = system_cde;
            }
            if (i == 2) {
                str_temp = cred_nbr;
            }
            if (i == 3) {
                str_temp = str_cs;
            }
            if (i == 4) {
                str_temp = str_ici;
            }
            if (i == 5) {
                str_temp = pi_iden;
            }


            // Now Run the loop for all the fields.
            for (int ii = 0; ii < str_temp.length(); ii++) {
                Character c = new Character(str_temp.charAt(ii));
                String s = c.toString();

                temp_result = converttoBCD(Integer.parseInt(s));
                result = combine_data(result, temp_result);

                for (int a = 0; a < temp_result.length; a++) {
                    lrc[a] = (byte) (lrc[a] ^ temp_result[a]);
                }

            }

            // Now add the field separator BCD CODE.
            if (i != 5) // Do not add field separator after the last field value.
            {
                temp_result = converttoBCD(11);
                result = combine_data(result, temp_result);

                for (int a = 0; a < temp_result.length; a++) {
                    lrc[a] = (byte) (lrc[a] ^ temp_result[a]);
                }

            }
        }


        // Now add the Ending field BCD CODE.
        temp_result = converttoBCD(12);
        result = combine_data(result, temp_result);

        for (int a = 0; a < temp_result.length; a++) {
            lrc[a] = (byte) (lrc[a] ^ temp_result[a]);
        }


        // Now compute LRC bit on LRC byte.
        byte[] lrc_bit = new byte[1];
        byte[] lrc_temp = new byte[4];
        byte[] lrc_final = new byte[5];

        for (int i = 0; i < lrc.length - 1; i++) {

            if (i == 0) {
                lrc_bit[0] = lrc[i];

            } else {
                lrc_bit[0] = (byte) (lrc_bit[0] ^ lrc[i]);
            }
        }

        // Finally XOR with 1.
        lrc_bit[0] = (byte) (lrc_bit[0] ^ 1);
        lrc_temp = strip_data_end(lrc, 4);
        lrc_final = combine_data(lrc_temp, lrc_bit);

        result = combine_data(result, lrc_final);

        // Now convert into String
        StringBuilder str_result_temp = new StringBuilder();
        for (int i = 0; i < result.length; i++) {

            str_result_temp.append(result[i]);
        }


        StringBuilder finalResult = new StringBuilder();
        for (int x = 0; x < str_result_temp.length(); x = x + 8) {

            String hex = str_result_temp.substring(x, x + 8);
            int i = Integer.parseInt(hex, 2);
            String hexString = Integer.toHexString(i);

            if (hexString.length() < 2) {
                hexString = "0" + hexString;
            }
            finalResult.append(hexString);

        }


        str_hex_ret = finalResult.toString();

        return str_hex_ret;


    }

    public String genUUID() {

        UUID uuid = UUID.randomUUID();
        String str_hex = String.valueOf(uuid);

        // 6e5c4ab7-94e3-4823-a7ea-1d71e1ef3a08
        String str_hex1 = str_hex.substring(0, 8);
        String str_hex2 = str_hex.substring(9, 13);
        String str_hex3 = str_hex.substring(14, 18);
        String str_hex4 = str_hex.substring(19, 23);
        String str_hex5 = str_hex.substring(24, 36);

        str_hex = str_hex1 + str_hex2 + str_hex3 + str_hex4 + str_hex5;

        // System.out.println("final_result" + str_hex );

        return str_hex;

    }

    public String[] parseFASCN(String infascn) {
        // d4e739da739ced39ce739da1685a1082108ce73984119257fc
        // String hex = "d4e739da739ced39ce739da1685a1082108ce73984119257fc";
        String bin = "";
        String binFragment = "";
        int iHex;
        infascn = infascn.trim();
        infascn = infascn.replaceFirst("0x", "");

        for (int i = 0; i < infascn.length(); i++) {
            iHex = Integer.parseInt("" + infascn.charAt(i), 16);
            binFragment = Integer.toBinaryString(iHex);

            while (binFragment.length() < 4) {
                binFragment = "0" + binFragment;
            }


            bin += binFragment;
        }
        String miscfascn = "";


        String[] fascn = new String[6];
        Integer i = 0;
        String partFascn = "";
        while (bin.length() > 0) {
            miscfascn = bin.substring(0, 5);
            bin = bin.substring(5);
            if (miscfascn.equalsIgnoreCase("11010")) {
                partFascn = "";
            } else if (miscfascn.equalsIgnoreCase("10110")) {

                fascn[i] = partFascn;
                partFascn = "";
                i++;
            } else if (miscfascn.equalsIgnoreCase("11111")) {
                fascn[i] = partFascn;
                partFascn = "";
                i++;
                break;
            } else {
                partFascn = partFascn + convertFascn(miscfascn);
            }


        }
        return fascn;// fascn.substring(0, fascn.length() - 1);
    }

    public String convertFascn(String binValue) {
        String retValue = "";

        if (binValue.matches("00001")) {
            retValue = "0";
        } else if (binValue.matches("10000")) {
            retValue = "1";
        } else if (binValue.matches("01000")) {
            retValue = "2";
        } else if (binValue.matches("11001")) {
            retValue = "3";
        } else if (binValue.matches("00100")) {
            retValue = "4";
        } else if (binValue.matches("10101")) {
            retValue = "5";
        } else if (binValue.matches("01101")) {
            retValue = "6";
        } else if (binValue.matches("11100")) {
            retValue = "7";
        } else if (binValue.matches("00010")) {
            retValue = "8";
        } else if (binValue.matches("10011")) {
            retValue = "9";
        }
        return retValue;

    }

    public byte[] combine_data(byte[][] arrays) {
        CommonUtil util = new CommonUtil();
        System.out.println(arrays.length);

        // Calculate length of combined byte arrays length.
        int len = 0;
        for (int i = 0; i < arrays.length; i++) {

            len = len + arrays[i].length;

        }

        System.out.println("Total Length " + len);


        byte[] combined = new byte[len];
        int start_length = 0;
        for (int i = 0; i < arrays.length; i++) {


            if (i == 0) {
                start_length = 0;
            } else {
                start_length = start_length + arrays[i - 1].length;
            }

            System.arraycopy(arrays[i], 0, combined, start_length, arrays[i].length);

        }

        System.out.println("combined array " + util.arrayToHex(combined));
        return combined;

    }

    public byte[] stringtoByte(String strVal) {

        return this.hex1ToByteArray(this.stringToHex(strVal));


    }

    public int findEndLocation(String str_hex1, String str_hex2) {

        int i_end;

        if (str_hex1 == null || str_hex2 == null || str_hex2.equals("") || str_hex1.equals("")) {
            return 0;
        }


        Pattern p = Pattern.compile(str_hex2, Pattern.CASE_INSENSITIVE + Pattern.LITERAL);
        Matcher m = p.matcher(str_hex1);

        if (m.find()) {
            i_end = m.end();

        } else {
            i_end = 0;
        }

        return i_end;

    }

    public int findFromEnd(String str_hex1, String str_hex2) {

        int i_prev_end = 0;

        int index = str_hex1.indexOf(str_hex2);
        if (str_hex1 == null || str_hex2 == null || str_hex2.equals("") || str_hex1.equals("")
                || index <= 0) {
            return 0;
        }

        while (index >= 0) {
            i_prev_end = index;
            index = str_hex1.indexOf(str_hex2, index + str_hex2.length());
        }


        return i_prev_end + str_hex2.length();

    }


    public byte[] calcBertValue(Integer len, boolean binclude_bert, boolean bsize) {
        // Determine value of Bert Tag
        String str_hex_size;
        StringBuilder tot_hex_size = new StringBuilder();
        byte[] bert_tag = null;
        byte[] num_bytes;
        byte[] bert_tag_size_vsmall = {0x53}; // identifier for size of data bytes
        byte[] bert_tag_size_small = {0x53, (byte) 0x81}; // identifier for size of data bytes
        byte[] bert_tag_size_big = {0x53, (byte) 0x82}; // identifier for size of data bytes
        byte[] len_tag_small = {(byte) 0x81};
        byte[] len_tag_big = {(byte) 0x82};

        if (len > 255) {
            if (binclude_bert) {
                bert_tag = bert_tag_size_big;
            } else {
                bert_tag = len_tag_big;
            }

        } else if (len > 127 && len < 256) {
            if (binclude_bert) {
                bert_tag = bert_tag_size_small;
            } else {
                bert_tag = len_tag_small;
            }

        } else {
            if (binclude_bert) {
                bert_tag = bert_tag_size_vsmall;
            }

        }

        // Now calculate actual data size being stored.
        // Following condition makes sure that converted hex string is always
        // even.

        str_hex_size = Integer.toHexString(len);

        if (str_hex_size.length() == 1 || str_hex_size.length() == 3) {
            tot_hex_size.append('0').append(str_hex_size);
        } else {
            tot_hex_size.append(str_hex_size);
        }


        if (!bsize) {
            if (bert_tag != null) {
                num_bytes = combine_data(bert_tag, hex1ToByteArray(tot_hex_size.toString()));
            } else {
                num_bytes = hex1ToByteArray(tot_hex_size.toString());
            }

        } else {
            num_bytes = hex1ToByteArray(tot_hex_size.toString());

        }

        return num_bytes;

    }

    public int[] stringToIntArray(String inString) {

        String[] parts = inString.split("\\.");
        int[] intArray = new int[parts.length];
        for (int j = 0; j < parts.length; j++) {
            intArray[j] = Integer.parseInt(parts[j]);
        }
        return intArray;

    }


    public byte[] byteIncrement(byte[] array, int index) {
        byte[] MaxValue = {(byte) 0xFF};
        // int index=array.length;

        if ((array[index] == MaxValue[0])) {
            array[index] = 0;

            if (index > 0)
                byteIncrement(array, index - 1);
        } else {
            array[index]++;
        }

        return array;

    }

}
