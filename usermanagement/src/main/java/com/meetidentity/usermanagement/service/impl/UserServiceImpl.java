package com.meetidentity.usermanagement.service.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.imageio.ImageIO;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.Hibernate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.config.ApplicationProperties;
import com.meetidentity.usermanagement.config.Constants;
import com.meetidentity.usermanagement.domain.APIGatewayInfoEntity;
import com.meetidentity.usermanagement.domain.DeviceProfileConfigEntity;
import com.meetidentity.usermanagement.domain.DeviceProfileEntity;
import com.meetidentity.usermanagement.domain.DrivingLicenseRestrictionEntity;
import com.meetidentity.usermanagement.domain.EnrollmentStepsStatusEntity;
import com.meetidentity.usermanagement.domain.GroupAttributeEntity;
import com.meetidentity.usermanagement.domain.IDProofIssuingAuthorityEntity;
import com.meetidentity.usermanagement.domain.IDProofTypeEntity;
import com.meetidentity.usermanagement.domain.IdentityTypeEntity;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.OrganizationIdentitiesEntity;
import com.meetidentity.usermanagement.domain.PairMobileDeviceConfigEntity;
import com.meetidentity.usermanagement.domain.RoleAttributeEntity;
import com.meetidentity.usermanagement.domain.RoleEntity;
import com.meetidentity.usermanagement.domain.UserAddressEntity;
import com.meetidentity.usermanagement.domain.UserAppearanceEntity;
import com.meetidentity.usermanagement.domain.UserAttributeEntity;
import com.meetidentity.usermanagement.domain.UserBiometricAttributeEntity;
import com.meetidentity.usermanagement.domain.UserBiometricEntity;
import com.meetidentity.usermanagement.domain.UserBiometricSkipEntity;
import com.meetidentity.usermanagement.domain.UserDrivingLicenseEntity;
import com.meetidentity.usermanagement.domain.UserEmployeeIDEntity;
import com.meetidentity.usermanagement.domain.UserEnrollmentHistoryEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserGroupsEntity;
import com.meetidentity.usermanagement.domain.UserHealthIDEntity;
import com.meetidentity.usermanagement.domain.UserHealthServiceEntity;
import com.meetidentity.usermanagement.domain.UserHealthServiceVaccineInfoEntity;
import com.meetidentity.usermanagement.domain.UserIDProofDocumentEntity;
import com.meetidentity.usermanagement.domain.UserIdentificationCodeEntity;
import com.meetidentity.usermanagement.domain.UserLocationEntity;
import com.meetidentity.usermanagement.domain.UserNationalIdEntity;
import com.meetidentity.usermanagement.domain.UserOtpEntity;
import com.meetidentity.usermanagement.domain.UserPermanentResidentIdEntity;
import com.meetidentity.usermanagement.domain.UserPersonalizedIDEntity;
import com.meetidentity.usermanagement.domain.UserPivEntity;
import com.meetidentity.usermanagement.domain.UserStudentIDEntity;
import com.meetidentity.usermanagement.domain.UserVoterIDEntity;
import com.meetidentity.usermanagement.domain.VehicleClassificationEntity;
import com.meetidentity.usermanagement.domain.WorkflowEntity;
import com.meetidentity.usermanagement.domain.express.enroll.Address;
import com.meetidentity.usermanagement.domain.express.enroll.ExpressEnrollmentSchema;
import com.meetidentity.usermanagement.domain.express.enroll.Image;
import com.meetidentity.usermanagement.domain.express.enroll.Item;
import com.meetidentity.usermanagement.enums.DirectionEnum;
import com.meetidentity.usermanagement.enums.EnrollmentStepsEnum;
import com.meetidentity.usermanagement.enums.FingerprintEnum;
import com.meetidentity.usermanagement.enums.GroupAttributeNameEnum;
import com.meetidentity.usermanagement.enums.RoleAttributeNameEnum;
import com.meetidentity.usermanagement.enums.RoleTypeEnum;
import com.meetidentity.usermanagement.enums.UserStatusEnum;
import com.meetidentity.usermanagement.exception.EnrollmentServiceException;
import com.meetidentity.usermanagement.exception.EntityCreateException;
import com.meetidentity.usermanagement.exception.EntityNotFoundException;
import com.meetidentity.usermanagement.exception.EntityUpdateException;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.InconsistentDataException;
import com.meetidentity.usermanagement.exception.PairDeviceInfoServiceException;
import com.meetidentity.usermanagement.exception.UIMSException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.exception.security.CryptoOperationException;
import com.meetidentity.usermanagement.keycloak.domain.ApprovalAttributes;
import com.meetidentity.usermanagement.keycloak.domain.GroupRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.RoleRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.StatsAttributes;
import com.meetidentity.usermanagement.keycloak.domain.UserPendingActions;
import com.meetidentity.usermanagement.keycloak.domain.UserRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.UserStats;
import com.meetidentity.usermanagement.repository.APIGatewayInfoRepository;
import com.meetidentity.usermanagement.repository.DeviceProfileConfigRepository;
import com.meetidentity.usermanagement.repository.DeviceProfileRepository;
import com.meetidentity.usermanagement.repository.DrivingLicenseRestrictionsRepository;
import com.meetidentity.usermanagement.repository.EnrollmentStepStatusRepository;
import com.meetidentity.usermanagement.repository.IDProofIssuingAuthorityRepository;
import com.meetidentity.usermanagement.repository.IDProofTypeRepository;
import com.meetidentity.usermanagement.repository.IdentityTypeRepository;
import com.meetidentity.usermanagement.repository.OrganizationIdentitiesRepository;
import com.meetidentity.usermanagement.repository.OrganizationRepository;
import com.meetidentity.usermanagement.repository.PairMobileDeviceConfigRepository;
import com.meetidentity.usermanagement.repository.RoleRepository;
import com.meetidentity.usermanagement.repository.UserAddressRepository;
import com.meetidentity.usermanagement.repository.UserAppearanceRepository;
import com.meetidentity.usermanagement.repository.UserAttributeRepository;
import com.meetidentity.usermanagement.repository.UserBiometricAttributeRepository;
import com.meetidentity.usermanagement.repository.UserBiometricRepository;
import com.meetidentity.usermanagement.repository.UserBiometricSkipRepository;
import com.meetidentity.usermanagement.repository.UserDrivingLicenseRepository;
import com.meetidentity.usermanagement.repository.UserEmployeeIDRepository;
import com.meetidentity.usermanagement.repository.UserEnrollmentHistoryRepository;
import com.meetidentity.usermanagement.repository.UserHealthIDRepository;
import com.meetidentity.usermanagement.repository.UserHealthServiceRepository;
import com.meetidentity.usermanagement.repository.UserIDProofDocumentRepository;
import com.meetidentity.usermanagement.repository.UserIdentificationCodeRepository;
import com.meetidentity.usermanagement.repository.UserLocationRepository;
import com.meetidentity.usermanagement.repository.UserNationalIDRepository;
import com.meetidentity.usermanagement.repository.UserOtpRepository;
import com.meetidentity.usermanagement.repository.UserPermanentResidentIdRepository;
import com.meetidentity.usermanagement.repository.UserPersonalizedIDRepository;
import com.meetidentity.usermanagement.repository.UserPivRepository;
import com.meetidentity.usermanagement.repository.UserRepository;
import com.meetidentity.usermanagement.repository.UserStudentIDRepository;
import com.meetidentity.usermanagement.repository.UserVoterIDRepository;
import com.meetidentity.usermanagement.repository.VehicleClassificationRepository;
import com.meetidentity.usermanagement.repository.WFStepRepository;
import com.meetidentity.usermanagement.repository.WorkflowRepository;
import com.meetidentity.usermanagement.repository.specification.UserSpecification;
import com.meetidentity.usermanagement.security.CryptoFunction;
import com.meetidentity.usermanagement.service.DrivingLicenseService;
import com.meetidentity.usermanagement.service.IDProofService;
import com.meetidentity.usermanagement.service.IdentityProviderUserService;
import com.meetidentity.usermanagement.service.KeycloakService;
import com.meetidentity.usermanagement.service.OrganizationService;
import com.meetidentity.usermanagement.service.UserCredentialsService;
import com.meetidentity.usermanagement.service.UserPairDeviceInfoService;
import com.meetidentity.usermanagement.service.UserService;
import com.meetidentity.usermanagement.service.VehicleService;
import com.meetidentity.usermanagement.service.WorkflowService;
import com.meetidentity.usermanagement.service.helper.BackgroundColorTransparency;
import com.meetidentity.usermanagement.service.helper.SecretKeyLength;
import com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans.FaceModality;
import com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans.FingerprintModality;
import com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans.FingerprintTemplate;
import com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans.IRISModality;
import com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans.UserBioInfo;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.feign.client.ApduFeignClient;
import com.meetidentity.usermanagement.web.feign.client.BiometricIdentityFeignClient;
import com.meetidentity.usermanagement.web.rest.model.AuthResponse;
import com.meetidentity.usermanagement.web.rest.model.AuthenticatorClientInfo;
import com.meetidentity.usermanagement.web.rest.model.BiometricVerificationResponse;
import com.meetidentity.usermanagement.web.rest.model.ConfigValue;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfile;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfileConfig;
import com.meetidentity.usermanagement.web.rest.model.EnableSoftOTP2FA;
import com.meetidentity.usermanagement.web.rest.model.EnrollmentStepsStatus;
import com.meetidentity.usermanagement.web.rest.model.ExpressEnrollForm;
import com.meetidentity.usermanagement.web.rest.model.ExpressEnrollmentResponse;
import com.meetidentity.usermanagement.web.rest.model.Group;
import com.meetidentity.usermanagement.web.rest.model.IDProofIssuingAuthority;
import com.meetidentity.usermanagement.web.rest.model.IdentityProviderUser;
import com.meetidentity.usermanagement.web.rest.model.MobileCredential;
import com.meetidentity.usermanagement.web.rest.model.OrganizationIdentity;
import com.meetidentity.usermanagement.web.rest.model.PairMobileDeviceConfig;
import com.meetidentity.usermanagement.web.rest.model.RegistrationConfigDetails;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.Role;
import com.meetidentity.usermanagement.web.rest.model.SearchUserResult;
import com.meetidentity.usermanagement.web.rest.model.TokenDetails;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAddress;
import com.meetidentity.usermanagement.web.rest.model.UserAppearance;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserAvailability;
import com.meetidentity.usermanagement.web.rest.model.UserBiometric;
import com.meetidentity.usermanagement.web.rest.model.UserBiometricAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserBiometricSkip;
import com.meetidentity.usermanagement.web.rest.model.UserDetails;
import com.meetidentity.usermanagement.web.rest.model.UserDevice;
import com.meetidentity.usermanagement.web.rest.model.UserDrivingLicense;
import com.meetidentity.usermanagement.web.rest.model.UserEmployeeID;
import com.meetidentity.usermanagement.web.rest.model.UserEnrollmentHistory;
import com.meetidentity.usermanagement.web.rest.model.UserHealthID;
import com.meetidentity.usermanagement.web.rest.model.UserHealthService;
import com.meetidentity.usermanagement.web.rest.model.UserHealthServiceVaccineInfo;
import com.meetidentity.usermanagement.web.rest.model.UserIDProofDocument;
import com.meetidentity.usermanagement.web.rest.model.UserIdNamePair;
import com.meetidentity.usermanagement.web.rest.model.UserIssuanceStatus;
import com.meetidentity.usermanagement.web.rest.model.UserNationalID;
import com.meetidentity.usermanagement.web.rest.model.UserPIVModel;
import com.meetidentity.usermanagement.web.rest.model.UserPermanentResidentID;
import com.meetidentity.usermanagement.web.rest.model.UserPersonalizedID;
import com.meetidentity.usermanagement.web.rest.model.UserRequest;
import com.meetidentity.usermanagement.web.rest.model.UserRolesAndGroups;
import com.meetidentity.usermanagement.web.rest.model.UserStudentID;
import com.meetidentity.usermanagement.web.rest.model.UserVoterID;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIDStepAdjudicationConfig;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIDStepCertConfig;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIDStepFaceVerificationConfig;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIDStepIdentityConfig;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIDStepOnboardAndEnrollConfig;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIDStepOnboardConfig;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIdStepCertificate;
import com.meetidentity.usermanagement.web.rest.model.WFStepApprovalGroupConfig;
import com.meetidentity.usermanagement.web.rest.model.WFStepGenericConfig;
import com.meetidentity.usermanagement.web.rest.model.WFStepRegistrationConfig;
import com.meetidentity.usermanagement.web.rest.model.WFStepVisualTemplateConfig;
import com.meetidentity.usermanagement.web.rest.model.WfMobileIDStepDetails;
import com.meetidentity.usermanagement.web.rest.model.Workflow;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStep;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStepDetail;
import com.meetidentity.usermanagement.web.rest.model.notification.NotificationServiceHelper;
import com.meetidentity.usermanagement.web.rest.util.HeaderUtil;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.util.GeometricShapeFactory;


@Service
@Transactional(rollbackForClassName = { "Exception" })
public class UserServiceImpl implements UserService {

	private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	UserIdentificationCodeRepository userIdentificationCodeRepository;

	@Autowired
	PairMobileDeviceConfigRepository pairMobileDeviceConfigRepository;
	
	@Autowired
	ApplicationProperties applicationProperties;

	@Autowired
	CryptoFunction cryptoFunction;

	@Autowired
	private KeycloakService keycloakService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private WorkflowService workflowService;	

	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private UserAttributeRepository userAttributeRepository;

	@Autowired
	private UserAppearanceRepository userAppearanceRepository;
	
	@Autowired
	private UserPivRepository userPivRepository;

	@Autowired
	private UserStudentIDRepository userStudentIDRepository;
	
	@Autowired
	private UserEmployeeIDRepository userEmployeeIDRepository;

	@Autowired
	private UserPersonalizedIDRepository userPersonalizedIDRepository;	

	@Autowired
	private UserHealthIDRepository userHealthIDRepository;
	
	@Autowired
	private UserHealthServiceRepository userHealthServiceRepository;
	
	@Autowired
	private UserDrivingLicenseRepository userDrivingLicenseRepository;

	@Autowired
	private UserNationalIDRepository userNationalIDRepository;

	@Autowired
	private UserVoterIDRepository userVoterIDRepository;
	
	@Autowired
	private UserPermanentResidentIdRepository userPermanentResidentIdRepository;
	
	@Autowired
	private VehicleClassificationRepository vehicleClassificationRepository;

	@Autowired
	private DrivingLicenseRestrictionsRepository drivingLicenseRestrictionsRepository;

	@Autowired
	private UserBiometricRepository userBiometricRepository;
	
	@Autowired
	private UserBiometricAttributeRepository userBiometricAttributeRepository;

	@Autowired
	private UserBiometricSkipRepository userBiometricSkipRepository;

	@Autowired
	private UserOtpRepository userOtpRepository;

	@Autowired
	private IDProofTypeRepository idProofTypeRepository;

	@Autowired
	private UserIDProofDocumentRepository userIDProofDocumentRepository;

	@Autowired
	private ApduFeignClient apduFeignClient;

	@Autowired
	private IDProofIssuingAuthorityRepository idProofIssuingAuthorityRepository;

	@Autowired
	private IdentityProviderUserService identityProviderUserService;

	@Autowired
	private UserCredentialsService userCredentialsService;

	@Autowired
	private WFStepRepository wfStepRepository;

	@Autowired
	private UserLocationRepository userLocationRepository;

	@Autowired
	private UserLocationCustomServiceImpl userLocationCustomServiceImpl;

	@Autowired
	private WorkflowRepository workflowRepository;
	
	@Autowired
	private DeviceProfileRepository deviceProfileRepository;
	
	@Autowired
	private BiometricIdentityFeignClient biometricFeignClient;
	
	@Autowired
	private UserPairDeviceInfoService userPairDeviceInfoService;
	
	@Autowired
	private UserEnrollmentHistoryRepository userEnrollmentHistoryRepository;
	
	@Autowired
	private RoleRepository rolerepository;
	
	@Autowired
	private OrganizationService organizationService;
	
	@Autowired
	private DrivingLicenseService drivingLicenseService;
	
	@Autowired
	private VehicleService vehicleRegistrationService;
	
	@Autowired
	private IDProofService idProofTypeService;
	
	@Autowired
	private EnrollmentStepStatusRepository enrollmentStepStatusRepository;
	
	@Autowired
	private UserAddressRepository userAddressRepository;
	
	@Autowired
	private DeviceProfileConfigRepository deviceProfileConfigRepository;
	
	@Autowired
	private BackgroundColorTransparency backgroundColorTransparency;
	
	@Autowired
	private NotificationServiceHelper notificationServiceHelper;
	
	@Autowired
	private APIGatewayInfoRepository aPIGatewayInfoRepository;
	
	@Autowired
	private IdentityTypeRepository identityTypeRepository;
	
	@Autowired
	private OrganizationIdentitiesRepository orgIdentitiesRepository;
	
	String pattern = "MM-dd-yyyy";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

	String pattern_ngid = "yyyy-MM-dd'T'HH:mm:ss";
	SimpleDateFormat simpleDateFormat_ngid = new SimpleDateFormat(pattern_ngid);

	@Override
	public void registerMobileDeviceUser(String organizationName, User user) throws EnrollmentServiceException {
		List<UserIDProofDocument> userIDProofDocuments =  user.getUserIDProofDocuments();
		if(userIDProofDocuments.size() > 0) {
			userIDProofDocuments.forEach(idProofDoc -> {
				if (idProofDoc.getIdProofTypeName() != null) {
					// get the org identities id by this name and set
					OrganizationIdentitiesEntity entity = orgIdentitiesRepository.getByIdentityTypeNameAndOrg(organizationName, idProofDoc.getIdProofTypeName());
					if(entity != null) {
						idProofDoc.setIdProofTypeId(entity.getId());
					}
				}
			});
		}
		registerUser(organizationName, user);
	}
	
	@Override
	@Transactional(rollbackForClassName = { "Exception", "EnrollmentServiceException" })
	public void registerUser(String organizationName, User user) throws EnrollmentServiceException {
		boolean isApprovalRequired = false;
		boolean isBiometricValidtionRequired = false;
		// TODO handle Exceptions
//		int responseStatusCode = keycloakService.createUser(user);

		try {
			// TODO
			log.info("0\n"+organizationName);
			//
			OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);
			
			if(user.isExpressMobileEnrollment()) {
				String role_id = null;
				log.info("1-0\n"+organizationEntity.getGroupAttributes());
				log.info("1-1\n"+organizationEntity.getRoleAttributes());
				for (GroupAttributeEntity groupAttribute : organizationEntity.getGroupAttributes()) {
					log.info("1-2\n"+groupAttribute.getGroupId());
					if(groupAttribute.getName().equalsIgnoreCase(GroupAttributeNameEnum.DEFAULT_GROUP.getName())){
						user.setGroupId(groupAttribute.getGroupId());
						Group group = keycloakService.getGroupById(organizationName, groupAttribute.getGroupId());
						user.setGroupName(group.getName());
						break;
					}
				} 
				if(user.getGroupId() == null || user.getGroupId().isEmpty()) {

					log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.USER_NOT_ENROLLED, user.getName()));
					throw new EnrollmentServiceException(ErrorMessages.USER_NOT_ENROLLED, user.getName()," Group details are missing");
				
				}
				for (RoleAttributeEntity roleAttribute : organizationEntity.getRoleAttributes()) {
					log.info("1-3\n"+roleAttribute.getName());
					if(roleAttribute.getName().equalsIgnoreCase(RoleAttributeNameEnum.DEFAULT_ROLE.getName())){
						role_id = roleAttribute.getValue();
						break;
					}
				} 

				log.info("1-4\n"+role_id);
				/*********************************************************************************************************/
				IdentityProviderUser identityProviderUser = new IdentityProviderUser();
				identityProviderUser.setUserName(user.getName());
				identityProviderUser.setFirstName(user.getUserAttribute().getFirstName());
				identityProviderUser.setLastName(user.getUserAttribute().getLastName());
				identityProviderUser.setEmail(user.getUserAttribute().getEmail());
				if (user.getUserAttribute().getContact() == null) {
					//
				} else {
					identityProviderUser.setPhoneNumber(String.valueOf(user.getUserAttribute().getContact()));
				}
				identityProviderUser.setEnabled(true);
				identityProviderUser.setCredentialType("password");
				identityProviderUser.setCredentialValue("");
				identityProviderUser.setCredentialTemporary(true);
				identityProviderUser.setGroupId(user.getGroupId());
				identityProviderUser.setAuthServiceRoleID(role_id.split(","));
				identityProviderUser.setOrgId(organizationEntity.getId());
				identityProviderUser.setSendInvite(false);
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				headers.set(Constants.LOGGED_IN_USER, user.getFirstName()+""+user.getLastName());
				APIGatewayInfoEntity apiGatewayInfoEntity = aPIGatewayInfoRepository.getAPIGatewayInfoByOrgName(organizationName);
				log.info("1-5\n"+apiGatewayInfoEntity.getHost());
				String[] domain = apiGatewayInfoEntity.getHost().split("[.]");
				
				// domain size should be 3
				for (String string : domain) {
					log.info("1-6\n domain:"+string);
					
				}
				headers.set(Constants.UIMS_URL, "https://"+organizationName.toLowerCase()+"."+domain[1]+"."+domain[2]);
				headers.set(Constants.EXPRESS_ENROLLMENT, "true");
				ResponseEntity<User> user_ = identityProviderUserService.createUserAndAssignGroupAndRoles(headers, organizationName, identityProviderUser,
						null);
				
				
			}

			// User
			UserEntity userEntity = userRepository.getUserByName(user.getName(), organizationName);
			UserAttributeEntity userAttributeEntity = null;
			if (userEntity == null) {
				userEntity = new UserEntity();
				userAttributeEntity = new UserAttributeEntity();
				userEntity.setName(user.getName());
				userEntity.setReferenceName(user.getName());
				userEntity.setOrganization(organizationEntity);
				userEntity.setGroupId(user.getGroupId());
				userEntity.setGroupName(user.getGroupName());
				UserGroupsEntity userGroupsEntity = new UserGroupsEntity();
				userGroupsEntity.setGroupId(user.getGroupId());
				userGroupsEntity.setGroupName(user.getGroupName());
				userEntity.addUserGroups(userGroupsEntity);
			} else {
				userAttributeEntity = userEntity.getUserAttribute();
			}
			userEntity.setIsEnrollmentUpdated(false);
			userEntity.setUserStatus(UserStatusEnum.ACTIVE.getUserStatus());
			
			log.info("3\n"+user.getGroupId());

			Workflow workflow = workflowService.getWorkflowValidityByGroupID(organizationName, user.getGroupId());

			log.info("4\n workflow queried");
			WorkflowStepDetail workflowStepRegDetail = workflowService.getStepDetails(organizationName,
					workflow.getName(), "REGISTRATION");
			if (!workflowStepRegDetail.getWfStepGenericConfigs().isEmpty()) {
				List<WFStepGenericConfig> configs = workflowStepRegDetail.getWfStepGenericConfigs();
				for (WFStepGenericConfig config : configs) {
					if (config.getName().equalsIgnoreCase("BIOMETRIC_VALIDATION_REQUIRED")) {
						if (config.getValue().equals("Y")) {
							isBiometricValidtionRequired = true;
						}
					}
				}
			}
			
			// Set USER Status
			WorkflowStepDetail workflowStepDetail = workflowService.getStepDetails(organizationName,
					user.getWorkflowName(), "APPROVAL_BEFORE_ISSUANCE");
			if (workflowStepDetail.getWfStepApprovalGroupConfigs().size() == 0) {
				// If no Approval Step Exists
				userEntity.setStatus(UserStatusEnum.READY_FOR_ISSUANCE.name());				
			} else {
				List<WFStepApprovalGroupConfig> configs = workflowStepDetail.getWfStepApprovalGroupConfigs();
				for (WFStepApprovalGroupConfig approvalGroupConfig : configs) {
					userEntity.setRefApprovalGroupId(approvalGroupConfig.getGroupId());
					userEntity.setStatus(UserStatusEnum.PENDING_APPROVAL.name());
				}
				isApprovalRequired = true;
			}

			// User Attributes // User Appearance // User DL // User address
			UserAttribute userAttribute = user.getUserAttribute();
			UserAppearance userAppearance = user.getUserAppearance();
			List<UserIDProofDocument> userIDProofDocs = user.getUserIDProofDocuments();
			UserEnrollmentHistory userEnrollmentHistory = user.getUserEnrollmentHistory();
			UserAddress userAddress = user.getUserAddress();

			userAttributeEntity.setEmail(userAttribute.getEmail());
			if (userAttribute.getDateOfBirth() != null) {
				userAttributeEntity.setDateOfBirth(userAttribute.getDateOfBirth().toInstant());
			}
			userAttributeEntity.setNationality(userAttribute.getNationality());
			userAttributeEntity.setPlaceOfBirth(userAttribute.getPlaceOfBirth());
			userAttributeEntity.setGender(userAttribute.getGender());
			userAttributeEntity.setCountryCode(userAttribute.getCountryCode());
			userAttributeEntity.setContact(userAttribute.getContact());
			userAttributeEntity.setAddress(userAttribute.getAddress());
			userAttributeEntity.setVeteran(userAttribute.isVeteran());
			userAttributeEntity.setOrganDonor(userAttribute.isOrganDonor());
			userAttributeEntity.setBloodType(userAttribute.getBloodType());
			userAttributeEntity.setMothersMaidenName(userAttribute.getMothersMaidenName());
			userAttributeEntity.setFirstName(userAttribute.getFirstName());
			userAttributeEntity.setMiddleName(userAttribute.getMiddleName());
			userAttributeEntity.setLastName(userAttribute.getLastName());
			userAttributeEntity.setUser(userEntity);
			if (userEntity.getUserAttribute() != null) {
				userAttributeEntity.setId(userEntity.getUserAttribute().getId());
			}
			userEntity.setName(userAttribute.getFirstName() + " " + userAttribute.getLastName());
			
            UserEnrollmentHistoryEntity userEnrollmentHistoryEntity = new UserEnrollmentHistoryEntity();

			if(userEnrollmentHistory != null) {	
				if(!isApprovalRequired) {
					userEnrollmentHistoryEntity.setApprovedBy(userEnrollmentHistory.getEnrolledBy());
					userEnrollmentHistoryEntity.setApprovalStatus("APPROVED");
					userEnrollmentHistoryEntity.setReason("Auto Approved");
					userEnrollmentHistoryEntity.setApprovedDate(Timestamp.from(Instant.now()));
					userEnrollmentHistoryEntity.setStatus("Active");
				}				
				userEnrollmentHistoryEntity.setEnrolledBy(userEnrollmentHistory.getEnrolledBy());
				userEnrollmentHistoryEntity.setEnrollmentStartDate(userEnrollmentHistory.getEnrollmentStartDate());
				userEnrollmentHistoryEntity.setEnrollmentEndDate(userEnrollmentHistory.getEnrollmentEndDate());				
				userEnrollmentHistoryEntity.setCreatedBy(userEnrollmentHistory.getEnrolledBy());
				userEnrollmentHistoryEntity.setCreatedDate(Instant.now());
				userEnrollmentHistoryEntity.setUser(userEntity);
				userEntity.addUserEnrollmentHistory(userEnrollmentHistoryEntity);
			}				
			
			UserAppearanceEntity userAppearanceEntity = userEntity.getUserAppearance();
			if (userAppearance != null) {
				if(userAppearanceEntity == null) {
					userAppearanceEntity = new UserAppearanceEntity();
				}
//				userAppearanceEntity = new UserAppearanceEntity(userAppearance.getEyeColor(),
//						userAppearance.getHeight(), userAppearance.getWeight(), userAppearance.getHairColor());
				userAppearanceEntity.setEyeColor(userAppearance.getEyeColor());
				userAppearanceEntity.setHeight(userAppearance.getHeight());
				userAppearanceEntity.setWeight(userAppearance.getWeight());
				userAppearanceEntity.setHairColor(userAppearance.getHairColor());
				
				userAppearanceEntity.setUser(userEntity);
			}
			UserAddressEntity userAddressEntity = userEntity.getUserAddress();
			if(userAddress != null) {
				if(userAddressEntity == null) {
					userAddressEntity = new UserAddressEntity();
				}
				userAddressEntity.setAddressLine1(userAddress.getAddressLine1());
				userAddressEntity.setAddressLine2(userAddress.getAddressLine2());
				userAddressEntity.setCity(userAddress.getCity());
				userAddressEntity.setState(userAddress.getState());
				userAddressEntity.setCountry(userAddress.getCountry());
				userAddressEntity.setZipCode(userAddress.getZipCode());
				userAddressEntity.setUser(userEntity);
				
				userAddressRepository.save(userAddressEntity);
			}
			IdentityTypeEntity identityTypeEntity = identityTypeRepository.findIdentityTypeByName(workflow.getOrganizationIdentities().getIdentityTypeName());

			
			switch (workflow.getOrganizationIdentities().getIdentityTypeName()) {

			case Constants.Driving_License:

				UserDrivingLicense userDrivingLicense = user.getUserDrivingLicense();
				List<UserDrivingLicenseEntity> userDrivingLicenseEntities = userEntity.getUserDrivingLicenses();
				if (userDrivingLicense != null) {
					if (userDrivingLicenseEntities == null) {
						userDrivingLicenseEntities = new ArrayList<UserDrivingLicenseEntity>();
					}
					if (userDrivingLicense.getRestrictions() != null && userDrivingLicense.getVehicleClass() != null) {
						UserDrivingLicenseEntity userDrivingLicenseEntity = new UserDrivingLicenseEntity();
						DrivingLicenseRestrictionEntity dlrEntity = drivingLicenseRestrictionsRepository
								.findOneByTypeAndOrganization(userDrivingLicense.getRestrictions(), organizationName);
						userDrivingLicenseEntity.setRestrictions(dlrEntity);

						VehicleClassificationEntity vehicleClassificationEntity = vehicleClassificationRepository
								.findOneByTypeAndOrganization(userDrivingLicense.getVehicleClass(), organizationName);
						userDrivingLicenseEntity.setVehicleClassification(vehicleClassificationEntity);
						userDrivingLicenseEntity.setVehicleClass(vehicleClassificationEntity.getType());

						userDrivingLicenseEntity.setEndorsements(userDrivingLicense.getEndorsements());
						userDrivingLicenseEntity.setUser(userEntity);

						Calendar c21 = Calendar.getInstance();
						c21.setTime(userAttribute.getDateOfBirth());
						c21.add(Calendar.YEAR, 21);
						userDrivingLicenseEntity.setUnder21Until(Instant.now());
						Calendar c19 = Calendar.getInstance();
						c19.setTime(userAttribute.getDateOfBirth());
						c19.add(Calendar.YEAR, 19);
						userDrivingLicenseEntity.setUnder19Until(Instant.now());
						Calendar c18 = Calendar.getInstance();
						c18.setTime(userAttribute.getDateOfBirth());
						c18.add(Calendar.YEAR, 18);
						userDrivingLicenseEntity.setUnder18Until(Instant.now());

						userDrivingLicenseEntities.add(userDrivingLicenseEntity);
						userEntity.setUserDrivingLicenses(userDrivingLicenseEntities);

					}
				}
				if (userEntity.getId() != null) {
					if (userDrivingLicenseEntities.size() > 0) {
						userDrivingLicenseRepository.save(userDrivingLicenseEntities);
					}
				}
				break;
			case Constants.PIV_and_PIV_1:
				UserPIVModel userPIV = user.getUserPIV();
				List<UserPivEntity> userPivEntities = userEntity.getUserPivCredentials();
				if (userPIV != null) {
					if (userPivEntities == null) {
						userPivEntities = new ArrayList<UserPivEntity>();
					}
					UserPivEntity userPIVEntity = new UserPivEntity();
					userPIVEntity.setEmployeeAffiliation(userPIV.getEmployeeAffiliation());
					userPIVEntity.setEmployeeAffiliationColorCode(userPIV.getEmployeeAffiliationColorCode());
					userPIVEntity.setRank(userPIV.getRank());
					userPIVEntity.setAgency(organizationEntity.getDisplayName());
					userPIVEntity.setIssuerIdentificationNumber(organizationEntity.getIssuerIdentificationNumber());
					userPIVEntity.setAgencyAbbreviation(organizationEntity.getAbbreviation());
					userPIVEntity.setUser(userEntity);
					userPivEntities.add(userPIVEntity);
					userEntity.setUserPivCredentials(userPivEntities);
				}
				if (userEntity.getId() != null) {
					if (userPivEntities.size() > 0) {
						userPivRepository.save(userPivEntities);
					}
				}
				break;
			case Constants.National_ID:
				UserNationalID userNationalID = user.getUserNationalID();
				UserNationalIdEntity userNationalIdEntity = new UserNationalIdEntity();
				userNationalIdEntity.setUser(userEntity);
				userEntity.setUserNationalId(userNationalIdEntity);
				if (userNationalID != null) {
					userNationalIdEntity.setVaccineName(userNationalID.getVaccineName());
					userNationalIdEntity.setVaccinationLocationName(userNationalID.getVaccinationLocationName());
					userNationalIdEntity.setVaccinationDate(userNationalID.getVaccinationDate());
				}
				if (userEntity.getId() != null) {
					userNationalIDRepository.save(userNationalIdEntity);
				}
				break;
			case Constants.Voter_ID:
				UserVoterID userVoterID = user.getUserVoterID();
				UserVoterIDEntity userVoterIDEntity = new UserVoterIDEntity();
				userVoterIDEntity.setUser(userEntity);
				userEntity.setUserVoterID(userVoterIDEntity);
				if(userVoterID == null ) {
					userVoterIDEntity.setElectorKey(Util.getAlphaNumericString(18).toUpperCase());
					userVoterIDEntity.setRegistrationYear(Calendar.getInstance().get(Calendar.YEAR)+"");
				}else {
					userVoterIDEntity.setElectorKey(userVoterID.getElectorKey());
					if(userVoterID.getRegistrationYear() == null || userVoterID.getRegistrationYear().length()<=0) {
						userVoterIDEntity.setRegistrationYear(Calendar.getInstance().get(Calendar.YEAR)+"");
					}else {
						userVoterIDEntity.setRegistrationYear(userVoterID.getRegistrationYear());
					}
					userVoterIDEntity.setCurp(userVoterID.getCurp());
				}
				if (userEntity.getId() != null) {
					userVoterIDRepository.save(userVoterIDEntity);
				}
				break;
			case Constants.Permanent_Resident_ID:
				UserPermanentResidentID userPermanentResidentID = user.getUserPermanentResidentID();
				UserPermanentResidentIdEntity userPermanentResidentIdEntity = new UserPermanentResidentIdEntity();
				userPermanentResidentIdEntity.setUser(userEntity);
				userEntity.setUserPermanentResidentId(userPermanentResidentIdEntity);
				if (userPermanentResidentID != null) {
					userPermanentResidentIdEntity.setPassportNumber(userPermanentResidentID.getPassportNumber());
					userPermanentResidentIdEntity.setResidentcardTypeNumber(userPermanentResidentID.getResidentcardTypeNumber());
					userPermanentResidentIdEntity.setResidentcardTypeCode(userPermanentResidentID.getResidentcardTypeCode());
				}
				if (userEntity.getId() != null) {
					userPermanentResidentIdRepository.save(userPermanentResidentIdEntity);
				}
				break;
			case Constants.Health_ID:
				UserHealthID userHealthID = user.getUserHealthID();

				List<UserHealthIDEntity> userHealthIDEntities = userEntity.getUserHealthIDs();

				if (userHealthID != null) {
					if (userHealthIDEntities == null) {
						userHealthIDEntities = new ArrayList<UserHealthIDEntity>();
					}

					UserHealthIDEntity userHealthIDEntity = new UserHealthIDEntity();
					userHealthIDEntity.setGroupPlan(userHealthID.getGroupPlan());
					userHealthIDEntity.setPolicyNumber(userHealthID.getPolicyNumber());
					userHealthIDEntity.setHealthPlanNumber(userHealthID.getHealthPlanNumber());
					userHealthIDEntity.setPrimarySubscriber(userHealthID.getPrimarySubscriber());
					userHealthIDEntity.setPrimarySubscriberID(userHealthID.getPrimarySubscriberID());
					userHealthIDEntity.setPcp(userHealthID.getPcp());
					userHealthIDEntity.setPcpPhone(userHealthID.getPcpPhone());
					userHealthIDEntity.setIssuerIN(organizationEntity.getIssuerIdentificationNumber());

					userHealthIDEntity.setUser(userEntity);
					userHealthIDEntities.add(userHealthIDEntity);
					userEntity.setUserHealthIDs(userHealthIDEntities);
				}
				if (userEntity.getId() != null) {
					if (userHealthIDEntities.size() > 0) {
						userHealthIDRepository.save(userHealthIDEntities);
					}
				}
				break;
			case Constants.Health_Service_ID:
				UserHealthService userHealthService = user.getUserHealthService();
				if (userHealthService != null) {
					UserHealthServiceEntity userHealthServiceEntity = new UserHealthServiceEntity();

					userHealthServiceEntity.setUser(userEntity);

					userHealthServiceEntity.setIdNumber(Util.get8DigitRandomNumber());
					userHealthServiceEntity.setIssuingAuthority("Issuing Authority ");
					userHealthServiceEntity.setPrimarySubscriber(userHealthService.getPrimarySubscriber());
					userHealthServiceEntity.setPrimarySubscriberID(userHealthService.getPrimarySubscriberID());
					userHealthServiceEntity.setPrimaryDoctor(userHealthService.getPrimaryDoctor());
					userHealthServiceEntity.setPrimaryDoctorPhone(userHealthService.getPrimaryDoctorPhone());
					userHealthServiceEntity
							.setUserHealthServiceVaccineInfos(new ArrayList<UserHealthServiceVaccineInfoEntity>());
					List<UserHealthServiceVaccineInfoEntity> UserHealthServiceVaccineInfoEntities = userHealthServiceEntity
							.getUserHealthServiceVaccineInfos();

					for (UserHealthServiceVaccineInfo userHealthServiceVaccineInfo : userHealthService
							.getUserHealthServiceVaccineInfos()) {

						UserHealthServiceVaccineInfoEntity userHealthServiceVaccineInfoEntity = new UserHealthServiceVaccineInfoEntity();
						userHealthServiceVaccineInfoEntity.setVaccine(userHealthServiceVaccineInfo.getVaccine());
						userHealthServiceVaccineInfoEntity.setRoute(userHealthServiceVaccineInfo.getRoute());
						userHealthServiceVaccineInfoEntity.setSite(userHealthServiceVaccineInfo.getSite());
						userHealthServiceVaccineInfoEntity.setDate(userHealthServiceVaccineInfo.getDate());
						userHealthServiceVaccineInfoEntity
								.setAdministeredBy(userHealthServiceVaccineInfo.getAdministeredBy());
						userHealthServiceVaccineInfoEntity.setUserhs(userHealthServiceEntity);

						UserHealthServiceVaccineInfoEntities.add(userHealthServiceVaccineInfoEntity);
					}
					userHealthServiceEntity.setUimsId("UIA" + Util.get8DigitRandomNumber());
					userEntity.setUserHealthService(userHealthServiceEntity);

					userHealthServiceRepository.save(userHealthServiceEntity);
				}
				break;
			case Constants.Student_ID:
				UserStudentID userStudentID = user.getUserStudentID();
				List<UserStudentIDEntity> userStudentIDEntities = userEntity.getUserStudentIDs();
				if (userStudentID != null) {
					if (userStudentIDEntities == null) {
						userStudentIDEntities = new ArrayList<UserStudentIDEntity>();
					}

					UserStudentIDEntity userStudentIDEntity = new UserStudentIDEntity();
					userStudentIDEntity.setUser(userEntity);
					userStudentIDEntity.setDepartment(userStudentID.getDepartment());
					userStudentIDEntity.setInstitution(organizationEntity.getDescription());
					userStudentIDEntity.setIssuerIN(organizationEntity.getIssuerIdentificationNumber());

					userStudentIDEntities.add(userStudentIDEntity);
					userEntity.setUserStudentIDs(userStudentIDEntities);
				}
				if (userEntity.getId() != null) {
					if (userStudentIDEntities.size() > 0) {
						userStudentIDRepository.save(userStudentIDEntities);
					}
				}
				break;
			case Constants.Employee_ID:
				UserEmployeeID userEmployeeID = user.getUserEmployeeID();
				List<UserEmployeeIDEntity> userEmployeeIDEntities = userEntity.getUserEmployeeIDs();
				if(userEmployeeIDEntities == null || userEmployeeIDEntities.isEmpty()) {
					userEmployeeIDEntities = new ArrayList<UserEmployeeIDEntity>();
					UserEmployeeIDEntity userEmployeeIDEntity = new UserEmployeeIDEntity();
					userEmployeeIDEntity.setUser(userEntity);
					userEmployeeIDEntities.add(userEmployeeIDEntity);
					userEntity.addUserEmployeeIDs(userEmployeeIDEntities);
				}
				for (UserEmployeeIDEntity userEmployeeIDEntity : userEmployeeIDEntities) {		//should get only one record		
					if (userEmployeeID != null) {				
						userEmployeeIDEntity.setDepartment(userEmployeeID.getDepartment());
						userEmployeeIDEntity.setAffiliation(userEmployeeID.getAffiliation());
					}
				}
				if (userEntity.getId() != null) {
					if (userEmployeeIDEntities.size() > 0) {
						userEmployeeIDRepository.save(userEmployeeIDEntities);
					}
				}
				break;
			default:
				//UserPersonalizedID userPersonalizedID = user.getUserPersonalizedID();
				List<UserPersonalizedIDEntity> userPersonalizedIDEntities = userEntity.getUserPersonalizedIDs();

				if (userPersonalizedIDEntities == null || userPersonalizedIDEntities.isEmpty()) {
					if(userPersonalizedIDEntities == null) {
						userPersonalizedIDEntities = new ArrayList<UserPersonalizedIDEntity>();
					}
					UserPersonalizedIDEntity userPersonalizedIDEntity = new UserPersonalizedIDEntity();
					userPersonalizedIDEntity.setUser(userEntity);
					userPersonalizedIDEntity.setIdentityType(identityTypeEntity);
					userPersonalizedIDEntities.add(userPersonalizedIDEntity);
				}
			
				if (userEntity.getId() != null) {
					if (userPersonalizedIDEntities.size() > 0) {
						userPersonalizedIDRepository.save(userPersonalizedIDEntities);
					}
				}
				break;
			}

			if (userEntity.getId() == null) {
				if (userAttributeEntity != null) {
					userEntity.setUserAttribute(userAttributeEntity);
				}
				if (userAppearanceEntity != null) {
					userEntity.setUserAppearance(userAppearanceEntity);
				}
				/*if(userEnrollmentHistoryEntity !=null) {
					userEntity.setUserEnrollmentsHistory(userEnrollmentHistoryEntity);
				}*/
			} else {
				if (userAttributeEntity != null) {
					userAttributeRepository.save(userAttributeEntity);
				}
				if (userAppearanceEntity != null) {
					userAppearanceRepository.save(userAppearanceEntity);
				}
			}

			if (userIDProofDocs.size() != 0) {
				// set id proof docs
				for (UserIDProofDocument userIDProofDoc : userIDProofDocs) {
					UserIDProofDocumentEntity userIDProofDocumentEntity = new UserIDProofDocumentEntity();
					userIDProofDocumentEntity.setName(userIDProofDoc.getName());
					userIDProofDocumentEntity.setFrontImg(userIDProofDoc.getFrontImg());
					userIDProofDocumentEntity.setBackImg(userIDProofDoc.getBackImg());
					userIDProofDocumentEntity.setFileData(userIDProofDoc.getFile());
					userIDProofDocumentEntity.setFrontFileType(userIDProofDoc.getFrontFileType());
					userIDProofDocumentEntity.setBackFileType(userIDProofDoc.getBackFileType());;
					userIDProofDocumentEntity.setFrontFileName(userIDProofDoc.getFrontFileName());
					userIDProofDocumentEntity.setBackFileName(userIDProofDoc.getBackFileName());
					userIDProofDocumentEntity
							.setOrganizationIdentity(orgIdentitiesRepository.getOne(userIDProofDoc.getIdProofTypeId()));
					userIDProofDocumentEntity.setIdProofIssuingAuthority(
							idProofIssuingAuthorityRepository.findOne(userIDProofDoc.getIssuingAuthorityId()));
					userIDProofDocumentEntity.setUser(userEntity);
					userEntity.addUserIDProofDocuments(userIDProofDocumentEntity);
				}
			}

			

			// User Biometrics

			// Set Fingerprint values to UserBioInfo object. This object will send for bio
			// enrollment in Nserver
			UserBioInfo userBioInfo = new UserBioInfo();
			
			List<UserBiometric> userBiometrics = user.getUserBiometrics();

			if (!isApprovalRequired && userBiometrics.size() > 0) {
				userBioInfo = getUserBioInfoObj(userBiometrics);
				// set user ID
				userBioInfo.setUserID(userEntity.getId());
			}
			for (UserBiometric userBiometric : userBiometrics) {
				UserBiometricEntity userBiometricEntity = new UserBiometricEntity();
				userBiometricEntity.setType(userBiometric.getType());
				userBiometricEntity.setPosition(userBiometric.getPosition());
				userBiometricEntity.setFormat(userBiometric.getFormat());
				userBiometricEntity.setData(userBiometric.getData());
				userBiometricEntity.setAnsi(userBiometric.getAnsi());
				userBiometricEntity.setImageQuality(userBiometric.getImageQuality());
				userBiometricEntity.setWsqData(userBiometric.getWsqData());

				// User Biometric Attributes
				userBiometric.getUserBiometricAttributes().forEach((userBiometricAttribute) -> {
					UserBiometricAttributeEntity userBiometricAttributeEntity = new UserBiometricAttributeEntity();
					userBiometricAttributeEntity.setName(userBiometricAttribute.getName());
					userBiometricAttributeEntity.setValue(userBiometricAttribute.getValue());

					userBiometricEntity.addUserBiometricAttribute(userBiometricAttributeEntity);
				});

				if (userEntity.getId() == null) {
					userEntity.addUserBiometric(userBiometricEntity);
				} else {
					userBiometricEntity.setUser(userEntity);
					userBiometricRepository.save(userBiometricEntity);
				}
			}

			// User Biometric Skip

			List<UserBiometricSkip> userBiometricSkips = user.getUserBiometricSkips();
			for (UserBiometricSkip userBiometricSkip : userBiometricSkips) {

				UserBiometricSkipEntity userBiometricSkipEntity = new UserBiometricSkipEntity();
				userBiometricSkipEntity.setType(userBiometricSkip.getType());
				userBiometricSkipEntity.setPosition(userBiometricSkip.getPosition());
				userBiometricSkipEntity.setSkipType(userBiometricSkip.getSkipType());
				userBiometricSkipEntity.setSkipReason(userBiometricSkip.getSkipReason());

				if (userEntity.getId() == null) {
					userEntity.addUserBiometricSkip(userBiometricSkipEntity);
				} else {
					userBiometricSkipEntity.setUser(userEntity);
					userBiometricSkipRepository.save(userBiometricSkipEntity);
				}
			}
			
        // 	enrollment in Nserver
		//	if (!isApprovalRequired) {
			if (isApprovalRequired) {
				if (userBioInfo.getFingerprintModalities() != null || userBioInfo.getFaceModality() != null
						|| userBioInfo.getIrisModalities() != null) {
					userBioInfo.setUserID(userEntity.getId());
					userBioInfo.setDeDuplicationCheck(false);
					ResponseEntity<BiometricVerificationResponse> response = null;
					response = biometricFeignClient.enroll(organizationName,userBioInfo);
					if(response.getStatusCodeValue() == 200) {
						if(!response.getBody().getStatusMessage().equalsIgnoreCase("OK")) {
							if(response.getBody().getStatusMessage().equalsIgnoreCase("DUPLICATE_ID")) {
								// do nothing
							} else {
								if(response.getBody().getStatusMessage().equalsIgnoreCase("BAD_OBJECT")
										 || response.getBody().getStatusMessage().equalsIgnoreCase("OBJECT_NOT_FOUND")) {
								 throw new EnrollmentServiceException("Biometrics are not properly captured.");
								} else {
									throw new EnrollmentServiceException(response.getBody().getStatusMessage());
								}
							}
						}
					} else {
						throw new EnrollmentServiceException(response.getBody().getStatusMessage());
					}					
				}
			}
			if (userEntity.getId() == null) {
				userRepository.save(userEntity);
			}

			JSONObject userJsonObject = new JSONObject();
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(user.getName(), organizationName);
			if(userRepresentation.getFederationLink() == null) {
				userJsonObject.put("firstName", user.getUserAttribute().getFirstName());
				userJsonObject.put("lastName", user.getUserAttribute().getLastName());
				userJsonObject.put("email", user.getUserAttribute().getEmail());
				keycloakService.updateUser(userRepresentation.getId(), userJsonObject, organizationName, user.getName());
			} else {
				System.out.println("user is from Ldap");
				// then this user is Ldap user so we can't edit the user attributes for this user, and also it depends on the use federation configuration
			}

			log.info(ApplicationConstants.notifyCreate, ErrorMessages.USER_ENROLLED, user.getName());
			
			if(user.isExpressMobileEnrollment()) {
				sendPairYourDeviceInviteNotification(organizationName, userEntity, userEntity.getId(), userEntity.getName());	
			}

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.USER_NOT_ENROLLED, user.getName()),
					cause);
			throw new EnrollmentServiceException(cause, ErrorMessages.USER_NOT_ENROLLED, user.getName(),
					cause.getMessage());
		}
	}

	@Override
	public void updateUser(String organizationName, User user) throws EnrollmentServiceException {

		// TODO handle Exceptions
		boolean isApprovalRequired = false;

		try {

			OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);

			// User
			UserEntity userEntity = userRepository.getUserByName(user.getName(), organizationName);
			
			// Set USER Status
			WorkflowStepDetail workflowStepDetail = workflowService.getStepDetails(organizationName,
					user.getWorkflowName(), "APPROVAL_BEFORE_ISSUANCE");
			if (workflowStepDetail.getWfStepApprovalGroupConfigs().size() == 0) {
				if(userEntity != null) {
					if(userEntity.getStatus().equals(UserStatusEnum.IDENTITY_ISSUED.name())) {
						userEntity.setStatus(UserStatusEnum.READY_FOR_REISSUANCE.name());
					}
				}
				// No change for READY_FOR_REISSUANCE, READY_FOR_ISSUANCE
			} else {
				List<WFStepApprovalGroupConfig> configs = workflowStepDetail.getWfStepApprovalGroupConfigs();
				for (WFStepApprovalGroupConfig approvalGroupConfig : configs) {

					//userEntity.setRefApprovalGroupId(approvalGroupConfig.getGroupId());

					userEntity.setStatus(UserStatusEnum.PENDING_APPROVAL.name());
				}
				isApprovalRequired = true;

			}
			userEntity.setIsEnrollmentUpdated(true);
			// User Attributes // User Appearance // User DL // User IdProofDocs // User Enrollment history
			UserAttribute userAttribute = user.getUserAttribute();
			UserAppearance userAppearance = user.getUserAppearance();
			UserPIVModel userPIV = user.getUserPIV();
			UserNationalID userNationalID = user.getUserNationalID();
			UserVoterID userVoterID = user.getUserVoterID();
			UserPermanentResidentID userPermanentResidentID = user.getUserPermanentResidentID();
			UserEmployeeID userEmployeeID = user.getUserEmployeeID();
			UserHealthID userHealthID = user.getUserHealthID();
			UserStudentID userStudentID = user.getUserStudentID();
			UserDrivingLicense userDrivingLicense = user.getUserDrivingLicense();
			List<UserIDProofDocument> userIDProofDocs = user.getUserIDProofDocuments();
			UserEnrollmentHistory userEnrollmentsHistory = user.getUserEnrollmentHistory();

			UserAttributeEntity userAttributeEntity = userEntity.getUserAttribute();
			if(userAttribute != null) {
				if (userAttributeEntity == null) {
					userAttributeEntity = new UserAttributeEntity();
				}
				userAttributeEntity.setFirstName(userAttribute.getFirstName());
				userAttributeEntity.setMiddleName(userAttribute.getMiddleName());
				userAttributeEntity.setLastName(userAttribute.getLastName());
				userAttributeEntity.setMothersMaidenName(userAttribute.getMothersMaidenName());
				userAttributeEntity.setEmail(userAttribute.getEmail());
				userAttributeEntity.setDateOfBirth(userAttribute.getDateOfBirth().toInstant());
				userAttributeEntity.setNationality(userAttribute.getNationality());
				userAttributeEntity.setPlaceOfBirth(userAttribute.getPlaceOfBirth());
				userAttributeEntity.setGender(userAttribute.getGender());
				userAttributeEntity.setCountryCode(userAttribute.getCountryCode());
				userAttributeEntity.setContact(userAttribute.getContact());
				userAttributeEntity.setAddress(userAttribute.getAddress());
				userAttributeEntity.setVeteran(userAttribute.isVeteran());
				userAttributeEntity.setOrganDonor(userAttribute.isOrganDonor());
				userAttributeEntity.setBloodType(userAttribute.getBloodType());
				userAttributeEntity.setUser(userEntity);
				userEntity.setName(userAttribute.getFirstName() + " " + userAttribute.getLastName());
			} else {
				if (userAttributeEntity != null) {
					// delete all user attributes from db
					userAttributeEntity.setUser(null);
					userAttributeRepository.delete(userAttributeEntity);
				}
			}
			
			List<UserEnrollmentHistoryEntity> userEnrollmentsHistoryEntity = userEntity.getUserEnrollmentsHistory();
			UserEnrollmentHistoryEntity userEnrollmentHistoryEntity = null;
			if(!userEnrollmentsHistoryEntity.isEmpty()) {
				for(UserEnrollmentHistoryEntity userEnrollment:userEntity.getUserEnrollmentsHistory()) {
					if(userEnrollment.getStatus()!=null && userEnrollment.getStatus().equalsIgnoreCase("Active")){
						userEnrollment.setStatus("Inactive");
						userEnrollmentHistoryEntity=new UserEnrollmentHistoryEntity();
					} else if(userEnrollment.getStatus()== null) {
						userEnrollmentHistoryEntity = userEnrollment;
					}
				}
				if(userEnrollmentHistoryEntity == null) {
					userEnrollmentHistoryEntity = new UserEnrollmentHistoryEntity();
				}
				/*if(userEnrollmentsHistory != null) {
					userEnrollmentHistoryEntity =new UserEnrollmentHistoryEntity();
				}*/
				if(!isApprovalRequired) {
					userEnrollmentHistoryEntity.setApprovedBy(userEnrollmentsHistory.getApprovedBy());
					userEnrollmentHistoryEntity.setApprovalStatus(userEnrollmentsHistory.getApprovalStatus());
					userEnrollmentHistoryEntity.setReason(userEnrollmentsHistory.getReason());
					userEnrollmentHistoryEntity.setApprovedDate(Timestamp.from(Instant.now()));
				}	
				userEnrollmentHistoryEntity.setEnrolledBy(userEnrollmentsHistory.getEnrolledBy());
				userEnrollmentHistoryEntity.setEnrollmentStartDate(userEnrollmentsHistory.getEnrollmentStartDate());
				userEnrollmentHistoryEntity.setEnrollmentEndDate(userEnrollmentsHistory.getEnrollmentEndDate());
				userEnrollmentHistoryEntity.setCreatedBy(userEnrollmentsHistory.getEnrolledBy());
				userEnrollmentHistoryEntity.setCreatedDate(Instant.now());
				userEnrollmentHistoryEntity.setUser(userEntity);	
				userEntity.addUserEnrollmentHistory(userEnrollmentHistoryEntity);
			}		

			UserAppearanceEntity userAppearanceEntity = userEntity.getUserAppearance();
			if (userAppearance != null) {
				if(userAppearanceEntity == null) {
					userAppearanceEntity = new UserAppearanceEntity(userAppearance.getEyeColor(),
							userAppearance.getHeight(), userAppearance.getWeight(), userAppearance.getHairColor());
				}
				userAppearanceEntity.setEyeColor(userAppearance.getEyeColor());
				userAppearanceEntity.setHeight(userAppearance.getHeight());
				userAppearanceEntity.setWeight(userAppearance.getWeight());
				userAppearanceEntity.setHairColor(userAppearance.getHairColor());
				userAppearanceEntity.setUser(userEntity);
			} else {
				if(userAppearanceEntity != null) {
					userAppearanceEntity.setUser(null);
					userAppearanceRepository.delete(userAppearanceEntity);
				}
			}
			UserAddress userAddress = user.getUserAddress();
			UserAddressEntity userAddressEntity = userEntity.getUserAddress();
			if(userAddress != null) {
				if(userAddressEntity == null) {
					userAddressEntity = new UserAddressEntity();
				}
				userAddressEntity.setAddressLine1(userAddress.getAddressLine1());
				userAddressEntity.setAddressLine2(userAddress.getAddressLine2());
				userAddressEntity.setCity(userAddress.getCity());
				userAddressEntity.setState(userAddress.getState());
				userAddressEntity.setCountry(userAddress.getCountry());
				userAddressEntity.setZipCode(userAddress.getZipCode());
				userAddressEntity.setUser(userEntity);
				
				userAddressRepository.save(userAddressEntity);
			}
			List<UserPivEntity> userPivEntities = userEntity.getUserPivCredentials();
			if (userPIV != null) {
				for (UserPivEntity userPIVEntity : userPivEntities) {
					userPIVEntity.setRank(userPIV.getRank());
					userPIVEntity.setEmployeeAffiliation(userPIV.getEmployeeAffiliation());
					userPIVEntity.setEmployeeAffiliationColorCode(userPIV.getEmployeeAffiliationColorCode());
				}
			}
					
			UserNationalIdEntity userNationalIdEntity = userEntity.getUserNationalId();
			if (userNationalIdEntity != null) {
				userNationalIdEntity.setVaccineName(userNationalID.getVaccineName());
				userNationalIdEntity.setVaccinationLocationName(userNationalID.getVaccinationLocationName());
				userNationalIdEntity.setVaccinationDate(userNationalID.getVaccinationDate());
			}
			
			UserPermanentResidentIdEntity userPermanentResidentIdEntity = userEntity.getUserPermanentResidentId();
			if (userPermanentResidentIdEntity != null) {
				userPermanentResidentIdEntity.setPassportNumber(userPermanentResidentID.getPassportNumber());
				userPermanentResidentIdEntity.setResidentcardTypeNumber(userPermanentResidentID.getResidentcardTypeNumber());
				userPermanentResidentIdEntity.setResidentcardTypeCode(userPermanentResidentID.getResidentcardTypeCode());
			}
			
			List<UserEmployeeIDEntity> userEmployeeIDEntities = userEntity.getUserEmployeeIDs();
			if (userEmployeeID != null) {
				for (UserEmployeeIDEntity userEmployeeIDEntity : userEmployeeIDEntities) {
					userEmployeeIDEntity.setDepartment(userEmployeeID.getDepartment());
					userEmployeeIDEntity.setAffiliation(userEmployeeID.getAffiliation());
				}
			}
			
			List<UserStudentIDEntity> userStudentIDEntities = userEntity.getUserStudentIDs();
			if (userStudentID != null) {
				for (UserStudentIDEntity userStudentIDEntity : userStudentIDEntities) {
					userStudentIDEntity.setDepartment(userStudentID.getDepartment());
				}
			}
			
			List<UserHealthIDEntity> userHealthIDEntities = userEntity.getUserHealthIDs();
			if (userHealthID != null) {
				for (UserHealthIDEntity userHealthIDEntity : userHealthIDEntities) {
					userHealthIDEntity.setGroupPlan(userHealthID.getGroupPlan());
					userHealthIDEntity.setPolicyNumber(userHealthID.getPolicyNumber());
					userHealthIDEntity.setHealthPlanNumber(userHealthID.getHealthPlanNumber());
					userHealthIDEntity.setPrimarySubscriber(userHealthID.getPrimarySubscriber());
					userHealthIDEntity.setPrimarySubscriberID(userHealthID.getPrimarySubscriberID());
					userHealthIDEntity.setPcp(userHealthID.getPcp());
					userHealthIDEntity.setPcpPhone(userHealthID.getPcpPhone());
				}
			}

			List<UserDrivingLicenseEntity> userDrivingLicenseEntities = userEntity.getUserDrivingLicenses();
			if (userDrivingLicense != null) {
				if (userDrivingLicense.getRestrictions() != null && userDrivingLicense.getVehicleClass() != null) {
					for (UserDrivingLicenseEntity userDrivingLicenseEntity : userDrivingLicenseEntities) {
						DrivingLicenseRestrictionEntity dlrEntity = drivingLicenseRestrictionsRepository
								.findOneByTypeAndOrganization(userDrivingLicense.getRestrictions(), organizationName);
						userDrivingLicenseEntity.setRestrictions(dlrEntity);

						VehicleClassificationEntity vehicleClassificationEntity = vehicleClassificationRepository
								.findOneByTypeAndOrganization(userDrivingLicense.getVehicleClass(), organizationName);
						userDrivingLicenseEntity.setVehicleClassification(vehicleClassificationEntity);
						userDrivingLicenseEntity.setVehicleClass(vehicleClassificationEntity.getType());
						userDrivingLicenseEntity.setEndorsements(userDrivingLicense.getEndorsements());

						Calendar c = Calendar.getInstance();
						c.setTime(userAttribute.getDateOfBirth());					
						c.add(Calendar.YEAR, 21);
						userDrivingLicenseEntity.setUnder21Until(Instant.now());
						c.setTime(userAttribute.getDateOfBirth());					
						c.add(Calendar.YEAR, 19);
						userDrivingLicenseEntity.setUnder19Until(Instant.now());
						c.setTime(userAttribute.getDateOfBirth());					
						c.add(Calendar.YEAR, 18);
						userDrivingLicenseEntity.setUnder18Until(Instant.now());
					}
				}
			} 

			if (userAttributeEntity != null) {
				userAttributeRepository.save(userAttributeEntity);
			}
			if (userAppearanceEntity != null) {
				userAppearanceRepository.save(userAppearanceEntity);
			}
			if (userPivEntities.size() > 0) {
				userPivRepository.save(userPivEntities);
			}
			if (userNationalIdEntity != null) {
				userNationalIDRepository.save(userNationalIdEntity);
			}
			if (userPermanentResidentIdEntity != null) {
				userPermanentResidentIdRepository.save(userPermanentResidentIdEntity);
			}
			if (userEmployeeIDEntities.size() > 0) {
				userEmployeeIDRepository.save(userEmployeeIDEntities);
			}
			if (userStudentIDEntities.size() > 0) {
				userStudentIDRepository.save(userStudentIDEntities);
			}
			if (userHealthIDEntities.size() > 0) {
				userHealthIDRepository.save(userHealthIDEntities);
			}
			if (userDrivingLicenseEntities.size() > 0) {
				userDrivingLicenseRepository.save(userDrivingLicenseEntities);
			}

			List<UserIDProofDocumentEntity> userIDProofDocumentsToRemove = new ArrayList<>();
			// delete the fields from db which are not selected in ui
			userEntity.getUserIDProofDocuments().stream().forEach(idProofDocEntity -> {
				List<UserIDProofDocument> userIdProofDocs = userIDProofDocs.stream()
						.filter(idProofDoc -> idProofDoc.getIdProofTypeId().equals(idProofDocEntity.getOrganizationIdentity().getId()))
						.filter(idProofDoc -> idProofDoc.getIssuingAuthorityId().equals(idProofDocEntity.getIdProofIssuingAuthority().getId()))
						.collect(Collectors.toList());
				if(userIdProofDocs.size() == 0) {
					userIDProofDocumentsToRemove.add(idProofDocEntity);
				}
			});
			// add if any thing added newly
			for (UserIDProofDocument userIDProofDoc : userIDProofDocs) {
				List<UserIDProofDocumentEntity> list = userEntity.getUserIDProofDocuments().stream()
						.filter(idProofDocEntity -> idProofDocEntity.getOrganizationIdentity().getId().equals(userIDProofDoc.getIdProofTypeId()))
						.filter(idProofDocEntity -> idProofDocEntity.getIdProofIssuingAuthority().getId().equals(userIDProofDoc.getIssuingAuthorityId()))
						.collect(Collectors.toList());
				UserIDProofDocumentEntity userIDProofDocumentEntity = new UserIDProofDocumentEntity();
				if (list.size() != 0) {
					userIDProofDocumentEntity = list.get(0);
				}
				userIDProofDocumentEntity.setName(userIDProofDoc.getName());
				userIDProofDocumentEntity.setFrontImg(userIDProofDoc.getFrontImg());
				userIDProofDocumentEntity.setBackImg(userIDProofDoc.getBackImg());
				userIDProofDocumentEntity.setFileData(userIDProofDoc.getFile());
				userIDProofDocumentEntity.setFrontFileType(userIDProofDoc.getFrontFileType());
				userIDProofDocumentEntity.setBackFileType(userIDProofDoc.getBackFileType());
				userIDProofDocumentEntity.setFrontFileName(userIDProofDoc.getFrontFileName());
				userIDProofDocumentEntity.setBackFileName(userIDProofDoc.getBackFileName());
				userIDProofDocumentEntity.setOrganizationIdentity(orgIdentitiesRepository.getOne(userIDProofDoc.getIdProofTypeId()));
				userIDProofDocumentEntity.setIdProofIssuingAuthority(idProofIssuingAuthorityRepository.findOne(userIDProofDoc.getIssuingAuthorityId()));
				userIDProofDocumentEntity.setUser(userEntity);
				userEntity.addUserIDProofDocuments(userIDProofDocumentEntity);
				userIDProofDocumentRepository.save(userIDProofDocumentEntity);
			}
			for (UserIDProofDocumentEntity obj : userIDProofDocumentsToRemove) {
				 userEntity.removeUserIDProofDocuments(obj);
				 wfStepRepository.delete(obj.getId());
			}
			 
			// User Biometrics

			// Set Fingerprint values to UserBioInfo object. This object will send for bio
			// enrollment in Nserver
			UserBioInfo userBioInfo = new UserBioInfo();
			
			List<UserBiometric> userBiometrics = user.getUserBiometrics();

			if(userBiometrics.size() > 0) {
				userBioInfo = getUserBioInfoObj(userBiometrics);
				// set user ID
				userBioInfo.setUserID(userEntity.getId());
			}
			List<UserBiometricEntity> userBiometricFPEntityListToRemove = new ArrayList<>();
			// delete the fields from db which are not selected in ui
			userEntity.getUserBiometrics().stream().filter(Objects::nonNull)
					.filter(userBiometricEntity -> userBiometricEntity.getType().equalsIgnoreCase("FINGERPRINT"))
					.forEach(userBioEntity -> {
						List<UserBiometric> userBiometricsList = user.getUserBiometrics().stream()
								.filter(e -> e.getType().equalsIgnoreCase("FINGERPRINT"))
								.filter(e1 -> e1.getPosition().equalsIgnoreCase(userBioEntity.getPosition()))
								.collect(Collectors.toList());
						if (userBiometricsList.size() == 0) {
							userBiometricFPEntityListToRemove.add(userBioEntity);
						}
					});

			userEntity.getUserBiometrics().stream().filter(Objects::nonNull)
					.filter(userBiometricEntity -> userBiometricEntity.getType().equalsIgnoreCase("FACE"))
					.forEach(userBioEntity -> {
						List<UserBiometric> userBiometricsList = user.getUserBiometrics().stream()
								.filter(e -> e.getType().equalsIgnoreCase("FACE")).collect(Collectors.toList());
						if (userBiometricsList.size() == 0) {
							userBiometricFPEntityListToRemove.add(userBioEntity);
						}
					});
			userEntity.getUserBiometrics().stream().filter(Objects::nonNull)
					.filter(userBiometricEntity -> userBiometricEntity.getType().equalsIgnoreCase("IRIS"))
					.forEach(userBioEntity -> {
						List<UserBiometric> userBiometricsList = user.getUserBiometrics().stream()
								.filter(e -> e.getType().equalsIgnoreCase("IRIS")).collect(Collectors.toList());
						if (userBiometricsList.size() == 0) {
							userBiometricFPEntityListToRemove.add(userBioEntity);
						}
					});
			userEntity.getUserBiometrics().stream().filter(Objects::nonNull)
					.filter(userBiometricEntity -> userBiometricEntity.getType().equalsIgnoreCase("SIGNATURE"))
					.forEach(userBioEntity -> {
						List<UserBiometric> userBiometricsList = user.getUserBiometrics().stream()
								.filter(e -> e.getType().equalsIgnoreCase("SIGNATURE")).collect(Collectors.toList());
						if (userBiometricsList.size() == 0) {
							userBiometricFPEntityListToRemove.add(userBioEntity);
						}
					});
			for (UserBiometric userBiometric : userBiometrics) {
				switch (userBiometric.getType()) {
				case "FACE":
					if (userEntity.getUserBiometrics().size() > 0) {
						List<UserBiometricEntity> userBiometricEntityList = userEntity.getUserBiometrics().stream()
								.filter(Objects::nonNull)
								.filter(userBiometricEntity -> userBiometricEntity.getType().equalsIgnoreCase("FACE"))
								.collect(Collectors.toList());
						UserBiometricEntity userBiometricEntity = new UserBiometricEntity();
						if (userBiometricEntityList.size() != 0) {
							userBiometricEntity = userBiometricEntityList.get(0);
						} else {
							userBiometricEntity.setUser(userEntity);
						}
						userBiometricEntity.setType(userBiometric.getType());
						userBiometricEntity.setPosition(userBiometric.getPosition());
						userBiometricEntity.setFormat(userBiometric.getFormat());
						userBiometricEntity.setData(userBiometric.getData());
						userBiometricEntity.setAnsi(userBiometric.getAnsi());
						userBiometricEntity.setImageQuality(userBiometric.getImageQuality());
						userBiometricRepository.save(userBiometricEntity);
					} else {
						continue;
					}
					break;

				case "FINGERPRINT":
					// edit fp or add if any added newly
					if (userEntity.getUserBiometrics().size() > 0) {
						List<UserBiometricEntity> userBiometricEntityList = userEntity.getUserBiometrics().stream()
								.filter(Objects::nonNull)
								.filter(userBiometricEntity -> userBiometricEntity.getType()
										.equalsIgnoreCase("FINGERPRINT"))
								.filter(userBiometricEntity -> userBiometricEntity.getPosition()
										.equalsIgnoreCase(userBiometric.getPosition()))
								.collect(Collectors.toList());
						UserBiometricEntity userBiometricEntity = new UserBiometricEntity();
						if (userBiometricEntityList.size() != 0) {
							userBiometricEntity = userBiometricEntityList.get(0);
						} else {
							userBiometricEntity.setUser(userEntity);
						}
						userBiometricEntity.setType(userBiometric.getType());
						userBiometricEntity.setPosition(userBiometric.getPosition());
						userBiometricEntity.setFormat(userBiometric.getFormat());
						userBiometricEntity.setData(userBiometric.getData());
						userBiometricEntity.setAnsi(userBiometric.getAnsi());
						userBiometricEntity.setImageQuality(userBiometric.getImageQuality());
						userBiometricEntity.setWsqData(userBiometric.getWsqData());
						userBiometricRepository.save(userBiometricEntity);
					}
					break;
				case "IRIS":
					if (userEntity.getUserBiometrics().size() > 0) {
						List<UserBiometricEntity> userBiometricEntityList = userEntity.getUserBiometrics().stream()
								.filter(Objects::nonNull)
								.filter(userBiometricEntity -> userBiometricEntity.getType().equalsIgnoreCase("IRIS"))
								.filter(userBiometricEntity -> userBiometricEntity.getPosition()
										.equalsIgnoreCase(userBiometric.getPosition()))
								.collect(Collectors.toList());
						UserBiometricEntity userBiometricEntity = new UserBiometricEntity();
						if (userBiometricEntityList.size() != 0) {
							userBiometricEntity = userBiometricEntityList.get(0);
						} else {
							userBiometricEntity.setUser(userEntity);
						}
						userBiometricEntity.setType(userBiometric.getType());
						userBiometricEntity.setPosition(userBiometric.getPosition());
						userBiometricEntity.setFormat(userBiometric.getFormat());
						userBiometricEntity.setData(userBiometric.getData());
						userBiometricEntity.setAnsi(userBiometric.getAnsi());
						userBiometricEntity.setImageQuality(userBiometric.getImageQuality());
						userBiometricRepository.save(userBiometricEntity);
					}
					break;
				case "SIGNATURE":
					if (userEntity.getUserBiometrics().size() > 0) {
						List<UserBiometricEntity> userBiometricEntityList = userEntity.getUserBiometrics().stream()
								.filter(Objects::nonNull).filter(userBiometricEntity -> userBiometricEntity.getType()
										.equalsIgnoreCase("SIGNATURE"))
								.collect(Collectors.toList());
						UserBiometricEntity userBiometricEntity = new UserBiometricEntity();
						if (userBiometricEntityList.size() != 0) {
							userBiometricEntity = userBiometricEntityList.get(0);
						} else {
							userBiometricEntity.setUser(userEntity);
						}
						userBiometricEntity.setType(userBiometric.getType());
						userBiometricEntity.setPosition(userBiometric.getPosition());
						userBiometricEntity.setFormat(userBiometric.getFormat());
						userBiometricEntity.setData(userBiometric.getData());
						userBiometricEntity.setAnsi(userBiometric.getAnsi());
						userBiometricEntity.setImageQuality(userBiometric.getImageQuality());
						userBiometricRepository.save(userBiometricEntity);
					}
					break;
				}
			}
			for (UserBiometricEntity obj : userBiometricFPEntityListToRemove) {
				userEntity.removeBiometric(obj);
				userBiometricRepository.delete(obj.getId());
			}
			/*
			 * // User Biometric Attributes
			 * userBiometric.getUserBiometricAttributes().forEach((userBiometricAttribute)
			 * -> { UserBiometricAttributeEntity userBiometricAttributeEntity = new
			 * UserBiometricAttributeEntity();
			 * userBiometricAttributeEntity.setName(userBiometricAttribute.getName());
			 * userBiometricAttributeEntity.setValue(userBiometricAttribute.getValue());
			 * 
			 * userBiometricEntity.addUserBiometricAttribute(userBiometricAttributeEntity);
			 * });
			 */

			// userBiometricRepository.save(userBiometricEntity);

			// User Biometric Skip
//			if(userEntity.getUserBiometricSkips() != null && !userEntity.getUserBiometricSkips().isEmpty()) {				
//				userBiometricSkipRepository.delete(userEntity.getUserBiometricSkips());
//			}
//			List<UserBiometricSkip> userBiometricSkips = user.getUserBiometricSkips();
//			for (UserBiometricSkip userBiometricSkip : userBiometricSkips) {
//
//				UserBiometricSkipEntity userBiometricSkipEntity = new UserBiometricSkipEntity();
//				userBiometricSkipEntity.setType(userBiometricSkip.getType());
//				userBiometricSkipEntity.setPosition(userBiometricSkip.getPosition());
//				userBiometricSkipEntity.setSkipType(userBiometricSkip.getSkipType());
//				userBiometricSkipEntity.setSkipReason(userBiometricSkip.getSkipReason());
//
//				
//					userBiometricSkipEntity.setUser(userEntity);
//					userBiometricSkipRepository.save(userBiometricSkipEntity);
//				
//			}

			userRepository.save(userEntity);

			/*
			 * if(fingerprintModalities.size() > 0 || faceModality.getFaceImg() != null) {
			 * //Insert records in Nserver userBioInfo.setUserID(userEntity.getId()); //
			 * true needs to be fetched from configuration
			 * userBioInfo.setDeDuplicationCheck(true);
			 * userBioInfo.setFingerprintModalities(fingerprintModalities);
			 * userBioInfo.setFaceModality(faceModality); try { String response =
			 * fingerprintService.enroll(userBioInfo); if(response !=
			 * NBiometricStatus.OK.name()) { userRepository.delete(userEntity); //return
			 * "Fail"; } }catch(Exception e ) { // Getting Connection timed out exception
			 * repeatedly.... Network team need to look into this
			 * userRepository.delete(userEntity); //return "Fail"; }
			 * 
			 * }
			 */

			JSONObject userJsonObject = new JSONObject();
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(user.getName(), organizationName);
			if(userRepresentation.getFederationLink() == null) {
				userJsonObject.put("firstName", user.getUserAttribute().getFirstName());
				userJsonObject.put("lastName", user.getUserAttribute().getLastName());
				userJsonObject.put("email", user.getUserAttribute().getEmail());
				keycloakService.updateUser(userRepresentation.getId(), userJsonObject, organizationName, user.getName());
			} else {
				System.out.println("user is from Ldap");
				// then this user is Ldap user so we can't edit the user attributes for this user, and also it depends on the use federation configuration
			}
			
			log.info(ApplicationConstants.notifyCreate, ErrorMessages.USER_ENROLLED, user.getName());
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.USER_NOT_ENROLLED, user.getName()),
					cause);
			throw new EnrollmentServiceException(cause, ErrorMessages.USER_NOT_ENROLLED, user.getName());
		}
	}

	@Override
	public User getAllUserDetails(String orgName, String name)
			throws EntityNotFoundException, InconsistentDataException {
		User user = new User();
		boolean isNewLdapUser = false;
		user.setOrganisationName(orgName);
		user.setIsNewLdapUser(false);
		try {
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(name,
					orgName);
			if (userRepresentation == null) {
				log.info(ApplicationConstants.notifySearch,
						ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER,
						name);
				throw new InconsistentDataException(
						Util.format(ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER,
								name));
			}
			UserAttribute userAttribute = new UserAttribute();
			userAttribute.setFirstName(userRepresentation.getFirstName());
			userAttribute.setLastName(userRepresentation.getLastName());
			userAttribute.setEmail(userRepresentation.getEmail());
			
			user.setName(userRepresentation.getUsername());
			user.setUserAttribute(userAttribute);
			/*
			 * user.setEmail(userRepresentation.getEmail());
			 * user.setFirstName(userRepresentation.getFirstName());
			 * user.setLastName(userRepresentation.getLastName());
			 */
			UserEntity userEntity = userRepository.getUserByName(name, orgName);
			if (userEntity != null) {
				user.setId(userEntity.getId());
				user.setStatus(userEntity.getStatus());
				if(userEntity.getStatus().equals(UserStatusEnum.PENDING_ENROLLMENT.name())) {
					if(userRepresentation.getFederationLink() != null) {
						isNewLdapUser = true;
						user.setIsNewLdapUser(true);
					}
				}
				if (userEntity.getUserAttribute() != null) {
					user.setUserAttribute(getUserAttributes(userEntity.getUserAttribute()));
				} else {
					log.info(ApplicationConstants.notifySearch,
							Util.format(ErrorMessages.USER_ATTRIBUTES_NOT_FOUND_WITH_USERNAME, name));
				}
				if(userEntity.getUserAddress() != null) {
					user.setUserAddress(getUserAddress(userEntity.getUserAddress()));
				}
				if (userEntity.getUserAppearance() != null) {
					UserAppearanceEntity UserAppearanceEntity = userEntity.getUserAppearance();
					UserAppearance userAppearance = new UserAppearance(UserAppearanceEntity.getEyeColor(),
							UserAppearanceEntity.getHeight(), UserAppearanceEntity.getWeight(),
							UserAppearanceEntity.getHairColor());
					user.setUserAppearance(userAppearance);
				} else {
					log.info(ApplicationConstants.notifySearch,
							Util.format(ErrorMessages.USER_APPEARANCE_NOT_FOUND_WITH_USERNAME, name));
				}

				if (userEntity.getUserBiometrics() != null) {
					user.setUserBiometrics(getUserBiometrics(userEntity.getUserBiometrics()));
				} else {
					log.info(ApplicationConstants.notifySearch,
							Util.format(ErrorMessages.USER_BIOMETRICS_NOT_FOUND_WITH_USERNAME, name));
				}

				user.setUserIDProofDocuments(getUserIDProofDocs(userEntity.getUserIDProofDocuments()));
				
				List<UserDrivingLicenseEntity> userDrivingLicence = userEntity.getUserDrivingLicenses();
				if (userDrivingLicence != null) {
					userDrivingLicence.forEach(dl -> {
						Date dateOfIssuance = null; Date dateOfExpiry = null;
						Date under18Until = null; Date under19Until = null; Date under21Until = null;
						if(dl.getDateOfIssuance() != null) {
							dateOfIssuance = Date.from(dl.getDateOfIssuance());
						}
						if(dl.getDateOfExpiry() != null) {
							dateOfExpiry = Date.from(dl.getDateOfExpiry());
						}
						if(dl.getUnder18Until() != null) {
							under18Until = Date.from(dl.getUnder18Until());
						}
						if(dl.getUnder19Until() != null) {
							under19Until = Date.from(dl.getUnder19Until());
						}
						if(dl.getUnder21Until() != null) {
							under21Until = Date.from(dl.getUnder21Until());
						}
						UserDrivingLicense userDL = new UserDrivingLicense(dl.getUimsId(), dl.getLicenseNumber(), dl.getDocumentDescriminator(), dl.getIssuingAuthority(), dl.getLicenseCategory(),
								dateOfIssuance, dateOfExpiry, dl.getRestrictions(), dl.getEndorsements(), dl.getVehicleClass(), 
								under18Until, under19Until, under21Until, dl.getAuditInformation());
						user.setUserDrivingLicense(userDL);
					});
				} else {
					log.info(ApplicationConstants.notifySearch,
							ErrorMessages.USER_DRIVINGLICENSE_NOT_FOUND_WITH_USERNAME, name);
				}
				
				List<UserEmployeeIDEntity> userEmployeeIDEntities = userEntity.getUserEmployeeIDs();
				if (userEmployeeIDEntities != null) {
					userEmployeeIDEntities.forEach(userEmployeeIDEntity -> {
						Date dateOfIssuance = null; Date dateOfExpiry = null;
						if(userEmployeeIDEntity.getDateOfIssuance() != null) {
							dateOfIssuance = Date.from(userEmployeeIDEntity.getDateOfIssuance());
						}
						if(userEmployeeIDEntity.getDateOfExpiry() != null) {
							dateOfExpiry = Date.from(userEmployeeIDEntity.getDateOfExpiry());
						}
						UserEmployeeID userEmployeeID = new UserEmployeeID(userEmployeeIDEntity.getId(), userEmployeeIDEntity.getUimsId(), userEmployeeIDEntity.getEmployeeIDNumber(), dateOfIssuance, dateOfExpiry,
								userEmployeeIDEntity.getAffiliation(), userEmployeeIDEntity.getDepartment(), userEmployeeIDEntity.getIssuerIN(),userEmployeeIDEntity.getAgencyCardSn());
						user.setUserEmployeeID(userEmployeeID);
					});
				} else {
					log.info(ApplicationConstants.notifySearch,
							ErrorMessages.USER_EMPLOYEEID_NOT_FOUND_WITH_USERNAME, name);
				}
				
				List<UserStudentIDEntity> userStudentIDs = userEntity.getUserStudentIDs();
				if (userStudentIDs != null) {
					userStudentIDs.forEach(userStudentIDEntity -> {
						Date dateOfIssuance = null; Date dateOfExpiry = null;
						if(userStudentIDEntity.getDateOfIssuance() != null) {
							dateOfIssuance = Date.from(userStudentIDEntity.getDateOfIssuance());
						}
						if(userStudentIDEntity.getDateOfExpiry() != null) {
							dateOfExpiry = Date.from(userStudentIDEntity.getDateOfExpiry());
						}
						UserStudentID userStudentID = new UserStudentID(userStudentIDEntity.getId(), userStudentIDEntity.getUimsId(), userStudentIDEntity.getStudentIdNumber(), dateOfIssuance, dateOfExpiry,
								userStudentIDEntity.getInstitution(), userStudentIDEntity.getDepartment(), userStudentIDEntity.getIssuerIN(),userStudentIDEntity.getAgencyCardSn());
						user.setUserStudentID(userStudentID);
					});
				} else {
					log.info(ApplicationConstants.notifySearch,
							ErrorMessages.USER_STUDENTID_NOT_FOUND_WITH_USERNAME, name);
				}
				
				
				List<UserPivEntity> UserPivEntities = userEntity.getUserPivCredentials();
				if (UserPivEntities != null) {
					UserPivEntities.forEach(UserPivEntity -> {
						UserPIVModel userPIVModel = new UserPIVModel(UserPivEntity.getId(), UserPivEntity.getUimsID(), UserPivEntity.getPivIdNumber(),
								UserPivEntity.getEmployeeAffiliation(), UserPivEntity.getEmployeeAffiliationColorCode(), UserPivEntity.getAgencyCardSerialNumber(), 
								UserPivEntity.getAgency(), UserPivEntity.getIssuerIdentificationNumber(), 
								Util.formatDate(UserPivEntity.getDateOfIssuance(),Util.ddmmYYYY_format), Util.formatDate(UserPivEntity.getDateOfExpiry(),Util.ddmmYYYY_format),
								UserPivEntity.getCardExpiry(), UserPivEntity.getRank());
						user.setUserPIV(userPIVModel);
					});
				} else {
					log.info(ApplicationConstants.notifySearch,
							ErrorMessages.USER_PIVID_NOT_FOUND_WITH_USERNAME, name);
				}
				
				List<UserEnrollmentHistoryEntity> userEnrollmentHistoryEntityList=new ArrayList<>();
				List<UserEnrollmentHistory> userEnrollmentHistoryList=new ArrayList<>();
				userEnrollmentHistoryEntityList=userEnrollmentHistoryRepository.getByUserId(userEntity.getId());
				if(userEnrollmentHistoryEntityList!=null) {
					userEnrollmentHistoryEntityList.stream().forEach(userEnrollmentHistoryEntity->{
						UserEnrollmentHistory userEnrollmentHistory=new UserEnrollmentHistory();
						userEnrollmentHistory.setModifiedSteps(userEnrollmentHistoryEntity.getModifiedSteps());
						userEnrollmentHistory.setStatus(userEnrollmentHistoryEntity.getStatus());
						userEnrollmentHistory.setApprovalStatus(userEnrollmentHistoryEntity.getApprovalStatus());
						userEnrollmentHistory.setApprovedBy(userEnrollmentHistoryEntity.getApprovedBy());
						userEnrollmentHistory.setApprovedDate(userEnrollmentHistoryEntity.getApprovedDate());
						userEnrollmentHistory.setReason(userEnrollmentHistoryEntity.getReason());
						userEnrollmentHistory.setEnrolledBy(userEnrollmentHistoryEntity.getEnrolledBy());
						userEnrollmentHistory.setEnrollmentStartDate(userEnrollmentHistoryEntity.getEnrollmentStartDate());
						userEnrollmentHistory.setEnrollmentEndDate(userEnrollmentHistoryEntity.getEnrollmentEndDate());
						userEnrollmentHistory.setRejectedBy(userEnrollmentHistoryEntity.getRejectedBy());
						userEnrollmentHistory.setRejectedDate(userEnrollmentHistoryEntity.getRejectedDate());
						userEnrollmentHistoryList.add(userEnrollmentHistory);
					});
					user.setUserEnrollmentHistoryList(userEnrollmentHistoryList);
				}else {
					log.info(ApplicationConstants.notifySearch,
							ErrorMessages.USER_ENROLLMENTHISTORY_NOT_FOUND_WITH_USERNAME, name);
				}
				
				UserNationalIdEntity userNationalIdEntity = userEntity.getUserNationalId();
				if (userNationalIdEntity != null) {
					Date dateOfIssuance = null; Date dateOfExpiry = null;
					if(userNationalIdEntity.getDateOfIssuance() != null) {
						dateOfIssuance = Date.from(userNationalIdEntity.getDateOfIssuance());
					}
					if(userNationalIdEntity.getDateOfExpiry() != null) {
						dateOfExpiry = Date.from(userNationalIdEntity.getDateOfExpiry());
					}
					UserNationalID userNationalID = new UserNationalID(userNationalIdEntity.getId(), userNationalIdEntity.getUimsId(), dateOfIssuance, dateOfExpiry,
							userNationalIdEntity.getDocumentNumber(), userNationalIdEntity.getPersonalCode(), userNationalIdEntity.getVaccineName(), userNationalIdEntity.getVaccinationLocationName(), userNationalIdEntity.getVaccinationLocation(), userNationalIdEntity.getVaccinationDate());
						user.setUserNationalID(userNationalID);
					
				} else {
					log.info(ApplicationConstants.notifySearch,
							ErrorMessages.USER_NATIONALID_NOT_FOUND_WITH_USERNAME, name);
				}
				
				UserVoterIDEntity userVoterIDEntity = userEntity.getUserVoterID();
				if (userVoterIDEntity != null) {
					Date dateOfIssuance = null; Date dateOfExpiry = null;
					if(userVoterIDEntity.getDateOfIssuance() != null) {
						dateOfIssuance = Date.from(userVoterIDEntity.getDateOfIssuance());
					}
					if(userVoterIDEntity.getDateOfExpiry() != null) {
						dateOfExpiry = Date.from(userVoterIDEntity.getDateOfExpiry());
					}
					UserVoterID userVoterID = new UserVoterID(userVoterIDEntity.getId(), userVoterIDEntity.getUimsId(), dateOfIssuance, dateOfExpiry,
							userVoterIDEntity.getCurp(), userVoterIDEntity.getRegistrationYear(), userVoterIDEntity.getElectorKey());
						user.setUserVoterID(userVoterID);
					
				} else {
					log.info(ApplicationConstants.notifySearch,
							ErrorMessages.USER_VOTERID_NOT_FOUND_WITH_USERNAME, name);
				}
				UserPermanentResidentIdEntity userPermanentResidentIdEntity = userEntity.getUserPermanentResidentId();
				if (userPermanentResidentIdEntity != null) {
					Date dateOfIssuance = null; Date dateOfExpiry = null;
					if(userPermanentResidentIdEntity.getDateOfIssuance() != null) {
						dateOfIssuance = Date.from(userPermanentResidentIdEntity.getDateOfIssuance());
					}
					if(userPermanentResidentIdEntity.getDateOfExpiry() != null) {
						dateOfExpiry = Date.from(userPermanentResidentIdEntity.getDateOfExpiry());
					}
					UserPermanentResidentID userPermanentResidentID = new UserPermanentResidentID(userPermanentResidentIdEntity.getId(), userPermanentResidentIdEntity.getUimsId(), dateOfIssuance, dateOfExpiry,
							userPermanentResidentIdEntity.getDocumentNumber(), userPermanentResidentIdEntity.getPersonalCode(), "", userPermanentResidentIdEntity.getPassportNumber(), userPermanentResidentIdEntity.getResidentcardTypeNumber(), userPermanentResidentIdEntity.getResidentcardTypeCode());
						user.setUserPermanentResidentID(userPermanentResidentID);
					
				} else {
					log.info(ApplicationConstants.notifySearch,
							ErrorMessages.USER_PERMANENT_RESIDENT_ID_NOT_FOUND_WITH_USERNAME, name);
				}
				
				List<UserHealthIDEntity> UserHealthIDEntities = userEntity.getUserHealthIDs();
				if (UserHealthIDEntities != null) {
					UserHealthIDEntities.forEach(userHealthIDEntity -> {
						Date dateOfIssuance = null; Date dateOfExpiry = null;
						if(userHealthIDEntity.getDateOfIssuance() != null) {
							dateOfIssuance = Date.from(userHealthIDEntity.getDateOfIssuance());
						}
						if(userHealthIDEntity.getDateOfExpiry() != null) {
							dateOfExpiry = Date.from(userHealthIDEntity.getDateOfExpiry());
						}
						
						UserHealthID userHealthID = new UserHealthID( userHealthIDEntity.getUimsId(), userHealthIDEntity.getIdNumber(),
								userHealthIDEntity.getPrimarySubscriber(), userHealthIDEntity.getPrimarySubscriberID(),
								userHealthIDEntity.getPcp(), userHealthIDEntity.getPcpPhone(), userHealthIDEntity.getPolicyNumber(), userHealthIDEntity.getGroupPlan(),
								userHealthIDEntity.getHealthPlanNumber(), userHealthIDEntity.getIssuerIN(),userHealthIDEntity.getAgencyCardSN(),
								dateOfIssuance, dateOfExpiry);
						user.setUserHealthID(userHealthID);
					});
				} else {
					log.info(ApplicationConstants.notifySearch,
							ErrorMessages.USER_HEALTHID_NOT_FOUND_WITH_USERNAME, name);
				}
				
				UserHealthServiceEntity userHealthServiceEntity = userEntity.getUserHealthService();
				if (userHealthServiceEntity != null) {

					UserHealthService userHealthService = new UserHealthService();

					userHealthService.setIdNumber(userHealthServiceEntity.getIdNumber());
					userHealthService.setIssuingAuthority(userHealthServiceEntity.getIssuingAuthority());
					userHealthService.setPrimarySubscriber(userHealthServiceEntity.getPrimarySubscriber());
					userHealthService.setPrimarySubscriberID(userHealthServiceEntity.getPrimarySubscriberID());
					userHealthService.setPrimaryDoctor(userHealthServiceEntity.getPrimaryDoctor());
					userHealthService.setPrimaryDoctorPhone(userHealthServiceEntity.getPrimaryDoctorPhone());

					List<UserHealthServiceVaccineInfo> userHealthServiceVaccineInfos = new ArrayList<UserHealthServiceVaccineInfo>();

					for (UserHealthServiceVaccineInfoEntity userHealthServiceVaccineInfoEntity : userHealthServiceEntity
							.getUserHealthServiceVaccineInfos()) {

						UserHealthServiceVaccineInfo userHealthServiceVaccineInfo = new UserHealthServiceVaccineInfo(
								userHealthServiceVaccineInfoEntity.getVaccine(),
								userHealthServiceVaccineInfoEntity.getRoute(),
								userHealthServiceVaccineInfoEntity.getSite(),
								userHealthServiceVaccineInfoEntity.getDate(),
								userHealthServiceVaccineInfoEntity.getAdministeredBy());
						userHealthServiceVaccineInfos.add(userHealthServiceVaccineInfo);
					}
					userHealthService.setUserHealthServiceVaccineInfos(userHealthServiceVaccineInfos);

					user.setUserHealthService(userHealthService);

				} else {
					log.info(ApplicationConstants.notifySearch,
							ErrorMessages.USER_HEALTHSERVICEID_NOT_FOUND_WITH_USERNAME, name);
				}

				/*
				 * List<UserEmployeeIDEntity> userEIDList = userEntity.getUserEmployeeIDs();
				 */
			} else {
				//If user is direct from identity broker case
				user.setStatus(UserStatusEnum.PENDING_ENROLLMENT.name());
				if(userRepresentation.getFederationLink() != null) {
					// it means the user is from Ldap
					isNewLdapUser = true;
					user.setIsNewLdapUser(true);
				}
			}
			if(isNewLdapUser) {
				Map<Class<?>, Object> map = userCredentialsService
						.getGroupAndWorkflowByUserName(orgName, userRepresentation.getUsername());
				Workflow workflow = ((Workflow) map.get(Workflow.class));
				keycloakService.getLdapUserAttributes(workflow.getOrganizationIdentities().getIdentityTypeName(), user, userRepresentation);
			}
			/* else {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, name));
				throw new EntityNotFoundException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, name));
			}*/
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, name),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, user.getName());
		}

		log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_FOUND_WITH_USERNAME, name));
		return user;
	}

	private List<UserBiometric> getUserBiometrics(List<UserBiometricEntity> userBiometricsEntities) {
		List<UserBiometric> userBiometricsList = new ArrayList<>();
		userBiometricsEntities.stream().forEach(userBiometrics -> {
			UserBiometric userBiometric = new UserBiometric();
			userBiometric.setType(userBiometrics.getType());
			userBiometric.setPosition(userBiometrics.getPosition());
			userBiometric.setFormat(userBiometrics.getFormat());
			userBiometric.setData(userBiometrics.getData());
			userBiometric.setImageQuality(userBiometrics.getImageQuality());
			userBiometric.setWsqData(userBiometrics.getWsqData());
			userBiometric.setAnsi(userBiometrics.getAnsi());
			userBiometrics.getUserBiometricAttributes().forEach((userBiometricAttributes) -> {
				UserBiometricAttribute userBiometricAttribute = new UserBiometricAttribute();
				userBiometricAttribute.setName(userBiometricAttributes.getName());
				userBiometricAttribute.setValue(userBiometricAttributes.getValue());

				userBiometric.addUserBiometricAttribute(userBiometricAttribute);
			});
			userBiometricsList.add(userBiometric);
		});
		return userBiometricsList;
	}

	private UserAttribute getUserAttributes(UserAttributeEntity userAttr) {
		Date birthDate = null;
		if(userAttr.getDateOfBirth()!= null) {
			birthDate = (Date.from(userAttr.getDateOfBirth()));
		}
		UserAttribute userAttribute = new UserAttribute(birthDate, userAttr.getNationality(),
				userAttr.getPlaceOfBirth(), userAttr.getGender(), userAttr.getContact(), userAttr.getAddress(),
				userAttr.isVeteran(), userAttr.isOrganDonor(), userAttr.getBloodType());
		userAttribute.setFirstName(userAttr.getFirstName());
		userAttribute.setMiddleName(userAttr.getMiddleName());
		userAttribute.setLastName(userAttr.getLastName());
		userAttribute.setMothersMaidenName(userAttr.getMothersMaidenName());
		userAttribute.setEmail(userAttr.getEmail());
		userAttribute.setCountryCode(userAttr.getCountryCode());
		return userAttribute;
	}
	
	private UserAddress getUserAddress(UserAddressEntity userAddressEntity) {
		UserAddress userAddress = null;
		if(userAddressEntity!=null) {
			userAddress = new UserAddress();
			userAddress.setAddressLine1(userAddressEntity.getAddressLine1());
			userAddress.setAddressLine2(userAddressEntity.getAddressLine2());
			userAddress.setCity(userAddressEntity.getCity());
			userAddress.setState(userAddressEntity.getState());
			userAddress.setCountry(userAddressEntity.getCountry());
			userAddress.setZipCode(userAddressEntity.getZipCode());
			return userAddress;
		} else {
			return null;
		}
		
	}

	private List<UserIDProofDocument> getUserIDProofDocs(List<UserIDProofDocumentEntity> idProofDocEntities) {
		List<UserIDProofDocument> idProofDocs = new ArrayList<>();
		idProofDocEntities.stream().filter(Objects::nonNull).forEach(userIDProofDocEntity -> {
			UserIDProofDocument UserIDProofDocument = new UserIDProofDocument();

			UserIDProofDocument.setId(userIDProofDocEntity.getId());
			UserIDProofDocument.setName(userIDProofDocEntity.getName());
			UserIDProofDocument.setFrontImg(userIDProofDocEntity.getFrontImg());
			UserIDProofDocument.setBackImg(userIDProofDocEntity.getBackImg());
			UserIDProofDocument.setFile(userIDProofDocEntity.getFileData());
			UserIDProofDocument.setFrontFileType(userIDProofDocEntity.getFrontFileType());
			UserIDProofDocument.setBackFileType(userIDProofDocEntity.getBackFileType());
			UserIDProofDocument.setFrontFileName(userIDProofDocEntity.getFrontFileName());
			UserIDProofDocument.setBackFileName(userIDProofDocEntity.getBackFileName());
			
			OrganizationIdentitiesEntity orgIddentityEntity = userIDProofDocEntity.getOrganizationIdentity();//new OrganizationIdentitiesEntity();
			OrganizationIdentity orgIdentity = new OrganizationIdentity();
			orgIdentity.setId(orgIddentityEntity.getId());
			orgIdentity.setIdentityTypeName(orgIddentityEntity.getIdentityType().getName());

			UserIDProofDocument.setIdProofType(orgIdentity);

			IDProofIssuingAuthorityEntity idProofIsuuingAuthEntity = userIDProofDocEntity.getIdProofIssuingAuthority();
			IDProofIssuingAuthority idProofIssuingAuthority = new IDProofIssuingAuthority();
			idProofIssuingAuthority.setId(idProofIsuuingAuthEntity.getId());
			idProofIssuingAuthority.setIssuingAuthority(idProofIsuuingAuthEntity.getIssuingAuthority());
			UserIDProofDocument.setIdProofIssuingAuthority(idProofIssuingAuthority);

			idProofDocs.add(UserIDProofDocument);
		});
		return idProofDocs;
	}

	@Override
	public User getUserByName(String organizationName, String name) throws EntityNotFoundException {
		User user = null;
		try {
			user = getUserInfo(userRepository.getUserByName(name, organizationName));
			if (user == null) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, name));
				//throw new EntityNotFoundException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, name));
				UserRepresentation userRepresentation = keycloakService.getUserByUsername(name, organizationName);
				if (userRepresentation == null) {
					log.info(ApplicationConstants.notifySearch,
							ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER, name);
					//throw new InconsistentDataException(
						//	Util.format(ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER, name));
					return user;
				} else {
					user =new User();
					user.setName(userRepresentation.getUsername());
					user.setEmail(userRepresentation.getEmail());
					user.setFirstName(userRepresentation.getFirstName());
					user.setLastName(userRepresentation.getLastName());
					user.setEmail(userRepresentation.getEmail());
					user.setStatus(UserStatusEnum.PENDING_ENROLLMENT.name());
				}
				
				return user;
			} else {
				Workflow workflow = getWorkflowByUserName(organizationName, name);
				user.setWorkflow(workflow);
				if(workflow != null) {
					WorkflowEntity wfEntity = workflowRepository.findByName(organizationName, workflow.getName());
					if (wfEntity != null) {
						List<DeviceProfile> deviceProfiles = new ArrayList<>();
						workflowRepository.findWorkflowDeviceProfiles(organizationName, workflow.getName()).stream()
								.forEach(deviceProfileName -> {
									DeviceProfileEntity devProfEtity = deviceProfileRepository
											.findByNameAndOrganization(organizationName, deviceProfileName);
									
									DeviceProfileConfigEntity deviceProfileConfig = deviceProfileConfigRepository.getDevProfConfigByConfigValueID(devProfEtity.getConfigValueProductName().getId());
									DeviceProfile deviceProfile = new DeviceProfile();
									ConfigValue configValueCategory=new ConfigValue();
									ConfigValue configValueSupplier=new ConfigValue();
									deviceProfile.setName(devProfEtity.getName());
									deviceProfile.setConfigValueProductName(new ConfigValue(devProfEtity.getConfigValueProductName().getId(), devProfEtity.getConfigValueProductName().getValue()));
									
									DeviceProfileConfig devProfConfig = new DeviceProfileConfig();
									devProfConfig.setIsMobileId(deviceProfileConfig.getIsMobileId());
									devProfConfig.setIsPlasticId(deviceProfileConfig.getIsPlasticId());
									devProfConfig.setIsRfidCard(deviceProfileConfig.getIsRfidCard());
									devProfConfig.setIsAppletLoadingRequired(deviceProfileConfig.getIsAppletLoadingRequired());
									configValueCategory.setValue(devProfEtity.getConfigValueCategory().getValue());
									configValueSupplier.setValue(devProfEtity.getConfigValueSupplier().getValue());
									deviceProfile.setDeviceProfileConfig(devProfConfig);
									deviceProfile.setConfigValueCategory(configValueCategory);
									deviceProfile.setConfigValueSupplier(configValueSupplier);
									deviceProfiles.add(deviceProfile);
								});
						user.setWfDeviceProfiles(deviceProfiles);
					}
				}
			}			
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, name),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, name);
		}

		log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_FOUND_WITH_USERNAME, name));
		return user;
	}

	@Override
	public User getUserByID(Long id, String gpsCoordinates) throws EntityNotFoundException {
		User user = null;
		UserEntity userEntity = null;
		try {

			if (id != null && id.longValue() != 0l) {
				userEntity = userRepository.findOne(id);
			}
			if (userEntity != null) {
				/*
				 * user = getUserInfo(userEntity); user = setUserBiometrics(userEntity);
				 */
				user = getUserInfoForCredential(userEntity);

				if (gpsCoordinates != null && gpsCoordinates.length() > 3) {

					String[] points = gpsCoordinates.split(",");

					if (points.length == 2) {
						String wktPoint = "Point(" + points[0] + " " + points[1] + ")";

						// First interpret the WKT string to a point
						WKTReader fromText = new WKTReader();
						Geometry geom = null;
						try {
							geom = fromText.read(wktPoint);
						} catch (ParseException e) {
							throw new RuntimeException("Not a WKT string:" + wktPoint);
						}
						if (!geom.getGeometryType().equals("Point")) {
							throw new RuntimeException("Geometry must be a point. Got a " + geom.getGeometryType());
						}

						/*
						 * GeometryFactory gf = new GeometryFactory(); Point geom = gf.createPoint(new
						 * Coordinate(-178.87650, 87.00965));
						 */

						UserLocationEntity userlocationentity = null;
						userlocationentity = userLocationRepository.findByUserID(userEntity.getId());
						if (userlocationentity == null) {
							userlocationentity = new UserLocationEntity(userEntity, "name", (Point) geom, new Date());
							userLocationRepository.save(userlocationentity);
						} else {
							userlocationentity.setLocation((Point) geom);
						}

					}
				}

			}
			if (user == null) {
				log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, id));
				throw new EntityNotFoundException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, id));
			}
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(userEntity.getReferenceName(),
					userEntity.getOrganization().getName());
			if (userRepresentation == null) {
				log.info(ApplicationConstants.notifySearch,
						ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER, userEntity.getReferenceName());
				throw new InconsistentDataException(
						Util.format(ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER,
								userEntity.getReferenceName()));
			}
			user.setId((id));
			user.setFirstName(userRepresentation.getFirstName());
			user.setLastName(userRepresentation.getLastName());
			user.setOrganisationName(userEntity.getOrganization().getName());
			user.setOrganisationId(userEntity.getOrganization().getId());
			user.setUserPrincipalName(userRepresentation.getEmail());
			user.setEmail(userRepresentation.getEmail());
			user.setWfMobileIDStepDetails(this.getUserWfMobileIDStepDetails((id)));
			user.setUserStatus(userEntity.getUserStatus());
			user.setEnrollmentStatus(userEntity.getStatus());
			if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name()) || userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_APPROVAL.name())) {
				user.setCanIssueIdentity(false);
			} else {
				user.setCanIssueIdentity(true);
			}
			
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, id),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_WITH_USERID, id);
		}
		log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_FOUND_WITH_USERID, id));
		return user;

	}

	@Override
	public UserDetails getUserDetailsByName(String organizationName, String userName) throws EntityNotFoundException{
		
		return getUserByIDOrEntity(null, getUserEntityByUserName(organizationName, userName));
	}
	
	@Override
	public UserDetails getUserByIDOrEntity(Long userID, UserEntity userEntity) throws EntityNotFoundException {
		UserDetails user = null;
		try {

			if (userID != null && userID.longValue() != 0l) {
				userEntity = userRepository.findOne(userID);
			} 
			
			if (userEntity != null) {		
				userID = userEntity.getId();
				user = getUserBiometricDetails(userEntity);			
			
				if (user == null) {
					log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userID));
					throw new EntityNotFoundException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userID));
				}
				UserRepresentation userRepresentation = keycloakService.getUserByUsername(userEntity.getReferenceName(),
						userEntity.getOrganization().getName());
				if (userRepresentation == null) {
					log.info(ApplicationConstants.notifySearch,
							ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER, userEntity.getReferenceName());
					throw new InconsistentDataException(
							Util.format(ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER,
									userEntity.getReferenceName()));
				}
				user.setId((userID));
				user.setName(userEntity.getReferenceName());
				user.setFullName(userEntity.getName());
				user.setStatus(userEntity.getStatus());
				user.setGroupId(userEntity.getGroupId());
				user.setGroupName(userEntity.getGroupName());
				user.setFirstName(userRepresentation.getFirstName());
				user.setLastName(userRepresentation.getLastName());
				user.setOrganisationName(userEntity.getOrganization().getName());
				user.setOrganisationId(userEntity.getOrganization().getId());
				user.setUserPrincipalName(userRepresentation.getEmail());
				user.setEmail(userRepresentation.getEmail());
				user.setWfMobileIDStepDetails(this.getUserWfMobileIDStepDetails((userID)));
				user.setUserStatus(userEntity.getUserStatus());
				user.setEnrollmentStatus(userEntity.getStatus());
				if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name()) || userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_APPROVAL.name())) {
					user.setCanIssueIdentity(false);
				} else {
					user.setCanIssueIdentity(true);
				}
			}
			
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userID),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_WITH_USERID, userID);
		}
		log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_FOUND_WITH_USERID, userID));
		return user;

	}
	
	@Override
	public AuthenticatorClientInfo getAuthenticatorClientInfo(String organizationName, String publicKeyB64, String publicKeyAlgorithm) throws CryptoOperationException, IdentityBrokerServiceException {
			
			return keycloakService.getAuthenticatorClientInfo(organizationName, publicKeyB64, publicKeyAlgorithm);

	}
	
	@Override
	public AuthenticatorClientInfo getOrgClientCredentials(String organizationName) throws CryptoOperationException, IdentityBrokerServiceException {
			
			return keycloakService.getOrgOIDCClientCredentials(organizationName, aPIGatewayInfoRepository.getAPIGatewayInfoByOrgName(organizationName));

	}
	
	@Override
	public User getUserByID(String id) throws EntityNotFoundException {
		User user = null;
		UserEntity userEntity = null;
		try {

			if (id != null && !id.equalsIgnoreCase("null")) {
				userEntity = userRepository.findOne(Long.parseLong(id));
			}
			if (userEntity != null) {
				/*
				 * user = getUserInfo(userEntity); user = setUserBiometrics(userEntity);
				 */
				user = getUserInfoForCredential(userEntity);

			}
			if (user == null) {
				log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, id));
				throw new EntityNotFoundException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, id));
			}
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(userEntity.getReferenceName(),userEntity.getOrganization().getName());
			if (userRepresentation == null) {
				log.info(ApplicationConstants.notifySearch,
						ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER, userEntity.getReferenceName());
				throw new InconsistentDataException(
						Util.format(ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER,
								userEntity.getReferenceName()));
			}
			user.setFirstName(userRepresentation.getFirstName());
			user.setLastName(userRepresentation.getLastName());
			user.setOrganisationName(userEntity.getOrganization().getName());
			user.setUserPrincipalName(userRepresentation.getEmail());
			user.setEmail(userRepresentation.getEmail());
			user.setWfMobileIDStepDetails(this.getUserWfMobileIDStepDetails(Long.parseLong(id)));

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, id),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_WITH_USERID, id);
		}
		log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_FOUND_WITH_USERID, id));
		return user;

	}

	private User setUserBiometrics(UserEntity userEntity) {
		User user = new User();
		user.setId(userEntity.getId());
		user.setName(userEntity.getReferenceName());

		List<UserBiometric> userBiometricsList = new ArrayList<>();
		if (userEntity.getUserBiometrics() != null) {
			userEntity.getUserBiometrics().stream().forEach(userBiometrics -> {
				if (userBiometrics.getType().equals("FACE") || userBiometrics.getType().equals("SIGNATURE")) {
					UserBiometric userBiometric = new UserBiometric();
					userBiometric.setType(userBiometrics.getType());
					userBiometric.setPosition(userBiometrics.getPosition());
					userBiometric.setFormat(userBiometrics.getFormat());
					userBiometric.setData(userBiometrics.getData());
					userBiometric.setAnsi(userBiometrics.getAnsi());
					userBiometric.setImageQuality(userBiometrics.getImageQuality());

					userBiometricsList.add(userBiometric);
				}
			});
			user.setUserBiometrics(userBiometricsList);
		} else {
			log.info(ApplicationConstants.notifySearch,
					Util.format(ErrorMessages.USER_BIOMETRICS_NOT_FOUND_WITH_USERNAME, userEntity.getReferenceName()));
		}
		return user;
	}

	private User getUserInfo(UserEntity userEntity) {
		User user = null;

		if (userEntity != null) {
			user = new User();
			user.setId(userEntity.getId());
			user.setName(userEntity.getReferenceName());
			user.setFullName(userEntity.getName());
			user.setStatus(userEntity.getStatus());
			user.setGroupId(userEntity.getGroupId());
			user.setGroupName(userEntity.getGroupName());
			if (userEntity.getUserAttribute() != null) {
				UserAttributeEntity userAttr = userEntity.getUserAttribute();
				Date birthDate = null;
				if(userAttr.getDateOfBirth()!= null) {
					birthDate = (Date.from(userAttr.getDateOfBirth()));
				}
				UserAttribute userAttribute = new UserAttribute(birthDate, userAttr.getNationality(),
						userAttr.getPlaceOfBirth(), userAttr.getGender(), userAttr.getContact(), userAttr.getAddress(),
						userAttr.isVeteran(), userAttr.isOrganDonor(), userAttr.getBloodType());
				user.setFirstName(userAttr.getFirstName());
				user.setLastName(userAttr.getLastName());
				userAttribute.setFirstName(userAttr.getFirstName());
				userAttribute.setLastName(userAttr.getLastName());
				userAttribute.setMiddleName(userAttr.getMiddleName());
				userAttribute.setMothersMaidenName(userAttr.getMothersMaidenName());
				userAttribute.setEmail(userAttr.getEmail());
				user.setUserAttribute(userAttribute);
			} else {
				log.info(ApplicationConstants.notifySearch, Util
						.format(ErrorMessages.USER_ATTRIBUTES_NOT_FOUND_WITH_USERNAME, userEntity.getReferenceName()));
			}
			if (userEntity.getUserAppearance() != null) {
				UserAppearanceEntity UserAppearanceEntity = userEntity.getUserAppearance();

				UserAppearance userAppearance = new UserAppearance(UserAppearanceEntity.getEyeColor(),
						UserAppearanceEntity.getHeight(), UserAppearanceEntity.getWeight(),
						UserAppearanceEntity.getHairColor());

				user.setUserAppearance(userAppearance);
			} else {
				log.info(ApplicationConstants.notifySearch, Util
						.format(ErrorMessages.USER_APPEARANCE_NOT_FOUND_WITH_USERNAME, userEntity.getReferenceName()));
			}
			if(userEntity.getUserAddress() != null) {
				user.setUserAddress(getUserAddress(userEntity.getUserAddress()));
			}
			List<UserBiometric> userBiometricsList = new ArrayList<>();
			if (userEntity.getUserBiometrics() != null) {
				userEntity.getUserBiometrics().stream().forEach(userBiometrics -> {
					UserBiometric userBiometric = new UserBiometric();
					userBiometric.setType(userBiometrics.getType());
					userBiometric.setPosition(userBiometrics.getPosition());
					userBiometric.setFormat(userBiometrics.getFormat());
					userBiometric.setData(userBiometrics.getData());
					userBiometric.setAnsi(userBiometrics.getAnsi());
					userBiometric.setImageQuality(userBiometrics.getImageQuality());
					userBiometric.setWsqData(userBiometrics.getWsqData());

					userBiometrics.getUserBiometricAttributes().forEach((userBiometricAttributes) -> {
						UserBiometricAttribute userBiometricAttribute = new UserBiometricAttribute();
						userBiometricAttribute.setName(userBiometricAttributes.getName());
						userBiometricAttribute.setValue(userBiometricAttributes.getValue());

						userBiometric.addUserBiometricAttribute(userBiometricAttribute);
					});
					userBiometricsList.add(userBiometric);
				});
				user.setUserBiometrics(userBiometricsList);
			} else {
				log.info(ApplicationConstants.notifySearch, Util
						.format(ErrorMessages.USER_BIOMETRICS_NOT_FOUND_WITH_USERNAME, userEntity.getReferenceName()));
			}
		}
		return user;
	}
	
	private UserDetails getUserBiometricDetails(UserEntity userEntity) {
		UserDetails userSyncDetails = null;

		if (userEntity != null) {
			userSyncDetails = new UserDetails();
			
			List<UserBiometric> userBiometricsList = new ArrayList<>();
			if (userEntity.getUserBiometrics() != null) {
				for (UserBiometricEntity userBiometrics : userEntity.getUserBiometrics()) {
					if (userBiometrics.getType().equals("FACE")) {
						UserBiometric userBiometric = new UserBiometric();
						userBiometric.setType(userBiometrics.getType());
						userBiometric.setPosition(userBiometrics.getPosition());
						userBiometric.setFormat(userBiometrics.getFormat());
						userBiometric.setData(userBiometrics.getData());
						userBiometric.setAnsi(userBiometrics.getAnsi());
						userBiometric.setImageQuality(userBiometrics.getImageQuality());

						userBiometricsList.add(userBiometric);
					}
				}
				userSyncDetails.setUserBiometrics(userBiometricsList);
			} else {
				log.info(ApplicationConstants.notifySearch, Util
						.format(ErrorMessages.USER_BIOMETRICS_NOT_FOUND_WITH_USERNAME, userEntity.getReferenceName()));
			}
		}
		return userSyncDetails;
	}

	private User getUserInfoForCredential(UserEntity userEntity) {
		User user = null;

		if (userEntity != null) {
			user = new User();
			user.setId(userEntity.getId());
			user.setName(userEntity.getReferenceName());
			user.setOrganisationName(userEntity.getOrganization().getName());
			if (userEntity.getUserAttribute() != null) {
				UserAttributeEntity userAttr = userEntity.getUserAttribute();

				Date birthDate = null;
				if(userAttr.getDateOfBirth()!= null) {
					birthDate = (Date.from(userAttr.getDateOfBirth()));
				}
				UserAttribute userAttribute = new UserAttribute(birthDate, userAttr.getNationality(),
						userAttr.getPlaceOfBirth(), userAttr.getGender(), userAttr.getContact(), userAttr.getAddress(),
						userAttr.isVeteran(), userAttr.isOrganDonor(), userAttr.getBloodType());
				userAttribute.setFirstName(userAttr.getFirstName());
				userAttribute.setMiddleName(userAttr.getMiddleName());
				userAttribute.setMothersMaidenName(userAttr.getMiddleName());
				userAttribute.setEmail(userAttr.getEmail());
				user.setUserAttribute(userAttribute);
			} else {
				log.info(ApplicationConstants.notifySearch, Util
						.format(ErrorMessages.USER_ATTRIBUTES_NOT_FOUND_WITH_USERNAME, userEntity.getReferenceName()));
			}
			if(userEntity.getUserAddress() != null) {
				user.setUserAddress(getUserAddress(userEntity.getUserAddress()));
			}
			if (userEntity.getUserAppearance() != null) {
				UserAppearanceEntity UserAppearanceEntity = userEntity.getUserAppearance();

				UserAppearance userAppearance = new UserAppearance(UserAppearanceEntity.getEyeColor(),
						UserAppearanceEntity.getHeight(), UserAppearanceEntity.getWeight(),
						UserAppearanceEntity.getHairColor());

				user.setUserAppearance(userAppearance);
			} else {
				log.info(ApplicationConstants.notifySearch, Util
						.format(ErrorMessages.USER_APPEARANCE_NOT_FOUND_WITH_USERNAME, userEntity.getReferenceName()));
			}
			List<UserBiometric> userBiometricsList = new ArrayList<>();
			if (userEntity.getUserBiometrics() != null) {
				userEntity.getUserBiometrics().stream().forEach(userBiometrics -> {
					if (userBiometrics.getType().equals("FACE") || userBiometrics.getType().equals("SIGNATURE")) {
						UserBiometric userBiometric = new UserBiometric();
						userBiometric.setType(userBiometrics.getType());
						userBiometric.setPosition(userBiometrics.getPosition());
						userBiometric.setFormat(userBiometrics.getFormat());
						userBiometric.setData(userBiometrics.getData());
						userBiometric.setAnsi(userBiometrics.getAnsi());
						userBiometric.setImageQuality(userBiometrics.getImageQuality());

						userBiometricsList.add(userBiometric);
					}
				});
				user.setUserBiometrics(userBiometricsList);
			} else {
				log.info(ApplicationConstants.notifySearch, Util
						.format(ErrorMessages.USER_BIOMETRICS_NOT_FOUND_WITH_USERNAME, userEntity.getReferenceName()));
			}
		}
		return user;
	}

	@Override
	public String decryptUserUUID(String encryptedUUID) throws CryptoOperationException {
		String uuid = null;
		try {
			uuid = cryptoFunction.decrypt(applicationProperties.getQrcode().getIvParam().getBytes(),
					applicationProperties.getQrcode().getAesKey().getBytes(),
					Base64.getDecoder().decode((new String(encryptedUUID.getBytes("UTF-8"))).getBytes()));
			log.info(ApplicationConstants.notifyValidate, Util.format(ErrorMessages.UUID_VALIDATED, encryptedUUID));

		} catch (BadPaddingException | IllegalBlockSizeException | InvalidKeyException
				| InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchPaddingException
				| UnsupportedEncodingException cause) {
			uuid = null;
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.UNABLE_TO_VALIDATE_UUID, encryptedUUID), cause);
			throw new CryptoOperationException(cause, ErrorMessages.UNABLE_TO_VALIDATE_UUID, encryptedUUID);
		}
		return uuid;
	}

	@Override
	public UserEntity getUserbyUUID(String organizationName, String encryptedUUID) throws CryptoOperationException, EntityNotFoundException {
		UserEntity userEntity = null;
		String uuid = null;
		try {
			//uuid = decryptUserUUID(encryptedUUID);
			uuid = encryptedUUID;
			if (uuid != null) {
				UserIdentificationCodeEntity userIdentificationCodeEntity = userIdentificationCodeRepository
						.getUserIdentificationCodeByUUID(uuid, organizationName);
				Hibernate.initialize(userIdentificationCodeEntity.getUser());
				userEntity = userIdentificationCodeEntity.getUser();
				PairMobileDeviceConfigEntity pairMobileDeviceConfigEntity = pairMobileDeviceConfigRepository
						.findPMDConfigByOrg(userEntity.getOrganization().getId());
				if (userIdentificationCodeEntity != null) {
					if(pairMobileDeviceConfigEntity.isDisableQRCodeExpiry() == true) {
						log.info(ApplicationConstants.notifySearch,
								Util.format(ErrorMessages.UUID_VALIDATED, encryptedUUID));

					} else {
						if ((userIdentificationCodeEntity.getValidityTill()).compareTo(Instant.now()) > 0) {
							log.info(ApplicationConstants.notifySearch,
									Util.format(ErrorMessages.UUID_VALIDATED, encryptedUUID));
						} else {
							log.info(ApplicationConstants.notifySearch,
									Util.format(ErrorMessages.INVALID_UUID_EITHER_EXPIRED_OR_NOT_ACTIVE, encryptedUUID));
							uuid = null;
						}
					}
				} else {
					log.info(ApplicationConstants.notifySearch,
							Util.format(ErrorMessages.USER_IDENTIFICATION_CODE_NOT_FOUND_WITH_UUID, encryptedUUID));
					uuid = null;
					throw new EntityNotFoundException(
							Util.format(ErrorMessages.USER_IDENTIFICATION_CODE_NOT_FOUND_WITH_UUID, encryptedUUID));
				}

			}
			if (uuid == null) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.UNABLE_TO_VALIDATE_UUID, encryptedUUID));
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.USER_NOT_FOUND_WITH_UUID, encryptedUUID), cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_WITH_UUID, encryptedUUID);
		}
		if (userEntity == null) {
			log.info(ApplicationConstants.notifySearch,
					Util.format(ErrorMessages.USER_NOT_FOUND_WITH_UUID, encryptedUUID));
			throw new EntityNotFoundException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_UUID, encryptedUUID));
		}
		log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_FOUND_WITH_UUID, encryptedUUID));
		return userEntity;
	}

	@Override
	public List<UserIdNamePair> getUserIdNames(String organizationName) throws EntityNotFoundException {
		List<UserIdNamePair> userIdNamePairs = new ArrayList<>();
		try {
			List<UserEntity> userEntities = userRepository.findUserByOrganization(organizationName);
			if (userEntities != null) {
				userEntities.stream().forEach(userEntity -> {
					UserIdNamePair userIdNamePair = new UserIdNamePair();
					userIdNamePair.setUserId(userEntity.getId());
					userIdNamePair.setUserName(userEntity.getName());

					userIdNamePairs.add(userIdNamePair);
				});
			} else {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.USER_NOT_FOUND_UNDER_ORGANIZATION, organizationName));
				throw new EntityNotFoundException(
						Util.format(ErrorMessages.USER_NOT_FOUND_UNDER_ORGANIZATION, organizationName));
			}

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.USER_NOT_FOUND_UNDER_ORGANIZATION, organizationName), cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_UNDER_ORGANIZATION, organizationName);
		}
		log.info(ApplicationConstants.notifySearch,
				Util.format(ErrorMessages.USER_FOUND_UNDER_ORGANIZATION, organizationName));
		return userIdNamePairs;
	}

	@Override
	public Workflow getWorkflowByUser(Long userID) throws EntityNotFoundException {

		try {
			UserEntity userEntity = userRepository.findOne(userID);
			if (userEntity == null) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userID));
				throw new EntityNotFoundException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userID));
			}
			return getWorkflowByUserName(userEntity.getOrganization().getName(), userEntity.getReferenceName());
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_USERID, userID), cause);
			throw new EntityNotFoundException(cause, ErrorMessages.WORKFLOW_NOT_FOUND_FOR_USERID, userID);
		}
	}

	@Override
	public Workflow getWorkflowByUserName(String organizationName, String userName) throws EntityNotFoundException {
		List<Workflow> workflows = new ArrayList<Workflow>();
		try {
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(userName, organizationName);
			if (userRepresentation != null) {
				List<GroupRepresentation> groupRepresentations = keycloakService
						.getGroupsByUserID(userRepresentation.getId(), organizationName);
				if (groupRepresentations != null) {
					groupRepresentations.stream().forEach(groupRepresentation -> {
						List<RoleRepresentation> roleRepresentations = keycloakService
								.getRolesByGroupID(groupRepresentation.getId(), organizationName);
						if (roleRepresentations != null) {
							roleRepresentations.stream().forEach(roleRepresentation -> {
								if (roleRepresentation.getName().endsWith("WORKFLOW")
										|| roleRepresentation.getName().endsWith("workflow")
										|| roleRepresentation.getName().endsWith("Workflow")) {
									Workflow workflow = workflowService.getWorkflowByRole(organizationName,
											roleRepresentation.getName());
									workflows.add(workflow);
								}
							});
						} else {
							log.info(ApplicationConstants.notifySearch, Util
									.format(ErrorMessages.ROLE_NOT_FOUND_WITH_GORUPID, groupRepresentation.getId()));
							throw new EntityNotFoundException(Util.format(ErrorMessages.ROLE_NOT_FOUND_WITH_GORUPID,
									groupRepresentation.getId()));
						}
					});
				} else {
					log.info(ApplicationConstants.notifySearch,
							Util.format(ErrorMessages.GROUP_NOT_FOUND_FOR_USERID, userRepresentation.getId()));
					throw new EntityNotFoundException(
							Util.format(ErrorMessages.GROUP_NOT_FOUND_FOR_USERID, userRepresentation.getId()));
				}
			} else {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, userName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, userName));
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.ENTITY_NOT_FOUND, cause);
			throw new EntityNotFoundException(ErrorMessages.ENTITY_NOT_FOUND, cause);
		}
		if(workflows.size() > 0) {
			return workflows.get(0);
		} else {
			if (workflows.isEmpty()) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_USER, userName));
				// throw new EntityNotFoundException(
						// Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_USER, userName));
			}
			return null;
		}
	}
	
	@Override
	public Workflow getWorkflowGeneralDetailsByUserName(String organizationName, String userName) {
		List<Workflow> workflows = new ArrayList<Workflow>();
		try {
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(userName, organizationName);
			if (userRepresentation != null) {
				List<GroupRepresentation> groupRepresentations = keycloakService
						.getGroupsByUserID(userRepresentation.getId(), organizationName);
				if (groupRepresentations != null) {
					groupRepresentations.stream().forEach(groupRepresentation -> {
						List<RoleRepresentation> roleRepresentations = keycloakService
								.getRolesByGroupID(groupRepresentation.getId(), organizationName);
						if (roleRepresentations != null) {
							roleRepresentations.stream().forEach(roleRepresentation -> {
								if (roleRepresentation.getName().endsWith("WORKFLOW")
										|| roleRepresentation.getName().endsWith("workflow")
										|| roleRepresentation.getName().endsWith("Workflow")) {
									Workflow workflow = workflowService.getWorkflowByRole(organizationName,
											roleRepresentation.getName());
									workflows.add(workflow);
								}
							});
						} else {
							log.info(ApplicationConstants.notifySearch, Util
									.format(ErrorMessages.ROLE_NOT_FOUND_WITH_GORUPID, groupRepresentation.getId()));
							throw new EntityNotFoundException(Util.format(ErrorMessages.ROLE_NOT_FOUND_WITH_GORUPID,
									groupRepresentation.getId()));
						}
					});
				} else {
					log.info(ApplicationConstants.notifySearch,
							Util.format(ErrorMessages.GROUP_NOT_FOUND_FOR_USERID, userRepresentation.getId()));
					throw new EntityNotFoundException(
							Util.format(ErrorMessages.GROUP_NOT_FOUND_FOR_USERID, userRepresentation.getId()));
				}
			} else {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, userName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, userName));
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.ENTITY_NOT_FOUND, cause);
			throw new EntityNotFoundException(ErrorMessages.ENTITY_NOT_FOUND, cause);
		}
		if(workflows.size() > 0) {
			return workflows.get(0);
		} else {
			if (workflows.isEmpty()) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_USER, userName));
				// throw new EntityNotFoundException(
						// Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_USER, userName));
			}
			return null;
		}
	}

	/**
	 * sort- sort by field order - desc, asc page - page no. size - no. of records
	 * per page
	 */
	@Override
	public ResultPage getUsersBySearchCriteria(String organizationName, String searchCriteria, int page, int size)
			throws EntityNotFoundException {
		List<SearchUserResult> searchUserResults = new ArrayList<>();
		int start = 0;
		int max = size;
		if (size == 0) {
			max = 100;
		}
		if (page == 0) {
			start = page;
		} else {
			start = (page * size);
		}
		ResultPage resultPage = null;
		try {
			resultPage = keycloakService.getUsersBySearchCriteria(organizationName, searchCriteria, start, max);
			List<UserRepresentation> userRepresentations = (List<UserRepresentation>) resultPage.getContent();
			boolean isMobileIDUser = false;
			if (userRepresentations != null) {
				userRepresentations.stream().forEach(userRepresentation -> {
					SearchUserResult searchUserResult = new SearchUserResult();
					UserRolesAndGroups userRolesAndGroups = keycloakService.getGroupsAndRolesOfUserByID(userRepresentation.getId(), organizationName);
					List<RoleRepresentation> roleRepresentations = userRolesAndGroups.getRoleRepresentations();
					if(roleRepresentations.size() > 0) {
						List<Role> roles  = new ArrayList<Role>();
						List<String> roleIDs  = new ArrayList<String>();
						
						for (RoleRepresentation roleRepresentation : roleRepresentations) {
							roles.add(new Role(roleRepresentation.getName(), roleRepresentation.getDescription(), roleRepresentation.getId()));
							roleIDs.add(roleRepresentation.getId());
							if(roleRepresentation.getName().equalsIgnoreCase(RoleTypeEnum.ADMIN.getUserRole())) {
								searchUserResult.setAdmin(true);
							} else
							if(searchUserResult.isAdmin() ==false && roleRepresentation.getName().equalsIgnoreCase(RoleTypeEnum.OPERATOR.getUserRole())) {
								searchUserResult.setOperator(true);
							} else
							if(searchUserResult.isAdmin() ==false && searchUserResult.isOperator() ==false && roleRepresentation.getName().equalsIgnoreCase(RoleTypeEnum.ADJUDICATOR.getUserRole())) {
								searchUserResult.setAdjudicator(true);
							} else 
							if(searchUserResult.isAdmin() ==false && searchUserResult.isOperator() ==false &&  searchUserResult.isAdjudicator() ==false && roleRepresentation.getName().equalsIgnoreCase(RoleTypeEnum.MOBILEIDUSER.getUserRole())) {
								searchUserResult.setMobileIDUser(true);
							}else 
							if(searchUserResult.isAdmin() ==false && searchUserResult.isOperator() ==false &&  searchUserResult.isAdjudicator() ==false && searchUserResult.isMobileIDUser() ==false && roleRepresentation.getName().equalsIgnoreCase(RoleTypeEnum.USER.getUserRole())) {
								searchUserResult.setUser(true);
							}
						}
						searchUserResult.setRoles(roles);
						searchUserResult.setRoleIDs(roleIDs);
					}
					
					if (searchUserResult.isMobileIDUser()) {
						return; // to skips the iteration if user is MobileIDUser
					}
					
					List<GroupRepresentation> groupRepresentations = userRolesAndGroups.getGroupRepresentations();
					
					searchUserResult.setUserName(userRepresentation.getUsername());
					searchUserResult.setFirstName(userRepresentation.getFirstName());
					searchUserResult.setLastName(userRepresentation.getLastName());
					searchUserResult.setEmail((userRepresentation.getEmail() == null || userRepresentation.getEmail().equals(""))?"--":userRepresentation.getEmail());
					searchUserResult.setEnabled(userRepresentation.isEnabled());
					searchUserResult.setSub(userRepresentation.getId());
					if(userRepresentation.getFederationLink() != null) {
						searchUserResult.setIsFederated(true);
					} else {
						searchUserResult.setIsFederated(false);
					}
					if(groupRepresentations.size() > 0) {						
					
						searchUserResult.setGroupName(groupRepresentations.get(0).getName());
						searchUserResult.setGroupId(groupRepresentations.get(0).getId());
						String[] workflow_identityTypeName = workflowService.getIdentityTypeAndWrkflowByGroupID(organizationName, groupRepresentations.get(0).getId());
						searchUserResult.setIdentityTypeName((workflow_identityTypeName == null || workflow_identityTypeName.length == 0)?"--":workflow_identityTypeName[1]);
						searchUserResult.setWorkflowName((workflow_identityTypeName == null || workflow_identityTypeName.length == 0)?"--":workflow_identityTypeName[0]);
						searchUserResult.setOnboardSelfService((workflow_identityTypeName == null || workflow_identityTypeName.length == 0)?"--":workflow_identityTypeName[2]);
					}
					
					
						UserEntity userEntity = userRepository.getUserByName(userRepresentation.getUsername(),
								organizationName);
						if(userEntity != null) {
							searchUserResult.setUserID(userEntity.getId());
						}
						if (userEntity == null || userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name())) {
							searchUserResult.setCanEnroll(true);
							searchUserResult.setUserStatus(UserStatusEnum.PENDING_ENROLLMENT.getUserStatus());
							searchUserResult.setIsEnrollmentInProgress(false);
						} else if (userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name())) {
							searchUserResult.setCanEnroll(true);
							searchUserResult.setUserStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.getUserStatus());
							searchUserResult.setIsEnrollmentInProgress(true);
						} else {
							searchUserResult.setOnboardSelfService("false");
							searchUserResult.setUserStatus(UserStatusEnum.valueOf(userEntity.getStatus()).getUserStatus());
							searchUserResult.setIsEnrollmentInProgress(false);
							searchUserResult.setCanDisplayEnrollDetails(true);
							if (userEntity == null || userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_APPROVAL.name())) {
								searchUserResult.setCanApprove(true);
								searchUserResult.setUserStatus(UserStatusEnum.PENDING_APPROVAL.getUserStatus());
								if ((apduFeignClient.getCountOfIssuedUserDevices(userEntity.getId(), organizationName) > 0l)) {
									searchUserResult.setCanLifeCycle(true);
								}
							}else {
								searchUserResult.setCanIssue(true);
								// TODO Make a call to APDU Service to see if user has already issued devices,
								// then enable canLifeCycle and if its in READY_FOR_ISSUANCE, it means its first timer so disable 
								if (userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.IDENTITY_ISSUED.name()) || userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.READY_FOR_REISSUANCE.name()) || (apduFeignClient.getCountOfIssuedUserDevices(userEntity.getId(), organizationName) > 0l)) {
									searchUserResult.setCanLifeCycle(true);
								}
							}
						}
						searchUserResults.add(searchUserResult);
					
				});
			}
			resultPage.setContent(searchUserResults);
			log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_FOUND_WITH_SEARCHCRITERIA,
					searchUserResults.size(), searchCriteria, organizationName));

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.USER_NOT_FOUND_WITH_SEARCHCRITERIA, searchCriteria, organizationName),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_WITH_SEARCHCRITERIA, searchCriteria,
					organizationName);
		}
		return resultPage;
	}

	@Override
//	public ResultPage getUsersByFilters(String organizationName, String userName, String status, Instant startDate,
//			Instant endDate, int page, int size) throws EntityNotFoundException {
	public ResultPage getUsersByFilters(String organizationName, UserRequest userRequest, int page, int size)
			throws EntityNotFoundException {

		// TODO handle page and size
		// TODO append approvedby and createdby search criteria
		ResultPage resultPage = new ResultPage();

		String userName = userRequest.getName();
		Instant startDate = userRequest.getStartDate();
		Instant endDate = userRequest.getEndDate();
		List<String> statusLst = userRequest.getStatus();

//		Set<String> statusSet = new Gson().fromJson(status, new TypeToken<HashSet<String>>() {
//		}.getType());

		List<User> users = new ArrayList<User>();
		String searchFieldsMsg = "startDate, endDate " + userName != null ? ", userName"
				: "" + statusLst != null ? ", status" : "";
		try {
			List<UserEntity> userEntities = userRepository.findUsersByFilters(organizationName, userName, statusLst,
					startDate, endDate);

			if (userEntities != null) {
				userEntities.stream().forEach(userentity -> {
					User user = new User();
					// user.setId(userentity..getId);
					user.setName(userentity.getName());
					user.setFirstName(userentity.getUserAttribute().getFirstName());
					user.setMiddleName(userentity.getUserAttribute().getMiddleName());
					user.setLastName(userentity.getUserAttribute().getLastName());
					user.setStatus(userentity.getStatus());
					user.setCreatedBy(userentity.getCreatedBy());
					user.setCreatedDate(userentity.getCreatedDate());
					user.setLastModifiedBy(userentity.getLastModifiedBy());
					user.setLastModifiedDate(userentity.getLastModifiedDate());

					users.add(user);
				});
			}
			resultPage.setContent(users);
			resultPage.setSize(users.size());
			log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_FOUND_WITH_SEARCHCRITERIA,
					users.size(), searchFieldsMsg, organizationName));
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.USER_NOT_FOUND_WITH_SEARCHCRITERIA, searchFieldsMsg, organizationName),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_WITH_SEARCHCRITERIA, userName, statusLst,
					startDate, endDate, organizationName);
		}
		return resultPage;
	}

	/**
	 * sort- sort by field order - desc, asc page - page no. size - no. of records
	 * per page
	 */
	@Override
	public ResultPage getMembersOfAGroup(String organizationName, String groupID, int page, int size)
			throws EntityNotFoundException {
		List<SearchUserResult> searchUserResults = new ArrayList<>();
		int start = 0;
		int max = size;
		if (size == 0) {
			max = 100;
		}
		if (page == 0) {
			start = page;
		} else {
			start = (page * size);
		}
		ResultPage resultPage = null;
		try {
			resultPage = keycloakService.getMembersOfAGroup(organizationName, groupID, start, max);
			List<UserRepresentation> userRepresentations = (List<UserRepresentation>) resultPage.getContent();

			if (userRepresentations != null) {
				userRepresentations.stream().forEach(userRepresentation -> {
					SearchUserResult searchUserResult = new SearchUserResult();
					searchUserResult.setUserName(userRepresentation.getUsername());
					searchUserResult.setFirstName(userRepresentation.getFirstName());
					searchUserResult.setLastName(userRepresentation.getLastName());
					searchUserResult.setEmail(userRepresentation.getEmail());
					searchUserResult.setEnabled(userRepresentation.isEnabled());
					UserEntity userEntity = userRepository.getUserByName(userRepresentation.getUsername(),
							organizationName);
					if (userEntity == null || userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name())) {
						searchUserResult.setCanEnroll(true);
					} else {
						searchUserResult.setCanDisplayEnrollDetails(true);
						searchUserResult.setCanIssue(true);
						// TODO Make a call to APDU Service to see if user has already issued devices,
						// then enable canLifeCycle
						if (userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.IDENTITY_ISSUED.name())) {
							searchUserResult.setCanLifeCycle(true);
						}
					}
					searchUserResults.add(searchUserResult);
				});
			}
			resultPage.setContent(searchUserResults);
			log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_FOUND_FOR_GROUPID,
					searchUserResults.size(), groupID, organizationName));

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.USER_NOT_FOUND_FOR_GROUPID, groupID, organizationName), cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_FOR_GROUPID, groupID,
					organizationName);
		}
		return resultPage;
	}

	@Override
	public void updateUserStatusAfterIssuance(Long userID, User user) throws EntityUpdateException {
		try {
			UserEntity userEntity = userRepository.getOne(userID);
			if(!userEntity.getStatus().equalsIgnoreCase(user.getStatus())) {
				userEntity.setStatus(user.getStatus());
				userRepository.save(userEntity);
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyUpdate,
					Util.format(ErrorMessages.FAILED_TO_UPDATE_USER_STATUS, userID, user.getStatus()), cause);
			throw new EntityUpdateException(cause, ErrorMessages.FAILED_TO_UPDATE_USER_STATUS, userID,
					user.getStatus());
		}
	}
	
	@Override
	public void updateUserStatustoReissuance(String organizationName, Set<Long>  userIDs) throws EntityUpdateException {
		try {
			
			List<UserEntity>  userEntities = userRepository.updateTheseUsersUnderOrganization(organizationName, userIDs, UserStatusEnum.PENDING_APPROVAL.name());
			for (UserEntity userEntity : userEntities) {
				userEntity.setStatus(UserStatusEnum.READY_FOR_REISSUANCE.name());
				userRepository.save(userEntity);
			}
			/*
			 * { log.error(ApplicationConstants.notifyUpdate, Util.format(ErrorMessages.
			 * FAILED_TO_UPDATE_USER_STATUS_AFTER_DEVICE_REVOKE_AS_NOT_ALL_USERS_ARE_UPDATED,
			 * count, userIDs)); throw new EntityUpdateException(new Exception(),
			 * ErrorMessages.
			 * FAILED_TO_UPDATE_USER_STATUS_AFTER_DEVICE_REVOKE_AS_NOT_ALL_USERS_ARE_UPDATED,
			 * count, userIDs); }
			 */		
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyUpdate,
					Util.format(ErrorMessages.FAILED_TO_UPDATE_USER_STATUS_AFTER_DEVICE_REVOKE), cause);
			throw new EntityUpdateException(cause, ErrorMessages.FAILED_TO_UPDATE_USER_STATUS_AFTER_DEVICE_REVOKE);
		}
	}

	
	@Override
	public void resetMobileOTPCredential(String organizationName, String  userName) throws EntityUpdateException {
		try {
			
			keycloakService.resetMobileOTPCredential(organizationName, userName);
			
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyUpdate,
					Util.format(ErrorMessages.FAILED_TO_UPDATE_USER_STATUS_AFTER_DEVICE_REVOKE), cause);
			throw new EntityUpdateException(cause, ErrorMessages.FAILED_TO_UPDATE_USER_STATUS_AFTER_DEVICE_REVOKE);
		}
	}
	
	@Override
	public void updateUserStatusAfterDeleteCred(String organizationName, Map<Long,String>  userStatus) throws EntityUpdateException {
		List<UserEntity>  userEntities = new ArrayList<UserEntity>();
		try {
			for (Long userID :  userStatus.keySet()) {
				UserEntity  userEntity = userRepository.getUserwithoutPendingApprovalState(organizationName, userID, UserStatusEnum.PENDING_APPROVAL.name());
				if(userEntity!=null) {
					userEntity.setStatus(userStatus.get(userID));				
					userEntities.add(userEntity);
				}
			}
			if(userEntities.size()>0) {
				userRepository.save(userEntities);
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyUpdate,
					Util.format(ErrorMessages.FAILED_TO_UPDATE_USER_STATUS_AFTER_DEVICE_UNPAIR), cause);
			throw new EntityUpdateException(cause, ErrorMessages.FAILED_TO_UPDATE_USER_STATUS_AFTER_DEVICE_UNPAIR);
		}
	}
	
	@Override
	public User verifyOtp(String mobileNumber, String otp) throws EntityNotFoundException {
		// TO-DO get user by mobile number --- User object not yet available
		try {
			UserOtpEntity userOtpEntity = userOtpRepository.getUserOtpByPhoneNumber(mobileNumber, otp);
			if (userOtpEntity != null) {
				// TO-DO time validation with threshold should be happened
				// Get user Object by phone number and return

			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.OTP_VERIFICATION_FAILED_FOR_USER, otp), cause);
			throw new EntityNotFoundException(cause, ErrorMessages.OTP_VERIFICATION_FAILED_FOR_USER, otp);
		}
		return null;
	}

	@Override
	public UserEntity addUser(UserEntity entity, String organizationName) throws EntityCreateException {
		UserEntity userEntity;
		try {
			OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);

			// User
			entity.setOrganization(organizationEntity);
			userEntity = userRepository.save(entity);

			// Commenting GPS changes, this should be added into system during mobile paring
			// rather than user onboard

			/*
			 * double latitue = Math.random() * (90 - (-90)) + (-90); double longitude =
			 * Math.random() * (180 - (-180)) + (-180); String wktPoint =
			 * "Point("+longitude+" "+latitue+")"; //First interpret the WKT string to a
			 * point WKTReader fromText = new WKTReader(); Geometry geom = null; try { geom
			 * = fromText.read(wktPoint); } catch (ParseException e) { throw new
			 * RuntimeException("Not a WKT string:" + wktPoint); } if
			 * (!geom.getGeometryType().equals("Point")) { throw new
			 * RuntimeException("Geometry must be a point. Got a " +
			 * geom.getGeometryType()); }
			 * 
			 * 
			 * 
			 * // GeometryFactory gf = new GeometryFactory(); Point geom = gf.createPoint(new Coordinate(-178.87650, 87.00965));
			 * 
			 * 
			 * UserLocationEntity userlocationentity = null; userlocationentity =
			 * userLocationRepository.findByUserID(entity.getId()); if(userlocationentity ==
			 * null) { userlocationentity = new
			 * UserLocationEntity(entity,"name",(Point)geom, new Date());
			 * userLocationRepository.save(userlocationentity); }else {
			 * userlocationentity.setLocation((Point)geom); }
			 */

		} catch (DataIntegrityViolationException cause) {
			keycloakService.deleteUser(entity.getReferenceName(), entity.getOrganization().getName());
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_ADD_USER_WITH_USERNAME, entity.getReferenceName()), cause);
			throw new EntityCreateException(cause, ErrorMessages.FAILED_TO_ADD_USER_WITH_USERNAME,
					entity.getReferenceName());
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_ADD_USER_WITH_USERNAME, entity.getReferenceName()), cause);
			throw new EntityCreateException(cause, ErrorMessages.FAILED_TO_ADD_USER_WITH_USERNAME,
					entity.getReferenceName());
		}
		return userEntity;
	}

	@Override
	public ResultPage pendingEnrollmentStat(String organizationName, String groupID, String sortBy, String order,
			int page, int size, Date createdDateStart, Date createdDateEnd) throws EntityNotFoundException {
		ResultPage resultPage = new ResultPage();
		StatsAttributes statsAttr = new StatsAttributes();
		statsAttr.setOrganizationName(organizationName);
		statsAttr.setGroupID(groupID);
		statsAttr.setSortBy(sortBy);
		statsAttr.setOrder(order);
		statsAttr.setPage(page);
		statsAttr.setSize(size);
		statsAttr.setCreatedDateStart(createdDateStart);
		statsAttr.setCreatedDateEnd(createdDateEnd);
		
		try {
			resultPage = getStatsByStatus(UserStatusEnum.PENDING_ENROLLMENT.name(),statsAttr);
		}
		catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_FETCH_PENDINGENROLLMENTS_UNDER_ORGANIZATION, organizationName),
					cause);
			throw new EntityNotFoundException(cause,
					ErrorMessages.FAILED_TO_FETCH_PENDINGENROLLMENTS_UNDER_ORGANIZATION, organizationName);
		}		
		return resultPage;
	}

	@Override
	public ResultPage pendingIssuanceStat(String organizationName, String groupID, String sortBy, String order,
			int page, int size, Date createdDateStart, Date createdDateEnd) throws EntityNotFoundException {
		ResultPage resultPage = new ResultPage();	
		StatsAttributes statsAttr = new StatsAttributes();
		statsAttr.setOrganizationName(organizationName);
		statsAttr.setGroupID(groupID);
		statsAttr.setSortBy(sortBy);
		statsAttr.setOrder(order);
		statsAttr.setPage(page);
		statsAttr.setSize(size);
		statsAttr.setCreatedDateStart(createdDateStart);
		statsAttr.setCreatedDateEnd(createdDateEnd);
		try {
			resultPage = getStatsByStatus(UserStatusEnum.READY_FOR_ISSUANCE.name(),statsAttr);
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					String.format(ErrorMessages.FAILED_TO_FETCH_PENDINGISSUANCES_UNDER_ORGANIZATION, organizationName),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessages.FAILED_TO_FETCH_PENDINGISSUANCES_UNDER_ORGANIZATION,
					organizationName);
		}
		return resultPage;
	}
	
	@Override
	public ResultPage pendingApprovalStat(String organizationName, String groupID, String sortBy, String order,
			int page, int size, Date createdDateStart, Date createdDateEnd) throws EntityNotFoundException {
		ResultPage resultPage = new ResultPage();
		StatsAttributes statsAttr = new StatsAttributes();
		statsAttr.setOrganizationName(organizationName);
		statsAttr.setGroupID(groupID);
		statsAttr.setSortBy(sortBy);
		statsAttr.setOrder(order);
		statsAttr.setPage(page);
		statsAttr.setSize(size);
		statsAttr.setCreatedDateStart(createdDateStart);
		statsAttr.setCreatedDateEnd(createdDateEnd);
		try {
			resultPage = getStatsByStatus(UserStatusEnum.PENDING_APPROVAL.name(),statsAttr);
			
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_FETCH_PENDINGAPPROVAL_UNDER_ORGANIZATION, organizationName),
					cause);
			throw new EntityNotFoundException(cause,
					ErrorMessages.FAILED_TO_FETCH_PENDINGAPPROVAL_UNDER_ORGANIZATION, organizationName);
		}
		return resultPage;
	}
	
	private ResultPage getStatsByStatus(String userStatus,StatsAttributes statsAttr)throws EntityNotFoundException {
		List<UserPendingActions> users = new ArrayList<UserPendingActions>();
		ResultPage resultPage = new ResultPage();
		Sort sort = null;
		Direction direction = null;
		String order = statsAttr.getOrder();
		String sortBy = statsAttr.getSortBy();
		String ErrorMessage = null;
		if(userStatus.equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name())) {
			ErrorMessage = ErrorMessages.FAILED_TO_FETCH_PENDINGENROLLMENTS_UNDER_ORGANIZATION;
		} else if(userStatus.equalsIgnoreCase(UserStatusEnum.READY_FOR_ISSUANCE.name())) {
			ErrorMessage = ErrorMessages.FAILED_TO_FETCH_PENDINGISSUANCES_UNDER_ORGANIZATION;
		} else if(userStatus.equalsIgnoreCase(UserStatusEnum.PENDING_APPROVAL.name())) {
			ErrorMessage = ErrorMessages.FAILED_TO_FETCH_PENDINGAPPROVAL_UNDER_ORGANIZATION;
		}
		try {
			if (!sortBy.equalsIgnoreCase("undefined") && !(order.isEmpty())) {
				if (order.equalsIgnoreCase(DirectionEnum.ASC.getDirectionCode())) {
					direction = Direction.ASC;
				} else if (order.equalsIgnoreCase(DirectionEnum.DESC.getDirectionCode())) {
					direction = Direction.DESC;
				}
				if (direction != null) {
					if (sortBy.equalsIgnoreCase("requestedFor")) {
						sortBy = "name";
						sort = new Sort(direction, sortBy);
					} else if (sortBy.equalsIgnoreCase("requestedDate")) {
						sortBy = "createdDate";
						sort = new Sort(direction, sortBy);
					} else {
						sort = new Sort(new Sort.Order(direction, sortBy));
					}
				}
			}

			Pageable pageable = new PageRequest(statsAttr.getPage(), statsAttr.getSize(), sort);
			Specification<UserEntity> spec = Specifications.where(UserSpecification.userSpecifications(statsAttr.getOrganizationName(),
					statsAttr.getGroupID(), userStatus, statsAttr.getCreatedDateStart(), statsAttr.getCreatedDateEnd()));
			Page<UserEntity> pagedResult = userRepository.findAll(spec, pageable);
			if (pagedResult.getTotalElements() != 0) {
				pagedResult.forEach(userEntity -> {
					UserPendingActions user = new UserPendingActions();
					
					String[] workflow_identityTypeName = workflowService.getIdentityTypeAndWrkflowByGroupID(userEntity.getOrganization().getName(), userEntity.getGroupId());
					user.setIdentityTypeName((workflow_identityTypeName == null || workflow_identityTypeName.length == 0)?"--":workflow_identityTypeName[1]);
					
					user.setFirstName(userEntity.getUserAttribute().getFirstName());
					user.setLastName(userEntity.getUserAttribute().getLastName());
					user.setUsername(userEntity.getReferenceName());
					user.setEmail(userEntity.getUserAttribute().getEmail());
					user.setIsFederated(userEntity.getIsFederated());
					user.setGroupId(userEntity.getGroupId());
					user.setGroupName(userEntity.getGroupName());
					user.setCreatedDate(simpleDateFormat.format(Date.from(userEntity.getCreatedDate())));
					user.setEnrolledBy(userEntity.getLastModifiedBy());
					users.add(user);
				});
				resultPage.setFirst(pagedResult.isFirst());
				resultPage.setLast(pagedResult.isLast());
				resultPage.setNumber(pagedResult.getNumber());
				resultPage.setNumberOfElements(pagedResult.getNumberOfElements());
				resultPage.setSize(pagedResult.getSize());
				resultPage.setSort(pagedResult.getSort());
				resultPage.setTotalElements(pagedResult.getTotalElements());
				resultPage.setTotalPages(pagedResult.getTotalPages());
			}
			resultPage.setContent(users);
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessage, statsAttr.getOrganizationName()),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessage, statsAttr.getOrganizationName());
		}
		return resultPage;
	}

	@Override
	public List<UserStats> userStats(String organizationName) throws EntityNotFoundException {
		// TODO Auto-generated method stub
		
		// For each Role
		List<RoleEntity> roles = rolerepository.getActiveUserRolesByOrganization(organizationName);
		//List<Role>  roles = keycloakService.getRolesOfClientFromAuthenticationService(organizationName);
		List<String> userstatus = new ArrayList<String>();
		List<UserStats> list = new ArrayList<UserStats>();
		for (RoleEntity role : roles) {
			UserStats obj = new UserStats();
			if(!role.getName().equalsIgnoreCase(RoleTypeEnum.MOBILEIDUSER.getUserRole())) {
				obj.setType(role.getName());
				list.add(obj);
			}
		}
		for (UserStats userStat : list) {
			try {
					List<String> userNames = new ArrayList<String>();
					List<UserRepresentation> userReps = keycloakService.getUsersWithSpecifiedRole(userStat.getType(),organizationName);
					if(userReps == null || userReps.isEmpty() ) {
						userStat.setTotalUsers(0);
						userStat.setTotalEnrolled(0);
						userStat.setTotalActive(0);
					}else {
						for (UserRepresentation userRepresentation : userReps) {
							userNames.add(userRepresentation.getUsername());
						}
						userstatus.add(UserStatusEnum.PENDING_ENROLLMENT.name());
						userstatus.add(UserStatusEnum.PENDING_APPROVAL.name());
						Long e_count = userRepository.getUserCountByStatus(userNames,organizationName,userstatus);
					
					
						//Long a_count = userRepository.getUserCountByUserStatus(userNames,organizationName,"ACTIVE");
						
						userStat.setTotalUsers(userReps.size());
						userStat.setTotalEnrolled(e_count.intValue());
						int activeCount=0;
						for (UserRepresentation userRepresentation : userReps) {
							if(userRepresentation.isEnabled()) {
								activeCount++;
							}
						}
						userStat.setTotalActive(activeCount);
					}
				} catch (Exception cause) {
					log.error(ApplicationConstants.notifyAdmin,
							Util.format(ErrorMessages.FAILED_TO_FETCH_PENDING_ENROLLMENTS_ISSUANCES_UNDER_ORGANIZATION,
									organizationName),
							cause);
//					throw new EntityNotFoundException(cause,
//							ErrorMessages.FAILED_TO_FETCH_PENDING_ENROLLMENTS_ISSUANCES_UNDER_ORGANIZATION, organizationName);
				}
			//}
		}
		return list;
	}

	@Override
	public Long getUserIDByName(String organizationName, String name) throws EntityNotFoundException {
		// TODO Auto-generated method stub
		try {
			return userRepository.getUserIDByName(organizationName, name);
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, name),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, name);
		}
	}

	@Override
	public void userLogout(String organizationName, String userName) throws IdentityBrokerServiceException {
		// TODO Auto-generated method stub
		UserRepresentation user = keycloakService.getUserByUsername(userName, organizationName);
		keycloakService.userLogout(user.getId(), organizationName);
	}

	@Override
	public ResponseEntity<HttpStatus> deleteUserByName(String organizationName, String userName, boolean isFederated)
			throws EntityNotFoundException {
		try {

			UserEntity userEntity = userRepository.getUserByName(userName, organizationName);

			if (userEntity != null) {
				apduFeignClient.deleteUserById(userEntity.getId());
				userRepository.delete(userEntity);
				// if user is from ldap, dont delete the user in keycloak
				if(!isFederated) {
					keycloakService.deleteUser(userName, organizationName);
				}
				if(!userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name())) {
					if(userEntity.getUserBiometrics() != null || !userEntity.getUserBiometrics().isEmpty()) {
						UserBioInfo userBioInfo = getUserBioInfoObj(getUserBiometrics(userEntity.getUserBiometrics()));
						userBioInfo.setUserID(userEntity.getId());
						
						ResponseEntity<BiometricVerificationResponse> response = null;
						response = biometricFeignClient.delete(organizationName, userBioInfo);
						if(response.getStatusCodeValue() == 200) {
							if(response.getBody().getStatusMessage() != null ) {
								if(!response.getBody().getStatusMessage().equalsIgnoreCase("OK")) {
									//Need to handle in case delte API returned other than OK
									log.info("Delete api from Nserver:::"+response.getBody().getStatusMessage());
								}
							}
						}	
					}
				}
			} else if(!isFederated) {
				keycloakService.deleteUser(userName, organizationName);
			}
			
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception cause) {

			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_DELETE_USER_WITH_USERNAME, userName), cause);
			throw new EntityNotFoundException(cause, ErrorMessages.FAILED_TO_DELETE_USER_WITH_USERNAME, userName);

			// return new ResponseEntity<>(HttpStatus.CONFLICT);
		}

	}

	public static byte[] convertJPEGtoPNGFormat(byte[] input) throws IOException {
		InputStream inputStream = new ByteArrayInputStream(input);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		// reads input image from file
		BufferedImage inputImage = ImageIO.read(inputStream);

		// writes to the output image in specified format
		boolean result = ImageIO.write(inputImage, "png", outputStream);
		// needs to close the streams
		outputStream.close();
		inputStream.close();

		return outputStream.toByteArray();
	}

	public static byte[] convertPNGtoJPEGFormat(byte[] input) throws IOException {
		InputStream inputStream = new ByteArrayInputStream(input);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		// reads input image from file
		BufferedImage inputImage = ImageIO.read(inputStream);

		// writes to the output image in specified format
		boolean result = ImageIO.write(inputImage, "jpeg", outputStream);
		// needs to close the streams
		outputStream.close();
		inputStream.close();

		return outputStream.toByteArray();
	}
	
	@Override
	public int experssEnroll(HttpHeaders httpHeaders, String organizationName, String enrolledBy, String groupID, String groupName, String roleIDs, ExpressEnrollmentSchema schema)
			throws EnrollmentServiceException {
		int statusCode = 201;
		boolean isApprovalRequired = false;
		try {

			/*********************************************************************************************************/
			OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);
			/*********************************************************************************************************/

			List<Item> list_items = schema.getItems();
			Item userdetails = list_items.get(0);

			// User
			UserEntity userEntity = new UserEntity();
			userEntity.setOrganization(organizationEntity);
			userEntity.setStatus("READY_FOR_ISSUANCE");
			userEntity.setOrganization(organizationEntity);
			userEntity.setGroupId(groupID);
			userEntity.setGroupName(groupName);		
			UserGroupsEntity userGroupsEntity = new UserGroupsEntity();
			userGroupsEntity.setGroupId(groupID);
			userGroupsEntity.setGroupName(groupName);
			userEntity.addUserGroups(userGroupsEntity);
			userEntity.setIsEnrollmentUpdated(false);
			userEntity.setUserStatus("ACTIVE");
			userEntity = userRepository.save(userEntity);	
			// Set Status
			
			// Set USER Status
						String  workflowName = workflowService.getWorkflowNameByGroupID(organizationName, groupID);
						WorkflowStepDetail workflowStepDetail = workflowService.getStepDetails(organizationName,
								workflowName, "APPROVAL_BEFORE_ISSUANCE");
						if (workflowStepDetail.getWfStepApprovalGroupConfigs().size() == 0) {
							// If no Approval Step Exists
							userEntity.setStatus(UserStatusEnum.READY_FOR_ISSUANCE.name());				
						} else {
							List<WFStepApprovalGroupConfig> configs = workflowStepDetail.getWfStepApprovalGroupConfigs();
							for (WFStepApprovalGroupConfig approvalGroupConfig : configs) {
								userEntity.setRefApprovalGroupId(approvalGroupConfig.getGroupId());
								userEntity.setStatus(UserStatusEnum.PENDING_APPROVAL.name());
							}
							isApprovalRequired = true;
						}
			

			//set attributes , appearance, biometrics
			for (Item item : list_items) {

				switch (item.getDataElementType()) {
				case "Biographic":
					UserAttributeEntity userAttributeEntity = new UserAttributeEntity();
					UserAddressEntity userAddressEntity = new UserAddressEntity();
//					TODO need to add state, city , addressline1, addressline2 properties in Address class
					Address address = item.getAddresses().get(0);
				//	userAttributeEntity.setAddress(address.getStreetAddress() + ", " + address.getLocality() + ", "
					//		+ address.getRegion() + ", " + address.getPostalCode());
					
					userAddressEntity.setAddressLine1(address.getStreetAddress());
					userAddressEntity.setAddressLine2("");
					userAddressEntity.setCity(address.getLocality());
					userAddressEntity.setState(address.getRegion());
					userAddressEntity.setCountry(address.getCountry());
					userAddressEntity.setZipCode(address.getPostalCode());
					userAddressEntity.setUser(userEntity);
					
					userAddressRepository.save(userAddressEntity);
					
					userAttributeEntity.setDateOfBirth(simpleDateFormat_ngid.parse(item.getDateofbirth()).toInstant());
					userAttributeEntity.setGender(item.getGenderCode().substring(0,1));
					
					if (item.getEmails() == null || item.getEmails().equals("null")) {
						int min = 5;
						int max = 2000;
						int x = (int) (Math.random() * max) + min;
						System.out.println(x);

						userAttributeEntity.setEmail("temp" + x + "@meetidentity.com");
					} else {
						userAttributeEntity.setEmail((String) item.getEmails());
					}
					userAttributeEntity.setLastName(item.getName().getFamilyName());
					userAttributeEntity.setFirstName(item.getName().getGivenName());
					userAttributeEntity.setMothersMaidenName(item.getName().getMiddleName());
					userAttributeEntity.setNationality(item.getNationality());
					userAttributeEntity.setPlaceOfBirth(item.getPlaceofbirth()==null || "".equals(item.getPlaceofbirth())?item.getNationality():item.getPlaceofbirth());
					userAttributeEntity.setBloodType("O+");

					//userAttributeEntity.setContact(item.getPhoneNumbers());
					userEntity.setUserAttribute(userAttributeEntity);

					UserAppearanceEntity userAppearanceEntity = new UserAppearanceEntity();
					userAppearanceEntity.setHeight(item.getHeightFeet() + "'-" + item.getHeightInches() + "\"");
					userAppearanceEntity.setEyeColor(item.getEyeColorCode().toUpperCase());
					userAppearanceEntity.setHairColor(item.getHairColorCode().toUpperCase());
					userEntity.setUserAppearance(userAppearanceEntity);
					userEntity.setName(userAttributeEntity.getFirstName() + " " + userAttributeEntity.getLastName());
					userEntity.setReferenceName(userAttributeEntity.getFirstName().replace(" ", "").trim());
					

		            UserEnrollmentHistoryEntity userEnrollmentHistoryEntity = new UserEnrollmentHistoryEntity();

						if(!isApprovalRequired) {
							userEnrollmentHistoryEntity.setApprovedBy(enrolledBy);
							userEnrollmentHistoryEntity.setApprovalStatus("APPROVED");
							userEnrollmentHistoryEntity.setReason("Auto Approved");
							userEnrollmentHistoryEntity.setApprovedDate(Timestamp.from(Instant.now()));
							userEnrollmentHistoryEntity.setStatus("ACTIVE");
						}				
						userEnrollmentHistoryEntity.setEnrolledBy(enrolledBy);
						userEnrollmentHistoryEntity.setEnrollmentStartDate(Timestamp.from(Instant.now()));
						userEnrollmentHistoryEntity.setEnrollmentEndDate(Timestamp.from(Instant.now()));				
						userEnrollmentHistoryEntity.setCreatedBy(enrolledBy);
						userEnrollmentHistoryEntity.setCreatedDate(Instant.now());
						userEnrollmentHistoryEntity.setUser(userEntity);
						userEntity.addUserEnrollmentHistory(userEnrollmentHistoryEntity);	
					
						Workflow workflow = workflowService.getWorkflowValidityByGroupID(organizationName, groupID);
						
						switch (workflow.getOrganizationIdentities().getIdentityTypeName()) {

						case Constants.Driving_License:
							
							break;
						case Constants.PIV_and_PIV_1:
							List<UserPivEntity> userPivEntities = new ArrayList<UserPivEntity>();
							UserPivEntity userPIVEntity = new UserPivEntity();
							userPIVEntity.setEmployeeAffiliation(
									item.getAffiliation() == null || "".equals(item.getAffiliation()) ? "Employee"
											: item.getAffiliation());
							userPIVEntity.setEmployeeAffiliationColorCode("G");
							userPIVEntity.setRank("SVP");
							userPIVEntity.setAgency(organizationEntity.getDisplayName());
							userPIVEntity
									.setIssuerIdentificationNumber(organizationEntity.getIssuerIdentificationNumber());
							userPIVEntity.setAgencyAbbreviation(organizationEntity.getAbbreviation());
							userPivEntities.add(userPIVEntity);
							userEntity.setUserPivCredentials(userPivEntities);
							if (userEntity.getId() != null) {
								if (userPivEntities.size() > 0) {
									userPivRepository.save(userPivEntities);
								}
							}
							break;
						case Constants.National_ID:
							UserNationalIdEntity userNationalIdEntity = new UserNationalIdEntity();

							userNationalIdEntity.setVaccineName("XYZ");
							userNationalIdEntity.setVaccinationLocationName("XYZ");
							userNationalIdEntity.setVaccinationDate(new Date());
							userEntity.setUserNationalId(userNationalIdEntity);

							if (userEntity.getId() != null) {
								if (userNationalIdEntity != null) {
									userNationalIDRepository.save(userNationalIdEntity);
								}
							}
							break;
						case Constants.Voter_ID:
							UserVoterIDEntity userVoterIDEntity = new UserVoterIDEntity();
							userVoterIDEntity.setUser(userEntity);
							userVoterIDEntity.setElectorKey(Util.getAlphaNumericString(18).toUpperCase());
							userVoterIDEntity.setRegistrationYear(Calendar.getInstance().get(Calendar.YEAR)+"");
							userEntity.setUserVoterID(userVoterIDEntity);
							if (userEntity.getId() != null) {
								userVoterIDRepository.save(userVoterIDEntity);
							}
							break;
						case Constants.Permanent_Resident_ID:
							UserPermanentResidentIdEntity userPermanentResidentIdEntity = new UserPermanentResidentIdEntity();

							userPermanentResidentIdEntity.setPassportNumber("XYZ");
							userPermanentResidentIdEntity.setResidentcardTypeNumber("XYZ");
							userPermanentResidentIdEntity.setResidentcardTypeCode("XYZ");
							userEntity.setUserPermanentResidentId(userPermanentResidentIdEntity);

							if (userEntity.getId() != null) {
								if (userPermanentResidentIdEntity != null) {
									userPermanentResidentIdRepository.save(userPermanentResidentIdEntity);
								}
							}
							break;
						case Constants.Health_ID:

							List<UserHealthIDEntity> userHealthIDEntities = new ArrayList<UserHealthIDEntity>();

							UserHealthIDEntity userHealthIDEntity = new UserHealthIDEntity();
							userHealthIDEntity.setGroupPlan("High Deductible Health Plans");
							userHealthIDEntity.setPolicyNumber("12345678");
							userHealthIDEntity.setHealthPlanNumber("12345678");
							userHealthIDEntity.setPrimarySubscriber("John");
							userHealthIDEntity.setPrimarySubscriberID("12345678");
							userHealthIDEntity.setPcp("PCP PCP");
							userHealthIDEntity.setPcpPhone("12345678");
							userHealthIDEntity.setIssuerIN(organizationEntity.getIssuerIdentificationNumber());

							userHealthIDEntities.add(userHealthIDEntity);
							userEntity.setUserHealthIDs(userHealthIDEntities);

							if (userEntity.getId() != null) {
								if (userHealthIDEntities.size() > 0) {
									userHealthIDRepository.save(userHealthIDEntities);
								}
							}
							break;
						case Constants.Health_Service_ID:

							break;
						case Constants.Student_ID:
							List<UserStudentIDEntity> userStudentIDEntities = new ArrayList<UserStudentIDEntity>();

							UserStudentIDEntity userStudentIDEntity = new UserStudentIDEntity();
							userStudentIDEntity.setDepartment("Health Management");
							userStudentIDEntity.setInstitution(organizationEntity.getDescription());
							userStudentIDEntity.setIssuerIN(organizationEntity.getIssuerIdentificationNumber());

							userStudentIDEntities.add(userStudentIDEntity);
							userEntity.setUserStudentIDs(userStudentIDEntities);

							if (userEntity.getId() != null) {
								if (userStudentIDEntities.size() > 0) {
									userStudentIDRepository.save(userStudentIDEntities);
								}
							}
							break;
						case Constants.Employee_ID:
							List<UserEmployeeIDEntity> userEmployeeIDEntities = new ArrayList<UserEmployeeIDEntity>();

							UserEmployeeIDEntity userEmployeeIDEntity = new UserEmployeeIDEntity();
							userEmployeeIDEntity.setDepartment("Engineering");
							userEmployeeIDEntity.setAffiliation("Civil Servant");

							userEmployeeIDEntities.add(userEmployeeIDEntity);
							userEntity.setUserEmployeeIDs(userEmployeeIDEntities);

							if (userEntity.getId() != null) {
								if (userEmployeeIDEntities.size() > 0) {
									userEmployeeIDRepository.save(userEmployeeIDEntities);
								}
							}
							break;
						default:
							break;
						}
						
						
					break;
				case "Biometric":
					UserBiometricEntity userBiometricEntity = null;
					List<UserBiometricEntity> bioEntities = userEntity.getUserBiometrics();
					boolean flag = true;
					switch (item.getSampleType()) {

					case "FaceFrontal":

						if (bioEntities != null && !bioEntities.isEmpty()) {
							for (UserBiometricEntity userBiometric : bioEntities) {
								if (userBiometric.getType().equals("FACE")) {
									userBiometricEntity = userBiometric;
									flag = false;
									break;
								}
							}
						}
						if (userBiometricEntity == null) {
							userBiometricEntity = new UserBiometricEntity();
							userBiometricEntity.setType("FACE");
							userBiometricEntity.setPosition(null);
							userBiometricEntity.setFormat("BASE64");
						}

						if (userBiometricEntity.getData() == null
								&& item.getSampleEncoding().equalsIgnoreCase("JPEG")) {
							userBiometricEntity
									.setData(convertJPEGtoPNGFormat(Base64.getDecoder().decode(item.getSampleData())));
						} else if (userBiometricEntity.getAnsi() == null
								&& item.getSampleEncoding().equalsIgnoreCase("INCITS_385")) {
							userBiometricEntity.setAnsi(item.getSampleData());
						}
						userBiometricEntity
								.setCreatedDate(simpleDateFormat_ngid.parse(item.getCollectedOn()).toInstant());
						if (flag) {
							userEntity.addUserBiometric(userBiometricEntity);
						}
						break;
					case "LeftEye":
						if (bioEntities != null && !bioEntities.isEmpty()) {
							for (UserBiometricEntity userBiometric : bioEntities) {
								if (userBiometric.getType().equals("IRIS")
										&& userBiometric.getPosition().equalsIgnoreCase("LEFT_EYE")) {
									userBiometricEntity = userBiometric;
									flag = false;
									break;
								}
							}
						}
						if (userBiometricEntity == null) {
							userBiometricEntity = new UserBiometricEntity();
							userBiometricEntity.setType("IRIS");
							userBiometricEntity.setPosition("LEFT_EYE");
							userBiometricEntity.setFormat("BASE64");
						}
						userBiometricEntity.setData(Base64.getDecoder().decode(item.getSampleData()));
						userBiometricEntity
								.setCreatedDate(simpleDateFormat_ngid.parse(item.getCollectedOn()).toInstant());
						if (flag) {
							userEntity.addUserBiometric(userBiometricEntity);
						}
						break;
					case "RightEye":
						if (bioEntities != null && !bioEntities.isEmpty()) {
							for (UserBiometricEntity userBiometric : bioEntities) {
								if (userBiometric.getType().equals("IRIS")
										&& userBiometric.getPosition().equalsIgnoreCase("RIGHT_EYE")) {
									userBiometricEntity = userBiometric;
									flag = false;
									break;
								}
							}
						}
						if (userBiometricEntity == null) {
							userBiometricEntity = new UserBiometricEntity();
							userBiometricEntity.setType("IRIS");
							userBiometricEntity.setPosition("RIGHT_EYE");
							userBiometricEntity.setFormat("BASE64");
						}
						userBiometricEntity.setData(Base64.getDecoder().decode(item.getSampleData()));
						userBiometricEntity
								.setCreatedDate(simpleDateFormat_ngid.parse(item.getCollectedOn()).toInstant());
						if (flag) {
							userEntity.addUserBiometric(userBiometricEntity);
						}
						break;
					case "LeftIndex":
						if (bioEntities != null && !bioEntities.isEmpty()) {
							for (UserBiometricEntity userBiometric : bioEntities) {
								if (userBiometric.getType().equalsIgnoreCase("FINGERPRINT")
										&& userBiometric.getPosition().equalsIgnoreCase("LEFT_INDEX")) {
									userBiometricEntity = userBiometric;
									flag = false;
									break;
								}
							}
						}
						if (userBiometricEntity == null) {
							userBiometricEntity = new UserBiometricEntity();
							userBiometricEntity.setType("FINGERPRINT");
							userBiometricEntity.setPosition("LEFT_INDEX");
							userBiometricEntity.setFormat("BASE64");
						}

						if (userBiometricEntity.getData() == null
								&& item.getSampleEncoding().equalsIgnoreCase("JPEG")) {
							// byte[] png =
							// Base64.getEncoder().encode(convertJPEGtoPNGFormat(Base64.getDecoder().decode(item.getSampleData())));
							userBiometricEntity
									.setData(convertJPEGtoPNGFormat(Base64.getDecoder().decode(item.getSampleData())));
							userBiometricEntity.setImageQuality(80);

						} else if (userBiometricEntity.getAnsi() == null
								&& item.getSampleEncoding().equalsIgnoreCase("INCITS_378")) {
							userBiometricEntity.setAnsi(item.getSampleData());
						}
						userBiometricEntity
								.setCreatedDate(simpleDateFormat_ngid.parse(item.getCollectedOn()).toInstant());
						if (flag) {
							userEntity.addUserBiometric(userBiometricEntity);
						}
						break;

					case "RightIndex":
						if (bioEntities != null && !bioEntities.isEmpty()) {
							for (UserBiometricEntity userBiometric : bioEntities) {
								if (userBiometric.getType().equalsIgnoreCase("FINGERPRINT")
										&& userBiometric.getPosition().equalsIgnoreCase("RIGHT_INDEX")) {
									userBiometricEntity = userBiometric;
									flag = false;
									break;
								}
							}
						}
						if (userBiometricEntity == null) {
							userBiometricEntity = new UserBiometricEntity();
							userBiometricEntity.setType("FINGERPRINT");
							userBiometricEntity.setPosition("RIGHT_INDEX");
							userBiometricEntity.setFormat("BASE64");
						}

						if (userBiometricEntity.getData() == null
								&& item.getSampleEncoding().equalsIgnoreCase("JPEG")) {
							// byte[] png =
							// Base64.getEncoder().encode(convertJPEGtoPNGFormat(Base64.getDecoder().decode(str)));
							userBiometricEntity
									.setData(convertJPEGtoPNGFormat(Base64.getDecoder().decode(item.getSampleData())));
							userBiometricEntity.setImageQuality(80);

						} else if (userBiometricEntity.getAnsi() == null
								&& item.getSampleEncoding().equalsIgnoreCase("INCITS_378")) {
							userBiometricEntity.setAnsi(item.getSampleData());
						}
						userBiometricEntity
								.setCreatedDate(simpleDateFormat_ngid.parse(item.getCollectedOn()).toInstant());
						if (flag) {
							userEntity.addUserBiometric(userBiometricEntity);
						}
						break;

					}
					break;
				case "Document":
					if (item.getInfo().getDocumentType().equalsIgnoreCase("EMPLOYMENT_AUTHORIZATION")) {
						List<Image> documentSacnImages = item.getImages();
						UserIDProofDocumentEntity userIDProofDocumentEntity = new UserIDProofDocumentEntity();
						userIDProofDocumentEntity
								.setName(userEntity.getReferenceName() + "_" + item.getInfo().getDocumentType());

						for (Image image : documentSacnImages) {

							if (image.getSide().equalsIgnoreCase("Front")) {
								if (image.getImageDataEncoding().equalsIgnoreCase("JPEG")) {
									userIDProofDocumentEntity
											.setFrontImg(("data:image/jpeg;base64," + image.getImageData()).getBytes());

								}
								if (image.getImageDataEncoding().equalsIgnoreCase("PNG")) {
									userIDProofDocumentEntity
											.setFrontImg(("data:image/png;base64," + image.getImageData()).getBytes());

								}
							} else if (image.getSide().equalsIgnoreCase("Back")) {
								if (image.getImageDataEncoding().equalsIgnoreCase("JPEG")) {
									userIDProofDocumentEntity
											.setBackImg(("data:image/jpeg;base64," + image.getImageData()).getBytes());

								}
								if (image.getImageDataEncoding().equalsIgnoreCase("PNG")) {
									userIDProofDocumentEntity
											.setBackImg(("data:image/png;base64," + image.getImageData()).getBytes());

								}
							}
						}
						// userIDProofDocumentEntity.setFileData(userIDProofDoc.getFile());
						
						List<IDProofTypeEntity> identitytypes = idProofTypeRepository.findAllByOrganization(organizationName);
						
						userIDProofDocumentEntity.setOrganizationIdentity(orgIdentitiesRepository.getOne(20l));
						userIDProofDocumentEntity.setIdProofIssuingAuthority(idProofIssuingAuthorityRepository.findOne(20l));
						userIDProofDocumentEntity.setUser(userEntity);
						userIDProofDocumentRepository.save(userIDProofDocumentEntity);
						userEntity.addUserIDProofDocuments(userIDProofDocumentEntity);

					}

					break;
				case "CardProfile":
					userBiometricEntity = new UserBiometricEntity();
					userBiometricEntity.setType("SIGNATURE");
					userBiometricEntity.setPosition(null);
					userBiometricEntity.setFormat("BASE64");
					userBiometricEntity.setData(convertJPEGtoPNGFormat(Base64.getDecoder().decode(item.getSignatureImage())));
					
					// convertJPEGtoPNGFormat(Base64.getDecoder().decode(item.getSampleData()))
					userBiometricEntity.setCreatedDate(simpleDateFormat_ngid.parse(item.getCollectedOn()).toInstant());
					userEntity.addUserBiometric(userBiometricEntity);
					break;
				default:
					break;
				}
			}

			/*
			 * int i = 0; if(i%2 == 0) { return 500; }
			 */

			/*********************************************************************************************************/
			IdentityProviderUser identityProviderUser = new IdentityProviderUser();
			identityProviderUser.setUserName(userEntity.getUserAttribute().getFirstName().replace(" ", "").trim());
			identityProviderUser.setFirstName(userEntity.getUserAttribute().getFirstName());
			identityProviderUser.setLastName(userEntity.getUserAttribute().getLastName());
			identityProviderUser.setEmail(userEntity.getUserAttribute().getEmail());
			if (userEntity.getUserAttribute().getContact() == null) {
				//
			} else {
				identityProviderUser.setPhoneNumber(String.valueOf(userEntity.getUserAttribute().getContact()));
			}
			identityProviderUser.setEnabled(true);
			identityProviderUser.setCredentialType("password");
			identityProviderUser.setCredentialValue("");
			identityProviderUser.setCredentialTemporary(true);
			identityProviderUser.setGroupId(groupID);
			identityProviderUser.setAuthServiceRoleID(roleIDs.split(","));
			identityProviderUser.setOrgId(organizationEntity.getId());
			identityProviderUser.setSendInvite(false);
			userEntity.setName(identityProviderUser.getFirstName() + " " + identityProviderUser.getLastName());
			userEntity.setReferenceName(identityProviderUser.getUserName());
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add(Constants.LOGGED_IN_USER, enrolledBy);
			if (httpHeaders.containsKey(Constants.UIMS_URL)) {
				headers.add(Constants.UIMS_URL, httpHeaders.get(Constants.UIMS_URL).get(0));
			}
			ResponseEntity<User> user = identityProviderUserService.createUserAndAssignGroupAndRoles(headers, organizationName, identityProviderUser,
					userEntity);

			return user.getStatusCode().value();
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.USER_NOT_ENROLLED, schema.getItems().get(0).getName()), cause);
			throw new EnrollmentServiceException(cause, ErrorMessages.USER_NOT_ENROLLED,
					schema.getItems().get(0).getName());
		}
	}

	@Override
	public List<WFStepVisualTemplateConfig> getWorkflowVisualTemplates(String organizationName, String userName) {
		return userCredentialsService.getWorkflowVisualTemplates("web_portal_enroll_summary", organizationName,
				userName);
	}

	@Override
	public List<MobileCredential> getUserMobileIDIdentities(Long userId) {
		UserEntity user = userRepository.getOne(userId); // .getReferenceName()
		Map<Class<?>, Object> map = userCredentialsService
				.getGroupAndWorkflowByUserName(user.getOrganization().getName(), user.getReferenceName());
		String referenceGroupId = ((GroupRepresentation) map.get(GroupRepresentation.class)).getId();
		String workflowName = ((Workflow) map.get(Workflow.class)).getName();

		List<MobileCredential> mobileCredentials = new ArrayList<>();

		WorkflowStepDetail workflowStepDetail = workflowService.getStepDetails(user.getOrganization().getName(),
				workflowName, "MOBILE_ID_IDENTITY_ISSUANCE");
		List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdConfig = workflowStepDetail
				.getWfMobileIDStepIdentityConfigs();
		Map<String,Boolean> workingCreds = keycloakService.checkForCredentials(user.getOrganization().getName(), user.getReferenceName());
		if (wfMobileIDStepIdConfig != null) {
			wfMobileIDStepIdConfig.forEach(wfMobileIdStep -> {
				MobileCredential mobileCredential = new MobileCredential();

				mobileCredential.setFriendlyName(wfMobileIdStep.getFriendlyName());
				if (wfMobileIdStep.getWfMobileIdStepCertificates().size() > 0) {
					mobileCredential.setPkiCertificatesEnabled(true);
					mobileCredential.setNoOfCertificates(wfMobileIdStep.getWfMobileIdStepCertificates().size());
				}
				if (wfMobileIdStep.getSoftOTP()) {
					mobileCredential.setSoftOTP(true);
					if(workingCreds.get("OTP") != null) {
						if(workingCreds.get("OTP").booleanValue()) {
							mobileCredential.setOtpConfigStatus("ACTIVE");						
						}else {
							mobileCredential.setOtpConfigStatus("INACTIVE");
						}
					}
					mobileCredential.setWfMobileIDIdentitySoftOTPConfig(wfMobileIdStep.getWfMobileIDIdentitySoftOTPConfig());
					// TODO get otp related info from respective table and set otp number here
				} else {
					mobileCredential.setSoftOTP(false);
				}
				if (wfMobileIdStep.getPushVerify()) {
					mobileCredential.setPushVerify(true);
					// TODO get push verify related info and set push verify number here
				} else {
					mobileCredential.setPushVerify(false);
				}
				if (wfMobileIdStep.getFido2()) {
					mobileCredential.setFido2(true);
					// TODO get push verify related info and set push verify number here
				} else {
					mobileCredential.setFido2(false);
				}
				wfMobileIdStep.getWfMobileIdStepVisualIDGroupConfigs().forEach(visualIdGroupConfig -> {
					if (visualIdGroupConfig.getReferenceGroupId().equalsIgnoreCase(referenceGroupId)) {
						mobileCredential.setWfMobileIDVisualConfigId(visualIdGroupConfig.getId());
						// TODO set visual number here
						mobileCredential.setCredentialTemplateID(visualIdGroupConfig.getVisualTemplateId());
						switch (visualIdGroupConfig.getIdentityTypeName()) {
						case Constants.PIV_and_PIV_1:
							mobileCredential.setVisualIdNumber(user.getUserPivCredentials().get(0).getPivIdNumber());
							break;
						case Constants.National_ID:
							mobileCredential.setVisualIdNumber(user.getUserNationalId().getPersonalCode());
							break;
						case Constants.Voter_ID:
							mobileCredential.setVisualIdNumber(user.getUserVoterID().getCurp());
							break;
						case Constants.Permanent_Resident_ID:
							mobileCredential.setVisualIdNumber(user.getUserPermanentResidentId().getPersonalCode());
							break;
						case Constants.Employee_ID:
							mobileCredential.setVisualIdNumber(user.getUserEmployeeIDs().get(0).getEmployeeIDNumber());
							break;
						case Constants.Driving_License:
							mobileCredential.setVisualIdNumber(user.getUserDrivingLicenses().get(0).getLicenseNumber());
							break;
						case Constants.Health_ID:
							mobileCredential.setVisualIdNumber(user.getUserHealthIDs().get(0).getIdNumber());
							break;
						case Constants.Student_ID:
							mobileCredential.setVisualIdNumber(user.getUserStudentIDs().get(0).getStudentIdNumber());
							break;
						default:
						}
					}
				});
				mobileCredentials.add(mobileCredential);
			});
		}
		return mobileCredentials;
	}

	@Override
	public WfMobileIDStepDetails getUserWfMobileIDStepDetails(Long userId) {
		WfMobileIDStepDetails wfMobileIDStepDetails = new WfMobileIDStepDetails();
		UserEntity user = userRepository.getOne(userId); // .getReferenceName()
		Map<Class<?>, Object> map = userCredentialsService
				.getGroupAndWorkflowByUserName(user.getOrganization().getName(), user.getReferenceName());
		String referenceGroupId = ((GroupRepresentation) map.get(GroupRepresentation.class)).getId();
		String workflowName = ((Workflow) map.get(Workflow.class)).getName();

		if (wfStepRepository.findStepDetailByName(user.getOrganization().getName(), workflowName, "MOBILE_ID_HARDWARE_BACKED_KEYSTORE").isPresent()) {
			wfMobileIDStepDetails.setHardwareBackedAuthentication(true);
		}
		wfMobileIDStepDetails.setWfMobileIDStepOnboardConfig(getWfMobileIDStepOnboardConfig(user, workflowName));
		wfMobileIDStepDetails.setWfMobileIDStepIdentityConfigs(getWfMobileIDStepIdentityConfigs(user, workflowName, referenceGroupId));
		return wfMobileIDStepDetails;
	}

	private WFMobileIDStepOnboardConfig getWfMobileIDStepOnboardConfig(UserEntity user, String workflowName) {
		WorkflowStepDetail workflowMobileIdOnboardStepDetail = workflowService
				.getStepDetails(user.getOrganization().getName(), workflowName, "MOBILE_ID_ONBOARDING_CONFIG");
		if (workflowMobileIdOnboardStepDetail != null) {
			List<WFStepGenericConfig> wfStepConfigs = workflowMobileIdOnboardStepDetail.getWfStepGenericConfigs();
			if (wfStepConfigs.size() != 0) {
				WFMobileIDStepOnboardConfig wfMobileIDStepOnboardConfig = new WFMobileIDStepOnboardConfig();
				WFMobileIDStepOnboardAndEnrollConfig onboardAndEnrollConfig = new WFMobileIDStepOnboardAndEnrollConfig();
				;
				WFMobileIDStepFaceVerificationConfig faceVerification = new WFMobileIDStepFaceVerificationConfig();
				WFMobileIDStepAdjudicationConfig adjudication = new WFMobileIDStepAdjudicationConfig();
				wfStepConfigs.forEach(wfStepConfig -> {

					if (wfStepConfig.getName().equalsIgnoreCase("ONBOARD_ENROLL_USING_EXISTING_IDENTITY")) {
						if (wfStepConfig.getValue().equalsIgnoreCase("Y")) {
							wfMobileIDStepOnboardConfig.setRequireOnboardAndEnroll(true);

							List<OrganizationIdentity> trustedExistingIdentities = new ArrayList<>();

							workflowMobileIdOnboardStepDetail.getWfMobileIDStepTrustedIdentities()
									.forEach(trustedIdentity -> {
										OrganizationIdentity orgIdentities = new OrganizationIdentity();
										orgIdentities.setId(trustedIdentity.getId());
										orgIdentities.setIdentityTypeName(trustedIdentity.getIdentityTypeName());
										orgIdentities.setIdentityTypeDetails(trustedIdentity.getIdentityTypeDetails());
										trustedExistingIdentities.add(trustedIdentity);
									});
							onboardAndEnrollConfig.setTrustedExistingIdentities(trustedExistingIdentities);
						} else {
							wfMobileIDStepOnboardConfig.setRequireOnboardAndEnroll(false);
						}
					}
					if (wfMobileIDStepOnboardConfig.getRequireOnboardAndEnroll()) {
						if (wfStepConfig.getName().equalsIgnoreCase("SCAN_QR_CODE")) {
							if (wfStepConfig.getValue().equalsIgnoreCase("Y")) {
								onboardAndEnrollConfig.setScanQrCode(true);
							} else {
								onboardAndEnrollConfig.setScanQrCode(false);
							}
						}
						if (wfStepConfig.getName().equalsIgnoreCase("TAP_AND_READ_THROUGH_NFC")) {
							if (wfStepConfig.getValue().equalsIgnoreCase("Y")) {
								onboardAndEnrollConfig.setTapAndReadThroughNFC(true);
							} else {
								onboardAndEnrollConfig.setTapAndReadThroughNFC(false);
							}
						}
						if (wfStepConfig.getName().equalsIgnoreCase("CAPTURE_FRONT_OF_DOCUMENT")) {
							if (wfStepConfig.getValue().equalsIgnoreCase("Y")) {
								onboardAndEnrollConfig.setCaptureFrontDocument(true);
							} else {
								onboardAndEnrollConfig.setCaptureFrontDocument(false);
							}
						}
						if (wfStepConfig.getName().equalsIgnoreCase("CAPTURE_BACK_OF_DOCUMENT")) {
							if (wfStepConfig.getValue().equalsIgnoreCase("Y")) {
								onboardAndEnrollConfig.setCaptureBackDocument(true);
							} else {
								onboardAndEnrollConfig.setCaptureBackDocument(false);
							}
						}
					}
					if (wfStepConfig.getName().equalsIgnoreCase("FACE_VERIFY_DURING_ONBOARDING_REQUIRED")) {
						if (wfStepConfig.getValue().equalsIgnoreCase("Y")) {
							wfMobileIDStepOnboardConfig.setRequireFaceVerification(true);
						} else {
							wfMobileIDStepOnboardConfig.setRequireFaceVerification(false);
						}
					}
					if (wfMobileIDStepOnboardConfig.getRequireFaceVerification()) {
						if (wfStepConfig.getName().equalsIgnoreCase("SAVE_CAPTURED_FACE_FOR_VISUAL_ID")) {
							if (wfStepConfig.getValue().equalsIgnoreCase("Y")) {
								faceVerification.setSaveCapturedFaceForVisualID(true);
							} else {
								faceVerification.setSaveCapturedFaceForVisualID(false);
							}
						}
						if (wfStepConfig.getName().equalsIgnoreCase("CROP_SIZE")) {
							faceVerification.setCropSize(wfStepConfig.getValue());
						}
					}
					if (wfStepConfig.getName().equalsIgnoreCase("APPROVAL_OR_ADJUDICATION_REQUIRED")) {
						if (wfStepConfig.getValue().equalsIgnoreCase("Y")) {
							wfMobileIDStepOnboardConfig.setRequireAdjudicationOrAprroval(true);
						} else {
							wfMobileIDStepOnboardConfig.setRequireAdjudicationOrAprroval(false);
						}
					}

					if (wfMobileIDStepOnboardConfig.getRequireAdjudicationOrAprroval()) {
						List<Group> groups = new ArrayList<>();
						workflowMobileIdOnboardStepDetail.getWfStepAdjudicationGroupConfigs().forEach(adjGroup -> {
							Group group = new Group();
							group.setId(adjGroup.getId());
							group.setName(adjGroup.getName());
							groups.add(group);
						});
						adjudication.setAdjudicationGroups(groups);
					}
				});
				wfMobileIDStepOnboardConfig.setOnboardAndEnroll(onboardAndEnrollConfig);
				wfMobileIDStepOnboardConfig.setFaceVerification(faceVerification);
				wfMobileIDStepOnboardConfig.setAdjudication(adjudication);
				return wfMobileIDStepOnboardConfig;
			}
		}
		return null;
	}

	private List<WFMobileIDStepIdentityConfig> getWfMobileIDStepIdentityConfigs(UserEntity user, String workflowName, String referenceGroupId) {
		List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdentityConfigs = new ArrayList<>();
		WorkflowStepDetail workflowStepDetail = workflowService.getStepDetails(user.getOrganization().getName(), workflowName, "MOBILE_ID_IDENTITY_ISSUANCE");
		List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdConfig = workflowStepDetail
				.getWfMobileIDStepIdentityConfigs();
		if (wfMobileIDStepIdConfig != null) {
			wfMobileIDStepIdConfig.forEach(wfMobileIdStep -> {
				WFMobileIDStepIdentityConfig wfMobileIDStepIdentityConfig = new WFMobileIDStepIdentityConfig();
				wfMobileIDStepIdentityConfig
						.setWfMobileIDStepIdentityConfigId(wfMobileIdStep.getWfMobileIDStepIdentityConfigId());
				wfMobileIDStepIdentityConfig.setFriendlyName(wfMobileIdStep.getFriendlyName());
				wfMobileIDStepIdentityConfig.setPushVerify(wfMobileIdStep.getPushVerify());
				wfMobileIDStepIdentityConfig.setSoftOTP(wfMobileIdStep.getSoftOTP());
				wfMobileIDStepIdentityConfig.setFido2(wfMobileIdStep.getFido2());
				wfMobileIDStepIdentityConfig.setIsContentSigning(wfMobileIdStep.getIsContentSigning());
				wfMobileIDStepIdentityConfig.setPkiCertificatesEnabled(wfMobileIdStep.getPkiCertificatesEnabled());
				
				if(wfMobileIdStep.getSoftOTP() == true) {
					wfMobileIDStepIdentityConfig.setWfMobileIDIdentitySoftOTPConfig(wfMobileIdStep.getWfMobileIDIdentitySoftOTPConfig());
				}
				// set visual ids list if required
				wfMobileIdStep.getWfMobileIdStepVisualIDGroupConfigs().forEach(visualIdGroupConfig -> {
					if (visualIdGroupConfig.getReferenceGroupId().equalsIgnoreCase(referenceGroupId)) {
						wfMobileIDStepIdentityConfig.setWfMobileIDStepIdentityConfigId(visualIdGroupConfig.getId());
						wfMobileIDStepIdentityConfig.setWfMobileIdStepVisualIDGroupConfigId(visualIdGroupConfig.getId());
						wfMobileIDStepIdentityConfig.setIdentityTypeName(visualIdGroupConfig.getIdentityTypeName());
					}
				});
				if (wfMobileIdStep.getWfMobileIdStepCertificates().size() > 0) {
					wfMobileIdStep.getWfMobileIdStepCertificates().forEach(cert -> {
						WFMobileIdStepCertificate wfMobileIdStepCertificates = new WFMobileIdStepCertificate();
						wfMobileIdStepCertificates.setWfMobileIdStepCertId(cert.getWfMobileIdStepCertId());
						
						wfMobileIdStepCertificates.setCertType(cert.getCertType());
						wfMobileIdStepCertificates.setCaServer(cert.getCaServer());
						wfMobileIdStepCertificates.setCertTemplate(cert.getCertTemplate());
						wfMobileIdStepCertificates.setAlgorithm(cert.getAlgorithm());
						wfMobileIdStepCertificates.setKeysize(cert.getKeysize());
						wfMobileIdStepCertificates.setKeyEscrow(cert.isKeyEscrow());
						wfMobileIdStepCertificates.setDisableRevoke(cert.isDisableRevoke());
						wfMobileIdStepCertificates.setSubjectDN(cert.getSubjectDN());
						// prepare CSRBean obj
						wfMobileIdStepCertificates.setSubjectAN(cert.getSubjectAN());

						wfMobileIDStepIdentityConfig.addWfMobileIdStepCertificate(wfMobileIdStepCertificates);
					});
					WFMobileIDStepCertConfig wfMobileIDStepCertConfig = wfMobileIdStep.getWfMobileIDStepCertConfig();
					//wfMobileIDStepIdentityConfig.setIsContentSigning(wfMobileIDStepCertConfig.getIsContentSigning());
					wfMobileIDStepIdentityConfig
							.setEmailNotificationFreq(wfMobileIDStepCertConfig.getEmailNotificationFreq().getValue());
					wfMobileIDStepIdentityConfig.setNoOfDaysBeforeCertExpiry(
							wfMobileIDStepCertConfig.getNoOfDaysBeforeCertExpiry().getValue());
				}
				wfMobileIDStepIdentityConfigs.add(wfMobileIDStepIdentityConfig);
			});
			return wfMobileIDStepIdentityConfigs;
		}
		return wfMobileIDStepIdentityConfigs;
	}

	@Override
	public User getUserRefNameOrgByUserId(Long userId) {
		UserEntity userEntity = userRepository.getOne(userId);
		User user = new User();
		user.setId(userEntity.getId());
		user.setOrganisationName(userEntity.getOrganization().getName());
		user.setName(userEntity.getReferenceName());
		if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name()) || userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_APPROVAL.name())) {
			user.setCanIssueIdentity(false);
		} else {
			user.setCanIssueIdentity(true);
		}
		return user;
	}

	@Override
	public ResultPage getUsersByLocation(String organizationName, String searchCriteria, int page, int size) {

		try {

			GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);

			GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
			int radius = 100;
			int numPoins = 50;
			String[] str_point = searchCriteria.split(",");
			if (str_point.length <= 1) {
				str_point = new String[] { "0", "0" };
			}

			String wktPoint = "Point(10 8)";
			// First interpret the WKT string to a point
			WKTReader fromText = new WKTReader();
			Geometry geom = null;
			try {
				geom = fromText.read(wktPoint);
			} catch (ParseException e) {
				throw new RuntimeException("Not a WKT string:" + wktPoint);
			}

			shapeFactory.setNumPoints(numPoins);

			shapeFactory.setCentre(
					new Coordinate(Double.valueOf(str_point[0].trim()), Double.valueOf(str_point[1].trim())));
			shapeFactory.setSize(radius * 2);
			Geometry circle = shapeFactory.createCircle();
			List<Point> points = new ArrayList<Point>();
			for (com.vividsolutions.jts.geom.Coordinate coordinate : circle.getCoordinates()) {
				Point lngLatAtl = geometryFactory.createPoint(new Coordinate(coordinate.x, coordinate.y));
				points.add(lngLatAtl);
			}
			Collections.reverse(points);
			CoordinateArraySequence coordinateArraySeq = new CoordinateArraySequence(points.size());
			Coordinate[] coordinates = coordinateArraySeq.toCoordinateArray();
			int i = 0;
			for (Point point : points) {
				coordinates[i] = point.getCoordinate();
				i++;
			}

			// Point point = geometryFactory.createPoint( new
			// Coordinate(Double.valueOf(str_point[0].trim()),
			// Double.valueOf(str_point[1].trim()) ) );
			Polygon footprint = geometryFactory.createPolygon(coordinates);
			System.out.println(footprint);
			// list = userLocationCustomServiceImpl.findByLocation(point);
			return userLocationCustomServiceImpl.findByLocation(footprint, page, size);

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.USER_NOT_FOUND_WITH_SEARCHCRITERIA, searchCriteria, organizationName),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_WITH_SEARCHCRITERIA, searchCriteria,
					organizationName);
		}

	}

	@Override
	public void updateUsersLocationStatus(String organizationName, int status, List<Long> markedUsers, int enablePush) {

		try {

			List<UserLocationEntity> userLocationEntities = userLocationRepository.findByID(markedUsers);
			for (UserLocationEntity userLocationEntity : userLocationEntities) {
				userLocationEntity.setStatus(status);
			}

			if (enablePush == 1) {
				for (Long userID : markedUsers) {

					UserDevice device = new UserDevice();
					device.setChallenge(organizationName + "#" + userID + "#" + status);
					apduFeignClient.sendStatusChangeNotification("MobileID_App", userID, Long.valueOf(status), device);
				}
			}

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					String.format(ErrorMessages.FAILED_TO_FETCH_PENDINGISSUANCES_UNDER_ORGANIZATION, organizationName),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessages.FAILED_TO_FETCH_PENDINGISSUANCES_UNDER_ORGANIZATION,
					organizationName);
		}

	}

	@Override
	public UserBioInfo getUserBiometricsByUserId(Long userId) {
		UserEntity userEntity = userRepository.getOne(userId);
		UserBioInfo userBioInfo = new UserBioInfo();
		List<UserBiometricEntity> userBiometrics = userEntity.getUserBiometrics();

		if (!userBiometrics.isEmpty()) {
			userBioInfo = getUserBioInfoObj(getUserBiometrics(userBiometrics));
			userBioInfo.setUserID(userId);
			return userBioInfo;
		} else {
			log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_BIOMETRICS_NOT_FOUND_WITH_USERNAME, userId));
			throw new EntityNotFoundException(Util.format(ErrorMessages.USER_BIOMETRICS_NOT_FOUND_WITH_USERNAME, userId));
		}
	}

	private UserBioInfo getUserBioInfoObj(List<UserBiometric> userBiometrics) {
		UserBioInfo userBioInfo = new UserBioInfo();
		FingerprintModality fingerprintModality = null;
		List<FingerprintModality> fingerprintModalities = new ArrayList<>();
		List<IRISModality> irisModalities = new ArrayList<>();
		IRISModality irisModality = null;
		if (!userBiometrics.isEmpty()) {
			for (UserBiometric userBiometric : userBiometrics) {
				switch (userBiometric.getType()) {
				case "FACE":
					FaceModality faceModality = new FaceModality();
					faceModality.setFaceImg(Base64.getEncoder().encodeToString(userBiometric.getData()));
					userBioInfo.setFaceModality(faceModality);
					break;
				case "IRIS":
					irisModality = new IRISModality();
					if (userBiometric.getPosition().equalsIgnoreCase("LEFT_EYE")) {
						irisModality.setIrisPosition("LEFT");
					} else if (userBiometric.getPosition().equalsIgnoreCase("RIGHT_EYE")) {
						irisModality.setIrisPosition("RIGHT");
					}
					irisModality.setIrisData(Base64.getEncoder().encodeToString(userBiometric.getData()));
					irisModalities.add(irisModality);
					break;
				case "FINGERPRINT":
					 fingerprintModality = new FingerprintModality();
					// Set Finger Position. Better change finger position values in Config_Value
					// Table to same as Neuro Engine. Because this is unnecessary code.
					switch (userBiometric.getPosition()) {
					// Left Hand Fingers
					case "LEFT_THUMB":
						fingerprintModality.setFingerPosition(FingerprintEnum.LEFT_THUMB.name());
						break;
					case "LEFT_INDEX":
						fingerprintModality.setFingerPosition(FingerprintEnum.LEFT_INDEX_FINGER.name());
						break;
					case "LEFT_MIDDLE":
						fingerprintModality.setFingerPosition(FingerprintEnum.LEFT_MIDDLE_FINGER.name());
						break;
					case "LEFT_RING":
						fingerprintModality.setFingerPosition(FingerprintEnum.LEFT_RING_FINGER.name());
						break;
					case "LEFT_LITTLE":
						fingerprintModality.setFingerPosition(FingerprintEnum.LEFT_LITTLE_FINGER.name());
						break;
					// Right hand fingers
					case "RIGHT_THUMB":
						fingerprintModality.setFingerPosition(FingerprintEnum.RIGHT_THUMB.name());
						break;
					case "RIGHT_INDEX":
						fingerprintModality.setFingerPosition(FingerprintEnum.RIGHT_INDEX_FINGER.name());
						break;
					case "RIGHT_MIDDLE":
						fingerprintModality.setFingerPosition(FingerprintEnum.RIGHT_MIDDLE_FINGER.name());
						break;
					case "RIGHT_RING":
						fingerprintModality.setFingerPosition(FingerprintEnum.RIGHT_RING_FINGER.name());
						break;
					case "RIGHT_LITTLE":
						fingerprintModality.setFingerPosition(FingerprintEnum.RIGHT_LITTLE_FINGER.name());
						break;
					}
					// Set Finger Data
					fingerprintModality.setFingerInputData(Base64.getEncoder().encodeToString(userBiometric.getData()));
					fingerprintModality.setImageType(userBiometric.getFormat());
					fingerprintModalities.add(fingerprintModality);
					break;
				case "":
					break;
				}
			}
			userBioInfo.setFingerprintModalities(fingerprintModalities);
			userBioInfo.setIrisModalities(irisModalities);
			return userBioInfo;
		}
		return userBioInfo;
	}
	@Override
	public UserBioInfo getUserBioByUserIdAndBioType(Long userId, String biometricType) {
		List<UserBiometricEntity> userBiometrics = userBiometricRepository.getUserBiometricsByType(userId, biometricType);
		UserBioInfo userBioInfo=new UserBioInfo();
		FingerprintModality fingerModality=null;
		List<FingerprintModality> fingerList=new ArrayList<>();
				
		if(!userBiometrics.isEmpty()) {
			for(UserBiometricEntity userBioEntity: userBiometrics) {
				if(userBioEntity.getType().equalsIgnoreCase("FACE")) {
					FaceModality face=new FaceModality();
					face.setFaceImg(Base64.getEncoder().encodeToString(userBioEntity.getData()));
					userBioInfo.setFaceModality(face);
				} else if(userBioEntity.getType().equalsIgnoreCase("FINGERPRINT")) {
					fingerModality =new FingerprintModality();
					fingerModality.setFingerPosition(userBioEntity.getPosition());
					fingerModality.setFingerInputData(Base64.getEncoder().encodeToString(userBioEntity.getData()));		
					fingerList.add(fingerModality);
				}				
			}		
			userBioInfo.setFingerprintModalities(fingerList);			
			return userBioInfo;
		} else {
			log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_BIOMETRICS_NOT_FOUND_WITH_USERNAME, userId));
			throw new EntityNotFoundException(Util.format(ErrorMessages.USER_BIOMETRICS_NOT_FOUND_WITH_USERNAME, userId));
		}		
	}
	
	
	
	@Override
	public ResponseEntity<Void> enableSoftOTP2FA(String organizationName, EnableSoftOTP2FA enableSoftOTP2FA) {
		try {
			String  username = userRepository.getUserNameByUserID(enableSoftOTP2FA.getUserid(), enableSoftOTP2FA.getOrganizationName());
			keycloakService.enableSoftOTP2FA(organizationName, username, enableSoftOTP2FA);
			return new ResponseEntity<Void>(HttpStatus.OK);
		}catch(Exception e) {			
			return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
		}
			
	}

	@Override
	public String generateSessionId(String organizationName, Long userId) throws PairDeviceInfoServiceException {
		UserEntity userEntity = userRepository.getOne(userId);
		String secretKey = null;
		if (userEntity != null) {
			PairMobileDeviceConfig userPairDevice = userPairDeviceInfoService.generateUserIdentifierToPairMobile(userEntity);
			if (userPairDevice != null) {
				secretKey = userPairDevice.getSecretKey();
			}
		}
		return secretKey;
	}

	@Override
	public ResponseEntity<Void> approveEnrollment(HttpHeaders httpHeaders, String organizationName, String userName, ApprovalAttributes approveAttr) {
		try {
			String loggedInUser = null;
			Long loggedInUserId = null;
			HeaderUtil.validateRequiredHeaders(httpHeaders, Arrays.asList(Constants.LOGGED_IN_USER, Constants.LOGGED_IN_USER_ID));
			if (httpHeaders.containsKey(Constants.LOGGED_IN_USER)) {
				loggedInUser = httpHeaders.get(Constants.LOGGED_IN_USER).get(0);
			}
			if (httpHeaders.containsKey(Constants.LOGGED_IN_USER_ID)) {
				loggedInUserId = Long.valueOf(httpHeaders.get(Constants.LOGGED_IN_USER_ID).get(0));
			}
			Long userId=userRepository.getUserIDByName(organizationName, userName);
			UserEntity userEntity = userRepository.findOne(userId);
			UserEnrollmentHistoryEntity userEnrollmentHistoryEntity = null;
			if(userEntity!=null) {
				for(UserEnrollmentHistoryEntity userEnrollment:userEntity.getUserEnrollmentsHistory()) {
					if(userEnrollment.getStatus()!=null && userEnrollment.getStatus().equalsIgnoreCase("Active")){
						userEnrollment.setStatus("Inactive");
						userEnrollmentHistoryEntity = new UserEnrollmentHistoryEntity();
					} else if(userEnrollment.getStatus()== null) {
						userEnrollmentHistoryEntity = userEnrollment;
					}
				}
				if(userEnrollmentHistoryEntity == null) {
					userEnrollmentHistoryEntity = new UserEnrollmentHistoryEntity();
				}
				userEnrollmentHistoryEntity.setReason(approveAttr.getReason());
				userEnrollmentHistoryEntity.setApprovedBy(approveAttr.getApprovedBy());
				userEnrollmentHistoryEntity.setApprovalStatus("Approved");
				userEnrollmentHistoryEntity.setApprovedDate(Timestamp.from(Instant.now()));
				// IDENTITY_ISSUED case never arises
				if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.IDENTITY_ISSUED.name())) {
					userEntity.setStatus(UserStatusEnum.IDENTITY_ISSUED.name());	
				} else {
					if(apduFeignClient.getissuedUserCredentialsCount(userId, organizationName) > 0l) {
						userEntity.setStatus(UserStatusEnum.READY_FOR_REISSUANCE.name());
					}else {
						userEntity.setStatus(UserStatusEnum.READY_FOR_ISSUANCE.name());						
					}
				}
				userEnrollmentHistoryEntity.setStatus(userEntity.getStatus());
				userEnrollmentHistoryEntity.setUser(userEntity);
				userEntity.addUserEnrollmentHistory(userEnrollmentHistoryEntity);
//				userRepository.save(userEntity);
				sendPairYourDeviceInviteNotification(organizationName, userEntity, loggedInUserId, loggedInUser);
				
			}
			return new ResponseEntity<Void>(HttpStatus.OK);
			//userEnrollmentHistoryRepository.approve(organizationName, userName, reason);
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					String.format(ErrorMessages.FAILED_TO_APPROVE_REQUEST, organizationName),
					cause);
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}

	@Override
	public ResponseEntity<Void> rejectEnrollment(String organizationName, String userName, ApprovalAttributes approveAttr) {
		try {
			Long userId=userRepository.getUserIDByName(organizationName, userName);
			UserEntity userEntity = userRepository.getOne(userId);
			UserEnrollmentHistoryEntity userEnrollmentHistoryEntity = null;
			if(userEntity!=null) {
				System.out.println("UserServiceImpl.rejectEnrollment()"+userEntity.getUserEnrollmentsHistory().size());
				for(UserEnrollmentHistoryEntity userEnrollment:userEntity.getUserEnrollmentsHistory()) {
					if(userEnrollment.getStatus()!=null && userEnrollment.getStatus().equalsIgnoreCase("Active")){
						userEnrollment.setStatus("Inactive");
						userEnrollmentHistoryEntity=new UserEnrollmentHistoryEntity();
					} else if(userEnrollment.getStatus()== null) {
						userEnrollmentHistoryEntity = userEnrollment;
					}
				}
				if(userEnrollmentHistoryEntity == null) {
					userEnrollmentHistoryEntity = new UserEnrollmentHistoryEntity();
				}
				userEnrollmentHistoryEntity.setReason(approveAttr.getReason());
				userEnrollmentHistoryEntity.setRejectedBy(approveAttr.getRejectedBy());
				userEnrollmentHistoryEntity.setApprovalStatus("Rejected");
				userEnrollmentHistoryEntity.setRejectedDate(Timestamp.from(Instant.now()));
				userEntity.setStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name());
				userEnrollmentHistoryEntity.setStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name());
				userEnrollmentHistoryEntity.setUser(userEntity);
				userEntity.addUserEnrollmentHistory(userEnrollmentHistoryEntity);
			}
			return new ResponseEntity<Void>(HttpStatus.OK);
			//userEnrollmentHistoryRepository.reject(organizationName, userName, reason);
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					String.format(ErrorMessages.FAILED_TO_REJECT_REQUEST, organizationName),
					cause);
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}

	@Override
	public UserDevice validateUserSession(String organizationName, String userName) {
		
			UserRepresentation userRepresentation = keycloakService.validateUserSession(organizationName, userName);

			UserEntity userEntity = userRepository.getUserByName(userName, organizationName);
			
			if(userEntity == null) {
				userEntity= new UserEntity();
				
				userEntity.setName(userRepresentation.getFirstName()+" "+userRepresentation.getLastName());
				userEntity.setStatus(UserStatusEnum.PENDING_ENROLLMENT.name());
				userEntity.setUserStatus("ACTIVE");
				userEntity.setReferenceName(userRepresentation.getUsername());
				
				List<GroupRepresentation> groupRepresentations = keycloakService.getGroupsByUserID(userRepresentation.getId(), organizationName);
				userEntity.setGroupId(groupRepresentations.get(0).getId());
				userEntity.setGroupName(groupRepresentations.get(0).getName());
				
				UserAttributeEntity userAttributeEntity = new UserAttributeEntity(); 
				userAttributeEntity.setFirstName(userRepresentation.getFirstName());
				userAttributeEntity.setLastName(userRepresentation.getLastName());
				userAttributeEntity.setEmail(userRepresentation.getEmail());
				userEntity.setUserAttribute(userAttributeEntity);
				
				addUser(userEntity,organizationName);
			}
			
			PairMobileDeviceConfigEntity pairMobileDeviceConfigEntity = pairMobileDeviceConfigRepository
					.findPMDConfigByOrg(organizationName);

			String uuid = null;
			if (pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength() == SecretKeyLength.THIRTYTWO
					.getValue()) {
				uuid = UUID.randomUUID().toString();
			} else if (pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength() < SecretKeyLength.THIRTYTWO
			  		.getValue()) {
				uuid = Util.getAlphaNumericString(pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength());
			}
			UserIdentificationCodeEntity userIdentificationCodeEntity = new UserIdentificationCodeEntity();
			userIdentificationCodeEntity.setUuid(uuid);
			userIdentificationCodeEntity.setUser(userEntity);
			userIdentificationCodeEntity.setStatus(true);
			userIdentificationCodeEntity.setValidityFrom(Instant.now());
			userIdentificationCodeEntity.setValidityTill(new Date(
					System.currentTimeMillis() + (pairMobileDeviceConfigEntity.getQrCodeExpiryInMins()) * 60 * 1000)
							.toInstant());

			userIdentificationCodeRepository.save(userIdentificationCodeEntity);
			UserDevice userDevice = new UserDevice();
			userDevice.setUuid(uuid);
			return userDevice;
		
	}

	@Override
	public Long getIssuedDevicesByUserName(String userName, String organizationName) {
		UserEntity userEntity = userRepository.getUserByName(userName, organizationName);
		if(userEntity == null) {
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(userName, organizationName);
			userEntity= new UserEntity();
			
			userEntity.setName(userRepresentation.getFirstName()+" "+userRepresentation.getLastName());
			userEntity.setStatus(UserStatusEnum.PENDING_ENROLLMENT.name());
			userEntity.setUserStatus("ACTIVE");
			userEntity.setReferenceName(userRepresentation.getUsername());
			
			List<GroupRepresentation> groupRepresentations = keycloakService.getGroupsByUserID(userRepresentation.getId(), organizationName);
			userEntity.setGroupId(groupRepresentations.get(0).getId());
			userEntity.setGroupName(groupRepresentations.get(0).getName());
			
			UserAttributeEntity userAttributeEntity = new UserAttributeEntity(); 
			userAttributeEntity.setFirstName(userRepresentation.getFirstName());
			userAttributeEntity.setLastName(userRepresentation.getLastName());
			userAttributeEntity.setEmail(userRepresentation.getEmail());
			userEntity.setUserAttribute(userAttributeEntity);
			
			addUser(userEntity,organizationName);
			return 0l;
		}else {
			return apduFeignClient.getCountOfIssuedUserDevices(userEntity.getId(), organizationName);
		}
	}

	public UserDevice getUUID(String organizationName, String userName) {
		
		UserRepresentation userRepresentation = keycloakService.getUserByUsername(userName, organizationName);
		
		UserEntity userEntity = userRepository.getUserByName(userRepresentation.getUsername(), organizationName);
				
		if(userEntity == null) {
			userEntity= new UserEntity();
			
			userEntity.setName(userRepresentation.getFirstName()+" "+userRepresentation.getLastName());
			userEntity.setStatus(UserStatusEnum.PENDING_ENROLLMENT.name());
			userEntity.setUserStatus("ACTIVE");
			userEntity.setReferenceName(userRepresentation.getUsername());
			
			List<GroupRepresentation> groupRepresentations = keycloakService.getGroupsByUserID(userRepresentation.getId(), organizationName);
			userEntity.setGroupId(groupRepresentations.get(0).getId());
			userEntity.setGroupName(groupRepresentations.get(0).getName());
			
			UserAttributeEntity userAttributeEntity = new UserAttributeEntity(); 
			userAttributeEntity.setFirstName(userRepresentation.getFirstName());
			userAttributeEntity.setLastName(userRepresentation.getLastName());
			userAttributeEntity.setEmail(userRepresentation.getEmail());
			userEntity.setUserAttribute(userAttributeEntity);
			
			addUser(userEntity,organizationName);
		}
		
		PairMobileDeviceConfigEntity pairMobileDeviceConfigEntity = pairMobileDeviceConfigRepository
				.findPMDConfigByOrg(organizationName);

		String uuid = null;
		if (pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength() == SecretKeyLength.THIRTYTWO
				.getValue()) {
			uuid = UUID.randomUUID().toString();
		} else if (pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength() < SecretKeyLength.THIRTYTWO
		  		.getValue()) {
			uuid = Util.getAlphaNumericString(pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength());
		}
		UserIdentificationCodeEntity userIdentificationCodeEntity = new UserIdentificationCodeEntity();
		userIdentificationCodeEntity.setUuid(uuid);
		userIdentificationCodeEntity.setUser(userEntity);
		userIdentificationCodeEntity.setStatus(true);
		userIdentificationCodeEntity.setValidityFrom(Instant.now());
		userIdentificationCodeEntity.setValidityTill(new Date(
				System.currentTimeMillis() + (pairMobileDeviceConfigEntity.getQrCodeExpiryInMins()) * 60 * 1000)
						.toInstant());

		userIdentificationCodeRepository.save(userIdentificationCodeEntity);
		UserDevice userDevice = new UserDevice();
		userDevice.setUuid(uuid);
		return userDevice;
	
	}

	@Override
	public UserIssuanceStatus getIssuanceStatus(String organizationName, String userName) {
		UserIssuanceStatus userIssuanceStatus = new UserIssuanceStatus(false, userName, organizationName);
		UserEntity userEntity = userRepository.getUserByName(userName, organizationName);
		if(userEntity == null) {
			log.error(ApplicationConstants.notifyAdmin,
					String.format(ErrorMessages.USER_NOT_FOUND_UNDER_ORGANIZATION, userName, organizationName));
			return userIssuanceStatus;
		}else {

			if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name()) || 
					userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name()) || 
						userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_APPROVAL.name())) {
				return userIssuanceStatus;
		    } else {
				userIssuanceStatus = new UserIssuanceStatus(true, userName, organizationName);
				return userIssuanceStatus;
		    }
		}
	}

	@Override
	public ResponseEntity<User> registerLoggedInUser(String organizationName, User user) throws EnrollmentServiceException {
		UserEntity userEntity = userRepository.getUserByName(user.getName(), organizationName);
		OrganizationEntity orgEntity = organizationRepository.findByName(organizationName);
		if(userEntity == null) {
			userEntity = new UserEntity();
			userEntity.setStatus(UserStatusEnum.PENDING_ENROLLMENT.name());
			userEntity.setName(user.getFirstName()+" "+user.getLastName());
			userEntity.setReferenceName(user.getName());
			userEntity.setGroupId(user.getGroupId());
			userEntity.setGroupName(user.getGroupName());
			userEntity.setUserStatus(UserStatusEnum.ACTIVE.getUserStatus());
			userEntity.setIsFederated(user.getIsFederated());
			UserAttributeEntity userAttributeEntity = new UserAttributeEntity(); 
			userAttributeEntity.setFirstName(user.getFirstName());
			userAttributeEntity.setLastName(user.getLastName());
			userAttributeEntity.setEmail(user.getEmail());
			userEntity.setUserAttribute(userAttributeEntity);
			userEntity.setOrganization(orgEntity);
			userEntity = userRepository.save(userEntity);
		}
		
		if (userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name()) && userEntity.getIsFederated()) {
			System.out.println("Enrolll federated user::::::::::: for express issuance");

			user.setOrganisationName(organizationName);
			try {
				UserRepresentation userRepresentation = keycloakService.getUserByUsername(user.getName(),organizationName);
				if (userRepresentation == null) {
					log.info(ApplicationConstants.notifyAdmin,
							ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER, user.getName());
					throw new InconsistentDataException(Util
							.format(ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER, user.getName()));
				}
				UserAttribute userAttribute = new UserAttribute();
				userAttribute.setFirstName(user.getFirstName());
				userAttribute.setLastName(user.getLastName());
				userAttribute.setEmail(user.getEmail());

				user.setName(user.getName());
				user.setUserAttribute(userAttribute);

				user.setEmail(userRepresentation.getEmail());
				user.setFirstName(userRepresentation.getFirstName());
				user.setLastName(userRepresentation.getLastName());

				user.setGroupId(userEntity.getGroupId());
				String[] workflow_identityTypeName = workflowService.getIdentityTypeAndWrkflowByGroupID(organizationName, user.getGroupId());
				String dentityTypeName = (workflow_identityTypeName == null || workflow_identityTypeName.length == 0) ? "--" : workflow_identityTypeName[1];
				if (!dentityTypeName.equals("--")) {
					user = keycloakService.getLdapUserAttributes(dentityTypeName, user, userRepresentation);
					if (user != null) {
						registerUser(organizationName, user);
					}
				} else {
					// No workflow found for the user's group
				}
			} catch (Exception ex) {
				log.info(ApplicationConstants.notifyAdmin,
						ErrorMessages.FEDERATED_USER_ENROLLMENT_FAILED, user.getName());
				throw new InconsistentDataException(Util
						.format(ErrorMessages.FEDERATED_USER_ENROLLMENT_FAILED, user.getName()));
			}
		}
		
		User returnUser = new User();
		returnUser.setId(userEntity.getId());
		returnUser.setStatus(userEntity.getStatus());
		returnUser.setOrganisationId(orgEntity.getId());
		return new ResponseEntity<User>(returnUser, HttpStatus.CREATED);
	}
	
	@Override
	public void saveIDProofDocs(HttpHeaders httpHeaders, String organizationName, User user) throws EnrollmentServiceException {
		UserEntity userEntity = userRepository.getUserByName(user.getName(), organizationName);
		List<UserIDProofDocumentEntity> userIDProofDocumentsToRemove = new ArrayList<>();
		// delete the fields from db which are not selected in ui
		if(!userEntity.getUserIDProofDocuments().isEmpty()) {
			userEntity.getUserIDProofDocuments().stream().forEach(idProofDocEntity -> {
				List<UserIDProofDocument> userIdProofDocs = user.getUserIDProofDocuments().stream()
						.filter(idProofDoc -> idProofDoc.getIdProofTypeId().equals(idProofDocEntity.getOrganizationIdentity().getId()))
						.filter(idProofDoc -> idProofDoc.getIssuingAuthorityId().equals(idProofDocEntity.getIdProofIssuingAuthority().getId()))
						.collect(Collectors.toList());
				if(userIdProofDocs.size() == 0) {
					userIDProofDocumentsToRemove.add(idProofDocEntity);
				}
			});
		}
		// add if any thing added newly
		for (UserIDProofDocument userIDProofDoc : user.getUserIDProofDocuments()) {
			List<UserIDProofDocumentEntity> list = userEntity.getUserIDProofDocuments().stream()
					.filter(idProofDocEntity -> idProofDocEntity.getOrganizationIdentity().getId().equals(userIDProofDoc.getIdProofTypeId()))
					.filter(idProofDocEntity -> idProofDocEntity.getIdProofIssuingAuthority().getId().equals(userIDProofDoc.getIssuingAuthorityId()))
					.collect(Collectors.toList());
			UserIDProofDocumentEntity userIDProofDocumentEntity = new UserIDProofDocumentEntity();
			if (list.size() != 0) {
				userIDProofDocumentEntity = list.get(0);
			}
			userIDProofDocumentEntity.setName(userIDProofDoc.getName());
			userIDProofDocumentEntity.setFrontImg(userIDProofDoc.getFrontImg());
			userIDProofDocumentEntity.setBackImg(userIDProofDoc.getBackImg());
			userIDProofDocumentEntity.setFileData(userIDProofDoc.getFile());
			userIDProofDocumentEntity.setFrontFileType(userIDProofDoc.getFrontFileType());
			userIDProofDocumentEntity.setBackFileType(userIDProofDoc.getBackFileType());
			userIDProofDocumentEntity.setFrontFileName(userIDProofDoc.getFrontFileName());
			userIDProofDocumentEntity.setBackFileName(userIDProofDoc.getBackFileName());
			userIDProofDocumentEntity.setOrganizationIdentity(orgIdentitiesRepository.getOne(userIDProofDoc.getIdProofTypeId()));
			userIDProofDocumentEntity.setIdProofIssuingAuthority(idProofIssuingAuthorityRepository.findOne(userIDProofDoc.getIssuingAuthorityId()));
			userIDProofDocumentEntity.setUser(userEntity);
			userEntity.addUserIDProofDocuments(userIDProofDocumentEntity);
			userIDProofDocumentRepository.save(userIDProofDocumentEntity);
		}

//		WorkflowStepDetail workflowStepDetail = workflowService.getStepDetails(organizationName,
//				user.getWorkflowName(), "APPROVAL_BEFORE_ISSUANCE");
//		List<WFStepApprovalGroupConfig> configs = workflowStepDetail.getWfStepApprovalGroupConfigs();
//		for (WFStepApprovalGroupConfig approvalGroupConfig : configs) {
//			userEntity.setRefApprovalGroupId(approvalGroupConfig.getGroupId());
//			userEntity.setStatus(UserStatusEnum.PENDING_APPROVAL.name());
//		}
		
		EnrollmentStepsStatusEntity esse = userEntity.getEnrollmentStepsStatusEntity();
		if(esse == null) {
			esse = new EnrollmentStepsStatusEntity();
		}
		esse.setUser(userEntity);
		esse.setIdproofStatus(1);
		userEntity.setEnrollmentStepsStatusEntity(esse);
//		enrollmentStepStatusRepository.save(esse);
		
		for (UserIDProofDocumentEntity obj : userIDProofDocumentsToRemove) {
			 userEntity.removeUserIDProofDocuments(obj);
		}
		UserEnrollmentHistory	userEnrollmentHistory=user.getUserEnrollmentHistory();
		UserEnrollmentHistoryEntity userEnrollmentHistoryEntity = new UserEnrollmentHistoryEntity();
		userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.ID_PROOFING.getEnrollmentStep());
		userEnrollmentHistoryEntity.setEnrolledBy(userEnrollmentHistory.getEnrolledBy());
		userEnrollmentHistoryEntity.setEnrollmentStartDate(userEnrollmentHistory.getEnrollmentStartDate());
		userEnrollmentHistoryEntity.setEnrollmentEndDate(userEnrollmentHistory.getEnrollmentEndDate());
		userEnrollmentHistoryEntity.setCreatedBy(userEnrollmentHistory.getEnrolledBy());
		userEnrollmentHistoryEntity.setUser(userEntity);
		
		if(UserStatusEnum.PENDING_ENROLLMENT.name().equalsIgnoreCase(userEntity.getStatus())) {
				userEntity.setStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name());		
				userEnrollmentHistoryEntity.setStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name());
		} else if(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name().equalsIgnoreCase(userEntity.getStatus())) {
			userEnrollmentHistoryEntity.setStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name());
		} else {
			String status = completeEnrollmentInProgress(httpHeaders, userEntity, organizationName, user, false);
			userEnrollmentHistoryEntity.setStatus(status);
		}

		userEnrollmentHistoryRepository.save(userEnrollmentHistoryEntity);
		userRepository.save(userEntity);
	}
	@Override
	public void saveSkipUserBiometrics(HttpHeaders httpHeaders, String organizationName, User user) throws EnrollmentServiceException {
		UserEntity userEntity = userRepository.getUserByName(user.getName(), organizationName);
		
		// User Biometric Skip
		List<UserBiometricSkip> userBiometricSkips = user.getUserBiometricSkips();
		for (UserBiometricSkip userBiometricSkip : userBiometricSkips) {

			UserBiometricSkipEntity userBiometricSkipEntity = new UserBiometricSkipEntity();
			userBiometricSkipEntity.setType(userBiometricSkip.getType());
			userBiometricSkipEntity.setPosition(userBiometricSkip.getPosition());
			userBiometricSkipEntity.setSkipType(userBiometricSkip.getSkipType());
			userBiometricSkipEntity.setSkipReason(userBiometricSkip.getSkipReason());

			userBiometricSkipEntity.setUser(userEntity);
			userBiometricSkipRepository.save(userBiometricSkipEntity);
		}
		
		if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name())) {
			userEntity.setStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name());
		}else if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name())) {
			// no change
		}else {
			completeEnrollmentInProgress(httpHeaders, userEntity, organizationName, user, false);
		}
		userRepository.save(userEntity);
	} 
	@Override
	public void saveRegistration(HttpHeaders httpHeaders, String organizationName, User user) throws EnrollmentServiceException {
		try {
			UserEntity userEntity = userRepository.getUserByName(user.getName(), organizationName);
			
			// User Registration 
	
			boolean isApprovalRequired = false;
			boolean isBiometricValidtionRequired = false;
			// TODO handle Exceptions
			// int responseStatusCode = keycloakService.createUser(user);

			OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);

			// User
			UserAttributeEntity userAttributeEntity =  userEntity.getUserAttribute();
			
			userEntity.setIsEnrollmentUpdated(false);
			userEntity.setUserStatus(UserStatusEnum.ACTIVE.getUserStatus());

			Workflow workflow = workflowService.getWorkflowValidityByGroupID(organizationName, user.getGroupId());
			
			WorkflowStepDetail workflowStepRegDetail = workflowService.getStepDetails(organizationName,
 					user.getWorkflowName(), "REGISTRATION");
			if (!workflowStepRegDetail.getWfStepGenericConfigs().isEmpty()) {
				List<WFStepGenericConfig> configs = workflowStepRegDetail.getWfStepGenericConfigs();
				for (WFStepGenericConfig config : configs) {
					if (config.getName().equalsIgnoreCase("BIOMETRIC_VALIDATION_REQUIRED")) {
						if (config.getValue().equals("Y")) {
							isBiometricValidtionRequired = true;
						}
					}
				}
			}
			
			// Set USER Status
//			WorkflowStepDetail workflowStepDetail = workflowService.getStepDetails(organizationName,
//					user.getWorkflowName(), "APPROVAL_BEFORE_ISSUANCE");
//			if (workflowStepDetail.getWfStepApprovalGroupConfigs().size() == 0) {
//				// If no Approval Step Exists
//				userEntity.setStatus(UserStatusEnum.READY_FOR_ISSUANCE.name());				
//			} else {
//				List<WFStepApprovalGroupConfig> configs = workflowStepDetail.getWfStepApprovalGroupConfigs();
//				for (WFStepApprovalGroupConfig approvalGroupConfig : configs) {
//					userEntity.setRefApprovalGroupId(approvalGroupConfig.getGroupId());
//					userEntity.setStatus(UserStatusEnum.PENDING_APPROVAL.name());
//				}
//				isApprovalRequired = true;
//			}

			// User Attributes // User Appearance // User DL
			UserAttribute userAttribute = user.getUserAttribute();
			UserAppearance userAppearance = user.getUserAppearance();
			UserAddress userAddress = user.getUserAddress();
			List<UserIDProofDocument> userIDProofDocs = user.getUserIDProofDocuments();
			UserEnrollmentHistory userEnrollmentHistory = user.getUserEnrollmentHistory();

			userAttributeEntity.setEmail(userAttribute.getEmail());
			if(userAttribute.getDateOfBirth() != null) {
				userAttributeEntity.setDateOfBirth(userAttribute.getDateOfBirth().toInstant());
			}
			userAttributeEntity.setNationality(userAttribute.getNationality());
			userAttributeEntity.setPlaceOfBirth(userAttribute.getPlaceOfBirth());
			userAttributeEntity.setGender(userAttribute.getGender());
			userAttributeEntity.setCountryCode(userAttribute.getCountryCode());
			userAttributeEntity.setContact(userAttribute.getContact());
			userAttributeEntity.setAddress(userAttribute.getAddress());
			userAttributeEntity.setVeteran(userAttribute.isVeteran());
			userAttributeEntity.setOrganDonor(userAttribute.isOrganDonor());
			userAttributeEntity.setBloodType(userAttribute.getBloodType());
			userAttributeEntity.setMothersMaidenName(userAttribute.getMothersMaidenName());
			userAttributeEntity.setFirstName(userAttribute.getFirstName());
			userAttributeEntity.setMiddleName(userAttribute.getMiddleName());
			userAttributeEntity.setLastName(userAttribute.getLastName());
			userAttributeEntity.setUser(userEntity);
			if (userEntity.getUserAttribute() != null) {
				userAttributeEntity.setId(userEntity.getUserAttribute().getId());
			}
			userEntity.setName(userAttribute.getFirstName() + " " + userAttribute.getLastName());
			
			UserAppearanceEntity userAppearanceEntity = userEntity.getUserAppearance();
			if (userAppearance != null) {
				if(userAppearanceEntity == null) {
					userAppearanceEntity = new UserAppearanceEntity();
				}
				userAppearanceEntity.setEyeColor(userAppearance.getEyeColor());
				userAppearanceEntity.setHeight(userAppearance.getHeight());
				userAppearanceEntity.setWeight(userAppearance.getWeight());
				userAppearanceEntity.setHairColor(userAppearance.getHairColor());
				userAppearanceEntity.setUser(userEntity);
				userAppearanceRepository.save(userAppearanceEntity);
			}
			
			UserAddressEntity userAddressEntity = userEntity.getUserAddress();
			if(userAddress != null) {
				if(userAddressEntity == null) {
					userAddressEntity = new UserAddressEntity();
				}
				userAddressEntity.setAddressLine1(userAddress.getAddressLine1());
				userAddressEntity.setAddressLine2(userAddress.getAddressLine2());
				userAddressEntity.setCity(userAddress.getCity());
				userAddressEntity.setState(userAddress.getState());
				userAddressEntity.setCountry(userAddress.getCountry());
				userAddressEntity.setZipCode(userAddress.getZipCode());
				userAddressEntity.setUser(userEntity);
				
				userAddressRepository.save(userAddressEntity);
			}
			
			IdentityTypeEntity identityTypeEntity = identityTypeRepository.findIdentityTypeByName(workflow.getOrganizationIdentities().getIdentityTypeName());

			
			switch (workflow.getOrganizationIdentities().getIdentityTypeName()) {

			case Constants.Driving_License:

				UserDrivingLicense userDrivingLicense = user.getUserDrivingLicense();
				List<UserDrivingLicenseEntity> userDrivingLicenseEntities = userEntity.getUserDrivingLicenses();
				UserDrivingLicenseEntity userDrivingLicenseEntity = new UserDrivingLicenseEntity();
				if (userDrivingLicense != null) {
					if (userDrivingLicenseEntities == null || userDrivingLicenseEntities.isEmpty()) {
						userDrivingLicenseEntities = new ArrayList<UserDrivingLicenseEntity>();
					} else if(userDrivingLicenseEntities.size() > 0){
						userDrivingLicenseEntity = userDrivingLicenseEntities.get(0);
					}
					if (userDrivingLicense.getRestrictions() != null && userDrivingLicense.getVehicleClass() != null) {
						
						DrivingLicenseRestrictionEntity dlrEntity = drivingLicenseRestrictionsRepository
								.findOneByTypeAndOrganization(userDrivingLicense.getRestrictions(), organizationName);
						userDrivingLicenseEntity.setRestrictions(dlrEntity);

						VehicleClassificationEntity vehicleClassificationEntity = vehicleClassificationRepository
								.findOneByTypeAndOrganization(userDrivingLicense.getVehicleClass(), organizationName);
						if(vehicleClassificationEntity != null) {
							userDrivingLicenseEntity.setVehicleClassification(vehicleClassificationEntity);
							userDrivingLicenseEntity.setVehicleClass(vehicleClassificationEntity.getType());
						}
						
						userDrivingLicenseEntity.setEndorsements(userDrivingLicense.getEndorsements());
						userDrivingLicenseEntity.setUser(userEntity);

						Calendar c21 = Calendar.getInstance();
						Calendar c19 = Calendar.getInstance();
						Calendar c18 = Calendar.getInstance();
						if(userAttribute.getDateOfBirth() != null) {
							c21.setTime(userAttribute.getDateOfBirth());
							c21.add(Calendar.YEAR, 21);
							
							c19.setTime(userAttribute.getDateOfBirth());
							c19.add(Calendar.YEAR, 19);
							
							c18.setTime(userAttribute.getDateOfBirth());
							c18.add(Calendar.YEAR, 18);
						}
						userDrivingLicenseEntity.setUnder21Until(Instant.now());
						
						userDrivingLicenseEntity.setUnder19Until(Instant.now());
						
						userDrivingLicenseEntity.setUnder18Until(Instant.now());

						userDrivingLicenseEntities.add(userDrivingLicenseEntity);

					}
				}
				if (userEntity.getId() != null) {
					if (userDrivingLicenseEntities.size() > 0) {
						userDrivingLicenseRepository.save(userDrivingLicenseEntities);
					}
				}
				break;
			case Constants.PIV_and_PIV_1:
				UserPIVModel userPIV = user.getUserPIV();
				List<UserPivEntity> userPivEntities = userEntity.getUserPivCredentials();
				if (userPIV != null) {
					if (userPivEntities == null || userPivEntities.isEmpty()) {
						userPivEntities = new ArrayList<UserPivEntity>();
						userPivEntities.add(new UserPivEntity());
					}
					for (UserPivEntity userPIVEntity : userPivEntities) {
						userPIVEntity.setEmployeeAffiliation(userPIV.getEmployeeAffiliation());
						userPIVEntity.setEmployeeAffiliationColorCode(userPIV.getEmployeeAffiliationColorCode());
						userPIVEntity.setRank(userPIV.getRank());
						userPIVEntity.setAgency(organizationEntity.getDisplayName());
						userPIVEntity.setIssuerIdentificationNumber(organizationEntity.getIssuerIdentificationNumber());
						userPIVEntity.setAgencyAbbreviation(organizationEntity.getAbbreviation());
						userPIVEntity.setUser(userEntity);
					}					
				}
				if (!userPivEntities.isEmpty()) {
					userPivRepository.save(userPivEntities);
				}	
				
				break;
			case Constants.National_ID:
				UserNationalID userNationalID = user.getUserNationalID();
				UserNationalIdEntity userNationalIdEntity = userEntity.getUserNationalId();
				if(userNationalIdEntity == null) {
					userNationalIdEntity = new UserNationalIdEntity();
					userEntity.setUserNationalId(userNationalIdEntity);
				}
				userNationalIdEntity.setUser(userEntity);
				if (userNationalID != null) {
					userNationalIdEntity.setVaccineName(userNationalID.getVaccineName());
					userNationalIdEntity.setVaccinationLocationName(userNationalID.getVaccinationLocationName());
					userNationalIdEntity.setVaccinationDate(userNationalID.getVaccinationDate());
				}
				if (userEntity.getId() != null) {
					userNationalIDRepository.save(userNationalIdEntity);
				}
				break;
			case Constants.Voter_ID:
				UserVoterIDEntity userVoterIDEntity = userEntity.getUserVoterID();
				if(userVoterIDEntity == null) {
					userVoterIDEntity = new UserVoterIDEntity();
					userEntity.setUserVoterID(userVoterIDEntity);
				}
				userVoterIDEntity.setUser(userEntity);
				userVoterIDEntity.setElectorKey(Util.getAlphaNumericString(18).toUpperCase());
				userVoterIDEntity.setRegistrationYear(Calendar.getInstance().get(Calendar.YEAR)+"");
				
				if (userEntity.getId() != null) {
					userVoterIDRepository.save(userVoterIDEntity);
				}
				break;
			case Constants.Permanent_Resident_ID:								
				UserPermanentResidentID userPermanentResidentID = user.getUserPermanentResidentID();
				UserPermanentResidentIdEntity userPermanentResidentIdEntity  = userEntity.getUserPermanentResidentId();
				if(userPermanentResidentIdEntity == null) {
					userPermanentResidentIdEntity = new UserPermanentResidentIdEntity();
					userEntity.setUserPermanentResidentId(userPermanentResidentIdEntity);
				}
				userPermanentResidentIdEntity.setUser(userEntity);
				if (userPermanentResidentID != null) {
					userPermanentResidentIdEntity.setPassportNumber(userPermanentResidentID.getPassportNumber());
					userPermanentResidentIdEntity.setResidentcardTypeNumber(userPermanentResidentID.getResidentcardTypeNumber());
					userPermanentResidentIdEntity.setResidentcardTypeCode(userPermanentResidentID.getResidentcardTypeCode());
				}
				if (userEntity.getId() != null) {
					userPermanentResidentIdRepository.save(userPermanentResidentIdEntity);
				}
				break;
			case Constants.Health_ID:
				UserHealthID userHealthID = user.getUserHealthID();

				List<UserHealthIDEntity> userHealthIDEntities = userEntity.getUserHealthIDs();

				if (userHealthID != null) {
					if (userHealthIDEntities == null) {
						userHealthIDEntities = new ArrayList<UserHealthIDEntity>();
					}

					UserHealthIDEntity userHealthIDEntity = new UserHealthIDEntity();
					userHealthIDEntity.setGroupPlan(userHealthID.getGroupPlan());
					userHealthIDEntity.setPolicyNumber(userHealthID.getPolicyNumber());
					userHealthIDEntity.setHealthPlanNumber(userHealthID.getHealthPlanNumber());
					userHealthIDEntity.setPrimarySubscriber(userHealthID.getPrimarySubscriber());
					userHealthIDEntity.setPrimarySubscriberID(userHealthID.getPrimarySubscriberID());
					userHealthIDEntity.setPcp(userHealthID.getPcp());
					userHealthIDEntity.setPcpPhone(userHealthID.getPcpPhone());
					userHealthIDEntity.setIssuerIN(organizationEntity.getIssuerIdentificationNumber());
					userHealthIDEntity.setUser(userEntity);
					
					userHealthIDEntities.add(userHealthIDEntity);
				}
				if (userEntity.getId() != null) {
					if (userHealthIDEntities.size() > 0) {
						userHealthIDRepository.save(userHealthIDEntities);
					}
				}
				break;
			case Constants.Health_Service_ID:
				UserHealthService userHealthService = user.getUserHealthService();
				if (userHealthService != null) {
					UserHealthServiceEntity userHealthServiceEntity = new UserHealthServiceEntity();

					userHealthServiceEntity.setUser(userEntity);

					userHealthServiceEntity.setIdNumber(Util.get8DigitRandomNumber());
					userHealthServiceEntity.setIssuingAuthority("Issuing Authority ");
					userHealthServiceEntity.setPrimarySubscriber(userHealthService.getPrimarySubscriber());
					userHealthServiceEntity.setPrimarySubscriberID(userHealthService.getPrimarySubscriberID());
					userHealthServiceEntity.setPrimaryDoctor(userHealthService.getPrimaryDoctor());
					userHealthServiceEntity.setPrimaryDoctorPhone(userHealthService.getPrimaryDoctorPhone());
					userHealthServiceEntity
							.setUserHealthServiceVaccineInfos(new ArrayList<UserHealthServiceVaccineInfoEntity>());
					List<UserHealthServiceVaccineInfoEntity> UserHealthServiceVaccineInfoEntities = userHealthServiceEntity
							.getUserHealthServiceVaccineInfos();

					for (UserHealthServiceVaccineInfo userHealthServiceVaccineInfo : userHealthService
							.getUserHealthServiceVaccineInfos()) {

						UserHealthServiceVaccineInfoEntity userHealthServiceVaccineInfoEntity = new UserHealthServiceVaccineInfoEntity();
						userHealthServiceVaccineInfoEntity.setVaccine(userHealthServiceVaccineInfo.getVaccine());
						userHealthServiceVaccineInfoEntity.setRoute(userHealthServiceVaccineInfo.getRoute());
						userHealthServiceVaccineInfoEntity.setSite(userHealthServiceVaccineInfo.getSite());
						userHealthServiceVaccineInfoEntity.setDate(userHealthServiceVaccineInfo.getDate());
						userHealthServiceVaccineInfoEntity
								.setAdministeredBy(userHealthServiceVaccineInfo.getAdministeredBy());
						userHealthServiceVaccineInfoEntity.setUserhs(userHealthServiceEntity);

						UserHealthServiceVaccineInfoEntities.add(userHealthServiceVaccineInfoEntity);
					}
					userHealthServiceEntity.setUimsId("UIA" + Util.get8DigitRandomNumber());

					userHealthServiceRepository.save(userHealthServiceEntity);
				}
				break;
			case Constants.Student_ID:
				UserStudentID userStudentID = user.getUserStudentID();
				List<UserStudentIDEntity> userStudentIDEntities = userEntity.getUserStudentIDs();
				if (userStudentID != null) {
					if (userStudentIDEntities == null) {
						userStudentIDEntities = new ArrayList<UserStudentIDEntity>();
					}

					UserStudentIDEntity userStudentIDEntity = new UserStudentIDEntity();
					userStudentIDEntity.setDepartment(userStudentID.getDepartment());
					userStudentIDEntity.setInstitution(organizationEntity.getDescription());
					userStudentIDEntity.setIssuerIN(organizationEntity.getIssuerIdentificationNumber());
					userStudentIDEntity.setUser(userEntity);
					
					userStudentIDEntities.add(userStudentIDEntity);
				}
				if (userEntity.getId() != null) {
					if (userStudentIDEntities.size() > 0) {
						userStudentIDRepository.save(userStudentIDEntities);
					}
				}
				break;
			case Constants.Employee_ID:
				UserEmployeeID userEmployeeID = user.getUserEmployeeID();
				List<UserEmployeeIDEntity> userEmployeeIDEntities = userEntity.getUserEmployeeIDs();
			
					if (userEmployeeIDEntities == null || userEmployeeIDEntities.isEmpty()) {
						userEmployeeIDEntities = new ArrayList<UserEmployeeIDEntity>();
						userEmployeeIDEntities.add(new UserEmployeeIDEntity());
					}
					for (UserEmployeeIDEntity userEmployeeIDEntity : userEmployeeIDEntities) {
						userEmployeeIDEntity.setUser(userEntity);
						if (userEmployeeID != null) {
							userEmployeeIDEntity.setDepartment(userEmployeeID.getDepartment());
							userEmployeeIDEntity.setAffiliation(userEmployeeID.getAffiliation());
						}
					}						
				
				if (userEntity.getId() != null) {
					if (userEmployeeIDEntities.size() > 0) {
						userEmployeeIDRepository.save(userEmployeeIDEntities);
					}
				}
				break;
			default:
				UserPersonalizedID userPersonalizedID = user.getUserPersonalizedID();
					List<UserPersonalizedIDEntity> userPersonalizedIDEntities = userEntity.getUserPersonalizedIDs();
					
					if (userPersonalizedIDEntities == null ||userPersonalizedIDEntities.isEmpty()) {
						userPersonalizedIDEntities = new ArrayList<UserPersonalizedIDEntity>();
						userPersonalizedIDEntities.add(new UserPersonalizedIDEntity());
					}
					for (UserPersonalizedIDEntity userPersonalizedIDEntity : userPersonalizedIDEntities) {
						userPersonalizedIDEntity.setUser(userEntity);
						userPersonalizedIDEntity.setIdentityType(identityTypeEntity);
					}						
				
					if (userEntity.getId() != null) {
						if (userPersonalizedIDEntities.size() > 0) {
							userPersonalizedIDRepository.save(userPersonalizedIDEntities);
						}
					}
				break;
			}

			if (userEntity.getId() == null) {
				if (userAttributeEntity != null) {
					userEntity.setUserAttribute(userAttributeEntity);
				}
				if (userAppearanceEntity != null) {
					userEntity.setUserAppearance(userAppearanceEntity);
				}
				/*if(userEnrollmentHistoryEntity !=null) {
					userEntity.setUserEnrollmentsHistory(userEnrollmentHistoryEntity);
				}*/
			} else {
				if (userAttributeEntity != null) {
					userAttributeRepository.save(userAttributeEntity);
				}
				if (userAppearanceEntity != null) {
					userAppearanceRepository.save(userAppearanceEntity);
				}
			}

			EnrollmentStepsStatusEntity esse = userEntity.getEnrollmentStepsStatusEntity();
			if(esse == null) {
				esse = new EnrollmentStepsStatusEntity();
			}
			esse.setUser(userEntity);
			esse.setRegistrationFormStatus(1);
			userEntity.setEnrollmentStepsStatusEntity(esse);
//			enrollmentStepStatusRepository.save(esse);
			
			UserEnrollmentHistoryEntity userEnrollmentHistoryEntity = new UserEnrollmentHistoryEntity();
			userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.INFORMATION_CAPTURE.getEnrollmentStep());
			userEnrollmentHistoryEntity.setEnrolledBy(userEnrollmentHistory.getEnrolledBy());
			userEnrollmentHistoryEntity.setEnrollmentStartDate(userEnrollmentHistory.getEnrollmentStartDate());
			userEnrollmentHistoryEntity.setEnrollmentEndDate(userEnrollmentHistory.getEnrollmentEndDate());
			userEnrollmentHistoryEntity.setCreatedBy(user.getCreatedBy());
			userEnrollmentHistoryEntity.setUser(userEntity);


			if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name())) {
					userEntity.setStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name());		
					userEnrollmentHistoryEntity.setStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name());
			}else if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name())) {
				userEnrollmentHistoryEntity.setStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name());
			}else {
				String status = completeEnrollmentInProgress(httpHeaders, userEntity, organizationName, user, false);
				userEnrollmentHistoryEntity.setStatus(status);
			}

			userEnrollmentHistoryRepository.save(userEnrollmentHistoryEntity);
			
			userRepository.save(userEntity);
			
			JSONObject userJsonObject = new JSONObject();
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(user.getName(), organizationName);
			if(userRepresentation.getFederationLink() == null) {
				userJsonObject.put("firstName", user.getUserAttribute().getFirstName());
				userJsonObject.put("lastName", user.getUserAttribute().getLastName());
				userJsonObject.put("email", user.getUserAttribute().getEmail());
				keycloakService.updateUser(userRepresentation.getId(), userJsonObject, organizationName, user.getName());
			} else {
				System.out.println("user is from Ldap");
				// then this user is Ldap user so we can't edit the user attributes for this user, and also it depends on the use federation configuration
			}
		}catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.USER_NOT_ENROLLED, user.getName()),
					cause);
			throw new EnrollmentServiceException(cause, ErrorMessages.USER_NOT_ENROLLED, user.getName());
		}
		
	} 
	
	
	@Override
	public void saveUserBiometrics(HttpHeaders httpHeaders, String organizationName, User user, String biometricType) throws EnrollmentServiceException {
		UserEntity userEntity = userRepository.getUserByName(user.getName(), organizationName);
		
		// User Biometrics

		// Set Fingerprint values to UserBioInfo object. This object will send for bio		
		UserBioInfo userBioInfo = new UserBioInfo();
		
		List<UserBiometric> userBiometrics = user.getUserBiometrics();

		List<UserBiometricEntity> fpFromDb=userEntity.getUserBiometrics();
		
		List<UserBiometricEntity> fpData=fpFromDb.stream().filter(e->e.getType().equalsIgnoreCase("FINGERPRINT")).collect(Collectors.toList());
	
		if(userBiometrics.size() > 0 && "FINGERPRINT".equalsIgnoreCase(biometricType)) {

			List<UserBiometric> userBiometricsListToGetAnsi =  userBiometrics.stream().filter(e -> e.getType().equalsIgnoreCase("FINGERPRINT") && (e.getAnsi() == "" || e.getAnsi() == null)).collect(Collectors.toList());
			if(userBiometricsListToGetAnsi.size() > 0) {
				//List<UserBiometric> userBiometricsListToGetAnsi = userBiometricsFpList.stream().filter(e -> e.getAnsi() == "" || e.getAnsi() == null).collect(Collectors.toList());
				if(!userBiometricsListToGetAnsi.isEmpty()) {
					userBioInfo = getUserBioInfoObj(userBiometricsListToGetAnsi);
					// set user ID
					userBioInfo.setUserID(userEntity.getId());

					List<FingerprintTemplate> fpTemplate =biometricFeignClient.extractANSIData(organizationName, "Neuro", userBioInfo);

					userBiometrics.forEach(obj -> {
						List<FingerprintTemplate> fpTemplateList = fpTemplate.stream().filter(e -> obj.getPosition()!=null && e.getFingerPosition().contains(obj.getPosition())).collect(Collectors.toList());
						if(fpTemplateList.size() > 0) {
							obj.setAnsi(fpTemplateList.get(0).getAnsi());
						}
					});
				}				
			}
		}
		// delete the fields from db which are not selected in ui
		List<UserBiometricEntity> userBiometricEntityListToRemove = new ArrayList<>();
	
		List<UserBiometricEntity> selectedFPs=new ArrayList<>();
		
		UserEnrollmentHistory userEnrollmentHistory = user.getUserEnrollmentHistory();
		UserEnrollmentHistoryEntity userEnrollmentHistoryEntity= new UserEnrollmentHistoryEntity();

		userEnrollmentHistoryEntity.setEnrolledBy(userEnrollmentHistory.getEnrolledBy());
		userEnrollmentHistoryEntity.setEnrollmentStartDate(userEnrollmentHistory.getEnrollmentStartDate());
		userEnrollmentHistoryEntity.setEnrollmentEndDate(userEnrollmentHistory.getEnrollmentEndDate());
		userEnrollmentHistoryEntity.setCreatedBy(user.getCreatedBy());
		userEnrollmentHistoryEntity.setUser(userEntity);
		
		for (UserBiometric userBiometric : userBiometrics) {
			UserBiometricEntity userBiometricEntity = new UserBiometricEntity();
			UserBiometricAttributeEntity userBiometricAttributeEntity = new UserBiometricAttributeEntity();
			
			userBiometricEntity.setUser(userEntity);
			boolean found = false;
			if (userBiometric.getType().equalsIgnoreCase(biometricType) && "FACE".equalsIgnoreCase(biometricType)) {
			// case "FACE":
					for (UserBiometricEntity userBiometricEntityy : userEntity.getUserBiometrics()) {
						if(userBiometricEntityy.getType().equalsIgnoreCase(userBiometric.getType())) {
							userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.FACE_CAPTURE.getEnrollmentStep());
							
							userBiometricEntityy.setType(userBiometric.getType());
							userBiometricEntityy.setPosition(userBiometric.getPosition());
							userBiometricEntityy.setFormat(userBiometric.getFormat());
							userBiometricEntityy.setData(userBiometric.getData());
							userBiometricEntityy.setAnsi(userBiometric.getAnsi());
							userBiometricEntityy.setImageQuality(userBiometric.getImageQuality());
							
							List<UserBiometricAttributeEntity> userBiomeytricAttributes = userBiometricEntityy.getUserBiometricAttributes();
							if(userBiomeytricAttributes.size() > 0) {
								List<UserBiometricAttributeEntity> userBiometricAttributeEntities = userBiomeytricAttributes.stream().filter(e -> e.getName().equalsIgnoreCase(Constants.TRANSPARENT_PHOTO_ALLOWED)).collect(Collectors.toList());
								if(userBiometricAttributeEntities.size() > 0) {
									userBiometricAttributeEntity = userBiometricAttributeEntities.get(0);
								}
							} else {
								userBiomeytricAttributes = new ArrayList<>();
							}
							UserBiometricAttribute userBiometricAttribute = null;
							if(userBiometric.getUserBiometricAttributes() != null) {
								List<UserBiometricAttribute> userBioAttributes = userBiometric.getUserBiometricAttributes().stream().filter(e -> e.getName().equalsIgnoreCase(Constants.TRANSPARENT_PHOTO_ALLOWED)).collect(Collectors.toList());
								if(userBioAttributes.size() > 0) {
									userBiometricAttribute = userBioAttributes.get(0);
								}
							}
							UserBiometricEntity savedUserBiometricEntityy = userBiometricRepository.save(userBiometricEntityy);
							
							if(userBiometricAttribute != null) {
								userBiometricAttributeEntity.setUserBiometric(savedUserBiometricEntityy);
								userBiometricAttributeEntity.setName(Constants.TRANSPARENT_PHOTO_ALLOWED);
								userBiometricAttributeEntity.setValue(userBiometricAttribute.getValue());
								userBiometricEntityy.addUserBiometricAttribute(userBiometricAttributeEntity);
								userBiometricAttributeRepository.save(userBiometricAttributeEntity);
							}

							EnrollmentStepsStatusEntity esse = userEntity.getEnrollmentStepsStatusEntity();
							if(esse == null) {
								esse = new EnrollmentStepsStatusEntity();
							}
							esse.setUser(userEntity);
							esse.setFaceStatus(1);
							userEntity.setEnrollmentStepsStatusEntity(esse);
//							enrollmentStepStatusRepository.save(esse);
							found = true;
						}
					} if(!found) {
						userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.FACE_CAPTURE.getEnrollmentStep());
						userBiometricEntity.setUser(userEntity);
						userBiometricEntity.setType(userBiometric.getType());
						userBiometricEntity.setPosition(userBiometric.getPosition());
						userBiometricEntity.setFormat(userBiometric.getFormat());
						userBiometricEntity.setData(userBiometric.getData());
						userBiometricEntity.setAnsi(userBiometric.getAnsi());
						userBiometricEntity.setImageQuality(userBiometric.getImageQuality());
						
						List<UserBiometricAttributeEntity> userBiomeytricAttributes = new ArrayList<>();// userBiometricEntityy.getUserBiometricAttributes();
//						if(userBiomeytricAttributes.size() > 0) {
//							List<UserBiometricAttributeEntity> userBiometricAttributeEntities = userBiomeytricAttributes.stream().filter(e -> e.getName().equalsIgnoreCase(Constants.TRANSPARENT_PHOTO_ALLOWED)).collect(Collectors.toList());
//							if(userBiometricAttributeEntities.size() > 0) {
//								userBiometricAttributeEntity = userBiometricAttributeEntities.get(0);
//							}
//						}
						UserBiometricAttribute userBiometricAttribute = null;
						if(userBiometric.getUserBiometricAttributes() != null) {
							List<UserBiometricAttribute> userBioAttributes = userBiometric.getUserBiometricAttributes().stream().filter(e -> e.getName().equalsIgnoreCase(Constants.TRANSPARENT_PHOTO_ALLOWED)).collect(Collectors.toList());
							if(userBioAttributes.size() > 0) {
								userBiometricAttribute = userBioAttributes.get(0);
							}
						}
						
						if(userBiometricAttribute != null) {
							userBiometricAttributeEntity.setUserBiometric(userBiometricEntity);
							userBiometricAttributeEntity.setName(Constants.TRANSPARENT_PHOTO_ALLOWED);
							userBiometricAttributeEntity.setValue(userBiometricAttribute.getValue());
							
							userBiometricEntity.addUserBiometricAttribute(userBiometricAttributeEntity);
						}
						
						userBiometricRepository.save(userBiometricEntity);

						EnrollmentStepsStatusEntity esse = userEntity.getEnrollmentStepsStatusEntity();
						if(esse == null) {
							esse = new EnrollmentStepsStatusEntity();
						}
						esse.setUser(userEntity);
						esse.setFaceStatus(1);
						userEntity.setEnrollmentStepsStatusEntity(esse);
//						enrollmentStepStatusRepository.save(esse);
					}		
				// break;
			} else 
			if (userBiometric.getType().equalsIgnoreCase(biometricType) && "FINGERPRINT".equalsIgnoreCase(biometricType) ||
					userBiometric.getType().equalsIgnoreCase(biometricType) && "ROLL_FINGERPRINT".equalsIgnoreCase(biometricType)) {
				// case "FINGERPRINT":
				boolean isRollFinger = false;
				if(userBiometric.getType().equalsIgnoreCase(biometricType) && "ROLL_FINGERPRINT".equalsIgnoreCase(biometricType)) {
					isRollFinger = true;
				}
				    String ansiData = null;
					for (UserBiometricEntity userBiometricEntityy : userEntity.getUserBiometrics()) {
						if(userBiometricEntityy.getType().equalsIgnoreCase(userBiometric.getType()) && userBiometricEntityy.getPosition().equalsIgnoreCase(userBiometric.getPosition())) {
							if(!isRollFinger) {
								userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.FINGERPRINT_CAPTURE.getEnrollmentStep());
							} else {
								userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.FINGERPRINT_ROLL_CAPTURE.getEnrollmentStep());
							}
							userBiometricEntityy.setType(userBiometric.getType());
							userBiometricEntityy.setPosition(userBiometric.getPosition());
							userBiometricEntityy.setFormat(userBiometric.getFormat());
							userBiometricEntityy.setData(userBiometric.getData());
							userBiometricEntityy.setAnsi(userBiometric.getAnsi());							
							userBiometricEntityy.setImageQuality(userBiometric.getImageQuality());
							userBiometricEntityy.setWsqData(userBiometric.getWsqData());
							userBiometricRepository.save(userBiometricEntityy);

							EnrollmentStepsStatusEntity esse = userEntity.getEnrollmentStepsStatusEntity();
							if(esse == null) {
								esse = new EnrollmentStepsStatusEntity();
							}
							esse.setUser(userEntity);
							if(isRollFinger) {
								esse.setRollFingerprintStatus(1);
							} else {
								esse.setFingerprintStatus(1);
							}
							userEntity.setEnrollmentStepsStatusEntity(esse);
//							enrollmentStepStatusRepository.save(esse);
							found = true;
							selectedFPs.add(userBiometricEntityy);
						}
					} if(!found) {
						if(!isRollFinger) {
							userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.FINGERPRINT_CAPTURE.getEnrollmentStep());
						} else {
							userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.FINGERPRINT_ROLL_CAPTURE.getEnrollmentStep());
						}
						userBiometricEntity.setUser(userEntity);
						userBiometricEntity.setType(userBiometric.getType());
						userBiometricEntity.setPosition(userBiometric.getPosition());
						userBiometricEntity.setFormat(userBiometric.getFormat());
						userBiometricEntity.setData(userBiometric.getData());
						userBiometricEntity.setAnsi(userBiometric.getAnsi());
						userBiometricEntity.setImageQuality(userBiometric.getImageQuality());
						userBiometricEntity.setWsqData(userBiometric.getWsqData());
						userBiometricRepository.save(userBiometricEntity);

						EnrollmentStepsStatusEntity esse = userEntity.getEnrollmentStepsStatusEntity();
						if(esse == null) {
							esse = new EnrollmentStepsStatusEntity();
						}
						esse.setUser(userEntity);
						if(isRollFinger) {
							esse.setRollFingerprintStatus(1);
						} else {
							esse.setFingerprintStatus(1);
						}
						userEntity.setEnrollmentStepsStatusEntity(esse);
//						enrollmentStepStatusRepository.save(esse);
					}
			//	break;
			}else 
			if (userBiometric.getType().equalsIgnoreCase(biometricType) && "IRIS".equalsIgnoreCase(biometricType)) {
				// case "IRIS":
				for (UserBiometricEntity userBiometricEntityy : userEntity.getUserBiometrics()) {
					if(userBiometricEntityy.getType().equalsIgnoreCase(userBiometric.getType()) && userBiometricEntityy.getPosition().equalsIgnoreCase(userBiometric.getPosition())) {
						userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.IRIS_CAPTURE.getEnrollmentStep());
						userBiometricEntityy.setType(userBiometric.getType());
						userBiometricEntityy.setPosition(userBiometric.getPosition());
						userBiometricEntityy.setFormat(userBiometric.getFormat());
						userBiometricEntityy.setData(userBiometric.getData());
						userBiometricEntityy.setAnsi(userBiometric.getAnsi());
						userBiometricEntityy.setImageQuality(userBiometric.getImageQuality());
						userBiometricRepository.save(userBiometricEntityy);

						EnrollmentStepsStatusEntity esse = userEntity.getEnrollmentStepsStatusEntity();
						if(esse == null) {
							esse = new EnrollmentStepsStatusEntity();
						}
						esse.setUser(userEntity);
						esse.setIrisStatus(1);
						userEntity.setEnrollmentStepsStatusEntity(esse);
//						enrollmentStepStatusRepository.save(esse);
						found = true;
					}
				} if(!found) {
					userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.IRIS_CAPTURE.getEnrollmentStep());
					userBiometricEntity.setUser(userEntity);
					userBiometricEntity.setType(userBiometric.getType());
					userBiometricEntity.setPosition(userBiometric.getPosition());
					userBiometricEntity.setFormat(userBiometric.getFormat());
					userBiometricEntity.setData(userBiometric.getData());
					userBiometricEntity.setAnsi(userBiometric.getAnsi());
					userBiometricEntity.setImageQuality(userBiometric.getImageQuality());
					userBiometricRepository.save(userBiometricEntity);

					EnrollmentStepsStatusEntity esse = userEntity.getEnrollmentStepsStatusEntity();
					if(esse == null) {
						esse = new EnrollmentStepsStatusEntity();
					}
					esse.setUser(userEntity);
					esse.setIrisStatus(1);
					userEntity.setEnrollmentStepsStatusEntity(esse);
//					enrollmentStepStatusRepository.save(esse);
				}
			//	break;
			}else 
			if (userBiometric.getType().equalsIgnoreCase(biometricType) && "SIGNATURE".equalsIgnoreCase(biometricType)) {
				// case "SIGNATURE":
				for (UserBiometricEntity userBiometricEntityy : userEntity.getUserBiometrics()) {
					if(userBiometricEntityy.getType().equalsIgnoreCase(userBiometric.getType())) {
						userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.SIGNATURE_CAPTURE.getEnrollmentStep());
						userBiometricEntityy.setType(userBiometric.getType());
						userBiometricEntityy.setPosition(userBiometric.getPosition());
						userBiometricEntityy.setFormat(userBiometric.getFormat());
						userBiometricEntityy.setData(userBiometric.getData());
						userBiometricEntityy.setAnsi(userBiometric.getAnsi());
						userBiometricEntityy.setImageQuality(userBiometric.getImageQuality());
						userBiometricRepository.save(userBiometricEntityy);

						EnrollmentStepsStatusEntity esse = userEntity.getEnrollmentStepsStatusEntity();
						if(esse == null) {
							esse = new EnrollmentStepsStatusEntity();
						}
						esse.setUser(userEntity);
						esse.setSignatureStatus(1);
						userEntity.setEnrollmentStepsStatusEntity(esse);
//						enrollmentStepStatusRepository.save(esse);
						found = true;
					}
				} if(!found) {
					userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.SIGNATURE_CAPTURE.getEnrollmentStep());
					userBiometricEntity.setUser(userEntity);
					userBiometricEntity.setType(userBiometric.getType());
					userBiometricEntity.setPosition(userBiometric.getPosition());
					userBiometricEntity.setFormat(userBiometric.getFormat());
					userBiometricEntity.setData(userBiometric.getData());
					userBiometricEntity.setAnsi(userBiometric.getAnsi());
					userBiometricEntity.setImageQuality(userBiometric.getImageQuality());
					userBiometricRepository.save(userBiometricEntity);

					EnrollmentStepsStatusEntity esse = userEntity.getEnrollmentStepsStatusEntity();
					if(esse == null) {
						esse = new EnrollmentStepsStatusEntity();
					}
					esse.setUser(userEntity);
					esse.setSignatureStatus(1);
					userEntity.setEnrollmentStepsStatusEntity(esse);
//					enrollmentStepStatusRepository.save(esse);
				}	
			//	break;
			}
		}
		
		/** userEntity.getUserBiometrics().forEach(biometricEntity -> {
			List<UserBiometric> userBiometric = userBiometrics.stream().filter(biometric -> biometric.getType().equals(biometricEntity.getType())).collect(Collectors.toList());
			if(biometricEntity.getType().equals("FINGERPRINT")) {
				if(userBiometric.size() != 0 ) {
					List<UserBiometric> userFpBiometric = userBiometric.stream().filter(biometric -> biometricEntity.getPosition().equals(biometric.getPosition())).collect(Collectors.toList());
					if(userFpBiometric.size() == 0) {
						userBiometricEntityListToRemove.add(biometricEntity);
					}
				}
			}
		});*/
		
		if (biometricType.equalsIgnoreCase("FINGERPRINT")) {
			fpData.removeIf(element -> selectedFPs.contains(element));
			for (UserBiometricEntity e : fpData)
				userBiometricRepository.deleteById(e.getId());
		}
		for (UserBiometricEntity obj : userBiometricEntityListToRemove) {
			userBiometricRepository.delete(obj);
		}
		
		if (userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name())) {
			userEntity.setStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name());
			userEnrollmentHistoryEntity.setStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name());
		} else if (userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name())) {
			userEnrollmentHistoryEntity.setStatus(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name());
		} else {
			String status = completeEnrollmentInProgress(httpHeaders, userEntity, organizationName, user, false);
			userEnrollmentHistoryEntity.setStatus(status);
		}

		userEnrollmentHistoryRepository.save(userEnrollmentHistoryEntity);
		
		userRepository.save(userEntity);
	} 
	
	@Override
	public String completeEnrollmentInProgress(HttpHeaders httpHeaders, UserEntity userEntity, String organizationName, User user, boolean isDirectSave) throws EnrollmentServiceException {
		
		String loggedInUser = null;
		Long loggedInUserId = null;
		boolean isApprovalRequired;
		try {
			HeaderUtil.validateRequiredHeaders(httpHeaders, Arrays.asList(Constants.LOGGED_IN_USER, Constants.LOGGED_IN_USER_ID));
		} catch (UIMSException e) {
			e.printStackTrace();
		}
		if (httpHeaders.containsKey(Constants.LOGGED_IN_USER)) {
			loggedInUser = httpHeaders.get(Constants.LOGGED_IN_USER).get(0);
		}
		if (httpHeaders.containsKey(Constants.LOGGED_IN_USER_ID)) {
			loggedInUserId = Long.valueOf(httpHeaders.get(Constants.LOGGED_IN_USER_ID).get(0));
		}
		if (userEntity == null) {
			userEntity = userRepository.getUserByName(user.getName(), organizationName);
		}
			// Set USER Status
			WorkflowStepDetail workflowStepDetail = workflowService.getStepDetails(organizationName,
					user.getWorkflowName(), "APPROVAL_BEFORE_ISSUANCE");
			if (workflowStepDetail.getWfStepApprovalGroupConfigs().size() == 0) {
				// If no Approval Step Exists
				isApprovalRequired = false;

				if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.READY_FOR_ISSUANCE.name())) {
					// no change
				}else if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.READY_FOR_REISSUANCE.name())) {
					// no change
				}else if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name())) {
					userEntity.setStatus(UserStatusEnum.READY_FOR_ISSUANCE.name());	
				}else if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.IDENTITY_ISSUED.name())) {
					userEntity.setStatus(UserStatusEnum.READY_FOR_REISSUANCE.name());							
				}else {
					userEntity.setStatus(UserStatusEnum.PENDING_APPROVAL.name());
				}
			} else {
				List<WFStepApprovalGroupConfig> configs = workflowStepDetail.getWfStepApprovalGroupConfigs();
				for (WFStepApprovalGroupConfig approvalGroupConfig : configs) {
					userEntity.setRefApprovalGroupId(approvalGroupConfig.getGroupId());
					userEntity.setStatus(UserStatusEnum.PENDING_APPROVAL.name());
				}
				isApprovalRequired = true;

			}

			EnrollmentStepsStatusEntity esse = userEntity.getEnrollmentStepsStatusEntity();
			if(esse == null) {
				esse = new EnrollmentStepsStatusEntity();
			}
			esse.setUser(userEntity);
			esse.setSummaryStatus(1);
			userEntity.setEnrollmentStepsStatusEntity(esse);
//			enrollmentStepStatusRepository.save(esse);
			userRepository.save(userEntity);
			if(isDirectSave) {
				log.info("send Enrolled and Fetch Credentials notification");
				sendPairYourDeviceInviteNotification(organizationName, userEntity, loggedInUserId, loggedInUser);
				saveUserEnrollmentHistoryInSummary(user,userEntity,isApprovalRequired,organizationName);
			}
			
		return userEntity.getStatus();
	}
	
	@Override
	public EnrollmentStepsStatus enrollmentStepsStatus(String organizationName, String userName)
			throws EnrollmentServiceException {
		UserEntity userEntity = userRepository.getUserByName(userName, organizationName);
		// USER Status
		if (userEntity != null) {
			EnrollmentStepsStatusEntity enrollmentStepsStatusEntity = userEntity.getEnrollmentStepsStatusEntity();
			if (enrollmentStepsStatusEntity != null) {
				EnrollmentStepsStatus enrollmentStepsStatus = new EnrollmentStepsStatus(
						enrollmentStepsStatusEntity.getId(), enrollmentStepsStatusEntity.getUser().getId(),
						enrollmentStepsStatusEntity.getIdproofStatus(),
						enrollmentStepsStatusEntity.getRegistrationFormStatus(),
						enrollmentStepsStatusEntity.getFaceStatus(), enrollmentStepsStatusEntity.getIrisStatus(),
						enrollmentStepsStatusEntity.getRollFingerprintStatus(),
						enrollmentStepsStatusEntity.getFingerprintStatus(),
						enrollmentStepsStatusEntity.getSignatureStatus(),
						enrollmentStepsStatusEntity.getSummaryStatus(), enrollmentStepsStatusEntity.getComments(),
						enrollmentStepsStatusEntity.getVersion());
				return enrollmentStepsStatus;
			}
		}
		return null;
	}
	
	@Override
	public ResponseEntity<TokenDetails> generateMobileIDUserTokens(String organizationName, String devicePublicKey, String devicePublicKeyAlgorithm) {
			TokenDetails tokenDetails =  keycloakService.generateMobileIDUserTokens(organizationName, devicePublicKey, devicePublicKeyAlgorithm);
			return ResponseEntity.ok(tokenDetails);
	}
	
	@Override
	public void validatePNLogin(String organizationName, AuthResponse authResponse) {
			keycloakService.validatePNLogin(organizationName, authResponse);
	}
	
	@Override
	public ResponseEntity<UserDetails> getUserEnrollmentDetails(String organizationName, String userName) {
		UserDetails userDetails =  new UserDetails();
		Workflow workflow = getWorkflowGeneralDetailsByUserName(organizationName, userName);
		List<WorkflowStep> workflowfSteps = workflowService.getStepsByWorkflow(organizationName, workflow.getName());

		RegistrationConfigDetails registrationConfigDetails = organizationService.getRegistrationConfigDetailsByOrgName(organizationName, workflow.getOrganizationIdentities().getId());
		
		List<WorkflowStep> workflowSteps = new ArrayList<>();
		WorkflowStep wrkflowStep = null;
		for (WorkflowStep workflowStep : workflowfSteps) {
			wrkflowStep = new WorkflowStep();
			wrkflowStep.setName(workflowStep.getName());
			switch (workflowStep.getName()) {
			case Constants.ID_PROOFING:
				// get id proof types
				wrkflowStep.setIdProofTypes(idProofTypeService.getIDProofTypes(organizationName));
				break;
			case Constants.MOBILE_ID_ONBOARDING_CONFIG:
				// get id proof types
				wrkflowStep.setWfMobileIDStepTrustedIdentities(workflowService.getTrustedDocumentsForSelfServiceOnboard(organizationName, workflow.getName()));
				break;
			case Constants.REGISTRATION:
				// get reg step details
				WorkflowStepDetail wfRegStepDetails = workflowService.getStepDetails(organizationName,
						workflow.getName(), workflowStep.getName());
				wrkflowStep.setName(workflowStep.getName());
				wrkflowStep.setWfStepRegistrationConfigs(setRegistrationFields(
						wfRegStepDetails.getWfStepRegistrationConfigs(), registrationConfigDetails, organizationName));
				
				break;
			case Constants.FACE_CAPTURE:
				// get face step details
				WorkflowStepDetail wfFaceStepDetails = workflowService.getStepDetails(organizationName,
						workflow.getName(), workflowStep.getName());
				wrkflowStep.setWfStepGenericConfigs(wfFaceStepDetails.getWfStepGenericConfigs());
				break;
			case Constants.FINGERPRINT_CAPTURE:
				// get fp step details
				WorkflowStepDetail wfFpStepDetail = workflowService.getStepDetails(organizationName, workflow.getName(),
						workflowStep.getName());
				wrkflowStep.setWfStepGenericConfigs(wfFpStepDetail.getWfStepGenericConfigs());
				break;
			case Constants.APPROVAL_BEFORE_ISSUANCE:
				WorkflowStepDetail wfApprovalStepDetail = workflowService.getStepDetails(organizationName, workflow.getName(),
						workflowStep.getName());
				wrkflowStep.setWfStepApprovalGroupConfigs(wfApprovalStepDetail.getWfStepApprovalGroupConfigs());
				break;
			}
			workflowSteps.add(wrkflowStep);
		}
		// get step configs and add type and value to it
		workflow.setWorkflowSteps(workflowSteps);
		
		userDetails.setWorkflow(workflow);
		try {
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(userName, organizationName);
			if (userRepresentation == null) {
				log.info(ApplicationConstants.notifySearch,
						ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER,
						userName);
				throw new InconsistentDataException(
						Util.format(ErrorMessages.APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER,
								userName));
			}
			userDetails.setName(userRepresentation.getUsername());
			userDetails.setEmail(userRepresentation.getEmail());
			userDetails.setFirstName(userRepresentation.getFirstName());
			userDetails.setLastName(userRepresentation.getLastName());
			UserEntity userEntity = userRepository.getUserByName(userName, organizationName);
			if (userEntity != null) {
				userDetails.setId(userEntity.getId());
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, userName),
					cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, userDetails.getName());
		}
		
		return new ResponseEntity<UserDetails>(userDetails, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<UserDetails> getUserStatus(String organizationName, String userName) {
		UserDetails userDetails = new UserDetails();
		try {

			UserEntity userEntity = userRepository.getUserByName(userName, organizationName);
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(userName, organizationName);
			if(userRepresentation != null) {
				if(userRepresentation.getFederationLink() != null) {
					userDetails.setIsFederated(true);
				} else {
					userDetails.setIsFederated(false);
				}
				userDetails.setEnabled(userRepresentation.isEnabled());
			}
			if (userEntity == null || userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name())) {
				userDetails.setCanEnroll(true);
				userDetails.setUserStatus(UserStatusEnum.PENDING_ENROLLMENT.getUserStatus());
				userDetails.setStatus(UserStatusEnum.PENDING_ENROLLMENT.name());
			} else {

				String[] workflow_identityTypeName = workflowService.getIdentityTypeAndWrkflowByGroupID(organizationName, userEntity.getGroupId());
				userDetails.setOnboardSelfService((workflow_identityTypeName == null || workflow_identityTypeName.length == 0)?"--":workflow_identityTypeName[2]);

				userDetails.setStatus(userEntity.getStatus());
				userDetails.setId(userEntity.getId());
				userDetails.setName(userEntity.getReferenceName());
				userDetails.setFirstName(userEntity.getUserAttribute().getFirstName());
				userDetails.setLastName(userEntity.getUserAttribute().getLastName());
				userDetails.setEnrollmentStatus(userEntity.getStatus());
				for (UserBiometricEntity userBiometricEntity : userEntity.getUserBiometrics()) {
					if (userBiometricEntity.getType().equalsIgnoreCase("FACE")) {
						userDetails.getUserBiometrics().add(new UserBiometric("FACE", null, userBiometricEntity.getType(), null , userBiometricEntity.getData(), null, null ,0,new ArrayList<>()));
					}						
				}
			
				userDetails.setUserStatus(UserStatusEnum.valueOf(userEntity.getStatus()).getUserStatus());
				userDetails.setCanDisplayEnrollDetails(true);
				if (userEntity == null || userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_APPROVAL.name())) {
					userDetails.setCanApprove(true);
					userDetails.setUserStatus(UserStatusEnum.PENDING_APPROVAL.getUserStatus());
					if ((apduFeignClient.getCountOfIssuedUserDevices(userEntity.getId(), organizationName) > 0l)) {
						userDetails.setCanLifeCycle(true);
					}
				}else {
					userDetails.setCanIssue(true);
					// TODO Make a call to APDU Service to see if user has already issued devices,
					// then enable canLifeCycle and if its in READY_FOR_ISSUANCE, it means its first timer so disable 
					if (userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.IDENTITY_ISSUED.name()) || userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.READY_FOR_REISSUANCE.name()) || (apduFeignClient.getCountOfIssuedUserDevices(userEntity.getId(), organizationName) > 0l)) {
						userDetails.setCanLifeCycle(true);
					}
				}
			}
			
			
			
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, userName), cause);
			throw new EntityNotFoundException(cause, ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, userDetails.getName());
		}

		return new ResponseEntity<UserDetails>(userDetails, HttpStatus.OK);
	}
	
	@Override
	public List<WFStepRegistrationConfig> setRegistrationFields(List<WFStepRegistrationConfig> wfStepRegistrationConfigs, RegistrationConfigDetails registrationConfigDetails, String organizationName) {
		wfStepRegistrationConfigs.forEach(regConfig -> {
			if(regConfig.getFieldName().equalsIgnoreCase(Constants.Date_Of_Birth)) {
				regConfig.setType(Constants.date);
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Vehicle_Classification)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(vehicleRegistrationService.getVehicleClassifications(organizationName));
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Department)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(registrationConfigDetails.getDepartment());
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Veteran)) {
				regConfig.setType(Constants.checkbox);
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Group_Plan)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(registrationConfigDetails.getHealthGroupPlan());
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Restrictions)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(drivingLicenseService.getDrivingLicenseRestrictions(organizationName));
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Endorsements)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(registrationConfigDetails.getDrivingLicenseEndorsements());
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Height)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(registrationConfigDetails.getPhysicalccHeight());
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Hair_Color)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(registrationConfigDetails.getPhysicalccHair());
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Eye_Color)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(registrationConfigDetails.getPhysicalccEye());
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Gender)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(registrationConfigDetails.getGender());
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Blood_Type)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(registrationConfigDetails.getBloodType());
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Rank_Grade_Employee_Status)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(registrationConfigDetails.getRanks());
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Employee_Affiliation)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(registrationConfigDetails.getEmployeeAffiliation());
			} else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Employee_Affiliation_Color_Code)) {
				regConfig.setType(Constants.dropdown);
				regConfig.setDropDownList(registrationConfigDetails.getEmployeeColorCode());
			}  else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Donor)) {
				regConfig.setType(Constants.checkbox);
			}  else if(regConfig.getFieldName().equalsIgnoreCase(Constants.Vaccination_Date)) {
				regConfig.setType(Constants.date);
			} else {
				regConfig.setType(Constants.input);
			}
		});
		return wfStepRegistrationConfigs;
	}
	
	@Override
	public User getUserDetailsByUserId(String organizationName, Long userId) {
		UserEntity userEntity = userRepository.getOne(userId);
		User user = new User();
		user.setName(userEntity.getReferenceName());
		user.setWorkflow(getWorkflowByUser(userId));
		user.setUserAttribute(getUserAttributes(userEntity.getUserAttribute()));
		user.setUserAddress(getUserAddress(userEntity.getUserAddress()));
		user.setUserBiometrics(getUserBiometrics(userEntity.getUserBiometrics()));
		return user;
	}
	
	@Override
	public UserEntity getUserEntityByUserName(String organizationName,  String userName) {
		return userRepository.getUserByName(userName, organizationName);
		
	}
	
	@Override
	public UserAttribute getUserAttributesByUserId(String organizationName, Long userId) {
		UserAttributeEntity userAttributeEntity = userRepository.getOne(userId).getUserAttribute();
		UserAttribute userAttribute = new UserAttribute();		
		userAttribute.setFirstName(userAttributeEntity.getFirstName());
		userAttribute.setLastName(userAttributeEntity.getLastName());
		return userAttribute;
	}

	@Override
	public List<Long> getOrgUsersFromUserIds(String organizationName, List<Long> userIdList) {
		return userRepository.findOrgUserIds(userIdList, organizationName);
	}
	
	@Override
	public User getTransparentFacialImage(HttpHeaders httpHeaders, String organizationName, User user) {
		if (user != null) {
			int width = 0;
			int height = 0;
			try {
				HeaderUtil.validateRequiredHeaders(httpHeaders,Arrays.asList(Constants.cropWidth, Constants.cropHeight));

				if (httpHeaders.containsKey(Constants.cropWidth)) {
					width = Integer.valueOf(httpHeaders.get(Constants.cropWidth).get(0));
				}
				if (httpHeaders.containsKey(Constants.cropHeight)) {
					height = Integer.valueOf(httpHeaders.get(Constants.cropHeight).get(0));
				}

				List<UserBiometric> userBiometrics = user.getUserBiometrics().stream()
						.filter(e -> e.getType().equalsIgnoreCase("FACE")).collect(Collectors.toList());
				if (userBiometrics.size() > 0) {
					UserBiometric userBio = userBiometrics.get(0);

					java.awt.Image image;
					try {
						image = com.meetidentity.usermanagement.util.Util
								.convertBase64ToImage(Base64.getEncoder().encodeToString(userBio.getData()));

						BufferedImage bufferedImage = Util.resizeImage(image, width, height);
						bufferedImage = backgroundColorTransparency.generate(bufferedImage, organizationName);
						String base64Data = Util.convertBufferedImageToBase64(bufferedImage);
						userBio.setBase64OfTransparentImageData(base64Data);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			} catch (UIMSException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}
		return user;
	}

	@Override
	public ResponseEntity<ExpressEnrollmentResponse> expressEnroll(String organizationName,
			ExpressEnrollForm expressEnrollForm) throws EnrollmentServiceException {
		
		int statusCode = 201;
		boolean isApprovalRequired = false;
		ResponseEntity<User> responseEntity = null;
		try {

			/*********************************************************************************************************/
			OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);
			/*********************************************************************************************************/

			// User
			UserEntity userEntity = new UserEntity();
			userEntity.setReferenceName(expressEnrollForm.getUserName());		
			userEntity.setOrganization(organizationEntity);
			userEntity.setGroupId(expressEnrollForm.getGroupID());
			userEntity.setGroupName(expressEnrollForm.getGroupName());		
			userEntity.setIsEnrollmentUpdated(false);
			// Set Status
			userEntity.setStatus("READY_FOR_ISSUANCE");
			userEntity.setUserStatus("ACTIVE");
			
			// Set USER Status
						String  workflowName = workflowService.getWorkflowNameByGroupID(organizationName, expressEnrollForm.getGroupID());
						WorkflowStepDetail workflowStepDetail = workflowService.getStepDetails(organizationName,
								workflowName, "APPROVAL_BEFORE_ISSUANCE");
						if (workflowStepDetail.getWfStepApprovalGroupConfigs().size() == 0) {
							// If no Approval Step Exists
							userEntity.setStatus(UserStatusEnum.READY_FOR_ISSUANCE.name());				
						} else {
							List<WFStepApprovalGroupConfig> configs = workflowStepDetail.getWfStepApprovalGroupConfigs();
							for (WFStepApprovalGroupConfig approvalGroupConfig : configs) {
								userEntity.setRefApprovalGroupId(approvalGroupConfig.getGroupId());
								userEntity.setStatus(UserStatusEnum.PENDING_APPROVAL.name());
							}
							isApprovalRequired = true;
						}
			
			userEntity = userRepository.save(userEntity);	
			
			
			//set address, attributes , appearance, biometrics
				
					
					// add user address
					UserAddress userAddress = expressEnrollForm.getUserAddress();
					UserAddressEntity userAddressEntity = new UserAddressEntity();
					
					if (userAddress != null) {
						userAddressEntity.setAddressLine1(userAddress.getAddressLine1());
						userAddressEntity.setAddressLine2(userAddress.getAddressLine2());
						userAddressEntity.setCity(userAddress.getCity());
						userAddressEntity.setState(userAddress.getState());
						userAddressEntity.setCountry(userAddress.getCountry());
						userAddressEntity.setZipCode(userAddress.getZipCode());
						userAddressEntity.setUser(userEntity);
						userEntity.setUserAddress(userAddressEntity);
						
						userAddressRepository.save(userAddressEntity);
					}
					

					// add user attributes
					UserAttribute userAttribute = expressEnrollForm.getUserAttribute();
					UserAttributeEntity userAttributeEntity = new UserAttributeEntity();
					
					if (userAttribute != null) {
						userAttributeEntity.setDateOfBirth(userAttribute.getDateOfBirth().toInstant());
						userAttributeEntity.setGender(userAttribute.getGender());
						userAttributeEntity.setEmail(userAttribute.getEmail());
						userAttributeEntity.setLastName(userAttribute.getLastName());
						userAttributeEntity.setFirstName(userAttribute.getFirstName());
						userAttributeEntity.setMiddleName(userAttribute.getMiddleName());
						userAttributeEntity.setMothersMaidenName(userAttribute.getMothersMaidenName());
						userAttributeEntity.setNationality(userAttribute.getNationality());
						userAttributeEntity.setPlaceOfBirth(userAttribute.getPlaceOfBirth());
						userAttributeEntity.setBloodType(userAttribute.getBloodType());
						userAttributeEntity.setCountryCode(userAttribute.getCountryCode());
						userAttributeEntity.setContact(userAttribute.getContact());
						userAttributeEntity.setUser(userEntity);

						userEntity.setName(userAttributeEntity.getFirstName() + " " + userAttributeEntity.getLastName());
						userEntity.setUserAttribute(userAttributeEntity);
						userAttributeRepository.save(userAttributeEntity);
					}


					// add user appearance
					UserAppearance userAppearance = expressEnrollForm.getUserAppearance();
					UserAppearanceEntity userAppearanceEntity = new UserAppearanceEntity();
					
					if (userAppearance != null) {
						userAppearanceEntity.setHeight(userAppearance.getHeight());
						userAppearanceEntity.setEyeColor(userAppearance.getEyeColor());
						userAppearanceEntity.setHairColor(userAppearance.getHairColor());
						userAppearanceEntity.setWeight(userAppearance.getWeight());
						userAppearanceEntity.setUser(userEntity);
						
						userEntity.setUserAppearance(userAppearanceEntity);
						userAppearanceRepository.save(userAppearanceEntity);
					}
						

		            UserEnrollmentHistoryEntity userEnrollmentHistoryEntity = new UserEnrollmentHistoryEntity();

						if(!isApprovalRequired) {
							userEnrollmentHistoryEntity.setApprovedBy(expressEnrollForm.getOperatorName());
							userEnrollmentHistoryEntity.setApprovalStatus("APPROVED");
							userEnrollmentHistoryEntity.setReason("Auto Approved");
							userEnrollmentHistoryEntity.setApprovedDate(Timestamp.from(Instant.now()));
							userEnrollmentHistoryEntity.setStatus("ACTIVE");
						}				
						userEnrollmentHistoryEntity.setEnrolledBy(expressEnrollForm.getOperatorName());
						userEnrollmentHistoryEntity.setEnrollmentStartDate(Timestamp.from(Instant.now()));
						userEnrollmentHistoryEntity.setEnrollmentEndDate(Timestamp.from(Instant.now()));				
						userEnrollmentHistoryEntity.setCreatedBy(expressEnrollForm.getOperatorName());
						userEnrollmentHistoryEntity.setCreatedDate(Instant.now());
						userEnrollmentHistoryEntity.setUser(userEntity);
						userEntity.addUserEnrollmentHistory(userEnrollmentHistoryEntity);	
					
						Workflow workflow = workflowService.getWorkflowValidityByGroupID(organizationName, expressEnrollForm.getGroupID());
						
						switch (workflow.getOrganizationIdentities().getIdentityTypeName()) {

							case Constants.Driving_License:
								
								break;
							case Constants.PIV_and_PIV_1:
								
								break;
							case Constants.National_ID:
								
								break;
							case Constants.Voter_ID:
								
								break;
							case Constants.Permanent_Resident_ID:
								
								break;
							case Constants.Health_ID:
	
								break;
							case Constants.Health_Service_ID:
	
								break;
							case Constants.Student_ID:
	
								break;
							case Constants.Employee_ID:
								List<UserEmployeeIDEntity> userEmployeeIDEntities = new ArrayList<UserEmployeeIDEntity>();
	
								UserEmployeeIDEntity userEmployeeIDEntity = new UserEmployeeIDEntity();
								userEmployeeIDEntity.setUser(userEntity);
	
								userEmployeeIDEntities.add(userEmployeeIDEntity);
								userEntity.setUserEmployeeIDs(userEmployeeIDEntities);
	
								if (userEntity.getId() != null) {
									if (userEmployeeIDEntities.size() > 0) {
										userEmployeeIDRepository.save(userEmployeeIDEntities);
									}
								}
								break;
							default:
								break;
						}
						
						// User Biometrics

						// Set Fingerprint values to UserBioInfo object. This object will send for bio
						// enrollment in Nserver
						// UserBioInfo userBioInfo = new UserBioInfo();
						
						List<UserBiometric> userBiometrics = expressEnrollForm.getUserBiometrics();

						if(userBiometrics.size() > 0) {
//							userBioInfo = getUserBioInfoObj(userBiometrics);
//							// set user ID
//							userBioInfo.setUserID(userEntity.getId());
												ObjectMapper mapper = new ObjectMapper();
						for (UserBiometric userBiometric : userBiometrics) {
							String[] formats = null;
							UserBiometricEntity userBiometricEntity = new UserBiometricEntity();
							System.out.println(mapper.writeValueAsString(userBiometric));
							switch (userBiometric.getType()) {
							case "FACE":
									
									userBiometricEntity.setUser(userEntity);
									userBiometricEntity.setType(userBiometric.getType());
									formats = userBiometric.getFormat().split(":");
									if(formats[1].equalsIgnoreCase("jpeg") || formats[1].equalsIgnoreCase("jpg")) {
										userBiometricEntity
										.setData(convertJPEGtoPNGFormat(Base64.getDecoder().decode(userBiometric.getBioData())));										
									}if(formats[1].equalsIgnoreCase("png")) {
										userBiometricEntity
										.setData((Base64.getDecoder().decode(userBiometric.getBioData())));										
									}
									userBiometricEntity.setFormat(formats[0].toUpperCase());
									userBiometricEntity.setCreatedBy(expressEnrollForm.getOperatorName());
									userBiometricRepository.save(userBiometricEntity);
									
									userEntity.addUserBiometric(userBiometricEntity);							
								
								break;

							case "FINGERPRINT":
									userBiometricEntity.setUser(userEntity);
									userBiometricEntity.setType(userBiometric.getType());
									formats = userBiometric.getFormat().split(":");
									if(formats[1].equalsIgnoreCase("jpeg") || formats[1].equalsIgnoreCase("jpg")) {
										userBiometricEntity
										.setData((Base64.getDecoder().decode(userBiometric.getBioData())));										
									}if(formats[1].equalsIgnoreCase("png")) {
										userBiometricEntity
										.setData(convertPNGtoJPEGFormat(Base64.getDecoder().decode(userBiometric.getBioData())));										
									}
									userBiometricEntity.setFormat("jpg");
									userBiometricEntity.setImageQuality(userBiometric.getImageQuality());
									userBiometricEntity.setWsqData(userBiometric.getWsqData());
									userBiometricEntity.setPosition(userBiometric.getPosition());
									userBiometricEntity.setAnsi(userBiometric.getAnsi());
									userBiometricEntity.setCreatedBy(expressEnrollForm.getOperatorName());
									userBiometricRepository.save(userBiometricEntity);
									
									userEntity.addUserBiometric(userBiometricEntity);	
									
									break;
							case "IRIS":
									userBiometricEntity.setUser(userEntity);
									userBiometricEntity.setType(userBiometric.getType());
									formats = userBiometric.getFormat().split(":");
									if(formats[1].equalsIgnoreCase("jpeg") || formats[1].equalsIgnoreCase("jpg")) {
										userBiometricEntity
										.setData(convertJPEGtoPNGFormat(Base64.getDecoder().decode(userBiometric.getBioData())));										
									}if(formats[1].equalsIgnoreCase("png")) {
										userBiometricEntity
										.setData((Base64.getDecoder().decode(userBiometric.getBioData())));										
									}
									userBiometricEntity.setFormat("png");
									userBiometricEntity.setImageQuality(userBiometric.getImageQuality());
									userBiometricEntity.setPosition(userBiometric.getPosition());
									userBiometricEntity.setAnsi(userBiometric.getAnsi());
									userBiometricEntity.setCreatedBy(expressEnrollForm.getOperatorName());
									userBiometricRepository.save(userBiometricEntity);
									
									userEntity.addUserBiometric(userBiometricEntity);	
									
									break;
							case "SIGNATURE":
									userBiometricEntity.setUser(userEntity);
									userBiometricEntity.setType(userBiometric.getType());
									formats = userBiometric.getFormat().split(":");
									if(formats[1].equalsIgnoreCase("jpeg") || formats[1].equalsIgnoreCase("jpg")) {
										userBiometricEntity
										.setData(convertJPEGtoPNGFormat(Base64.getDecoder().decode(userBiometric.getBioData())));										
									}if(formats[1].equalsIgnoreCase("png")) {
										userBiometricEntity
										.setData((Base64.getDecoder().decode(userBiometric.getBioData())));										
									}
									userBiometricEntity.setFormat("png");
									userBiometricEntity.setImageQuality(userBiometric.getImageQuality());
									userBiometricRepository.save(userBiometricEntity);
									
									userEntity.addUserBiometric(userBiometricEntity);	
								
								break;
							}
						}
					}
				userEntity = userRepository.save(userEntity);	
				/*********************************************************************************************************/
				IdentityProviderUser identityProviderUser = new IdentityProviderUser();
				identityProviderUser.setUserName(expressEnrollForm.getUserName());
				identityProviderUser.setFirstName(userEntity.getUserAttribute().getFirstName());
				identityProviderUser.setLastName(userEntity.getUserAttribute().getLastName());
				identityProviderUser.setEmail(userEntity.getUserAttribute().getEmail());
				if (userEntity.getUserAttribute().getContact() == null) {
					//
				} else {
					identityProviderUser.setCountryCode(userEntity.getUserAttribute().getCountryCode());
					identityProviderUser.setContactNumber(userEntity.getUserAttribute().getContact());
				}
				identityProviderUser.setEnabled(expressEnrollForm.isPortalAccessEnabled());
				identityProviderUser.setCredentialType("password");
				identityProviderUser.setCredentialValue("");
				identityProviderUser.setCredentialTemporary(false);
				identityProviderUser.setGroupId(expressEnrollForm.getGroupID());
				identityProviderUser.setAuthServiceRoleID(expressEnrollForm.getRoleID().split(","));
				identityProviderUser.setOrgId(organizationEntity.getId());
				identityProviderUser.setSendInvite(false);
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				headers.add(Constants.LOGGED_IN_USER, expressEnrollForm.getOperatorName());
				headers.add(Constants.UIMS_URL, organizationName);
				responseEntity = identityProviderUserService.createUserAndAssignGroupAndRoles(headers, organizationName, identityProviderUser,
						userEntity);
				sendPairYourDeviceInviteNotification(organizationName, userEntity, userEntity.getId(), expressEnrollForm.getOperatorName());
				
				ExpressEnrollmentResponse expressEnrollmentResponse = new ExpressEnrollmentResponse(responseEntity.getBody().getId(), userEntity.getUserEmployeeIDs().get(0).getEmployeeIDNumber(), responseEntity.getBody().getName(), userEntity.getStatus(), userEntity.getUserStatus());
			
				return new ResponseEntity<ExpressEnrollmentResponse>(expressEnrollmentResponse, HttpStatus.OK);
		} catch (Exception cause) {

			if(responseEntity!=null) {
				keycloakService.deleteUserByID(responseEntity.getBody().getRepresentationId(), organizationName);
			}
		
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.USER_NOT_ENROLLED, expressEnrollForm.getUserName()), cause);
			throw new EnrollmentServiceException(cause, ErrorMessages.USER_NOT_ENROLLED,
					expressEnrollForm.getUserName());
		}
	}
	
	private void sendPairYourDeviceInviteNotification(String organizationName, UserEntity userEntity, Long loggedInUserId, String loggedInUser) {
		if(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.READY_FOR_ISSUANCE.name()) || userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.READY_FOR_REISSUANCE.name())) {
			int responseStatusCode = notificationServiceHelper.sendPairYourDeviceInviteNotification(organizationName, loggedInUser, loggedInUserId, userEntity);
			if (responseStatusCode != HttpStatus.OK.value()) {
				log.error(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.FAILED_TO_SEND_INVITE_FOR_ENROLLMENT, userEntity.getName()));
			}
		}
	}

	@Override
	public ResponseEntity<HttpStatus> searchUser_SendInvite(String organizationName, String email, String groupName,  String roleId) throws EnrollmentServiceException {
		if(email == null) {
			return new ResponseEntity<HttpStatus>(HttpStatus.BAD_REQUEST);
		}else {
			
			try {
				UserRepresentation user = keycloakService.getUserExistsByUsernameOREmail(email, organizationName);
				if(user != null) {
					return new ResponseEntity<HttpStatus>(HttpStatus.FOUND);
				}else {

					IdentityProviderUser identityProviderUser = new IdentityProviderUser();
					List<Group> groups = keycloakService.getAllGroups(organizationName);
					List<Group> foundGroups = groups.stream()
							.filter(e -> e.getName().equalsIgnoreCase(groupName)).collect(Collectors.toList());
					if(foundGroups.size()>0) {
						identityProviderUser.setGroupId(foundGroups.get(0).getId());
						identityProviderUser.setGroupName(foundGroups.get(0).getName());
					}else {
						foundGroups = groups.stream()
								.filter(e -> e.getName().equalsIgnoreCase("UNIVERSAL ID")).collect(Collectors.toList());
						if(foundGroups.size()>0) {
							identityProviderUser.setGroupId(foundGroups.get(0).getId());
							identityProviderUser.setGroupName(foundGroups.get(0).getName());
						}else {
							return new ResponseEntity<HttpStatus>(HttpStatus.BAD_REQUEST);
						}
					}
					
						String userName = email.split("@")[0].replace(".", "").trim();		  
		
						HttpHeaders headers = new HttpHeaders();
						headers.set("loggedInUser", userName);
						headers.set("uimsUrl", "https://"+organizationName.toLowerCase()+".meetidentity.net");
						
						identityProviderUser.setUserName(userName);
						identityProviderUser.setEmail(email);
						identityProviderUser.setFirstName(userName);
						identityProviderUser.setLastName("");
						String[] roles = new String[1];
						if(roleId == null) {
							roles[0] = "725d0f44-1c90-4c06-a365-3f481ac52c15";
						}else {
							roles[0] = roleId;
						}
						identityProviderUser.setAuthServiceRoleID(roles);
						identityProviderUser.setEnabled(true);
						identityProviderUser.setSendInvite(true);
						identityProviderUser.setCredentialType("password");
						identityProviderUser.setCredentialTemporary(true);
						OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);
						identityProviderUser.setOrgId(organizationEntity.getId());
						identityProviderUser.setLoggedInUserId(0l);
						identityProviderUserService.createUserAndAssignGroupAndRoles(headers, organizationName, identityProviderUser, null).getBody();
	
						return new ResponseEntity<HttpStatus>(HttpStatus.CREATED);
					
				}	
			} catch (Exception cause) {
				
				log.error(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.USER_NOT_ENROLLED, email), cause);
				throw new EnrollmentServiceException(cause, ErrorMessages.USER_NOT_ENROLLED,
						email);
			}
		}
	}
	
	@Override
	public ResponseEntity<HttpStatus> changeUserAccess(String organizationName, String userName, String status){
		try {

			UserEntity userEntity = userRepository.getUserByName(userName, organizationName);

			if (userEntity != null) {
				JSONObject userJsonObject = new JSONObject();
				UserRepresentation userRepresentation = keycloakService.getUserByUsername(userName, organizationName);
				if(status.equalsIgnoreCase("enabled")) {
					userJsonObject.put("enabled", true);
				} else {
					userJsonObject.put("enabled", false);
				}
				keycloakService.updateUser(userRepresentation.getId(), userJsonObject, organizationName, userName);
			} 
			
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception cause) {

			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_DELETE_USER_WITH_USERNAME, userName), cause);
			throw new EntityNotFoundException(cause, ErrorMessages.FAILED_TO_DELETE_USER_WITH_USERNAME, userName);

		}

	}
	
	private void saveUserEnrollmentHistoryInSummary(User user, UserEntity userEntity, boolean isApprovalRequired,String organizationName) {

		UserEnrollmentHistory userEnrollmentHistory = user.getUserEnrollmentHistory();
		UserEnrollmentHistoryEntity userEnrollmentHistoryEntity = new UserEnrollmentHistoryEntity();
		
		userEnrollmentHistoryEntity.setEnrolledBy(userEnrollmentHistory.getEnrolledBy());
		userEnrollmentHistoryEntity.setEnrollmentStartDate(userEnrollmentHistory.getEnrollmentStartDate());
		userEnrollmentHistoryEntity.setEnrollmentEndDate(userEnrollmentHistory.getEnrollmentEndDate());
		userEnrollmentHistoryEntity.setCreatedBy(user.getCreatedBy());
		userEnrollmentHistoryEntity.setModifiedSteps(EnrollmentStepsEnum.ENROLL_SUMMARY.name());
		userEnrollmentHistoryEntity.setStatus(userEntity.getStatus());
		userEnrollmentHistoryEntity.setUser(userEntity);

		if (!isApprovalRequired) {
			userEnrollmentHistoryEntity.setApprovalStatus("Approved");
			userEnrollmentHistoryEntity.setReason("Auto Approved");
			userEnrollmentHistoryEntity.setApprovedDate(Timestamp.from(Instant.now()));
			userEnrollmentHistoryEntity.setApprovedBy(userEnrollmentHistory.getEnrolledBy());

		}
		userEnrollmentHistoryRepository.save(userEnrollmentHistoryEntity);
		return;
	}
	@Override
	public ResponseEntity<UserAvailability> findUserAvailability(String organizationName, UserAvailability userAvailability) {

		ResultPage resultPage = null;
		log.info(userAvailability.getEmail() + "-"+ userAvailability.getUsername());
		try {
			if(userAvailability.getEmail() != null) {
				resultPage = keycloakService.getUsersBySearchCriteria(organizationName, userAvailability.getEmail(), 0, 1);
				List<UserRepresentation> userRepresentations = (List<UserRepresentation>) resultPage.getContent();

				log.info(userRepresentations.size()+": email");
				if (userRepresentations != null && userRepresentations.size()>0) {
					log.info("email return");
					userAvailability.setEmail_available(false);
					return new ResponseEntity<UserAvailability>(userAvailability,HttpStatus.OK);				
				}else {
					userAvailability.setEmail_available(true);
				}
			}

			if(userAvailability.getUsername() != null) {
				resultPage = keycloakService.getUsersBySearchCriteria(organizationName, userAvailability.getUsername(), 0, 1);
				List<UserRepresentation> userRepresentations = (List<UserRepresentation>) resultPage.getContent();
				
				log.info(userRepresentations.size()+": name");
				if (userRepresentations != null && userRepresentations.size()>0) {
					log.info("name return");
					userAvailability.setUsername_available(false);
					return new ResponseEntity<UserAvailability>(userAvailability,HttpStatus.OK);			
				}else {
					userAvailability.setUsername_available(true);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw new EntityNotFoundException(e.getCause(), ErrorMessages.FAILED_TO_SEARCH_USER, organizationName);
		}
		log.info("can be used");
		return new ResponseEntity<UserAvailability>(userAvailability,HttpStatus.OK);
	}
	@Override
	public ResponseEntity<SearchUserResult> getUserByUserNameOrEmail(String organizationName, String userNameOrEmail) {
		SearchUserResult user = null;
		UserEntity userEntity = userRepository.getUserByUserNameOrEmail(organizationName, userNameOrEmail);
		if (userEntity != null) {
			user = new SearchUserResult();
			user.setUserID(userEntity.getId());
			UserAttributeEntity userAttributeEntity = userEntity.getUserAttribute();
			user.setFirstName(userAttributeEntity.getFirstName());
			user.setLastName(userAttributeEntity.getLastName());
			user.setEmail(userAttributeEntity.getEmail());
			user.setUserName(userEntity.getReferenceName());
			user.setUserStatus(userEntity.getStatus());
			user.setGroupName(userEntity.getGroupName());
			
			return new ResponseEntity<SearchUserResult>(user, HttpStatus.OK);
		}
		return new ResponseEntity<SearchUserResult>(user, HttpStatus.OK);
	}
}
