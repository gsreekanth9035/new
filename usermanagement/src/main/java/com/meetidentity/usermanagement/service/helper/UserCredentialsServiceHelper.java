package com.meetidentity.usermanagement.service.helper;

import org.springframework.stereotype.Component;

import com.meetidentity.usermanagement.domain.UserAddressEntity;
import com.meetidentity.usermanagement.web.rest.model.UserAddress;

@Component
public class UserCredentialsServiceHelper {
	public String getAddressFromAddressEntity_addressLine1(UserAddressEntity userAddressEntity) {
		StringBuffer address = null;
		if (userAddressEntity != null) {
			address = new StringBuffer();
			String addressLine1 = userAddressEntity.getAddressLine1();
			address.append(addressLine1 != null && addressLine1 != "" ? addressLine1 + ", " : "");
			return address.toString();
		}
		return "";
	}
	
	public String getAddressFromAddressEntity_addressLine2(UserAddressEntity userAddressEntity) {
		StringBuffer address = null;
		if (userAddressEntity != null) {
			address = new StringBuffer();
			String addressLine2 = userAddressEntity.getAddressLine2();
			String city = userAddressEntity.getCity();
			String state = userAddressEntity.getState();
			String country = userAddressEntity.getCountry();
			String zipCode = userAddressEntity.getZipCode();
			address.append(addressLine2 != null && addressLine2 != "" ? addressLine2 + ", " : "")
					.append(city != null && city != "" ? city + ", " : "")
					.append(state != null && state != "" ? state  : "")
					.append(zipCode != null && zipCode != "" ? zipCode + ", " : "")
					.append(country != null && country != "" ? country : "");
		}
		if(address!= null && address.length() > 0) {
			if (address.charAt(address.length() - 1) == ',' || address.charAt(address.length() - 1) == ' ') {
				address.deleteCharAt(address.length() - 1);
			}
			if (address.charAt(address.length() - 1) == ',') {
				address.deleteCharAt(address.length() - 1);
			}
			return address.toString();
		}
		return "";
	}
	
	public String getAddressFromAddressObj_addressLine1(UserAddressEntity userAddress) {
		StringBuffer address = null;
		if (userAddress != null) {
			address = new StringBuffer();
			String addressLine1 = userAddress.getAddressLine1();
			address.append(addressLine1 != null && addressLine1 != "" ? addressLine1 + ", " : "");
			return address.toString();
		}
		
		return "";
	}

	public String getAddressFromAddressObj_addressLine2(UserAddressEntity userAddress) {
		StringBuffer address = null;
		if (userAddress != null) {
			address = new StringBuffer();
			String addressLine2 = userAddress.getAddressLine2();
			String city = userAddress.getCity();
			String state = userAddress.getState();
			String zipCode = userAddress.getZipCode();
			String country = userAddress.getCountry();

			address.append(addressLine2 != null && addressLine2 != "" ? addressLine2 + ", " : "")
					.append(city != null && city != "" ? city + ", " : "")
					.append(state != null && state != "" ? state  : "")
					.append(zipCode != null && zipCode != "" ? zipCode + ", " : "")
					.append(country != null  && country != "" ? country : "");
		}
		if(address!= null && address.length() > 0) {
			if (address.charAt(address.length() - 1) == ',' || address.charAt(address.length() - 1) == ' ') {
				address.deleteCharAt(address.length() - 1);
			}
			if (address.charAt(address.length() - 1) == ',') {
				address.deleteCharAt(address.length() - 1);
			}
			return address.toString();
		}
		return "";
	}
	
	
	public String getAddressFromAddressObj_addressLine1(UserAddress userAddress) {
		StringBuffer address = null;
		if (userAddress != null) {
			address = new StringBuffer();
			String addressLine1 = userAddress.getAddressLine1();
			address.append(addressLine1 != null && addressLine1 != "" ? addressLine1 + ", " : "");
			return address.toString();
		}
		
		return "";
	}
	
	public String getAddressFromAddressObj_addressLine2(UserAddress userAddress) {
		StringBuffer address = null;
		if (userAddress != null) {
			address = new StringBuffer();
			String addressLine2 = userAddress.getAddressLine2();
			String city = userAddress.getCity();
			String state = userAddress.getState();
			String zipCode = userAddress.getZipCode();
			String country = userAddress.getCountry();

			address.append(addressLine2 != null && addressLine2 != "" ? addressLine2 + ", " : "")
					.append(city != null && city != "" ? city + ", " : "")
					.append(state != null && state != "" ? state  : "")
					.append(zipCode != null && zipCode != "" ? zipCode + ", " : "")
					.append(country != null  && country != "" ? country : "");
		}
		if(address!= null && address.length() > 0) {
			if (address.charAt(address.length() - 1) == ',' || address.charAt(address.length() - 1) == ' ') {
				address.deleteCharAt(address.length() - 1);
			}
			if (address.charAt(address.length() - 1) == ',') {
				address.deleteCharAt(address.length() - 1);
			}
			return address.toString();
		}
		return "";
	}
}
