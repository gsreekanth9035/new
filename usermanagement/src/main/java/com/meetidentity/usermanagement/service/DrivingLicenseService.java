package com.meetidentity.usermanagement.service;

import java.util.List;

import com.meetidentity.usermanagement.web.rest.model.DrivingLicenseRestriction;

public interface DrivingLicenseService {

	List<DrivingLicenseRestriction> getDrivingLicenseRestrictions(String organizationName);

}
