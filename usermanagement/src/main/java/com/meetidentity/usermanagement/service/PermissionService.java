package com.meetidentity.usermanagement.service;

import java.util.List;

import com.meetidentity.usermanagement.web.rest.model.Permission;

public interface PermissionService {
	
	List<Permission> getPermissionsByRole(String organizationName, String roleName);

}
