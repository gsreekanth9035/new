package com.meetidentity.usermanagement.service;

import java.util.List;

import com.meetidentity.usermanagement.domain.UserLocationEntity;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

public interface UserLocationCustomService {
	 List<UserLocationEntity> findByLocation(Point point);

	List<UserLocationEntity> findByLocation(Polygon footprint);
	
	List<UserLocationEntity> findByLocation(List<Point> points);

	ResultPage findByLocation(Polygon footprint, int page, int size);
}
