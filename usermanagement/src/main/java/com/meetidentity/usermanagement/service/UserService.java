package com.meetidentity.usermanagement.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.Response;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.express.enroll.ExpressEnrollmentSchema;
import com.meetidentity.usermanagement.exception.EnrollmentServiceException;
import com.meetidentity.usermanagement.exception.EntityCreateException;
import com.meetidentity.usermanagement.exception.EntityNotFoundException;
import com.meetidentity.usermanagement.exception.EntityUpdateException;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.InconsistentDataException;
import com.meetidentity.usermanagement.exception.PairDeviceInfoServiceException;
import com.meetidentity.usermanagement.exception.security.CryptoOperationException;
import com.meetidentity.usermanagement.keycloak.domain.ApprovalAttributes;
import com.meetidentity.usermanagement.keycloak.domain.UserStats;
import com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans.UserBioInfo;
import com.meetidentity.usermanagement.web.rest.model.AuthResponse;
import com.meetidentity.usermanagement.web.rest.model.AuthenticatorClientInfo;
import com.meetidentity.usermanagement.web.rest.model.EnableSoftOTP2FA;
import com.meetidentity.usermanagement.web.rest.model.EnrollmentStepsStatus;
import com.meetidentity.usermanagement.web.rest.model.ExpressEnrollForm;
import com.meetidentity.usermanagement.web.rest.model.ExpressEnrollmentResponse;
import com.meetidentity.usermanagement.web.rest.model.MobileCredential;
import com.meetidentity.usermanagement.web.rest.model.RegistrationConfigDetails;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.SearchUserResult;
import com.meetidentity.usermanagement.web.rest.model.TokenDetails;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserAvailability;
import com.meetidentity.usermanagement.web.rest.model.UserDetails;
import com.meetidentity.usermanagement.web.rest.model.UserDevice;
import com.meetidentity.usermanagement.web.rest.model.UserIdNamePair;
import com.meetidentity.usermanagement.web.rest.model.UserIssuanceStatus;
import com.meetidentity.usermanagement.web.rest.model.UserRequest;
import com.meetidentity.usermanagement.web.rest.model.WFStepRegistrationConfig;
import com.meetidentity.usermanagement.web.rest.model.WFStepVisualTemplateConfig;
import com.meetidentity.usermanagement.web.rest.model.WfMobileIDStepDetails;
import com.meetidentity.usermanagement.web.rest.model.Workflow;

public interface UserService {

	void registerUser(String organizationName, User user) throws EnrollmentServiceException;
	
	void registerMobileDeviceUser(String organizationName, User user) throws EnrollmentServiceException;
	
	ResponseEntity<User> registerLoggedInUser(String organizationName, User user) throws EnrollmentServiceException;
	
	User getUserByName(String orgName, String name) throws EntityNotFoundException;
	
	User getAllUserDetails(String orgName, String name) throws EntityNotFoundException, InconsistentDataException;

	User getUserByID(String id) throws EntityNotFoundException;

	User getUserByID(Long id, String gpsCoordinates) throws EntityNotFoundException;	

	UserDetails getUserByIDOrEntity(Long id, UserEntity userEntity) throws EntityNotFoundException;
	
	UserDetails getUserDetailsByName(String orgName, String name) throws EntityNotFoundException;

	User verifyOtp(String mobileNumber, String otp) throws EntityNotFoundException;

	String decryptUserUUID(String encryptedUUID) throws CryptoOperationException;

	UserEntity getUserbyUUID(String organizationName, String encryptedUUID) throws CryptoOperationException, EntityNotFoundException;

	List<UserIdNamePair> getUserIdNames(String organizationName) throws EntityNotFoundException;

	Workflow getWorkflowByUser(Long userID) throws EntityNotFoundException;

	Workflow getWorkflowByUserName(String organizationName, String userName) throws EntityNotFoundException;

	ResultPage getUsersBySearchCriteria(String organizationName, String searchCriteria, int page, int size) throws EntityNotFoundException;

	ResultPage getMembersOfAGroup(String organizationName, String groupID, int page, int size) throws EntityNotFoundException;

	void updateUserStatusAfterIssuance(Long userID, User user) throws EntityUpdateException;

	void updateUserStatustoReissuance(String organizationName, Set<Long> userIDs) throws EntityUpdateException;

	UserEntity addUser(UserEntity entity, String organizationName) throws EntityCreateException;

	ResultPage pendingEnrollmentStat(String organizationName, String groupID, String sortBy, String order, int page, int size, Date createdDateStart, Date createdDateEnd)  throws EntityNotFoundException ;

	ResultPage pendingIssuanceStat(String organizationName, String groupID, String sortBy, String order, int page, int size, Date createdDateStart, Date createdDateEnd)  throws EntityNotFoundException ;

	ResultPage pendingApprovalStat(String organizationName, String groupID, String sortBy, String order, int page, int size, Date createdDateStart, Date createdDateEnd)  throws EntityNotFoundException ;
	
	List<UserStats> userStats(String organizationName) throws EntityNotFoundException ;

	Long getUserIDByName(String organizationName, String name) throws EntityNotFoundException ;

	void userLogout(String organizationName, String userName) throws IdentityBrokerServiceException;
	
	ResponseEntity<HttpStatus> deleteUserByName(String organizationName, String userName, boolean isFederated) throws IdentityBrokerServiceException;

	int experssEnroll(HttpHeaders httpHeaders, String organizationName, String enrolledBy, String groupID, String groupName, String roleIDs, ExpressEnrollmentSchema schema)   throws EnrollmentServiceException;

	void updateUser(String organizationName, User user) throws EnrollmentServiceException;
	
	List<WFStepVisualTemplateConfig>  getWorkflowVisualTemplates(String organizationName, String userName);
	
	List<MobileCredential> getUserMobileIDIdentities(Long userId);
	
	WfMobileIDStepDetails getUserWfMobileIDStepDetails(Long userId);
	
	User getUserRefNameOrgByUserId(Long userId);

	ResultPage getUsersByLocation(String organizationName, String searchCriteria, int page, int size);

	void updateUsersLocationStatus(String organizationName, int status, List<Long> markedUsers, int enablePush);
	
	UserBioInfo getUserBiometricsByUserId(Long userId);
	
	UserBioInfo getUserBioByUserIdAndBioType(Long userId,String biometricType);

	ResponseEntity<Void> enableSoftOTP2FA(String organizationName, EnableSoftOTP2FA enableSoftOTP2FA);

	String generateSessionId(String organizationName, Long userId)  throws PairDeviceInfoServiceException;
	
	/**
	 * startDate and endDate mandatory </br>
	 * userName and status not mandatory. Pass Null
	 * 
	 * @param organizationName
	 * @param userName
	 * @param status
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws EntityNotFoundException
	 */
	ResultPage getUsersByFilters(String organizationName, UserRequest userRequest, int page, int size)
			throws EntityNotFoundException;
//	ResultPage getUsersByFilters(String organizationName, String userName, String status, Instant startDate,
//			Instant endDate, int page, int size) throws EntityNotFoundException;

	Workflow getWorkflowGeneralDetailsByUserName(String organizationName, String userName);

	ResponseEntity<Void> approveEnrollment(HttpHeaders httpHeaders, String organizationName, String userName, ApprovalAttributes approveAttr);
	
	ResponseEntity<Void> rejectEnrollment(String organizationName, String userName, ApprovalAttributes approveAttr);
	
	UserDevice validateUserSession(String organizationName, String userName);

	Long getIssuedDevicesByUserName(String userName, String organizationName);
	
	UserDevice getUUID(String organizationName, String userName);

	UserIssuanceStatus getIssuanceStatus(String organizationName, String userName);

	void updateUserStatusAfterDeleteCred(String organizationName, Map<Long, String> userStatus)
			throws EntityUpdateException;

	void resetMobileOTPCredential(String organizationName, String userName) throws EntityUpdateException;

	AuthenticatorClientInfo getAuthenticatorClientInfo(String organizationName, String publicKeyB64, String publicKeyAlgorithm) throws EntityNotFoundException;
	
	ResponseEntity<TokenDetails> generateMobileIDUserTokens(String organizationName, String publicKeyB64,
			String publicKeyAlgorithm);
			
	ResponseEntity<UserDetails> getUserEnrollmentDetails(String organizationName, String userName);
	ResponseEntity<UserDetails> getUserStatus(String organizationName, String userName);
	
	User getUserDetailsByUserId(String organizationName, Long userId);
	
	UserAttribute getUserAttributesByUserId(String organizationName, Long userId);
	
	List<Long> getOrgUsersFromUserIds(String organizationName, List<Long> userIdList);

	void  saveIDProofDocs(HttpHeaders httpHeaders, String organizationName, User user) throws EnrollmentServiceException;
	
	void saveSkipUserBiometrics(HttpHeaders httpHeaders, String organizationName, User user) throws EnrollmentServiceException;

	void saveUserBiometrics(HttpHeaders httpHeaders, String organizationName, User user, String biometricType) throws EnrollmentServiceException;

	void saveRegistration(HttpHeaders httpHeaders, String organizationName, User user) throws EnrollmentServiceException;

	String completeEnrollmentInProgress(HttpHeaders httpHeaders, UserEntity userEntity, String organizationName, User user, boolean isDirectSave) throws EnrollmentServiceException;

	EnrollmentStepsStatus enrollmentStepsStatus(String organizationName, String userName) throws EnrollmentServiceException;

	User getTransparentFacialImage(HttpHeaders httpHeaders, String organizationName, User user);

	ResponseEntity<ExpressEnrollmentResponse> expressEnroll(String organizationName, ExpressEnrollForm selfRegistrationForm) throws EnrollmentServiceException;

	ResponseEntity<HttpStatus> searchUser_SendInvite(String organizationName, String email, String groupName, String roleId) throws EnrollmentServiceException;

	UserEntity getUserEntityByUserName(String organizationName, String userName);

	AuthenticatorClientInfo getOrgClientCredentials(String organizationName)
			throws CryptoOperationException, IdentityBrokerServiceException;
	
	void validatePNLogin(String organizationName, AuthResponse authResponse) throws Exception;

	ResponseEntity<HttpStatus> changeUserAccess(String organizationName, String userName, String status);

	List<WFStepRegistrationConfig> setRegistrationFields(List<WFStepRegistrationConfig> wfStepRegistrationConfigs,
			RegistrationConfigDetails registrationConfigDetails, String organizationName);

	ResponseEntity<UserAvailability> findUserAvailability(String organizationName, UserAvailability userAvailability);

	ResponseEntity<SearchUserResult> getUserByUserNameOrEmail(String organizationName, String userNameOrEmail);
}
