package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meetidentity.usermanagement.domain.DeviceTypeEntity;
import com.meetidentity.usermanagement.repository.DeviceTypeRepository;
import com.meetidentity.usermanagement.service.DeviceTypeService;
import com.meetidentity.usermanagement.web.rest.model.DeviceType;

@Service
@Transactional
public class DeviceTypeServiceImpl implements DeviceTypeService {

	@Autowired
	private DeviceTypeRepository deviceTypeRepository;

	@Override
	public List<DeviceType> getVisualApplicableDeviceTypes() {
		List<DeviceType> deviceTypes = new ArrayList<>();
		List<DeviceTypeEntity> deviceTypeEntities = deviceTypeRepository.findVisualApplicableDevices();
		deviceTypeEntities.stream().filter(Objects::nonNull).forEach(deviceTypeEntity -> {
			DeviceType deviceType = new DeviceType();
			deviceType.setId(deviceTypeEntity.getId());
			deviceType.setName(deviceTypeEntity.getName());
			deviceTypes.add(deviceType);
		});
		return deviceTypes;
	}

}
