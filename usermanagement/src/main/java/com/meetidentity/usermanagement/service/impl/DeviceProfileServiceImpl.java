package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.ConfigValueEntity;
import com.meetidentity.usermanagement.domain.DeviceProfileConfigEntity;
import com.meetidentity.usermanagement.domain.DeviceProfileEntity;
import com.meetidentity.usermanagement.domain.DeviceProfileHsmInfoEntity;
import com.meetidentity.usermanagement.domain.DeviceProfileKeyManagerEntity;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.WorkflowEntity;
import com.meetidentity.usermanagement.enums.DirectionEnum;
import com.meetidentity.usermanagement.exception.DeviceProfileServiceException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.repository.ConfigValueRepository;
import com.meetidentity.usermanagement.repository.DeviceProfileConfigRepository;
import com.meetidentity.usermanagement.repository.DeviceProfileRepository;
import com.meetidentity.usermanagement.repository.OrganizationRepository;
import com.meetidentity.usermanagement.repository.WorkflowRepository;
import com.meetidentity.usermanagement.service.DeviceProfileServcie;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.feign.client.ApduFeignClient;
import com.meetidentity.usermanagement.web.rest.model.ConfigValue;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfile;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfileConfig;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfileHsmInfo;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfileKeyManager;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;

@Service
@Transactional
public class DeviceProfileServiceImpl implements DeviceProfileServcie {

	private static final Logger log = LoggerFactory.getLogger(DeviceProfileServiceImpl.class);

	@Autowired
	private DeviceProfileRepository devProfRepository;

	@Autowired
	private ConfigValueRepository configValueRepository;
	
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private ApduFeignClient apdutFeignClient;
	
	@Autowired
	private WorkflowRepository workflowRepository;
	
	@Autowired
	private DeviceProfileConfigRepository deviceProfileConfigRepository;
	
	@Override
	public List<DeviceProfile> getDeviceProfiles(String organizationName) throws DeviceProfileServiceException{
		List<DeviceProfile> deviceProfileList = new ArrayList<>();
		try{
			List<DeviceProfileEntity> devProfileEntityList = devProfRepository.findAllByOrgName(organizationName);
			
			for (DeviceProfileEntity deviceProfileEntity : devProfileEntityList) {
				DeviceProfile deviceProfile = new DeviceProfile();
				deviceProfile.setId(deviceProfileEntity.getId());
				deviceProfile.setName(deviceProfileEntity.getName());
				
				ConfigValueEntity cvEntityAppletType = deviceProfileEntity.getConfigValueProductName();
				ConfigValue cvAppletType = new ConfigValue();
				cvAppletType.setValue(cvEntityAppletType.getValue());
				deviceProfile.setConfigValueProductName(cvAppletType);
				
				List<Object[]> objects = deviceProfileConfigRepository.getDevProfConfig(cvEntityAppletType.getId());
				if(objects.size() > 0) {
					Object[] deviceProfileConfigDetails = objects.get(0);
					DeviceProfileConfig deviceProfileConfig = new DeviceProfileConfig();
					deviceProfileConfig.setIsMobileId((Boolean) deviceProfileConfigDetails[0]);
					deviceProfileConfig.setIsPlasticId((Boolean) deviceProfileConfigDetails[1]);
					deviceProfileConfig.setIsRfidCard((Boolean) deviceProfileConfigDetails[2]);
					deviceProfileConfig.setIsAppletLoadingRequired((Boolean) deviceProfileConfigDetails[3]);
					deviceProfile.setDeviceProfileConfig(deviceProfileConfig);
				}
				deviceProfileList.add(deviceProfile);
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,ErrorMessages.FAILED_TO_GET_DEVICEPROFILES + " : " + cause.getMessage());
			throw new DeviceProfileServiceException(ErrorMessages.FAILED_TO_GET_DEVICEPROFILES, cause);
		}
		return deviceProfileList;
	}
	
	@Override
	public ResultPage getDeviceProfilesList(String organizationName, String sortBy, String order, int page, int size) throws DeviceProfileServiceException{
		
		List<DeviceProfile> deviceProfileList = new ArrayList<>();
		ResultPage resultPage = null;
		Sort sort = null;
		if (!sortBy.equalsIgnoreCase("undefined") && !(order.isEmpty())) {
			if (sortBy.equalsIgnoreCase("category")) {
				sortBy = "configValueCategory.value";
			} else if (sortBy.equalsIgnoreCase("supplier")) {
				sortBy = "configValueSupplier.value";
			} else if (sortBy.equalsIgnoreCase("productName")) {
				sortBy = "configValueProductName.value";
			}
			if (order.equals(DirectionEnum.ASC.getDirectionCode())) {
				sort = new Sort(new Sort.Order(Direction.ASC, sortBy));
			}
			if (order.equals(DirectionEnum.DESC.getDirectionCode())) {
				sort = new Sort(new Sort.Order(Direction.DESC, sortBy));
			}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		}	
		try {
			Pageable pageable = new PageRequest(page, size, sort);
			Page<DeviceProfileEntity> pagedResult = devProfRepository.findAllByOrg(organizationName,pageable);
			if (pagedResult.getTotalElements() != 0) {
				for (DeviceProfileEntity deviceProfileEntity : pagedResult) {
					DeviceProfile deviceProfile = new DeviceProfile();
					deviceProfile.setId(deviceProfileEntity.getId());
					deviceProfile.setName(deviceProfileEntity.getName());
					deviceProfile.setDescription(deviceProfileEntity.getDescription());
					ConfigValueEntity cvEntityDevManufacturer = deviceProfileEntity.getConfigValueSupplier();
					ConfigValueEntity cvEntityAppletType = deviceProfileEntity.getConfigValueProductName();
					ConfigValueEntity cvEntityCategory = deviceProfileEntity.getConfigValueCategory();
					ConfigValue cvDevManufacturer = new ConfigValue();
					cvDevManufacturer.setValue(cvEntityDevManufacturer.getValue());
					ConfigValue cvAppletType = new ConfigValue();
					cvAppletType.setId(cvEntityAppletType.getId());
					cvAppletType.setValue(cvEntityAppletType.getValue());
					ConfigValue cvCategory = new ConfigValue();
					cvCategory.setValue(cvEntityCategory.getValue());
	
					deviceProfile.setConfigValueCategory(cvCategory);
					deviceProfile.setConfigValueSupplier(cvDevManufacturer);
					deviceProfile.setConfigValueProductName(cvAppletType);
					deviceProfileList.add(deviceProfile);
				}
				resultPage = new ResultPage();
				resultPage.setContent(deviceProfileList);
				resultPage.setFirst(pagedResult.isFirst());
				resultPage.setLast(pagedResult.isLast());
				resultPage.setNumber(pagedResult.getNumber());
				resultPage.setNumberOfElements(pagedResult.getNumberOfElements());
				resultPage.setSize(pagedResult.getSize());
				resultPage.setSort(pagedResult.getSort());
				resultPage.setTotalElements(pagedResult.getTotalElements());
				resultPage.setTotalPages(pagedResult.getTotalPages());
			}
		}catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,ErrorMessages.FAILED_TO_GET_DEVICEPROFILES + " : " + cause.getMessage());
			throw new DeviceProfileServiceException(ErrorMessages.FAILED_TO_GET_DEVICEPROFILES, cause);
		}
		return resultPage;
	}

	@Override
	public DeviceProfile getDeviceProfileById(Long deviceProfileID)  throws DeviceProfileServiceException{
		try {
			DeviceProfileEntity deviceProfileEntity = devProfRepository.findDeviceProfileByID(deviceProfileID);
			return getDevProfFromEntity(deviceProfileEntity);
		}catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.FAILED_TO_GET_DEVICEPROFILE_FOR_ID,deviceProfileID) + " : " + cause.getMessage());
			throw new DeviceProfileServiceException(cause, Util.format(ErrorMessages.FAILED_TO_GET_DEVICEPROFILE_FOR_ID,deviceProfileID));
		}
	}
	
	@Override
	public DeviceProfile getDevProfFromEntity(DeviceProfileEntity deviceProfileEntity) throws DeviceProfileServiceException{
		DeviceProfile deviceProfile = new DeviceProfile();
		try{
			if (deviceProfileEntity != null) {
			deviceProfile.setId(deviceProfileEntity.getId());
			deviceProfile.setName(deviceProfileEntity.getName());
			deviceProfile.setDescription(deviceProfileEntity.getDescription());

			ConfigValueEntity cvEntityDevManufacturer = deviceProfileEntity.getConfigValueSupplier();
			ConfigValueEntity cvEntityAppletType = deviceProfileEntity.getConfigValueProductName();
			ConfigValueEntity cvEntityCategory = deviceProfileEntity.getConfigValueCategory();
			ConfigValue cvDevManufacturer = new ConfigValue();
			cvDevManufacturer.setId(cvEntityDevManufacturer.getId());
			cvDevManufacturer.setValue(cvEntityDevManufacturer.getValue());
			ConfigValue cvAppletType = new ConfigValue();
			cvAppletType.setId(cvEntityAppletType.getId());
			cvAppletType.setValue(cvEntityAppletType.getValue());
			ConfigValue cvCategory = new ConfigValue();
			cvCategory.setId(cvEntityCategory.getId());
			cvCategory.setValue(cvEntityCategory.getValue());
			
			deviceProfile.setConfigValueSupplier(cvDevManufacturer);
			deviceProfile.setConfigValueProductName(cvAppletType);
			deviceProfile.setConfigValueCategory(cvCategory);
			
			deviceProfile.setKeyStorageType(deviceProfileEntity.getKeyStorageType());
			if (deviceProfileEntity.getDevProfKeyMgr() != null) {

				DeviceProfileKeyManagerEntity devProfKeyMgrEntity = deviceProfileEntity.getDevProfKeyMgr();
				DeviceProfileConfig deviceProfileConfig = getDeviceProfileConfigs(cvAppletType.getId());
				
				DeviceProfileKeyManager devProfKeyMgr = new DeviceProfileKeyManager();

				devProfKeyMgr.setId(devProfKeyMgrEntity.getId());
				devProfKeyMgr.setDiversify(devProfKeyMgrEntity.getDiversify());
				devProfKeyMgr.setDiversify(devProfKeyMgrEntity.getDiversify());
				devProfKeyMgr.setIsAdminKeySameAsMasterKey(devProfKeyMgrEntity.getIsAdminKeySameAsMasterKey());
				if(deviceProfileConfig.getEnableAdminKey() && devProfKeyMgrEntity.getAdminKey() != null) {
					devProfKeyMgr.setAdminKey(apdutFeignClient.decrypt(devProfKeyMgrEntity.getAdminKey()));
				} if(deviceProfileConfig.getEnableMasterKey() && devProfKeyMgrEntity.getMasterKey() != null) {
					devProfKeyMgr.setMasterKey(apdutFeignClient.decrypt(devProfKeyMgrEntity.getMasterKey()));
				} if(deviceProfileConfig.getEnableCustomerMasterKey() && devProfKeyMgrEntity.getCustomerMasterKey() != null) {
					devProfKeyMgr.setCustomerMasterKey(apdutFeignClient.decrypt(devProfKeyMgrEntity.getCustomerMasterKey()));
				} if(deviceProfileConfig.getEnableCustomerAdminKey() && devProfKeyMgrEntity.getCustomerAdminKey() != null) {
					devProfKeyMgr.setCustomerAdminKey(apdutFeignClient.decrypt(devProfKeyMgrEntity.getCustomerAdminKey()));
				} if(deviceProfileConfig.getEnableFactoryManagementKey() && devProfKeyMgrEntity.getFactoryManagementKey() != null) {
					devProfKeyMgr.setFactoryManagementKey(apdutFeignClient.decrypt(devProfKeyMgrEntity.getFactoryManagementKey()));
				} if(deviceProfileConfig.getEnableCustomerManagementKey() && devProfKeyMgrEntity.getCustomerManagementKey() != null) {
					devProfKeyMgr.setCustomerManagementKey(apdutFeignClient.decrypt(devProfKeyMgrEntity.getCustomerManagementKey()));
				}

				deviceProfile.setDevProfKeyMgr(devProfKeyMgr);
			} if (deviceProfileEntity.getDevProfHsmInfo() != null) {

				DeviceProfileHsmInfoEntity devProfhsmInfoEntity = deviceProfileEntity.getDevProfHsmInfo();
				DeviceProfileHsmInfo devProfhsmInfo = new DeviceProfileHsmInfo();

				devProfhsmInfo.setId(devProfhsmInfoEntity.getId());
				devProfhsmInfo.setHsmTypeId(devProfhsmInfoEntity.getHsmTypeId());
				devProfhsmInfo.setPartitionName(devProfhsmInfoEntity.getPartitionName());
				devProfhsmInfo.setPartitionPassword(devProfhsmInfoEntity.getPartitionPassword());
				devProfhsmInfo.setPartitionSerialNumber(devProfhsmInfoEntity.getPartitionSerialNumber());

				deviceProfile.setDevProfHsmInfo(devProfhsmInfo);
			}
		}
		}catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.FAILED_TO_GET_DEVICEPROFILE_FOR_DEVICEPROFILEENTITY,deviceProfileEntity.getName()) + " : " + cause.getMessage());
			throw new DeviceProfileServiceException(cause, Util.format(ErrorMessages.FAILED_TO_GET_DEVICEPROFILE_FOR_DEVICEPROFILEENTITY,deviceProfileEntity.getName()));
		}
		return deviceProfile;
	}

	@Override
	public ResponseEntity<String> saveDeviceProfile(String organizationName, DeviceProfile deviceProfile) throws DeviceProfileServiceException {
		// TODO handle Exceptions

		OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);
		
		DeviceProfileEntity devProfEntity = new DeviceProfileEntity();
		try{
			DeviceProfileEntity devProfFromDb = devProfRepository.findByNameAndOrganization(organizationName, deviceProfile.getName());
		
			if (devProfFromDb != null) {
				if (deviceProfile.getName().equalsIgnoreCase(devProfFromDb.getName())) {
					log.debug("Device Profile with the name" + " " + deviceProfile.getName() + " " + "already exists");
					// throw new Exception(
							// "Device Profile with the name" + " " + deviceProfile.getName() + " " + "already exists");
					return new ResponseEntity<>(HttpStatus.CONFLICT);
				}
			}
			devProfEntity.setName(deviceProfile.getName());
			devProfEntity.setDescription(deviceProfile.getDescription());
			devProfEntity.setOrganization(organizationEntity);
			ConfigValueEntity cvEntityDevManufacturer = new ConfigValueEntity();
			ConfigValueEntity cvEntityAppletType = new ConfigValueEntity();
			ConfigValueEntity cvEntityCategory = new ConfigValueEntity();
			ConfigValue cvCategory = deviceProfile.getConfigValueCategory();
			ConfigValue cvDevManufacturer = deviceProfile.getConfigValueSupplier();
			ConfigValue cvAppletType = deviceProfile.getConfigValueProductName();
			
			cvEntityCategory.setId(configValueRepository.findByOrgAndValue(organizationName, cvCategory.getValue()).getId());
			cvEntityDevManufacturer.setId(configValueRepository.findByConfigValueAndParentConfigValue(organizationName,cvDevManufacturer.getValue(), cvCategory.getValue()).getId());
			ConfigValueEntity cvEntityForProductName=  configValueRepository.findByConfigValueAndParentConfigValue(organizationName, cvAppletType.getValue(), cvDevManufacturer.getValue());
			cvEntityAppletType.setId(cvEntityForProductName.getId());
			
			devProfEntity.setConfigValueSupplier(cvEntityDevManufacturer);
			devProfEntity.setConfigValueProductName(cvEntityAppletType);
			devProfEntity.setConfigValueCategory(cvEntityCategory);
			devProfEntity.setKeyStorageType(deviceProfile.getKeyStorageType());
			if (deviceProfile.getDevProfKeyMgr() != null) {
				DeviceProfileKeyManager devProfKeyMgr = deviceProfile.getDevProfKeyMgr();
				DeviceProfileKeyManagerEntity devProfKeyMgrEntity = new DeviceProfileKeyManagerEntity();
				DeviceProfileConfig deviceProfileConfig = getDeviceProfileConfigs(cvEntityForProductName.getId());
				devProfKeyMgrEntity.setDeviceProfile(devProfEntity);
				devProfKeyMgrEntity.setDiversify(devProfKeyMgr.getDiversify());
				devProfKeyMgrEntity.setIsAdminKeySameAsMasterKey(devProfKeyMgr.getIsAdminKeySameAsMasterKey());
				
				if(deviceProfileConfig.getEnableAdminKey()) {
					devProfKeyMgrEntity.setAdminKey(apdutFeignClient.encrypt(devProfKeyMgr.getAdminKey()));
				} if(deviceProfileConfig.getEnableMasterKey()) {
					devProfKeyMgrEntity.setMasterKey(apdutFeignClient.encrypt(devProfKeyMgr.getMasterKey()));
				} if(deviceProfileConfig.getEnableCustomerMasterKey()) {
					devProfKeyMgrEntity.setCustomerMasterKey(apdutFeignClient.encrypt(devProfKeyMgr.getCustomerMasterKey()));
				} if(deviceProfileConfig.getEnableCustomerAdminKey()) {
					devProfKeyMgrEntity.setCustomerAdminKey(apdutFeignClient.encrypt(devProfKeyMgr.getCustomerAdminKey()));
				} if(deviceProfileConfig.getEnableFactoryManagementKey()) {
					devProfKeyMgrEntity.setFactoryManagementKey(apdutFeignClient.encrypt(devProfKeyMgr.getFactoryManagementKey()));
				} if(deviceProfileConfig.getEnableCustomerManagementKey()) {
					devProfKeyMgrEntity.setCustomerManagementKey(apdutFeignClient.encrypt(devProfKeyMgr.getCustomerManagementKey()));
				}
				devProfEntity.setDevProfKeyMgr(devProfKeyMgrEntity);
			}
			 if (deviceProfile.getKeyStorageType().equalsIgnoreCase("HSM")) {
				DeviceProfileHsmInfoEntity hsmInfoEntity = new DeviceProfileHsmInfoEntity();
				DeviceProfileHsmInfo devProfHsmInfo = deviceProfile.getDevProfHsmInfo();
				hsmInfoEntity.setHsmTypeId(devProfHsmInfo.getHsmTypeId()); // need to decide the hsm type id
				hsmInfoEntity.setPartitionName(devProfHsmInfo.getPartitionName());
				hsmInfoEntity.setPartitionSerialNumber(devProfHsmInfo.getPartitionSerialNumber());
				hsmInfoEntity.setPartitionPassword(devProfHsmInfo.getPartitionPassword());
				hsmInfoEntity.setDeviceProfile(devProfEntity);
				devProfEntity.setDevProfHsmInfo(hsmInfoEntity);
			 }
			
			DeviceProfileEntity savedDevProfEntity = devProfRepository.save(devProfEntity);
			return new ResponseEntity<>(HttpStatus.CREATED);
		}catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.FAILED_TO_CREATE_DEVICEPROFILE,deviceProfile.getName()) + " : " + cause.getMessage());
			throw new DeviceProfileServiceException(cause, Util.format(ErrorMessages.FAILED_TO_CREATE_DEVICEPROFILE,deviceProfile.getName()));
		}
	}

	@Override
	public ResponseEntity<Void> deleteDeviceProfile(String organizationName, Long devProfileID) throws DeviceProfileServiceException {
		try {
			DeviceProfileEntity devProfEntity = devProfRepository.getOne(devProfileID);
			if(devProfEntity != null) {
				List<WorkflowEntity> wfs = workflowRepository.getByDeviceProfileId(organizationName, devProfileID);
				if(wfs.size() == 0) {
					// it means this device profile is not using in workflow
					devProfRepository.delete(devProfileID);
					return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				} else {
					return new ResponseEntity<>(HttpStatus.CONFLICT);
				}
			}
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.FAILED_TO_DELETE_DEVICEPROFILE_WITH_ID,devProfileID) + " : " + cause.getMessage());
			throw new DeviceProfileServiceException(cause, Util.format(ErrorMessages.FAILED_TO_DELETE_DEVICEPROFILE_WITH_ID,devProfileID));
		}
	}

	@Override
	public ResponseEntity<String> updateDeviceProfile(Long devProfileID, DeviceProfile deviceProfile)  throws DeviceProfileServiceException {
		DeviceProfileEntity deviceProfileEntity = devProfRepository.findDeviceProfileByID(devProfileID);
		try{
			if (deviceProfileEntity == null) {
				log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.DEVICEPROFILE_NOT_FOUND_WITH_ID,devProfileID));				
				throw new DeviceProfileServiceException(Util.format(ErrorMessages.DEVICEPROFILE_NOT_FOUND_WITH_ID,devProfileID));
			}
	
			String devProfName = deviceProfileEntity.getName();
			DeviceProfileEntity devProf = devProfRepository.findByNameAndOrganization(deviceProfileEntity.getOrganization().getName(), deviceProfile.getName());
	
			if (devProf != null) {
				if (devProf.getName() != deviceProfileEntity.getName()) {
					log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.DEVICEPROFILE_ALREADY_EXISTS_WITH_SAME_NAME,devProf.getName()));
					return new ResponseEntity<>(HttpStatus.CONFLICT);
				 //	throw new EntityAlreadyExistsException(Util.format(ErrorMessages.DEVICEPROFILE_ALREADY_EXISTS_WITH_SAME_NAME,devProf.getName()));		
				}
			}
			deviceProfileEntity.setName(deviceProfile.getName());
			deviceProfileEntity.setDescription(deviceProfile.getDescription());
	
			if (deviceProfile.getDevProfKeyMgr() != null) {
				DeviceProfileKeyManagerEntity devProfKeyMgrEntity = deviceProfileEntity.getDevProfKeyMgr();
				DeviceProfileKeyManager devProfKeyMgr = deviceProfile.getDevProfKeyMgr();
				DeviceProfileConfig deviceProfileConfig = getDeviceProfileConfigs(deviceProfile.getConfigValueProductName().getId());
				devProfKeyMgrEntity.setDiversify(devProfKeyMgr.getDiversify());
				devProfKeyMgrEntity.setIsAdminKeySameAsMasterKey(devProfKeyMgr.getIsAdminKeySameAsMasterKey());
				
				if(deviceProfileConfig.getEnableAdminKey()) {
					devProfKeyMgrEntity.setAdminKey(apdutFeignClient.encrypt(devProfKeyMgr.getAdminKey()));
				} if(deviceProfileConfig.getEnableMasterKey()) {
					devProfKeyMgrEntity.setMasterKey(apdutFeignClient.encrypt(devProfKeyMgr.getMasterKey()));
				} if(deviceProfileConfig.getEnableCustomerMasterKey()) {
					devProfKeyMgrEntity.setCustomerMasterKey(apdutFeignClient.encrypt(devProfKeyMgr.getCustomerMasterKey()));
				} if(deviceProfileConfig.getEnableCustomerAdminKey()) {
					devProfKeyMgrEntity.setCustomerAdminKey(apdutFeignClient.encrypt(devProfKeyMgr.getCustomerAdminKey()));
				} if(deviceProfileConfig.getEnableFactoryManagementKey()) {
					devProfKeyMgrEntity.setFactoryManagementKey(apdutFeignClient.encrypt(devProfKeyMgr.getFactoryManagementKey()));
				} if(deviceProfileConfig.getEnableCustomerManagementKey()) {
					devProfKeyMgrEntity.setCustomerManagementKey(apdutFeignClient.encrypt(devProfKeyMgr.getCustomerManagementKey()));
				}
					
				deviceProfileEntity.setDevProfKeyMgr(devProfKeyMgrEntity);
			} if (deviceProfile.getDevProfHsmInfo() != null) {
				DeviceProfileHsmInfoEntity devProfHsmInfoEntity = deviceProfileEntity.getDevProfHsmInfo();
				DeviceProfileHsmInfo devProfHsmInfo = deviceProfile.getDevProfHsmInfo();
				devProfHsmInfoEntity.setPartitionName(deviceProfile.getDevProfHsmInfo().getPartitionName());
				devProfHsmInfoEntity.setPartitionSerialNumber(devProfHsmInfo.getPartitionSerialNumber());
				devProfHsmInfoEntity.setPartitionPassword(devProfHsmInfo.getPartitionPassword());
			}
			devProfRepository.save(deviceProfileEntity);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.FAILED_TO_UPDATE_DEVICEPROFILE,deviceProfile.getName()) + " : " + cause.getMessage());
			throw new DeviceProfileServiceException(cause, Util.format(ErrorMessages.FAILED_TO_UPDATE_DEVICEPROFILE,deviceProfile.getName()));
		}
	}

	@Override
	public String getDevProfName(Long devProfileID)  throws DeviceProfileServiceException {
		try {
			DeviceProfileEntity devProfEntity = devProfRepository.findOne(devProfileID);
			String productName = devProfEntity.getConfigValueProductName().getValue();
			String returnDevProfName = (devProfEntity.getConfigValueSupplier().getValue() + " " + productName);;
			if(productName.equalsIgnoreCase("ID-One PIV v 2.4.1 on Cosmo V8.1")) {
				productName = "ID-One PIV";
				returnDevProfName = (devProfEntity.getConfigValueSupplier().getValue() + " " + productName);
			} else {
				returnDevProfName = productName;
			}
			return returnDevProfName;
		}catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.FAILED_TO_GET_DEVICEPROFILE_FOR_ID,devProfileID) + " : " + cause.getMessage());
			throw new DeviceProfileServiceException(cause, Util.format(ErrorMessages.FAILED_TO_GET_DEVICEPROFILE_FOR_ID,devProfileID));
		}
	}

	@Override
	public DeviceProfileConfig getDeviceProfileConfigs(Long productValueId) {
		DeviceProfileConfig deviceProfileConfig = null;
		DeviceProfileConfigEntity deviceProfileConfigEntity = deviceProfileConfigRepository.getDevProfConfigByConfigValueID(productValueId);
		if(deviceProfileConfigEntity != null) {
			deviceProfileConfig = new DeviceProfileConfig();
			
			deviceProfileConfig.setConfigValueId(productValueId);
			deviceProfileConfig.setEnableKeyManager(deviceProfileConfigEntity.getEnableKeyManager());
			deviceProfileConfig.setEnableDiversify(deviceProfileConfigEntity.getEnableDiversify());
			deviceProfileConfig.setEnableMasterKey(deviceProfileConfigEntity.getEnableMasterKey());
			deviceProfileConfig.setEnableAkIsIdenticalToMkCheck(deviceProfileConfigEntity.getEnableAkIsIdenticalToMkCheck());
			deviceProfileConfig.setEnableAdminKey(deviceProfileConfigEntity.getEnableAdminKey());
			deviceProfileConfig.setEnableCustomerMasterKey(deviceProfileConfigEntity.getEnableCustomerMasterKey());
			deviceProfileConfig.setEnableCustomerAdminKey(deviceProfileConfigEntity.getEnableCustomerAdminKey());
			deviceProfileConfig.setEnableCustomerManagementKey(deviceProfileConfigEntity.getEnableCustomerManagementKey());
			deviceProfileConfig.setEnableFactoryManagementKey(deviceProfileConfigEntity.getEnableFactoryManagementKey());
			deviceProfileConfig.setEnableCustomerManagementKey(deviceProfileConfigEntity.getEnableCustomerManagementKey());
			deviceProfileConfig.setKeyLength(deviceProfileConfigEntity.getKeyLength());
			deviceProfileConfig.setMasterKeyLength(deviceProfileConfigEntity.getMasterKeyLength());
			deviceProfileConfig.setIsMobileId(deviceProfileConfigEntity.getIsMobileId());
			deviceProfileConfig.setIsPlasticId(deviceProfileConfigEntity.getIsPlasticId());
			deviceProfileConfig.setIsRfidCard(deviceProfileConfigEntity.getIsRfidCard());
			deviceProfileConfig.setIsAppletLoadingRequired(deviceProfileConfigEntity.getIsAppletLoadingRequired());
		}
		return deviceProfileConfig;
	}

}
