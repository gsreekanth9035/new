   package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.exception.EntityNotFoundException;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.exception.security.CryptoOperationException;
import com.meetidentity.usermanagement.repository.DeviceTypeActionsMappingRepository;
import com.meetidentity.usermanagement.service.UserDeviceService;
import com.meetidentity.usermanagement.service.UserService;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.feign.client.ApduFeignClient;
import com.meetidentity.usermanagement.web.rest.model.RoleDeviceAction;
import com.meetidentity.usermanagement.web.rest.model.UserDevice;

@Service
@Transactional
public class UserDeviceServiceImpl implements UserDeviceService {

	private final Logger log = LoggerFactory.getLogger(UserDeviceServiceImpl.class);

	@Autowired
	ApduFeignClient apduFeignClient;

	@Autowired
	KeycloakServiceImpl keycloakServiceImpl;

	@Autowired
	PushNotificationDeviceServiceImpl pushNotificationDeviceServiceImpl;
	
	@Autowired
	private DeviceTypeActionsMappingRepository deviceTypeActionsMappingRepository;

	@Autowired
	UserService userServicel;

	@Override
	public ResponseEntity<Void> registerUserDevice(UserDevice userDevice) throws CryptoOperationException, EntityNotFoundException {
		
		ResponseEntity<Void> response = null;
			userDevice.setUserId(userServicel.getUserbyUUID(userDevice.getOrganizationName() ,userDevice.getUuid()).getId());
			try {
				response = apduFeignClient.pairUserDevice(userDevice);
			}catch (Exception cause) {
				log.error(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.UNABLE_TO_REGISTER_USERDEVICE, userDevice.getFriendlyName(), userDevice.getUserId()), cause);
				throw new CryptoOperationException(cause, ErrorMessages.UNABLE_TO_REGISTER_USERDEVICE, userDevice.getFriendlyName(), userDevice.getUserId());
			}
		return response;
	}

	@Override
	public ResponseEntity<String> userKeycloakAuthentication(String userName, String password)  throws IdentityBrokerServiceException {

		/*
		 * UserRepresentation user = keycloakServiceImpl.getUserByUsername(userName);
		 * List<GroupRepresentation> groups =
		 * keycloakServiceImpl.getGroupsByUserID(user.getId());
		 * 
		 * response.getBody().concat("group:"+groups.get(0).getName());
		 */

		ResponseEntity<String> response = keycloakServiceImpl.userLogin(userName, password,"");
		
		return response;
	}

	@Override
	public ResponseEntity<Void> sendUserOtp(String mobileNumber) {
		// TO-DO Validate phone number
		// Get UserEntity using phone number
		UserEntity userEntity = null;
		if (userEntity != null)
			return pushNotificationDeviceServiceImpl.sendSMS(mobileNumber, userEntity);
		else
			return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
	}

	@Override
	public List<RoleDeviceAction> getDeviceActionsByRole(String organizationName, String role) {
		List<RoleDeviceAction> roleDeviceActions = new ArrayList<>();
		List<Object[]> listOfValues = deviceTypeActionsMappingRepository.getAllDevicesActionsByStatusAndRole(organizationName, role);
		listOfValues.forEach(obj -> {
			RoleDeviceAction roleDeviceAction = new RoleDeviceAction();
			
			roleDeviceAction.setRoleName(String.valueOf(obj[0]));
			roleDeviceAction.setDeviceType(String.valueOf(obj[1]));
			roleDeviceAction.setAction(String.valueOf(obj[2]));
			roleDeviceAction.setIsConnected((Boolean) obj[3]);
			roleDeviceAction.setStatus(String.valueOf(obj[4]));
			
			roleDeviceActions.add(roleDeviceAction);
		});
		return roleDeviceActions;
	}

}
