package com.meetidentity.usermanagement.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.w3c.dom.Document;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.config.Constants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserAttributeEntity;
import com.meetidentity.usermanagement.domain.UserBiometricAttributeEntity;
import com.meetidentity.usermanagement.domain.UserBiometricEntity;
import com.meetidentity.usermanagement.domain.UserDrivingLicenseEntity;
import com.meetidentity.usermanagement.domain.UserEmployeeIDEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserHealthIDEntity;
import com.meetidentity.usermanagement.domain.UserHealthServiceEntity;
import com.meetidentity.usermanagement.domain.UserHealthServiceVaccineInfoEntity;
import com.meetidentity.usermanagement.domain.UserNationalIdEntity;
import com.meetidentity.usermanagement.domain.UserPermanentResidentIdEntity;
import com.meetidentity.usermanagement.domain.UserPersonalizedIDEntity;
import com.meetidentity.usermanagement.domain.UserPivEntity;
import com.meetidentity.usermanagement.domain.UserStudentIDEntity;
import com.meetidentity.usermanagement.domain.UserVoterIDEntity;
import com.meetidentity.usermanagement.domain.VehicleCategoryEntity;
import com.meetidentity.usermanagement.domain.VisualTemplateEntity;
import com.meetidentity.usermanagement.domain.WFGroupEntity;
import com.meetidentity.usermanagement.domain.WFMobileIDIdentityIssuanceEntity;
import com.meetidentity.usermanagement.domain.WFStepEntity;
import com.meetidentity.usermanagement.domain.WorkflowEntity;
import com.meetidentity.usermanagement.domain.visual.credentials.dl.UtilityDL;
import com.meetidentity.usermanagement.domain.visual.credentials.eid.UtilityEid;
import com.meetidentity.usermanagement.domain.visual.credentials.hid.UtilityHID;
import com.meetidentity.usermanagement.domain.visual.credentials.hsid.UtilityHSID;
import com.meetidentity.usermanagement.domain.visual.credentials.nid.UtilityNID;
import com.meetidentity.usermanagement.domain.visual.credentials.piv.UtilityPIV;
import com.meetidentity.usermanagement.domain.visual.credentials.prid.UtilityPRID;
import com.meetidentity.usermanagement.domain.visual.credentials.pzid.UtilityPZid;
import com.meetidentity.usermanagement.domain.visual.credentials.sid.UtilitySID;
import com.meetidentity.usermanagement.domain.visual.credentials.vid.UtilityVID;
import com.meetidentity.usermanagement.enums.UserStatusEnum;
import com.meetidentity.usermanagement.exception.EntityNotFoundException;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.keycloak.domain.GroupRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.RoleRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.UserRepresentation;
import com.meetidentity.usermanagement.repository.IdentityTypeRepository;
import com.meetidentity.usermanagement.repository.OrganizationIdentitiesRepository;
import com.meetidentity.usermanagement.repository.OrganizationRepository;
import com.meetidentity.usermanagement.repository.UserDrivingLicenseRepository;
import com.meetidentity.usermanagement.repository.UserEmployeeIDRepository;
import com.meetidentity.usermanagement.repository.UserHealthIDRepository;
import com.meetidentity.usermanagement.repository.UserHealthServiceRepository;
import com.meetidentity.usermanagement.repository.UserLocationRepository;
import com.meetidentity.usermanagement.repository.UserNationalIDRepository;
import com.meetidentity.usermanagement.repository.UserPermanentResidentIdRepository;
import com.meetidentity.usermanagement.repository.UserPersonalizedIDRepository;
import com.meetidentity.usermanagement.repository.UserPivRepository;
import com.meetidentity.usermanagement.repository.UserRepository;
import com.meetidentity.usermanagement.repository.UserStudentIDRepository;
import com.meetidentity.usermanagement.repository.UserVoterIDRepository;
import com.meetidentity.usermanagement.repository.VehicleCategoryRepository;
import com.meetidentity.usermanagement.repository.VisualTemplateRepository;
import com.meetidentity.usermanagement.repository.WorkflowRepository;
import com.meetidentity.usermanagement.service.KeycloakService;
import com.meetidentity.usermanagement.service.UserCredentialsService;
import com.meetidentity.usermanagement.service.UserService;
import com.meetidentity.usermanagement.service.WorkflowService;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.service.helper.UserCredentialsServiceHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.UCard;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserHealthServiceVaccineInfo;
import com.meetidentity.usermanagement.web.rest.model.UserIdentityData;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIDStepIdentityConfig;
import com.meetidentity.usermanagement.web.rest.model.WFStepVisualTemplateConfig;
import com.meetidentity.usermanagement.web.rest.model.Workflow;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStepDetail;


@Service
@Transactional
public class UserCredentialsServiceImpl implements UserCredentialsService {

	private final Logger log = LoggerFactory.getLogger(UserCredentialsServiceImpl.class);
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired 
	WorkflowService workflowService;
	
	@Autowired 
	KeycloakService keycloakService;
	
	@Autowired
	VisualTemplateRepository visualTemplateRepository;
		
	@Autowired
	UserDrivingLicenseRepository userDrivingLicenseRepository;
	
	@Autowired
	UserNationalIDRepository userNationalIDRepository;

	@Autowired
	UserVoterIDRepository userVoterIDRepository;
	
	@Autowired
	UserPermanentResidentIdRepository userPermanentResidentIdRepository;
	
	@Autowired
	UserEmployeeIDRepository userEmployeeIDRepository;

	@Autowired
	UserPersonalizedIDRepository userPersonalizedIDRepository;
	
	@Autowired
	UserStudentIDRepository userStudentIDRepository;
	
	@Autowired
	UserHealthIDRepository userHealthIDRepository;
	
	@Autowired
	VehicleCategoryRepository vehicleCategoryRepository;
	
	@Autowired 
	UserPivRepository userPivRepository;
	
	@Autowired 
	UserHealthServiceRepository userHealthServiceRepository;
	
	@Autowired
	private WorkflowRepository workflowRepository;
	
	@Autowired
	private UserLocationRepository userLocationRepository;
	
	@Autowired
	private OrganizationIdentitiesRepository organizationIdentitiesRepository;
	
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private IdentityTypeRepository identityTypeRepository;
	
	/*
	 * @Autowired private UtilityDL_GTO utilityDL_GTO;
	 */
	@Autowired
	private UtilityDL utilityDL;
	
	@Autowired
	private UtilityPIV utilityPIV;
	
	@Autowired
	private UtilityNID utilityNID;
	
	@Autowired
	private UtilityVID utilityVID;

	@Autowired
	private UtilityPRID utilityPRID;
	
	@Autowired
	private UtilityHID utilityHID;
	
	@Autowired
	private UtilityHSID utilityHSID;	
	
	@Autowired
	private UtilityEid utilityEid;

	@Autowired
	private UtilityPZid utilityPZid;
	
	@Autowired
	private UtilitySID utilitySid;
	
	@Autowired
	private UserCredentialsServiceHelper userCredentialsServiceHelper;
	
	static String documentNumber_prefix = "GOV";
	
	/**
	 * 
	 * @param updatedSVG
	 * @param height
	 * @param width
	 * @return
	 * @throws Exception
	 */
	private String convertSVGtoPNG(Document updatedSVG,float height, float width, String credentialType) throws VisualCredentialException {
		try {
		
			log.debug("convertSVGtoPNG: Start");
			ImageTranscoder imageTranscoder = new PNGTranscoder();
			imageTranscoder.addTranscodingHint(PNGTranscoder.KEY_WIDTH, new Float(width));
			imageTranscoder.addTranscodingHint(PNGTranscoder.KEY_HEIGHT, new Float(height)); 	
			imageTranscoder.addTranscodingHint(PNGTranscoder.KEY_PIXEL_UNIT_TO_MILLIMETER,  25.4f / 600.0f);
		    TranscoderInput transcoderInput = new TranscoderInput(updatedSVG);
		    OutputStream osByteArray = new ByteArrayOutputStream();
		    TranscoderOutput output = new TranscoderOutput(osByteArray);
		    imageTranscoder.transcode(transcoderInput, output);
		    osByteArray.flush();
		    byte[] base64Bytes = Base64.encodeBase64(((ByteArrayOutputStream) osByteArray).toByteArray());
		    osByteArray.close();
		    if(base64Bytes == null) {
		    	log.error(ApplicationConstants.notifyCreate, Util.format(ErrorMessages.BASE64_CONVERSION_OF_PNG_FAILED_FOR_VISUAL_CREDENTIAL, credentialType));
		    	throw new VisualCredentialException( Util.format(ErrorMessages.BASE64_CONVERSION_OF_PNG_FAILED_FOR_VISUAL_CREDENTIAL, credentialType));
		    }else {	
		    	String base64 = new String(base64Bytes);				   
			    log.info(ApplicationConstants.notifyCreate, Util.format(ErrorMessages.VISUAL_CREDENTIAL_CONVERTED_FROM_SVG_TO_PNG,credentialType));				
			    log.debug("convertSVGtoPNG: End");
			    return base64;
		    }
		}catch(TranscoderException | IOException cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL_FROM_SVG_TO_PNG, credentialType), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL_FROM_SVG_TO_PNG, credentialType);
		}
	}

	@Override
	public List<WFStepVisualTemplateConfig> getConfiguredCredentialTypes(String requestFrom, Long userId)
			throws VisualCredentialException {
		List<WFStepVisualTemplateConfig> stepVisualTemplateConfigs = new ArrayList<WFStepVisualTemplateConfig>();
		try {
			UserEntity userEntity = userRepository.findOne(userId);
			if (userEntity != null) {
				String orgName = userEntity.getOrganization().getName();
				String userName = userEntity.getReferenceName();
				stepVisualTemplateConfigs = getWorkflowVisualTemplates(requestFrom, orgName, userName);
			} else {
		    	log.error(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userId));
		    	throw new EntityNotFoundException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userId));
		    }
			return stepVisualTemplateConfigs;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_VISUAL_CREDENTIALS_FOR_USERID, userId),
					cause);
			throw new VisualCredentialException(cause,
					ErrorMessages.FAILED_TO_GET_VISUAL_CREDENTIALS_FOR_USERID, userId);
		}
	}

	@Override
	public List<WFStepVisualTemplateConfig> getWorkflowVisualTemplates(String requestFrom, String orgName, String userName) {
		List<WFStepVisualTemplateConfig> stepVisualTemplateConfigs = new ArrayList<WFStepVisualTemplateConfig>();
		
		Map<Class<?>, Object> map = getGroupAndWorkflowByUserName(orgName, userName);
		String referenceGroupId =  ((GroupRepresentation) map.get(GroupRepresentation.class)).getId();
		String workflowName =  ((Workflow) map.get(Workflow.class)).getName();
		
		WorkflowEntity workflowEntity = workflowRepository.findByName(orgName, workflowName);
		if (workflowEntity != null ) {
			if (requestFrom.equalsIgnoreCase("web_portal_print") || requestFrom.equalsIgnoreCase("web_portal_enroll_summary")) {
				workflowEntity.getWfGroups().stream().forEach(wfGroup -> {
					if (wfGroup.getReferenceGroupId().equalsIgnoreCase(referenceGroupId)) {
						if (wfGroup.getVisualTemplate() != null) {
							VisualTemplateEntity visualTemplate = visualTemplateRepository.getOne(wfGroup.getVisualTemplate().getId());

							WFStepVisualTemplateConfig wfStepVisualTemplateConfig = new WFStepVisualTemplateConfig();
							wfStepVisualTemplateConfig.setId(visualTemplate.getId());
							wfStepVisualTemplateConfig.setVisualTemplateID(visualTemplate.getId());
							wfStepVisualTemplateConfig.setVisualTemplateName(visualTemplate.getName());
							wfStepVisualTemplateConfig.setFrontOrientation(visualTemplate.getFrontOrientation());
							wfStepVisualTemplateConfig.setBackOrientation(visualTemplate.getBackOrientation());
							wfStepVisualTemplateConfig.setIdentityTypeName(visualTemplate.getOrgIdentityType().getIdentityType().getName());
							
							stepVisualTemplateConfigs.add(wfStepVisualTemplateConfig);
						}
					}
				});
			}  else {
				WorkflowStepDetail workflowStepDetail = workflowService.getStepDetails(orgName, workflowName,"MOBILE_ID_IDENTITY_ISSUANCE");
				List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdConfig = workflowStepDetail.getWfMobileIDStepIdentityConfigs();
				if (wfMobileIDStepIdConfig != null) {
					wfMobileIDStepIdConfig.forEach(wfMobileIdStep -> {
						wfMobileIdStep.getWfMobileIdStepVisualIDGroupConfigs().forEach(visualIdGroupConfig -> {
							if (visualIdGroupConfig.getReferenceGroupId().equalsIgnoreCase(referenceGroupId)) {
								VisualTemplateEntity visualTemplate = visualTemplateRepository.getOne(visualIdGroupConfig.getVisualTemplateId());
								WFStepVisualTemplateConfig wfStepVisualTemplateConfig = new WFStepVisualTemplateConfig();
								wfStepVisualTemplateConfig.setId(visualIdGroupConfig.getId());
								wfStepVisualTemplateConfig.setWorkflowMobileIdentityIssuanceId(visualIdGroupConfig.getWorkflowMobileIdentityIssuanceId());
								wfStepVisualTemplateConfig.setVisualTemplateID(visualTemplate.getId());
								wfStepVisualTemplateConfig.setVisualTemplateName(visualTemplate.getName());
								wfStepVisualTemplateConfig.setFrontOrientation(visualTemplate.getFrontOrientation());
								wfStepVisualTemplateConfig.setBackOrientation(visualTemplate.getBackOrientation());
								wfStepVisualTemplateConfig.setIdentityTypeName(visualTemplate.getOrgIdentityType().getIdentityType().getName());
								stepVisualTemplateConfigs.add(wfStepVisualTemplateConfig);
							}
						});
					});
				}
			
			}
		} else {
			log.error(ApplicationConstants.notifySearch, Util.format(ErrorMessages.FAILED_TO_GET_WORKFLOW_FOR_USERID, userName));
	    	throw new EntityNotFoundException(Util.format(ErrorMessages.FAILED_TO_GET_WORKFLOW_FOR_USERID, userName));
		}
	  return stepVisualTemplateConfigs;
	}
	
	@Override
	public Map<Class<?>, Object> getGroupAndWorkflowByUserName(String organizationName, String userName) throws EntityNotFoundException {
		List<String> workflowNames = new ArrayList<String>();
		Map<Class<?>, Object> map = new HashMap<>();
		try {
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(userName, organizationName);
			if (userRepresentation != null) {
				List<GroupRepresentation> groupRepresentations = keycloakService.getGroupsByUserID(userRepresentation.getId(), organizationName);
				if (groupRepresentations != null) {
					groupRepresentations.stream().forEach(groupRepresentation -> {
						List<RoleRepresentation> roleRepresentations = keycloakService.getRolesByGroupID(groupRepresentation.getId(), organizationName);
						if (roleRepresentations != null) {
							roleRepresentations.stream().forEach(roleRepresentation -> {
								if (roleRepresentation.getName().endsWith("WORKFLOW")
										|| roleRepresentation.getName().endsWith("workflow")
										|| roleRepresentation.getName().endsWith("Workflow")) {
									Workflow workflow = workflowService.getWorkflowByRole(organizationName, roleRepresentation.getName());
									workflowNames.add(workflow.getName());
									map.put(GroupRepresentation.class, groupRepresentation);
									map.put(Workflow.class, workflow);
								}
							});

						} /*else {
							log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.ROLE_NOT_FOUND_WITH_GORUPID,
									groupRepresentation.getId()));
							throw new EntityNotFoundException(Util.format(ErrorMessages.ROLE_NOT_FOUND_WITH_GORUPID,
									groupRepresentation.getId()));
						}*/
					});
				} else {
					log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.GROUP_NOT_FOUND_FOR_USERID,
							userRepresentation.getId()));
					throw new EntityNotFoundException(
							Util.format(ErrorMessages.GROUP_NOT_FOUND_FOR_USERID, userRepresentation.getId()));
				}
				if (workflowNames.isEmpty()) {
					log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_USER,
							userName));
					throw new EntityNotFoundException(
							Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_USER, userName));
				}
			} else {
				log.info(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, userName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, userName));
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.ENTITY_NOT_FOUND, cause);
			throw new EntityNotFoundException(ErrorMessages.ENTITY_NOT_FOUND, cause);
		}
		return map;
	}

	@Override
	public UCard getUserCredentialsWithTemplateID(String requestFrom, Long userId, String credentialTemplateID,String userData) throws VisualCredentialException {
		UCard uCard = new UCard();
		try{
			if(userId != null) {
				UserEntity userEntity = userRepository.findOne(userId);				
				Boolean transparentPhoto = false;
				
				if(userEntity != null && userEntity.getUserBiometrics() != null) {
					List<UserBiometricEntity> userBiometrics = userEntity.getUserBiometrics().stream().filter(e -> e.getType().equalsIgnoreCase("FACE")).collect(Collectors.toList());
					if(userBiometrics != null && userBiometrics.size() > 0) {
						if (userBiometrics.get(0).getUserBiometricAttributes() != null) {
							List<UserBiometricAttributeEntity> userBiometricAttibutes = userBiometrics.get(0)
									.getUserBiometricAttributes().stream()
									.filter(e -> e.getName().equalsIgnoreCase(Constants.TRANSPARENT_PHOTO_ALLOWED))
									.collect(Collectors.toList());
							if(userBiometricAttibutes.size() > 0) {
								transparentPhoto = Boolean.parseBoolean(userBiometricAttibutes.get(0).getValue());
							}
						}
					}
				}
				
				uCard = generateVisualCredential(requestFrom, userEntity, userEntity.getReferenceName(), userEntity.getOrganization().getName(), credentialTemplateID, new User(), transparentPhoto);
				// WfMobileIDStepDetails userService.getUserWfMobileIDStepDetails(userEntity.getId());
			}else {
		    	log.error(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userId));
		    	throw new EntityNotFoundException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userId));
		    }
			return uCard;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIALS_FOR_USERID, userId),
					cause);
			throw new VisualCredentialException(cause,
					ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIALS_FOR_USERID, userId);
		}		
	}
	private UserIdentityData setUserDataFields(String labelName, Object value) {
		UserIdentityData userIdentityData = new UserIdentityData();
		userIdentityData.setLabelName(labelName);
		userIdentityData.setValue(value);
		return userIdentityData;
	}
	@Override
	public UCard generateVisualCredential(String requestFrom, UserEntity userEntity, String userName, String organizationName, String credentialTemplateID, User user, boolean transparentPhoto) throws VisualCredentialException{
		UCard uCard = new UCard();
		try {
			user.setIsTransparentPhotoRequired(transparentPhoto);
			VisualTemplateEntity visualTemplateEntity = visualTemplateRepository
					.findOne(Long.parseLong(credentialTemplateID));
			byte[] svgFrontData = visualTemplateEntity.getFrontDesign();
			byte[] svgBackData = visualTemplateEntity.getBackDesign();
			byte[] jsonData = visualTemplateEntity.getAttributeMapping();
			UserAttributeEntity userAttributeEntity = null;
			UserAttribute userAttribute = null;
			int status = 4;
			// Get User Visual Credential
			String parser = XMLResourceDescriptor.getXMLParserClassName();
			SAXSVGDocumentFactory sAXSVGDocumentFactory = new SAXSVGDocumentFactory(parser);
			List<UserIdentityData> userIdentityDataList  = new ArrayList<>();
			if (userEntity == null ) { 
				if( user != null && user.getId() != null) {			
					userEntity = userRepository.findOne(user.getId());
					setUserAttributeData(userIdentityDataList, userEntity);
				}else if( userName != null) {
					userEntity = userRepository.getUserByName(userName, organizationName);
					setUserAttributeData(userIdentityDataList, userEntity);
				}
			}else {
				setUserAttributeData(userIdentityDataList, userEntity);
			}
				
				//status =	"Active";
			

			OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);
			
			
			String visualTemplateType = visualTemplateEntity.getOrgIdentityType().getIdentityType().getName();
			switch (visualTemplateType) {

			case Constants.Driving_License:
				UserDrivingLicenseEntity userDrivingLicenseEntity = null;
				List<VehicleCategoryEntity> vc = null;
				switch (requestFrom) {

					case "web_portal_enroll_summary":
					case "web_portal_summary":
					case "web_portal_approval_summary":
						userDrivingLicenseEntity = userDrivingLicenseRepository.findByUserID(userEntity.getId());
						break;					
					case "web_portal_print":
					case "Request_From_Mobile":
						userDrivingLicenseEntity = issueDrivingLicenseEntity(organizationName, credentialTemplateID, user, userEntity, visualTemplateType);
						uCard.setIdentitySerialNumber(userDrivingLicenseEntity.getLicenseNumber());
						break;
					default :
					
				}
				if (userDrivingLicenseEntity != null) {
					uCard.setUserIdentityData(setDrivingLicenceData(userIdentityDataList, userDrivingLicenseEntity));
				} else {
					uCard.setUserIdentityData(null);
				}
				if (userEntity != null && userDrivingLicenseEntity != null) { 
					uCard.setUserData(new Gson().toJson(user));
					uCard.setQrCodeData(java.util.Base64.getEncoder()
							.encodeToString((new QRCodeHelper().getQRCodeImage(
									organizationName + "#" + userEntity.getId() + "#" + credentialTemplateID + '#' + userDrivingLicenseEntity.getLicenseNumber(),
									BarcodeFormat.QR_CODE, 300, 300, status))));
				}
				
				if(user.getId() != null) {
					vc = vehicleCategoryRepository.findByVehicleClassificationID(user.getUserDrivingLicense().getVehicle_classification_id());
					
				}else if ( userEntity != null) {
					vc = vehicleCategoryRepository.findByVehicleClassificationID(userEntity.getUserDrivingLicenses().get(0).getVehicleClassification().getId());
					
				}
				
				if (vc == null || vc.isEmpty()) {
					vc = vehicleCategoryRepository.findByVehicleClassificationType(user.getUserDrivingLicense().getVehicleClass());
				}
				if (vc == null || vc.isEmpty()) {
					vc = vehicleCategoryRepository.findByVehicleClassificationType(userDrivingLicenseEntity.getVehicleClass());
				}

				{
					if (requestFrom.equalsIgnoreCase("web_portal_print")) {
						uCard.setFront(convertSVGtoPNG(utilityDL.parseDLFront(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData,jsonData, userDrivingLicenseEntity, vc, visualTemplateType, false),(float) 883, (float) 1389, visualTemplateType));
						uCard.setBack(convertSVGtoPNG(utilityDL.parseDLBack(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, credentialTemplateID,svgBackData, jsonData, userDrivingLicenseEntity, vc, visualTemplateType, false),(float) 863, (float) 1353, visualTemplateType));
					} else if (requestFrom.equalsIgnoreCase("Request_From_Mobile")) { 
						uCard.setFront(convertSVGtoPNG(utilityDL.parseDLFront(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData,jsonData, userDrivingLicenseEntity, vc, visualTemplateType, false),(float) 1389, (float) 883, visualTemplateType));
						uCard.setBack(convertSVGtoPNG(utilityDL.parseDLBack(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, credentialTemplateID,svgBackData, jsonData, userDrivingLicenseEntity, vc, visualTemplateType, false),(float) 1389, (float) 883, visualTemplateType));
					} else {
						uCard.setFront(convertSVGtoPNG(utilityDL.parseDLFront(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData,jsonData, userDrivingLicenseEntity, vc, visualTemplateType, false),(float) 600, (float) 1100, visualTemplateType));
						uCard.setBack(convertSVGtoPNG(utilityDL.parseDLBack(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, credentialTemplateID,svgBackData, jsonData, userDrivingLicenseEntity, vc, visualTemplateType, false),(float) 600, (float) 1100, visualTemplateType));
					}
				}
				break;
			case Constants.PIV_and_PIV_1:
				UserPivEntity userPivEntity = null;
				switch (requestFrom) {

					case "web_portal_enroll_summary":
					case "web_portal_summary":
					case "web_portal_approval_summary":
						userPivEntity = userPivRepository.findByUserID(userEntity.getId());
						break;					
					case "web_portal_print":
					case "Request_From_Mobile":
						userPivEntity = issuePivEntity(organizationName, credentialTemplateID, user, userEntity, visualTemplateType);
						uCard.setIdentitySerialNumber(userPivEntity.getPivIdNumber());
						break;
					default :
					
				}
				
				uCard.setFront(convertSVGtoPNG(utilityPIV.parsePIVFront(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, userPivEntity, credentialTemplateID, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));
				uCard.setBack(convertSVGtoPNG(utilityPIV.parsePIVBack(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgBackData, jsonData, userPivEntity, visualTemplateType, false), (float) 638, (float) 1011, visualTemplateType));
				
				if (userPivEntity != null) {
					uCard.setUserIdentityData(setPIVData(userIdentityDataList, userPivEntity));
				} else {
					uCard.setUserIdentityData(null);
				}

				if (userEntity != null && userPivEntity != null) { 
					uCard.setUserData(new Gson().toJson(user));
					uCard.setQrCodeData(java.util.Base64.getEncoder()
							.encodeToString((new QRCodeHelper().getQRCodeImage(
									organizationName + "#" + userEntity.getId() + "#" + credentialTemplateID + '#' + userPivEntity.getPivIdNumber(),
									BarcodeFormat.QR_CODE, 300, 300, status))));
				}
				break;

			case Constants.National_ID:
				
				UserNationalIdEntity userNationalIdEntity = null;
				switch (requestFrom) {

					case "web_portal_enroll_summary":
					case "web_portal_summary":
					case "web_portal_approval_summary":
						userNationalIdEntity = userNationalIDRepository.findByUserID(userEntity.getId());
						break;					
					case "web_portal_print":
					case "Request_From_Mobile":
						userNationalIdEntity = issueNIDEntity(organizationName, credentialTemplateID, user, userEntity, visualTemplateType);
						uCard.setIdentitySerialNumber(userNationalIdEntity.getPersonalCode());
						break;
					default :
					
				}
				
				if (userNationalIdEntity != null) {
					uCard.setUserIdentityData(setNIDData(userIdentityDataList, userNationalIdEntity));
				} else {
					uCard.setUserIdentityData(null);
				}
				if (requestFrom.equalsIgnoreCase("web_portal_print")) {
					uCard.setFront(convertSVGtoPNG( utilityNID.parseNIDFront(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, userNationalIdEntity, credentialTemplateID, visualTemplateType, false), (float) 883, (float) 1389, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityNID.parseNIDBack(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgBackData, jsonData, userNationalIdEntity, credentialTemplateID, visualTemplateType, false),(float) 883, (float) 1389, visualTemplateType));
				} else if (requestFrom.equalsIgnoreCase("Request_From_Mobile")) {
					uCard.setFront(convertSVGtoPNG(	utilityNID.parseNIDFront(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, userNationalIdEntity, credentialTemplateID, visualTemplateType, false),(float) 1389, (float) 883, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityNID.parseNIDBack(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgBackData, jsonData, userNationalIdEntity, credentialTemplateID, visualTemplateType, false),(float) 1389, (float) 883, visualTemplateType));
				}else {
					uCard.setFront(convertSVGtoPNG(	utilityNID.parseNIDFront(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, userNationalIdEntity, credentialTemplateID, visualTemplateType, false),(float) 600, (float) 1100, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityNID.parseNIDBack(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgBackData, jsonData, userNationalIdEntity, credentialTemplateID, visualTemplateType, false),(float) 600, (float) 1100, visualTemplateType));
				}

				if (userEntity != null && userNationalIdEntity != null) { 
					uCard.setUserData(new Gson().toJson(user));
					uCard.setQrCodeData(java.util.Base64.getEncoder()
							.encodeToString((new QRCodeHelper().getQRCodeImage(
									organizationName + "#" + userEntity.getId() + "#" + credentialTemplateID + '#' + userNationalIdEntity.getPersonalCode(),
									BarcodeFormat.QR_CODE, 300, 300, status))));
				}
				break;
				
			case Constants.Voter_ID:
				
				UserVoterIDEntity userVoterIDEntity = null;
				switch (requestFrom) {

					case "web_portal_enroll_summary":
					case "web_portal_summary":
					case "web_portal_approval_summary":
						userVoterIDEntity = userVoterIDRepository.findByUserID(userEntity.getId());
						break;					
					case "web_portal_print":
					case "Request_From_Mobile":
						userVoterIDEntity = issueVIDEntity(organizationName, credentialTemplateID, user, userEntity, visualTemplateType);
						uCard.setIdentitySerialNumber(userVoterIDEntity.getCurp());
						break;
					default :
					
				}
				
				if (userVoterIDEntity != null) {
					uCard.setUserIdentityData(setVIDData(userIdentityDataList, userVoterIDEntity));
				} else {
					uCard.setUserIdentityData(null);
				}
				if (requestFrom.equalsIgnoreCase("web_portal_print")) {
					uCard.setFront(convertSVGtoPNG( utilityVID.parseVIDFront(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, userVoterIDEntity, credentialTemplateID, visualTemplateType, false), (float) 883, (float) 1389, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityVID.parseVIDBack(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgBackData, jsonData, userVoterIDEntity, credentialTemplateID, visualTemplateType, false),(float) 883, (float) 1389, visualTemplateType));
				} else if (requestFrom.equalsIgnoreCase("Request_From_Mobile")) {
					uCard.setFront(convertSVGtoPNG(	utilityVID.parseVIDFront(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, userVoterIDEntity, credentialTemplateID, visualTemplateType, false),(float) 1389, (float) 883, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityVID.parseVIDBack(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgBackData, jsonData, userVoterIDEntity, credentialTemplateID, visualTemplateType, false),(float) 1389, (float) 883, visualTemplateType));
				}else {
					uCard.setFront(convertSVGtoPNG(	utilityVID.parseVIDFront(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, userVoterIDEntity, credentialTemplateID, visualTemplateType, false),(float) 600, (float) 1100, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityVID.parseVIDBack(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgBackData, jsonData, userVoterIDEntity, credentialTemplateID, visualTemplateType, false),(float) 600, (float) 1100, visualTemplateType));
				}

				if (userEntity != null && userVoterIDEntity != null) { 
					uCard.setUserData(new Gson().toJson(user));
					uCard.setQrCodeData(java.util.Base64.getEncoder()
							.encodeToString((new QRCodeHelper().getQRCodeImage(
									organizationName + "#" + userEntity.getId() + "#" + credentialTemplateID + '#' + userVoterIDEntity.getCurp(),
									BarcodeFormat.QR_CODE, 300, 300, status))));
				}
				break;
			case Constants.Permanent_Resident_ID:
				
				UserPermanentResidentIdEntity userPermanentResidentIdEntity = null;
				switch (requestFrom) {

					case "web_portal_enroll_summary":
					case "web_portal_summary":
					case "web_portal_approval_summary":
						userPermanentResidentIdEntity = userPermanentResidentIdRepository.findByUserID(userEntity.getId());
						break;					
					case "web_portal_print":
					case "Request_From_Mobile":
						userPermanentResidentIdEntity = issuePRIDEntity(organizationName, credentialTemplateID, user, userEntity, visualTemplateType);
						uCard.setIdentitySerialNumber(userPermanentResidentIdEntity.getPersonalCode());
						break;
					default :
					
				}
				
				if (userPermanentResidentIdEntity != null) {
					uCard.setUserIdentityData(setPRIDData(userIdentityDataList, userPermanentResidentIdEntity));
				} else {
					uCard.setUserIdentityData(null);
				}
				if (requestFrom.equalsIgnoreCase("web_portal_print")) {
					uCard.setFront(convertSVGtoPNG( utilityPRID.parsePRIDFront(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, userPermanentResidentIdEntity, credentialTemplateID, visualTemplateType, false), (float) 883, (float) 1389, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityPRID.parsePRIDBack(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgBackData, jsonData, userPermanentResidentIdEntity, credentialTemplateID, visualTemplateType, false),(float) 883, (float) 1389, visualTemplateType));
				} else if (requestFrom.equalsIgnoreCase("Request_From_Mobile")) {
					uCard.setFront(convertSVGtoPNG(	utilityPRID.parsePRIDFront(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, userPermanentResidentIdEntity, credentialTemplateID, visualTemplateType, false),(float) 1389, (float) 883, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityPRID.parsePRIDBack(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgBackData, jsonData, userPermanentResidentIdEntity, credentialTemplateID, visualTemplateType, false),(float) 1389, (float) 883, visualTemplateType));
				} else {
					uCard.setFront(convertSVGtoPNG(	utilityPRID.parsePRIDFront(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, userPermanentResidentIdEntity, credentialTemplateID, visualTemplateType, false),(float) 600, (float) 1100, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityPRID.parsePRIDBack(requestFrom,sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgBackData, jsonData, userPermanentResidentIdEntity, credentialTemplateID, visualTemplateType, false),(float) 600, (float) 1100, visualTemplateType));
				}

				if (userEntity != null && userPermanentResidentIdEntity != null) { 
					uCard.setUserData(new Gson().toJson(user));
					uCard.setQrCodeData(java.util.Base64.getEncoder()
							.encodeToString((new QRCodeHelper().getQRCodeImage(
									organizationName + "#" + userEntity.getId() + "#" + credentialTemplateID + '#' + userPermanentResidentIdEntity.getPersonalCode(),
									BarcodeFormat.QR_CODE, 300, 300, status))));
				}
				break;
			case Constants.Health_ID:

				UserHealthIDEntity userHealthIDEntity = null;
				switch (requestFrom) {

					case "web_portal_enroll_summary":
					case "web_portal_summary":
					case "web_portal_approval_summary":
						userHealthIDEntity = userHealthIDRepository.findByUserID(userEntity.getId());
						break;					
					case "web_portal_print":
					case "Request_From_Mobile":
						userHealthIDEntity = issueHealthIDEntity(organizationName, credentialTemplateID, user, userEntity, visualTemplateType);
						uCard.setIdentitySerialNumber(userHealthIDEntity.getIdNumber());
						break;
					default :
					
				}
				if (userHealthIDEntity != null) {
					uCard.setUserIdentityData(setHealthIdData(userIdentityDataList, userHealthIDEntity));
				} else {
					uCard.setUserIdentityData(null);
				}
				
				
				if (requestFrom.equalsIgnoreCase("web_portal_print")) {
					uCard.setFront(convertSVGtoPNG(utilityHID.parseHIDFront(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user,  svgFrontData, jsonData, credentialTemplateID, userHealthIDEntity, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityHID.parseHIDBack(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user,  credentialTemplateID, svgBackData, jsonData, userHealthIDEntity, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));
					
				} else {
					uCard.setFront(convertSVGtoPNG(utilityHID.parseHIDFront(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user,  svgFrontData, jsonData, credentialTemplateID, userHealthIDEntity, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityHID.parseHIDBack(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user,  credentialTemplateID, svgBackData, jsonData, userHealthIDEntity, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));
				}

				if (userEntity != null && userHealthIDEntity != null) { 
					uCard.setUserData(new Gson().toJson(user));
					uCard.setQrCodeData(java.util.Base64.getEncoder()
							.encodeToString((new QRCodeHelper().getQRCodeImage(
									organizationName + "#" + userEntity.getId() + "#" + credentialTemplateID + '#' + userHealthIDEntity.getIdNumber(),
									BarcodeFormat.QR_CODE, 300, 300, status))));
				}
				break;
			case Constants.Health_Service_ID:

				UserHealthServiceEntity userHealthServiceEntity = null;
				
				if (user.getId() != null) {
					userHealthServiceEntity = generateHSID(organizationName, null, credentialTemplateID, user, userEntity, visualTemplateType);
					
					uCard.setUserIdentityData(setHealthServiceIdData(userIdentityDataList, userHealthServiceEntity));
				} else {
					// TODO get details from org / need discussion
					userHealthServiceEntity = generateHSID(organizationName, null, credentialTemplateID, user, userEntity, visualTemplateType);
					uCard.setUserIdentityData(null);
				}
				
				
				if (requestFrom.equalsIgnoreCase("web_portal_print")) {
					uCard.setFront(convertSVGtoPNG(utilityHSID.parseHSIDFront(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, status, credentialTemplateID, svgFrontData, jsonData, userHealthServiceEntity, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityHSID.parseHSIDBack(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, status, credentialTemplateID, svgBackData, jsonData, userHealthServiceEntity, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));
					
				} else {
					uCard.setFront(convertSVGtoPNG(utilityHSID.parseHSIDFront(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, status, credentialTemplateID, svgFrontData, jsonData, userHealthServiceEntity, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));
					uCard.setBack(convertSVGtoPNG(utilityHSID.parseHSIDBack(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, status, credentialTemplateID, svgBackData, jsonData, userHealthServiceEntity, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));
				}

				if (userEntity != null && userHealthServiceEntity != null) { 
					uCard.setUserData(new Gson().toJson(user));
					uCard.setQrCodeData(java.util.Base64.getEncoder()
							.encodeToString((new QRCodeHelper().getQRCodeImage(
									organizationName + "#" + userEntity.getId() + "#" + credentialTemplateID + '#' + userHealthServiceEntity.getIdNumber(),
									BarcodeFormat.QR_CODE, 300, 300, status))));
				}
				break;
			case Constants.Student_ID:
				UserStudentIDEntity userStudentIDEntity = null;
				switch (requestFrom) {

					case "web_portal_enroll_summary":
					case "web_portal_summary":
					case "web_portal_approval_summary":
						userStudentIDEntity = userStudentIDRepository.findByUserID(userEntity.getId());
						break;					
					case "web_portal_print":
					case "Request_From_Mobile":
						userStudentIDEntity = issueStudentIDEntity(organizationName, credentialTemplateID, user, userEntity, visualTemplateType);
						uCard.setIdentitySerialNumber(userStudentIDEntity.getStudentIdNumber());
						break;
					default :
					
				}
				if (userStudentIDEntity != null) {
					uCard.setUserIdentityData(setStudentIdData(userIdentityDataList, userStudentIDEntity));
				} else {
					uCard.setUserIdentityData(null);
				}
				uCard.setFront(convertSVGtoPNG(utilitySid.parseSidFront(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, credentialTemplateID, userStudentIDEntity, visualTemplateType, false), (float) 638, (float) 1011, visualTemplateType));
				uCard.setBack(convertSVGtoPNG(utilitySid.parseSidBack(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, credentialTemplateID, svgBackData, jsonData, userStudentIDEntity, visualTemplateType, false), (float) 638, (float) 1011, visualTemplateType));
				

				if (userEntity != null && userStudentIDEntity != null) { 
					uCard.setUserData(new Gson().toJson(user));
					uCard.setQrCodeData(java.util.Base64.getEncoder()
							.encodeToString((new QRCodeHelper().getQRCodeImage(
									organizationName + "#" + userEntity.getId() + "#" + credentialTemplateID + '#' + userStudentIDEntity.getStudentIdNumber(),
									BarcodeFormat.QR_CODE, 300, 300, status))));
				}
				break;
			case Constants.Employee_ID:
				UserEmployeeIDEntity userEmployeeIDEntity = null;
				switch (requestFrom) {

					case "web_portal_enroll_summary":
					case "web_portal_summary":
					case "web_portal_approval_summary":
						userEmployeeIDEntity = userEmployeeIDRepository.findByUserID(userEntity.getId());
						break;					
					case "web_portal_print":
					case "Request_From_Mobile":
						userEmployeeIDEntity = issueEmployeeIDEntity(organizationName, credentialTemplateID, user, userEntity, visualTemplateType);
						uCard.setIdentitySerialNumber(userEmployeeIDEntity.getEmployeeIDNumber());
						break;
					default :
					
				}
				if (userEmployeeIDEntity != null) {
					uCard.setUserIdentityData(setEmployeeIdData(userIdentityDataList, userEmployeeIDEntity));
				} else {
					uCard.setUserIdentityData(null);
				}
				uCard.setFront(convertSVGtoPNG(utilityEid.parseEidFront(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, credentialTemplateID, userEmployeeIDEntity, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));
				uCard.setBack(convertSVGtoPNG(utilityEid.parseEidBack(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, credentialTemplateID, svgBackData, jsonData, userEmployeeIDEntity, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));

				if (userEntity != null && userEmployeeIDEntity != null) { 
					uCard.setUserData(new Gson().toJson(user));
					uCard.setQrCodeData(java.util.Base64.getEncoder()
							.encodeToString((new QRCodeHelper().getQRCodeImage(
									organizationName + "#" + userEntity.getId() + "#" + credentialTemplateID + '#' + userEmployeeIDEntity.getEmployeeIDNumber(),
									BarcodeFormat.QR_CODE, 300, 300, status))));
				}
				
				break;
			default:
				UserPersonalizedIDEntity userPersonalizedIDEntity = null;
				switch (requestFrom) {

					case "web_portal_enroll_summary":
					case "web_portal_summary":
					case "web_portal_approval_summary":
						userPersonalizedIDEntity = userPersonalizedIDRepository.findByUserID(userEntity.getId());
						break;					
					case "web_portal_print":
					case "Request_From_Mobile":
						userPersonalizedIDEntity = issuePersonalizedIDEntity(organizationName, credentialTemplateID, user, userEntity, visualTemplateType);
						uCard.setIdentitySerialNumber(userPersonalizedIDEntity.getIdNumber());
						break;
					default :
					
				}
				if (userPersonalizedIDEntity != null) {
					uCard.setUserIdentityData(setPersonalizedIdData(userIdentityDataList, userPersonalizedIDEntity));
				} else {
					uCard.setUserIdentityData(null);
				}
				uCard.setFront(convertSVGtoPNG(utilityPZid.parsePZidFront(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, svgFrontData, jsonData, credentialTemplateID, userPersonalizedIDEntity, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));
				uCard.setBack(convertSVGtoPNG(utilityPZid.parsePZidBack(requestFrom, sAXSVGDocumentFactory, organizationEntity, userEntity, user, credentialTemplateID, svgBackData, jsonData, userPersonalizedIDEntity, visualTemplateType, false), (float) 1011, (float) 638, visualTemplateType));

				if (userEntity != null && userPersonalizedIDEntity != null) { 
					uCard.setUserData(new Gson().toJson(user));
					uCard.setQrCodeData(java.util.Base64.getEncoder()
							.encodeToString((new QRCodeHelper().getQRCodeImage(
									organizationName + "#" + userEntity.getId() + "#" + credentialTemplateID + '#' + userPersonalizedIDEntity.getIdNumber(),
									BarcodeFormat.QR_CODE, 300, 300, status))));
				}
				
				break;
			}

			uCard.setTemplateName(visualTemplateEntity.getName());
			uCard.setIdentityTypeName(visualTemplateEntity.getOrgIdentityType().getIdentityType().getName());
			// user.setUserBiometrics(new ArrayList<UserBiometric>());
								

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIALS_FOR_CREDENTIALTEMPLATEID,credentialTemplateID),cause);
			throw new VisualCredentialException(cause,ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIALS_FOR_CREDENTIALTEMPLATEID,credentialTemplateID);
		}

		if (uCard.getFront() == null || uCard.getBack() == null) {
			log.error(ApplicationConstants.notifyCreate,Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIALS_FOR_CREDENTIALTEMPLATEID,credentialTemplateID));
			throw new VisualCredentialException(Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIALS_FOR_CREDENTIALTEMPLATEID,credentialTemplateID));
		} else {

			return uCard;
		}

	}
	
	private List<UserIdentityData> setUserAttributeData(List<UserIdentityData> userIdentityDataList, UserEntity userEntity) {
		UserAttributeEntity userAttributeEntity = userEntity.getUserAttribute();
    	if( userAttributeEntity.getNationality() != null) {
    		userIdentityDataList.add(setUserDataFields(Constants.Nationality, userAttributeEntity.getNationality()));
    	}
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Birth, Util.formatDate(userAttributeEntity.getDateOfBirth(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.Email, userAttributeEntity.getEmail()));
		userIdentityDataList.add(setUserDataFields(Constants.Gender, userAttributeEntity.getGender()));
		// TODO add address obj data
		String address = userCredentialsServiceHelper.getAddressFromAddressEntity_addressLine1(userEntity.getUserAddress())+userCredentialsServiceHelper.getAddressFromAddressEntity_addressLine2(userEntity.getUserAddress());
		if(address == null) {
			userIdentityDataList.add(setUserDataFields(Constants.Address, userAttributeEntity.getAddress()));
		} else {
			userIdentityDataList.add(setUserDataFields(Constants.Address, address));
		}
		return userIdentityDataList;
	}
	private List<UserIdentityData> setUserAttributeData(List<UserIdentityData> userIdentityDataList, User user) {
		UserAttribute userAttribute = user.getUserAttribute();
		userIdentityDataList.add(setUserDataFields(Constants.Nationality, userAttribute.getNationality()));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Birth, Util.formatDate(userAttribute.getDateOfBirth(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.Email, userAttribute.getEmail()));
		userIdentityDataList.add(setUserDataFields(Constants.Gender, userAttribute.getGender()));
		// TODO add address obj data
		String address = userCredentialsServiceHelper.getAddressFromAddressObj_addressLine1(user.getUserAddress())+userCredentialsServiceHelper.getAddressFromAddressObj_addressLine2(user.getUserAddress());
		if(address == null) {
			userIdentityDataList.add(setUserDataFields(Constants.Address, userAttribute.getAddress()));
		} else {
			userIdentityDataList.add(setUserDataFields(Constants.Address, address));
		}
		return userIdentityDataList;
	}
	
	private List<UserIdentityData> setDrivingLicenceData(List<UserIdentityData> userIdentityDataList, UserDrivingLicenseEntity dl) {
		userIdentityDataList.add(setUserDataFields(Constants.License_Number, dl.getLicenseNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Document_Descriminator, dl.getDocumentDescriminator()));
		userIdentityDataList.add(setUserDataFields(Constants.Issuing_Authority, dl.getIssuingAuthority()));
		userIdentityDataList.add(setUserDataFields(Constants.License_Category, dl.getLicenseCategory()));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Issuance, Util.formatDate(dl.getDateOfIssuance(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Expiry, Util.formatDate(dl.getDateOfExpiry(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.Restrictions, dl.getRestrictions().getType()));
		userIdentityDataList.add(setUserDataFields(Constants.Endorsements, dl.getEndorsements()));
		userIdentityDataList.add(setUserDataFields(Constants.Vehicle_Class, dl.getVehicleClass()));
		//userIdentityDataList.add(setUserDataFields(Constants.Under18Until, Util.formatDate(dl.getUnder18Until(), Util.MMDDYYYY_format)));
		//userIdentityDataList.add(setUserDataFields(Constants.Under19Until, Util.formatDate(dl.getUnder19Until(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.Under21Until, Util.formatDate(dl.getUnder21Until(), Util.MMDDYYYY_format)));
		///userIdentityDataList.add(setUserDataFields(Constants.Audit_Information, dl.getAuditInformation()));
		return userIdentityDataList;
	}
	private List<UserIdentityData> setPIVData(List<UserIdentityData> userIdentityDataList, UserPivEntity userPivEntity) {
		userIdentityDataList.add(setUserDataFields(Constants.Piv_Number, userPivEntity.getPivIdNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Rank, userPivEntity.getRank()));
		userIdentityDataList.add(setUserDataFields(Constants.Affiliation, userPivEntity.getEmployeeAffiliation()));
		//userIdentityDataList.add(setUserDataFields(Constants.Affiliation_Optional_Line, userPivEntity.getAffiliationOptionalLine()));
		userIdentityDataList.add(setUserDataFields(Constants.Affiliation_Color, userPivEntity.getEmployeeAffiliationColorCode()));
		userIdentityDataList.add(setUserDataFields(Constants.Agency_Card_Serial_Number, userPivEntity.getAgencyCardSerialNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Issuance, Util.formatDate(userPivEntity.getDateOfIssuance(), Util.MMDDYYYY_format)));
    	userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Expiry, Util.formatDate(userPivEntity.getDateOfExpiry(), Util.MMDDYYYY_format)));
		return userIdentityDataList;
	}
	private List<UserIdentityData> setNIDData(List<UserIdentityData> userIdentityDataList, UserNationalIdEntity userNationalIdEntity) {
		userIdentityDataList.add(setUserDataFields(Constants.Id_Number, userNationalIdEntity.getPersonalCode()));
		userIdentityDataList.add(setUserDataFields(Constants.Document_Number, userNationalIdEntity.getDocumentNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Issuance, Util.formatDate(userNationalIdEntity.getDateOfIssuance(), Util.MMDDYYYY_format)));
    	userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Expiry, Util.formatDate(userNationalIdEntity.getDateOfExpiry(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.Vaccine_Name, userNationalIdEntity.getVaccineName()));
		userIdentityDataList.add(setUserDataFields(Constants.Vaccination_Date, Util.formatDate(userNationalIdEntity.getVaccinationDate(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.Vaccination_Location_Name, userNationalIdEntity.getVaccinationLocationName()));
		return userIdentityDataList;
	}

	private List<UserIdentityData> setVIDData(List<UserIdentityData> userIdentityDataList, UserVoterIDEntity userVoterIDEntity) {
		userIdentityDataList.add(setUserDataFields(Constants.Curp, userVoterIDEntity.getCurp()));
		userIdentityDataList.add(setUserDataFields(Constants.Elector_Key, userVoterIDEntity.getElectorKey()));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Issuance, Util.formatDate(userVoterIDEntity.getDateOfIssuance(), Util.MMDDYYYY_format)));
    	userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Expiry, Util.formatDate(userVoterIDEntity.getDateOfExpiry(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.Year_Of_Registration, userVoterIDEntity.getRegistrationYear()));
		userIdentityDataList.add(setUserDataFields(Constants.State, 32));
		return userIdentityDataList;
	}
	
	private List<UserIdentityData> setPRIDData(List<UserIdentityData> userIdentityDataList, UserPermanentResidentIdEntity userPermanentResidentIdEntity) {
		userIdentityDataList.add(setUserDataFields(Constants.Id_Number, userPermanentResidentIdEntity.getPersonalCode()));
		userIdentityDataList.add(setUserDataFields(Constants.Document_Number, userPermanentResidentIdEntity.getDocumentNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Issuance, Util.formatDate(userPermanentResidentIdEntity.getDateOfIssuance(), Util.MMDDYYYY_format)));
    	userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Expiry, Util.formatDate(userPermanentResidentIdEntity.getDateOfExpiry(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.Passport_Number, userPermanentResidentIdEntity.getPassportNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Residentcard_Type_Number, userPermanentResidentIdEntity.getResidentcardTypeNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Residentcard_Type_Code, userPermanentResidentIdEntity.getResidentcardTypeCode()));
		return userIdentityDataList;
	}
	private List<UserIdentityData> setHealthServiceIdData(List<UserIdentityData> userIdentityDataList, UserHealthServiceEntity userHealthServiceEntity) {
		List<UserIdentityData> userIdentityVaccineInfo = new ArrayList<>();
		for (UserHealthServiceVaccineInfoEntity userHealthServiceVaccineInfoEntity : userHealthServiceEntity.getUserHealthServiceVaccineInfos()) {
			userIdentityVaccineInfo.add(setUserDataFields(Constants.Vaccine, userHealthServiceVaccineInfoEntity.getVaccine()));
			userIdentityVaccineInfo.add(setUserDataFields(Constants.Route, userHealthServiceVaccineInfoEntity.getRoute()));
			userIdentityVaccineInfo.add(setUserDataFields(Constants.Site, userHealthServiceVaccineInfoEntity.getSite()));
			userIdentityVaccineInfo.add(setUserDataFields(Constants.Date, userHealthServiceVaccineInfoEntity.getDate()));
			userIdentityVaccineInfo.add(setUserDataFields(Constants.Administered_By, userHealthServiceVaccineInfoEntity.getAdministeredBy()));
		}
		userIdentityDataList.add(setUserDataFields(Constants.Id_Number, userHealthServiceEntity.getIdNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Issuing_Authority, userHealthServiceEntity.getIssuingAuthority()));
		userIdentityDataList.add(setUserDataFields(Constants.Primary_Subscriber, userHealthServiceEntity.getPrimarySubscriber()));
		userIdentityDataList.add(setUserDataFields(Constants.Primary_Subscriber_ID, userHealthServiceEntity.getPrimarySubscriberID()));
		userIdentityDataList.add(setUserDataFields(Constants.Primary_Doctor, userHealthServiceEntity.getPrimaryDoctor()));
		userIdentityDataList.add(setUserDataFields(Constants.Primary_Doctor_Phone, userHealthServiceEntity.getPrimaryDoctorPhone()));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Issuance, Util.formatDate(userHealthServiceEntity.getDateOfIssuance(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Expiry, Util.formatDate(userHealthServiceEntity.getDateOfExpiry(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.userHealthServiceVaccineInfos, userIdentityVaccineInfo));
		return userIdentityDataList;
	}
	private List<UserIdentityData> setEmployeeIdData(List<UserIdentityData> userIdentityDataList, UserEmployeeIDEntity userEmployeeIDEntity) {
		userIdentityDataList.add(setUserDataFields(Constants.Id_Number, userEmployeeIDEntity.getEmployeeIDNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Issuance, Util.formatDate(userEmployeeIDEntity.getDateOfIssuance(), Util.MMDDYYYY_format)));
    	userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Expiry, Util.formatDate(userEmployeeIDEntity.getDateOfExpiry(), Util.MMDDYYYY_format)));
    	if( userEmployeeIDEntity.getAffiliation() != null) {
		userIdentityDataList.add(setUserDataFields(Constants.Affiliation, userEmployeeIDEntity.getAffiliation()));
    	}
    	if( userEmployeeIDEntity.getDepartment() != null) {
		userIdentityDataList.add(setUserDataFields(Constants.Department, userEmployeeIDEntity.getDepartment()));
    	}
		userIdentityDataList.add(setUserDataFields(Constants.Issuer_IN, userEmployeeIDEntity.getIssuerIN()));
		userIdentityDataList.add(setUserDataFields(Constants.Agency_Card_Sn, userEmployeeIDEntity.getAgencyCardSn()));
		return userIdentityDataList;
	}
	private List<UserIdentityData> setPersonalizedIdData(List<UserIdentityData> userIdentityDataList, UserPersonalizedIDEntity userPersonalizedIDEntity) {
		userIdentityDataList.add(setUserDataFields(Constants.Id_Number, userPersonalizedIDEntity.getIdNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Issuance, Util.formatDate(userPersonalizedIDEntity.getDateOfIssuance(), Util.MMDDYYYY_format)));
    	userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Expiry, Util.formatDate(userPersonalizedIDEntity.getDateOfExpiry(), Util.MMDDYYYY_format)));
    	if( userPersonalizedIDEntity.getIdentityType().getDescription() != null) {
		userIdentityDataList.add(setUserDataFields(Constants.Identity_Type, userPersonalizedIDEntity.getIdentityType().getDescription()));
    	}
		userIdentityDataList.add(setUserDataFields(Constants.Issuer_IN, userPersonalizedIDEntity.getIssuerIN()));
		userIdentityDataList.add(setUserDataFields(Constants.Agency_Card_Sn, userPersonalizedIDEntity.getAgencyCardSn()));
		return userIdentityDataList;
	}
	private List<UserIdentityData> setStudentIdData(List<UserIdentityData> userIdentityDataList, UserStudentIDEntity userStudentIDEntity) {
		userIdentityDataList.add(setUserDataFields(Constants.Id_Number, userStudentIDEntity.getStudentIdNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Issuance, Util.formatDate(userStudentIDEntity.getDateOfIssuance(), Util.MMDDYYYY_format)));
    	userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Expiry, Util.formatDate(userStudentIDEntity.getDateOfExpiry(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.Institution, userStudentIDEntity.getInstitution()));
		userIdentityDataList.add(setUserDataFields(Constants.Department, userStudentIDEntity.getDepartment()));
		userIdentityDataList.add(setUserDataFields(Constants.Issuer_IN, userStudentIDEntity.getIssuerIN()));
		userIdentityDataList.add(setUserDataFields(Constants.Agency_Card_Sn, userStudentIDEntity.getAgencyCardSn()));
		return userIdentityDataList;
	}
	private List<UserIdentityData> setHealthIdData(List<UserIdentityData> userIdentityDataList, UserHealthIDEntity userHealthIDEntity) {
		userIdentityDataList.add(setUserDataFields(Constants.Id_Number, userHealthIDEntity.getIdNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Issuance, Util.formatDate(userHealthIDEntity.getDateOfIssuance(), Util.MMDDYYYY_format)));
    	userIdentityDataList.add(setUserDataFields(Constants.Date_Of_Expiry, Util.formatDate(userHealthIDEntity.getDateOfExpiry(), Util.MMDDYYYY_format)));
		userIdentityDataList.add(setUserDataFields(Constants.Subscriber, userHealthIDEntity.getPrimarySubscriber()));
		userIdentityDataList.add(setUserDataFields(Constants.Subscriber_ID, userHealthIDEntity.getPrimarySubscriberID()));
		userIdentityDataList.add(setUserDataFields(Constants.Pcp, userHealthIDEntity.getPcp()));
		userIdentityDataList.add(setUserDataFields(Constants.Pcp_Phone, userHealthIDEntity.getPcpPhone()));
		userIdentityDataList.add(setUserDataFields(Constants.Policy_Number, userHealthIDEntity.getPolicyNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Group_Plan, userHealthIDEntity.getGroupPlan()));
		userIdentityDataList.add(setUserDataFields(Constants.Health_Plan_Number, userHealthIDEntity.getHealthPlanNumber()));
		userIdentityDataList.add(setUserDataFields(Constants.Agency_Card_Sn, userHealthIDEntity.getAgencyCardSN()));
		return userIdentityDataList;
	}
	private UserHealthServiceEntity generateHSID(String organizationName, String workflowName,
			String credentialTemplateID, User user, UserEntity userEntity,
			String visualTemplateType) {

		UserHealthServiceEntity userHealthServiceEntity = null;
		try {

			if(user.getId()!= null) {
				userHealthServiceEntity = userHealthServiceRepository.findByUserID(userEntity.getId());
			}
			
			
			if (userHealthServiceEntity == null) {
				userHealthServiceEntity = new UserHealthServiceEntity();
				if(user.getId()!= null) 
					userHealthServiceEntity.setUser(userEntity);
				userHealthServiceEntity.setIdNumber(usingMathClass());
				//TODO make these attributes dynamic 
				userHealthServiceEntity.setIssuingAuthority("Issuing Authority ");
				/* userPivEntity.setAffiliation("IT Department"); */
				userHealthServiceEntity.setPrimarySubscriber(user.getUserHealthService().getPrimarySubscriber());
				userHealthServiceEntity.setPrimarySubscriberID(user.getUserHealthService().getPrimarySubscriberID());
				userHealthServiceEntity.setPrimaryDoctor(user.getUserHealthService().getPrimaryDoctor());
				userHealthServiceEntity.setPrimaryDoctorPhone(user.getUserHealthService().getPrimaryDoctorPhone());
				userHealthServiceEntity.setUserHealthServiceVaccineInfos(new ArrayList<UserHealthServiceVaccineInfoEntity>());
				List<UserHealthServiceVaccineInfoEntity> UserHealthServiceVaccineInfoEntities = userHealthServiceEntity.getUserHealthServiceVaccineInfos() ;
							
				for (UserHealthServiceVaccineInfo userHealthServiceVaccineInfo : user.getUserHealthService().getUserHealthServiceVaccineInfos()) {
				
					UserHealthServiceVaccineInfoEntity userHealthServiceVaccineInfoEntity = new UserHealthServiceVaccineInfoEntity();
					userHealthServiceVaccineInfoEntity.setVaccine(userHealthServiceVaccineInfo.getVaccine());
					userHealthServiceVaccineInfoEntity.setRoute(userHealthServiceVaccineInfo.getRoute());
					userHealthServiceVaccineInfoEntity.setSite(userHealthServiceVaccineInfo.getSite());
					userHealthServiceVaccineInfoEntity.setDate(userHealthServiceVaccineInfo.getDate());
					userHealthServiceVaccineInfoEntity.setAdministeredBy(userHealthServiceVaccineInfo.getAdministeredBy());
					userHealthServiceVaccineInfoEntity.setUserhs(userHealthServiceEntity);
					
					UserHealthServiceVaccineInfoEntities.add(userHealthServiceVaccineInfoEntity);
				}
				userHealthServiceEntity.setUimsId(""+usingMathClass());

				//Calculate Expiry date
				Workflow workflow =  null;
				if(userEntity.getGroupId() == null) {
					workflow = userService.getWorkflowByUserName(organizationName, userEntity.getReferenceName());
				}else {
					workflow = workflowService.getWorkflowValidityByGroupID(organizationName, userEntity.getGroupId());
				}
				int months = workflow.getExpirationInMonths();
				int days = workflow.getExpirationInDays();
				Instant createdOn = workflow.getCreatedDate();
				Calendar cal = Calendar.getInstance();
				cal.setTime(null);
				cal.add(Calendar.DAY_OF_MONTH, months * 30 + days);
				userHealthServiceEntity.setDateOfExpiry(null);
				userHealthServiceEntity.setDateOfIssuance(null);
			} else {
				userHealthServiceEntity.setDateOfIssuance(Instant.now());
				Workflow workflow = workflowService.getWorkflowValidityByName(organizationName, workflowName);

				int months = workflow.getExpirationInMonths();
				int days = workflow.getExpirationInDays();
				Instant createdOn = workflow.getCreatedDate();
				Calendar cal = Calendar.getInstance();
				cal.setTime(null);
				cal.add(Calendar.DAY_OF_MONTH, months * 30 + days);
				userHealthServiceEntity.setDateOfExpiry(null);
			}

			if(user.getId()!= null)
				userHealthServiceRepository.save(userHealthServiceEntity);
		
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName()),
					cause);
			throw new VisualCredentialException(cause,
					ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName());
		}	
		return userHealthServiceEntity;

	}
		
	private UserDrivingLicenseEntity issueDrivingLicenseEntity(String organizationName, String credentialTemplateID, User user, UserEntity userEntity,  String visualTemplateType) throws VisualCredentialException{

		UserDrivingLicenseEntity userDrivingLicenseEntity = null;
		try {

			
				userDrivingLicenseEntity = userDrivingLicenseRepository.findByUserID(userEntity.getId());
				if(userDrivingLicenseEntity.getDateOfIssuance() == null) {
					userDrivingLicenseEntity.setUimsId(userEntity.getOrganization().getAbbreviation()+""+usingMathClass());
					userDrivingLicenseEntity.setDocumentDescriminator(documentNumber_prefix+Util.get6DigitRandomNumber());
					userDrivingLicenseEntity.setDateOfIssuance(Instant.now());
				}
				
				//Calculate Expiry date
				Workflow workflow =  null;
				if(userEntity.getGroupId() == null) {
					workflow = userService.getWorkflowByUserName(organizationName, userEntity.getReferenceName());
				}else {
					workflow = workflowService.getWorkflowValidityByGroupID(organizationName, userEntity.getGroupId());
				}
				Long identityExpiryDate_inDays = 0l;
				if(workflow.isDisableExpirationEnforcement()) {
					identityExpiryDate_inDays = organizationIdentitiesRepository.getOrganizationIdentityExpiryDate(organizationName, workflow.getOrganizationIdentities().getId());
					Instant expiryDate = Instant.now().plus(identityExpiryDate_inDays, ChronoUnit.DAYS);
					userDrivingLicenseEntity.setDateOfExpiry(expiryDate);
				} else {
					int months = workflow.getExpirationInMonths();
					int days = workflow.getExpirationInDays();
					Instant workflow_createdOn = workflow.getCreatedDate();
					Instant workflow_expiryOn = workflow_createdOn.plus(months*30+days, ChronoUnit.DAYS);
					userDrivingLicenseEntity.setDateOfExpiry(workflow_expiryOn);
				}
			
				userDrivingLicenseRepository.save(userDrivingLicenseEntity);
		
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName()),
					cause);
			throw new VisualCredentialException(cause,
					ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName());
		}	
		return userDrivingLicenseEntity;

	}
	 
	private UserPivEntity issuePivEntity(String organizationName, String credentialTemplateID, User user, UserEntity userEntity,  String visualTemplateType) throws VisualCredentialException{

		UserPivEntity userPivEntity = null;
		try {

			
				userPivEntity = userPivRepository.findByUserID(userEntity.getId());
				userPivEntity.setAgencyCardSerialNumber(userPivEntity.getAgencyAbbreviation()+""+usingMathClass());
				if(userPivEntity.getDateOfIssuance() == null) {
					userPivEntity.setUimsID(userPivEntity.getAgencyAbbreviation()+""+usingMathClass());
					userPivEntity.setDateOfIssuance(Instant.now());
				}
				//Calculate Expiry date
				Workflow workflow =  null;
				if(userEntity.getGroupId() == null) {
					workflow = userService.getWorkflowByUserName(organizationName, userEntity.getReferenceName());
				}else {
					workflow = workflowService.getWorkflowValidityByGroupID(organizationName, userEntity.getGroupId());
				}
				
				Long identityExpiryDate_inDays = 0l;
				if(workflow.isDisableExpirationEnforcement()) {
					identityExpiryDate_inDays = organizationIdentitiesRepository.getOrganizationIdentityExpiryDate(organizationName, workflow.getOrganizationIdentities().getId());
					Instant expiryDate = Instant.now().plus(identityExpiryDate_inDays, ChronoUnit.DAYS);
					userPivEntity.setDateOfExpiry(expiryDate);
					userPivEntity.setCardExpiry(Util.formatDate(expiryDate, Util.MMMYYYY_format));
				} else {
					int months = workflow.getExpirationInMonths();
					int days = workflow.getExpirationInDays();
					Instant workflow_createdOn = workflow.getCreatedDate();
					Instant workflow_expiryOn = workflow_createdOn.plus(months*30+days, ChronoUnit.DAYS);
					userPivEntity.setDateOfExpiry(workflow_expiryOn);
					userPivEntity.setCardExpiry(Util.formatDate(workflow_expiryOn, Util.MMMYYYY_format));
				}
			
				userPivRepository.save(userPivEntity);
		
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName()),
					cause);
			throw new VisualCredentialException(cause,
					ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName());
		}	
		return userPivEntity;

	}
	
	private UserNationalIdEntity issueNIDEntity(String organizationName, String credentialTemplateID, User user, UserEntity userEntity,  String visualTemplateType) throws VisualCredentialException{

		UserNationalIdEntity userNationalIdEntity = null;
		try {

			
				userNationalIdEntity = userNationalIDRepository.findByUserID(userEntity.getId());
										
				if(userNationalIdEntity.getDateOfIssuance() == null) {
					userNationalIdEntity.setUimsId(userEntity.getOrganization().getAbbreviation()+""+usingMathClass());
					userNationalIdEntity.setDocumentNumber(documentNumber_prefix+Util.get6DigitRandomNumber());
					userNationalIdEntity.setDateOfIssuance(Instant.now());
				}
				
				//Calculate Expiry date
				Workflow workflow =  null;
				if(userEntity.getGroupId() == null) {
					workflow = userService.getWorkflowByUserName(organizationName, userEntity.getReferenceName());
				}else {
					workflow = workflowService.getWorkflowValidityByGroupID(organizationName, userEntity.getGroupId());
				}
				
				Long identityExpiryDate_inDays = 0l;
				if(workflow.isDisableExpirationEnforcement()) {
					identityExpiryDate_inDays = organizationIdentitiesRepository.getOrganizationIdentityExpiryDate(organizationName, workflow.getOrganizationIdentities().getId());
					Instant expiryDate = Instant.now().plus(identityExpiryDate_inDays, ChronoUnit.DAYS);
					userNationalIdEntity.setDateOfExpiry(expiryDate);
				} else {
					int months = workflow.getExpirationInMonths();
					int days = workflow.getExpirationInDays();
					Instant workflow_createdOn = workflow.getCreatedDate();
					Instant workflow_expiryOn = workflow_createdOn.plus(months*30+days, ChronoUnit.DAYS);
					userNationalIdEntity.setDateOfExpiry(workflow_expiryOn);
				}
			
				userNationalIDRepository.save(userNationalIdEntity);
		
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName()),
					cause);
			throw new VisualCredentialException(cause,
					ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName());
		}	
		return userNationalIdEntity;

	}
	
	private UserVoterIDEntity issueVIDEntity(String organizationName, String credentialTemplateID, User user, UserEntity userEntity,  String visualTemplateType) throws VisualCredentialException{

		UserVoterIDEntity userVoterIDEntity = null;
		try {

			
			userVoterIDEntity = userVoterIDRepository.findByUserID(userEntity.getId());
			if(userVoterIDEntity.getCurp() == null) {
				userVoterIDEntity.setCurp(documentNumber_prefix+Util.getAlphaNumericString(18).toUpperCase());
			}
				if(userVoterIDEntity.getDateOfIssuance() == null) {
					userVoterIDEntity.setUimsId(userEntity.getOrganization().getAbbreviation()+""+usingMathClass());
					userVoterIDEntity.setDateOfIssuance(Instant.now());
				}
				
				//Calculate Expiry date
				Workflow workflow =  null;
				if(userEntity.getGroupId() == null) {
					workflow = userService.getWorkflowByUserName(organizationName, userEntity.getReferenceName());
				}else {
					workflow = workflowService.getWorkflowValidityByGroupID(organizationName, userEntity.getGroupId());
				}
				
				Long identityExpiryDate_inDays = 0l;
				if(workflow.isDisableExpirationEnforcement()) {
					identityExpiryDate_inDays = organizationIdentitiesRepository.getOrganizationIdentityExpiryDate(organizationName, workflow.getOrganizationIdentities().getId());
					Instant expiryDate = Instant.now().plus(identityExpiryDate_inDays, ChronoUnit.DAYS);
					userVoterIDEntity.setDateOfExpiry(expiryDate);
				} else {
					int months = workflow.getExpirationInMonths();
					int days = workflow.getExpirationInDays();
					Instant workflow_createdOn = workflow.getCreatedDate();
					Instant workflow_expiryOn = workflow_createdOn.plus(months*30+days, ChronoUnit.DAYS);
					userVoterIDEntity.setDateOfExpiry(workflow_expiryOn);
				}
			
				userVoterIDRepository.save(userVoterIDEntity);
		
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName()),
					cause);
			throw new VisualCredentialException(cause,
					ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName());
		}	
		return userVoterIDEntity;

	}
	
	private UserPermanentResidentIdEntity issuePRIDEntity(String organizationName, String credentialTemplateID, User user, UserEntity userEntity,  String visualTemplateType) throws VisualCredentialException{

		UserPermanentResidentIdEntity userPermanentResidentIdEntity = null;
		try {

			
			userPermanentResidentIdEntity = userPermanentResidentIdRepository.findByUserID(userEntity.getId());
										
			if(userPermanentResidentIdEntity.getDateOfIssuance() == null) {
				userPermanentResidentIdEntity.setUimsId(userEntity.getOrganization().getAbbreviation()+""+usingMathClass());
				userPermanentResidentIdEntity.setDocumentNumber(documentNumber_prefix+Util.get6DigitRandomNumber());
				userPermanentResidentIdEntity.setDateOfIssuance(Instant.now());
			}
				
				//Calculate Expiry date
				Workflow workflow =  null;
				if(userEntity.getGroupId() == null) {
					workflow = userService.getWorkflowByUserName(organizationName, userEntity.getReferenceName());
				}else {
					workflow = workflowService.getWorkflowValidityByGroupID(organizationName, userEntity.getGroupId());
				}
				
				Long identityExpiryDate_inDays = 0l;
				if(workflow.isDisableExpirationEnforcement()) {
					identityExpiryDate_inDays = organizationIdentitiesRepository.getOrganizationIdentityExpiryDate(organizationName, workflow.getOrganizationIdentities().getId());
					Instant expiryDate = Instant.now().plus(identityExpiryDate_inDays, ChronoUnit.DAYS);
					userPermanentResidentIdEntity.setDateOfExpiry(expiryDate);
				} else {
					int months = workflow.getExpirationInMonths();
					int days = workflow.getExpirationInDays();
					Instant workflow_createdOn = workflow.getCreatedDate();
					Instant workflow_expiryOn = workflow_createdOn.plus(months*30+days, ChronoUnit.DAYS);
					userPermanentResidentIdEntity.setDateOfExpiry(workflow_expiryOn);
				}
			
				userPermanentResidentIdRepository.save(userPermanentResidentIdEntity);
		
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName()),
					cause);
			throw new VisualCredentialException(cause,
					ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName());
		}	
		return userPermanentResidentIdEntity;

	}
	
	private UserHealthIDEntity issueHealthIDEntity(String organizationName, String credentialTemplateID, User user, UserEntity userEntity,  String visualTemplateType) throws VisualCredentialException{

		UserHealthIDEntity userHealthIDEntity = null;
		try {

			
				userHealthIDEntity = userHealthIDRepository.findByUserID(userEntity.getId());
				userHealthIDEntity.setAgencyCardSN(userEntity.getOrganization().getAbbreviation()+""+usingMathClass());
				if(userHealthIDEntity.getDateOfIssuance() == null) {
					userHealthIDEntity.setUimsId(userEntity.getOrganization().getAbbreviation()+""+usingMathClass());
					userHealthIDEntity.setIssuerIN(userEntity.getOrganization().getIssuerIdentificationNumber());
					userHealthIDEntity.setDateOfIssuance(Instant.now());
				}
				
				//Calculate Expiry date
				Workflow workflow =  null;
				if(userEntity.getGroupId() == null) {
					workflow = userService.getWorkflowByUserName(organizationName, userEntity.getReferenceName());
				}else {
					workflow = workflowService.getWorkflowValidityByGroupID(organizationName, userEntity.getGroupId());
				}
				
				Long identityExpiryDate_inDays = 0l;
				if(workflow.isDisableExpirationEnforcement()) {
					identityExpiryDate_inDays = organizationIdentitiesRepository.getOrganizationIdentityExpiryDate(organizationName, workflow.getOrganizationIdentities().getId());
					Instant expiryDate = Instant.now().plus(identityExpiryDate_inDays, ChronoUnit.DAYS);
					userHealthIDEntity.setDateOfExpiry(expiryDate);
				} else {
					int months = workflow.getExpirationInMonths();
					int days = workflow.getExpirationInDays();
					Instant workflow_createdOn = workflow.getCreatedDate();
					Instant workflow_expiryOn = workflow_createdOn.plus(months*30+days, ChronoUnit.DAYS);
					userHealthIDEntity.setDateOfExpiry(workflow_expiryOn);
				}
			
				userHealthIDRepository.save(userHealthIDEntity);
		
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName()),
					cause);
			throw new VisualCredentialException(cause,
					ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName());
		}	
		return userHealthIDEntity;

	}
	
	
	private UserPersonalizedIDEntity issuePersonalizedIDEntity(String organizationName, String credentialTemplateID, User user, UserEntity userEntity,  String visualTemplateType) throws VisualCredentialException{

		UserPersonalizedIDEntity userPersonalizedIDEntity = null;
		try {

			
			userPersonalizedIDEntity = userPersonalizedIDRepository.findByUserID(userEntity.getId());
										
			userPersonalizedIDEntity.setAgencyCardSn(userEntity.getOrganization().getAbbreviation()+""+usingMathClass());
			userPersonalizedIDEntity.setIssuerIN(userEntity.getOrganization().getIssuerIdentificationNumber());
			if(userPersonalizedIDEntity.getDateOfIssuance() == null) {
				userPersonalizedIDEntity.setUimsId(userEntity.getOrganization().getAbbreviation()+""+usingMathClass());
				userPersonalizedIDEntity.setDateOfIssuance(Instant.now());
				userPersonalizedIDEntity.setDateOfExpiry(Instant.now());
			}
				
			//Calculate Expiry date
			Workflow workflow =  null;
			if(userEntity.getGroupId() == null) {
				workflow = userService.getWorkflowByUserName(organizationName, userEntity.getReferenceName());
			}else {
				workflow = workflowService.getWorkflowValidityByGroupID(organizationName, userEntity.getGroupId());
			}
			Long identityExpiryDate_inDays = 0l;
			if(workflow.isDisableExpirationEnforcement()) {
				identityExpiryDate_inDays = organizationIdentitiesRepository.getOrganizationIdentityExpiryDate(organizationName, workflow.getOrganizationIdentities().getId());
				Instant expiryDate = Instant.now().plus(identityExpiryDate_inDays, ChronoUnit.DAYS);
				userPersonalizedIDEntity.setDateOfExpiry(expiryDate);
			} else {
				int months = workflow.getExpirationInMonths();
				int days = workflow.getExpirationInDays();
				Instant workflow_createdOn = workflow.getCreatedDate();
				Instant workflow_expiryOn = workflow_createdOn.plus(months*30+days, ChronoUnit.DAYS);
				userPersonalizedIDEntity.setDateOfExpiry(workflow_expiryOn);
			}
			
			userPersonalizedIDRepository.save(userPersonalizedIDEntity);
		
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName()),
					cause);
			throw new VisualCredentialException(cause,
					ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName());
		}	
		return userPersonalizedIDEntity;

	}
	
	private UserEmployeeIDEntity issueEmployeeIDEntity(String organizationName, String credentialTemplateID, User user, UserEntity userEntity,  String visualTemplateType) throws VisualCredentialException{

		UserEmployeeIDEntity userEmployeeIDEntity = null;
		try {

			
				userEmployeeIDEntity = userEmployeeIDRepository.findByUserID(userEntity.getId());
										
				userEmployeeIDEntity.setUimsId(userEntity.getOrganization().getAbbreviation()+""+usingMathClass());
				userEmployeeIDEntity.setAgencyCardSn(userEntity.getOrganization().getAbbreviation()+""+usingMathClass());
				userEmployeeIDEntity.setIssuerIN(userEntity.getOrganization().getIssuerIdentificationNumber());
				userEmployeeIDEntity.setDateOfIssuance(Instant.now());
				
				//Calculate Expiry date
				Workflow workflow =  null;
				if(userEntity.getGroupId() == null) {
					workflow = userService.getWorkflowByUserName(organizationName, userEntity.getReferenceName());
				}else {
					workflow = workflowService.getWorkflowValidityByGroupID(organizationName, userEntity.getGroupId());
				}
				Long identityExpiryDate_inDays = 0l;
				if(workflow.isDisableExpirationEnforcement()) {
					identityExpiryDate_inDays = organizationIdentitiesRepository.getOrganizationIdentityExpiryDate(organizationName, workflow.getOrganizationIdentities().getId());
					Instant expiryDate = Instant.now().plus(identityExpiryDate_inDays, ChronoUnit.DAYS);
					userEmployeeIDEntity.setDateOfExpiry(expiryDate);
				} else {
					int months = workflow.getExpirationInMonths();
					int days = workflow.getExpirationInDays();
					Instant workflow_createdOn = workflow.getCreatedDate();
					Instant workflow_expiryOn = workflow_createdOn.plus(months*30+days, ChronoUnit.DAYS);
					userEmployeeIDEntity.setDateOfExpiry(workflow_expiryOn);
				}
			
				userEmployeeIDRepository.save(userEmployeeIDEntity);
		
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName()),
					cause);
			throw new VisualCredentialException(cause,
					ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName());
		}	
		return userEmployeeIDEntity;

	}
	
	private UserStudentIDEntity issueStudentIDEntity(String organizationName, String credentialTemplateID, User user, UserEntity userEntity,  String visualTemplateType) throws VisualCredentialException{

		UserStudentIDEntity userStudentIDEntity = null;
		try {

			
				userStudentIDEntity = userStudentIDRepository.findByUserID(userEntity.getId());
											
				userStudentIDEntity.setUimsId(userEntity.getOrganization().getAbbreviation()+""+usingMathClass());
				userStudentIDEntity.setAgencyCardSn(userEntity.getOrganization().getAbbreviation()+""+usingMathClass());
				userStudentIDEntity.setIssuerIN(userEntity.getOrganization().getIssuerIdentificationNumber());
				userStudentIDEntity.setDateOfIssuance(Instant.now());
				
				//Calculate Expiry date
				Workflow workflow =  null;
				if(userEntity.getGroupId() == null) {
					workflow = userService.getWorkflowByUserName(organizationName, userEntity.getReferenceName());
				}else {
					workflow = workflowService.getWorkflowValidityByGroupID(organizationName, userEntity.getGroupId());
				}
				Long identityExpiryDate_inDays = 0l;
				if(workflow.isDisableExpirationEnforcement()) {
					identityExpiryDate_inDays = organizationIdentitiesRepository.getOrganizationIdentityExpiryDate(organizationName, workflow.getOrganizationIdentities().getId());
					Instant expiryDate = Instant.now().plus(identityExpiryDate_inDays, ChronoUnit.DAYS);
					userStudentIDEntity.setDateOfExpiry(expiryDate);
				} else {
					int months = workflow.getExpirationInMonths();
					int days = workflow.getExpirationInDays();
					Instant workflow_createdOn = workflow.getCreatedDate();
					Instant workflow_expiryOn = workflow_createdOn.plus(months*30+days, ChronoUnit.DAYS);
					userStudentIDEntity.setDateOfExpiry(workflow_expiryOn);
				}
			
				userStudentIDRepository.save(userStudentIDEntity);
		
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName()),
					cause);
			throw new VisualCredentialException(cause,
					ErrorMessages.ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL, visualTemplateType,user.getName());
		}	
		return userStudentIDEntity;

	}

	@Override
	public List<UserIdentityData> getUserIdentityWithSerialNumberAndTemplateID(String organizationName, Long userID, 
			String identitySerialNumber, Long credentialTemplateID) {
		List<UserIdentityData> userIdentityDataList  = new ArrayList<>();
		UserEntity userEntity = null;
		if (userEntity == null ) {
				userEntity = userRepository.findOne(userID);
				setUserAttributeData(userIdentityDataList, userEntity);
				userEntity.getUserBiometrics().stream().forEach(userBio -> {
					if (userBio.getType().equalsIgnoreCase("FACE")) {
						userIdentityDataList.add(setUserDataFields(Constants.Photo, Base64Utils.encodeToString(userBio.getData())));						
					}
				});
				userIdentityDataList.add(setUserDataFields(Constants.UserStatus, userEntity.getUserStatus()));
		}
		VisualTemplateEntity visualTemplateEntity = visualTemplateRepository.findOne(credentialTemplateID);
		String visualTemplateType = visualTemplateEntity.getOrgIdentityType().getIdentityType().getName();
		switch (visualTemplateType) {

			case Constants.Driving_License:
				UserDrivingLicenseEntity userDrivingLicenseEntity = userDrivingLicenseRepository.findByUserID(userEntity.getId());
				setDrivingLicenceData(userIdentityDataList, userDrivingLicenseEntity);
				
				break;
			case Constants.PIV_and_PIV_1:
				UserPivEntity userPivEntity = userPivRepository.findByUserID(userEntity.getId());
				setPIVData(userIdentityDataList, userPivEntity);
				
				break;
	
			case Constants.National_ID:			
				UserNationalIdEntity userNationalIdEntity = userNationalIDRepository.findByUserID(userEntity.getId());
				setNIDData(userIdentityDataList, userNationalIdEntity);
				
				break;
	
			case Constants.Voter_ID:			
				UserVoterIDEntity userVoterIDEntity = userVoterIDRepository.findByUserID(userEntity.getId());
				setVIDData(userIdentityDataList, userVoterIDEntity);
				
				break;
			case Constants.Health_ID:
				UserHealthIDEntity userHealthIDEntity = userHealthIDRepository.findByUserID(userEntity.getId());
				setHealthIdData(userIdentityDataList, userHealthIDEntity);
					
				break;
			case Constants.Health_Service_ID:
				UserHealthServiceEntity userHealthServiceEntity = null;
				
				break;
			case Constants.Student_ID:
				UserStudentIDEntity userStudentIDEntity = userStudentIDRepository.findByUserID(userEntity.getId());
				setStudentIdData(userIdentityDataList, userStudentIDEntity);
	
				break;
			case Constants.Employee_ID:
				UserEmployeeIDEntity userEmployeeIDEntity = userEmployeeIDRepository.findByUserID(userEntity.getId());
				setEmployeeIdData(userIdentityDataList, userEmployeeIDEntity);
	
				break;
			default:
				break;
		}
		return userIdentityDataList;
	}
	
	@Override
	public List<UserIdentityData> getUserAgeWithID(String organizationName, Long userID) {
		List<UserIdentityData> userIdentityDataList  = new ArrayList<>();
		UserEntity userEntity = null;

		OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);
		String config = organizationEntity.getSystemCode();
		String[] tokens = config.split(",");
		
				userEntity = userRepository.findOne(userID);
				Instant user_dob = userEntity.getUserAttribute().getDateOfBirth();
				
				int age = Util.calculateAge(LocalDateTime.ofInstant(user_dob, ZoneOffset.UTC).toLocalDate(), LocalDate.now());
//				switch (tokens[0]) {
//				case "21":

							userIdentityDataList.add(setUserDataFields(Constants.Verify_Age_For_Number, tokens[0]));
							userIdentityDataList.add(setUserDataFields(Constants.Verify_Age_For_Text, tokens[1]));
							String[] ucolorTokens = tokens[2].split(":");
							String[] ocolorTokens = tokens[3].split(":");
							userIdentityDataList.add(setUserDataFields(ucolorTokens[0], ucolorTokens[1]));
							userIdentityDataList.add(setUserDataFields(ocolorTokens[0], ocolorTokens[1]));
						if (age>=Integer.parseInt(tokens[0])) {
							userIdentityDataList.add(setUserDataFields(tokens[1], false));
						}
						else if (age<Integer.parseInt(tokens[0])) {
							userIdentityDataList.add(setUserDataFields(tokens[1], true));					
						}
//					break;
//				case "19":
//						if (age>=Integer.parseInt(tokens[0])) {
//							userIdentityDataList.add(setUserDataFields(Constants.Verify_Age_For_Text, Constants.Under19));
//							userIdentityDataList.add(setUserDataFields(Constants.Verify_Age_For_Number, 19));
//							userIdentityDataList.add(setUserDataFields(Constants.Under19, false));
//						}
//						else if (age<Integer.parseInt(tokens[0])) {
//							userIdentityDataList.add(setUserDataFields(Constants.Verify_Age_For_Text, Constants.Under19));
//							userIdentityDataList.add(setUserDataFields(Constants.Verify_Age_For_Number, 19));
//							userIdentityDataList.add(setUserDataFields(Constants.Under19, true));					
//						}
//					break;
//				default:
//						if (age>=Integer.parseInt(tokens[0])) {
//							userIdentityDataList.add(setUserDataFields(Constants.Verify_Age_For_Text, Constants.Under18));
//							userIdentityDataList.add(setUserDataFields(Constants.Verify_Age_For_Number, 18));
//							userIdentityDataList.add(setUserDataFields(Constants.Under18, false));
//						}
//						else if (age<Integer.parseInt(tokens[0])) {
//							userIdentityDataList.add(setUserDataFields(Constants.Verify_Age_For_Text, Constants.Under18));
//							userIdentityDataList.add(setUserDataFields(Constants.Verify_Age_For_Number, 18));
//							userIdentityDataList.add(setUserDataFields(Constants.Verify_Age_For_Number, 18));
//							userIdentityDataList.add(setUserDataFields(Constants.Under18, true));
//						}
//					break;
//				}
				userEntity.getUserBiometrics().stream().forEach(userBio -> {
					if (userBio.getType().equalsIgnoreCase("FACE")) {
						userIdentityDataList.add(setUserDataFields(Constants.Photo, Base64Utils.encodeToString(userBio.getData())));						
					}
				});
				
				userIdentityDataList.add(setUserDataFields(Constants.UserStatus, userEntity.getUserStatus()));
		
		return userIdentityDataList;
	}
		
	
	private long usingMathClass() {
		double x = (Math.random() * ((999999999 - 100000000) + 1)) + 100000000;
		return (long) x;

	}

	private String appendJson(String jsonVisualdata, String string) {
		jsonVisualdata = jsonVisualdata.concat(string);
		jsonVisualdata = jsonVisualdata.replaceAll("\\}\\{", ",");
		return jsonVisualdata;
	}



}
