package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meetidentity.usermanagement.domain.DrivingLicenseRestrictionEntity;
import com.meetidentity.usermanagement.repository.DrivingLicenseRestrictionsRepository;
import com.meetidentity.usermanagement.service.DrivingLicenseService;
import com.meetidentity.usermanagement.web.rest.model.DrivingLicenseRestriction;


@Service
@Transactional
public class DrivingLicenseServiceImpl  implements DrivingLicenseService {

	private static final Logger log = LoggerFactory.getLogger(DeviceProfileServiceImpl.class);

	@Autowired
	private DrivingLicenseRestrictionsRepository drivingLicenseRestrictionsRepository;

	@Override
	public List<DrivingLicenseRestriction> getDrivingLicenseRestrictions(String organizationName) {
		List<DrivingLicenseRestriction> drivingLicenseRestrictionsList = new ArrayList<>();
		List<DrivingLicenseRestrictionEntity> drivingLicenseRestrictionEntities = drivingLicenseRestrictionsRepository.findByOrganizationName(organizationName);
		if(drivingLicenseRestrictionEntities != null) {
			drivingLicenseRestrictionEntities.forEach(drivingLicenseRestrictionEntity -> {
				DrivingLicenseRestriction drivingLicenseRestriction = new DrivingLicenseRestriction();
				drivingLicenseRestriction.setId(drivingLicenseRestrictionEntity.getId());
				drivingLicenseRestriction.setType(drivingLicenseRestrictionEntity.getType());
				drivingLicenseRestriction.setDescription(drivingLicenseRestrictionEntity.getDescription());
				drivingLicenseRestrictionsList.add(drivingLicenseRestriction);
			});
		}
		return drivingLicenseRestrictionsList;
	}

	

}
