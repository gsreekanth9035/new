package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.domain.PermissionEntity;
import com.meetidentity.usermanagement.repository.PermissionRepository;
import com.meetidentity.usermanagement.service.PermissionService;
import com.meetidentity.usermanagement.web.rest.model.Permission;

@Service
public class PermissionServiceImpl implements PermissionService {
	
	@Autowired
	private PermissionRepository permissionRepository;

	@Override
	public List<Permission> getPermissionsByRole(String organizationName, String roleName) {
		List<PermissionEntity> permissionEntities = permissionRepository.findAllPermissionsByRole(organizationName, roleName);
		
		List<Permission> permissions = new ArrayList<>();
		for (PermissionEntity permissionEntity : permissionEntities) {
			Permission permission = new Permission();
			permission.setName(permissionEntity.getName());
			permission.setDisplayName(permissionEntity.getDisplayName());
			permission.setDescription(permissionEntity.getDescription());
			
			permissions.add(permission);
		}
		
		return permissions;
	}
	
	

}
