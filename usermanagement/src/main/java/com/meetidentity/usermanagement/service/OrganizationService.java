package com.meetidentity.usermanagement.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.meetidentity.usermanagement.web.rest.model.EnrollmentApproveRejectReasons;
import com.meetidentity.usermanagement.web.rest.model.Organization;
import com.meetidentity.usermanagement.web.rest.model.OrganizationBrandingInfo;
import com.meetidentity.usermanagement.web.rest.model.RegistrationConfigDetails;

public interface OrganizationService {

	public Organization getOrgByName(String orgName);

	public List<String> getOrgLogoByName(String orgName);
	
	public RegistrationConfigDetails getRegistrationConfigDetailsByOrgName(String organizationName, long id);
	
	public OrganizationBrandingInfo getOrgBrandingByName(String orgName);	

	public List<String> getOrgLogo();

	public EnrollmentApproveRejectReasons getApprovalRejectionReasons(String organizationName);
	
	public List<Organization> getAllOrganizations();
	
	public ResponseEntity<Boolean> checkTransparentImageEnabled(String organizationName);

}
