package com.meetidentity.usermanagement.service.impl;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.SecretKey;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.InvalidParameterException;
import com.amazonaws.services.sns.model.PublishResult;
import com.google.gson.Gson;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.config.ApplicationProperties;
import com.meetidentity.usermanagement.config.message.MessagingService;
import com.meetidentity.usermanagement.domain.PushNotificationDeviceEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserOtpEntity;
import com.meetidentity.usermanagement.exception.PushNotificationServiceException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.repository.PushNotificationDeviceRepository;
import com.meetidentity.usermanagement.repository.UserOtpRepository;
import com.meetidentity.usermanagement.security.CryptoFunction;
import com.meetidentity.usermanagement.service.PushNotificationDeviceService;
import com.meetidentity.usermanagement.service.UserService;
import com.meetidentity.usermanagement.service.helper.PushNotificationServiceHelper;
import com.meetidentity.usermanagement.service.helper.PushNotificationServiceHelper.Platform;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.Challenge;
import com.meetidentity.usermanagement.web.rest.model.PushNotificationDevice;
import com.meetidentity.usermanagement.web.rest.model.PushNotificationPayLoad;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;


@Service
@Transactional
public class PushNotificationDeviceServiceImpl implements PushNotificationDeviceService {

	private static final Logger log = LoggerFactory.getLogger(PushNotificationDeviceServiceImpl.class);
	
	@Autowired
	private PushNotificationDeviceRepository pushNotificationDeviceRepo;
	
	@Autowired
	ApplicationProperties applicationProperties;
	
	@Autowired
	UserOtpRepository userOtpRepository;
	
	@Autowired
	UserService userService;
	
	@Autowired
	private CryptoFunction cryptoFunction;
	
	@Override
	public List<PushNotificationDevice> findAllDevicesByUserName(String userName) throws PushNotificationServiceException{
		
		try {
			List<PushNotificationDeviceEntity> notificationDeviceEntities = pushNotificationDeviceRepo.findByUserName(userName);
			List<PushNotificationDevice> pushNotificationDevices = new ArrayList<>();
			
			
			for (PushNotificationDeviceEntity pushNotificationDeviceEntity : notificationDeviceEntities) {
				 Gson gson = new Gson();
			     String json = gson.toJson(pushNotificationDeviceEntity);
				pushNotificationDevices.add(gson.fromJson(json, PushNotificationDevice.class));
			}
			
			return pushNotificationDevices;
		}catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.UNABLE_TO_FIND_USER_DEVICES, userName), cause);
			throw new PushNotificationServiceException(cause, ErrorMessages.UNABLE_TO_FIND_USER_DEVICES, userName);
		}
	}

	@Override
	public PushNotificationPayLoad getPushNotificationPayLoadData(PushNotificationDevice pushNotificationDevice)
			throws PushNotificationServiceException {
		 Gson gson = new Gson();
		 Challenge challenge = pushNotificationDevice.getChallenge();
		 UserAttribute userAttribute = userService.getUserAttributesByUserId(challenge.getOrganizationName(), challenge.getUserId());
		 challenge.setUserName(userAttribute.getFirstName() +" "+userAttribute.getLastName());
		 
		if(challenge.getNotificationID() == null) {
		     // TO-DO set new ID and set here 
			 pushNotificationDevice.setCustomData(gson.toJson(challenge));	
		     String json = gson.toJson(pushNotificationDevice);
			 PushNotificationDeviceEntity pushNotificationDeviceEntity =  pushNotificationDeviceRepo.save(gson.fromJson(json, PushNotificationDeviceEntity.class));
			 pushNotificationDevice.setUimsId(pushNotificationDeviceEntity.getUimsId());
			 challenge.setNotificationID(pushNotificationDeviceEntity.getUimsId());
			 pushNotificationDevice.setCustomData(gson.toJson(pushNotificationDevice.getChallenge()));
		}else {
			PushNotificationDeviceEntity pushNotificationDeviceEntity =  pushNotificationDeviceRepo.findByUIMSID(challenge.getNotificationID());
			pushNotificationDeviceEntity.setCustomData(gson.toJson(challenge));
			pushNotificationDevice.setCustomData(pushNotificationDeviceEntity.getCustomData());					    
			pushNotificationDeviceRepo.save(pushNotificationDeviceEntity);
		}
		return getPayLoadData(pushNotificationDevice);
	}
	
	private PushNotificationPayLoad getPayLoadData(PushNotificationDevice pushNotificationDevice) throws PushNotificationServiceException{
		log.info("inside getPayLoadData method::::::::::");
		try {
			// custom data or the payload needs to be encrypted with receivers public key
			log.info("Send Notification is being called");
			Gson gson = new Gson();
			PushNotificationPayLoad payload = new PushNotificationPayLoad();
			PushNotificationServiceHelper pushNotificatoinHelper = new PushNotificationServiceHelper();
			if (pushNotificationDevice.type.equalsIgnoreCase("android")) {
				
				log.info("inside android");

				log.info("pushNotificationDevice.getEncryptionAlgorithm() : "+ pushNotificationDevice.getEncryptionAlgorithm());

				log.info("pushNotificationDevice.getUserDevicePublicKey() : "+ pushNotificationDevice.getUserDevicePublicKey());

				KeyFactory kf = KeyFactory.getInstance(pushNotificationDevice.getEncryptionAlgorithm());

				X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(
						Base64Utils.decodeFromString(pushNotificationDevice.getUserDevicePublicKey()));
				PublicKey rsaPubicKey = kf.generatePublic(keySpecX509);
				SecretKey secKey = cryptoFunction.generateAES256Key();
				log.debug("getCustomData-->"+pushNotificationDevice.getCustomData());
				log.debug("key-->"+Base64Utils.encodeToString(secKey.getEncoded()));
				String initVector = Util.getAlphaNumericString(16);
				
				payload.setData(cryptoFunction.encryptWithAES(secKey, initVector, pushNotificationDevice.getCustomData(), "AES/CBC/PKCS5Padding"));
				payload.setKey(Base64Utils.encodeToString(cryptoFunction.encryptWithRSA(pushNotificationDevice.getEncryptionAlgorithm(),
						rsaPubicKey, secKey.getEncoded())));
				return payload;
			} else {

				KeyFactory kf = KeyFactory.getInstance(pushNotificationDevice.getEncryptionAlgorithm());

				X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(
						Base64Utils.decodeFromString(pushNotificationDevice.getUserDevicePublicKey()));
				PublicKey rsaPubicKey = kf.generatePublic(keySpecX509);
				
				SecretKey secKey = cryptoFunction.generateAES256Key();
				log.debug("getCustomData-->"+pushNotificationDevice.getCustomData());
				log.debug("key-->"+Base64Utils.encodeToString(secKey.getEncoded()));
				String initVector = Util.getAlphaNumericString(16);
				
				payload.setData(cryptoFunction.encryptWithAES(secKey, initVector, pushNotificationDevice.getCustomData(), "AES/CBC/PKCS5Padding"));
				payload.setKey(Base64Utils.encodeToString(cryptoFunction.encryptWithRSA(pushNotificationDevice.getEncryptionAlgorithm(),
						rsaPubicKey, secKey.getEncoded())));
				return payload;
			}
		}catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_SEND_PUSHNOTIFICATION_TO_USER_DEVICE,pushNotificationDevice.getUserName()), cause);
			throw new PushNotificationServiceException(cause, ErrorMessages.FAILED_TO_SEND_PUSHNOTIFICATION_TO_USER_DEVICE,pushNotificationDevice.getUserName());
		}
	}
	
	@Override
	public ResponseEntity<Void> addPushNotificationDevice( List<PushNotificationDevice> pushNotificationDevices ) throws PushNotificationServiceException{
		
		try{
			for(PushNotificationDevice pushNotificationDevice : pushNotificationDevices) {
				PushNotificationServiceHelper pushNotificatoinHelper = new PushNotificationServiceHelper(); 
				 //Application name and server key  from push notification config table 
				// CreatePlatformApplicationResult createPlatformApplicationResult = pushNotificatoinHelper.createPlatformApplication("ApplicationName1",Platform.GCM,pushNotificationDevice.getPrinciple(),"AIzaSyB1eDTPOWSdoJtqD5Wv7eQycn-g5I5yg28");
				try {
					CreatePlatformEndpointResult platformEndpointResult = pushNotificatoinHelper.createPlatformEndpoint(/*pushNotificationDevice.getCustomData()*/ null, pushNotificationDevice.getGcmRegistrationId(), pushNotificationDevice.getPlatformAppArn());
					 pushNotificationDevice.setAppEndPointArn(platformEndpointResult.getEndpointArn());	
				} catch (InvalidParameterException ipe) {
		            String message = ipe.getErrorMessage();
		            //System.out.println("Exception message: " + message);
		            Pattern p = Pattern
		                    .compile(".*Endpoint (arn:aws:sns[^ ]+) already exists " +
		                            "with the same Token.*");
		            Matcher m = p.matcher(message);
		            if (m.matches()) {
		                // the endpoint already exists for this token, but with
		                // additional custom data that
		                // CreateEndpoint doesn't want to overwrite. Just use the
		                // existing endpoint.
		                // endpointArn = m.group(1);
		                pushNotificationDevice.setAppEndPointArn(m.group(1));
		            } else {
		                // rethrow exception, the input is actually bad
		                throw ipe;
		            }
		        }
				
				 //pushNotificationDevice.setPlatformArn(createPlatformApplicationResult.getPlatformApplicationArn());
				 //pushNotificationDevice.setPlatformAppArn(createPlatformApplicationResult.getPlatformApplicationArn());
				 Gson gson = new Gson();
				 Challenge challenge = pushNotificationDevice.getChallenge();
				 UserAttribute userAttribute = userService.getUserAttributesByUserId(challenge.getOrganizationName(), challenge.getUserId());
				 challenge.setUserName(userAttribute.getFirstName() +" "+userAttribute.getLastName());
				 
				if(challenge.getNotificationID() == null) {
				     // TO-DO set new ID and set here 
					 pushNotificationDevice.setCustomData(gson.toJson(challenge));	
				     String json = gson.toJson(pushNotificationDevice);
					 PushNotificationDeviceEntity pushNotificationDeviceEntity =  pushNotificationDeviceRepo.save(gson.fromJson(json, PushNotificationDeviceEntity.class));
					 pushNotificationDevice.setUimsId(pushNotificationDeviceEntity.getUimsId());
					 challenge.setNotificationID(pushNotificationDeviceEntity.getUimsId());
					 pushNotificationDevice.setCustomData(gson.toJson(pushNotificationDevice.getChallenge()));
				}else {
					PushNotificationDeviceEntity pushNotificationDeviceEntity =  pushNotificationDeviceRepo.findByUIMSID(challenge.getNotificationID());
					pushNotificationDeviceEntity.setCustomData(gson.toJson(challenge));
					pushNotificationDevice.setCustomData(pushNotificationDeviceEntity.getCustomData());					    
					pushNotificationDeviceRepo.save(pushNotificationDeviceEntity);
				}
				 
			     sendUserNotification (pushNotificationDevice); 
			}		 
		     
			return new ResponseEntity<>( HttpStatus.OK );
		}catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_ADD_PUSHNOTIFICATION_DEVICE), cause);
			throw new PushNotificationServiceException(cause, ErrorMessages.FAILED_TO_ADD_PUSHNOTIFICATION_DEVICE);
		}
	}

	@Override
	public PublishResult sendUserNotification(PushNotificationDevice pushNotificationDevice) throws PushNotificationServiceException{
		try {

			// custom data or the payload needs to be encrypted with receivers public key
			log.info("Send Notification is being called");
			Gson gson = new Gson();
			PushNotificationPayLoad payload = new PushNotificationPayLoad();
			PushNotificationServiceHelper pushNotificatoinHelper = new PushNotificationServiceHelper();
			if (pushNotificationDevice.type.equalsIgnoreCase("android")) {
				KeyFactory kf = KeyFactory.getInstance(pushNotificationDevice.getEncryptionAlgorithm());

				X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(
						Base64Utils.decodeFromString(pushNotificationDevice.getUserDevicePublicKey()));
				PublicKey rsaPubicKey = kf.generatePublic(keySpecX509);
				SecretKey secKey = cryptoFunction.generateAES256Key();
				log.debug("getCustomData-->"+pushNotificationDevice.getCustomData());
				log.debug("key-->"+Base64Utils.encodeToString(secKey.getEncoded()));
				String initVector = Util.getAlphaNumericString(16);
				
				payload.setData(cryptoFunction.encryptWithAES(secKey, initVector, pushNotificationDevice.getCustomData(), "AES/CBC/PKCS5Padding"));
				payload.setKey(Base64Utils.encodeToString(cryptoFunction.encryptWithRSA(pushNotificationDevice.getEncryptionAlgorithm(),
						rsaPubicKey, secKey.getEncoded())));
				return pushNotificatoinHelper.publish(pushNotificationDevice.getAppEndPointArn(), Platform.GCM, gson.toJson(payload));
			} else {

				KeyFactory kf = KeyFactory.getInstance(pushNotificationDevice.getEncryptionAlgorithm());

				X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(
						Base64Utils.decodeFromString(pushNotificationDevice.getUserDevicePublicKey()));
				PublicKey rsaPubicKey = kf.generatePublic(keySpecX509);
				
				SecretKey secKey = cryptoFunction.generateAES256Key();
				log.debug("getCustomData-->"+pushNotificationDevice.getCustomData());
				log.debug("key-->"+Base64Utils.encodeToString(secKey.getEncoded()));
				String initVector = Util.getAlphaNumericString(16);
				
				payload.setData(cryptoFunction.encryptWithAES(secKey, initVector, pushNotificationDevice.getCustomData(), "AES/CBC/PKCS5Padding"));
				payload.setKey(Base64Utils.encodeToString(cryptoFunction.encryptWithRSA(pushNotificationDevice.getEncryptionAlgorithm(),
						rsaPubicKey, secKey.getEncoded())));
				return pushNotificatoinHelper.publish(pushNotificationDevice.getAppEndPointArn(), Platform.APNS, gson.toJson(payload));
			}
		}catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_SEND_PUSHNOTIFICATION_TO_USER_DEVICE,pushNotificationDevice.getUserName()), cause);
			throw new PushNotificationServiceException(cause, ErrorMessages.FAILED_TO_SEND_PUSHNOTIFICATION_TO_USER_DEVICE,pushNotificationDevice.getUserName());
		}
	}

	@Override
	public ResponseEntity<Void> sendSMS(String mobileNumber,UserEntity userEntity) throws PushNotificationServiceException{
		Random random = new Random();
		String otpId = String.format("%04d", random.nextInt(10000));
		String payload = "Your OTP is : "+ otpId;
		
		try {
			MessagingService messagingService = MessagingService.getInstance();
			AmazonSNSClient amazonSNSClient= messagingService.getAwsSNSClient(applicationProperties.getSms().getId(), applicationProperties.getSms().getSecrete());
			PublishResult publishResult = messagingService.sendSMSFromAWS(amazonSNSClient, mobileNumber, payload);
			if(publishResult.getMessageId() != null) {
				
				//Prepare UserOtpEntity 
				UserOtpEntity userOtpEntity = new UserOtpEntity();
				userOtpEntity.setOtp(otpId);
				userOtpEntity.setStatus(false);
				userOtpEntity.setUser(userEntity);
				userOtpEntity.setMobileNumber(mobileNumber);
				
				//Save OTP details in DB for verification 
				userOtpRepository.save(userOtpEntity);
				return new ResponseEntity<>( HttpStatus.OK );
			}
			return new ResponseEntity<>( HttpStatus.NOT_ACCEPTABLE );
		}catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_SEND_SMS_TO_USER_DEVICE,userEntity.getReferenceName()), cause);
			throw new PushNotificationServiceException(cause, ErrorMessages.FAILED_TO_SEND_SMS_TO_USER_DEVICE,userEntity.getReferenceName());
		}
	}

}
