package com.meetidentity.usermanagement.service.impl;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.service.AsynchronousService;
import com.meetidentity.usermanagement.service.helper.NotificationTask;
import com.meetidentity.usermanagement.web.feign.client.NotificationServiceFeignClient;
import com.meetidentity.usermanagement.web.rest.model.notification.DeliverReqBean;

@Service
public class AsynchronousServiceImpl implements AsynchronousService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationTask.class);
	
	@Autowired
	private NotificationServiceFeignClient nsFeignClient;

	@Override
	public void executeAsynchronously(DeliverReqBean deliverReqBean, HttpHeaders headers) throws Exception {

		LOGGER.info("inside executeAsynchronously : execute ");
		ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();
		executor.execute(new NotificationTask(deliverReqBean, headers, nsFeignClient));

		LOGGER.info("return executeAsynchronously : execute ");
	}

}
