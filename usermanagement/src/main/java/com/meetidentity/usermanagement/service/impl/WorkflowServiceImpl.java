package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.config.Constants;
import com.meetidentity.usermanagement.domain.ConfigValueEntity;
import com.meetidentity.usermanagement.domain.DeviceProfileEntity;
import com.meetidentity.usermanagement.domain.FingerTypeEntity;
import com.meetidentity.usermanagement.domain.OrgIdentityFieldEntity;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.OrganizationIdentitiesEntity;
import com.meetidentity.usermanagement.domain.RoleEntity;
import com.meetidentity.usermanagement.domain.WFDeviceProfileEntity;
import com.meetidentity.usermanagement.domain.WFGroupEntity;
import com.meetidentity.usermanagement.domain.WFMobileIDIdentityIssuanceEntity;
import com.meetidentity.usermanagement.domain.WFMobileIDIdentitySoftOTPEntity;
import com.meetidentity.usermanagement.domain.WFMobileIDStepCertConfigEntity;
import com.meetidentity.usermanagement.domain.WFMobileIDStepTrustedIdentityEntity;
import com.meetidentity.usermanagement.domain.WFMobileIdStepCertificatesEntity;
import com.meetidentity.usermanagement.domain.WFMobileIdStepVisualIDGroupConfigEntity;
import com.meetidentity.usermanagement.domain.WFStepAdjudicationGroupConfigEntity;
import com.meetidentity.usermanagement.domain.WFStepApprovalGroupConfigEntity;
import com.meetidentity.usermanagement.domain.WFStepCertConfigEntity;
import com.meetidentity.usermanagement.domain.WFStepConfigEntity;
import com.meetidentity.usermanagement.domain.WFStepDefEntity;
import com.meetidentity.usermanagement.domain.WFStepEntity;
import com.meetidentity.usermanagement.domain.WFStepRegistrationConfigEntity;
import com.meetidentity.usermanagement.domain.WfStepAppletLoadingInfoEntity;
import com.meetidentity.usermanagement.domain.WfStepChipEncodeAndVIDPrintGroupConfigEntity;
import com.meetidentity.usermanagement.domain.WfStepChipEncodeGroupConfigEntity;
import com.meetidentity.usermanagement.domain.WfStepPrintGroupConfigEntity;
import com.meetidentity.usermanagement.domain.WorkflowAttributeEntity;
import com.meetidentity.usermanagement.domain.WorkflowEntity;
import com.meetidentity.usermanagement.enums.DirectionEnum;
import com.meetidentity.usermanagement.enums.EnrollmentStepsEnum;
import com.meetidentity.usermanagement.enums.WorkflowAttributeNameEnum;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.WorkflowServiceException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.keycloak.domain.RoleRepresentation;
import com.meetidentity.usermanagement.repository.ConfigValueRepository;
import com.meetidentity.usermanagement.repository.DeviceProfileConfigRepository;
import com.meetidentity.usermanagement.repository.DeviceProfileRepository;
import com.meetidentity.usermanagement.repository.FingerTypeRepository;
import com.meetidentity.usermanagement.repository.OrgIdentityFieldRepository;
import com.meetidentity.usermanagement.repository.OrganizationIdentitiesRepository;
import com.meetidentity.usermanagement.repository.OrganizationRepository;
import com.meetidentity.usermanagement.repository.RoleRepository;
import com.meetidentity.usermanagement.repository.VisualTemplateRepository;
import com.meetidentity.usermanagement.repository.WFGroupRepository;
import com.meetidentity.usermanagement.repository.WFMobileIDIdentityIssuanceRepository;
import com.meetidentity.usermanagement.repository.WFMobileIDIdentitySoftOTPRepository;
import com.meetidentity.usermanagement.repository.WFMobileIDStepTrustedIdentityRepository;
import com.meetidentity.usermanagement.repository.WFMobileIdStepVisualIDGroupConfigRepository;
import com.meetidentity.usermanagement.repository.WFStepAdjudicationGroupConfigRepository;
import com.meetidentity.usermanagement.repository.WFStepApprovalGroupConfigRepository;
import com.meetidentity.usermanagement.repository.WFStepConfigRepository;
import com.meetidentity.usermanagement.repository.WFStepDefRepository;
import com.meetidentity.usermanagement.repository.WFStepRegistrationConfigRepository;
import com.meetidentity.usermanagement.repository.WFStepRepository;
import com.meetidentity.usermanagement.repository.WfStepChipEncodeAndVIDPrintGroupConfigRepository;
import com.meetidentity.usermanagement.repository.WfStepChipEncodeGroupConfigRepository;
import com.meetidentity.usermanagement.repository.WfStepPrintGroupConfigRepository;
import com.meetidentity.usermanagement.repository.WorkflowAttributeRepository;
import com.meetidentity.usermanagement.repository.WorkflowRepository;
import com.meetidentity.usermanagement.service.DeviceProfileServcie;
import com.meetidentity.usermanagement.service.IDProofService;
import com.meetidentity.usermanagement.service.KeycloakService;
import com.meetidentity.usermanagement.service.OrganizationService;
import com.meetidentity.usermanagement.service.UserService;
import com.meetidentity.usermanagement.service.WorkflowService;
import com.meetidentity.usermanagement.service.helper.WorkflowServiceHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.ConfigValue;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfile;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfileConfig;
import com.meetidentity.usermanagement.web.rest.model.Finger;
import com.meetidentity.usermanagement.web.rest.model.Group;
import com.meetidentity.usermanagement.web.rest.model.OrgIdentityFieldIdNamePair;
import com.meetidentity.usermanagement.web.rest.model.OrganizationIdentity;
import com.meetidentity.usermanagement.web.rest.model.RegistrationConfigDetails;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.Role;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIDIdentitySoftOTPConfig;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIDStepIdentityConfig;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIDStepTrustedIdentity;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIdStepCertificate;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIdStepVisualIDGroupConfig;
import com.meetidentity.usermanagement.web.rest.model.WFStepApprovalGroupConfig;
import com.meetidentity.usermanagement.web.rest.model.WFStepCertConfig;
import com.meetidentity.usermanagement.web.rest.model.WFStepGenericConfig;
import com.meetidentity.usermanagement.web.rest.model.WFStepRegistrationConfig;
import com.meetidentity.usermanagement.web.rest.model.WfStepAppletLoadingInfo;
import com.meetidentity.usermanagement.web.rest.model.WfStepGroupConfig;
import com.meetidentity.usermanagement.web.rest.model.Workflow;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStep;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStepDef;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStepDetail;



@Transactional
@Service
public class WorkflowServiceImpl implements WorkflowService {
	
	private final Logger log = LoggerFactory.getLogger(WorkflowServiceImpl.class);

	@Autowired
	private WorkflowRepository workflowRepository;
	
	@Autowired
	private WFGroupRepository wfGroupRepository;	
	
	@Autowired
	private WFStepRepository wfStepRepository;

	@Autowired
	private WFStepDefRepository wfStepDefRepository;
	
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private OrgIdentityFieldRepository orgIdentityFieldRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private DeviceProfileRepository devProfRepository;
	
	@Autowired
	private ConfigValueRepository configValueRepository;
	
	@Autowired
	private VisualTemplateRepository visualTemplateRepository;
	
	@Autowired
	private DeviceProfileServcie deviceProfileServcie;
	
	@Autowired
	private KeycloakService keycloakService;
	
	@Autowired
	private FingerTypeRepository fingerTypeRepository;
	
	@Autowired
	private OrganizationIdentitiesRepository organizationIdentitiesRepository;
	
	@Autowired
	private WFStepRegistrationConfigRepository wfStepRegistrationConfigRepository;
	
	@Autowired
	private WFStepConfigRepository wfStepConfigRepository;
	
	@Autowired
	private WFMobileIDStepTrustedIdentityRepository wfMobileIDStepTrustedIdentityRepository;
	
	@Autowired
	private WFStepAdjudicationGroupConfigRepository wfStepAdjudicationGroupConfigRepository;
	
	@Autowired
	private WFStepApprovalGroupConfigRepository wfStepApprovalGroupConfigRepository;
	
	@Autowired
	private WfStepChipEncodeGroupConfigRepository wfStepChipEncodeGroupConfigRepository;
	
	@Autowired
	private WfStepChipEncodeAndVIDPrintGroupConfigRepository wfStepChipEncodeVIDPrintGroupConfigRepository;
	
	@Autowired
	private WfStepPrintGroupConfigRepository wfStepPrintGroupConfigRepository;
	
	@Autowired
	private WFMobileIdStepVisualIDGroupConfigRepository wfMobileIdStepVIDGroupConfigRepository;
	
	@Autowired
	private WFMobileIDIdentityIssuanceRepository wfMobileIDIdentityIssuanceRepository;
	
	@Autowired
	private WFMobileIDIdentitySoftOTPRepository wfMobileIDIdentitySoftOTPRepository;
	
	@Autowired
	private WorkflowServiceHelper workflowServiceHelper;
	
	@Autowired
	private DeviceProfileConfigRepository deviceProfileConfigRepository;

	@Autowired
	private WorkflowAttributeRepository workflowAttributeRepository;
	
	@Autowired
	private IDProofService idProofTypeService;
	
	@Autowired
	private OrganizationService organizationService;	
	
	@Autowired
	private UserService userService;	
	
	@Override
	public List<WorkflowStepDef> getWorkflowStepDefs() throws WorkflowServiceException {
		List<WorkflowStepDef> workflowStepDefs = new ArrayList<>();
		try {
			wfStepDefRepository.findAll().forEach(wfStepDefEntity -> {
				WorkflowStepDef workflowStepDef = new WorkflowStepDef();
				workflowStepDef.setName(wfStepDefEntity.getName());
				workflowStepDef.setDescription(wfStepDefEntity.getDescription());
				workflowStepDef.setDisplayName(wfStepDefEntity.getDisplayName());
				workflowStepDef.setOrder(wfStepDefEntity.getStepDefOrder());

				workflowStepDefs.add(workflowStepDef);
			});
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					ErrorMessages.FAILED_TO_GET_WORKFLOW_STEP_DEFINITIONS + " : " + cause.getMessage());
			throw new WorkflowServiceException(ErrorMessages.FAILED_TO_GET_WORKFLOW_STEP_DEFINITIONS, cause);
		}

		return workflowStepDefs;
	}
	
	@Override
	public Workflow getWorkflowByRole(String organizationName, String roleName) throws WorkflowServiceException {
		Workflow workflow = new Workflow();
		try {
			workflowRepository.findWorkflowByRole(organizationName, roleName).ifPresent(entity -> {
				workflow.setName(entity.getName());
				workflow.setDescription(entity.getDescription());
				OrganizationIdentity orgIdentity = new OrganizationIdentity();
				orgIdentity.setIdentityTypeName(entity.getOrgIdentityType().getIdentityType().getName());
				workflow.setOrganizationIdentities(orgIdentity);
//				 workflow.setOrganizationIdentities(new OrganizationIdentity(entity.getOrgIdentityType().getIdentityType().getId(), entity.getOrgIdentityType().getIdentityType().getName()));
				workflow.setAllowedDevices(entity.getAllowedDevices());
				if(entity.getAllowedDevices() == null) {
					workflow.setAllowedDevicesApplicable(false);
				} else {
					workflow.setAllowedDevicesApplicable(true);
				}
				workflow.setExpirationInMonths(entity.getExpirationInMonths());
				workflow.setExpirationInDays(entity.getExpirationInDays());
				workflow.setOrganizationIdentities(new OrganizationIdentity(entity.getOrgIdentityType().getIdentityType().getId(), entity.getOrgIdentityType().getIdentityType().getName(), entity.getOrgIdentityType().getIdentityType().getNotes()));
				workflow.setDisableExpirationEnforcement(entity.getDisableExpirationEnforcement());
				workflow.setCeatedDate(entity.getCreatedDate());

				List<WorkflowAttributeEntity>  attributes = entity.getWorkflowAttributes();
				if(attributes != null && attributes.size() > 0) {
					for (WorkflowAttributeEntity attribute : attributes) {
						if(attribute.getName().equalsIgnoreCase(WorkflowAttributeNameEnum.DEFAULT_WORKFLOW.getName())){
							workflow.setDefaultWorkflow(true);
							break;
						}
					}
				}
			});
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_WORKFLOW_BY_ROLE_UNDER_ORGANIZATION, roleName, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_WORKFLOW_BY_ROLE_UNDER_ORGANIZATION, roleName, organizationName);
		}

		return workflow;
	}

	@Override
	public List<WorkflowStep> getStepsByWorkflow(String organizationName, String workflowName) throws WorkflowServiceException {
		List<WorkflowStep> workflowSteps = new ArrayList<>();
		try {
			wfStepRepository.findStepsByWorkflow(organizationName, workflowName).forEach(wfStepEntity -> {
				WorkflowStep workflowStep = new WorkflowStep();
				workflowStep.setName(wfStepEntity.getWfStepDef().getName());
				workflowStep.setDescription(wfStepEntity.getWfStepDef().getDescription());
				workflowStep.setDisplayName(wfStepEntity.getWfStepDef().getDisplayName());
				workflowStep.setOrder(wfStepEntity.getWfStepDef().getStepDefOrder());

				if (wfStepEntity.getWfStepDef().getName().equalsIgnoreCase(EnrollmentStepsEnum.FINGERPRINT_CAPTURE.getEnrollmentStep())) {
					wfStepEntity.getWfStepConfigs().stream().forEach(wfStepConfigEntity -> {
						WFStepGenericConfig wfStepGenericConfig = new WFStepGenericConfig();

						wfStepGenericConfig.setName(wfStepConfigEntity.getConfigValue().getConfig().getName());
						wfStepGenericConfig.setValue(wfStepConfigEntity.getConfigValue().getValue());

						workflowStep.getWfStepGenericConfigs().add(wfStepGenericConfig);
					});
				}
				workflowSteps.add(workflowStep);
			});
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_WORKFLOW_STEPS_OF_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_WORKFLOW_STEPS_OF_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName);
		}
		return workflowSteps;
	}
	
	@Override
	public List<OrganizationIdentity> getTrustedDocumentsForSelfServiceOnboard(String organizationName, String workflowName) throws WorkflowServiceException {
		List<OrganizationIdentity> wfMobileIDStepTrustedIdentities = new ArrayList<>();
		try {
			wfMobileIDStepTrustedIdentityRepository.findSelfServiceTrustedIdentitiesByWorkflow(organizationName, workflowName).forEach(wfMobileIDStepTrustedIdentityEntity -> {
				WFMobileIDStepTrustedIdentity wfMobileIDStepTrustedIdentity = new WFMobileIDStepTrustedIdentity();
//				WorkflowStep workflowStep = new WorkflowStep();
//				workflowStep.setName(wfMobileIDStepTrustedIdentityEntity.getWfStep().getWfStepDef().getName());
//				workflowStep.setDescription(wfMobileIDStepTrustedIdentityEntity.getWfStep().getWfStepDef().getDescription());
//				workflowStep.setDisplayName(wfMobileIDStepTrustedIdentityEntity.getWfStep().getWfStepDef().getDisplayName());
//				workflowStep.setOrder(wfMobileIDStepTrustedIdentityEntity.getWfStep().getStepOrder());
//				wfMobileIDStepTrustedIdentity.setWfStep(workflowStep);
				
				OrganizationIdentitiesEntity orgIddentityEntity = wfMobileIDStepTrustedIdentityEntity.getOrganizationIdentities();
				OrganizationIdentity orgIdentity = new OrganizationIdentity();
				orgIdentity.setId(orgIddentityEntity.getId());
				orgIdentity.setIdentityTypeName(orgIddentityEntity.getIdentityType().getName());
				orgIdentity.setIdentityTypeDetails(orgIddentityEntity.getIdentityType().getNotes());
				wfMobileIDStepTrustedIdentity.setOrganizationIdentity(orgIdentity);
				
				wfMobileIDStepTrustedIdentity.setId(wfMobileIDStepTrustedIdentityEntity.getId());
				
				wfMobileIDStepTrustedIdentities.add(orgIdentity);

			});
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_WORKFLOW_STEPS_OF_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_WORKFLOW_STEPS_OF_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName);
		}
		return wfMobileIDStepTrustedIdentities;
	}
	
	@Override
	public WorkflowStepDetail getStepDetails(String organizationName, String workflowName, String stepName) throws WorkflowServiceException {
		WorkflowStepDetail workflowStepDetail = new WorkflowStepDetail();
		try {
			wfStepRepository.findStepDetailByName(organizationName, workflowName, stepName).ifPresent(wfStepEntity -> {
				// TODO: Refactor code to reduce n+1 jpa query calls to db
				wfStepEntity.getWfStepConfigs().forEach(wfStepConfigEntity -> {
					WFStepGenericConfig wfStepGenericConfig = new WFStepGenericConfig();
					wfStepGenericConfig.setName(wfStepConfigEntity.getConfigValue().getConfig().getName());
					wfStepGenericConfig.setValue(wfStepConfigEntity.getConfigValue().getValue());
					
					workflowStepDetail.getWfStepGenericConfigs().add(wfStepGenericConfig);
				});
				
				wfStepEntity.getWfStepRegistrationConfigs().forEach(wfStepRegEntity -> {
					OrgIdentityFieldEntity orgIdentityFieldEntity = wfStepRegEntity.getOrgIdentityField();
					
					WFStepRegistrationConfig wfStepRegistrationConfig = new WFStepRegistrationConfig();
					wfStepRegistrationConfig.setId(wfStepRegEntity.getId());
					wfStepRegistrationConfig.setOrgFieldId(wfStepRegEntity.getOrgIdentityField().getId());
					wfStepRegistrationConfig.setFieldName(wfStepRegEntity.getOrgIdentityField().getIdentityField().getName());
					wfStepRegistrationConfig.setFieldLabel(wfStepRegEntity.getFieldLabel());
					wfStepRegistrationConfig.setRequired(wfStepRegEntity.getRequired());
					wfStepRegistrationConfig.setType(orgIdentityFieldEntity.getIdentityField().getType());
					
					workflowStepDetail.getWfStepRegistrationConfigs().add(wfStepRegistrationConfig);
				});
				
				wfStepEntity.getWfStepApprovalGroupConfigs().forEach(wfStepApprovalGroupConfigEntity -> {
					WFStepApprovalGroupConfig wfStepApprovalGroupConfig = new WFStepApprovalGroupConfig();
					wfStepApprovalGroupConfig.setGroupId(wfStepApprovalGroupConfigEntity.getReferenceApprovalGroupId());
					wfStepApprovalGroupConfig.setGroupName(wfStepApprovalGroupConfigEntity.getReferenceApprovalGroupName());
					
					workflowStepDetail.getWfStepApprovalGroupConfigs().add(wfStepApprovalGroupConfig);
				});
				
				if (wfStepEntity.getWfAppletLoadingInfo() != null) {
					WfStepAppletLoadingInfo wfAppletLoadingInfo = new WfStepAppletLoadingInfo();

					WfStepAppletLoadingInfoEntity wfAppletLoadingInfoEntity = wfStepEntity.getWfAppletLoadingInfo();

					wfAppletLoadingInfo.setPivAppletEnabled(wfAppletLoadingInfoEntity.getPivAppletEnabled());
					wfAppletLoadingInfo.setFido2AppletEnabled(wfAppletLoadingInfoEntity.getFido2AppletEnabled());
					wfAppletLoadingInfo.setAttestationCert(wfAppletLoadingInfoEntity.getAttestationCert());
					wfAppletLoadingInfo.setAttestationCertPrivateKey(wfAppletLoadingInfoEntity.getAttestationCertPrivateKey());
					wfAppletLoadingInfo.setAaguid(wfAppletLoadingInfoEntity.getAaguid());

					workflowStepDetail.setWfStepAppletLoadingInfo(wfAppletLoadingInfo);
				}
				
				wfStepEntity.getWfStepPrintGroupConfigs().forEach(wfStepPrintGroupConfigEntity -> {
					WfStepGroupConfig wfStepPrintGroupConfig = new WfStepGroupConfig();
					wfStepPrintGroupConfig.setGroupId(wfStepPrintGroupConfigEntity.getReferencePrintGroupId());
					wfStepPrintGroupConfig.setGroupName(wfStepPrintGroupConfigEntity.getReferencePrintGroupName());
					
					workflowStepDetail.getWfStepPrintGroupConfigs().add(wfStepPrintGroupConfig);
				});
				
				wfStepEntity.getWfStepChipEncodeGroupConfigs().stream().forEach(wfStepChipEncodeGroupConfig -> {
					WfStepGroupConfig wfStepGroupConfig = new WfStepGroupConfig();
					wfStepGroupConfig.setGroupId(wfStepChipEncodeGroupConfig.getReferenceChipEncodeGroupId());
					wfStepGroupConfig.setGroupName(wfStepChipEncodeGroupConfig.getReferenceChipEncodeGroupName());

					workflowStepDetail.getWfStepChipEncodeGroupConfigs().add(wfStepGroupConfig);
				});
				
				wfStepEntity.getWfStepChipEncodeVIDPrintGroupConfigs().stream().forEach(wfStepChipEncodeVIDGroupConfig -> {
					WfStepGroupConfig wfStepGroupConfig = new WfStepGroupConfig();
					wfStepGroupConfig.setGroupId(wfStepChipEncodeVIDGroupConfig.getReferenceChipEncodeVIDPrintGroupId());
					wfStepGroupConfig.setGroupName(wfStepChipEncodeVIDGroupConfig.getReferenceChipEncodeVIDPrintGroupName());

					workflowStepDetail.getWfStepChipEncodeVIDPrintGroupConfigs().add(wfStepGroupConfig);
				});
				
				wfStepEntity.getWfStepCertConfigs().forEach(wfStepCertEntity -> {
					if(wfStepCertEntity.getStatus().equalsIgnoreCase("ACTIVE")) {
						WFStepCertConfig wfStepCertConfig = new WFStepCertConfig();
						wfStepCertConfig.setId(wfStepCertEntity.getId());
						wfStepCertConfig.setCertType(wfStepCertEntity.getCertType());
						wfStepCertConfig.setCaServer(wfStepCertEntity.getCaServer());
						wfStepCertConfig.setCertTemplate(wfStepCertEntity.getCertTemplate());
						wfStepCertConfig.setKeyEscrow(wfStepCertEntity.getKeyEscrow());
						wfStepCertConfig.setDisableRevoke(wfStepCertEntity.getDisableRevoke());
						wfStepCertConfig.setAlgorithm(wfStepCertEntity.getAlgorithm());
						wfStepCertConfig.setKeysize(wfStepCertEntity.getKeySize());
						wfStepCertConfig.setSubjectDN(wfStepCertEntity.getSubjectDN());
						wfStepCertConfig.setSubjectAN(wfStepCertEntity.getSubjectAN());
										
						workflowStepDetail.getWfStepCertConfigs().add(wfStepCertConfig);
					}
				});
				wfStepEntity.getWfMobileIDStepTrustedIdentities().forEach(trustedExistingIdentity -> {
					OrganizationIdentity orgIdentity = new OrganizationIdentity();
					OrganizationIdentitiesEntity orgIdEntity = trustedExistingIdentity.getOrganizationIdentities();
					orgIdentity.setId(orgIdEntity.getId());
					orgIdentity.setIdentityTypeName(orgIdEntity.getIdentityType().getName());
					orgIdentity.setIdentityTypeDetails(orgIdEntity.getIdentityType().getNotes());
					
					workflowStepDetail.getWfMobileIDStepTrustedIdentities().add(orgIdentity);
				});
				wfStepEntity.getWfStepAdjudicationGroupConfigs().forEach(adjudicationGroup -> {
					Group group = new Group();
					group.setId(adjudicationGroup.getReferenceAdjudicationGroupId());
					group.setName(adjudicationGroup.getReferenceAdjudicationGroupName());
					
					workflowStepDetail.getWfStepAdjudicationGroupConfigs().add(group);
				});
				wfStepEntity.getWfMobileIDStepIdentityConfigs().forEach(wfMobileIDStepIdentity -> {
					WFMobileIDStepIdentityConfig wfMobileIDStepIdentityConfig = new WFMobileIDStepIdentityConfig();
					
					wfMobileIDStepIdentityConfig.setWfMobileIDStepIdentityConfigId(wfMobileIDStepIdentity.getId());
					wfMobileIDStepIdentityConfig.setFriendlyName(wfMobileIDStepIdentity.getFriendlyName());
					wfMobileIDStepIdentityConfig.setPushVerify(wfMobileIDStepIdentity.getPushVerify());
					wfMobileIDStepIdentityConfig.setSoftOTP(wfMobileIDStepIdentity.getSoftOTP());
					wfMobileIDStepIdentityConfig.setFido2(wfMobileIDStepIdentity.getFido2());
					wfMobileIDStepIdentityConfig.setIsContentSigning(wfMobileIDStepIdentity.getIsContentSigning());

					if(wfMobileIDStepIdentity.getSoftOTP() == true) {
						WFMobileIDIdentitySoftOTPEntity WFMobileIDIdentitySoftOTPEntity =  wfMobileIDStepIdentity.getWFMobileIDIdentitySoftOTPEntity();
						WFMobileIDIdentitySoftOTPConfig WFMobileIDIdentitySoftOTPConfig = new WFMobileIDIdentitySoftOTPConfig();
						WFMobileIDIdentitySoftOTPConfig.setId(WFMobileIDIdentitySoftOTPEntity.getId());
						WFMobileIDIdentitySoftOTPConfig.setAlgorithm(WFMobileIDIdentitySoftOTPEntity.getAlgorithm());
						WFMobileIDIdentitySoftOTPConfig.setCounter(WFMobileIDIdentitySoftOTPEntity.getCounter());
						WFMobileIDIdentitySoftOTPConfig.setDigits(WFMobileIDIdentitySoftOTPEntity.getDigits());
						WFMobileIDIdentitySoftOTPConfig.setInterval(WFMobileIDIdentitySoftOTPEntity.getPeriod());
						WFMobileIDIdentitySoftOTPConfig.setSecretKey(WFMobileIDIdentitySoftOTPEntity.getSecretKey());
						WFMobileIDIdentitySoftOTPConfig.setSoftOTPType(WFMobileIDIdentitySoftOTPEntity.getSoftOtpType());
						
						wfMobileIDStepIdentityConfig.setWfMobileIDIdentitySoftOTPConfig(WFMobileIDIdentitySoftOTPConfig);
						
					}
					
					
					wfMobileIDStepIdentity.getWfMobileIdStepVisualIDGroupConfigs().forEach(wfMobileVisualIdConfig -> {
						WFMobileIdStepVisualIDGroupConfig wfMobileIdStepVisualIDGroupConfig = new WFMobileIdStepVisualIDGroupConfig();
						
						wfMobileIdStepVisualIDGroupConfig.setId(wfMobileVisualIdConfig.getId());
						wfMobileIdStepVisualIDGroupConfig.setReferenceGroupId(wfMobileVisualIdConfig.getReferenceGroupId());
						wfMobileIdStepVisualIDGroupConfig.setReferenceGroupName(wfMobileVisualIdConfig.getReferenceGroupName());
						wfMobileIdStepVisualIDGroupConfig.setVisualTemplateId(wfMobileVisualIdConfig.getVisualTemplate().getId());
						wfMobileIdStepVisualIDGroupConfig.setIdentityTypeName(wfMobileVisualIdConfig.getVisualTemplate().getOrgIdentityType().getIdentityType().getName());
						wfMobileIdStepVisualIDGroupConfig.setWorkflowMobileIdentityIssuanceId(wfMobileVisualIdConfig.getWfMobileIDIdentitiesIssuance().getId());
						
						wfMobileIDStepIdentityConfig.addWfMobileIdStepVisualIDGroupConfigs(wfMobileIdStepVisualIDGroupConfig);
					});
					 if(wfMobileIDStepIdentity.getWfMobileIdStepCertificates().size() > 0) {
						 wfMobileIDStepIdentity.getWfMobileIdStepCertificates().forEach(wfMobileIDStepCerts -> {
								WFMobileIdStepCertificate wfMobileIdStepCertConfig = new WFMobileIdStepCertificate();
								if(wfMobileIDStepCerts.getStatus().equalsIgnoreCase("ACTIVE")) {
									wfMobileIDStepIdentityConfig.setPkiCertificatesEnabled(true);
									wfMobileIdStepCertConfig.setWfMobileIdStepCertId(wfMobileIDStepCerts.getId());
									wfMobileIdStepCertConfig.setCertType(wfMobileIDStepCerts.getCertType());
									wfMobileIdStepCertConfig.setCaServer(wfMobileIDStepCerts.getCaServer());
									wfMobileIdStepCertConfig.setCertTemplate(wfMobileIDStepCerts.getCertTemplate());
									wfMobileIdStepCertConfig.setKeyEscrow(wfMobileIDStepCerts.getKeyEscrow());
									wfMobileIdStepCertConfig.setDisableRevoke(wfMobileIDStepCerts.getDisableRevoke());
									wfMobileIdStepCertConfig.setAlgorithm(wfMobileIDStepCerts.getAlgorithm());
									wfMobileIdStepCertConfig.setKeysize(wfMobileIDStepCerts.getKeySize());
									wfMobileIdStepCertConfig.setSubjectDN(wfMobileIDStepCerts.getSubjectDN());
									wfMobileIdStepCertConfig.setSubjectAN(wfMobileIDStepCerts.getSubjectAN());
									wfMobileIDStepIdentityConfig.addWfMobileIdStepCertificate(wfMobileIdStepCertConfig);
								}
							});
						 WFMobileIDStepCertConfigEntity wfMobileIDStepCertConfig = wfMobileIDStepIdentity.getWfMobileIDStepCertConfig();
							//wfMobileIDStepIdentityConfig.getWfMobileIDStepCertConfig().setIsContentSigning(wfMobileIDStepCertConfig.getIsContentSigning());
							
							WFStepGenericConfig noOfDayBeforeCertExpiryConfigValue = new WFStepGenericConfig();
							noOfDayBeforeCertExpiryConfigValue.setName(wfMobileIDStepCertConfig.getNoOfDaysBeforeCertExpiry().getConfig().getName());
							noOfDayBeforeCertExpiryConfigValue.setValue(wfMobileIDStepCertConfig.getNoOfDaysBeforeCertExpiry().getValue());
							
							wfMobileIDStepIdentityConfig.getWfMobileIDStepCertConfig().setNoOfDaysBeforeCertExpiry(noOfDayBeforeCertExpiryConfigValue);
							
							WFStepGenericConfig emailNotificationConfigValue = new WFStepGenericConfig();
							emailNotificationConfigValue.setName(wfMobileIDStepCertConfig.getEmailNotificationFreq().getConfig().getName());
							emailNotificationConfigValue.setValue(wfMobileIDStepCertConfig.getEmailNotificationFreq().getValue());
							
							wfMobileIDStepIdentityConfig.getWfMobileIDStepCertConfig().setEmailNotificationFreq(emailNotificationConfigValue);
					 } else {
						 wfMobileIDStepIdentityConfig.setPkiCertificatesEnabled(false);
					 }
					
					workflowStepDetail.getWfMobileIDStepIdentityConfigs().add(wfMobileIDStepIdentityConfig);
				});
					
				
			});
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_STEP_DETAILS_OF_WORKFLOW_STEP_UNDER_ORGANIZATION, stepName, workflowName, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_STEP_DETAILS_OF_WORKFLOW_STEP_UNDER_ORGANIZATION, stepName, workflowName, organizationName);
		}
		return workflowStepDetail;
	}
	
	@Override
	public WorkflowStep getMobileIdentityIssuanceDetails(String organizationName, String workflowName) throws WorkflowServiceException {
		WorkflowStep workflowStep = new WorkflowStep();
		String stepName = "MOBILE_ID_IDENTITY_ISSUANCE";
		try {
			wfStepRepository.findStepDetailByName(organizationName, workflowName, stepName).ifPresent(wfStepEntity -> {
				// TODO: Refactor code to reduce n+1 jpa query calls to db

				wfStepEntity.getWfMobileIDStepIdentityConfigs().forEach(wfMobileIDStepIdentity -> {
					WFMobileIDStepIdentityConfig wfMobileIDStepIdentityConfig = new WFMobileIDStepIdentityConfig();

					wfMobileIDStepIdentityConfig.setWfMobileIDStepIdentityConfigId(wfMobileIDStepIdentity.getId());
					wfMobileIDStepIdentityConfig.setFriendlyName(wfMobileIDStepIdentity.getFriendlyName());
					wfMobileIDStepIdentityConfig.setPushVerify(wfMobileIDStepIdentity.getPushVerify());
					wfMobileIDStepIdentityConfig.setSoftOTP(wfMobileIDStepIdentity.getSoftOTP());
					wfMobileIDStepIdentityConfig.setFido2(wfMobileIDStepIdentity.getFido2());
					wfMobileIDStepIdentityConfig.setIsContentSigning(wfMobileIDStepIdentity.getIsContentSigning());
					if (wfMobileIDStepIdentity.getWfMobileIdStepCertificates().size() > 0) {
						wfMobileIDStepIdentityConfig.setPkiCertificatesEnabled(true);
					} else {
						wfMobileIDStepIdentityConfig.setPkiCertificatesEnabled(false);
					}
					workflowStep.getWfMobileIDStepIdentityConfigs().add(wfMobileIDStepIdentityConfig);
				});

			});
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_STEP_DETAILS_OF_WORKFLOW_STEP_UNDER_ORGANIZATION, stepName, workflowName, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_STEP_DETAILS_OF_WORKFLOW_STEP_UNDER_ORGANIZATION, stepName, workflowName, organizationName);
		}
		return workflowStep;
	}
	
	@Override
	public boolean checkIfWFStepApprovalIsEnabled(String organizationName, String groupID, String groupName, String stepName) throws WorkflowServiceException {
		try {

			WFGroupEntity wfGroupEntity = workflowRepository.findWorkflowGroup(organizationName, groupID);
			List<WFStepEntity> wfSteps = wfGroupEntity.getWorkflow().getWfSteps();
			for (WFStepEntity wfStepEntity : wfSteps) {
				if(wfStepEntity.getWfStepApprovalGroupConfigs() != null) {
					return true;
				}
			}
			return false;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_STEP_DETAILS_OF_WORKFLOW_STEP_FOR_GROUP_UNDER_ORGANIZATION, stepName, groupName, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_STEP_DETAILS_OF_WORKFLOW_STEP_FOR_GROUP_UNDER_ORGANIZATION, stepName, groupName, organizationName);
		}
	}

	@Override
	public List<DeviceProfile> getWorkflowDevProfiles(String organizationName, String wfName) throws WorkflowServiceException {
		List<DeviceProfileEntity> deviceProfileEntities = null;
		List<DeviceProfile> deviceProfileList = new ArrayList<>();
		try{
			deviceProfileEntities = devProfRepository.findWorkflowDevProfiles(organizationName, wfName);
			for(DeviceProfileEntity deviceProfileEntity: deviceProfileEntities) {
				deviceProfileList.add(deviceProfileServcie.getDevProfFromEntity(deviceProfileEntity));		
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_DEVICEPROFILES_FOR_WORKFLOW_UNDER_ORGANIZATION, wfName, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_DEVICEPROFILES_FOR_WORKFLOW_UNDER_ORGANIZATION, wfName, organizationName);
		}
		return deviceProfileList;
	}

	@Override
	public ResultPage getWorkflows(String organizationName, String sortBy, String order, int page, int size) throws WorkflowServiceException {
		List<Workflow> workflows = new ArrayList<>();
		ResultPage resultPage = null;
		Sort sort = null; Direction direction = null;
		
		if(order.equalsIgnoreCase(DirectionEnum.ASC.getDirectionCode())) {
			direction = Direction.ASC;
		} else if(order.equalsIgnoreCase(DirectionEnum.DESC.getDirectionCode())) {
			direction = Direction.DESC;
		}
		
		if (direction != null && !sortBy.equalsIgnoreCase("undefined") && !(order.isEmpty())) {
			if (sortBy.equalsIgnoreCase("groups")) {
				sortBy = "wfGroups.referenceGroupName";
				sort = new Sort(direction, sortBy).and(new Sort(direction, "name"));
			} else {
				sort = new Sort(new Sort.Order(direction, sortBy));
			}
		}
		try {
			Pageable pageable = new PageRequest(page, size, sort);
			Page<WorkflowEntity> pagedResult = workflowRepository.findWorkflowsByOrganization(organizationName, pageable);
			if (pagedResult.getTotalElements() == 0) {
			/*	log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_UNDER_ORGANIZATION, organizationName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_UNDER_ORGANIZATION, organizationName));*/
			} else if (pagedResult.getTotalElements() != 0) {
				pagedResult.forEach(workflowEntity -> {
					Workflow workflow = new Workflow();
					workflow.setName(workflowEntity.getName());
					workflow.setDescription(workflowEntity.getDescription());
					workflow.setDeviceProfiles(getWorkflowDeviceProfiles(organizationName, workflowEntity.getName()));
					workflow.setGroups(getWorkflowGroups(workflowEntity));
					workflow.setAllowedDevices(workflowEntity.getAllowedDevices());
					workflow.setOrganizationIdentities(new OrganizationIdentity(workflowEntity.getOrgIdentityType().getIdentityType().getId(), workflowEntity.getOrgIdentityType().getIdentityType().getName(), workflowEntity.getOrgIdentityType().getIdentityType().getNotes()));
					// TODO Determine Logic to perform Edit and Delete Operations. Hardcoding to  true by default
					workflow.setCanEdit(true);
					workflow.setCanDelete(true);
					List<WorkflowAttributeEntity>  attributes = workflowEntity.getWorkflowAttributes();
					if(attributes != null && attributes.size() > 0) {
						for (WorkflowAttributeEntity attribute : attributes) {
							if(attribute.getName().equalsIgnoreCase(WorkflowAttributeNameEnum.DEFAULT_WORKFLOW.getName())){
								workflow.setDefaultWorkflow(true);
								break;
							}
						}
					}
	
					workflows.add(workflow);
				});
				resultPage = new ResultPage();
				resultPage.setContent(workflows);
				resultPage.setFirst(pagedResult.isFirst());
				resultPage.setLast(pagedResult.isLast());
				resultPage.setNumber(pagedResult.getNumber());
				resultPage.setNumberOfElements(pagedResult.getNumberOfElements());
				resultPage.setSize(pagedResult.getSize());
				resultPage.setSort(pagedResult.getSort());
				resultPage.setTotalElements(pagedResult.getTotalElements());
				resultPage.setTotalPages(pagedResult.getTotalPages());
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_WORKFLOWS_UNDER_ORGANIZATION, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_WORKFLOWS_UNDER_ORGANIZATION, organizationName);
		}
		return resultPage;
	}
	

	@Override
	public Workflow getWorkflowByName(String organizationName, String workflowName) throws WorkflowServiceException {
		Workflow workflow = new Workflow();
		try{
			WorkflowEntity workflowEntity = workflowRepository.findByName(organizationName, workflowName);
			if (workflowEntity == null) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_WORKFLOWNAME_UNDER_ORGANIZATION, workflowName, organizationName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_WORKFLOWNAME_UNDER_ORGANIZATION, workflowName, organizationName));
			}
			workflow.setName(workflowEntity.getName());
			workflow.setDescription(workflowEntity.getDescription());
			workflow.setDeviceProfiles(setWorkflowDeviceProfiles(workflowEntity, organizationName));
			workflow.setGroups(getWorkflowGroups(workflowEntity));
			workflow.setOrganizationIdentities(setOrgIdentity(workflowEntity));
			workflow.setAllowedDevices(workflowEntity.getAllowedDevices());
			workflow.setExpirationInMonths(workflowEntity.getExpirationInMonths());
			workflow.setExpirationInDays(workflowEntity.getExpirationInDays());
			workflow.setDisableExpirationEnforcement(workflowEntity.getDisableExpirationEnforcement());
			workflow.setRevokeEncryptionCertificate(workflowEntity.getRevokeEncryptionCertificate());
			workflow.setRequiredEnrollment(workflowEntity.getRequiredEnrollment());
			workflow.setRequiredTokenCardIssuance(workflowEntity.getRequiredTokenCardIssuance());
			workflow.setRequiredMobileIssuance(workflowEntity.getRequiredMobileIssuance());

			List<WorkflowAttributeEntity>  attributes = workflowEntity.getWorkflowAttributes();
			if(attributes != null && attributes.size() > 0) {
				for (WorkflowAttributeEntity attribute : attributes) {
					if(attribute.getName().equalsIgnoreCase(WorkflowAttributeNameEnum.DEFAULT_WORKFLOW.getName())){
						workflow.setDefaultWorkflow(true);
						break;
					}
				}
			}
			
			List<WorkflowStep> workflowSteps = getStepsByWorkflow(organizationName, workflowName);
			workflowSteps.stream().forEach(workflowStep -> {
				WorkflowStepDetail workflowStepDetail = getStepDetails(organizationName, workflowName, workflowStep.getName());
				workflowStep.setWfStepGenericConfigs(workflowStepDetail.getWfStepGenericConfigs());
				workflowStep.setWfStepRegistrationConfigs(workflowStepDetail.getWfStepRegistrationConfigs());
				workflowStep.setWfStepApprovalGroupConfigs(workflowStepDetail.getWfStepApprovalGroupConfigs());
				workflowStep.setWfStepChipEncodeGroupConfigs(workflowStepDetail.getWfStepChipEncodeGroupConfigs());
				workflowStep.setWfStepChipEncodeVIDPrintGroupConfigs(workflowStepDetail.getWfStepChipEncodeVIDPrintGroupConfigs());
				workflowStep.setWfStepPrintGroupConfigs(workflowStepDetail.getWfStepPrintGroupConfigs());
				workflowStep.setWfStepCertConfigs(workflowStepDetail.getWfStepCertConfigs());
				workflowStep.setWfMobileIDStepIdentityConfigs(workflowStepDetail.getWfMobileIDStepIdentityConfigs());
				workflowStep.setWfMobileIDStepTrustedIdentities(workflowStepDetail.getWfMobileIDStepTrustedIdentities());
				workflowStep.setWfStepAdjudicationGroupConfigs(workflowStepDetail.getWfStepAdjudicationGroupConfigs());
				workflowStep.setWfStepAppletLoadingInfo(workflowStepDetail.getWfStepAppletLoadingInfo());
			});
			
			workflow.setWorkflowSteps(workflowSteps);
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_WORKFLOW_WITH_WORKFLOWNAME_UNDER_ORGANIZATION, workflowName, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_WORKFLOW_WITH_WORKFLOWNAME_UNDER_ORGANIZATION,workflowName, organizationName);
		}
		return workflow;
	}
	
	public List<DeviceProfile> setWorkflowDeviceProfiles(WorkflowEntity workflowEntity, String organizationName) {
		List<DeviceProfile> deviceProfiles = new ArrayList<>();
		try{
			workflowEntity.getWfDeviceProfiles().stream().forEach(wfDeviceProfileEntity -> {
				DeviceProfile deviceProfile = new DeviceProfile();
				deviceProfile.setName(wfDeviceProfileEntity.getDeviceProfile().getName());
				ConfigValueEntity cvEntityAppletType = wfDeviceProfileEntity.getDeviceProfile().getConfigValueProductName();
				ConfigValue cvAppletType = new ConfigValue();
				cvAppletType.setId(cvEntityAppletType.getId());
				cvAppletType.setValue(cvEntityAppletType.getValue());
				
				ConfigValueEntity cvEntityCategory = wfDeviceProfileEntity.getDeviceProfile().getConfigValueCategory();
				ConfigValue cvCategory = new ConfigValue();
				cvCategory.setId(cvEntityCategory.getId());
				cvCategory.setValue(cvEntityCategory.getValue());
				
				deviceProfile.setConfigValueProductName(cvAppletType);
				deviceProfile.setConfigValueCategory(cvCategory);
				
				List<Object[]> objects = deviceProfileConfigRepository.getDevProfConfig(cvEntityAppletType.getId());
				if(objects.size() > 0) {
					Object[] deviceProfileConfigDetails = objects.get(0);
					DeviceProfileConfig deviceProfileConfig = new DeviceProfileConfig();
					deviceProfileConfig.setIsMobileId((Boolean) deviceProfileConfigDetails[0]);
					deviceProfileConfig.setIsPlasticId((Boolean) deviceProfileConfigDetails[1]);
					deviceProfileConfig.setIsRfidCard((Boolean) deviceProfileConfigDetails[2]);
					deviceProfileConfig.setIsAppletLoadingRequired((Boolean) deviceProfileConfigDetails[3]);
					deviceProfile.setDeviceProfileConfig(deviceProfileConfig);
				}
				deviceProfiles.add(deviceProfile);
			});
			return deviceProfiles;
		}
		catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_DEVICEPROFILES_FOR_WORKFLOW_UNDER_ORGANIZATION, workflowEntity.getName(), organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_DEVICEPROFILES_FOR_WORKFLOW_UNDER_ORGANIZATION, workflowEntity.getName(), organizationName);
		}
	}
	
	@Override
	public Workflow getWorkflowValidityByName(String organizationName, String workflowName) throws WorkflowServiceException{
		Workflow workflow = new Workflow();
		
		try{
			WorkflowEntity workflowEntity = workflowRepository.findByName(organizationName, workflowName);
			if (workflowEntity == null) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_WORKFLOWNAME_UNDER_ORGANIZATION, workflowName, organizationName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_WORKFLOWNAME_UNDER_ORGANIZATION, workflowName, organizationName));
			}
			workflow.setName(workflowEntity.getName());
			workflow.setExpirationInMonths(workflowEntity.getExpirationInMonths());
			workflow.setExpirationInDays(workflowEntity.getExpirationInDays());
			workflow.setOrganizationIdentities(new OrganizationIdentity(workflowEntity.getOrgIdentityType().getIdentityType().getId(), workflowEntity.getOrgIdentityType().getIdentityType().getName(), workflowEntity.getOrgIdentityType().getIdentityType().getNotes()));
			workflow.setDisableExpirationEnforcement(workflowEntity.getDisableExpirationEnforcement());
			workflow.setCeatedDate(workflowEntity.getCreatedDate());
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_VALIDITY_OF_WORKFLOW_WITH_WORKFLOWNAME_UNDER_ORGANIZATION, workflowName, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_VALIDITY_OF_WORKFLOW_WITH_WORKFLOWNAME_UNDER_ORGANIZATION,workflowName, organizationName);
		}
		return workflow;
	}
	
	@Override
	public Workflow getWorkflowValidityByGroupID(String organizationName, String groupID) throws WorkflowServiceException{
		Workflow workflow = new Workflow();
		
		try{
			WorkflowEntity workflowEntity = wfGroupRepository.findWorkflowByGroupID(organizationName, groupID).getWorkflow();
			if (workflowEntity == null) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_GROUPID_UNDER_ORGANIZATION, groupID, organizationName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_GROUPID_UNDER_ORGANIZATION, groupID, organizationName));
			}
			workflow.setName(workflowEntity.getName());
			workflow.setExpirationInMonths(workflowEntity.getExpirationInMonths());
			workflow.setExpirationInDays(workflowEntity.getExpirationInDays());
			workflow.setOrganizationIdentities(new OrganizationIdentity(workflowEntity.getOrgIdentityType().getIdentityType().getId(), workflowEntity.getOrgIdentityType().getIdentityType().getName(), workflowEntity.getOrgIdentityType().getIdentityType().getNotes()));
			workflow.setDisableExpirationEnforcement(workflowEntity.getDisableExpirationEnforcement());
			workflow.setCeatedDate(workflowEntity.getCreatedDate());
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_VALIDITY_OF_WORKFLOW_WITH_GROUPID_UNDER_ORGANIZATION, groupID, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_VALIDITY_OF_WORKFLOW_WITH_GROUPID_UNDER_ORGANIZATION,groupID, organizationName);
		}
		return workflow;
	}
	
	@Override
	public String getIdentityTypeByGroupID(String organizationName, String groupID) throws WorkflowServiceException{
		
		try{
			WorkflowEntity workflowEntity = wfGroupRepository.findWorkflowByGroupID(organizationName, groupID).getWorkflow();
			if (workflowEntity == null) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_GROUPID_UNDER_ORGANIZATION, groupID, organizationName));
				return "";
			}else {
				
			}
			return workflowEntity.getOrgIdentityType().getIdentityType().getName();
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_IDENTITYTYPE_WITH_GROUPID_UNDER_ORGANIZATION, groupID, organizationName) + " : " + cause.getMessage());
			return "";
		}
	}
	
	@Override
	public String[] getIdentityTypeAndWrkflowByGroupID(String organizationName, String groupID) throws WorkflowServiceException{
		
		try{
			WorkflowEntity workflowEntity = wfGroupRepository.findWorkflowByGroupID(organizationName, groupID).getWorkflow();
			if (workflowEntity == null) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.FAILED_TO_GET_WORKFLOW_AND_IDENTITYTYPE_WITH_GROUPID_UNDER_ORGANIZATION, groupID, organizationName));
				return null;
			}else {
				
			}
			List<WorkflowStep> workflowfSteps = getStepsByWorkflow(organizationName, workflowEntity.getName());
			String onboardSelfService = "false";
			for (WorkflowStep workflowStep : workflowfSteps) {
				if(workflowStep.getName().contains(Constants.MOBILE_ID_ONBOARDING_CONFIG)) {
					onboardSelfService = "true";
				};
			}
			return new String[] {workflowEntity.getName(), workflowEntity.getOrgIdentityType().getIdentityType().getName(),onboardSelfService};
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_WORKFLOW_AND_IDENTITYTYPE_WITH_GROUPID_UNDER_ORGANIZATION, groupID, organizationName) + " : " + cause.getMessage());
			return null;
		}
	}
	
	@Override
	public List<Finger> getFingersByFingerCount(String organizationName, Integer count) {
		List<Finger> fingerList = new ArrayList<>();
		List<FingerTypeEntity> list = fingerTypeRepository.findFingersByOrgAndCount(organizationName, count);
		list.forEach(fingerTypeEntity -> {
			Finger finger = new Finger();
			finger.setFingerCount(fingerTypeEntity.getFingerCount());
			finger.setFingerConfigValueName(fingerTypeEntity.getConfigValueFingerPosition().getValue());
			fingerList.add(finger);
		});
		return fingerList;
	}
	
	private List<DeviceProfile> getWorkflowDeviceProfiles(String organizationName, String workflowName) throws WorkflowServiceException{
		List<DeviceProfile> deviceProfiles = new ArrayList<>();
		try{
			workflowRepository.findWorkflowDeviceProfiles(organizationName, workflowName).stream().forEach(deviceProfileName -> {
				DeviceProfile deviceProfile = new DeviceProfile();
				deviceProfile.setName(deviceProfileName);
				deviceProfiles.add(deviceProfile);
			});
			return deviceProfiles;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_DEVICEPROFILES_FOR_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_DEVICEPROFILES_FOR_WORKFLOW_UNDER_ORGANIZATION,workflowName, organizationName);
		}
	}
	
	private List<Group> getWorkflowGroups(WorkflowEntity workflowEntity) {
		List<Group> groups = new ArrayList<>();
		workflowEntity.getWfGroups().stream().forEach(wfGroup -> {
			Group group = new Group();
			group.setId(wfGroup.getReferenceGroupId());
			group.setName(wfGroup.getReferenceGroupName());
			if (wfGroup.getVisualTemplate() != null) {
				group.setVisualTemplateID(wfGroup.getVisualTemplate().getId());
			}
			groups.add(group);
		});
		return groups;
	}
	
	@Override
	public List<Group> getGroupsWithWorkflow(String organizationName) {
		List<Group> groups = new ArrayList<>();
		try{
			
			groups = keycloakService.getGroupsWithWorkflow(organizationName);
			
			// groups = workflowRepository.findGroupsWithWorkflow(organizationName);
			
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_GROUPS_WITH_WORKFLOW_UNDER_ORGANIZATION,  organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_GROUPS_WITH_WORKFLOW_UNDER_ORGANIZATION, organizationName);
		}
		return groups;
	}
	

	@Override
	public ResponseEntity<String> createWorkflow(String organizationName, Workflow workflow) throws WorkflowServiceException{
		WorkflowEntity wfFromDb = workflowRepository.findByName(organizationName, workflow.getName());
		if (wfFromDb != null) {
			if (wfFromDb.getName().equalsIgnoreCase(workflow.getName())) {
				log.debug("Workflow with the name" + " " + workflow.getName() + " " + "already exists");
				throw new WorkflowServiceException(
						"Workflow with the name" + " " + workflow.getName() + " " + "already exists");
			}
		}
		try{
			WorkflowEntity workflowEntity = new WorkflowEntity();
			workflowEntity.setName(workflow.getName());
			workflowEntity.setDescription(workflow.getDescription());
						
			OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);
			if (organizationEntity == null) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.ORGANIZATION_NOT_FOUND_WITH_NAME, organizationName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.ORGANIZATION_NOT_FOUND_WITH_NAME, organizationName));
			}
			workflowEntity.setStatus("ACTIVE");
			workflowEntity.setOrganization(organizationEntity);
			workflowEntity.setOrgIdentityType(organizationIdentitiesRepository.getOne(workflow.getOrganizationIdentities().getId()));
			workflowEntity.setAllowedDevices(workflow.getAllowedDevices());
			workflowEntity.setExpirationInMonths(workflow.getExpirationInMonths());
			workflowEntity.setExpirationInDays(workflow.getExpirationInDays());
			workflowEntity.setDisableExpirationEnforcement(workflow.isDisableExpirationEnforcement());
			workflowEntity.setRevokeEncryptionCertificate(workflow.isRevokeEncryptionCertificate());
			workflowEntity.setRequiredEnrollment(workflow.getRequiredEnrollment());
			workflowEntity.setRequiredTokenCardIssuance(workflow.getRequiredTokenCardIssuance());
			workflowEntity.setRequiredMobileIssuance(workflow.getRequiredMobileIssuance());
			setDeviceProfilesToWorkflow(workflow, workflowEntity);
			setGroupsToWorkflow(workflow, workflowEntity);
			if (workflow.getWorkflowSteps() != null) {
				
				for(WorkflowStep workflowStep: workflow.getWorkflowSteps() ) {
					ResponseEntity<String> response =  setWorkflowSteps(workflowStep, organizationName, workflowEntity);
					if (response.getStatusCode() != HttpStatus.OK) {
						return response;
					}
				}
			}
			
			// Create Workflow
			workflowRepository.save(workflowEntity);
			
			// Create Workflow Attribute Entity
			updateDefaultWorkflowAttribute(workflowEntity, workflow.isDefaultWorkflow(), organizationEntity);
			
			String workflowRoleName = workflowEntity.getName() + "_Workflow";
			log.info(ApplicationConstants.notifyCreate,
					Util.format(ErrorMessages.WORKFLOW_CREATED_WITH_NAME_UNDER_ORGANIZATION, workflowEntity.getName(), organizationName));
			// Create Role associated to Workflow
			RoleEntity roleEntity = new RoleEntity();
			roleEntity.setName(workflowRoleName);
			roleEntity.setDescription(workflowRoleName + "Desc");
			roleEntity.setOrganization(workflowEntity.getOrganization());
			roleEntity.setWorkflow(workflowEntity);
			roleEntity.setType("WORKFLOW");
			
			roleRepository.save(roleEntity);
			log.info(ApplicationConstants.notifyCreate,
					Util.format(ErrorMessages.ROLE_CREATED_WITH_NAME_FOR_WORKFLOW, roleEntity.getName()));
			// Create Role with the same name as above in Keycloak and assign to Group chosen
			createAndAssignRoleToGroup(workflow, null, workflowRoleName, organizationName);
			log.info(ApplicationConstants.notifyCreate,
					Util.format(ErrorMessages.IDENTITYBROKER_SUCCESSFULLY_CREATED_AND_ASSIGNED_ROLE_TO_GROUP_OF_WORKFLOW_UNDER_ORGANIZATION, roleEntity.getName(),workflowRoleName ));
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_CREATE_WORKFLOW_UNDER_ORGANIZATION, workflow.getName(), organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_CREATE_WORKFLOW_UNDER_ORGANIZATION, workflow.getName(), organizationName);
		}
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<String> updateWorkflow(String organizationName, String workflowName, Workflow workflow) throws WorkflowServiceException{
		WorkflowEntity workflowEntity = workflowRepository.findByName(organizationName, workflowName);
		if (workflowEntity == null) {
			log.info(ApplicationConstants.notifySearch,
					Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_WORKFLOWNAME_UNDER_ORGANIZATION, workflowName, organizationName));
			throw new EntityNotFoundException(Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_WORKFLOWNAME_UNDER_ORGANIZATION, workflowName, organizationName));
		}
		WorkflowEntity workflowEntityFromDb = workflowRepository.findByName(organizationName, workflow.getName());
		if (workflowEntityFromDb != null) {
			if (workflowEntity.getName() != workflowEntityFromDb.getName()) {
				log.debug("Workflow with the name" + " " + workflowEntityFromDb.getName() + " " + "already exists");
				throw new WorkflowServiceException("Workflow with the name" + " " + workflowEntityFromDb.getName() + " " + "already exists");
			}
		}
		try{
			OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);
			if (organizationEntity == null) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.ORGANIZATION_NOT_FOUND_WITH_NAME, organizationName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.ORGANIZATION_NOT_FOUND_WITH_NAME, organizationName));
			}
			// Update Workflow with new values
			workflowEntity.setName(workflow.getName());
			workflowEntity.setDescription(workflow.getDescription());
			workflowEntity.setAllowedDevices(workflow.getAllowedDevices());
			workflowEntity.setExpirationInMonths(workflow.getExpirationInMonths());
			workflowEntity.setExpirationInDays(workflow.getExpirationInDays());

			workflowEntity.setDisableExpirationEnforcement(workflow.isDisableExpirationEnforcement());
			workflowEntity.setRevokeEncryptionCertificate(workflow.isRevokeEncryptionCertificate());
			workflowEntity.setRequiredEnrollment(workflow.getRequiredEnrollment());
			workflowEntity.setRequiredTokenCardIssuance(workflow.getRequiredTokenCardIssuance());
			workflowEntity.setRequiredMobileIssuance(workflow.getRequiredMobileIssuance());
			
			try {
				setDeviceProfilesToWorkflow(workflow, workflowEntity);
			} catch (Exception cause) {
				log.error(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.FAILED_TO_ASSIGN_DEVICE_PROFILES_TO_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName) + " : " + cause.getMessage());
				throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_ASSIGN_DEVICE_PROFILES_TO_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName);
			}
			RoleEntity roleEntity = workflowEntity.getRole();
			roleEntity.setName(workflow.getName() + "_Workflow");
			workflowEntity.setRole(roleEntity);
			
			setGroupsToWorkflow(workflow, workflowEntity);			
			
			ResponseEntity<String> response=editWorkflowSteps(organizationName, workflow, workflowEntity);
			if(response.getStatusCode() != HttpStatus.OK){
				return response;
			}
			// Delete workflow role in Keycloak
			String workflowRoleName = workflowName + "_Workflow";
			int statusCode = 0;
			
			statusCode = keycloakService.deleteClientRole(workflowRoleName, organizationName); 
			if (statusCode == 204) {
				workflowRoleName = workflow.getName() + "_Workflow";
				// Create Role with the same name as above in Keycloak and assign to Group chosen
				createAndAssignRoleToGroup(workflow, workflowEntity, workflowRoleName, organizationName);
			} else {
				log.info(ApplicationConstants.notifyDelete,
						Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_DELETE_CLIENT_ROLE_USING_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName));
				throw new IdentityBrokerServiceException(Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_DELETE_CLIENT_ROLE_USING_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName));
			}
			
			// update Workflow Attribute Entity
			updateDefaultWorkflowAttribute(workflowEntity, workflow.isDefaultWorkflow(), organizationEntity);
			
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_UPDATE_WORKFLOW_UNDER_ORGANIZATION, workflow.getName(), organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_UPDATE_WORKFLOW_UNDER_ORGANIZATION, workflow.getName(), organizationName);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public void deleteWorkflow(String organizationName, String workflowName)  throws WorkflowServiceException {
		// Delete Workflow Role
		try{
			RoleEntity roleEntity = roleRepository.findRoleByWorkflow(organizationName, workflowName);
			if (roleEntity == null) {
				log.info(ApplicationConstants.notifyDelete,
						Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_GET_ROLE_USING_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_GET_ROLE_USING_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName));			
			}
			roleRepository.delete(roleEntity);
			
			// Delete Workflow
			WorkflowEntity workflowEntity =workflowRepository.findByName(organizationName, workflowName);
			// workflowRepository.deleteByName( workflowName);
			workflowRepository.delete(workflowEntity);
			
			// Delete Workflow Role in Keycloak
			int statusCode = keycloakService.deleteClientRole(workflowName + "_Workflow", organizationName); 
			if (statusCode != 204) {
				log.info(ApplicationConstants.notifyDelete,
						Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_DELETE_ROLE_USING_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_DELETE_ROLE_USING_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName));			
			}
			// update Workflow Attribute Entity
			OrganizationEntity organizationEntity = organizationRepository.findByName(organizationName);
			updateDefaultWorkflowAttribute(workflowEntity, false, organizationEntity);
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_DELETE_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_DELETE_WORKFLOW_UNDER_ORGANIZATION, workflowName, organizationName);
		}
	}

	private ResponseEntity<String> editWorkflowSteps(String organizationName, Workflow workflow, WorkflowEntity workflowEntity)
			throws WorkflowServiceException {
		try {
			List<WFStepEntity> wfStepsToRemove = new ArrayList<>();
			List<WFStepRegistrationConfigEntity> wfStepRegistrationConfigToRemove = new ArrayList<>();
			List<WFStepApprovalGroupConfigEntity> wfStepApprovalGroupConfigsToRemove = new ArrayList<>();
			List<WfStepPrintGroupConfigEntity> wfStepPrintGroupConfigToRemove = new ArrayList<>();
			List<WfStepChipEncodeGroupConfigEntity> wfStepChipEncodeGroupConfigToRemove = new ArrayList<>();
			List<WfStepChipEncodeAndVIDPrintGroupConfigEntity> wfStepChipEncodeVIDGroupConfigToRemove = new ArrayList<>();
			List<WFMobileIDStepTrustedIdentityEntity> wfMobileIDStepTrustedIdentitiesToRemove = new ArrayList<>();
			List<WFStepAdjudicationGroupConfigEntity> wfStepAdjudicationGroupConfigsToRemove = new ArrayList<>();
			List<WFMobileIDIdentityIssuanceEntity> wfMobileIDIdentityIssuanceEntitiesToRemove = new ArrayList<>();
			if (workflow.getWorkflowSteps() != null) {
				WFStepEntity wfStepEntity1 = null;
				for (WorkflowStep workflowStep : workflow.getWorkflowSteps()) {
					List<WFStepEntity> wfStepEntities = workflowEntity.getWfSteps().stream()
							.filter(wfStepEntity -> workflowStep.getName().equalsIgnoreCase(wfStepEntity.getWfStepDef().getName()))
							.collect(Collectors.toList());
					if (wfStepEntities.size() == 0) {
						// it means this is new step to be added
						setWorkflowSteps(workflowStep, organizationName, workflowEntity);
					} else {
						WFStepEntity wfStepEntity = wfStepEntities.get(0);
						wfStepEntity1 = wfStepEntity;
						if (workflowStep.getName().equalsIgnoreCase("REGISTRATION")) {
							if (workflowStep.getWfStepRegistrationConfigs() != null) {
								// delete the fields from db which are not selected in ui
								wfStepEntity.getWfStepRegistrationConfigs().stream().forEach(wfRegConfigEntity -> {
									
									List<WFStepRegistrationConfig> wfStepRegistrationConfigs = workflowStep.getWfStepRegistrationConfigs().stream()
											.filter(wfStepCnfg -> wfRegConfigEntity.getOrgIdentityField().getId().equals(wfStepCnfg.getOrgFieldId()))
											.collect(Collectors.toList());
									if(wfStepRegistrationConfigs.size() == 0) {
										wfStepRegistrationConfigToRemove.add(wfRegConfigEntity);
									}
								});
								//  add newly added form fields and edit existing
								workflowStep.getWfStepRegistrationConfigs().stream().forEach(wfStepRegistrationConfig -> {
									List<WFStepRegistrationConfigEntity> wfStepRegistrationConfigs = wfStepEntity.getWfStepRegistrationConfigs().stream()
											.filter(wfStepCnfgEntity -> wfStepCnfgEntity.getOrgIdentityField().getId().equals(wfStepRegistrationConfig.getOrgFieldId()))
											.collect(Collectors.toList());
										WFStepRegistrationConfigEntity wfStepRegistrationConfigEntity = new WFStepRegistrationConfigEntity();
										if (wfStepRegistrationConfigs.size() == 0) {
											wfStepRegistrationConfigEntity.setOrgIdentityField(orgIdentityFieldRepository.findOne(wfStepRegistrationConfig.getOrgFieldId()));
										} else {
											wfStepRegistrationConfigEntity = wfStepRegistrationConfigs.get(0);
										}
										wfStepRegistrationConfigEntity.setFieldLabel(wfStepRegistrationConfig.getFieldLabel());
										wfStepRegistrationConfigEntity.setRequired(wfStepRegistrationConfig.isRequired());
										wfStepEntity.addWfStepRegistrationConfig(wfStepRegistrationConfigEntity);
							     });
							}
							workflowEntity.addWfStep(wfStepEntity);
							for (WFStepRegistrationConfigEntity obj : wfStepRegistrationConfigToRemove) {
								 wfStepEntity1.removeWfStepRegistrationConfig(obj);
								 wfStepRegistrationConfigRepository.delete(obj.getId());
							}
						}
						if (workflowStep.getName().equalsIgnoreCase("FINGERPRINT_CAPTURE")) {
							List<WFStepConfigEntity> wfStepConfigsToRemove = new ArrayList<>();
							boolean isChanged = true;
							if (workflowStep.getWfStepGenericConfigs() != null) {
								for (WFStepGenericConfig wfStepConfig : workflowStep.getWfStepGenericConfigs()) {
									WFStepConfigEntity wfStepConfigEntity = new WFStepConfigEntity();
									if (!wfStepConfig.getName().equalsIgnoreCase("FINGER_POSITION")) {
										List<WFStepConfigEntity> wfStepConfigEntities = wfStepEntity.getWfStepConfigs().stream().filter(wfStepCfgEntity -> wfStepCfgEntity.getConfigValue().getConfig().getName().equalsIgnoreCase(wfStepConfig.getName())).collect(Collectors.toList());
										if(wfStepConfigEntities.size() == 1) {
										   wfStepConfigEntity = wfStepConfigEntities.get(0);
										   if (wfStepConfig.getName().equalsIgnoreCase("MINIMUM_FINGERS_REQUIRED")) {
											   if(wfStepConfigEntity.getConfigValue().getValue().equalsIgnoreCase(wfStepConfig.getValue())) {
												   isChanged = false;
											   }
										   }
										}
										wfStepConfigEntity.setConfigValue(configValueRepository.findByConfigNameAndValue(organizationName, wfStepConfig.getName(), wfStepConfig.getValue()));
										wfStepEntity.addWfStepConfig(wfStepConfigEntity);
									}
								}
								workflowEntity.addWfStep(wfStepEntity);
							}
							if (isChanged) {
								// delete finger position configs as min fingers required value is changed
								wfStepEntity.getWfStepConfigs().stream().forEach(wfStepConfigEntity -> {
									if (wfStepConfigEntity.getConfigValue().getConfig().getName().equalsIgnoreCase("FINGER_POSITION")) {
										wfStepConfigsToRemove.add(wfStepConfigEntity);
									}
								});
								for (WFStepGenericConfig wfStepConfig : workflowStep.getWfStepGenericConfigs()) {
									WFStepConfigEntity wfStepConfigEntity = new WFStepConfigEntity();
									if (wfStepConfig.getName().equalsIgnoreCase("FINGER_POSITION")) {
										wfStepConfigEntity.setConfigValue(configValueRepository.findByConfigNameAndValue(organizationName,wfStepConfig.getName(), wfStepConfig.getValue()));
										wfStepEntity.addWfStepConfig(wfStepConfigEntity);
									}
								}
								workflowEntity.addWfStep(wfStepEntity);
							}
							for (WFStepConfigEntity obj : wfStepConfigsToRemove) {
								wfStepEntity1.removeWfStepConfig(obj);
								wfStepConfigRepository.delete(obj.getId());
							}
						}
						// edit workflow step configs
						if (workflowStep.getWfStepGenericConfigs() != null) {
							// we are having configs differently for finger print step 
							if (!workflowStep.getName().equalsIgnoreCase("FINGERPRINT_CAPTURE")) {
							workflowStep.getWfStepGenericConfigs().stream().forEach(wfStepConfig -> {
								List<WFStepConfigEntity> wfStepConfigEntities = wfStepEntity.getWfStepConfigs()
										.stream().filter(wfStepConfigEntity -> wfStepConfigEntity.getConfigValue()
												.getConfig().getName().equalsIgnoreCase(wfStepConfig.getName()))
										.collect(Collectors.toList());
								WFStepConfigEntity wfStepConfigEntity = new WFStepConfigEntity();
								if (wfStepConfigEntities.size() == 1) {
									wfStepConfigEntity = wfStepConfigEntities.get(0);
								}
								wfStepConfigEntity.setConfigValue(configValueRepository.findByConfigNameAndValue(
										organizationName, wfStepConfig.getName(), wfStepConfig.getValue()));
								wfStepEntity.addWfStepConfig(wfStepConfigEntity);
							});
							workflowEntity.addWfStep(wfStepEntity);
						   }
						}
						if (workflowStep.getName().equalsIgnoreCase("APPROVAL_BEFORE_ISSUANCE")) {
							wfStepEntity.getWfStepApprovalGroupConfigs().stream().forEach(groupEntity -> {
								List<WFStepApprovalGroupConfig> wfStepApprovalGroupConfigs = workflowStep.getWfStepApprovalGroupConfigs().stream().filter(group -> group.getGroupId().equalsIgnoreCase(groupEntity.getReferenceApprovalGroupId())).collect(Collectors.toList());
								if(wfStepApprovalGroupConfigs.size() == 0) {
									wfStepApprovalGroupConfigsToRemove.add(groupEntity); 
								}
							});
							workflowStep.getWfStepApprovalGroupConfigs().stream().forEach(wfStepApprovalGroupConfig -> {
								List<WFStepApprovalGroupConfigEntity> wfStepApprovalGroupConfigEntities = wfStepEntity.getWfStepApprovalGroupConfigs().stream().filter(group -> 
									group.getReferenceApprovalGroupId().equalsIgnoreCase(wfStepApprovalGroupConfig.getGroupId())).collect(Collectors.toList());
								WFStepApprovalGroupConfigEntity  wfStepApprovalGroupConfigEntity = new WFStepApprovalGroupConfigEntity();
								if(wfStepApprovalGroupConfigEntities.size() != 0) {
									wfStepApprovalGroupConfigEntity = wfStepApprovalGroupConfigEntities.get(0);
								}
								wfStepApprovalGroupConfigEntity.setReferenceApprovalGroupId(wfStepApprovalGroupConfig.getGroupId());
								wfStepApprovalGroupConfigEntity.setReferenceApprovalGroupName(wfStepApprovalGroupConfig.getGroupName());	
								
								wfStepEntity.addWfStepApprovalGroupConfig(wfStepApprovalGroupConfigEntity);
							});
							workflowEntity.addWfStep(wfStepEntity);
							for(WFStepApprovalGroupConfigEntity obj : wfStepApprovalGroupConfigsToRemove) {
								wfStepEntity1.removeWfStepApprovalGroupConfig(obj);
								wfStepApprovalGroupConfigRepository.delete(obj.getId());
								
							}
						}
						if (workflowStep.getName().equalsIgnoreCase("PERSO_AND_ISSUANCE")) {
							
							if(workflowStep.getWfStepAppletLoadingInfo() != null) {
								
								WfStepAppletLoadingInfo wfAppletLoadingInfo = workflowStep.getWfStepAppletLoadingInfo();
								
								WfStepAppletLoadingInfoEntity wfAppletLoadingInfoEntity = new WfStepAppletLoadingInfoEntity();
								if (wfStepEntity.getWfAppletLoadingInfo() != null) {
									wfAppletLoadingInfoEntity = wfStepEntity.getWfAppletLoadingInfo();
								}
								wfAppletLoadingInfoEntity.setWfStep(wfStepEntity);
								wfAppletLoadingInfoEntity.setPivAppletEnabled(wfAppletLoadingInfo.getPivAppletEnabled());
								wfAppletLoadingInfoEntity.setFido2AppletEnabled(wfAppletLoadingInfo.getFido2AppletEnabled());
//								if(wfAppletLoadingInfoEntity.getFido2AppletEnabled()) {
									wfAppletLoadingInfoEntity.setAttestationCert(wfAppletLoadingInfo.getAttestationCert());
									wfAppletLoadingInfoEntity.setAttestationCertPrivateKey(wfAppletLoadingInfo.getAttestationCertPrivateKey());
									wfAppletLoadingInfoEntity.setAaguid(wfAppletLoadingInfo.getAaguid());
//								}
								wfStepEntity.setWfAppletLoadingInfo(wfAppletLoadingInfoEntity);
							}
							
							// delete or add or edit printing groups
							wfStepEntity.getWfStepPrintGroupConfigs().stream().forEach(wfStepPrintGroupConfig -> {
								List<WfStepGroupConfig> wfStepPrintGroupConfigs = workflowStep.getWfStepPrintGroupConfigs().stream().filter(printGroupConfig -> printGroupConfig.getGroupId().equalsIgnoreCase(wfStepPrintGroupConfig.getReferencePrintGroupId())).collect(Collectors.toList());
								if(wfStepPrintGroupConfigs.size() == 0) {
									wfStepPrintGroupConfigToRemove.add(wfStepPrintGroupConfig);
								}
							});
							wfStepEntity.getWfStepChipEncodeGroupConfigs().stream().forEach(wfStepChipEncodeGroupConfig -> {
								List<WfStepGroupConfig> wfStepChipEncodeGroupConfigs = workflowStep.getWfStepChipEncodeGroupConfigs().stream().filter(chipEncodeGroupConfig -> chipEncodeGroupConfig.getGroupId().equalsIgnoreCase(wfStepChipEncodeGroupConfig.getReferenceChipEncodeGroupId())).collect(Collectors.toList());
								if(wfStepChipEncodeGroupConfigs.size() == 0) {
									wfStepChipEncodeGroupConfigToRemove.add(wfStepChipEncodeGroupConfig);
								}
							});
							wfStepEntity.getWfStepChipEncodeVIDPrintGroupConfigs().stream().forEach(wfStepChipEncodeVIDGroupConfig -> {
								List<WfStepGroupConfig> wfStepChipEncodeVIDGroupConfigs = workflowStep.getWfStepChipEncodeVIDPrintGroupConfigs().stream().filter(chipEncodeVIDGroupConfig -> chipEncodeVIDGroupConfig.getGroupId().equalsIgnoreCase(wfStepChipEncodeVIDGroupConfig.getReferenceChipEncodeVIDPrintGroupId())).collect(Collectors.toList());
								if(wfStepChipEncodeVIDGroupConfigs.size() == 0) {
									wfStepChipEncodeVIDGroupConfigToRemove.add(wfStepChipEncodeVIDGroupConfig);
								}
							});
							// add if new groups added
							workflowStep.getWfStepPrintGroupConfigs().stream().forEach(printGroupConfig -> {
								List<WfStepPrintGroupConfigEntity> printGroupEntities = wfStepEntity.getWfStepPrintGroupConfigs().stream().filter(printGroupConfigEntity -> printGroupConfig.getGroupId().equalsIgnoreCase(printGroupConfigEntity.getReferencePrintGroupId())).collect(Collectors.toList());
								WfStepPrintGroupConfigEntity wfStepPrintGroupConfigEntity = new WfStepPrintGroupConfigEntity();
								if (printGroupEntities.size() != 0) {
									wfStepPrintGroupConfigEntity = printGroupEntities.get(0);
								}
								wfStepPrintGroupConfigEntity.setReferencePrintGroupId(printGroupConfig.getGroupId());
								wfStepPrintGroupConfigEntity.setReferencePrintGroupName(printGroupConfig.getGroupName());
								wfStepEntity.addWfStepPrintGroupConfigs(wfStepPrintGroupConfigEntity);
							});
							workflowStep.getWfStepChipEncodeGroupConfigs().stream().forEach(chipEncodeGroupConfig -> {
								List<WfStepChipEncodeGroupConfigEntity> chipEncodeGroupEntities = wfStepEntity.getWfStepChipEncodeGroupConfigs().stream().filter(chipEncodeGroupConfigEntity -> chipEncodeGroupConfig.getGroupId().equalsIgnoreCase(chipEncodeGroupConfigEntity.getReferenceChipEncodeGroupId())).collect(Collectors.toList());
								WfStepChipEncodeGroupConfigEntity wfStepCEGroupConfigEntity = new WfStepChipEncodeGroupConfigEntity();
								if (chipEncodeGroupEntities.size() != 0) {
									wfStepCEGroupConfigEntity = chipEncodeGroupEntities.get(0);
								}
								wfStepCEGroupConfigEntity.setReferenceChipEncodeGroupId(chipEncodeGroupConfig.getGroupId());
								wfStepCEGroupConfigEntity.setReferenceChipEncodeGroupName(chipEncodeGroupConfig.getGroupName());
								wfStepEntity.addWfStepChipEncodeGroupConfigEntity(wfStepCEGroupConfigEntity);
							});
							workflowStep.getWfStepChipEncodeVIDPrintGroupConfigs().stream().forEach(chipEncodeVIDGroupConfig -> {
								List<WfStepChipEncodeAndVIDPrintGroupConfigEntity> chipEncodeVIDGroupEntities = wfStepEntity.getWfStepChipEncodeVIDPrintGroupConfigs().stream().filter(chipEncodeVIDGroupConfigEntity -> chipEncodeVIDGroupConfig.getGroupId().equalsIgnoreCase(chipEncodeVIDGroupConfigEntity.getReferenceChipEncodeVIDPrintGroupId())).collect(Collectors.toList());
								WfStepChipEncodeAndVIDPrintGroupConfigEntity wfStepCEVIDGroupConfigEntity = new WfStepChipEncodeAndVIDPrintGroupConfigEntity();
								if (chipEncodeVIDGroupEntities.size() != 0) {
									wfStepCEVIDGroupConfigEntity = chipEncodeVIDGroupEntities.get(0);
								}
								wfStepCEVIDGroupConfigEntity.setReferenceChipEncodeVIDPrintGroupId(chipEncodeVIDGroupConfig.getGroupId());
								wfStepCEVIDGroupConfigEntity.setReferenceChipEncodeVIDPrintGroupName(chipEncodeVIDGroupConfig.getGroupName());
								wfStepEntity.addWfStepChipEncodeAndVIDPrintGroupConfigEntity(wfStepCEVIDGroupConfigEntity);
							});
							workflowEntity.addWfStep(wfStepEntity);
							for (WfStepChipEncodeGroupConfigEntity obj : wfStepChipEncodeGroupConfigToRemove) {
								wfStepEntity1.removeWfStepChipEncodeGroupConfigEntity(obj);
								wfStepChipEncodeGroupConfigRepository.delete(obj.getId());
							}
							for (WfStepChipEncodeAndVIDPrintGroupConfigEntity obj : wfStepChipEncodeVIDGroupConfigToRemove) {
								wfStepEntity1.removeWfStepChipEncodeAndVIDPrintGroupConfigEntity(obj);
								wfStepChipEncodeVIDPrintGroupConfigRepository.delete(obj.getId());
							}
							for (WfStepPrintGroupConfigEntity obj : wfStepPrintGroupConfigToRemove) {
								wfStepEntity1.removeWfStepPrintGroupConfigs(obj);
								wfStepPrintGroupConfigRepository.delete(obj.getId());
							}
						}
						
						if (workflowStep.getName().equalsIgnoreCase("PKI_CERTIFICATES")) {
							List<WFStepCertConfigEntity> passiveWfStepCertsConfigEntities = new ArrayList<>();
							List<WFStepCertConfigEntity> certsToBeAddedNewly = new ArrayList<>();
							
							if (workflowStep.getWfStepCertConfigs().size() != 0) {
								if (!wfStepEntity.getStatus().equalsIgnoreCase("ACTIVE")) {
									wfStepEntity.setStatus("ACTIVE");
								}
								 wfStepEntity.getWfStepCertConfigs().stream().forEach(wfStepCertEntity -> {
										List<WFStepCertConfig> wfStepCertConfigs = workflowStep.getWfStepCertConfigs().stream().filter(e -> wfStepCertEntity.getId().equals(e.getId()))
												.collect(Collectors.toList());
										if (wfStepCertConfigs.size() != 0) {
											// it means the this row is there and we need to check obj compare and then add or set as passive
										} else {
											if (!wfStepCertEntity.getStatus().equalsIgnoreCase("PASSIVE")) {
												passiveWfStepCertsConfigEntities.add(wfStepCertEntity);
											}
										}
									});
									 
									workflowStep.getWfStepCertConfigs().stream().forEach(wfStepCertConfig -> {
										List<WFStepCertConfigEntity> wfStepCertConfigEntities =  wfStepEntity.getWfStepCertConfigs().stream().
												filter(e -> e.getId().equals(wfStepCertConfig.getId())).collect(Collectors.toList());
										WFStepCertConfigEntity wfStepCertConfigEntity = new WFStepCertConfigEntity();
										if(wfStepCertConfigEntities.size() != 0) {
											 wfStepCertConfigEntity = wfStepCertConfigEntities.get(0);
											// compare two objects
											if (wfStepCertConfigEntity.getStatus().equalsIgnoreCase("ACTIVE")) {
												boolean isCertSame = workflowServiceHelper.compareWfCertObjects(wfStepCertConfig, wfStepCertConfigEntity);
												if (!isCertSame) {
													wfStepCertConfigEntity.setStatus("PASSIVE");
													passiveWfStepCertsConfigEntities.add(wfStepCertConfigEntity);
													
													wfStepCertConfigEntity = new WFStepCertConfigEntity();
													
													wfStepCertConfigEntity.setCertType(wfStepCertConfig.getCertType());
													wfStepCertConfigEntity.setCaServer(wfStepCertConfig.getCaServer());
													wfStepCertConfigEntity.setCertTemplate(wfStepCertConfig.getCertTemplate());
													wfStepCertConfigEntity.setKeyEscrow(wfStepCertConfig.isKeyEscrow());
													wfStepCertConfigEntity.setDisableRevoke(wfStepCertConfig.isDisableRevoke());
													wfStepCertConfigEntity.setAlgorithm(wfStepCertConfig.getAlgorithm());
													wfStepCertConfigEntity.setKeySize(wfStepCertConfig.getKeysize());
													wfStepCertConfigEntity.setSubjectDN(wfStepCertConfig.getSubjectDN());
													wfStepCertConfigEntity.setSubjectAN(wfStepCertConfig.getSubjectAN());
													wfStepCertConfigEntity.setStatus("ACTIVE");
													
													// wfStepEntity.addWfStepCertConfig(wfStepCertConfigEntity);
													certsToBeAddedNewly.add(wfStepCertConfigEntity);
												}
											}
											
										} else {
											wfStepCertConfigEntity.setCertType(wfStepCertConfig.getCertType());
											wfStepCertConfigEntity.setCaServer(wfStepCertConfig.getCaServer());
											wfStepCertConfigEntity.setCertTemplate(wfStepCertConfig.getCertTemplate());
											wfStepCertConfigEntity.setKeyEscrow(wfStepCertConfig.isKeyEscrow());
											wfStepCertConfigEntity.setDisableRevoke(wfStepCertConfig.isDisableRevoke());
											wfStepCertConfigEntity.setAlgorithm(wfStepCertConfig.getAlgorithm());
											wfStepCertConfigEntity.setKeySize(wfStepCertConfig.getKeysize());
											wfStepCertConfigEntity.setSubjectDN(wfStepCertConfig.getSubjectDN());
											wfStepCertConfigEntity.setSubjectAN(wfStepCertConfig.getSubjectAN());
											wfStepCertConfigEntity.setStatus("ACTIVE");
											
											certsToBeAddedNewly.add(wfStepCertConfigEntity);
										}
									});
							}
							
							for(WFStepCertConfigEntity wfStepCertConfigEntity: new HashSet<WFStepCertConfigEntity>(passiveWfStepCertsConfigEntities)) {
								wfStepCertConfigEntity.setStatus("PASSIVE");
								wfStepEntity.addWfStepCertConfig(wfStepCertConfigEntity);
							}
							for(WFStepCertConfigEntity wfStepCertConfigEntity: certsToBeAddedNewly) {
								wfStepCertConfigEntity.setStatus("ACTIVE");
								wfStepEntity.addWfStepCertConfig(wfStepCertConfigEntity);
							}
							
							workflowEntity.addWfStep(wfStepEntity);
						}
						if (workflowStep.getName().equalsIgnoreCase("MOBILE_ID_ONBOARDING_CONFIG")) {
							// delete or add Trusted existing identity
							wfStepEntity.getWfMobileIDStepTrustedIdentities().stream().forEach(trustedIdentityEntity -> {
								List<OrganizationIdentity> trustedIdentities = workflowStep.getWfMobileIDStepTrustedIdentities().stream().filter(identity -> identity.getIdentityTypeName().equalsIgnoreCase(trustedIdentityEntity.getOrganizationIdentities().getIdentityType().getName())).collect(Collectors.toList());
								if(trustedIdentities.size() == 0) {
									wfMobileIDStepTrustedIdentitiesToRemove.add(trustedIdentityEntity); 
								}
							});
							workflowStep.getWfMobileIDStepTrustedIdentities().stream().forEach(trustedIdentity -> {
								List<WFMobileIDStepTrustedIdentityEntity> wfMobileIDStepTrustedIdentityEntities = wfStepEntity.getWfMobileIDStepTrustedIdentities().stream().filter(trustedIdentityEntity -> 
								trustedIdentityEntity.getOrganizationIdentities().getIdentityType().getName().equalsIgnoreCase(trustedIdentity.getIdentityTypeName())).collect(Collectors.toList());
								WFMobileIDStepTrustedIdentityEntity  wfMobileIDStepTrustedIdentityEntity = new WFMobileIDStepTrustedIdentityEntity();
								if(wfMobileIDStepTrustedIdentityEntities.size() != 0) {
									wfMobileIDStepTrustedIdentityEntity = wfMobileIDStepTrustedIdentityEntities.get(0);
								}
								wfMobileIDStepTrustedIdentityEntity.setOrganizationIdentities(organizationIdentitiesRepository.getOne(trustedIdentity.getId()));
								wfStepEntity.addWFMobileIDStepTrustedIdentityEntity(wfMobileIDStepTrustedIdentityEntity);
							});
							// delete or add Adjudication group
							wfStepEntity.getWfStepAdjudicationGroupConfigs().stream().forEach(groupEntity -> {
								List<Group> wfStepApprovalGroups = workflowStep.getWfStepAdjudicationGroupConfigs().stream().filter(group -> group.getId().equalsIgnoreCase(groupEntity.getReferenceAdjudicationGroupId())).collect(Collectors.toList());
								if(wfStepApprovalGroups.size() == 0) {
									wfStepAdjudicationGroupConfigsToRemove.add(groupEntity); 
								}
							});
							workflowStep.getWfStepAdjudicationGroupConfigs().stream().forEach(wfStepApprovalGroupConfig -> {
								List<WFStepAdjudicationGroupConfigEntity> wfStepAdjudicationGroupConfigEntities = wfStepEntity.getWfStepAdjudicationGroupConfigs().stream().filter(group -> 
									group.getReferenceAdjudicationGroupId().equalsIgnoreCase(wfStepApprovalGroupConfig.getId())).collect(Collectors.toList());
								WFStepAdjudicationGroupConfigEntity  wfStepAdjudicationGroupConfigEntity = new WFStepAdjudicationGroupConfigEntity();
								if(wfStepAdjudicationGroupConfigEntities.size() != 0) {
									wfStepAdjudicationGroupConfigEntity = wfStepAdjudicationGroupConfigEntities.get(0);
								}
								wfStepAdjudicationGroupConfigEntity.setReferenceAdjudicationGroupId(wfStepApprovalGroupConfig.getId());
								wfStepAdjudicationGroupConfigEntity.setReferenceAdjudicationGroupName(wfStepApprovalGroupConfig.getName());	
								
								wfStepEntity.addWFStepAdjudicationGroupConfigEntity(wfStepAdjudicationGroupConfigEntity);
							});
							workflowEntity.addWfStep(wfStepEntity);
							for(WFMobileIDStepTrustedIdentityEntity obj : wfMobileIDStepTrustedIdentitiesToRemove) {
							    wfStepEntity1.removeWFMobileIDStepTrustedIdentityEntity(obj);
								wfMobileIDStepTrustedIdentityRepository.delete(obj.getId());
							}
							for(WFStepAdjudicationGroupConfigEntity obj : wfStepAdjudicationGroupConfigsToRemove) {
								wfStepEntity1.removeWFStepAdjudicationGroupConfigEntity(obj);
								wfStepAdjudicationGroupConfigRepository.delete(obj.getId());
							}
						}
						if (workflowStep.getName().equalsIgnoreCase("MOBILE_ID_IDENTITY_ISSUANCE")) {
							
							List<WFMobileIDStepIdentityConfig> wfMobileIdStepIdentityList = new ArrayList<>();
							List<String> listOfFriendlyName = new ArrayList<>();
							// here getting all WfMobileIDStepIdentityConfigs from workflow and counting the WfMobileIDStepIdentityConfigs
							wfMobileIdStepIdentityList = workflowStep.getWfMobileIDStepIdentityConfigs();
							long countOfFriendlyNameGivenByUser = wfMobileIdStepIdentityList.size();

							// here adding the user given workflow friendlynames to listOfFriendlyName variable and counting the friendlyName
							wfMobileIdStepIdentityList.stream().forEach(e -> listOfFriendlyName.add(e.getFriendlyName()));
							listOfFriendlyName.replaceAll(String::toUpperCase);
							long countOfFriendlyName = listOfFriendlyName.stream().distinct().count();

							// here we are checking both user given names and unique friendlyname count are same or not
							if (countOfFriendlyName == countOfFriendlyNameGivenByUser) {
								
							wfStepEntity.getWfMobileIDStepIdentityConfigs().forEach(mobileIdentityEntity -> {
								List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdentityConfigs = workflowStep.getWfMobileIDStepIdentityConfigs().stream().filter(identity -> mobileIdentityEntity.getId().
										equals(identity.getWfMobileIDStepIdentityConfigId())).collect(Collectors.toList());
								if(wfMobileIDStepIdentityConfigs.size() == 0) {
									wfMobileIDIdentityIssuanceEntitiesToRemove.add(mobileIdentityEntity);
								}
							});
							//workflowStep.getWfMobileIDStepIdentityConfigs().stream().forEach(identityConfig -> {
							for( WFMobileIDStepIdentityConfig identityConfig:workflowStep.getWfMobileIDStepIdentityConfigs()) {
								boolean check;
								if (identityConfig.getWfMobileIDStepIdentityConfigId() == null) {
									check = isFriendlyNameExist(identityConfig.getFriendlyName(), organizationName);
								} else {
									check = isFriendlyNameExistsInUpdate(identityConfig.getFriendlyName(),organizationName, identityConfig.getWfMobileIDStepIdentityConfigId());
								}
								if(check){
								WFMobileIDIdentityIssuanceEntity wfMobileIDStepIdentityConfigEntity = null;
								boolean edit = false;
								
								List<WFMobileIDIdentityIssuanceEntity> wfMobileIDStepIdentityConfigEntities = wfStepEntity.getWfMobileIDStepIdentityConfigs().stream().filter(wfMobileIDIssuanceEntity -> wfMobileIDIssuanceEntity.getId().equals(identityConfig.getWfMobileIDStepIdentityConfigId())).collect(Collectors.toList());
								if (wfMobileIDStepIdentityConfigEntities.size() != 0) {
									wfMobileIDStepIdentityConfigEntity = wfMobileIDStepIdentityConfigEntities.get(0);
									edit = true;
								} else {
									
									wfMobileIDStepIdentityConfigEntity = new WFMobileIDIdentityIssuanceEntity(); 
								}
								wfMobileIDStepIdentityConfigEntity.setFriendlyName(identityConfig.getFriendlyName());
								wfMobileIDStepIdentityConfigEntity.setPushVerify(identityConfig.getPushVerify());
								wfMobileIDStepIdentityConfigEntity.setSoftOTP(identityConfig.getSoftOTP());
								wfMobileIDStepIdentityConfigEntity.setFido2(identityConfig.getFido2());
								wfMobileIDStepIdentityConfigEntity.setIsContentSigning(identityConfig.getIsContentSigning());
								
								if(identityConfig.getSoftOTP() == true) {
									WFMobileIDIdentitySoftOTPEntity wfMobileIDIdentitySoftOTPEntity = new WFMobileIDIdentitySoftOTPEntity();
									System.out.println(wfMobileIDStepIdentityConfigEntity.getWFMobileIDIdentitySoftOTPEntity());
									if(wfMobileIDStepIdentityConfigEntity.getWFMobileIDIdentitySoftOTPEntity() != null) {
										if(wfMobileIDStepIdentityConfigEntity.getWFMobileIDIdentitySoftOTPEntity().getId() != null) {
											wfMobileIDIdentitySoftOTPEntity = wfMobileIDStepIdentityConfigEntity.getWFMobileIDIdentitySoftOTPEntity();
										}
									}
									WFMobileIDIdentitySoftOTPConfig wfMobileIDIdentitySoftOTPConfig = identityConfig.getWfMobileIDIdentitySoftOTPConfig();
									wfMobileIDIdentitySoftOTPEntity.setSoftOtpType(wfMobileIDIdentitySoftOTPConfig.getSoftOTPType());
									wfMobileIDIdentitySoftOTPEntity.setAlgorithm(wfMobileIDIdentitySoftOTPConfig.getAlgorithm());
									// wfMobileIDIdentitySoftOTPEntity.setSecretKey(wfMobileIDIdentitySoftOTPConfig.getSecretKey());
									wfMobileIDIdentitySoftOTPEntity.setDigits(wfMobileIDIdentitySoftOTPConfig.getDigits());
									wfMobileIDIdentitySoftOTPEntity.setPeriod(wfMobileIDIdentitySoftOTPConfig.getInterval());
									wfMobileIDIdentitySoftOTPEntity.setCounter(wfMobileIDIdentitySoftOTPConfig.getCounter());
									wfMobileIDStepIdentityConfigEntity.setWFMobileIDIdentitySoftOTPEntity(wfMobileIDIdentitySoftOTPEntity);
									wfMobileIDIdentitySoftOTPEntity.setWfMobileIDIdentityIssuance(wfMobileIDStepIdentityConfigEntity);
								} else {
									if(wfMobileIDStepIdentityConfigEntity.getWFMobileIDIdentitySoftOTPEntity() != null) {
										if(wfMobileIDStepIdentityConfigEntity.getWFMobileIDIdentitySoftOTPEntity().getId() != null) {
											WFMobileIDIdentitySoftOTPEntity wfMobileIDIdentitySoftOTPEntity = wfMobileIDStepIdentityConfigEntity.getWFMobileIDIdentitySoftOTPEntity();
											// delete this entity
											wfMobileIDStepIdentityConfigEntity.setWFMobileIDIdentitySoftOTPEntity(null);
											wfMobileIDIdentitySoftOTPRepository.delete(wfMobileIDIdentitySoftOTPEntity);
										}
									}
								}
								
								if (identityConfig.getWfMobileIdStepVisualIDGroupConfigs().size() > 0) {
									for (WFMobileIdStepVisualIDGroupConfig visuaIdGroupConfig : identityConfig.getWfMobileIdStepVisualIDGroupConfigs()) {
										WFMobileIdStepVisualIDGroupConfigEntity wfMobileIdStepVisualIDGroupConfigEntity = null;
										if (edit) {
											List<WFMobileIdStepVisualIDGroupConfigEntity> wfMobileIdStepVisualIDGroupConfigEntities = wfMobileIDStepIdentityConfigEntity.getWfMobileIdStepVisualIDGroupConfigs().stream()
													.filter(entity -> entity.getReferenceGroupId().equalsIgnoreCase(visuaIdGroupConfig.getReferenceGroupId()))
													.collect(Collectors.toList());
											if(wfMobileIdStepVisualIDGroupConfigEntities.size() == 0) {
												wfMobileIdStepVisualIDGroupConfigEntity = new WFMobileIdStepVisualIDGroupConfigEntity();
												wfMobileIdStepVisualIDGroupConfigEntity.setReferenceGroupId(visuaIdGroupConfig.getReferenceGroupId());
												wfMobileIdStepVisualIDGroupConfigEntity.setReferenceGroupName(visuaIdGroupConfig.getReferenceGroupName());
												wfMobileIdStepVisualIDGroupConfigEntity.setVisualTemplate(visualTemplateRepository.getOne(visuaIdGroupConfig.getVisualTemplateId()));
											} else {
												wfMobileIdStepVisualIDGroupConfigEntity = wfMobileIdStepVisualIDGroupConfigEntities.get(0);
												wfMobileIdStepVisualIDGroupConfigEntity.setVisualTemplate(visualTemplateRepository.getOne(visuaIdGroupConfig.getVisualTemplateId()));
											}
										} else {
											wfMobileIdStepVisualIDGroupConfigEntity = new WFMobileIdStepVisualIDGroupConfigEntity();
											wfMobileIdStepVisualIDGroupConfigEntity.setReferenceGroupId(visuaIdGroupConfig.getReferenceGroupId());
											wfMobileIdStepVisualIDGroupConfigEntity.setReferenceGroupName(visuaIdGroupConfig.getReferenceGroupName());
											wfMobileIdStepVisualIDGroupConfigEntity.setVisualTemplate(visualTemplateRepository.getOne(visuaIdGroupConfig.getVisualTemplateId()));
										}
										wfMobileIDStepIdentityConfigEntity.addWFMobileIdStepVisualIDGroupConfig(wfMobileIdStepVisualIDGroupConfigEntity);
									}
								}
								if (identityConfig.getWfMobileIdStepCertificates().size() > 0) {
									List<WFMobileIdStepCertificatesEntity> passiveWfStepCertsConfigEntities = new ArrayList<>();
									
									wfMobileIDStepIdentityConfigEntity.getWfMobileIdStepCertificates().forEach(entity -> {
										List<WFMobileIdStepCertificate> wfMobileIdStepCertificates = identityConfig.getWfMobileIdStepCertificates().stream().filter(e -> entity.getId().equals(e.getWfMobileIdStepCertId())).collect(Collectors.toList());
										if(wfMobileIdStepCertificates.size() == 0) {
											if (!entity.getStatus().equalsIgnoreCase("PASSIVE")) {
												entity.setStatus("PASSIVE");
											}
										}
									});
									
									WFMobileIDStepCertConfigEntity wfMobileIDStepCertConfigEntity = new WFMobileIDStepCertConfigEntity();
									for (WFMobileIdStepCertificate mobileIdStepCertConfig : identityConfig.getWfMobileIdStepCertificates()) {
										WFMobileIdStepCertificatesEntity wfMobileIdStepCertEntity = new WFMobileIdStepCertificatesEntity();
										if (edit) {
											if(wfMobileIDStepIdentityConfigEntity.getWfMobileIdStepCertificates().size()>0) {
												wfMobileIDStepCertConfigEntity = wfMobileIDStepIdentityConfigEntity.getWfMobileIDStepCertConfig();
											}
										}
										List<WFMobileIdStepCertificatesEntity> wfMobileIDStepCertConfigEntities = wfMobileIDStepIdentityConfigEntity.getWfMobileIdStepCertificates().stream()
												.filter(e -> e.getId()
														.equals(mobileIdStepCertConfig.getWfMobileIdStepCertId()))
												.collect(Collectors.toList());
										if(wfMobileIDStepCertConfigEntities.size() != 0) {
											wfMobileIdStepCertEntity = wfMobileIDStepCertConfigEntities.get(0);
											if (wfMobileIdStepCertEntity.getStatus().equalsIgnoreCase("ACTIVE")) {
												boolean isCertSame = workflowServiceHelper.compareWfMobleIdStepCertObjects(mobileIdStepCertConfig, wfMobileIdStepCertEntity);
												if (!isCertSame) {
													wfMobileIdStepCertEntity.setStatus("PASSIVE");
													passiveWfStepCertsConfigEntities.add(wfMobileIdStepCertEntity);
													
													wfMobileIdStepCertEntity = new WFMobileIdStepCertificatesEntity();
													wfMobileIdStepCertEntity.setCertType(mobileIdStepCertConfig.getCertType());
													wfMobileIdStepCertEntity.setCaServer(mobileIdStepCertConfig.getCaServer());
													wfMobileIdStepCertEntity.setCertTemplate(mobileIdStepCertConfig.getCertTemplate());
													wfMobileIdStepCertEntity.setKeyEscrow(mobileIdStepCertConfig.isKeyEscrow());
													wfMobileIdStepCertEntity.setDisableRevoke(mobileIdStepCertConfig.isDisableRevoke());
													wfMobileIdStepCertEntity.setAlgorithm(mobileIdStepCertConfig.getAlgorithm());
													wfMobileIdStepCertEntity.setKeySize(mobileIdStepCertConfig.getKeysize());
													wfMobileIdStepCertEntity.setSubjectDN(mobileIdStepCertConfig.getSubjectDN());
													wfMobileIdStepCertEntity.setSubjectAN(mobileIdStepCertConfig.getSubjectAN());
													wfMobileIdStepCertEntity.setStatus("ACTIVE");
													
													wfMobileIDStepIdentityConfigEntity.addWfMobileIdStepCertificate(wfMobileIdStepCertEntity);
												}
											}
										} else {
											wfMobileIdStepCertEntity.setCertType(mobileIdStepCertConfig.getCertType());
											wfMobileIdStepCertEntity.setCaServer(mobileIdStepCertConfig.getCaServer());
											wfMobileIdStepCertEntity.setCertTemplate(mobileIdStepCertConfig.getCertTemplate());
											wfMobileIdStepCertEntity.setKeyEscrow(mobileIdStepCertConfig.isKeyEscrow());
											wfMobileIdStepCertEntity.setDisableRevoke(mobileIdStepCertConfig.isDisableRevoke());
											wfMobileIdStepCertEntity.setAlgorithm(mobileIdStepCertConfig.getAlgorithm());
											wfMobileIdStepCertEntity.setKeySize(mobileIdStepCertConfig.getKeysize());
											wfMobileIdStepCertEntity.setSubjectDN(mobileIdStepCertConfig.getSubjectDN());
											wfMobileIdStepCertEntity.setSubjectAN(mobileIdStepCertConfig.getSubjectAN());
											wfMobileIdStepCertEntity.setStatus("ACTIVE");
											
											wfMobileIDStepIdentityConfigEntity.addWfMobileIdStepCertificate(wfMobileIdStepCertEntity);
										}
									}
									//wfMobileIDStepCertConfigEntity.setIsContentSigning(identityConfig.getWfMobileIDStepCertConfig().getIsContentSigning());
									wfMobileIDStepCertConfigEntity.setWfMobileIDIdentityIssuance(wfMobileIDStepIdentityConfigEntity);
									wfMobileIDStepCertConfigEntity.setNoOfDaysBeforeCertExpiry(configValueRepository.findByConfigNameAndValue(organizationName,
													identityConfig.getWfMobileIDStepCertConfig().getNoOfDaysBeforeCertExpiry().getName(),
													identityConfig.getWfMobileIDStepCertConfig().getNoOfDaysBeforeCertExpiry().getValue()));
									wfMobileIDStepCertConfigEntity.setEmailNotificationFreq(configValueRepository.findByConfigNameAndValue(organizationName,identityConfig.getWfMobileIDStepCertConfig().getEmailNotificationFreq().getName(),
													identityConfig.getWfMobileIDStepCertConfig().getEmailNotificationFreq().getValue()));
									wfMobileIDStepIdentityConfigEntity.setWfMobileIDStepCertConfig(wfMobileIDStepCertConfigEntity);
								 } else {
									wfMobileIDStepIdentityConfigEntity.getWfMobileIdStepCertificates().stream().forEach(wfMobileIDConfigentity -> {
										if(!wfMobileIDConfigentity.getStatus().equalsIgnoreCase("PASSIVE")) {
											wfMobileIDConfigentity.setStatus("PASSIVE");
										}
									});
								 }
							    wfStepEntity.addWFMobileIDStepIdentityConfigs(wfMobileIDStepIdentityConfigEntity);	
								} else {
									log.error(ApplicationConstants.notifyAdmin,
											Util.format(ErrorMessages.FRIENDLYNAME_ALREADY_EXISTS_WITH_SAME_NAME,identityConfig.getFriendlyName()));
									return new ResponseEntity<String>("Friendly Name aleady exists",org.springframework.http.HttpStatus.CONFLICT);
								}
							}
						} else {
							log.error("SAME FRIENDLYNAME ARE PRESENT IN WORKFLOW PLEASE CHECK");
							return new ResponseEntity<String>("SAME FRIENDLYNAME ARE PRESENT IN WORKFLOW PLEASE CHECK",org.springframework.http.HttpStatus.BAD_REQUEST);

						}
							workflowEntity.addWfStep(wfStepEntity);
							for(WFMobileIDIdentityIssuanceEntity obj : wfMobileIDIdentityIssuanceEntitiesToRemove) {
								wfStepEntity1.removeWFMobileIDStepIdentityConfigs(obj);
								wfMobileIDIdentityIssuanceRepository.delete(obj.getId());
							}
						}
					}
				}
				// to remove wf steps which are not checked in ui except pki certs and wf mobile id issuance
				for (WFStepEntity wfStepEntity : workflowEntity.getWfSteps()) {
					List<WorkflowStep> wfSteps = workflow.getWorkflowSteps().stream()
							.filter(wfStep -> wfStep.getName().equalsIgnoreCase(wfStepEntity.getWfStepDef().getName()))
							.collect(Collectors.toList());
					 if(!wfStepEntity.getWfStepDef().getName().equalsIgnoreCase("PKI_CERTIFICATES") && !wfStepEntity.getWfStepDef().getName().equalsIgnoreCase("MOBILE_ID_IDENTITY_ISSUANCE")) {
						if(wfSteps.size()==0) {
							wfStepsToRemove.add(wfStepEntity);
						}
					} else if (wfStepEntity.getWfStepDef().getName().equalsIgnoreCase("PKI_CERTIFICATES")) {
						if(wfSteps.size()==0) {
							if (!wfStepEntity.getStatus().equalsIgnoreCase("PASSIVE"))
								wfStepEntity.setStatus("PASSIVE");
							// change the status to passive
							wfStepEntity.getWfStepCertConfigs().stream().forEach(wfStepCertEntity -> {
								 if(!wfStepCertEntity.getStatus().equalsIgnoreCase("PASSIVE")) {
									 wfStepCertEntity.setStatus("PASSIVE");
								 }
							});
						}
					} else if (wfStepEntity.getWfStepDef().getName().equalsIgnoreCase("MOBILE_ID_IDENTITY_ISSUANCE")) {
						workflowEntity.setRequiredMobileIssuance(true);
					}
				}
				for (WFStepEntity obj : wfStepsToRemove) {
					 workflowEntity.removeWfStep(obj);
					 wfStepRepository.delete(obj.getId());
				}
			}
		} catch (WorkflowServiceException cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_ASSIGN_STEPS_TO_WORKFLOW_UNDER_ORGANIZATION, workflowEntity.getName(), organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_ASSIGN_STEPS_TO_WORKFLOW_UNDER_ORGANIZATION,  workflowEntity.getName(), organizationName);
		}
		return new ResponseEntity<>(org.springframework.http.HttpStatus.OK);
	}

	public ResponseEntity<String>  setWorkflowSteps(WorkflowStep workflowStep, String organizationName, WorkflowEntity workflowEntity) throws WorkflowServiceException  {
     try {
		WFStepEntity wfStepEntity = new WFStepEntity();
		WFStepDefEntity stepDef = wfStepDefRepository.findByName(workflowStep.getName());
		wfStepEntity.setWfStepDef(stepDef);
		wfStepEntity.setStatus("ACTIVE");
		// ADD workflow step details
		if (workflowStep.getWfStepGenericConfigs() != null) {
			workflowStep.getWfStepGenericConfigs().stream().forEach(wfStepConfig -> {
				WFStepConfigEntity wfStepConfigEntity = new WFStepConfigEntity();
				wfStepConfigEntity.setConfigValue(
						configValueRepository.findByConfigNameAndValue(organizationName, wfStepConfig.getName(), wfStepConfig.getValue()));
				wfStepEntity.addWfStepConfig(wfStepConfigEntity);
			});
		}
		
		if (workflowStep.getName().equalsIgnoreCase("REGISTRATION")) {
			if (workflowStep.getWfStepRegistrationConfigs() != null) {
				workflowStep.getWfStepRegistrationConfigs().stream().forEach(wfStepRegistrationConfig -> {
					WFStepRegistrationConfigEntity wfStepRegistrationConfigEntity = new WFStepRegistrationConfigEntity();
					wfStepRegistrationConfigEntity.setOrgIdentityField(orgIdentityFieldRepository.findOne(wfStepRegistrationConfig.getOrgFieldId()));
					wfStepRegistrationConfigEntity.setFieldLabel(wfStepRegistrationConfig.getFieldLabel());
					wfStepRegistrationConfigEntity.setRequired(wfStepRegistrationConfig.isRequired());
					
					wfStepEntity.addWfStepRegistrationConfig(wfStepRegistrationConfigEntity);
				});
			}
		}
		
		if (workflowStep.getName().equalsIgnoreCase("APPROVAL_BEFORE_ISSUANCE")) {
			workflowStep.getWfStepApprovalGroupConfigs().stream().forEach(wfStepApprovalGroupConfig -> {
				WFStepApprovalGroupConfigEntity wfStepApprovalGroupConfigEntity = new WFStepApprovalGroupConfigEntity();
				wfStepApprovalGroupConfigEntity.setReferenceApprovalGroupId(wfStepApprovalGroupConfig.getGroupId());
				wfStepApprovalGroupConfigEntity.setReferenceApprovalGroupName(wfStepApprovalGroupConfig.getGroupName());	
				
				wfStepEntity.addWfStepApprovalGroupConfig(wfStepApprovalGroupConfigEntity);
			});
		}
		
		if (workflowStep.getName().equalsIgnoreCase("PERSO_AND_ISSUANCE")) {
			
			if(workflowStep.getWfStepAppletLoadingInfo() != null) {
				
				WfStepAppletLoadingInfo wfAppletLoadingInfo = workflowStep.getWfStepAppletLoadingInfo();
				
				WfStepAppletLoadingInfoEntity wfAppletLoadingInfoEntity = new WfStepAppletLoadingInfoEntity();
				wfAppletLoadingInfoEntity.setPivAppletEnabled(wfAppletLoadingInfo.getPivAppletEnabled());
				wfAppletLoadingInfoEntity.setFido2AppletEnabled(wfAppletLoadingInfo.getFido2AppletEnabled());
				if(wfAppletLoadingInfoEntity.getFido2AppletEnabled()) {
					wfAppletLoadingInfoEntity.setAttestationCert(wfAppletLoadingInfo.getAttestationCert());
					wfAppletLoadingInfoEntity.setAttestationCertPrivateKey(wfAppletLoadingInfo.getAttestationCertPrivateKey());
					wfAppletLoadingInfoEntity.setAaguid(wfAppletLoadingInfo.getAaguid());
				}
				wfAppletLoadingInfoEntity.setWfStep(wfStepEntity);
				wfStepEntity.setWfAppletLoadingInfo(wfAppletLoadingInfoEntity);
			}
			
			// set allowed chip encode , chip encode+visual ID print, visual ID print groups
			workflowStep.getWfStepChipEncodeGroupConfigs().stream().forEach(wfStepChipEncodeGroupConfig -> {
				WfStepChipEncodeGroupConfigEntity wfStepChipEncodeGroupConfigEntity = new WfStepChipEncodeGroupConfigEntity();
				wfStepChipEncodeGroupConfigEntity
						.setReferenceChipEncodeGroupId(wfStepChipEncodeGroupConfig.getGroupId());
				wfStepChipEncodeGroupConfigEntity
						.setReferenceChipEncodeGroupName(wfStepChipEncodeGroupConfig.getGroupName());

				wfStepEntity.addWfStepChipEncodeGroupConfigEntity(wfStepChipEncodeGroupConfigEntity);
			});
			workflowStep.getWfStepChipEncodeVIDPrintGroupConfigs().stream().forEach(wfStepChipEncodeVIDGroupConfig -> {
				WfStepChipEncodeAndVIDPrintGroupConfigEntity wfStepChipEncodeVIDGroupConfigEntity = new WfStepChipEncodeAndVIDPrintGroupConfigEntity();
				wfStepChipEncodeVIDGroupConfigEntity
						.setReferenceChipEncodeVIDPrintGroupId(wfStepChipEncodeVIDGroupConfig.getGroupId());
				wfStepChipEncodeVIDGroupConfigEntity
						.setReferenceChipEncodeVIDPrintGroupName(wfStepChipEncodeVIDGroupConfig.getGroupName());

				wfStepEntity.addWfStepChipEncodeAndVIDPrintGroupConfigEntity(wfStepChipEncodeVIDGroupConfigEntity);
			});
			workflowStep.getWfStepPrintGroupConfigs().stream().forEach(wfStepPrintGroupConfig -> {
				WfStepPrintGroupConfigEntity wfStepPrintGroupConfigEntity = new WfStepPrintGroupConfigEntity();
				wfStepPrintGroupConfigEntity
						.setReferencePrintGroupId(wfStepPrintGroupConfig.getGroupId());
				wfStepPrintGroupConfigEntity
						.setReferencePrintGroupName(wfStepPrintGroupConfig.getGroupName());

				wfStepEntity.addWfStepPrintGroupConfigs(wfStepPrintGroupConfigEntity);
			});
		}
		
		if (workflowStep.getName().equalsIgnoreCase("PKI_CERTIFICATES")) {
			if (workflowStep.getWfStepCertConfigs() != null) {
				workflowStep.getWfStepCertConfigs().stream().forEach(wfStepCertConfig -> {
					WFStepCertConfigEntity wfStepCertConfigEntity = new WFStepCertConfigEntity();
					wfStepCertConfigEntity.setCertType(wfStepCertConfig.getCertType());
					wfStepCertConfigEntity.setCaServer(wfStepCertConfig.getCaServer());
					wfStepCertConfigEntity.setCertTemplate(wfStepCertConfig.getCertTemplate());
					wfStepCertConfigEntity.setKeyEscrow(wfStepCertConfig.isKeyEscrow());
					wfStepCertConfigEntity.setDisableRevoke(wfStepCertConfig.isDisableRevoke());
					wfStepCertConfigEntity.setAlgorithm(wfStepCertConfig.getAlgorithm());
					wfStepCertConfigEntity.setKeySize(wfStepCertConfig.getKeysize());
					wfStepCertConfigEntity.setSubjectDN(wfStepCertConfig.getSubjectDN());
					wfStepCertConfigEntity.setSubjectAN(wfStepCertConfig.getSubjectAN());
					wfStepCertConfigEntity.setStatus("ACTIVE");
					
					wfStepEntity.addWfStepCertConfig(wfStepCertConfigEntity);
				});
			}
		}
	
		if (workflowStep.getName().equalsIgnoreCase("MOBILE_ID_ONBOARDING_CONFIG")) {
			if (workflowStep.getWfMobileIDStepTrustedIdentities().size() > 0) {
				workflowStep.getWfMobileIDStepTrustedIdentities().forEach(trustedIdentity -> {
					WFMobileIDStepTrustedIdentityEntity wfMobileIDStepTrustedIdentityEntity = new WFMobileIDStepTrustedIdentityEntity();
					wfMobileIDStepTrustedIdentityEntity.setOrganizationIdentities( organizationIdentitiesRepository.getOne(trustedIdentity.getId()));
					wfStepEntity .addWFMobileIDStepTrustedIdentityEntity(wfMobileIDStepTrustedIdentityEntity);
				});
			}
			if (workflowStep.getWfStepAdjudicationGroupConfigs().size() > 0) {
				workflowStep.getWfStepAdjudicationGroupConfigs().forEach(adjudicationGroup -> {
					WFStepAdjudicationGroupConfigEntity wfStepAdjudicationGroupEntity = new WFStepAdjudicationGroupConfigEntity();
					wfStepAdjudicationGroupEntity .setReferenceAdjudicationGroupId(adjudicationGroup.getId());
					wfStepAdjudicationGroupEntity .setReferenceAdjudicationGroupName(adjudicationGroup.getName());
					wfStepEntity.addWFStepAdjudicationGroupConfigEntity(wfStepAdjudicationGroupEntity);
				});
			}
		}
		
		if (workflowStep.getName().equalsIgnoreCase("MOBILE_ID_IDENTITY_ISSUANCE")) {
			
			List<WFMobileIDStepIdentityConfig> WFMobileIDStepIdentityConfigList=new ArrayList<>();
			List<String> listOfFriendlyName=new ArrayList<>();
			
			//here getting all WfMobileIDStepIdentityConfigs from workflow and counting the WfMobileIDStepIdentityConfigs 
			WFMobileIDStepIdentityConfigList=workflowStep.getWfMobileIDStepIdentityConfigs();
			long countOfFriendlyNameGivenByUser= WFMobileIDStepIdentityConfigList.size();
			
			//here adding the user given workflow friendlynames to listOfFriendlyName variable and counting the friendlyName
			WFMobileIDStepIdentityConfigList.stream().forEach(e->listOfFriendlyName.add(e.getFriendlyName()));
			listOfFriendlyName.replaceAll(String::toUpperCase);
			long countOfFriendlyName=listOfFriendlyName.stream().distinct().count();
			
			//here we are checking both user given names and unique friendlyname count are same or not					
			if(countOfFriendlyName==countOfFriendlyNameGivenByUser) {
			//workflowStep.getWfMobileIDStepIdentityConfigs().stream().forEach(identityConfig -> {
				for(WFMobileIDStepIdentityConfig identityConfig : workflowStep.getWfMobileIDStepIdentityConfigs()) {
				WFMobileIDIdentityIssuanceEntity wfMobileIDStepIdentityConfigEntity = new WFMobileIDIdentityIssuanceEntity();
				
				if(isFriendlyNameExist(identityConfig.getFriendlyName(),organizationName)) {
				wfMobileIDStepIdentityConfigEntity.setFriendlyName(identityConfig.getFriendlyName());
				if(identityConfig.getWfMobileIdStepVisualIDGroupConfigs().size() > 0) {
					identityConfig.getWfMobileIdStepVisualIDGroupConfigs().stream().forEach(visuaIdGroupConfig -> {
						WFMobileIdStepVisualIDGroupConfigEntity wfMobileIdStepVisualIDGroupConfigEntity = new WFMobileIdStepVisualIDGroupConfigEntity();
						wfMobileIdStepVisualIDGroupConfigEntity.setReferenceGroupId(visuaIdGroupConfig.getReferenceGroupId());
						wfMobileIdStepVisualIDGroupConfigEntity.setReferenceGroupName(visuaIdGroupConfig.getReferenceGroupName());
						wfMobileIdStepVisualIDGroupConfigEntity.setVisualTemplate(visualTemplateRepository.getOne(visuaIdGroupConfig.getVisualTemplateId()));
						
						wfMobileIDStepIdentityConfigEntity.addWFMobileIdStepVisualIDGroupConfig(wfMobileIdStepVisualIDGroupConfigEntity);
					});
				}
				wfMobileIDStepIdentityConfigEntity.setPushVerify(identityConfig.getPushVerify());
				wfMobileIDStepIdentityConfigEntity.setSoftOTP(identityConfig.getSoftOTP());
				wfMobileIDStepIdentityConfigEntity.setFido2(identityConfig.getFido2());
				wfMobileIDStepIdentityConfigEntity.setIsContentSigning(identityConfig.getIsContentSigning());
				if(identityConfig.getSoftOTP() == true) {
					
					WFMobileIDIdentitySoftOTPEntity wfMobileIDIdentitySoftOTPEntity = new WFMobileIDIdentitySoftOTPEntity();
					WFMobileIDIdentitySoftOTPConfig wfMobileIDIdentitySoftOTPConfig = identityConfig.getWfMobileIDIdentitySoftOTPConfig();
					wfMobileIDIdentitySoftOTPEntity.setSoftOtpType(wfMobileIDIdentitySoftOTPConfig.getSoftOTPType());
					wfMobileIDIdentitySoftOTPEntity.setAlgorithm(wfMobileIDIdentitySoftOTPConfig.getAlgorithm());
					// wfMobileIDIdentitySoftOTPEntity.setSecretKey(wfMobileIDIdentitySoftOTPConfig.getSecretKey());
					wfMobileIDIdentitySoftOTPEntity.setDigits(wfMobileIDIdentitySoftOTPConfig.getDigits());
					wfMobileIDIdentitySoftOTPEntity.setPeriod(wfMobileIDIdentitySoftOTPConfig.getInterval());
					wfMobileIDIdentitySoftOTPEntity.setCounter(wfMobileIDIdentitySoftOTPConfig.getCounter());
					wfMobileIDStepIdentityConfigEntity.setWFMobileIDIdentitySoftOTPEntity(wfMobileIDIdentitySoftOTPEntity);
					wfMobileIDIdentitySoftOTPEntity.setWfMobileIDIdentityIssuance(wfMobileIDStepIdentityConfigEntity);
				
				}
				
				if(identityConfig.getWfMobileIdStepCertificates().size() > 0) {
					identityConfig.getWfMobileIdStepCertificates().stream().forEach(mobileIdStepCertConfig -> {
						WFMobileIdStepCertificatesEntity wfMobileIdStepCertConfigEntity = new WFMobileIdStepCertificatesEntity();
						
						wfMobileIdStepCertConfigEntity.setCertType(mobileIdStepCertConfig.getCertType());
						wfMobileIdStepCertConfigEntity.setCaServer(mobileIdStepCertConfig.getCaServer());
						wfMobileIdStepCertConfigEntity.setCertTemplate(mobileIdStepCertConfig.getCertTemplate());
						wfMobileIdStepCertConfigEntity.setKeyEscrow(mobileIdStepCertConfig.isKeyEscrow());
						wfMobileIdStepCertConfigEntity.setDisableRevoke(mobileIdStepCertConfig.isDisableRevoke());
						wfMobileIdStepCertConfigEntity.setAlgorithm(mobileIdStepCertConfig.getAlgorithm());
						wfMobileIdStepCertConfigEntity.setKeySize(mobileIdStepCertConfig.getKeysize());
						wfMobileIdStepCertConfigEntity.setSubjectDN(mobileIdStepCertConfig.getSubjectDN());
						wfMobileIdStepCertConfigEntity.setSubjectAN(mobileIdStepCertConfig.getSubjectAN());
						wfMobileIdStepCertConfigEntity.setStatus("ACTIVE");
						
						wfMobileIDStepIdentityConfigEntity.addWfMobileIdStepCertificate(wfMobileIdStepCertConfigEntity);
					});
					WFMobileIDStepCertConfigEntity wfMobileIDStepCertConfigEntity = new WFMobileIDStepCertConfigEntity();
					//wfMobileIDStepCertConfigEntity.setIsContentSigning(identityConfig.getWfMobileIDStepCertConfig().getIsContentSigning());
					wfMobileIDStepCertConfigEntity.setWfMobileIDIdentityIssuance(wfMobileIDStepIdentityConfigEntity);
					wfMobileIDStepCertConfigEntity.setNoOfDaysBeforeCertExpiry(
							configValueRepository.findByConfigNameAndValue(organizationName,
									identityConfig.getWfMobileIDStepCertConfig().getNoOfDaysBeforeCertExpiry().getName(),
									identityConfig.getWfMobileIDStepCertConfig().getNoOfDaysBeforeCertExpiry().getValue()));
					wfMobileIDStepCertConfigEntity.setEmailNotificationFreq(
							configValueRepository.findByConfigNameAndValue(organizationName,
									identityConfig.getWfMobileIDStepCertConfig().getEmailNotificationFreq().getName(),
									identityConfig.getWfMobileIDStepCertConfig().getEmailNotificationFreq().getValue()));
					wfMobileIDStepIdentityConfigEntity.setWfMobileIDStepCertConfig(wfMobileIDStepCertConfigEntity);
				}
				wfStepEntity.addWFMobileIDStepIdentityConfigs(wfMobileIDStepIdentityConfigEntity);
				} else {
					log.error(ApplicationConstants.notifyAdmin,
							Util.format(ErrorMessages.FRIENDLYNAME_ALREADY_EXISTS_WITH_SAME_NAME,identityConfig.getFriendlyName()));
					return new ResponseEntity<>("Friendly Name aleady exists",org.springframework.http.HttpStatus.CONFLICT);
				}
			}
		} else {
			log.error("SAME FRIENDLYNAME ARE PRESENT IN WORKFLOW PLEASE CHECK");
			return new ResponseEntity<>("SAME FRIENDLYNAME ARE PRESENT IN WORKFLOW PLEASE CHECK",org.springframework.http.HttpStatus.BAD_REQUEST);
		}
		}
		workflowEntity.addWfStep(wfStepEntity);
	} catch (Exception cause) {
		log.error(ApplicationConstants.notifyAdmin,
				Util.format(ErrorMessages.FAILED_TO_ASSIGN_STEPS_TO_WORKFLOW_UNDER_ORGANIZATION, workflowEntity.getName(), organizationName) + " : " + cause.getMessage());
		throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_ASSIGN_STEPS_TO_WORKFLOW_UNDER_ORGANIZATION,  workflowEntity.getName(), organizationName);
	 }
	return new ResponseEntity<>(HttpStatus.OK);
	}
	private void setDeviceProfilesToWorkflow(Workflow workflow, WorkflowEntity workflowEntity) {
		if (workflow.getDeviceProfiles() != null) {
			List<WFDeviceProfileEntity> devProfToRemove = new ArrayList<>();
			workflowEntity.getWfDeviceProfiles().stream().forEach(devProfEntity -> {
				List<DeviceProfile> devProfiles = workflow.getDeviceProfiles().stream().filter(
						devProf -> devProf.getName().equalsIgnoreCase(devProfEntity.getDeviceProfile().getName())).collect(Collectors.toList());
				if (devProfiles.size() == 0) {
					devProfToRemove.add(devProfEntity);
				}
			});
			workflow.getDeviceProfiles().stream().forEach(deviceProfile -> {
				List<WFDeviceProfileEntity> list = workflowEntity.getWfDeviceProfiles().stream().filter(devProfEntity -> 
					devProfEntity.getDeviceProfile().getName().equalsIgnoreCase(deviceProfile.getName())
				).collect(Collectors.toList());
				WFDeviceProfileEntity wfDeviceProfileEntity = new WFDeviceProfileEntity();
				if (list.size() == 0) {
					DeviceProfileEntity devProf = devProfRepository.findByNameAndOrganization(workflowEntity.getOrganization().getName(), deviceProfile.getName());
					wfDeviceProfileEntity.setDeviceProfile(devProf);
					workflowEntity.addWfDeviceProfile(wfDeviceProfileEntity);
				}
			});
			for (WFDeviceProfileEntity wfDeviceProfileEntity : devProfToRemove) {
				workflowEntity.removeWfDeviceProfile(wfDeviceProfileEntity);
			}
		}
	}

	private void setGroupsToWorkflow(Workflow workflow, WorkflowEntity workflowEntity) {
		List<WFGroupEntity> wfGroupsToRemove = new ArrayList<>();
		if (workflow.getGroups() != null) {
			workflow.getGroups().stream().forEach(group -> {
				WFGroupEntity wfGroupEntity  = new WFGroupEntity();
				List<WFGroupEntity> wfGroupEntityList = workflowEntity.getWfGroups().stream().filter(groupEntity -> 
					groupEntity.getReferenceGroupId().equalsIgnoreCase(group.getId())
				).collect(Collectors.toList());
				
				if(wfGroupEntityList.size() == 0) {
					wfGroupEntity.setReferenceGroupId(group.getId());
					wfGroupEntity.setReferenceGroupName(group.getName());
					if(group.getVisualTemplateID() != null) {
						wfGroupEntity.setVisualTemplate(visualTemplateRepository.getOne(group.getVisualTemplateID()));
					}
				}
				else {
					wfGroupEntity = wfGroupEntityList.get(0);
					if(wfGroupEntity.getVisualTemplate() != null) {
						if(wfGroupEntity.getVisualTemplate().getId() != group.getVisualTemplateID() && group.getVisualTemplateID() != null) {
							wfGroupEntity.setVisualTemplate(visualTemplateRepository.getOne(group.getVisualTemplateID()));
						} else if(group.getVisualTemplateID() == null) {
							wfGroupEntity.setVisualTemplate(null);
						}
					} else if (group.getVisualTemplateID() != null) {
						wfGroupEntity.setVisualTemplate(visualTemplateRepository.getOne(group.getVisualTemplateID()));
					}
				}
				workflowEntity.addWfGroup(wfGroupEntity);
			});
			/*workflowEntity.getWfGroups().stream().forEach(wfGroupEntity -> {
				List<Group> groups = workflow.getGroups().stream().filter(group -> group.getId().equalsIgnoreCase(wfGroupEntity.getReferenceGroupId())).collect(Collectors.toList());
				if(groups.size() == 0) {
					wfGroupsToRemove.add(wfGroupEntity);
				}
			});
			for (WFGroupEntity obj : wfGroupsToRemove) {
				workflowEntity.removeWfGroup(obj);
				wfGroupRepository.delete(obj.getId());
			}*/
		}
	}

	private OrganizationIdentity setOrgIdentity(WorkflowEntity workflowEntity) {
		OrganizationIdentitiesEntity orgIdentityEntity = workflowEntity.getOrgIdentityType();
		OrganizationIdentity orgIdentity = new OrganizationIdentity();
		if (orgIdentityEntity != null) {
			orgIdentity.setId(orgIdentityEntity.getId());
			orgIdentity.setIdentityTypeName(orgIdentityEntity.getIdentityType().getName());
			orgIdentity.setIdentityTypeDetails(orgIdentityEntity.getIdentityType().getNotes());
		}
		return orgIdentity;
	}

	private void createAndAssignRoleToGroup(Workflow workflow, WorkflowEntity workflowEntity, String workflowRoleName, String organizationName) throws WorkflowServiceException {
		Role role = new Role();
		role.setName(workflowRoleName);
		role.setDescription(workflowRoleName + "Desc"); 

		int statusCode = keycloakService.createClientRole(role, organizationName);
		if (statusCode == 201) {
			RoleRepresentation roleRepresentation =  keycloakService.getClientRoleByName(workflowRoleName, organizationName);
			
			// TODO Exception Handling
			if (workflow.getGroups() != null) {
				workflow.getGroups().stream().forEach(group -> {
					keycloakService.assignRoleToGroup(group.getId(), roleRepresentation, organizationName);
				});
				if(workflowEntity != null) {
					// in case of edit
					workflowEntity.getWfGroups().stream().forEach(wfGroupEntity -> {
						List<Group> groups = workflow.getGroups().stream().filter(group -> group.getId().equalsIgnoreCase(wfGroupEntity.getReferenceGroupId())).collect(Collectors.toList());
						if(groups.size() == 0) {
							keycloakService.assignRoleToGroup(wfGroupEntity.getReferenceGroupId(), roleRepresentation, organizationName);
						}
					});
				}
			}
			
		} else {
			log.info(ApplicationConstants.notifyCreate,
					Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_CREATE_AND_ASSIGN_ROLE_TO_GROUP_OF_WORKFLOW_UNDER_ORGANIZATION, workflowRoleName, workflow.getName(),organizationName));
			throw new WorkflowServiceException(Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_CREATE_AND_ASSIGN_ROLE_TO_GROUP_OF_WORKFLOW_UNDER_ORGANIZATION, workflowRoleName, workflow.getName(), organizationName));
		}
	}

	@Override
	public List<OrgIdentityFieldIdNamePair> getUserOrgIdentityFields(String organizationName, String identityTypeName)  throws WorkflowServiceException {
		List<OrgIdentityFieldIdNamePair> orgIdentityFieldIdNamePairs = new ArrayList<>();
		try {
			orgIdentityFieldRepository.findUserOrgIdentityFieldByOrganization(organizationName, identityTypeName, "USER").forEach(orgIdentityField -> {
				OrgIdentityFieldIdNamePair orgIdentityFieldIdNamePair = new OrgIdentityFieldIdNamePair();
				orgIdentityFieldIdNamePair.setOrgIdentityFieldId(orgIdentityField.getId());
				orgIdentityFieldIdNamePair.setFieldDisplayName(orgIdentityField.getIdentityField().getDisplayName());
				orgIdentityFieldIdNamePair.setMandatory(orgIdentityField.getIdentityField().isMandatory());
				orgIdentityFieldIdNamePairs.add(orgIdentityFieldIdNamePair);
			});
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_ORGANIZATION_IDENTITY_FIELDS_OF_ORGANIZATION, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_ORGANIZATION_IDENTITY_FIELDS_OF_ORGANIZATION,  organizationName);
		}
		return orgIdentityFieldIdNamePairs;
	}
	
	@Override
    public boolean isWorkflowExistsByGroupID(String organizationName, String groupID) throws WorkflowServiceException {        
           //checking whether workflow is there or not with groupId if group having workflow return false else return true
           return wfGroupRepository.isWorkflowExistsByGroupID(organizationName, groupID);
   }
	
	@Override
	public String getWorkflowNameByGroupID(String organizationName, String groupID) throws WorkflowServiceException {
		try {
			

			// Delete Workflow
			WFGroupEntity wfGroupEntity = wfGroupRepository.findWorkflowByGroupID(organizationName, groupID);
			if(wfGroupEntity != null) {
				WorkflowEntity  workflowEntity = wfGroupEntity.getWorkflow();
				Workflow workflow = new Workflow();
				
				workflow.setName(workflowEntity.getName());
				
				return workflowEntity.getName();
			} else {
				
			}
			
			if (wfGroupEntity == null) {
				log.info(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_GROUPID_UNDER_ORGANIZATION, groupID, organizationName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.WORKFLOW_NOT_FOUND_FOR_GROUPID_UNDER_ORGANIZATION, groupID, organizationName));
			}
			
			
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_ORGANIZATION_IDENTITY_FIELDS_OF_ORGANIZATION, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_ORGANIZATION_IDENTITY_FIELDS_OF_ORGANIZATION,  organizationName);
		}
		
		return null;
	}

	@Override
	public List<DeviceProfile> getWorkflowDevProfileNames(String organizationName, String wfName)
			throws WorkflowServiceException {
		List<DeviceProfileEntity> deviceProfileEntities = null;
		List<DeviceProfile> deviceProfileList = new ArrayList<>();
		try {
			deviceProfileEntities = devProfRepository.findWorkflowDevProfiles(organizationName, wfName);
			for (DeviceProfileEntity deviceProfileEntity : deviceProfileEntities) {
				DeviceProfile deviceProfile = new DeviceProfile();
				deviceProfile.setName(deviceProfileEntity.getName());
				
				ConfigValueEntity cvEntityAppletType = deviceProfileEntity.getConfigValueProductName();
				ConfigValue cvAppletType = new ConfigValue();
				cvAppletType.setId(cvEntityAppletType.getId());
				cvAppletType.setValue(cvEntityAppletType.getValue());
				deviceProfile.setConfigValueProductName(cvAppletType);
				
				deviceProfileList.add(deviceProfile);
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_DEVICEPROFILES_FOR_WORKFLOW_UNDER_ORGANIZATION, wfName,
							organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause,
					ErrorMessages.FAILED_TO_GET_DEVICEPROFILES_FOR_WORKFLOW_UNDER_ORGANIZATION, wfName,
					organizationName);
		}
		return deviceProfileList;
	}

	@Override
	public WfStepAppletLoadingInfo getAppletLoadingInfo(String organizationName, String workflowName, String requestFrom) {
		WorkflowStepDetail workflowStepDetail = new WorkflowStepDetail();
//		try {
			wfStepRepository.findStepDetailByName(organizationName, workflowName, "PERSO_AND_ISSUANCE").ifPresent(wfStepEntity -> {
						WfStepAppletLoadingInfo wfStepAppletLoadingInfo = new WfStepAppletLoadingInfo();
						if (wfStepEntity.getWfAppletLoadingInfo() != null) {

							WfStepAppletLoadingInfoEntity wfAppletLoadingInfoEntity = wfStepEntity.getWfAppletLoadingInfo();

							if(wfAppletLoadingInfoEntity.getPivAppletEnabled() != null) {
								wfStepAppletLoadingInfo.setPivAppletEnabled(wfAppletLoadingInfoEntity.getPivAppletEnabled());
							} else {
								wfStepAppletLoadingInfo.setPivAppletEnabled(false);
							}
							if(wfAppletLoadingInfoEntity.getFido2AppletEnabled() != null) {
								wfStepAppletLoadingInfo.setFido2AppletEnabled(wfAppletLoadingInfoEntity.getFido2AppletEnabled());
							} else {
								wfStepAppletLoadingInfo.setFido2AppletEnabled(false);
							}
							if (requestFrom.equalsIgnoreCase(Constants.server)) {
								wfStepAppletLoadingInfo.setAttestationCert(wfAppletLoadingInfoEntity.getAttestationCert());
								wfStepAppletLoadingInfo.setAttestationCertPrivateKey(wfAppletLoadingInfoEntity.getAttestationCertPrivateKey());
								wfStepAppletLoadingInfo.setAaguid(wfAppletLoadingInfoEntity.getAaguid());
							}
							workflowStepDetail.setWfStepAppletLoadingInfo(wfStepAppletLoadingInfo);
						}
					});
//		} catch (Exception cause) {
//			log.error(ApplicationConstants.notifyAdmin,
//					Util.format(ErrorMessages.FAILED_TO_GET_STEP_DETAILS_OF_WORKFLOW_STEP_UNDER_ORGANIZATION, "PERSO_AND_ISSUANCE", workflowName, organizationName) + " : " + cause.getMessage());
//			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_STEP_DETAILS_OF_WORKFLOW_STEP_UNDER_ORGANIZATION, "PERSO_AND_ISSUANCE", workflowName, organizationName);
//		}
		return workflowStepDetail.getWfStepAppletLoadingInfo();
	}
	
	@Override
	public List<WFStepGenericConfig> getWfStepDetails(String organizationName, String workflowName,String stepName) {
		List<WFStepGenericConfig> wfStepGenericConfigs = new ArrayList<>();
		try {
			wfStepRepository.findStepDetailByName(organizationName, workflowName,stepName).ifPresent(wfStepEntity ->{
				wfStepEntity.getWfStepConfigs().forEach(wfStepConfigEntity -> {
					WFStepGenericConfig wfStepGenericConfig = new WFStepGenericConfig();
					wfStepGenericConfig.setName(wfStepConfigEntity.getConfigValue().getConfig().getName());
					wfStepGenericConfig.setValue(wfStepConfigEntity.getConfigValue().getValue());
					wfStepGenericConfigs.add(wfStepGenericConfig);
				});	
			});
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_STEP_DETAILS_OF_WORKFLOW_STEP_UNDER_ORGANIZATION, stepName, workflowName, organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_STEP_DETAILS_OF_WORKFLOW_STEP_UNDER_ORGANIZATION, stepName, workflowName, organizationName);
		}
		return wfStepGenericConfigs;
	}
	
	private boolean isFriendlyNameExist(String name, String organizationName) {
		try {
			List<WFMobileIDIdentityIssuanceEntity> wfMobileIDStepIdentityConfigEntityList = new ArrayList<>();
			wfMobileIDStepIdentityConfigEntityList = wfMobileIDIdentityIssuanceRepository.getFriendlyName(name,organizationName);
			if (wfMobileIDStepIdentityConfigEntityList.size() == 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_CHECKING_FOR_FRIENDLY_NAME_WITH_NAME, name) + " : "+ cause.getMessage());
			throw new WorkflowServiceException("Error while checking for friendly name with " + name);
		}
	}

	private boolean isFriendlyNameExistsInUpdate(String name, String organizationName, long id) {
		try {
			List<WFMobileIDIdentityIssuanceEntity> wfMobileIDStepIdentityConfigEntityList = new ArrayList<>();
			wfMobileIDStepIdentityConfigEntityList = wfMobileIDIdentityIssuanceRepository.getFriendlyName(name, organizationName);
			List<Long> ids = new ArrayList<>();
			wfMobileIDStepIdentityConfigEntityList.stream().forEach(e -> ids.add(e.getId()));
			boolean check = ids.contains(id);
			if (wfMobileIDStepIdentityConfigEntityList.size() == 0 || check) {
				return true;
			} else {
				return false;
			}
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.ERROR_WHILE_CHECKING_FOR_FRIENDLY_NAME_WITH_NAME, name) + " : "+ cause.getMessage());
			throw new WorkflowServiceException("Error while checking for friendly name with " + name);
		}
	}
	
	private void updateDefaultWorkflowAttribute(WorkflowEntity workflowEntity, boolean defaultWorkflow, OrganizationEntity organizationEntity) {
	
		//Create Workflow Attribute Entity
		WorkflowAttributeEntity wfaEntity = new WorkflowAttributeEntity();
		if(defaultWorkflow) {
			boolean addAttribute = false;
			List<WorkflowAttributeEntity> attributes = workflowAttributeRepository.checkIfDefaultWorkflowExists(organizationEntity.getName(), WorkflowAttributeNameEnum.DEFAULT_WORKFLOW.getName());
			if(attributes == null || attributes.isEmpty()) {
				addAttribute = true;
			}else {
				for (WorkflowAttributeEntity workflowAttributeEntity : attributes) {
					if(workflowAttributeEntity.getWorkflow().getId().compareTo(workflowEntity.getId()) == 0) {
						addAttribute = false;
						break;
					}else {
						workflowAttributeRepository.delete(workflowAttributeEntity);
						log.info(ApplicationConstants.notifyCreate,
								Util.format(ErrorMessages.WORKFLOW_ATTRIBUTE_DELETED_WITH_NAME_UNDER_ORGANIZATION_AS_DEFAULT, workflowEntity.getName(), organizationEntity.getName()));	
						addAttribute = true;
					}
				}
			}
			if(addAttribute) {
				wfaEntity.setName(WorkflowAttributeNameEnum.DEFAULT_WORKFLOW.getName());
				wfaEntity.setValue(Boolean.TRUE.toString());
				wfaEntity.setWorkflow(workflowEntity);
				wfaEntity.setOrganization(organizationEntity);
				workflowAttributeRepository.save(wfaEntity);
				log.info(ApplicationConstants.notifyCreate,
						Util.format(ErrorMessages.WORKFLOW_ATTRIBUTE_CREATED_WITH_NAME_UNDER_ORGANIZATION_AS_DEFAULT, workflowEntity.getName(), organizationEntity.getName()));
			}
		}else {
			List<WorkflowAttributeEntity> attributes = workflowAttributeRepository.checkIfDefaultWorkflowExists(workflowEntity.getId(),organizationEntity.getId());
			if(attributes == null || attributes.isEmpty()) {
				
			}else {
				for (WorkflowAttributeEntity workflowAttributeEntity : attributes) {
					workflowAttributeRepository.delete(workflowAttributeEntity);
					log.info(ApplicationConstants.notifyCreate,
							Util.format(ErrorMessages.WORKFLOW_ATTRIBUTE_DELETED_WITH_NAME_UNDER_ORGANIZATION_AS_DEFAULT, workflowEntity.getName(), organizationEntity.getName()));	
				}
			}
		}
	}

	@Override
	public Workflow getDefaultWorkflow(String organizationName) {
		Workflow workflow = new Workflow();
		try {
			workflowAttributeRepository.findDefaultWorkflow(organizationName, WorkflowAttributeNameEnum.DEFAULT_WORKFLOW.getName()).ifPresent(entity -> {
				workflow.setName(entity.getName());
				workflow.setDescription(entity.getDescription());
				OrganizationIdentity orgIdentity = new OrganizationIdentity();
				orgIdentity.setIdentityTypeName(entity.getOrgIdentityType().getIdentityType().getName());
				workflow.setOrganizationIdentities(orgIdentity);

				workflow.setAllowedDevices(entity.getAllowedDevices());
				if(entity.getAllowedDevices() == null) {
					workflow.setAllowedDevicesApplicable(false);
				} else {
					workflow.setAllowedDevicesApplicable(true);
				}
				workflow.setExpirationInMonths(entity.getExpirationInMonths());
				workflow.setExpirationInDays(entity.getExpirationInDays());
				workflow.setOrganizationIdentities(new OrganizationIdentity(entity.getOrgIdentityType().getIdentityType().getId(), entity.getOrgIdentityType().getIdentityType().getName(), entity.getOrgIdentityType().getIdentityType().getNotes()));
				workflow.setDisableExpirationEnforcement(entity.getDisableExpirationEnforcement());
				workflow.setCeatedDate(entity.getCreatedDate());

				workflow.setDefaultWorkflow(true);
			});
			List<WorkflowStep> workflowfSteps = getStepsByWorkflow(organizationName, workflow.getName());

			RegistrationConfigDetails registrationConfigDetails = organizationService.getRegistrationConfigDetailsByOrgName(organizationName, workflow.getOrganizationIdentities().getId());
			
			List<WorkflowStep> workflowSteps = new ArrayList<>();
			WorkflowStep wrkflowStep = null;
			for (WorkflowStep workflowStep : workflowfSteps) {
				wrkflowStep = new WorkflowStep();
				wrkflowStep.setName(workflowStep.getName());
				switch (workflowStep.getName()) {
				case Constants.ID_PROOFING:
					// get id proof types
					wrkflowStep.setIdProofTypes(idProofTypeService.getIDProofTypes(organizationName));
					break;
				case Constants.MOBILE_ID_ONBOARDING_CONFIG:
					// get id proof types
					wrkflowStep.setWfMobileIDStepTrustedIdentities(getTrustedDocumentsForSelfServiceOnboard(organizationName, workflow.getName()));
					break;
				case Constants.REGISTRATION:
					// get reg step details
					WorkflowStepDetail wfRegStepDetails = getStepDetails(organizationName,
							workflow.getName(), workflowStep.getName());
					wrkflowStep.setName(workflowStep.getName());
					wrkflowStep.setWfStepRegistrationConfigs(userService.setRegistrationFields(
							wfRegStepDetails.getWfStepRegistrationConfigs(), registrationConfigDetails, organizationName));
					
					break;
				case Constants.FACE_CAPTURE:
					// get face step details
					WorkflowStepDetail wfFaceStepDetails = getStepDetails(organizationName,
							workflow.getName(), workflowStep.getName());
					wrkflowStep.setWfStepGenericConfigs(wfFaceStepDetails.getWfStepGenericConfigs());
					break;
				case Constants.FINGERPRINT_CAPTURE:
					// get fp step details
					WorkflowStepDetail wfFpStepDetail = getStepDetails(organizationName, workflow.getName(),
							workflowStep.getName());
					wrkflowStep.setWfStepGenericConfigs(wfFpStepDetail.getWfStepGenericConfigs());
					break;
				case Constants.APPROVAL_BEFORE_ISSUANCE:
					WorkflowStepDetail wfApprovalStepDetail = getStepDetails(organizationName, workflow.getName(),
							workflowStep.getName());
					wrkflowStep.setWfStepApprovalGroupConfigs(wfApprovalStepDetail.getWfStepApprovalGroupConfigs());
					break;
				}
				workflowSteps.add(wrkflowStep);
			}
			// get step configs and add type and value to it
			workflow.setWorkflowSteps(workflowSteps);
			
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GET_DEFAULT_WORKFLOW_UNDER_ORGANIZATION,  organizationName) + " : " + cause.getMessage());
			throw new WorkflowServiceException(cause, ErrorMessages.FAILED_TO_GET_DEFAULT_WORKFLOW_UNDER_ORGANIZATION,  organizationName);
		}

		return workflow;
	}

}

