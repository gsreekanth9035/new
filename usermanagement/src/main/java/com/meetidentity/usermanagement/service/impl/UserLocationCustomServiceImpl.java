package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserLocationEntity;
import com.meetidentity.usermanagement.service.UserLocationCustomService;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.SearchUserResult;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

@Service
@Transactional(rollbackForClassName={"Exception"})
public class UserLocationCustomServiceImpl implements UserLocationCustomService {

	private Util util = new Util();
	@PersistenceContext
    private EntityManager em;
    @Override
    @Transactional
	public List<UserLocationEntity> findByLocation(Point point) {
    	
    	//GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);
        //Point point = geometryFactory.createPoint( new Coordinate( 10, 8 ) );
		String query = "select e  from UserLocationEntity e  where WITHIN(e.location, POINT("+point.getX()+","+point.getY()+")) = true";
    	
        List<UserLocationEntity> list = em.createQuery(query, UserLocationEntity.class)   //also this works too   "where ST_WITHIN(e.location, POINT(-178,87)) = true", UserLocationEntity.class)
    		.getResultList();
        
        for (UserLocationEntity userLocationEntity : list) {
			System.out.println("X:::"+userLocationEntity.getLocation().getX());
			System.out.println("Y:::"+userLocationEntity.getLocation().getY());
			System.out.println("Y:::"+userLocationEntity.getLocation().getGeometryType());
			System.out.println("Y:::"+userLocationEntity.getLocation().toText());
		}
        
//    	List<UserLocationEntity> list = em.createQuery(
//    		    "select e " +
//    		    "from UserLocationEntity e " +
//    		    "where name = :name", UserLocationEntity.class)
//    		.setParameter("name", "name")
//    		.getResultList();
//        for (UserLocationEntity userLocationEntity : list) {
//			System.out.println("X:::"+userLocationEntity.getLocation().getX());
//			System.out.println("Y:::"+userLocationEntity.getLocation().getY());
//			System.out.println("Y:::"+userLocationEntity.getLocation().getGeometryType());
//			System.out.println("Y:::"+userLocationEntity.getLocation().toText());
//		}
		
        return list;
	}
    
    @Override
    @Transactional
	public ResultPage findByLocation(Polygon footprint, int page, int size) {
    	
    	List<SearchUserResult> searchUserResults = new ArrayList<SearchUserResult>(); 
    	
    	//GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);
        //Point point = geometryFactory.createPoint( new Coordinate( 10, 8 ) );
    	//also this works too   "where WITHIN(e.location, POINT(-178,87)) = true", UserLocationEntity.class)
		StringBuilder query = new StringBuilder(" from UserLocationEntity e  where ST_WITHIN(e.location, ST_GEOMFROMTEXT('POLYGON((");
		ResultPage resultPage = new ResultPage();
		Coordinate[] coordinates = footprint.getCoordinates();
		for (int i=0; i<coordinates.length;i++) {
			//query = query.append("(");
			query = query.append(coordinates[i].x);
			query = query.append(" ");
			query = query.append(coordinates[i].y);
			//query = query.append(")");
			if(i+1<coordinates.length) {
				query = query.append(",");
			}			
		}
		query = query.append("))')) = true");
		System.out.println(query.toString());
		
		String query_count = " SELECT COUNT(e) "+query.toString();
		String query_entities = " SELECT e "+query.toString();
    	
         
        long maxResults = ((Long) em.createQuery(query_count).getSingleResult()).intValue();
        
        List<UserLocationEntity> list = em.createQuery(query_entities, UserLocationEntity.class)   
        		.getResultList();
		for (UserLocationEntity userLocationEntity : list) {
			SearchUserResult searchUserResult = new SearchUserResult();
			UserEntity userEntity = userLocationEntity.getUser();
			searchUserResult.setUserID(userEntity.getId());
			searchUserResult.setUserName(userEntity.getReferenceName());
			searchUserResult.setFirstName(userEntity.getUserAttribute().getFirstName());
			searchUserResult.setLastName(userEntity.getUserAttribute().getLastName());
			searchUserResult.setEmail(userEntity.getUserAttribute().getEmail());
			searchUserResult.setEnabled(true);
			searchUserResult.setCanEnroll(false);
			searchUserResult.setCanDisplayEnrollDetails(false);
			searchUserResult.setCanIssue(false);
			searchUserResult.setCanLifeCycle(true);
			searchUserResult.setLocation(userLocationEntity.getLocation().getX()+","+userLocationEntity.getLocation().getY());
			searchUserResult.setDate(util.formatDate(userLocationEntity.getDate(),Util.ddmmYYYY_format_HH_mm_ss));
			searchUserResult.setStatus(userLocationEntity.getStatus());
			searchUserResults.add(searchUserResult);
		}
		 
        if (list.size()!= 0) {
			
        	if(page == 0) {
    			resultPage.setFirst(true);        		
        	}else {
    			resultPage.setFirst(false);         		
        	}
        	if(list.size()<=size) {
        		resultPage.setLast(true);
        	}else {
        		resultPage.setLast(false);
        	}
			
			resultPage.setNumber(page);
			resultPage.setNumberOfElements(list.size());
			resultPage.setSize(size);
			resultPage.setTotalElements(maxResults);
			resultPage.setTotalPages((int)Math.ceil((float)maxResults/size));
		}
		resultPage.setContent(searchUserResults);
          
        
        for (UserLocationEntity userLocationEntity : list) {
			System.out.println("X:::"+userLocationEntity.getLocation().getX());
			System.out.println("Y:::"+userLocationEntity.getLocation().getY());
			System.out.println("Y:::"+userLocationEntity.getLocation().getGeometryType());
			System.out.println("Y:::"+userLocationEntity.getLocation().toText());
		}
		return resultPage;
        
//    	List<UserLocationEntity> list = em.createQuery(
//    		    "select e " +
//    		    "from UserLocationEntity e " +
//    		    "where name = :name", UserLocationEntity.class)
//    		.setParameter("name", "name")
//    		.getResultList();
//        for (UserLocationEntity userLocationEntity : list) {
//			System.out.println("X:::"+userLocationEntity.getLocation().getX());
//			System.out.println("Y:::"+userLocationEntity.getLocation().getY());
//			System.out.println("Y:::"+userLocationEntity.getLocation().getGeometryType());
//			System.out.println("Y:::"+userLocationEntity.getLocation().toText());
//		}
		
	}

    @Override
    @Transactional
	public List<UserLocationEntity> findByLocation(List<Point> points) {
    	
    	//GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);
        //Point point = geometryFactory.createPoint( new Coordinate( 10, 8 ) );
		String query = "select e  from UserLocationEntity e  where WITHIN(e.location, POLYGON()) = true";
    	
        List<UserLocationEntity> list = em.createQuery(query, UserLocationEntity.class)   //also this works too   "where ST_WITHIN(e.location, POINT(-178,87)) = true", UserLocationEntity.class)
    		.getResultList();
        
        for (UserLocationEntity userLocationEntity : list) {
			System.out.println("X:::"+userLocationEntity.getLocation().getX());
			System.out.println("Y:::"+userLocationEntity.getLocation().getY());
			System.out.println("Y:::"+userLocationEntity.getLocation().getGeometryType());
			System.out.println("Y:::"+userLocationEntity.getLocation().toText());
		}
        
//    	List<UserLocationEntity> list = em.createQuery(
//    		    "select e " +
//    		    "from UserLocationEntity e " +
//    		    "where name = :name", UserLocationEntity.class)
//    		.setParameter("name", "name")
//    		.getResultList();
//        for (UserLocationEntity userLocationEntity : list) {
//			System.out.println("X:::"+userLocationEntity.getLocation().getX());
//			System.out.println("Y:::"+userLocationEntity.getLocation().getY());
//			System.out.println("Y:::"+userLocationEntity.getLocation().getGeometryType());
//			System.out.println("Y:::"+userLocationEntity.getLocation().toText());
//		}
		
        return list;
	}

	@Override
	public List<UserLocationEntity> findByLocation(Polygon footprint) {
		// TODO Auto-generated method stub
		return null;
	}

}
