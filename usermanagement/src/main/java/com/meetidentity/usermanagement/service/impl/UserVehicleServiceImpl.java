package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.domain.UserVehicleEntity;
import com.meetidentity.usermanagement.domain.VehicleEntity;
import com.meetidentity.usermanagement.repository.UserRepository;
import com.meetidentity.usermanagement.repository.UserVehicleRepository;
import com.meetidentity.usermanagement.repository.VehicleRepository;
import com.meetidentity.usermanagement.service.UserVehicleService;
import com.meetidentity.usermanagement.web.rest.model.VehicleRegistration;

@Transactional
@Service
public class UserVehicleServiceImpl implements UserVehicleService {

	@Autowired
	private UserVehicleRepository userVehicleRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private VehicleRepository vehicleRepository;
	
	@Override
	public void addVehicleToUser(String organizationName, Long userID, String vin) {
		UserVehicleEntity userVehicleEntity = new UserVehicleEntity();	
		userVehicleEntity.setUser(userRepository.findOne(userID));
		userVehicleEntity.setVehicle(vehicleRepository.findVehicleByVin(organizationName, vin));
		
		userVehicleRepository.save(userVehicleEntity);
	}

	@Override
	public List<VehicleRegistration> getVehiclesByUser(String organizationName, Long userID) {
		List<VehicleRegistration> vehicleRegistrations = new ArrayList<>();
		
		List<VehicleEntity> vehicleEntites = userVehicleRepository.findVehicleByUser(userID);
		vehicleEntites.stream().forEach(vehicleEntity -> {
			VehicleRegistration vehicleRegistration = new VehicleRegistration();
			vehicleRegistration.setVin(vehicleEntity.getVin());
			vehicleRegistration.setMake(vehicleEntity.getMake());
			vehicleRegistration.setYear(vehicleEntity.getYear());
			vehicleRegistration.setModel(vehicleEntity.getModel());
			vehicleRegistration.setColor(vehicleEntity.getColor());
			vehicleRegistration.setLicensePlateNumber(vehicleEntity.getLicensePlateNumber());
			vehicleRegistration.setCapacity(vehicleEntity.getCapacity());
			if (vehicleEntity.getDateOfSale() != null) {
				vehicleRegistration.setDateOfSale(Date.from(vehicleEntity.getDateOfSale()));
			//	vehicleRegistration.setDateOfSale(LocalDateTime.ofInstant(vehicleEntity.getDateOfSale(), ZoneId.systemDefault()).toLocalDate());
			}
			vehicleRegistration.setDealerName(vehicleEntity.getDealerName());
			vehicleRegistration.setDealerLicenseNumber(vehicleEntity.getDealerLicenseNumber());
			vehicleRegistration.setDealerInvoiceNumber(vehicleEntity.getDealerInvoiceNumber());
			
			if (!vehicleEntity.getVehicleOwners().isEmpty()) {
				vehicleEntity.getVehicleOwners().stream().forEach(vehicleOwner -> {
					vehicleRegistration.setOwnerName(vehicleOwner.getName());
					vehicleRegistration.setOwnerAddress(vehicleOwner.getAddress());					
				});
			}
			
			if (!vehicleEntity.getVehicleInsurances().isEmpty()) {
				vehicleEntity.getVehicleInsurances().stream().forEach(vehicleInsurance -> {
					vehicleRegistration.setPolicyNumber(vehicleInsurance.getPolicyNumber());
					vehicleRegistration.setInsuranceCompanyName(vehicleInsurance.getCompanyName());
					vehicleRegistration.setInsuranceAgentName(vehicleInsurance.getAgentName());
				});
			}
			
			vehicleRegistrations.add(vehicleRegistration);
		});
		
		return vehicleRegistrations;
	}

}
