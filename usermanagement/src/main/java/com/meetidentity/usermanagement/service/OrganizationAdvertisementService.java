package com.meetidentity.usermanagement.service;

import java.util.List;

import com.meetidentity.usermanagement.web.rest.model.OrganizationAdvertisementInfo;

public interface OrganizationAdvertisementService {
	
	public List<OrganizationAdvertisementInfo> getOrganizationAdvertisements(String organizationName);
}
