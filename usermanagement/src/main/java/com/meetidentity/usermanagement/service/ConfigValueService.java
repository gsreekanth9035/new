package com.meetidentity.usermanagement.service;

import java.util.List;

import com.meetidentity.usermanagement.web.rest.model.ConfigValue;

public interface ConfigValueService {

	List<ConfigValue> getConfigValues(String organizationName, String configName, Long parentConfigValueId);

}
