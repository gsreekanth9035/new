package com.meetidentity.usermanagement.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.amazonaws.services.sns.model.PublishResult;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.exception.PushNotificationServiceException;
import com.meetidentity.usermanagement.web.rest.model.PushNotificationDevice;
import com.meetidentity.usermanagement.web.rest.model.PushNotificationPayLoad;

public interface PushNotificationDeviceService {

	List<PushNotificationDevice> findAllDevicesByUserName(String userName) throws PushNotificationServiceException;

	ResponseEntity<Void> addPushNotificationDevice(List<PushNotificationDevice> pushNotificationDevices)
			throws PushNotificationServiceException;

	PublishResult sendUserNotification(PushNotificationDevice pushNotificationDevice)
			throws PushNotificationServiceException;

	ResponseEntity<Void> sendSMS(String mobileNumber, UserEntity userEntity) throws PushNotificationServiceException;

	PushNotificationPayLoad getPushNotificationPayLoadData(PushNotificationDevice pushNotificationDevice)
			throws PushNotificationServiceException;
}
