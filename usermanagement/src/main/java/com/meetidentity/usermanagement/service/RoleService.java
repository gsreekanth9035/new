package com.meetidentity.usermanagement.service;

import java.util.List;

import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.RoleServiceException;
import com.meetidentity.usermanagement.web.rest.model.Role;

public interface RoleService {
	
	void createRole(String organizationName, Role role) throws IdentityBrokerServiceException;
	
	List<Role> getRoles(String organizationName) throws RoleServiceException;
	
	Role getRoleByName(String organizationName, String roleName) throws RoleServiceException;

	List<String> getRoleNames(String organizationName);

	List<Role> getRoleWithColor(List<String> listOfRoles, String organization);
}
