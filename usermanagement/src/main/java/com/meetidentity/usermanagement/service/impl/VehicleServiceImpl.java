package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.domain.VehicleClassificationEntity;
import com.meetidentity.usermanagement.domain.VehicleEntity;
import com.meetidentity.usermanagement.domain.VehicleInsuranceEntity;
import com.meetidentity.usermanagement.domain.VehicleOwnerEntity;
import com.meetidentity.usermanagement.enums.DirectionEnum;
import com.meetidentity.usermanagement.repository.OrganizationRepository;
import com.meetidentity.usermanagement.repository.VehicleClassificationRepository;
import com.meetidentity.usermanagement.repository.VehicleRepository;
import com.meetidentity.usermanagement.service.VehicleService;
import com.meetidentity.usermanagement.web.rest.model.IDProofIssuingAuthority;
import com.meetidentity.usermanagement.web.rest.model.IDProofType;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.VehicleClassification;
import com.meetidentity.usermanagement.web.rest.model.VehicleRegistration;
import com.meetidentity.usermanagement.web.rest.model.VehicleRegistrations;

@Service
@Transactional
public class VehicleServiceImpl implements VehicleService {
	
	private static final Logger log = LoggerFactory.getLogger(VehicleServiceImpl.class);
	
	@Autowired
	private OrganizationRepository organizationRepository;

	@Autowired
	private VehicleRepository vehicleRepository;
	
	@Autowired
	private VehicleClassificationRepository vehicleClassificationRepository;


	@Override
	public VehicleRegistration getVehicleRegistrationDetails(String organizationName, String licensePlateNumber) {
		VehicleRegistration vehicleRegistration = new VehicleRegistration();
		VehicleEntity vehicleEntity = vehicleRepository.findVehicleByLicensePlateNumber(organizationName, licensePlateNumber);
		if (vehicleEntity != null) {
			vehicleRegistration.setVin(vehicleEntity.getVin());
			vehicleRegistration.setMake(vehicleEntity.getMake());
			vehicleRegistration.setYear(vehicleEntity.getYear());
			vehicleRegistration.setModel(vehicleEntity.getModel());
			vehicleRegistration.setColor(vehicleEntity.getColor());
			vehicleRegistration.setLicensePlateNumber(vehicleEntity.getLicensePlateNumber());
			vehicleRegistration.setCapacity(vehicleEntity.getCapacity());
			if (vehicleEntity.getDateOfSale() != null) {
				// vehicleRegistration.setDateOfSale(LocalDateTime.ofInstant(vehicleEntity.getDateOfSale(), ZoneId.systemDefault()).toLocalDate());
				vehicleRegistration.setDateOfSale(Date.from(vehicleEntity.getDateOfSale()));
			}
			vehicleRegistration.setDealerName(vehicleEntity.getDealerName());
			vehicleRegistration.setDealerLicenseNumber(vehicleEntity.getDealerLicenseNumber());
			vehicleRegistration.setDealerInvoiceNumber(vehicleEntity.getDealerInvoiceNumber());
			
			if (!vehicleEntity.getVehicleOwners().isEmpty()) {
				vehicleEntity.getVehicleOwners().stream().forEach(vehicleOwner -> {
					vehicleRegistration.setOwnerName(vehicleOwner.getName());
					vehicleRegistration.setOwnerAddress(vehicleOwner.getAddress());					
				});
			}
			
			if (!vehicleEntity.getVehicleInsurances().isEmpty()) {
				vehicleEntity.getVehicleInsurances().stream().forEach(vehicleInsurance -> {
					vehicleRegistration.setPolicyNumber(vehicleInsurance.getPolicyNumber());
					vehicleRegistration.setInsuranceCompanyName(vehicleInsurance.getCompanyName());
					vehicleRegistration.setInsuranceAgentName(vehicleInsurance.getAgentName());
				});
			}
			
		}
		return vehicleRegistration;
	}

	@Override
	public ResponseEntity<Void> createVehicleRegistrations(String organizationName, VehicleRegistrations vehicleRegistrations) throws Exception {
		if (vehicleRegistrations != null) {
			for (VehicleRegistration vehicleRegistration : vehicleRegistrations.getVehicleRegistrations()) {
					VehicleEntity vehicleEntityFromLPN = vehicleRepository.findVehicleByLicensePlateNumber(
							organizationName, vehicleRegistration.getLicensePlateNumber());
					VehicleEntity vehicleEntityByVIN = vehicleRepository.findVehicleByVin(organizationName,
							vehicleRegistration.getVin());
					VehicleEntity vehicleEntityByPolicyNumber = vehicleRepository
							.findVehicleByPolicyNumber(organizationName, vehicleRegistration.getPolicyNumber());
					if (vehicleEntityFromLPN != null) {
						if (vehicleEntityFromLPN.getLicensePlateNumber().equalsIgnoreCase(vehicleRegistration.getLicensePlateNumber())) {
							log.debug("Vehicle with Licence Plate Number" + " "+ vehicleRegistration.getLicensePlateNumber() + " " + "already exists");
							throw new Exception("Vehicle with Licence Plate Number" + " "+ vehicleRegistration.getLicensePlateNumber() + " " + "already exists");
						}
					}
					if (vehicleEntityByVIN != null) {
						if (vehicleEntityByVIN.getVin().equalsIgnoreCase(vehicleRegistration.getVin())) {
							log.debug("Vehicle with VIN" + " " + vehicleRegistration.getVin() + " " + "already exists");
							throw new Exception("Vehicle with VIN" + " " + vehicleRegistration.getVin() + " " + "already exists");
						}
					}
					if (vehicleEntityByPolicyNumber != null) {
						if (vehicleEntityByPolicyNumber.getVehicleInsurances().get(0).getPolicyNumber().equalsIgnoreCase(vehicleRegistration.getPolicyNumber())) {
							log.debug("Vehicle with Policy Number" + " " + vehicleRegistration.getPolicyNumber() + " " + "already exists");
							throw new Exception("Vehicle with Policy Number" + " " +vehicleRegistration.getPolicyNumber() + " " + "already exists");
						}
					}

					VehicleEntity vehicleEntity = new VehicleEntity();
					vehicleEntity.setOrganization(organizationRepository.findByName(organizationName));
					vehicleEntity.setVin(vehicleRegistration.getVin());
					vehicleEntity.setMake(vehicleRegistration.getMake());
					vehicleEntity.setYear(vehicleRegistration.getYear());
					vehicleEntity.setModel(vehicleRegistration.getModel());
					vehicleEntity.setColor(vehicleRegistration.getColor());
					vehicleEntity.setLicensePlateNumber(vehicleRegistration.getLicensePlateNumber());
					vehicleEntity.setCapacity(vehicleRegistration.getCapacity());
					vehicleEntity.setDateOfSale(vehicleRegistration.getDateOfSale().toInstant());
					// vehicleEntity.setDateOfSale(vehicleRegistration.getDateOfSale().atStartOfDay(ZoneId.systemDefault()).toInstant());
					vehicleEntity.setDealerName(vehicleRegistration.getDealerName());
					vehicleEntity.setDealerLicenseNumber(vehicleRegistration.getDealerLicenseNumber());
					vehicleEntity.setDealerInvoiceNumber(vehicleRegistration.getDealerInvoiceNumber());

					VehicleOwnerEntity vehicleOwnerEntity = new VehicleOwnerEntity();
					vehicleOwnerEntity.setName(vehicleRegistration.getOwnerName());
					vehicleOwnerEntity.setAddress(vehicleRegistration.getOwnerAddress());

					VehicleInsuranceEntity vehicleInsuranceEntity = new VehicleInsuranceEntity();
					vehicleInsuranceEntity.setPolicyNumber(vehicleRegistration.getPolicyNumber());
					vehicleInsuranceEntity.setCompanyName(vehicleRegistration.getInsuranceCompanyName());
					vehicleInsuranceEntity.setAgentName(vehicleRegistration.getInsuranceAgentName());

					vehicleEntity.addVehicleOwner(vehicleOwnerEntity);
					vehicleEntity.addVehicleInsurance(vehicleInsuranceEntity);

					vehicleRepository.save(vehicleEntity);
			}
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		} else {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}

	@Override
	public ResultPage getAllVehicleRegs(String organizationName, String sortBy, String order, int page, int size) {
		List<VehicleRegistration> vehicleRegistrations = new ArrayList<>();
		ResultPage resultPage = null;
		Sort sort = null;
		if (!sortBy.equalsIgnoreCase("undefined") && !(order.isEmpty())) {
			if(sortBy.equalsIgnoreCase("owner")) {
				sortBy = "vehicleOwners.name";
			}
			if (order.equals(DirectionEnum.ASC.getDirectionCode())) {
				sort = new Sort(new Sort.Order(Direction.ASC, sortBy));
			}
			if (order.equals(DirectionEnum.DESC.getDirectionCode())) {
				sort = new Sort(new Sort.Order(Direction.DESC, sortBy));
			}
		}
		Pageable pageable = new PageRequest(page, size, sort);
		Page<VehicleEntity> pagedResult = vehicleRepository.findAll(pageable);
		if (pagedResult.getTotalElements() != 0) {
			pagedResult.forEach(vehicleEntity -> {
				VehicleRegistration vehicleRegistration = new VehicleRegistration();
				vehicleRegistration.setVin(vehicleEntity.getVin());
				vehicleRegistration.setMake(vehicleEntity.getMake());
				vehicleRegistration.setYear(vehicleEntity.getYear());
				vehicleRegistration.setModel(vehicleEntity.getModel());
				vehicleRegistration.setLicensePlateNumber(vehicleEntity.getLicensePlateNumber());
				vehicleRegistration.setCapacity(vehicleEntity.getCapacity());
				if (!vehicleEntity.getVehicleOwners().isEmpty()) {
					vehicleEntity.getVehicleOwners().stream().forEach(vehicleOwner -> {
						vehicleRegistration.setOwnerName(vehicleOwner.getName());
					});
				}
				if (!vehicleEntity.getVehicleInsurances().isEmpty()) {
					vehicleEntity.getVehicleInsurances().stream().forEach(vehicleInsurance -> {
						vehicleRegistration.setPolicyNumber(vehicleInsurance.getPolicyNumber());
					});
				}
				vehicleRegistrations.add(vehicleRegistration);
			});
			resultPage = new ResultPage();
			resultPage.setContent(vehicleRegistrations);
			resultPage.setFirst(pagedResult.isFirst());
			resultPage.setLast(pagedResult.isLast());
			resultPage.setNumber(pagedResult.getNumber());
			resultPage.setNumberOfElements(pagedResult.getNumberOfElements());
			resultPage.setSize(pagedResult.getSize());
			resultPage.setSort(pagedResult.getSort());
			resultPage.setTotalElements(pagedResult.getTotalElements());
			resultPage.setTotalPages(pagedResult.getTotalPages());
		}
		return resultPage;
	}

	@Override
	public ResponseEntity<String> updateVehicleRegistration(String organizationName, String licencePlateNumber,
			VehicleRegistration vehicleRegistration) throws Exception {
		if (vehicleRegistration != null) {
			VehicleEntity vehicleEntity = vehicleRepository.findVehicleByLicensePlateNumber(organizationName, licencePlateNumber);
			if (vehicleEntity != null) {
				String lpn = vehicleRegistration.getLicensePlateNumber();
				String vin = vehicleRegistration.getVin();
			    String policyNumber = vehicleRegistration.getPolicyNumber();
				VehicleEntity vehicleEntityFromLPN = vehicleRepository.findVehicleByLicensePlateNumber(organizationName, lpn);
				VehicleEntity vehicleEntityByVIN = vehicleRepository.findVehicleByVin(organizationName, vin);
			    VehicleEntity vehicleEntityByPolicyNumber = vehicleRepository.findVehicleByPolicyNumber(organizationName, policyNumber);
				if (vehicleEntityFromLPN != null) {
					if (vehicleEntity.getId() != vehicleEntityFromLPN.getId()) {
						log.debug("Vehicle with Licence Plate Number" + " " + lpn + " " + "already exists");
						throw new Exception("Vehicle with Licence Plate Number" + " " + lpn + " " + "already exists");
					}
				}
				if (vehicleEntityByVIN != null) {
					if (vehicleEntity.getId() != vehicleEntityByVIN.getId()) {
						log.debug("Vehicle with VIN" + " " + vin + " " + "already exists");
						throw new Exception("Vehicle with VIN" + " " + vin + " " + "already exists");
					}
				} 
				if(vehicleEntityByPolicyNumber != null) {
					if (vehicleEntity.getId() != vehicleEntityByPolicyNumber.getId()) {
						log.debug("Vehicle with Policy Number" + " " + policyNumber + " " + "already exists");
						throw new Exception("Vehicle with Policy Number" + " " + policyNumber + " " + "already exists");
					}
				}

				vehicleEntity.setOrganization(organizationRepository.findByName(organizationName));
				vehicleEntity.setVin(vehicleRegistration.getVin());
				vehicleEntity.setMake(vehicleRegistration.getMake());
				vehicleEntity.setYear(vehicleRegistration.getYear());
				vehicleEntity.setModel(vehicleRegistration.getModel());
				vehicleEntity.setColor(vehicleRegistration.getColor());
				vehicleEntity.setLicensePlateNumber(vehicleRegistration.getLicensePlateNumber());
				vehicleEntity.setCapacity(vehicleRegistration.getCapacity());
				vehicleEntity.setDateOfSale(vehicleRegistration.getDateOfSale().toInstant());
				// vehicleEntity.setDateOfSale(vehicleRegistration.getDateOfSale().atStartOfDay(ZoneId.systemDefault()).toInstant());
				vehicleEntity.setDealerName(vehicleRegistration.getDealerName());
				vehicleEntity.setDealerLicenseNumber(vehicleRegistration.getDealerLicenseNumber());
				vehicleEntity.setDealerInvoiceNumber(vehicleRegistration.getDealerInvoiceNumber());

				VehicleOwnerEntity vehicleOwnerEntity = vehicleEntity.getVehicleOwners().get(0);
				vehicleOwnerEntity.setName(vehicleRegistration.getOwnerName());
				vehicleOwnerEntity.setAddress(vehicleRegistration.getOwnerAddress());

				VehicleInsuranceEntity vehicleInsuranceEntity = vehicleEntity.getVehicleInsurances().get(0);
				vehicleInsuranceEntity.setPolicyNumber(vehicleRegistration.getPolicyNumber());
				vehicleInsuranceEntity.setCompanyName(vehicleRegistration.getInsuranceCompanyName());
				vehicleInsuranceEntity.setAgentName(vehicleRegistration.getInsuranceAgentName());

				vehicleEntity.addVehicleOwner(vehicleOwnerEntity);
				vehicleEntity.addVehicleInsurance(vehicleInsuranceEntity);

				vehicleRepository.save(vehicleEntity);
				return new ResponseEntity<>("", HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>("", HttpStatus.CONFLICT);
			}
		} else {
			return new ResponseEntity<>("", HttpStatus.CONFLICT);
		}
	}

	@Override
	public ResponseEntity<String> deleteVehicleRegistration(String organizationName, String vin) {
		VehicleEntity vehicleEntityByVIN = vehicleRepository.findVehicleByVin(organizationName, vin);
		if(vehicleEntityByVIN != null) {
			vehicleRepository.delete(vehicleEntityByVIN);
			return new ResponseEntity<>("", HttpStatus.OK);
		}
		return null;
	}

	@Override
	public List<VehicleClassification> getVehicleClassifications(String organizationName) {
		List<VehicleClassification> vehicleClassificationsList = new ArrayList<>();
		List<VehicleClassificationEntity> vehicleClassificationEntities = vehicleClassificationRepository.findByOrgName(organizationName);
		if(vehicleClassificationEntities != null) {
			vehicleClassificationEntities.forEach(vehicleClassificationEntity -> {
				VehicleClassification vehicleClassification = new VehicleClassification();
				vehicleClassification.setId(vehicleClassificationEntity.getId());
				vehicleClassification.setType(vehicleClassificationEntity.getType());
				vehicleClassification.setDescription(vehicleClassificationEntity.getDescription());
				vehicleClassificationsList.add(vehicleClassification);
			});
		}
		return vehicleClassificationsList;
	}

}
