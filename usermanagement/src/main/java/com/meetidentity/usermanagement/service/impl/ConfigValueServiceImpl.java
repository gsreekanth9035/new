package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.domain.ConfigValueEntity;
import com.meetidentity.usermanagement.repository.ConfigValueRepository;
import com.meetidentity.usermanagement.service.ConfigValueService;
import com.meetidentity.usermanagement.web.rest.model.ConfigValue;

@Service
public class ConfigValueServiceImpl implements ConfigValueService {

	@Autowired
	private ConfigValueRepository configValueRepository;

	@Override
	public List<ConfigValue> getConfigValues(String organizationName, String configName, Long parentConfigValueId) {
		List<ConfigValue> configValueList = new ArrayList<>();
		List<ConfigValueEntity> configValueEntityList = new ArrayList<>();

		if (parentConfigValueId == null) {
			configValueEntityList = configValueRepository.findByConfigName(organizationName, configName);
		} else {
			configValueEntityList = configValueRepository.findByParentConfigValue(organizationName, parentConfigValueId);
		}
		if (configValueEntityList != null) {
			configValueEntityList.forEach(configValueEntity -> {
				ConfigValue configValue = new ConfigValue();
				configValue.setId(configValueEntity.getId());
				configValue.setValue(configValueEntity.getValue());
				configValueList.add(configValue);
			});
		}
		return configValueList;
	}
}
