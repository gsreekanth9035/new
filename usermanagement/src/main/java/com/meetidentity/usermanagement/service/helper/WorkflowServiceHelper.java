package com.meetidentity.usermanagement.service.helper;

import java.util.Comparator;

import org.springframework.stereotype.Component;

import com.meetidentity.usermanagement.domain.WFMobileIdStepCertificatesEntity;
import com.meetidentity.usermanagement.domain.WFStepCertConfigEntity;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIdStepCertificate;
import com.meetidentity.usermanagement.web.rest.model.WFStepCertConfig;

@Component
public class WorkflowServiceHelper {

	public boolean compareWfCertObjects(WFStepCertConfig wfStepCertConfig, WFStepCertConfigEntity wFStepCertConfigEntity) {
		WFStepCertConfigEntity wfStepCertConfigEntity1 = new WFStepCertConfigEntity();

		wfStepCertConfigEntity1.setCertType(wfStepCertConfig.getCertType());
		wfStepCertConfigEntity1.setCaServer(wfStepCertConfig.getCaServer());
		wfStepCertConfigEntity1.setCertTemplate(wfStepCertConfig.getCertTemplate());
		wfStepCertConfigEntity1.setKeyEscrow(wfStepCertConfig.isKeyEscrow());
		wfStepCertConfigEntity1.setDisableRevoke(wfStepCertConfig.isDisableRevoke());
		wfStepCertConfigEntity1.setAlgorithm(wfStepCertConfig.getAlgorithm());
		wfStepCertConfigEntity1.setKeySize(wfStepCertConfig.getKeysize());
		wfStepCertConfigEntity1.setSubjectDN(wfStepCertConfig.getSubjectDN());
		wfStepCertConfigEntity1.setSubjectAN(wfStepCertConfig.getSubjectAN());

		int returnValue = Comparator.comparing(WFStepCertConfigEntity::getCertType)
				.thenComparing(WFStepCertConfigEntity::getCaServer)
				.thenComparing(WFStepCertConfigEntity::getCertTemplate)
				.thenComparing(WFStepCertConfigEntity::getKeyEscrow)
				.thenComparing(WFStepCertConfigEntity::getDisableRevoke)
				.thenComparing(WFStepCertConfigEntity::getAlgorithm).thenComparing(WFStepCertConfigEntity::getKeySize)
				.thenComparing(WFStepCertConfigEntity::getSubjectDN).thenComparing(WFStepCertConfigEntity::getSubjectAN)
				.compare(wFStepCertConfigEntity, wfStepCertConfigEntity1);
		if (returnValue != 0) {
			return false;
		}
		return true;
	}
	
	public boolean compareWfMobleIdStepCertObjects(WFMobileIdStepCertificate wfMobileIdStepCertificate, WFMobileIdStepCertificatesEntity wfMobileIdStepCertEntity) {
		WFMobileIdStepCertificatesEntity wfMobileIdStepCertificatesEntity1 = new WFMobileIdStepCertificatesEntity();

		wfMobileIdStepCertificatesEntity1.setCertType(wfMobileIdStepCertificate.getCertType());
		wfMobileIdStepCertificatesEntity1.setCaServer(wfMobileIdStepCertificate.getCaServer());
		wfMobileIdStepCertificatesEntity1.setCertTemplate(wfMobileIdStepCertificate.getCertTemplate());
		wfMobileIdStepCertificatesEntity1.setKeyEscrow(wfMobileIdStepCertificate.isKeyEscrow());
		wfMobileIdStepCertificatesEntity1.setDisableRevoke(wfMobileIdStepCertificate.isDisableRevoke());
		wfMobileIdStepCertificatesEntity1.setAlgorithm(wfMobileIdStepCertificate.getAlgorithm());
		wfMobileIdStepCertificatesEntity1.setKeySize(wfMobileIdStepCertificate.getKeysize());
		wfMobileIdStepCertificatesEntity1.setSubjectDN(wfMobileIdStepCertificate.getSubjectDN());
		wfMobileIdStepCertificatesEntity1.setSubjectAN(wfMobileIdStepCertificate.getSubjectAN());

		int returnValue = Comparator.comparing(WFMobileIdStepCertificatesEntity::getCaServer)
				.thenComparing(WFMobileIdStepCertificatesEntity::getCertType)
				.thenComparing(WFMobileIdStepCertificatesEntity::getCertTemplate)
				.thenComparing(WFMobileIdStepCertificatesEntity::getKeyEscrow)
				.thenComparing(WFMobileIdStepCertificatesEntity::getDisableRevoke)
				.thenComparing(WFMobileIdStepCertificatesEntity::getAlgorithm).thenComparing(WFMobileIdStepCertificatesEntity::getKeySize)
				.thenComparing(WFMobileIdStepCertificatesEntity::getSubjectDN).thenComparing(WFMobileIdStepCertificatesEntity::getSubjectAN)
				.compare(wfMobileIdStepCertEntity, wfMobileIdStepCertificatesEntity1);
		if (returnValue != 0) {
			return false;
		}
		return true;
	}
}
