package com.meetidentity.usermanagement.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.meetidentity.usermanagement.web.rest.model.FingerprintDeviceConfig;

public interface FingerprintDeviceConfigService  {

	ResponseEntity<FingerprintDeviceConfig> getDetailByDeviceName(String organizationName, String deviceName,String deviceManufacturer);
	
	List<FingerprintDeviceConfig> getListOfDeviceByDeviceManufacturer(String organizationName,String deviceManufacturer);

}
