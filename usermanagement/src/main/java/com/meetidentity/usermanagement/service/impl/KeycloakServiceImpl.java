package com.meetidentity.usermanagement.service.impl;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLContext;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpStatus;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.config.ApplicationProperties;
import com.meetidentity.usermanagement.config.ApplicationProperties.KeycloakConfiguration;
import com.meetidentity.usermanagement.config.Constants;
import com.meetidentity.usermanagement.domain.APIGatewayInfoEntity;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.PairMobileDeviceConfigEntity;
import com.meetidentity.usermanagement.enums.RoleTypeEnum;
import com.meetidentity.usermanagement.exception.EmailServiceException;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.exception.security.CryptoOperationException;
import com.meetidentity.usermanagement.keycloak.domain.ClientMappings;
import com.meetidentity.usermanagement.keycloak.domain.ClientRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.CredentialRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.GroupRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.KeycloakUserAttributes;
import com.meetidentity.usermanagement.keycloak.domain.Mappings;
import com.meetidentity.usermanagement.keycloak.domain.MappingsRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.RoleRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.UserRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.UserSessionRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.WebApp;
import com.meetidentity.usermanagement.keycloak.domain.WebAuthnAuthenticatorRegistration;
import com.meetidentity.usermanagement.keycloak.domain.WebAuthnPolicyDetails;
import com.meetidentity.usermanagement.repository.OrganizationRepository;
import com.meetidentity.usermanagement.repository.PairMobileDeviceConfigRepository;
import com.meetidentity.usermanagement.repository.WFGroupRepository;
import com.meetidentity.usermanagement.repository.WFMobileIdStepVisualIDGroupConfigRepository;
import com.meetidentity.usermanagement.repository.WFStepAdjudicationGroupConfigRepository;
import com.meetidentity.usermanagement.repository.WFStepApprovalGroupConfigRepository;
import com.meetidentity.usermanagement.repository.WfStepChipEncodeAndVIDPrintGroupConfigRepository;
import com.meetidentity.usermanagement.repository.WfStepChipEncodeGroupConfigRepository;
import com.meetidentity.usermanagement.repository.WfStepPrintGroupConfigRepository;
import com.meetidentity.usermanagement.security.CryptoFunction;
import com.meetidentity.usermanagement.service.KeycloakService;
import com.meetidentity.usermanagement.service.WorkflowService;
import com.meetidentity.usermanagement.service.helper.KeycloakServiceHelper;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.AuthResponse;
import com.meetidentity.usermanagement.web.rest.model.AuthenticatorClientInfo;
import com.meetidentity.usermanagement.web.rest.model.EnableSoftOTP2FA;
import com.meetidentity.usermanagement.web.rest.model.Group;
import com.meetidentity.usermanagement.web.rest.model.IDPUserCredentialConfigResponse;
import com.meetidentity.usermanagement.web.rest.model.IDPUserCredentials;
import com.meetidentity.usermanagement.web.rest.model.IdentityProviderUser;
import com.meetidentity.usermanagement.web.rest.model.OpenIDConfiguration;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.Role;
import com.meetidentity.usermanagement.web.rest.model.TokenDetails;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAddress;
import com.meetidentity.usermanagement.web.rest.model.UserAppearance;
import com.meetidentity.usermanagement.web.rest.model.UserAttribute;
import com.meetidentity.usermanagement.web.rest.model.UserBiometric;
import com.meetidentity.usermanagement.web.rest.model.UserEmployeeID;
import com.meetidentity.usermanagement.web.rest.model.UserNationalID;
import com.meetidentity.usermanagement.web.rest.model.UserPIVModel;
import com.meetidentity.usermanagement.web.rest.model.UserPermanentResidentID;
import com.meetidentity.usermanagement.web.rest.model.UserRolesAndGroups;
import com.meetidentity.usermanagement.web.rest.model.UserVoterID;
import com.meetidentity.usermanagement.web.rest.model.WebauthnRegisterationConfig;
import com.webauthn4j.util.Base64UrlUtil;

import io.jsonwebtoken.impl.Base64UrlCodec;

@Service
@Transactional
public class KeycloakServiceImpl implements KeycloakService {

	private final Logger log = LoggerFactory.getLogger(KeycloakServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ApplicationProperties applicationProperties;
	
	@Autowired
	private CryptoFunction cryptoFunction;
	
	@Autowired
	private PairMobileDeviceConfigRepository pairMobileDeviceConfigRepository;

	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired 
	private WorkflowService workflowService;
	
	@Autowired
	private WFGroupRepository wfGroupRepository;
	
	@Autowired
	private WFMobileIdStepVisualIDGroupConfigRepository wfMobileIdStepVisualIDGroupConfigRepository;
	
    @Autowired
	private WFStepAdjudicationGroupConfigRepository wfStepAdjudicationGroupConfigRepository;

    @Autowired
	private WFStepApprovalGroupConfigRepository wfStepApprovalGroupConfigRepository;

    @Autowired
	private WfStepChipEncodeAndVIDPrintGroupConfigRepository wfStepChipEncodeAndVIDPrintGroupConfigRepository;

    @Autowired
	private WfStepChipEncodeGroupConfigRepository wfStepChipEncodeGroupConfigRepository;
    
    @Autowired
	private WfStepPrintGroupConfigRepository wfStepPrintGroupConfigRepository;
    
	@Override
	public ResponseEntity<UserRepresentation> createUser(IdentityProviderUser identityProviderUser, String organizationName,
			boolean configureOTP) throws IdentityBrokerServiceException {
		UserRepresentation userRepresentation = null;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			userRepresentation = createUserRepresentation(identityProviderUser);
			if (configureOTP) {
				List<String> requiredActions = new ArrayList<String>();
				requiredActions.add("CONFIGURE_TOTP");
				userRepresentation.setRequiredActions(requiredActions);
			}
			HttpEntity<UserRepresentation> requestEntity = new HttpEntity<>(userRepresentation, httpHeaders);
			int statusCode = 201;
			ResponseEntity<String> response = null;

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			response = restTemplate.postForEntity(getKeycloakUserUri(keycloakConfiguration), requestEntity,
					String.class);
			statusCode = response.getStatusCodeValue();

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (statusCode != HttpStatus.SC_CREATED) {
				try {
					HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);
					ResponseEntity<List<UserRepresentation>> userresponse = restTemplate.exchange(
							getKeycloakUserUri(keycloakConfiguration) + "?username=" + userRepresentation.getUsername(),
							HttpMethod.GET, headerEntity, new ParameterizedTypeReference<List<UserRepresentation>>() {
							});
					if (userresponse.getStatusCodeValue() == 200) {
						List<UserRepresentation> userRepresentations = userresponse.getBody();
						userRepresentation = userRepresentations.get(0);
						HttpHeaders httpHeaders2 = new HttpHeaders();
						httpHeaders2.setContentType(MediaType.APPLICATION_JSON);
						tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate,
								applicationProperties, organizationName);
						httpHeaders.set(Constants.HEADER_AUTHORIZATION,
								Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);
						headerEntity = new HttpEntity<String>(httpHeaders2);
						restTemplate.exchange(
								getKeycloakDeleteUserUri(userRepresentation.getId(), keycloakConfiguration),
								HttpMethod.DELETE, headerEntity, String.class);

						logoutRefreshAccessToken(organizationName, tokens[1]);
					}
					statusCode = response.getStatusCodeValue();
					log.info(ApplicationConstants.notifyDelete, Util.format(
							ErrorMessages.IDENTITYBROKER_USER_ACCOUNT_DELETED, identityProviderUser.getUserName()));

				} catch (NullPointerException | RestClientException e) {
					statusCode = ((HttpClientErrorException) e).getRawStatusCode();
					log.error(ApplicationConstants.notifyAdmin, Util.format(
							ErrorMessages.IDENTITYBROKER_USER_ACCOUNT_NOT_DELETED, identityProviderUser.getUserName()));

				}
				// return HttpStatus.SC_SERVICE_UNAVAILABLE;
				return new ResponseEntity<UserRepresentation>(userRepresentation, org.springframework.http.HttpStatus.CREATED);
			}

		} catch (NullPointerException | RestClientException | EmailServiceException cause) {
			if (!(cause instanceof NullPointerException)) {
				if (cause.getMessage().contains("" + 409)) {
				/*	log.error(ApplicationConstants.notifyAdmin,
							Util.format(ErrorMessages.IDENTITYBROKER_USER_DETAILS_CONFLICT,
									identityProviderUser.getUserName()),
							cause);*/
					// return ((HttpClientErrorException) cause).getStatusCode().value();
					userRepresentation = null;
					return new ResponseEntity<UserRepresentation>(userRepresentation, org.springframework.http.HttpStatus.valueOf(((HttpClientErrorException) cause).getStatusCode().value()));
					// throw new
					// IdentityBrokerServiceException(Util.format(ErrorMessages.IDENTITYBROKER_USER_DETAILS_CONFLICT,
					// identityProviderUser.getUserName()) , cause);
				}
			} else {
				UserRepresentation user = getUserByUsername(identityProviderUser.getUserName(),
						organizationName);
				deleteUserByID(user.getId(), organizationName);
				log.error(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_CREATEUSER_FOR_USER,
								identityProviderUser.getUserName()) + " : "
								+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
						cause);
				throw new IdentityBrokerServiceException(Util.format(
						ErrorMessages.IDENTITYBROKER_FAILED_TO_CREATEUSER_FOR_USER, identityProviderUser.getUserName())
						+ " : " + ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			}
		}
		// return 0;
		return new ResponseEntity<UserRepresentation>(userRepresentation, org.springframework.http.HttpStatus.CREATED);
	}

	@Override
	public int updateUser(String userKLKID, JSONObject userJsonObject, String organizationName, String userName)
			throws IdentityBrokerServiceException {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> request = new HttpEntity<String>(userJsonObject.toString(), httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<String> response = restTemplate.exchange(
					getKeycloakUpdateUserUri(userKLKID, keycloakConfiguration), HttpMethod.PUT, request, String.class);

			logoutRefreshAccessToken(organizationName, tokens[1]);
			return response.getStatusCodeValue();
		} catch (NullPointerException | RestClientException | EmailServiceException cause) {
			if (cause.getMessage().contains("" + 409)) {
				log.error(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.IDENTITYBROKER_USER_DETAILS_CONFLICT, userName), cause);
				return ((HttpClientErrorException) cause).getStatusCode().value();
			} else {
				log.error(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_CREATEUSER_FOR_USER, userName) + " : "
								+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
						cause);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_CREATEUSER_FOR_USER, userName) + " : "
								+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
						cause);
			}
		}
	}
	
	@Override
	public int updateUser(UserRepresentation userRepresentation, String organizationName)
			throws IdentityBrokerServiceException {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<UserRepresentation> request = new HttpEntity<UserRepresentation>(userRepresentation,
					httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<String> response = restTemplate.exchange(
					getKeycloakUpdateUserUri(userRepresentation.getId(), keycloakConfiguration), HttpMethod.PUT, request, String.class);

			logoutRefreshAccessToken(organizationName, tokens[1]);
			return response.getStatusCodeValue();
		} catch (NullPointerException | RestClientException | EmailServiceException cause) {
			if (cause.getMessage().contains("" + 409)) {
				log.error(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.IDENTITYBROKER_USER_DETAILS_CONFLICT, userRepresentation.getUsername()), cause);
				return ((HttpClientErrorException) cause).getStatusCode().value();
			} else {
				log.error(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_CREATEUSER_FOR_USER, userRepresentation.getUsername()) + " : "
								+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
						cause);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_CREATEUSER_FOR_USER, userRepresentation.getUsername()) + " : "
								+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
						cause);
			}
		}
	}

	@Override
	public void updateUsersRequiredActionsWithOTP(String organizationName, String userName, boolean configureOTP)
			throws IdentityBrokerServiceException {
		ResponseEntity<String> response = null;

		try {
			UserRepresentation userRepresentation = getUserByUsername(userName, organizationName);

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);
			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			ResponseEntity<List<CredentialRepresentation>> responseCred = restTemplate.exchange(
					getKeycloakUserUri(keycloakConfiguration) + "/" + userRepresentation.getId() + "/credentials",
					HttpMethod.GET, headerEntity, new ParameterizedTypeReference<List<CredentialRepresentation>>() {
					});
			boolean flag = false;
			for (CredentialRepresentation credentialRepresentation : responseCred.getBody()) {
				if ("otp".equalsIgnoreCase(credentialRepresentation.getType())) {

					flag = true;

				}
			}

			if (flag) {

			} else {
				List<String> requiredActions = new ArrayList<String>();
				requiredActions.add("CONFIGURE_TOTP");
				userRepresentation.setRequiredActions(requiredActions);

				httpHeaders.setContentType(MediaType.APPLICATION_JSON);
				HttpEntity<UserRepresentation> request = new HttpEntity<UserRepresentation>(userRepresentation,
						httpHeaders);

				response = restTemplate.exchange(
						getKeycloakUpdateUserUri(userRepresentation.getId(), keycloakConfiguration), HttpMethod.PUT,
						request, String.class);

			}
			logoutRefreshAccessToken(organizationName, tokens[1]);

		} catch (Exception cause) {

			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_UPDATEUSER_FOR_USER, userName) + " : "
							+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
					cause);
			throw new IdentityBrokerServiceException(
					Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_UPDATEUSER_FOR_USER, userName) + " : "
							+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
					cause);

		}
	}

	@Override
	public int createRole(Role role, String organizationName) throws IdentityBrokerServiceException {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<RoleRepresentation> requestEntity = new HttpEntity<>(createRoleRepresentation(role),
					httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<String> response = restTemplate.postForEntity(getKeycloakRealmRoleUri(keycloakConfiguration),
					requestEntity, String.class);

			logoutRefreshAccessToken(organizationName, tokens[1]);

			return response.getStatusCodeValue();
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

	public int createClientRole(Role role, String organizationName) throws IdentityBrokerServiceException {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<RoleRepresentation> requestEntity = new HttpEntity<>(createRoleRepresentation(role),
					httpHeaders);
			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			String url = getKeycloakClientRoleUri(getClientUniqueId(organizationName), keycloakConfiguration);
			ResponseEntity<String> response = restTemplate.postForEntity(url, requestEntity, String.class);

			logoutRefreshAccessToken(organizationName, tokens[1]);

			return response.getStatusCodeValue();
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

	@Override
	public int deleteClientRole(String roleName, String organizationName) throws IdentityBrokerServiceException {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<String> response = restTemplate
					.exchange(getKeycloakClientRoleUri(getClientUniqueId(organizationName), keycloakConfiguration) + "/"
							+ roleName, HttpMethod.DELETE, headerEntity, String.class);

			logoutRefreshAccessToken(organizationName, tokens[1]);

			return response.getStatusCodeValue();
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

	@Override
	public RoleRepresentation getClientRoleByName(String roleName, String organizationName)
			throws IdentityBrokerServiceException {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<RoleRepresentation> response = restTemplate
					.exchange(getKeycloakClientRoleUri(getClientUniqueId(organizationName), keycloakConfiguration) + "/"
							+ roleName, HttpMethod.GET, headerEntity, RoleRepresentation.class);

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				if (response.getBody() == null) {
					log.error(ApplicationConstants.notifySearch, ErrorMessages.ROLE_NOT_FOUND_WITH_ROLENAME, roleName);
					throw new IdentityBrokerServiceException(
							Util.format(ErrorMessages.ROLE_NOT_FOUND_WITH_ROLENAME, roleName));
				}
				return response.getBody();
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.ROLE_NOT_FOUND_WITH_ROLENAME, roleName);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.ROLE_NOT_FOUND_WITH_ROLENAME, roleName));
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

	@Override
	public RoleRepresentation getClientRoleByID(String roleID, String organizationName)
			throws IdentityBrokerServiceException {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<RoleRepresentation> response = restTemplate.exchange(
					getKeycloakClientRoleByIDUri(roleID, keycloakConfiguration), HttpMethod.GET, headerEntity,
					RoleRepresentation.class);

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				if (response.getBody() == null) {
					log.error(ApplicationConstants.notifySearch, ErrorMessages.ROLE_NOT_FOUND_WITH_ROLEID, roleID);
					throw new IdentityBrokerServiceException(
							Util.format(ErrorMessages.ROLE_NOT_FOUND_WITH_ROLEID, roleID));
				}
				return response.getBody();
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.ROLE_NOT_FOUND_WITH_ROLEID, roleID);
				throw new IdentityBrokerServiceException(Util.format(ErrorMessages.ROLE_NOT_FOUND_WITH_ROLEID, roleID));
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

	@Override
	public List<Group> getAllGroups(String organizationName) throws IdentityBrokerServiceException {
		List<Group> groups = new ArrayList<>();
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<GroupRepresentation>> response = restTemplate.exchange(
					getKeycloakGroupUri(keycloakConfiguration), HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<List<GroupRepresentation>>() {
					});

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				List<GroupRepresentation> keycloakGroups = response.getBody();
				if (keycloakGroups != null) {
					keycloakGroups.stream().forEach(keycloakGroup -> {
						Group group = new Group();
						group.setId(keycloakGroup.getId());
						group.setName(keycloakGroup.getName());
						groups.add(group);
					});
				}

			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.GROUP_NOT_FOUND_UNDER_ORGANIZATION,
						organizationName);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.GROUP_NOT_FOUND_UNDER_ORGANIZATION, organizationName));
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return groups;
	}

	@Override
	public List<Group> getGroupsForWorkflow(String organizationName) {
		List<Group> groups = new ArrayList<>();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
				organizationName);
		httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

		HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		ResponseEntity<List<GroupRepresentation>> response = restTemplate.exchange(
				getKeycloakGroupUri(keycloakConfiguration), HttpMethod.GET, headerEntity,
				new ParameterizedTypeReference<List<GroupRepresentation>>() {
				});

		if (response.getStatusCodeValue() == 200) {
			List<GroupRepresentation> keycloakGroups = response.getBody();
			if (keycloakGroups != null) {
				keycloakGroups.stream().forEach(keycloakGroup -> {
					Group group = new Group();
					group.setId(keycloakGroup.getId());
					group.setName(keycloakGroup.getName());

					ResponseEntity<MappingsRepresentation> roleMappingsResponse = restTemplate.exchange(
							getKeycloakGroupUri(keycloakConfiguration) + "/" + keycloakGroup.getId() + "/role-mappings",
							HttpMethod.GET, headerEntity, new ParameterizedTypeReference<MappingsRepresentation>() {
							});
					if (response.getStatusCodeValue() == 200) {
						MappingsRepresentation mappings = roleMappingsResponse.getBody();
						boolean isWorkflowRole = true;
						if (mappings != null) {
							ClientMappings clientMappings = mappings.getClientMappings();
							if (clientMappings != null) {
								WebApp webApp = clientMappings.getWeb_app();
								if (webApp != null && webApp.getClient().equalsIgnoreCase("web_app")) {
									List<Mappings> clientRoleMappings = webApp.getMappings();
									if (clientRoleMappings != null) {
										for (Mappings mapping : clientRoleMappings) {
											if ((mapping.getName().toLowerCase().contains("workflow") || RoleTypeEnum.MOBILEIDUSER.getUserRole().equalsIgnoreCase(mapping.getName()))) {
												// it is a workflow role, so do not add to groups, if is a mobile-id user role, do not add to groups 
												isWorkflowRole = false;
											}
										}
									}
								}
							}
						}
						if (isWorkflowRole) {
							groups.add(group);
						}
					}
				});
			}
		}

		logoutRefreshAccessToken(organizationName, tokens[1]);

		return groups;
	}
	
	@Override
	public List<Group> getGroupsWithWorkflow(String organizationName) {
		List<Group> groups = new ArrayList<>();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
				organizationName);
		httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

		HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		ResponseEntity<List<GroupRepresentation>> response = restTemplate.exchange(
				getKeycloakGroupUri(keycloakConfiguration), HttpMethod.GET, headerEntity,
				new ParameterizedTypeReference<List<GroupRepresentation>>() {
				});

		if (response.getStatusCodeValue() == 200) {
			List<GroupRepresentation> keycloakGroups = response.getBody();
			if (keycloakGroups != null) {
				keycloakGroups.stream().forEach(keycloakGroup -> {
					Group group = new Group();
					group.setId(keycloakGroup.getId());
					group.setName(keycloakGroup.getName());

					ResponseEntity<MappingsRepresentation> roleMappingsResponse = restTemplate.exchange(
							getKeycloakGroupUri(keycloakConfiguration) + "/" + keycloakGroup.getId() + "/role-mappings",
							HttpMethod.GET, headerEntity, new ParameterizedTypeReference<MappingsRepresentation>() {
							});
					if (response.getStatusCodeValue() == 200) {
						MappingsRepresentation mappings = roleMappingsResponse.getBody();
						boolean isWorkflowRole = false;
						if (mappings != null) {
							ClientMappings clientMappings = mappings.getClientMappings();
							if (clientMappings != null) {
								WebApp webApp = clientMappings.getWeb_app();
								if (webApp != null && webApp.getClient().equalsIgnoreCase("web_app")) {
									List<Mappings> clientRoleMappings = webApp.getMappings();
									if (clientRoleMappings != null) {
										for (Mappings mapping : clientRoleMappings) {
											if ((mapping.getName().toLowerCase().contains("workflow") || RoleTypeEnum.MOBILEIDUSER.getUserRole().equalsIgnoreCase(mapping.getName()))) {
												// it is a workflow role, so add to groups, if is a mobile-id user role, do not add to groups 
												isWorkflowRole = true;
											}
										}
									}
								}
							}
						}
						if (isWorkflowRole) {
							groups.add(group);
						}
					}
				});
			}
		}

		logoutRefreshAccessToken(organizationName, tokens[1]);

		return groups;
	}

	@Override
	public Group getGroupById(String organizationName, String groupId) throws IdentityBrokerServiceException {
		Group group = null;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<GroupRepresentation> response = restTemplate.exchange(
					getKeycloakGroupUri(keycloakConfiguration) + "/" + groupId, HttpMethod.GET, headerEntity,
					GroupRepresentation.class);
			// System.out.println(response);
			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				GroupRepresentation keycloakGroup = response.getBody();
				if (keycloakGroup != null) {
					group = new Group();
					group.setId(keycloakGroup.getId());
					group.setName(keycloakGroup.getName());
				}
			} else {
				log.error(ApplicationConstants.notifySearch,
						ErrorMessages.GROUP_NOT_FOUND_WITH_GROUPID_UNDER_ORGANIZATION, groupId, organizationName);
				throw new IdentityBrokerServiceException(Util.format(
						ErrorMessages.GROUP_NOT_FOUND_WITH_GROUPID_UNDER_ORGANIZATION, groupId, organizationName));
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return group;
	}

	@Override
	public void assignGroupToUser(String userId, String groupId, String organizationName)
			throws IdentityBrokerServiceException {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> requestEntity = new HttpEntity<>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			restTemplate.put(getKeycloakUserGroupUri(userId, groupId, keycloakConfiguration), requestEntity,
					new HashMap<String, String>());
			logoutRefreshAccessToken(organizationName, tokens[1]);
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.UNABLE_TO_ASSIGN_GROUP_TO_USERID, groupId,
					userId);
			throw new IdentityBrokerServiceException(
					Util.format(ErrorMessages.UNABLE_TO_ASSIGN_GROUP_TO_USERID, groupId, userId)
							+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
					cause);
		}
	}

	@Override
	public int updateUserGroupByDeletingOldGroup(boolean deletedOldGroup, String oldGroupId, String userId,
			String groupId, String organizationName) throws IdentityBrokerServiceException {
		ResponseEntity<String> response = null;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> requestEntity = new HttpEntity<>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);

			if (deletedOldGroup) {
				response = restTemplate.exchange(getKeycloakUserGroupUri(userId, oldGroupId, keycloakConfiguration),
						HttpMethod.DELETE, requestEntity, String.class);
			}

			restTemplate.put(getKeycloakUserGroupUri(userId, groupId, keycloakConfiguration), requestEntity,
					new HashMap<String, String>());

			logoutRefreshAccessToken(organizationName, tokens[1]);
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.UNABLE_TO_ASSIGN_GROUP_TO_USERID, groupId,
					userId);
			throw new IdentityBrokerServiceException(
					Util.format(ErrorMessages.UNABLE_TO_ASSIGN_GROUP_TO_USERID, groupId, userId)
							+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
					cause);
		}
		return HttpStatus.SC_NO_CONTENT;
	}

	@Override
	public UserRepresentation getUserByUsername(String username, String organizationName)
			throws IdentityBrokerServiceException {
		UserRepresentation userRepresentation = null;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<UserRepresentation>> response = restTemplate.exchange(
					getKeycloakUserUri(keycloakConfiguration) + "?username=" + username, HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<List<UserRepresentation>>() {
					});

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				List<UserRepresentation> userRepresentations = response.getBody();
				if (userRepresentations.size() > 0) {
					userRepresentation = userRepresentations.get(0);
				} else {
					log.error(ApplicationConstants.notifySearch, ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, username);
					throw new IdentityBrokerServiceException(
							Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, username));
				}
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, username);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, username));
			}

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return userRepresentation;
	}

	@Override
	public UserRepresentation getUserExistsByUsernameOREmail(String email, String organizationName)
			throws IdentityBrokerServiceException {
		UserRepresentation userRepresentation = null;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<UserRepresentation>> response = restTemplate.exchange(
					getKeycloakUserUri(keycloakConfiguration) + "?email="+email, HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<List<UserRepresentation>>() {
					});

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				List<UserRepresentation> userRepresentations = response.getBody();
				if (userRepresentations.size() > 0) {
					return userRepresentations.get(0);
				} else {
					return null;
				}
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, email);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, email));
			}

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

	@Override
	public AuthenticatorClientInfo getAuthenticatorClientInfo(String organizationName, String publicKeyB64, String publicKeyAlgorithm) throws CryptoOperationException, IdentityBrokerServiceException{
		AuthenticatorClientInfo authenticatorClientInfo = null;
		try {

			log.info("publicKeyB64 : "+ publicKeyB64);
			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			if(keycloakConfiguration != null) {
				if(publicKeyB64 != null) {
					publicKeyAlgorithm = publicKeyAlgorithm.toUpperCase();
					KeyFactory kf = KeyFactory.getInstance(publicKeyAlgorithm);

					X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64Utils.decodeFromString(publicKeyB64));
					PublicKey rsaPubicKey =  kf.generatePublic(keySpecX509);
				
					authenticatorClientInfo = new AuthenticatorClientInfo(keycloakConfiguration.getClientId(),Base64Utils.encodeToString(cryptoFunction.encryptWithRSA(publicKeyAlgorithm, rsaPubicKey, keycloakConfiguration.getClientSecret())),null);
					log.info("ClientSecret() : "+ authenticatorClientInfo.getClientSecret());
					
				}
			}else {
				log.error(ApplicationConstants.notifyAdmin, ErrorMessages.CLIENT_NOT_FOUND, organizationName);
				throw new IdentityBrokerServiceException(Util.format(ErrorMessages.CLIENT_NOT_FOUND, organizationName));
			}	

		} catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException cause) {
				throw new CryptoOperationException(ErrorMessages.INVALID_PUBLICKEY, cause);
			
		}
		return authenticatorClientInfo;
	}
	
	@Override
	public AuthenticatorClientInfo getOrgOIDCClientCredentials(String organizationName, APIGatewayInfoEntity apiGatewayInfoEntity) throws CryptoOperationException, IdentityBrokerServiceException{
		AuthenticatorClientInfo authenticatorClientInfo = null;
		try {

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			if(keycloakConfiguration != null) {
					PairMobileDeviceConfigEntity pairMobileDeviceConfigEntity = pairMobileDeviceConfigRepository.findPMDConfigByOrg(organizationName);

					String keyStr = apiGatewayInfoEntity.getHost();
					if(apiGatewayInfoEntity.getPort() != null) {
						keyStr = keyStr + "."+apiGatewayInfoEntity.getPort();
					}
					char[] password = keyStr.toCharArray();
					byte[] salt = organizationName.getBytes();
					
					SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			    	KeySpec spec = new PBEKeySpec(password, salt, 65536, 256);
			    	SecretKey tmp = factory.generateSecret(spec);
			    	SecretKey secKey = new SecretKeySpec(tmp.getEncoded(), "AES");
			    	System.out.println(secKey.getEncoded().length);
					
			 	    String encodedKey = Base64.getEncoder().encodeToString(secKey.getEncoded());
			 	    System.out.println(encodedKey);
			 	    
					log.debug("key-->"+encodedKey);
					String initVector = Util.getAlphaNumericString(16);

					String openIDC_string = this.sendGetOpenIDConnectDetails(keycloakConfiguration.getBasePath(), keycloakConfiguration.getRealm());
					
					ObjectMapper objectMapper = new ObjectMapper();
					OpenIDConfiguration openIDConfiguration = objectMapper.readValue(openIDC_string, OpenIDConfiguration.class);
					System.out.println(openIDConfiguration.getIssuer());
					System.out.println(openIDConfiguration.getAuthorizationEndpoint());
					
					
					authenticatorClientInfo = new AuthenticatorClientInfo(keycloakConfiguration.getClientId(),(cryptoFunction.encryptWithAES(secKey, initVector, keycloakConfiguration.getClientSecret(), pairMobileDeviceConfigEntity.getAesEnccipherAlgorithm())), openIDConfiguration);
					log.info("ClientSecret() : "+ authenticatorClientInfo.getClientSecret());
					
			}else {
				log.error(ApplicationConstants.notifyAdmin, ErrorMessages.CLIENT_NOT_FOUND, organizationName);
				throw new IdentityBrokerServiceException(Util.format(ErrorMessages.CLIENT_NOT_FOUND, organizationName));
			}	

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.CLIENT_NOT_FOUND, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.CLIENT_NOT_FOUND, cause);
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.INVALID_KEY_OR_ALGORITHM_USED, cause);
			throw new CryptoOperationException(ErrorMessages.INVALID_KEY_OR_ALGORITHM_USED, cause);
		}
		return authenticatorClientInfo;
	}
	
    private String sendGetOpenIDConnectDetails(String basePath, String organizationName) throws Exception {

		String urlOverHttps = basePath+"/realms/"+organizationName+"/.well-known/openid-configuration";
		TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
		SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

		Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
				.register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();

		BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
				socketFactoryRegistry);
		CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
				.setConnectionManager(connectionManager).build();

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
		ResponseEntity<String> response = new RestTemplate(requestFactory).exchange(urlOverHttps, HttpMethod.GET, null,
				String.class);
		System.out.println(response.getBody());
		httpClient.close();
		return response.getBody();
    }
	
	@Override
	public UserRolesAndGroups getGroupsAndRolesOfUserByID(String userID, String organizationName)
			throws IdentityBrokerServiceException {
		List<GroupRepresentation> groupRepresentations = null;
		List<RoleRepresentation> roleRepresentations = null;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<GroupRepresentation>> response_groups = restTemplate.exchange(
					getKeycloakUserUri(keycloakConfiguration) + "/" + userID + "/groups", HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<List<GroupRepresentation>>() {
					});

			String clientID = getClientUniqueId(organizationName);

			ResponseEntity<List<RoleRepresentation>> response_roles = restTemplate.exchange(
					getKeycloakUserRoleMappingUri(userID, clientID, keycloakConfiguration), HttpMethod.GET,
					headerEntity, new ParameterizedTypeReference<List<RoleRepresentation>>() {
					});
			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response_groups.getStatusCodeValue() == 200 && response_roles.getStatusCodeValue() == 200) {
				groupRepresentations = response_groups.getBody();
				roleRepresentations = response_roles.getBody();
				return new UserRolesAndGroups(userID, groupRepresentations, roleRepresentations);
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.GROUP_AND_ROLES_NOT_FOUND_FOR_USERID,
						userID);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.GROUP_AND_ROLES_NOT_FOUND_FOR_USERID, userID));
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

	@Override
	public List<GroupRepresentation> getGroupsByUserID(String userID, String organizationName)
			throws IdentityBrokerServiceException {
		List<GroupRepresentation> groupRepresentations = null;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<GroupRepresentation>> response = restTemplate.exchange(
					getKeycloakUserUri(keycloakConfiguration) + "/" + userID + "/groups", HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<List<GroupRepresentation>>() {
					});

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				groupRepresentations = response.getBody();
				if (groupRepresentations == null || groupRepresentations.size() <= 0) {
					log.error(ApplicationConstants.notifySearch, ErrorMessages.GROUP_NOT_FOUND_FOR_USERID, userID);
					throw new IdentityBrokerServiceException(
							Util.format(ErrorMessages.GROUP_NOT_FOUND_FOR_USERID, userID));
				}
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.GROUP_NOT_FOUND_FOR_USERID, userID);
				throw new IdentityBrokerServiceException(Util.format(ErrorMessages.GROUP_NOT_FOUND_FOR_USERID, userID));
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return groupRepresentations;
	}

	@Override
	public ResponseEntity<Void> addGroup(String organizationName, Group group) throws IdentityBrokerServiceException {

		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			GroupRepresentation groupRepresentation = new GroupRepresentation();
			groupRepresentation.setName(group.getName());
			HttpEntity<GroupRepresentation> requestEntity = new HttpEntity<>(groupRepresentation, httpHeaders);
			int statusCode = 201;
			ResponseEntity<String> response = null;

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			response = restTemplate.postForEntity(getKeycloakGroupUri(keycloakConfiguration), requestEntity,
					String.class);
			statusCode = response.getStatusCodeValue();

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (statusCode == 201) {
				return new ResponseEntity<>(org.springframework.http.HttpStatus.CREATED);
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.GROUP_FAILED_TO_CREATE, group.getName(),
						organizationName);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.GROUP_FAILED_TO_CREATE, group.getName(), organizationName));
			}
		} catch (NullPointerException | RestClientException cause) {
			if (cause.getMessage().contains("" + 409)) {
				log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
				return new ResponseEntity<>(((HttpClientErrorException) cause).getStatusCode());
			} else {
				log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
				throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			}
		}
	}

	@Override
	public ResponseEntity<Void> updateGroup(Group group, String organizationName)
			throws IdentityBrokerServiceException {

		try {
			if (group.getId() != null) {
				HttpHeaders httpHeaders = new HttpHeaders();
				httpHeaders.setContentType(MediaType.APPLICATION_JSON);
				String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate,
						applicationProperties, organizationName);
				httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

				KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
				HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);
				GroupRepresentation keycloakGroup = null;
				// get all groups and filter with the group name -- (newly edited group name)
				// if found -- check if that group is same as current group .. check with group id.....
				// if not same then throw a message group already found with given name....
				
				ResponseEntity<List<GroupRepresentation>> response = restTemplate.exchange(
						getKeycloakGroupUri(keycloakConfiguration), HttpMethod.GET, headerEntity,
						new ParameterizedTypeReference<List<GroupRepresentation>>() {
						});
				if (response.getStatusCodeValue() == 200) {
					List<GroupRepresentation> keycloakGroups = response.getBody().stream().filter(e -> e.getId().equalsIgnoreCase(group.getId())).collect(Collectors.toList());
					if (keycloakGroups.size() > 0) {
						keycloakGroup = keycloakGroups.get(0);
					}
					List<GroupRepresentation> groups = response.getBody().stream().filter(e -> e.getName().equalsIgnoreCase(group.getName())).collect(Collectors.toList());
					if (groups.size() > 0) {
						GroupRepresentation group1 = groups.get(0);
						if (!group1.getId().equals(group.getId())) {
							logoutRefreshAccessToken(organizationName, tokens[1]);
							return new ResponseEntity<>(org.springframework.http.HttpStatus.CONFLICT);
						}
					}
				}
				
				if(keycloakGroup !=null) {
				keycloakGroup.setName(group.getName());
				HttpEntity<GroupRepresentation> request = new HttpEntity<GroupRepresentation>(keycloakGroup,
						httpHeaders);
				int statusCode = 200;
				ResponseEntity<String> updategroupresponse = restTemplate.exchange(
						getKeycloakGroupUri( keycloakConfiguration) + "/" + group.getId(), HttpMethod.PUT, request,
						String.class);
				statusCode = updategroupresponse.getStatusCodeValue();

		        logoutRefreshAccessToken(organizationName, tokens[1]);
				if (statusCode == 204) {
					wfGroupRepository.updateGroupName(group.getId(), group.getName(),organizationName);
					wfMobileIdStepVisualIDGroupConfigRepository.updateGroupName(group.getId(), group.getName(),organizationName);
					wfStepAdjudicationGroupConfigRepository.updateGroupName(group.getId(), group.getName(),organizationName);
					wfStepApprovalGroupConfigRepository.updateGroupName(group.getId(), group.getName(),organizationName);
					wfStepPrintGroupConfigRepository.updateGroupName(group.getId(), group.getName(),organizationName);
					wfStepChipEncodeAndVIDPrintGroupConfigRepository.updateGroupName(group.getId(), group.getName(),organizationName);
					wfStepChipEncodeGroupConfigRepository.updateGroupName(group.getId(), group.getName(), organizationName);
					return new ResponseEntity<>(org.springframework.http.HttpStatus.OK);
				} else {
					log.error(ApplicationConstants.notifySearch, ErrorMessages.GROUP_FAILED_TO_UPDATE, group.getName(),
							organizationName);
					throw new IdentityBrokerServiceException(
							Util.format(ErrorMessages.GROUP_FAILED_TO_UPDATE, group.getName(), organizationName));
				}
			  }
			}

		} catch (NullPointerException | RestClientException cause) {
			if (cause.getMessage().contains("" + 409)) {
				log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
				return new ResponseEntity<>(((HttpClientErrorException) cause).getStatusCode());
			} else {
				log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
				throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			}
		}
		return null;
	}
	
	@Override
	public List<RoleRepresentation> getRolesByGroupID(String groupID, String organizationName)
			throws IdentityBrokerServiceException {
		List<RoleRepresentation> roleRepresentations = null;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<RoleRepresentation>> response = restTemplate.exchange(
					getKeycloakGroupClientRoleMappingUri(groupID, getClientUniqueId(organizationName),
							keycloakConfiguration),
					HttpMethod.GET, headerEntity, new ParameterizedTypeReference<List<RoleRepresentation>>() {
					});

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				roleRepresentations = response.getBody();
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.ROLE_NOT_FOUND_WITH_GORUPID, groupID);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.ROLE_NOT_FOUND_WITH_GORUPID, groupID));
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return roleRepresentations;
	}

	@Override
	public ResultPage getUsersBySearchCriteria(String organizationName, String searchCriteria, int first, int max)
			throws IdentityBrokerServiceException {
		List<UserRepresentation> userRepresentations = null;
		ResultPage resultPage = null;

		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<UserRepresentation>> response = restTemplate.exchange(
					getKeycloakUserUri(keycloakConfiguration) + "?search=" + searchCriteria + "&first=" + first
							+ "&max=" + max,
					HttpMethod.GET, headerEntity, new ParameterizedTypeReference<List<UserRepresentation>>() {
					});
			ResponseEntity<Integer> usersCount = restTemplate.exchange(
					getKeycloakUserUri(keycloakConfiguration) + "/count", HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<Integer>() {
					});

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				userRepresentations = response.getBody();
				resultPage = new ResultPage();
				resultPage.setContent(userRepresentations);
				if(userRepresentations.size() == 0) {
					resultPage.setTotalElements(0);
				} else {
					resultPage.setTotalElements(usersCount.getBody());
				}
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.USER_NOT_FOUND_WITH_SEARCHCRITERIA,
						searchCriteria);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.USER_NOT_FOUND_WITH_SEARCHCRITERIA, searchCriteria));
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return resultPage;
	}

	@Override
	public ResultPage getMembersOfAGroup(String organizationName, String groupId, int first, int max)
			throws IdentityBrokerServiceException {
		List<UserRepresentation> userRepresentations = null;
		ResultPage resultPage = null;

		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<UserRepresentation>> response = restTemplate.exchange(
					getKeycloakGroupMembersUri(groupId, keycloakConfiguration) + "?first=" + first + "&max=" + max,
					HttpMethod.GET, headerEntity, new ParameterizedTypeReference<List<UserRepresentation>>() {
					});
			ResponseEntity<List<UserRepresentation>> total_response = restTemplate.exchange(
					getKeycloakGroupMembersUri(groupId, keycloakConfiguration), HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<List<UserRepresentation>>() {
					});

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				userRepresentations = response.getBody();
				resultPage = new ResultPage();
				resultPage.setContent(userRepresentations);
				resultPage.setTotalElements(total_response.getBody().size());
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.USER_NOT_FOUND_FOR_GROUPID, groupId);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.USER_NOT_FOUND_FOR_GROUPID, groupId));
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return resultPage;
	}

	@Override
	public ResponseEntity<String> userLogin(String userName, String password, String organizationName)
			throws IdentityBrokerServiceException {
		ResponseEntity<String> response = null;

		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			HttpEntity<MultiValueMap<String, String>> userLoginEntity = new HttpEntity<>(
					KeycloakServiceHelper.createRequestBodyToUserLogin(applicationProperties, organizationName),
					httpHeaders);
			response = restTemplate.postForEntity(
					KeycloakServiceHelper.getKeycloakTokenUri(applicationProperties, organizationName), userLoginEntity,
					String.class);

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return response;
	}

	@Override
	public void userLogout(String userID, String organizationName) throws IdentityBrokerServiceException {
		ResponseEntity<String> response = null;

		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<MultiValueMap<String, String>> userLoginEntity = new HttpEntity<>(
					KeycloakServiceHelper.createRequestBodyToUserLogin(applicationProperties, organizationName),
					httpHeaders);
			response = restTemplate.postForEntity(
					KeycloakServiceHelper.getKeycloakLogoutUri(applicationProperties, userID, organizationName),
					userLoginEntity, String.class);

			logoutRefreshAccessToken(organizationName, tokens[1]);

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return;

	}

	@Override
	public void resetMobileOTPCredential(String organizationName, String userName)
			throws IdentityBrokerServiceException {
		ResponseEntity<String> response = null;
		try {
			UserRepresentation userRepresentation = getUserByUsername(userName, organizationName);

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);
			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			ResponseEntity<List<CredentialRepresentation>> responseCred = restTemplate.exchange(
					getKeycloakUserUri(keycloakConfiguration) + "/" + userRepresentation.getId() + "/credentials",
					HttpMethod.GET, headerEntity, new ParameterizedTypeReference<List<CredentialRepresentation>>() {
					});
			boolean flag = false;
			for (CredentialRepresentation credentialRepresentation : responseCred.getBody()) {
				if ("otp".equalsIgnoreCase(credentialRepresentation.getType())) {
					ResponseEntity<String> responseStr = restTemplate.exchange(
							getKeycloakUserUri(keycloakConfiguration) + "/" + userRepresentation.getId()
									+ "/credentials/" + credentialRepresentation.getId(),
							HttpMethod.DELETE, headerEntity, String.class);
					if (204 == responseStr.getStatusCodeValue()) {
						flag = true;
					}
				}
			}

			List<String> requiredActions = new ArrayList<String>();
			requiredActions.add("CONFIGURE_TOTP");
			userRepresentation.setRequiredActions(requiredActions);

			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<UserRepresentation> request = new HttpEntity<UserRepresentation>(userRepresentation,
					httpHeaders);

			response = restTemplate.exchange(
					getKeycloakUpdateUserUri(userRepresentation.getId(), keycloakConfiguration), HttpMethod.PUT,
					request, String.class);

			logoutRefreshAccessToken(organizationName, tokens[1]);

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return;

	}

	@Override
	public Map<String,Boolean> checkForCredentials(String organizationName, String userName)
			throws IdentityBrokerServiceException {
		ResponseEntity<String> response = null;
		Map<String,Boolean> response_ = new HashMap<String,Boolean>();
		try {
			UserRepresentation userRepresentation = getUserByUsername(userName, organizationName);

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);
			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			ResponseEntity<List<CredentialRepresentation>> responseCred = restTemplate.exchange(
					getKeycloakUserUri(keycloakConfiguration) + "/" + userRepresentation.getId() + "/credentials",
					HttpMethod.GET, headerEntity, new ParameterizedTypeReference<List<CredentialRepresentation>>() {
					});
			boolean flag = false;
			for (CredentialRepresentation credentialRepresentation : responseCred.getBody()) {
				if ("otp".equalsIgnoreCase(credentialRepresentation.getType())) {
					response_.put("OTP", true);
				}
				if ("password".equalsIgnoreCase(credentialRepresentation.getType())) {
					response_.put("PASSWORD", true);
				}
			}

			logoutRefreshAccessToken(organizationName, tokens[1]);

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return response_;

	}
	
	@Override
    public ResponseEntity<String> deleteGroup(String organizationName, String groupId) throws IdentityBrokerServiceException {
        try {
            if (groupId != null) {
                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate,
                        applicationProperties, organizationName);
                httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);
                HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);
                KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
                if (!(workflowService.isWorkflowExistsByGroupID(organizationName, groupId))) {
					ResponseEntity<List<UserRepresentation>> userResponse = restTemplate.exchange(
							getKeycloakGroupMembersUri(groupId, keycloakConfiguration), HttpMethod.GET, headerEntity,
							new ParameterizedTypeReference<List<UserRepresentation>>() {
							});
					if (userResponse.getBody() != null) {
						if (userResponse.getBody().isEmpty()) {
							ResponseEntity<String> deleteGroupResponse = restTemplate.exchange(
									getKeycloakGroupUri(keycloakConfiguration) + "/" + groupId, HttpMethod.DELETE,
									headerEntity, String.class);
							logoutRefreshAccessToken(organizationName, tokens[1]);
							if (deleteGroupResponse.getStatusCodeValue() == 204) {
								log.info("Group with groupid " + groupId + " was successfully deleted");
								return new ResponseEntity<>(org.springframework.http.HttpStatus.OK);
							}
						} else {
							log.info("Group  with groupid  " + groupId + "  under Organization " + organizationName);
							return new ResponseEntity<String>("USER_EXIST",org.springframework.http.HttpStatus.CONFLICT);
						}
					}
                    
                } else {
                    log.error("Group found with group ID  " +groupId+ "  under Organization "+organizationName);                    
                    return new ResponseEntity<>(org.springframework.http.HttpStatus.CONFLICT);
                }
            } else {
                log.error("No Group found with group ID "+groupId+"  under Organization "+organizationName);
                return new ResponseEntity<>(org.springframework.http.HttpStatus.BAD_REQUEST);
            }
        } catch (NullPointerException | RestClientException cause) {
            if (cause.getMessage().contains("" + 404)) {
                log.error("No Group found with group ID "+groupId+"  under Organization "+organizationName);
                return new ResponseEntity<>(org.springframework.http.HttpStatus.NOT_FOUND);
            } else {
                log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
                throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
            }
        }
        return null;
    }
	
	@Override
	public int deleteUser(String userName, String organizationName) throws IdentityBrokerServiceException {
		int statusCodeValue = HttpStatus.SC_CONFLICT;
		String userId = null;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<UserRepresentation>> userResponse = restTemplate.exchange(
					getKeycloakUserUri(keycloakConfiguration) + "?username=" + userName, HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<List<UserRepresentation>>() {
					});
			if (userResponse.getBody() != null) {
				if (userResponse.getBody() != null && !userResponse.getBody().isEmpty()) {
					userId = userResponse.getBody().get(0).getId();
				} else {

					logoutRefreshAccessToken(organizationName, tokens[1]);

					return statusCodeValue = HttpStatus.SC_NOT_FOUND;
				}
			}
			if (userId != null) {
				ResponseEntity<String> response = restTemplate.exchange(
						getKeycloakUserUri(keycloakConfiguration) + "/" + userId, HttpMethod.DELETE, headerEntity,
						String.class);

				logoutRefreshAccessToken(organizationName, tokens[1]);

				return response.getStatusCodeValue();
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return statusCodeValue;
	}

	@Override
	public int deleteUserByID(String userId, String organizationName) throws IdentityBrokerServiceException {
		int statusCodeValue = HttpStatus.SC_CONFLICT;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);

			if (userId != null) {
				ResponseEntity<String> response = restTemplate.exchange(
						getKeycloakUserUri(keycloakConfiguration) + "/" + userId, HttpMethod.DELETE, headerEntity,
						String.class);

				logoutRefreshAccessToken(organizationName, tokens[1]);

				return response.getStatusCodeValue();
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return statusCodeValue;
	}

	@Override
	public void enableSoftOTP2FA(String organizationName, String username, EnableSoftOTP2FA enableSoftOTP2FA) {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					enableSoftOTP2FA.getOrganizationName());
			httpHeaders.set(Constants.HEADER_AUTHORIZATION,
					Constants.HEADER_VALUE_PREFIX_BEARER + KeycloakServiceHelper.getKeycloakAccessToken(restTemplate,
							applicationProperties, enableSoftOTP2FA.getOrganizationName()));

			JSONObject userJsonObject = new JSONObject();
			userJsonObject.put("totp", true);
//			CredentialRepresentation[] credential = new CredentialRepresentation[1];
//			credential[0].setType("password");
//			credential[0].setValue("password");
			// userJsonObject.put("username", username);
			// userJsonObject.put("credentials", new
			// ArrayList<>(Arrays.asList(createCredentialRepresentation("password"))));

			HttpEntity<String> request = new HttpEntity<String>(userJsonObject.toString(), httpHeaders);
			KeycloakConfiguration keycloakConfiguration = applicationProperties
					.getKeycloak(enableSoftOTP2FA.getOrganizationName());
			
			// UserRepresentation userRepresentation = getUserByUsername(username, organizationName);

			restTemplate.put(getKeycloakUpdateUserUri("f792ec64-5e94-4676-bbfe-f86e49ad4960", keycloakConfiguration),
					request, String.class);

		} catch (NullPointerException | RestClientException | EmailServiceException | JSONException cause) {
			if (cause.getMessage().contains("" + 409)) {
				log.error(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.IDENTITYBROKER_USER_DETAILS_CONFLICT, ""), cause);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.IDENTITYBROKER_USER_DETAILS_CONFLICT, ""), cause);
			} else {
				log.error(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_CREATEUSER_FOR_USER, "") + " : "
								+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
						cause);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.IDENTITYBROKER_FAILED_TO_CREATEUSER_FOR_USER, "") + " : "
								+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
						cause);
			}
		}
	}

	public int assignRoleToGroup(String groupId, RoleRepresentation roleRepresentation, String organizationName)
			throws IdentityBrokerServiceException {
		ResponseEntity<String> response = null;

		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<List<RoleRepresentation>> requestEntity = new HttpEntity<>(Arrays.asList(roleRepresentation),
					httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			response = restTemplate.postForEntity(getKeycloakGroupClientRoleMappingUri(groupId,
					getClientUniqueId(organizationName), keycloakConfiguration), requestEntity, String.class);

			logoutRefreshAccessToken(organizationName, tokens[1]);

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return response.getStatusCodeValue();
	}

	private String getClientUniqueId(String organizationName) throws IdentityBrokerServiceException {
		String clientUniqueId = null;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);
			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<ClientRepresentation>> response = restTemplate.exchange(
					getKeycloakClientsUri(keycloakConfiguration), HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<List<ClientRepresentation>>() {
					});

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				List<ClientRepresentation> realmClients = response.getBody();
				if (realmClients != null) {
					for (ClientRepresentation realmClient : realmClients) {
						if (realmClient.getClientId().equalsIgnoreCase(keycloakConfiguration.getClientId())) {
							clientUniqueId = realmClient.getId();
							break;
						}
					}
				}
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return clientUniqueId;
	}

	private UserRepresentation createUserRepresentation(IdentityProviderUser identityProviderUser) {
		UserRepresentation userRepresentation = new UserRepresentation();
		userRepresentation.setFirstName(identityProviderUser.getFirstName());
		userRepresentation.setLastName(identityProviderUser.getLastName());
		userRepresentation.setEmail(identityProviderUser.getEmail());
		userRepresentation.setEnabled(identityProviderUser.isEnabled());
		if (identityProviderUser.getContactNumber() != null && !identityProviderUser.getContactNumber().isEmpty()
				&& identityProviderUser.getCountryCode() != null && !identityProviderUser.getCountryCode().isEmpty()) {
			KeycloakUserAttributes attributes = new KeycloakUserAttributes();
			attributes.setCountryCode(new ArrayList<String>(Arrays.asList(identityProviderUser.getCountryCode())));
			attributes.setContactNumber(new ArrayList<String>(Arrays.asList(identityProviderUser.getContactNumber())));
			userRepresentation.setAttributes(attributes);
		}
		userRepresentation.setUsername(identityProviderUser.getUserName());
		userRepresentation
				.setCredentials(new ArrayList<>(Arrays.asList(createCredentialRepresentation(identityProviderUser))));
		return userRepresentation;
	}

	private CredentialRepresentation createCredentialRepresentation(IdentityProviderUser identityProviderUser) {
		CredentialRepresentation credential = new CredentialRepresentation();
		credential.setType(identityProviderUser.getCredentialType());

		// RandomStringUtils

		// credential.setValue(identityProviderUser.getCredentialValue());
		credential.setValue(generateCommonLangPassword());
		credential.setTemporary(true);
		return credential;
	}

	private CredentialRepresentation createCredentialRepresentation(String passwordtype) {
		CredentialRepresentation credential = new CredentialRepresentation();
		credential.setType(passwordtype);

		// RandomStringUtils

		// credential.setValue(identityProviderUser.getCredentialValue());
		credential.setValue(generateCommonLangPassword());
		credential.setTemporary(true);
		return credential;
	}

	private CredentialRepresentation create2FASoftOTPCredentialRepresentation(EnableSoftOTP2FA enableSoftOTP2FA) {
		CredentialRepresentation credential = new CredentialRepresentation();
		credential.setType("totp");
		credential.setValue(enableSoftOTP2FA.getSecretKey());
		credential.setAlgorithm("SHA-256");
		credential.setDigits(enableSoftOTP2FA.getDigits());
		credential.setPeriod(enableSoftOTP2FA.getInterval());

		return credential;
	}

	public static String generateCommonLangPassword() {
		String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
		String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
		String numbers = RandomStringUtils.randomNumeric(2);
		String specialChar = RandomStringUtils.random(2, 33, 64, false, false);
		String totalChars = RandomStringUtils.randomAlphanumeric(2);
		String combinedChars = upperCaseLetters.concat(lowerCaseLetters).concat(numbers).concat(specialChar)
				.concat(totalChars);
		List<Character> pwdChars = combinedChars.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
		Collections.shuffle(pwdChars);
		String password = pwdChars.stream().collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
				.toString();
		return password;
	}

	private RoleRepresentation createRoleRepresentation(Role role) {
		RoleRepresentation roleRepresentation = new RoleRepresentation();
		roleRepresentation.setName(role.getName());
		roleRepresentation.setDescription(role.getDescription());
		return roleRepresentation;
	}

	private String getKeycloakClientsUri(KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/clients").toString();
	}

	private String getKeycloakUserUri(KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/users").toString();
	}

	private String getKeycloakDeleteUserUri(String userId, KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/users/").append(userId).toString();
	}

	private String getKeycloakUpdateUserUri(String userId, KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/users/").append(userId).toString();
	}

	private String getKeycloakGroupMembersUri(String groupId, KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/groups/").append(groupId).append("/members")
				.toString();
	}

	private String getKeycloakGroupUri(KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/groups").toString();
	}

	private String getKeycloakUserGroupUri(String userId, String groupId, KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/users/").append(userId).append("/groups/")
				.append(groupId).toString();
	}

	private String getKeycloakRealmRoleUri(KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/roles").toString();
	}

	private String getKeycloakClientRoleUri(String clientUniqueId, KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/clients/").append(clientUniqueId).append("/roles")
				.toString();
	}

	private String getKeycloakClientRoleByIDUri(String roleID, KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/roles-by-id/").append(roleID).toString();
	}

	private String getKeycloakUserRoleMappingUri(String userId, String clientUniqueId,
			KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/users/").append(userId)
				.append("/role-mappings/clients/").append(clientUniqueId).toString();
	}

	private String getKeycloakGroupClientRoleMappingUri(String groupId, String clientUniqueId,
			KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/groups/").append(groupId)
				.append("/role-mappings/clients/").append(clientUniqueId).toString();
	}

	private void logoutRefreshAccessToken(String organizationName, String refreshToken) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(KeycloakServiceHelper
				.createRequestBodyToUserLogout(applicationProperties, organizationName, refreshToken), httpHeaders);

		restTemplate.postForEntity(
				KeycloakServiceHelper.getKeycloakLogoutNewAccessTokenUri(applicationProperties, organizationName),
				requestEntity, Object.class);
	}
	
	
	private String getIDPUserCredentialsConfigUri(KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/realms/")
				.append(keycloakConfiguration.getRealm()).append("/credentials/config")
				.toString();
	}
	private String getIDPUserCredentialsUpdateUri(KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/realms/")
				.append(keycloakConfiguration.getRealm()).append("/credentials/update")
				.toString();
	}
	private String getIDPUserCredentialsDeleteUri(KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/realms/")
				.append(keycloakConfiguration.getRealm()).append("/credentials/delete")
				.toString();
	}
	
	private String getIDPWebAuthnRegisterPolicyDetails(String username, KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/realms/")
				.append(keycloakConfiguration.getRealm()).append("/webauthn/policy-details?username=").append(username)
				.toString();
	}
	
	private String registerWebAuthnAuthenticator(String username, KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/realms/")
				.append(keycloakConfiguration.getRealm()).append("/webauthn/register-authenticator?username=").append(username)
				.toString();
	}
	
	private String executeWebAuthnAuthenticator(String username, KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/realms/")
				.append(keycloakConfiguration.getRealm()).append("/webauthn/execute-webauthn?username=").append(username)
				.append("&client_id=web_app")
				.toString();
	}
	
	private String getIDPUserCredentialsUri(String userId, KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/users/").append(userId).append("/credentials").toString();
	}
	
	private String getClientsURI(KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/clients")
				.toString();
	}
	
	private String getClientsURI(KeycloakConfiguration keycloakConfiguration, String clientID) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/admin/realms/")
				.append(keycloakConfiguration.getRealm()).append("/clients/"+clientID)
				.toString();
	}
	
	private String getPNBasedUserLoginUri(KeycloakConfiguration keycloakConfiguration) {
		return new StringBuilder(keycloakConfiguration.getBasePath()).append("/realms/")
				.append(keycloakConfiguration.getRealm()).append("/auth/login")
				.toString();
	}
	@Override
	public void addRedirectURIs(String organizationName, List<String> redirectURIs) {
		
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<ClientRepresentation>> response = restTemplate.exchange(
					getClientsURI(keycloakConfiguration), HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<List<ClientRepresentation>>() {
					});		

			if (response.getStatusCodeValue() == 200) {
				List<ClientRepresentation> clientRepresentations = response.getBody();
				if (clientRepresentations.size() > 0) {
					for (ClientRepresentation clientRepresentation : clientRepresentations) {
						if(clientRepresentation.getClientId().equalsIgnoreCase("web_app")) {
							clientRepresentation.getRedirectUris().addAll(redirectURIs);
							clientRepresentation.getWebOrigins().addAll(redirectURIs);
							httpHeaders.setContentType(MediaType.APPLICATION_JSON);
							HttpEntity<ClientRepresentation> request = new HttpEntity<ClientRepresentation>(clientRepresentation,
									httpHeaders);

							ResponseEntity<String> responseString = restTemplate.exchange(
									getClientsURI(keycloakConfiguration, clientRepresentation.getId()), HttpMethod.PUT,	request, String.class);
							if (responseString.getStatusCodeValue() != 204) {
								throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION);
							}
							break;
						}
					}
				} 
			} 
			logoutRefreshAccessToken(organizationName, tokens[1]);
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}
	
	@Override
	public List<String> getRedirectURIs(String organizationName) {
		List<String> redirectURIs = new ArrayList<String>();
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<ClientRepresentation>> response = restTemplate.exchange(
					getClientsURI(keycloakConfiguration), HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<List<ClientRepresentation>>() {
					});		

			if (response.getStatusCodeValue() == 200) {
				List<ClientRepresentation> clientRepresentations = response.getBody();
				if (clientRepresentations.size() > 0) {
					for (ClientRepresentation clientRepresentation : clientRepresentations) {
						if(clientRepresentation.getClientId().equalsIgnoreCase("web_app")) {
							redirectURIs = clientRepresentation.getRedirectUris();
							break;
						}
					}
				} 
			} 
			logoutRefreshAccessToken(organizationName, tokens[1]);
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
		return redirectURIs;
	}
	
	@Override
	public void deleteRedirectURIs(String organizationName, String redirectURI) {
		
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<ClientRepresentation>> response = restTemplate.exchange(
					getClientsURI(keycloakConfiguration), HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<List<ClientRepresentation>>() {
					});		

			if (response.getStatusCodeValue() == 200) {
				List<ClientRepresentation> clientRepresentations = response.getBody();
				if (clientRepresentations.size() > 0) {
					for (ClientRepresentation clientRepresentation : clientRepresentations) {
						if(clientRepresentation.getClientId().equalsIgnoreCase("web_app")) {
							clientRepresentation.getRedirectUris().remove(redirectURI);
							clientRepresentation.getWebOrigins().remove(redirectURI);
							httpHeaders.setContentType(MediaType.APPLICATION_JSON);
							HttpEntity<ClientRepresentation> request = new HttpEntity<ClientRepresentation>(clientRepresentation,
									httpHeaders);

							ResponseEntity<String> responseString = restTemplate.exchange(
									getClientsURI(keycloakConfiguration, clientRepresentation.getId()), HttpMethod.PUT,	request, String.class);
							if (responseString.getStatusCodeValue() != 204) {
								throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION);
							}
							break;
						}
					}
				} 
			} 
			logoutRefreshAccessToken(organizationName, tokens[1]);
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}
	
	
	@Override
	public UserRepresentation validateUserSession(String organizationName, String userName) {

		try {
			UserRepresentation userRepresentation = this.getUserByUsername(userName, organizationName);
			;

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<UserSessionRepresentation>> response = restTemplate.exchange(
					getKeycloakUserUri(keycloakConfiguration) + "/" + userRepresentation.getId() + "/sessions",
					HttpMethod.GET, headerEntity, new ParameterizedTypeReference<List<UserSessionRepresentation>>() {
					});

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				List<UserSessionRepresentation> userSessionRepresentations = response.getBody();
				if (userSessionRepresentations.size() > 0) {
					return userRepresentation;
				} else {

					log.error(ApplicationConstants.notifySearch, ErrorMessages.USER_SESSION_NOT_FOUND_WITH_USERNAME,
							userName);
					throw new IdentityBrokerServiceException(
							Util.format(ErrorMessages.USER_SESSION_NOT_FOUND_WITH_USERNAME, userName));
				}
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.USER_SESSION_NOT_FOUND_WITH_USERNAME,
						userName);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.USER_SESSION_NOT_FOUND_WITH_USERNAME, userName));
			}

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

	@Override
	public List<Role> getUserClientRoles(String userKLKID, String organizationName) {
		List<Role> roles = new ArrayList<>();
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			String clientID = getClientUniqueId(organizationName);

			ResponseEntity<List<RoleRepresentation>> response = restTemplate.exchange(
					getKeycloakUserRoleMappingUri(userKLKID, clientID, keycloakConfiguration), HttpMethod.GET,
					headerEntity, new ParameterizedTypeReference<List<RoleRepresentation>>() {
					});
			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				if (response.getBody() == null) {
					log.error(ApplicationConstants.notifySearch, ErrorMessages.ROLE_NOT_FOUND_FOR_CLIENT, clientID);
					// throw new
					// IdentityBrokerServiceException(Util.format(ErrorMessages.ROLE_NOT_FOUND_FOR_CLIENT,
					// clientID));
				}
				List<RoleRepresentation> roleReps = response.getBody();
				for (RoleRepresentation roleRepresentation : roleReps) {
					if (!(roleRepresentation.getName().toLowerCase().contains("workflow") || RoleTypeEnum.MOBILEIDUSER.getUserRole().equalsIgnoreCase(roleRepresentation.getName()))) {
						Role role = new Role();
						role.setAuthServiceRoleID(roleRepresentation.getId());
						role.setName(roleRepresentation.getName());
						role.setDescription(roleRepresentation.getDescription());
						roles.add(role);
					}
				}
				if(roles.size() > 1) {
					roles.sort((Role role1, Role role2)->role2.getName().compareTo(role1.getName()));
				}
				return roles;
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.ROLE_NOT_FOUND_FOR_CLIENT, clientID);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.ROLE_NOT_FOUND_FOR_CLIENT, clientID));
			}

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

	@Override
	public int assignRoleToUser(String userId, String[] authServiceRoleIDs, String organizationName) {
		ResponseEntity<String> response = null;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);
			ArrayList<RoleRepresentation> roleRepresentations = new ArrayList<RoleRepresentation>();
			// String[] roleIDs = authServiceRoleIDs.split(",");
			for (String roleID : authServiceRoleIDs) {
				RoleRepresentation roleRepresentation = getClientRoleByID(roleID, organizationName);
				roleRepresentations.add(roleRepresentation);
			}

			HttpEntity<ArrayList<RoleRepresentation>> requestEntity = new HttpEntity<>(roleRepresentations,
					httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			String clientID = getClientUniqueId(organizationName);

			response = restTemplate.postForEntity(
					getKeycloakUserRoleMappingUri(userId, clientID, keycloakConfiguration), requestEntity,
					String.class);

			logoutRefreshAccessToken(organizationName, tokens[1]);
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.UNABLE_TO_ASSIGN_ROLE_TO_USERID,
					authServiceRoleIDs, userId);
			throw new IdentityBrokerServiceException(
					Util.format(ErrorMessages.UNABLE_TO_ASSIGN_ROLE_TO_USERID, authServiceRoleIDs, userId)
							+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
					cause);
		}
		return response.getStatusCodeValue();
	}

	@Override
	public int deleteClientRoleMappingsFromUserAndAssignNewRoles(String userKLKID, String[] authServiceRoleIDs,
			String organizationName) {
		ResponseEntity<String> response = null;
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);
			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			String clientID = getClientUniqueId(organizationName);
			// ArrayList<RoleRepresentation> roleRepresentations = new
			// ArrayList<RoleRepresentation>();

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			// Get the user client role mappings from keycloak
			ResponseEntity<List<RoleRepresentation>> clientRolesRepsonse = restTemplate.exchange(
					getKeycloakUserRoleMappingUri(userKLKID, clientID, keycloakConfiguration), HttpMethod.GET,
					headerEntity, new ParameterizedTypeReference<List<RoleRepresentation>>() {
					});
			ArrayList<RoleRepresentation> removedRoles = new ArrayList<>();
			ArrayList<RoleRepresentation> addedRoles = new ArrayList<>();

			if (clientRolesRepsonse.getStatusCodeValue() == 200 && clientRolesRepsonse.getBody() != null) {
				if (clientRolesRepsonse.getBody().size() > 0) {
					// iterate user client roles from keycloak and check if the role is seleted in
					// ui, if not it means that role is removed
					clientRolesRepsonse.getBody().forEach(roleRepresentation -> {
						boolean isRemoved = true;
						for (String roleId : authServiceRoleIDs) {
							if (roleId.equals(roleRepresentation.getId())) {
								isRemoved = false;
							}
						}
						if (isRemoved) {
							removedRoles.add(roleRepresentation);
						}
					});
				}
				// iterate roles selected in ui and check whether it is already selected, if not
				// that is newly added in edit user details
				for (String roleId : authServiceRoleIDs) {
					List<RoleRepresentation> roles = clientRolesRepsonse.getBody().stream()
							.filter(e -> e.getId().equals(roleId)).collect(Collectors.toList());
					if (roles.size() == 0) {
						RoleRepresentation roleRepresentation = getClientRoleByID(roleId, organizationName);
						addedRoles.add(roleRepresentation);
					}
				}
			} else {
				for (String roleID : authServiceRoleIDs) {
					RoleRepresentation roleRepresentation = getClientRoleByID(roleID, organizationName);
					addedRoles.add(roleRepresentation);
				}
			}
			if (removedRoles.size() > 0) {
				HttpEntity<ArrayList<RoleRepresentation>> requestEntity = new HttpEntity<>(removedRoles, httpHeaders);
				response = restTemplate.exchange(
						getKeycloakUserRoleMappingUri(userKLKID, clientID, keycloakConfiguration), HttpMethod.DELETE,
						requestEntity, String.class);
			}
			if (addedRoles.size() > 0) {
				HttpEntity<ArrayList<RoleRepresentation>> requestEntity = new HttpEntity<>(addedRoles, httpHeaders);
				response = restTemplate.postForEntity(
						getKeycloakUserRoleMappingUri(userKLKID, clientID, keycloakConfiguration), requestEntity,
						String.class);
			}
			logoutRefreshAccessToken(organizationName, tokens[1]);
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.UNABLE_TO_ASSIGN_ROLE_TO_USERID,
					authServiceRoleIDs, userKLKID);
			throw new IdentityBrokerServiceException(
					Util.format(ErrorMessages.UNABLE_TO_ASSIGN_ROLE_TO_USERID, authServiceRoleIDs, userKLKID)
							+ ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION,
					cause);
		}
		if (response == null) {
			return HttpStatus.SC_NO_CONTENT;
		}
		return response.getStatusCodeValue();
	}

	@Override
	public List<Role> getRolesOfClientFromAuthenticationService(String organizationName) {
		List<Role> roles = new ArrayList<Role>();
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			String clientID = getClientUniqueId(organizationName);

			ResponseEntity<List<RoleRepresentation>> response = restTemplate.exchange(
					getKeycloakClientRoleUri(clientID, keycloakConfiguration), HttpMethod.GET, headerEntity,
					new ParameterizedTypeReference<List<RoleRepresentation>>() {
					});

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				if (response.getBody() == null) {
					log.error(ApplicationConstants.notifySearch, ErrorMessages.ROLE_NOT_FOUND_FOR_CLIENT, clientID);
					throw new IdentityBrokerServiceException(
							Util.format(ErrorMessages.ROLE_NOT_FOUND_FOR_CLIENT, clientID));
				}

				List<RoleRepresentation> roleReps = response.getBody();
				for (RoleRepresentation roleRepresentation : roleReps) {
					if (!(roleRepresentation.getName().toLowerCase().contains("workflow") || RoleTypeEnum.MOBILEIDUSER.getUserRole().equalsIgnoreCase(roleRepresentation.getName()))) {
						Role role = new Role();
						role.setAuthServiceRoleID(roleRepresentation.getId());
						role.setDescription(roleRepresentation.getDescription());
						role.setName(roleRepresentation.getName());
						roles.add(role);
					}
				}
				if(roles.size() > 1) {
					roles.sort((Role role1, Role role2)->role2.getName().compareTo(role1.getName()));
				}
				return roles;
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.ROLE_NOT_FOUND_FOR_CLIENT, clientID);
				throw new IdentityBrokerServiceException(
						Util.format(ErrorMessages.ROLE_NOT_FOUND_FOR_CLIENT, clientID));
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

	@Override
	public List<UserRepresentation> getUsersWithSpecifiedRole(String roleName, String organizationName) {

		try {

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);
			String clientID = getClientUniqueId(organizationName);
			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<List<UserRepresentation>> response = restTemplate.exchange(
					getKeycloakClientRoleUri(clientID, keycloakConfiguration) + "/" + roleName + "/users",
					HttpMethod.GET, headerEntity, new ParameterizedTypeReference<List<UserRepresentation>>() {
					});

			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (response.getStatusCodeValue() == 200) {
				List<UserRepresentation> userRepresentations = response.getBody();
				if (userRepresentations.size() > 0) {
					return userRepresentations;
				} else {

					log.error(ApplicationConstants.notifySearch, ErrorMessages.USER_NOT_FOUND_WITH_ROLE, roleName);
					// throw new
					// IdentityBrokerServiceException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_ROLE,
					// roleName));
					return null;
				}
			} else {
				log.error(ApplicationConstants.notifySearch, ErrorMessages.USER_NOT_FOUND_WITH_ROLE, roleName);
				//throw new IdentityBrokerServiceException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_ROLE, roleName));
				return null;
			}

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}
	
	@Override
	public TokenDetails generateMobileIDUserTokens(String organizationName, String devicePublicKey,
			String devicePublicKeyAlgorithm) {

		try {
			TokenDetails tokenDetails = KeycloakServiceHelper.getKeycloakMobileIDUserRefreshAccessTokens(restTemplate,
					applicationProperties, organizationName);
			if (tokenDetails != null) {
				if (devicePublicKey != null) {
					devicePublicKeyAlgorithm = devicePublicKeyAlgorithm.toUpperCase();
					KeyFactory kf = KeyFactory.getInstance(devicePublicKeyAlgorithm);

					X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64Utils.decodeFromString(devicePublicKey));
					PublicKey deviceRSAPubicKey = kf.generatePublic(keySpecX509);
					// need to generate random AES key to encrypt large data, RSA cannot be used for encrypting large data
					PairMobileDeviceConfigEntity pairMobileDeviceConfigEntity = pairMobileDeviceConfigRepository.findPMDConfigByOrg(organizationName);
					SecretKey secKey = cryptoFunction.generateAES256Key();
					log.debug("token-->"+tokenDetails.getAccessToken());
					log.debug("key-->"+Base64Utils.encodeToString(secKey.getEncoded()));
					String initVector = Util.getAlphaNumericString(16);
					tokenDetails.setAccessToken(cryptoFunction.encryptWithAES(secKey, initVector, tokenDetails.getAccessToken(), pairMobileDeviceConfigEntity.getAesEnccipherAlgorithm()));
					tokenDetails.setRefreshToken(cryptoFunction.encryptWithAES(secKey, initVector, tokenDetails.getRefreshToken(), pairMobileDeviceConfigEntity.getAesEnccipherAlgorithm()));
					tokenDetails.setEncAESKey(Base64Utils.encodeToString(cryptoFunction.encryptWithRSA(devicePublicKeyAlgorithm, deviceRSAPubicKey, secKey.getEncoded())));
						
				}
				return tokenDetails;
			} else {
				log.error(ApplicationConstants.notifyCreate, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION);
				throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION);
			}
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeySpecException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.INVALID_KEY_OR_ALGORITHM_USED, cause);
			throw new CryptoOperationException(ErrorMessages.INVALID_KEY_OR_ALGORITHM_USED, cause);
		}
	}
	@Override
	public void validatePNLogin(String organizationName, AuthResponse authResponse) {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<Void> response =  restTemplate.postForEntity(getPNBasedUserLoginUri(keycloakConfiguration),
						authResponse, Void.class);

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}		
	}
	
	@Override
	public ResponseEntity<IDPUserCredentialConfigResponse> addUserCredentials(String organizationName, IDPUserCredentials idpUserCredentials) throws IdentityBrokerServiceException {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<IDPUserCredentialConfigResponse> response = null;
//			if(credentialToBeAdded.equalsIgnoreCase("push-verify")) {
				response = restTemplate.postForEntity(getIDPUserCredentialsConfigUri(keycloakConfiguration),
						idpUserCredentials, IDPUserCredentialConfigResponse.class);
//			} else if(credentialToBeAdded.equalsIgnoreCase("pki")) {
//				response = restTemplate.postForEntity(getUserPKICredentialConfigUri(keycloakConfiguration),
//						idpUserCredentials, String.class);
//			}
			return response;

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}
	
	@Override
	public void updateUserCredentials(String organizationName, IDPUserCredentials idpUserCredentials) throws IdentityBrokerServiceException {
		try {

			log.info("inside updateUserCredentials of keycloak service impl");
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<String> response = null;
			HttpEntity<IDPUserCredentials> requestEntity = new HttpEntity<>(idpUserCredentials, httpHeaders);
			
//			if(credentialToBeAdded.equalsIgnoreCase("push-verify")) {
				response = restTemplate.exchange(
						getIDPUserCredentialsUpdateUri(keycloakConfiguration), HttpMethod.POST, requestEntity,
							String.class);
//			} else if(credentialToBeAdded.equalsIgnoreCase("pki")) {
//				response = restTemplate.postForEntity(getUserPKICredentialConfigUri(keycloakConfiguration),
//						idpUserCredentials, String.class);
//			}
			log.info("return updateUserCredentials of keycloak service impl");
			return;

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}
	
	@Override
	public void deleteUserCredentials(String organizationName, IDPUserCredentials idpUserCredentials) throws IdentityBrokerServiceException {
		try {

			log.info("inside deleteUserCredentials of keycloak service impl");
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<String> response = null;
			HttpEntity<IDPUserCredentials> requestEntity = new HttpEntity<>(idpUserCredentials, httpHeaders);
			
//			if(credentialToBeAdded.equalsIgnoreCase("push-verify")) {
				response = restTemplate.exchange(
						 getIDPUserCredentialsDeleteUri(keycloakConfiguration), HttpMethod.DELETE, requestEntity,
							String.class);
//			} else if(credentialToBeAdded.equalsIgnoreCase("pki")) {
//				response = restTemplate.postForEntity(getUserPKICredentialConfigUri(keycloakConfiguration),
//						idpUserCredentials, String.class);
//			}
			log.info("return deleteUserCredentials of keycloak service impl");
			return;

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}
		
	@Override
	public ResponseEntity<WebAuthnPolicyDetails> getIDPWebAuthnRegisterPolicyDetails(String organizationName, String userName) throws IdentityBrokerServiceException {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);


			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);

			ResponseEntity<WebAuthnPolicyDetails> responseCred = restTemplate.exchange(
					getIDPWebAuthnRegisterPolicyDetails(userName, keycloakConfiguration), HttpMethod.GET, headerEntity, new ParameterizedTypeReference<WebAuthnPolicyDetails>() { });
					
			return responseCred;
			
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}
		
	@Override
	public ResponseEntity<WebAuthnAuthenticatorRegistration> registerWebAuthnAuthenticator(String organizationName, String userName, WebAuthnAuthenticatorRegistration webAuthnAuthenticatorRegistration) throws IdentityBrokerServiceException {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<WebAuthnAuthenticatorRegistration> response = null;
				response = restTemplate.postForEntity(registerWebAuthnAuthenticator(userName, keycloakConfiguration),
						 webAuthnAuthenticatorRegistration, WebAuthnAuthenticatorRegistration.class);
			return response;

		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}
	
	@Override
	public List<CredentialRepresentation> getCredentialsByUserId(String organizationName, String userId) throws IdentityBrokerServiceException{
		try {
			log.info("inside getCredentialsByUserId of keycloak service impl");

			List<CredentialRepresentation> credentialRepresentations = new ArrayList<>();
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);

			ResponseEntity<List<CredentialRepresentation>> responseCred = restTemplate.exchange(
					getIDPUserCredentialsUri(userId, keycloakConfiguration), HttpMethod.GET, headerEntity, new ParameterizedTypeReference<List<CredentialRepresentation>>() { });
			logoutRefreshAccessToken(organizationName, tokens[1]);

			if (responseCred.getStatusCodeValue() == 200) {
				List<CredentialRepresentation> credentialRepresentationsList= responseCred.getBody();
				
				credentialRepresentationsList.forEach(credRep -> {
					CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
					
					credentialRepresentation.setId(credRep.getId());
					credentialRepresentation.setType(credRep.getType());
					credentialRepresentation.setUserLabel(credRep.getUserLabel());
					credentialRepresentation.setPriority(credRep.getPriority());
					
					credentialRepresentations.add(credentialRepresentation);
				});
			}
			return credentialRepresentations;
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}
	
	@Override
	public ResponseEntity<String> moveCredentialAfterAnotherCredential(String organizationName, String userId, String credentialId,
			String newPreviousCredentialId) {
		try {
			log.info("inside moveCredentialAfterAnotherCredential of keycloak service impl");

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<String> response = restTemplate.exchange(
					getIDPUserCredentialsUri(userId, keycloakConfiguration)+"/"+ credentialId +"/moveAfter/"+newPreviousCredentialId, HttpMethod.POST, headerEntity, String.class);
			return response;
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

	@Override
	public ResponseEntity<String> moveCredentialToFirst(String organizationName, String userId, String credentialId) {
		try {
			log.info("inside moveCredentialToFirst of keycloak service impl");
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			String[] tokens = KeycloakServiceHelper.getKeycloakRefreshAccessTokens(restTemplate, applicationProperties,
					organizationName);
			httpHeaders.set(Constants.HEADER_AUTHORIZATION, Constants.HEADER_VALUE_PREFIX_BEARER + tokens[0]);

			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);

			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<String> response = restTemplate.exchange(
					getIDPUserCredentialsUri(userId, keycloakConfiguration)+"/"+ credentialId +"/moveToFirst", HttpMethod.POST, headerEntity, String.class);
			return response;
		} catch (NullPointerException | RestClientException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

	@Override
	public User getLdapUserAttributes(String identityType, User user, UserRepresentation userRepresentation) {
		KeycloakUserAttributes kcUserAttributes = userRepresentation.getAttributes();
		OrganizationEntity organizationEntity = organizationRepository.findByName(user.getOrganisationName());
		UserAttribute userAttribute = user.getUserAttribute();
		UserAddress userAddress = user.getUserAddress();
		try {
			userAttribute.setMiddleName(kcUserAttributes.getMiddleName() == null ? null : kcUserAttributes.getMiddleName().get(0));
			if (kcUserAttributes.getGender() != null) {
				String gender = kcUserAttributes.getGender().get(0);
				if (gender.equalsIgnoreCase("female")) {
					userAttribute.setGender("F");
				} else if (gender.equalsIgnoreCase("male")) {
					userAttribute.setGender("M");
				} else {
					userAttribute.setGender("X");
				}
			}

			String city = kcUserAttributes.getCity() == null ? null : kcUserAttributes.getCity().get(0);
			String state = kcUserAttributes.getState() == null ? null : kcUserAttributes.getState().get(0);
			String street = kcUserAttributes.getStreet() == null ? null : kcUserAttributes.getStreet().get(0);
			String country = kcUserAttributes.getCountry() == null ? null : kcUserAttributes.getCountry().get(0);
			String postalCode = kcUserAttributes.getPostalCode() == null ? null : kcUserAttributes.getPostalCode().get(0);
		/**	if (city != null || state != null || street != null || postalCode != null) {
				String address = "";
				if (street != null) {
					address = address.concat(street + " ");
				}
				if (city != null) {
					address = address.concat(city + " ");
				}
				if (state != null) {
					address = address.concat(state + " ");
				}
				if (postalCode != null) {
					address = address.concat(postalCode);
				}
				userAttribute.setAddress(address);
			} */
			if(userAddress == null) {
				userAddress = new UserAddress();
			}
			userAddress.setAddressLine1(street);
			userAddress.setCity(city);
			userAddress.setState(state);
			userAddress.setCountry(country);
			userAddress.setZipCode(postalCode);
			
			userAttribute.setNationality(country);
			
			userAttribute.setCountryCode((kcUserAttributes.getCountryCode() == null ? null : kcUserAttributes.getCountryCode().get(0)) != null ? kcUserAttributes.getCountryCode().get(0) : null);
			userAttribute.setContact((kcUserAttributes.getContactNumber() == null ? null : kcUserAttributes.getContactNumber().get(0)) != null ? kcUserAttributes.getContactNumber().get(0) : null);
			userAttribute.setPlaceOfBirth(userAttribute.getNationality());
			userAttribute.setMothersMaidenName(kcUserAttributes.getMothersMaidenName() == null ? null: kcUserAttributes.getMothersMaidenName().get(0));
			 SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			userAttribute.setDateOfBirth(kcUserAttributes.getDob() == null ? null: formatter.parse(kcUserAttributes.getDob().get(0)));
			userAttribute.setBloodType(kcUserAttributes.getBloodType() == null ? null
					: getBloodType(kcUserAttributes.getBloodType().get(0)));
			UserAppearance userAppearance = new UserAppearance();
			//	userAppearance.setEyeColor("BLA");
			//	userAppearance.setHeight("5'-2\"");
			userAppearance.setHeight(kcUserAttributes.getHeight() == null ? null : kcUserAttributes.getHeight().get(0));
			userAppearance.setEyeColor(
					kcUserAttributes.getEyeColor() == null ? null : getEyeColor(kcUserAttributes.getEyeColor().get(0)));
			userAppearance.setHairColor(kcUserAttributes.getHairColor() == null ? null
					: getHairColor(kcUserAttributes.getHairColor().get(0)));

			List<UserBiometric> userBiometricsList = new ArrayList<>();
			if (kcUserAttributes.getPhoto() != null) {
				String faceData = kcUserAttributes.getPhoto().get(0);
				if (faceData != null) {
					UserBiometric userBiometric = new UserBiometric();

					userBiometric.setType("FACE");
					userBiometric.setFormat("BASE64");
					userBiometric.setData(faceData.getBytes());
					userBiometricsList.add(userBiometric);
				}
			}

			user.setUserAttribute(userAttribute);
			user.setUserAppearance(userAppearance);
			user.setUserBiometrics(userBiometricsList);

			switch (identityType) {
			case Constants.PIV_and_PIV_1:
				UserPIVModel userPIVModel = new UserPIVModel();

				// userPIVModel.setEmployeeAffiliation(userPIV.getEmployeeAffiliation());
				// userPIVModel.setEmployeeAffiliationColorCode(userPIV.getEmployeeAffiliationColorCode());
				// userPIVModel.setRank(userPIV.getRank());
				userPIVModel.setAgency(organizationEntity.getDisplayName());
				userPIVModel.setIssuerIdentificationNumber(organizationEntity.getIssuerIdentificationNumber());
				userPIVModel.setAgencyAbbreviation(organizationEntity.getAbbreviation());
				// userPivEntities.add(userPIVEntity);
				// user.setUserPIV();//(userPivEntities);
				user.setUserPIV(userPIVModel);
				break;
			case Constants.Employee_ID:
				UserEmployeeID userEmployeeId = new UserEmployeeID();
				// userEmployeeId.setAffiliation(affiliation);
				//		userEmployeeId.set
				break;
			case Constants.National_ID:
				UserNationalID userNationalID = new UserNationalID();
				userNationalID.setPersonalCode(kcUserAttributes.getPersonalCode() == null ? null
						: kcUserAttributes.getPersonalCode().size() > 0 ? kcUserAttributes.getPersonalCode().get(0)
								: null);
				user.setUserNationalID(userNationalID);
			case Constants.Voter_ID:
				UserVoterID userVoterID = new UserVoterID();
				userVoterID.setCurp(kcUserAttributes.getCurp() == null ? null
						: kcUserAttributes.getCurp().size() > 0 ? kcUserAttributes.getCurp().get(0)
								: null);
				userVoterID.setElectorKey(kcUserAttributes.getElectorKey() == null ? null
						: kcUserAttributes.getElectorKey().size() > 0 ? kcUserAttributes.getElectorKey().get(0)
								: null);
				userVoterID.setRegistrationYear(kcUserAttributes.getYearOfRegistration() == null ? null
						: kcUserAttributes.getYearOfRegistration().size() > 0 ? kcUserAttributes.getYearOfRegistration().get(0)
								: null);
				user.setUserVoterID(userVoterID);
			case Constants.Permanent_Resident_ID:
				UserPermanentResidentID userPermanentResidentID = new UserPermanentResidentID();
				userPermanentResidentID.setPersonalCode(kcUserAttributes.getPersonalCode() == null ? null
						: kcUserAttributes.getPersonalCode().size() > 0 ? kcUserAttributes.getPersonalCode().get(0)
								: null);
				user.setUserPermanentResidentID(userPermanentResidentID);
				break;
			case Constants.Driving_License:
				break;
			case Constants.Health_ID:
				break;
			case Constants.Student_ID:
				break;
			}
			return user;
		} catch (Exception  ParseException ) {
			ParseException.printStackTrace();
			return null;
		}
	}

	private String getBloodType(String bloodType) {
		String returnBloodType = null;
		switch (bloodType.toLowerCase()) {
		case "o+":
		case "o positive":
			returnBloodType = "O+";
			break;
		case "o-":
		case "o negative":
			returnBloodType = "O-";
			break;
		case "a+":
		case "a positive":
			returnBloodType = "A+";
			break;
		case "a-":
		case "a negative":
			returnBloodType = "A-";
			break;
		case "b+":
		case "b positive":
			returnBloodType = "B+";
			break;
		case "b-":
		case "b negative":
			returnBloodType = "B-";
			break;
		case "ab+":
		case "ab positive":
			returnBloodType = "AB+";
			break;
		case "ab-":
		case "ab negative":
			returnBloodType = "AB-";
			break;
		default:
			returnBloodType = bloodType;
		}
		return returnBloodType;
	}

	private String getEyeColor(String eyeColor) {
		String returnEyeColor = null;
		switch (eyeColor.toLowerCase()) {
		case "blu":
		case "blue":
			returnEyeColor = "BLU";
			break;
		case "bro":
		case "brown":
			returnEyeColor = "BRO";
			break;
		case "bla":
		case "black":
			returnEyeColor = "BLA";
			break;
		case "gra":
		case "gray":
			returnEyeColor = "GRA";
			break;
		case "grn":
		case "green":
			returnEyeColor = "GRN";
			break;
		case "haz":
		case "hazel":
			returnEyeColor = "HAZ";
			break;
		case "red":
			returnEyeColor = "RED";
			break;
		case "pnk":
		case "pink":
			returnEyeColor = "PNK";
			break;
		case "mar":
		case "maroon":
			returnEyeColor = "MAR";
			break;
		case "dic":
		case "dichromatc":
			returnEyeColor = "DIC";
			break;
		default:
			returnEyeColor = eyeColor;
			break;
		}
		return returnEyeColor;
	}

	private String getHairColor(String hairColor) {
		String returnHairColor = null;
		switch (hairColor.toLowerCase()) {
		case "bla":
		case "black":
			returnHairColor = "BLA";
			break;
		case "bro":
		case "brown":
			returnHairColor = "BRO";
			break;
		case "blo":
		case "blonde":
			returnHairColor = "BLO";
			break;
		case "gra":
		case "gray":
			returnHairColor = "GRA";
			break;
		case "red":
			returnHairColor = "RED";
			break;
		case "san":
		case "sandy":
			returnHairColor = "SAN";
			break;
		case "whi":
		case "white":
			returnHairColor = "WHI";
			break;
		case "bal":
		case "bald":
			returnHairColor = "BAL";
			break;
		default:
			returnHairColor = hairColor;
			break;
		}
		return returnHairColor;
	}

	@Override
	public WebauthnRegisterationConfig executeWebauthn(String organizationName, String userName)
			throws IdentityBrokerServiceException {
		try {
			log.info(organizationName+"---"+userName);
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> headerEntity = new HttpEntity<String>(httpHeaders);
			KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
			ResponseEntity<String> response = null;
			response = restTemplate.exchange(executeWebAuthnAuthenticator(userName, keycloakConfiguration),
					HttpMethod.PUT, headerEntity, String.class);

			log.info(response.getStatusCode()+"");
			if (response.getStatusCodeValue() == 200) {
				log.info("inside");
				if (response.getBody() == null) {
					log.error(ApplicationConstants.notifySearch,
							ErrorMessages.FAILED_TO_EXECUTE_IDP_WEB_AUTHN_REGISTER_FOR_USERNAME, userName);
					throw new IdentityBrokerServiceException(
							Util.format(ErrorMessages.FAILED_TO_EXECUTE_IDP_WEB_AUTHN_REGISTER_FOR_USERNAME, userName));
				}else {
					log.info("config");
					WebauthnRegisterationConfig config = new WebauthnRegisterationConfig();
					log.info(response.getBody());
					log.info(Base64.getEncoder().encodeToString(new QRCodeHelper().getQRCodeImage( new String(Base64UrlUtil.decode(response.getBody())),	BarcodeFormat.QR_CODE, 300, 300)));
					config.setBase64url(response.getBody());
					config.setQrcode(Base64.getEncoder().encodeToString(new QRCodeHelper().getQRCodeImage( new String(Base64UrlUtil.decode(response.getBody())),	BarcodeFormat.QR_CODE, 300, 300)));
					return config;
					
				}
			}
			return null;

		} catch (NullPointerException | RestClientException | WriterException | IOException cause) {
			log.error(ApplicationConstants.notifyAdmin, ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
			throw new IdentityBrokerServiceException(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION, cause);
		}
	}

}
