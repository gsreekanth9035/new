package com.meetidentity.usermanagement.service.impl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.config.Constants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserAttributeEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserGroupsEntity;
import com.meetidentity.usermanagement.domain.WFGroupEntity;
import com.meetidentity.usermanagement.domain.WFMobileIDIdentityIssuanceEntity;
import com.meetidentity.usermanagement.domain.WFStepEntity;
import com.meetidentity.usermanagement.enums.UserStatusEnum;
import com.meetidentity.usermanagement.exception.EntityCreateException;
import com.meetidentity.usermanagement.exception.EntityUpdateException;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.UIMSException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.keycloak.domain.CredentialRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.GroupRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.KeycloakUserAttributes;
import com.meetidentity.usermanagement.keycloak.domain.UserRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.WebAuthnAuthenticatorRegistration;
import com.meetidentity.usermanagement.keycloak.domain.WebAuthnPolicyDetails;
import com.meetidentity.usermanagement.notification.mail.EmailService;
import com.meetidentity.usermanagement.repository.OrganizationRepository;
import com.meetidentity.usermanagement.repository.UserRepository;
import com.meetidentity.usermanagement.repository.WorkflowRepository;
import com.meetidentity.usermanagement.security.SpringSecurityAuditorAware;
import com.meetidentity.usermanagement.service.IdentityProviderUserService;
import com.meetidentity.usermanagement.service.KeycloakService;
import com.meetidentity.usermanagement.service.UserService;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.IDPUserCredentialConfigResponse;
import com.meetidentity.usermanagement.web.rest.model.IDPUserCredentials;
import com.meetidentity.usermanagement.web.rest.model.IdentityProviderUser;
import com.meetidentity.usermanagement.web.rest.model.Role;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.notification.NotificationServiceHelper;
import com.meetidentity.usermanagement.web.rest.util.HeaderUtil;

@Service
@Transactional
public class IdentityProviderUserServiceImpl implements IdentityProviderUserService {

	private final Logger log = LoggerFactory.getLogger(IdentityProviderUserServiceImpl.class);

	@Autowired
	private KeycloakService keycloakService;

	@Autowired
	private WorkflowRepository workflowRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private SpringSecurityAuditorAware springSecurityAuditorAware;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private EmailService  emailSevice;
	
	@Autowired
	private NotificationServiceHelper notificationServiceHelper;
	
	@Autowired
	private OrganizationRepository organizationRepository;


	@Override
	public ResponseEntity<User> createUserAndAssignGroupAndRoles(HttpHeaders httpHeaders, String organizationName,
			IdentityProviderUser identityProviderUser, UserEntity entity) throws EntityCreateException, UIMSException {
		HeaderUtil.validateRequiredHeadersForOnboard(httpHeaders);
			if (httpHeaders.containsKey(Constants.LOGGED_IN_USER)) {
				identityProviderUser.setLoggedInUser(httpHeaders.get(Constants.LOGGED_IN_USER).get(0));
			}

		if(identityProviderUser.isEnabled()) {
			if (httpHeaders.containsKey(Constants.UIMS_URL)) {
				identityProviderUser.setHost(httpHeaders.get(Constants.UIMS_URL).get(0));
			}
		}
		
		//Configure OTP in required actions need to be removed, reason: this is handled while issuing user credentials

		boolean configureOTP = false;
// 		Check if user has OTP credential
//		WFGroupEntity wfGroupEntity = workflowRepository.findWorkflowGroup(organizationName,
//				identityProviderUser.getGroupId());
//		List<WFStepEntity> wfSteps = wfGroupEntity.getWorkflow().getWfSteps();
//		for (WFStepEntity wfStepEntity : wfSteps) {
//			List<WFMobileIDIdentityIssuanceEntity> issuanceEntities = wfStepEntity.getWfMobileIDStepIdentityConfigs();
//			for (WFMobileIDIdentityIssuanceEntity wfMobileIDIdentityIssuanceEntity : issuanceEntities) {
//				if (wfMobileIDIdentityIssuanceEntity.getSoftOTP()) {
//					configureOTP = true;
//					break;
//				}
//			}
//		}

		int statusCode = 0;
		// int statusCode = keycloakService.createUser(identityProviderUser,
		// organizationName, configureOTP);
		ResponseEntity<UserRepresentation> userRepresentationResponse = keycloakService.createUser(identityProviderUser, organizationName,
				configureOTP);
		UserRepresentation userRepresentation = null;
		if(userRepresentationResponse != null) {
			userRepresentation = userRepresentationResponse.getBody();
			statusCode = userRepresentationResponse.getStatusCodeValue();
		}
		System.out.println(">>>>>>>>>>>>>"+statusCode);
		User user = null;
		// if (statusCode == 201) {
		if (null != userRepresentation) {
			log.info(ApplicationConstants.notifyCreate,
					Util.format(ErrorMessages.IDENTITYBROKER_SUCCESSFULLY_CREATED_USER_ACCOUNT,
							identityProviderUser.getUserName()));
			 UserRepresentation userRepresentation1 = keycloakService.getUserByUsername(identityProviderUser.getUserName(),
			   organizationName);
			 userRepresentation.setId(userRepresentation1.getId());
//			if (userRepresentation == null) {
//				log.error(ApplicationConstants.notifySearch, Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, identityProviderUser.getUserName()));
//				throw new IdentityBrokerServiceException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, identityProviderUser.getUserName()));
//			} else {
			try {
				keycloakService.assignGroupToUser(userRepresentation.getId(), identityProviderUser.getGroupId(),
						organizationName);
				log.info(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.IDENTITYBROKER_SUCCESSFULLY_ASSIGNED_GROUP_TO_USER_ACCOUNT,
								identityProviderUser.getGroupId(), userRepresentation.getId()));

				keycloakService.assignRoleToUser(userRepresentation.getId(),
						identityProviderUser.getAuthServiceRoleID(), organizationName);
				log.info(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.IDENTITYBROKER_SUCCESSFULLY_ASSIGNED_ROLE_TO_USER_ACCOUNT,
								identityProviderUser.getAuthServiceRoleID(), userRepresentation.getId()));

				if (entity == null) {
					entity = new UserEntity();

					entity.setName(identityProviderUser.getFirstName() + " " + identityProviderUser.getLastName());
					// entity.setGroupId(identityProviderUser.getGroupId());
					entity.setStatus(UserStatusEnum.PENDING_ENROLLMENT.name());
					entity.setReferenceName(identityProviderUser.getUserName());
					entity.setGroupId(identityProviderUser.getGroupId());
					entity.setGroupName(identityProviderUser.getGroupName());
					entity.setUserStatus(UserStatusEnum.ACTIVE.getUserStatus());
					entity.setIsFederated(false);
					UserAttributeEntity userAttributeEntity = new UserAttributeEntity();
					userAttributeEntity.setFirstName(identityProviderUser.getFirstName());
					userAttributeEntity.setLastName(identityProviderUser.getLastName());
					userAttributeEntity.setEmail(identityProviderUser.getEmail());
					if(identityProviderUser.getCountryCode() != null && !identityProviderUser.getCountryCode().isEmpty() && identityProviderUser.getContactNumber() != null && !identityProviderUser.getContactNumber().isEmpty()) {
						userAttributeEntity.setCountryCode(identityProviderUser.getCountryCode());
						userAttributeEntity.setContact(identityProviderUser.getContactNumber());
					}
					entity.setUserAttribute(userAttributeEntity);

					UserGroupsEntity userGroupsEntity = new UserGroupsEntity();
					userGroupsEntity.setGroupId(identityProviderUser.getGroupId());
					userGroupsEntity.setGroupName(identityProviderUser.getGroupName());
					entity.addUserGroups(userGroupsEntity);
					entity.setCreatedBy(springSecurityAuditorAware.getCurrentAuditor());
				}
				if (entity != null && entity.getId() == null) {
					entity = userService.addUser(entity, organizationName);					
				} else if (entity.getId() != null) {
					entity = userRepository.save(entity);					
				}

				user = new User();
				UserAttributeEntity userAttEntity = entity.getUserAttribute();
				user.setFirstName(userAttEntity.getFirstName());
				user.setLastName(userAttEntity.getLastName());
				user.setName(entity.getReferenceName());
				user.setId(entity.getId());
				user.setUimsId(entity.getUimsId());
				user.setRepresentationId(userRepresentation.getId());
				user.setGroupId(identityProviderUser.getGroupId());
				user.setGroupName(identityProviderUser.getGroupName());
				user.setEmail(userAttEntity.getEmail());
				user.setStatus(entity.getStatus());
				if (entity.getStatus().equalsIgnoreCase(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name())) {
					user.setIsEnrollmentInProgress(true);
				} else if(entity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name())){
					user.setIsEnrollmentInProgress(false);
				}
				
				log.info(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.USER_ADDED_WITH_USERNAME, identityProviderUser.getUserName()));

				
				if (identityProviderUser.isEnabled()) {
					// int emailStatus = emailSevice.sendTemporaryPassword(organizationName, userRepresentation.getFirstName(), userRepresentation.getEmail(), userRepresentation.getCredentials().get(0).getValue());
					log.info("send notification");
					int responseStatusCode = notificationServiceHelper.sendOnboardedWithTempPWDNotification(organizationName, userRepresentation, identityProviderUser,
							entity);
					
					if (responseStatusCode != HttpStatus.OK.value()) {
						log.error(ApplicationConstants.notifySearch,
								Util.format(ErrorMessages.EMAIL_FAILED_TO_SEND, identityProviderUser.getUserName()));
					}
				}
				if (identityProviderUser.isSendInvite() && !httpHeaders.containsKey(Constants.EXPRESS_ENROLLMENT)) {
					log.info("send Enroll Invite notification");
					int responseStatusCode = notificationServiceHelper.sendEnrollmentInviteNotification(organizationName, userRepresentation, identityProviderUser,
							entity);
					
					if (responseStatusCode != HttpStatus.OK.value()) {
						log.error(ApplicationConstants.notifySearch,
								Util.format(ErrorMessages.FAILED_TO_SEND_INVITE_FOR_ENROLLMENT, identityProviderUser.getUserName()));
					}
				}
			} catch (Exception e) {
				keycloakService.deleteUserByID(userRepresentation.getId(), organizationName);
				throw new EntityCreateException(ErrorMessages.FAILED_TO_ADD_USER_WITH_USERNAME,
						userRepresentation.getUsername(), e.getCause());
			}
		}
		return new ResponseEntity<User>(user, HttpStatus.valueOf(statusCode));
	}

	
	@Override
	public ResponseEntity<HttpStatus> createUserLocallyByIDP(IdentityProviderUser identityProviderUser) throws EntityCreateException{
		
		//userrepresentation is not available ..need to investigate further
		/* UserRepresentation userRepresentation = keycloakService.getUserByUsername(identityProviderUser.getUserName(), identityProviderUser.getOrgName());
		 if(userRepresentation != null && userRepresentation.getId()!=null) {
		*/	 
				OrganizationEntity organizationEntity = organizationRepository.findByName(identityProviderUser.getOrgName());
			 	UserEntity entity = new UserEntity();	
				entity.setName(identityProviderUser.getFirstName() + " " + identityProviderUser.getLastName());
				entity.setStatus(UserStatusEnum.PENDING_ENROLLMENT.name());
				entity.setReferenceName(identityProviderUser.getUserName());
				entity.setGroupId(identityProviderUser.getGroupId());
				entity.setGroupName(identityProviderUser.getGroupName());
				entity.setUserStatus(UserStatusEnum.ACTIVE.getUserStatus());
				entity.setIsFederated(false);
				entity.setOrganization(organizationEntity);
				
				UserAttributeEntity userAttributeEntity = new UserAttributeEntity();
				userAttributeEntity.setFirstName(identityProviderUser.getFirstName());
				userAttributeEntity.setLastName(identityProviderUser.getLastName());
				userAttributeEntity.setEmail(identityProviderUser.getEmail());
				entity.setUserAttribute(userAttributeEntity);
	
				UserGroupsEntity userGroupsEntity = new UserGroupsEntity();
				userGroupsEntity.setGroupId(identityProviderUser.getGroupId());
				userGroupsEntity.setGroupName(identityProviderUser.getGroupName());
				entity.addUserGroups(userGroupsEntity);
				entity.setCreatedBy(springSecurityAuditorAware.getCurrentAuditor());
			
				entity = userService.addUser(entity, identityProviderUser.getOrgName());	
			
				identityProviderUser.setOrgId(organizationEntity.getId());
				identityProviderUser.setLoggedInUserId(entity.getId());
				identityProviderUser.setLoggedInUser(entity.getReferenceName());
				
				try {
					log.info("-----"+new ObjectMapper().writeValueAsString(identityProviderUser));
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			if (identityProviderUser.isEnabled()) {
				log.info("send Onboarded With Temp PWD Notification");
				int responseStatusCode = notificationServiceHelper.sendOnboardedWithTempPWDNotification(identityProviderUser.getOrgName(), null, identityProviderUser,
						entity);
				
				if (responseStatusCode != HttpStatus.OK.value()) {
					log.error(ApplicationConstants.notifySearch,
							Util.format(ErrorMessages.EMAIL_FAILED_TO_SEND, identityProviderUser.getUserName()));
				}
			}
			if (identityProviderUser.isSendInvite()) {
				log.info("send Enrollment Invite Notification");
				int responseStatusCode = notificationServiceHelper.sendEnrollmentInviteNotification(identityProviderUser.getOrgName(), null, identityProviderUser,
						entity);
				
				if (responseStatusCode != HttpStatus.OK.value()) {
					log.error(ApplicationConstants.notifySearch,
							Util.format(ErrorMessages.FAILED_TO_SEND_INVITE_FOR_ENROLLMENT, identityProviderUser.getUserName()));
				}
			}
			/* } */
			return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<User> updateUserAndAssignGroupAndRoles(HttpHeaders httpHeaders, String organizationName,
			IdentityProviderUser identityProviderUser)
			throws EntityCreateException, IdentityBrokerServiceException, JSONException, UIMSException {
		log.info("headers in updateUserAndAssignGroupAndRoles" + httpHeaders);
		HeaderUtil.validateRequiredHeadersForOnboard(httpHeaders);
		if (httpHeaders.containsKey(Constants.LOGGED_IN_USER)) {
			identityProviderUser.setLoggedInUser(httpHeaders.get(Constants.LOGGED_IN_USER).get(0));
		}
		if (httpHeaders.containsKey(Constants.UIMS_URL)) {
			identityProviderUser.setHost(httpHeaders.get(Constants.UIMS_URL).get(0));
		}
		User user = new User();
		boolean userExists = false;
		try {

			// update selected user details/groups/roles/
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(identityProviderUser.getUserName(),
					   organizationName);
			if(identityProviderUser.getFirstName() == null) {
				identityProviderUser.setFirstName(userRepresentation.getFirstName());
			}else {
				userRepresentation.setFirstName(identityProviderUser.getFirstName());
			}
			if(identityProviderUser.getLastName() == null) {
				identityProviderUser.setLastName(userRepresentation.getLastName());
			}else {
				userRepresentation.setLastName(identityProviderUser.getLastName());
			}
			if(identityProviderUser.getEmail() == null) {
				identityProviderUser.setEmail(userRepresentation.getEmail());
			}else {
				userRepresentation.setEmail(identityProviderUser.getEmail());
			}
			userRepresentation.setEnabled(identityProviderUser.isEnabled());
			if (identityProviderUser.getContactNumber() != null) {
				KeycloakUserAttributes attributes = new KeycloakUserAttributes();
				attributes.setCountryCode(new ArrayList<String>(Arrays.asList(identityProviderUser.getCountryCode())));
				attributes.setContactNumber(new ArrayList<String>(Arrays.asList(identityProviderUser.getContactNumber())));
				userRepresentation.setAttributes(attributes);
			}
			
			//Configure OTP in required actions need to be removed, reason: this is handled while issuing user credentials
			
			
// 			Check if user has OTP credential
//			WFGroupEntity wfGroupEntity = workflowRepository.findWorkflowGroup(organizationName,
//					identityProviderUser.getGroupId());
//			boolean configureOTP = false;
//			boolean removeOTPConfig = false;
//			List<WFStepEntity> wfSteps = wfGroupEntity.getWorkflow().getWfSteps();
//			for (WFStepEntity wfStepEntity : wfSteps) {
//				List<WFMobileIDIdentityIssuanceEntity> issuanceEntities = wfStepEntity.getWfMobileIDStepIdentityConfigs();
//				for (WFMobileIDIdentityIssuanceEntity wfMobileIDIdentityIssuanceEntity : issuanceEntities) {
//					if (wfMobileIDIdentityIssuanceEntity.getSoftOTP()) {
//						configureOTP = true;
//						break;
//					}
//				}
//			}
//			List<String> requiredActions = userRepresentation.getRequiredActions();
//			
//			List<String> otpCredential = new ArrayList<>();
//			if(requiredActions != null) {
//				otpCredential = requiredActions.stream().filter(e -> e.equalsIgnoreCase("CONFIGURE_TOTP")).collect(Collectors.toList());
//				if(otpCredential.size()>0) {
//					if(configureOTP) {
//						configureOTP = false;
//					} else {
//						removeOTPConfig = true;
//					}
//				}
//			} else {
//				requiredActions = new ArrayList<String>();
//			}
//			if (configureOTP) {
//				requiredActions.add("CONFIGURE_TOTP");
//				userRepresentation.setRequiredActions(requiredActions);
//			} else if(removeOTPConfig){
//				requiredActions.removeIf(e -> e.equalsIgnoreCase("CONFIGURE_TOTP"));
//				userRepresentation.setRequiredActions(requiredActions);
//			}
			
			int statusCode = keycloakService.updateUser(userRepresentation, organizationName);
			if(userRepresentation.getFederationLink() != null) {
				user.setIsFederated(true);
			} else {
				user.setIsFederated(false);
			}
			if (statusCode == HttpStatus.CONFLICT.value()) {
				return new ResponseEntity<User>(user, HttpStatus.valueOf(statusCode));
			}
			/*
			 * } else { System.out.println("user is from Ldap"); // then this user is Ldap
			 * user so we can't edit the user attributes for this user, and also it depends
			 * on the use federation configuration }
			 */
			UserEntity entity = userRepository.getUserByName(identityProviderUser.getUserName(), organizationName);
			UserAttributeEntity userAttributeEntity = null;
			if (entity == null) {
				entity = new UserEntity();
				
				entity.setName(identityProviderUser.getFirstName() + " " + identityProviderUser.getLastName());
				entity.setStatus(UserStatusEnum.PENDING_ENROLLMENT.name());
				entity.setReferenceName(identityProviderUser.getUserName());
				entity.setGroupId(identityProviderUser.getGroupId());
				entity.setGroupName(identityProviderUser.getGroupName());
				entity.setUserStatus(UserStatusEnum.ACTIVE.getUserStatus());
				entity.setIsFederated(user.getIsFederated());
				entity.setCreatedBy(springSecurityAuditorAware.getCurrentAuditor());
				userAttributeEntity = new UserAttributeEntity();
				
				List<GroupRepresentation> groups = keycloakService.getGroupsByUserID(identityProviderUser.getUserKLKId(), organizationName);
				if (!groups.get(0).getId().equals(identityProviderUser.getGroupId())) {
					keycloakService.updateUserGroupByDeletingOldGroup(true, groups.get(0).getId(),
							identityProviderUser.getUserKLKId(), identityProviderUser.getGroupId(), organizationName);
				}
				log.info(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.IDENTITYBROKER_SUCCESSFULLY_ASSIGNED_GROUP_TO_USER_ACCOUNT,
								identityProviderUser.getGroupId(), identityProviderUser.getUserKLKId()));
	
				// delete client roles mappings from user if any roles removed add if any added
				keycloakService.deleteClientRoleMappingsFromUserAndAssignNewRoles(identityProviderUser.getUserKLKId(),
						identityProviderUser.getAuthServiceRoleID(), organizationName);
				log.info(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.IDENTITYBROKER_SUCCESSFULLY_ASSIGNED_ROLE_TO_USER_ACCOUNT,
								identityProviderUser.getAuthServiceRoleID(), identityProviderUser.getUserKLKId()));
			}else {
				userExists = true;
				entity.setLastModifiedBy(springSecurityAuditorAware.getCurrentAuditor());
				userAttributeEntity = entity.getUserAttribute();
				if (!entity.getGroupId().equals(identityProviderUser.getGroupId())) {
					keycloakService.updateUserGroupByDeletingOldGroup(true, entity.getGroupId(),
							identityProviderUser.getUserKLKId(), identityProviderUser.getGroupId(), organizationName);
				}
				// delete client roles mappings from user if any roles removed add if any added
				keycloakService.deleteClientRoleMappingsFromUserAndAssignNewRoles(identityProviderUser.getUserKLKId(),
						identityProviderUser.getAuthServiceRoleID(), organizationName);
			}
			
//				JSONObject userJsonObject = new JSONObject();
//				// UserRepresentation userRepresentation =
//				// keycloakService.getUserByUsername(identityProviderUser.getUserName(),
//				// organizationName);
//				// if(userRepresentation.getFederationLink() == null) {
//				userJsonObject.put("firstName", identityProviderUser.getFirstName());
//				userJsonObject.put("lastName", identityProviderUser.getLastName());
//				userJsonObject.put("email", identityProviderUser.getEmail());
//				userJsonObject.put("enabled", identityProviderUser.isEnabled());
//				int statusCode = keycloakService.updateUser(identityProviderUser.getUserKLKId(), userJsonObject,
//						organizationName, identityProviderUser.getUserName());
//				if (statusCode == HttpStatus.CONFLICT.value()) {
//					return new ResponseEntity<User>(user, HttpStatus.valueOf(statusCode));
//				}
//				/*
//				 * } else { System.out.println("user is from Ldap"); // then this user is Ldap
//				 * user so we can't edit the user attributes for this user, and also it depends
//				 * on the use federation configuration }
//				 */
//				// add newly selected role and delete removed roles
//				if (!entity.getGroupId().equals(identityProviderUser.getGroupId())) {
//					keycloakService.updateUserGroupByDeletingOldGroup(true, entity.getGroupId(),
//							identityProviderUser.getUserKLKId(), identityProviderUser.getGroupId(), organizationName);
//				}
//				// delete client roles mappings from user if any roles removed add if any added
//				keycloakService.deleteClientRoleMappingsFromUserAndAssignNewRoles(identityProviderUser.getUserKLKId(),
//						identityProviderUser.getAuthServiceRoleID(), organizationName);
	
				entity.setName(identityProviderUser.getFirstName() + " " + identityProviderUser.getLastName());
				entity.setGroupId(identityProviderUser.getGroupId());
				entity.setGroupName(identityProviderUser.getGroupName());				
				userAttributeEntity.setFirstName(identityProviderUser.getFirstName());
				userAttributeEntity.setLastName(identityProviderUser.getLastName());
				userAttributeEntity.setEmail(identityProviderUser.getEmail());
				userAttributeEntity.setCountryCode(identityProviderUser.getCountryCode());
				userAttributeEntity.setContact(identityProviderUser.getContactNumber());
				entity.setUserAttribute(userAttributeEntity);
				
				entity = userService.addUser(entity, organizationName);
	
				user.setFirstName(userAttributeEntity.getFirstName());
				user.setLastName(userAttributeEntity.getLastName());
				user.setName(entity.getReferenceName());
				user.setId(entity.getId());
				user.setGroupId(entity.getGroupId());
				user.setGroupName(entity.getGroupName());
				user.setEmail(userAttributeEntity.getEmail());
				user.setIsFederated(entity.getIsFederated());
				if (entity.getStatus().equalsIgnoreCase(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name())) {
					user.setIsEnrollmentInProgress(true);
				} else if(entity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name())){
					user.setIsEnrollmentInProgress(false);
				}

			if (identityProviderUser.isSendInvite()) {
				log.info("send Enroll Invite notification");
				int responseStatusCode = notificationServiceHelper.sendEnrollmentInviteNotification(organizationName, userRepresentation, identityProviderUser,
						entity);
				
				if (responseStatusCode != HttpStatus.OK.value()) {
					log.error(ApplicationConstants.notifySearch,
							Util.format(ErrorMessages.FAILED_TO_SEND_INVITE_FOR_ENROLLMENT, identityProviderUser.getUserName()));
				}
			}
		} catch (Exception e) {
			//keycloakService.deleteUserByID(identityProviderUser.getUserKLKId(), organizationName);
			throw new EntityCreateException(ErrorMessages.FAILED_TO_ADD_USER_WITH_USERNAME,
					identityProviderUser.getUserName(), e.getCause());
		}
		return new ResponseEntity<User>(user, HttpStatus.valueOf(200));
	}
	

	@Override
	public void updateUserRoles(String organizationName, IdentityProviderUser identityProviderUser)
			throws EntityCreateException, IdentityBrokerServiceException {
		try {

			// update selected user roles
			UserRepresentation userRepresentation = keycloakService
					.getUserByUsername(identityProviderUser.getUserName(), organizationName);

			// delete client roles mappings from user if any roles removed add if any added
			keycloakService.deleteClientRoleMappingsFromUserAndAssignNewRoles(userRepresentation.getId(),
					identityProviderUser.getAuthServiceRoleID(), organizationName);
			log.info(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.IDENTITYBROKER_SUCCESSFULLY_ASSIGNED_ROLE_TO_USER_ACCOUNT,
							identityProviderUser.getAuthServiceRoleID(), userRepresentation.getId()));

		} catch (Exception e) {
			//keycloakService.deleteUserByID(identityProviderUser.getUserKLKId(), organizationName);
			throw new EntityUpdateException(e.getCause(), ErrorMessages.FAILED_TO_ADD_USER_WITH_USERNAME,
					identityProviderUser.getUserName());
		}
	}
	
	@Override
	public ResponseEntity<IdentityProviderUser> getOnboardUserDetails(String organizationName, String userName) {
		IdentityProviderUser identityProviderUser = null;
		try {
			UserRepresentation userRepresentation = keycloakService.getUserByUsername(userName, organizationName);
			identityProviderUser = new IdentityProviderUser();
			identityProviderUser.setEnabled(userRepresentation.isEnabled());
			identityProviderUser.setUserKLKId(userRepresentation.getId());
			List<Role> roles = keycloakService.getUserClientRoles(userRepresentation.getId(), organizationName);
			identityProviderUser.setRoles(roles);
			identityProviderUser.setFirstName(userRepresentation.getFirstName());
			identityProviderUser.setLastName(userRepresentation.getLastName());
			identityProviderUser.setUserName(userRepresentation.getUsername());
			identityProviderUser.setEmail(userRepresentation.getEmail());
			List<GroupRepresentation> groups = keycloakService.getGroupsByUserID(userRepresentation.getId(),
					organizationName);
			identityProviderUser.setGroupId(groups.get(0).getId());
			identityProviderUser.setGroupName(groups.get(0).getName());
			/** if(userRepresentation.getAttributes() != null) {
				for (String phoneNumber : userRepresentation.getAttributes().getContactNumber()) {
					identityProviderUser.setContactNumber(phoneNumber);				
				} 
			} */
			UserEntity userEntity = userRepository.getUserByName(userName, organizationName);
			if(userEntity != null) {
				identityProviderUser.setUserStatus(userEntity.getStatus());
				UserAttributeEntity userAttributeEntity = userEntity.getUserAttribute();
				identityProviderUser.setContactNumber(userAttributeEntity.getContact());
				identityProviderUser.setCountryCode(userAttributeEntity.getCountryCode());
			} else {
				identityProviderUser.setUserStatus(UserStatusEnum.PENDING_ENROLLMENT.name());
				if(userRepresentation.getAttributes() != null) {
					for (String countryCode : userRepresentation.getAttributes().getCountryCode()) {
						identityProviderUser.setCountryCode(countryCode);		
					}
					for (String phoneNumber : userRepresentation.getAttributes().getContactNumber()) {
						identityProviderUser.setContactNumber(phoneNumber);		
					}
				}
			}
			return new ResponseEntity<IdentityProviderUser>(identityProviderUser, HttpStatus.OK);
		} catch (Exception e) {
			log.error(ApplicationConstants.notifySearch,
					Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, userName));
			throw new IdentityBrokerServiceException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERNAME, userName));
		}
	}

	
	@Override
	public ResponseEntity<IDPUserCredentialConfigResponse> addUserCredentials(Long userId, String organizationName, IDPUserCredentials credentials) {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			UserEntity userEntity = userRepository.getOne(userId);
			if (userEntity != null) {
				credentials.setUsername(userEntity.getReferenceName());
				ResponseEntity<IDPUserCredentialConfigResponse> response = keycloakService.addUserCredentials(organizationName, credentials);
				return  new ResponseEntity<IDPUserCredentialConfigResponse>(response.getBody(), HttpStatus.OK);
			} else {
				log.error(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userId));
				throw new IdentityBrokerServiceException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userId));
			}
		} catch (Exception e) {
			log.error(ApplicationConstants.notifySearch,
					Util.format(ErrorMessages.FAILED_TO_ADD_IDP_CREDENTIALS_FOR_USERID, userId));
			throw new IdentityBrokerServiceException(Util.format(ErrorMessages.FAILED_TO_ADD_IDP_CREDENTIALS_FOR_USERID, userId), e);
		}
	}
	
	@Override
	public void updateUserCredentials(Long userId, String organizationName, IDPUserCredentials credentials) {
		try {
			log.info("inside updateUserCredentials");
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			UserEntity userEntity = userRepository.getOne(userId);			
			if (userEntity != null) {
				credentials.setUsername(userEntity.getReferenceName());
				keycloakService.updateUserCredentials(organizationName, credentials);
				return;
			} else {
				log.error(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userId));
				throw new IdentityBrokerServiceException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userId));
			}
		} catch (Exception e) {
			log.error(ApplicationConstants.notifySearch,
					Util.format(ErrorMessages.FAILED_TO_UPDATE_IDP_CREDENTIALS_FOR_USERID, userId));
			throw new IdentityBrokerServiceException(Util.format(ErrorMessages.FAILED_TO_UPDATE_IDP_CREDENTIALS_FOR_USERID, userId), e);
		}
	}
	
	@Override
	public void deleteUserCredentials(Long userId, String organizationName, IDPUserCredentials credentials) {
		try {
			log.info("inside deleteUserCredentials");
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			UserEntity userEntity = userRepository.getOne(userId);			
			if (userEntity != null) {
				credentials.setUsername(userEntity.getReferenceName());
				keycloakService.deleteUserCredentials(organizationName, credentials);
				return;
			} else {
				log.error(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userId));
				throw new IdentityBrokerServiceException(Util.format(ErrorMessages.USER_NOT_FOUND_WITH_USERID, userId));
			}
		} catch (Exception e) {
			log.error(ApplicationConstants.notifySearch,
					Util.format(ErrorMessages.FAILED_TO_DELETE_IDP_CREDENTIALS_FOR_USERID, userId));
			throw new IdentityBrokerServiceException(Util.format(ErrorMessages.FAILED_TO_DELETE_IDP_CREDENTIALS_FOR_USERID, userId), e);
		}
	}

	@Override
	public String listUserCredentials(Long userId, String organizationName) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<CredentialRepresentation> getCredentialsByUserId(String organizationName, String userId) {	
		return keycloakService.getCredentialsByUserId(organizationName, userId);
	}


	@Override
	public ResponseEntity<String> moveCredentialAfterAnotherCredential(String organizationName, String userId,
			String credentialId, String newPreviousCredentialId) {
		return keycloakService.moveCredentialAfterAnotherCredential(organizationName, userId, credentialId, newPreviousCredentialId);
	}


	@Override
	public ResponseEntity<String> moveCredentialToFirst(String organizationName, String userId, String credentialId) {
		return keycloakService.moveCredentialToFirst(organizationName, userId, credentialId);
	}
	

	
	@Override
	public ResponseEntity<WebAuthnPolicyDetails> getIDPWebAuthnRegisterPolicyDetails(String userName, String organizationName) {
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentType(MediaType.APPLICATION_JSON);
			ResponseEntity<WebAuthnPolicyDetails> response = keycloakService.getIDPWebAuthnRegisterPolicyDetails(organizationName, userName);
			return response;

		} catch (Exception e) {
			log.error(ApplicationConstants.notifySearch,
					Util.format(ErrorMessages.FAILED_TO_LIST_IDP_WEB_AUTHN_POLICY_DETAILS_FOR_USERNAME, userName));
			throw new IdentityBrokerServiceException(Util.format(ErrorMessages.FAILED_TO_LIST_IDP_WEB_AUTHN_POLICY_DETAILS_FOR_USERNAME, userName), e);
		}
	}


	@Override
	public ResponseEntity<WebAuthnAuthenticatorRegistration> registerWebAuthnAuthenticator(String userName, String organizationName,
			WebAuthnAuthenticatorRegistration webAuthnAuthenticatorRegistration) {	try {
				HttpHeaders httpHeaders = new HttpHeaders();
				httpHeaders.setContentType(MediaType.APPLICATION_JSON);
				ResponseEntity<WebAuthnAuthenticatorRegistration> response = keycloakService.registerWebAuthnAuthenticator(organizationName, userName, webAuthnAuthenticatorRegistration);
				return  new ResponseEntity<WebAuthnAuthenticatorRegistration>(response.getBody(), HttpStatus.OK);
			
			} catch (Exception e) {
				log.error(ApplicationConstants.notifySearch,
						Util.format(ErrorMessages.FAILED_TO_ADD_IDP_CREDENTIALS_FOR_USERNAME, userName));
				throw new IdentityBrokerServiceException(Util.format(ErrorMessages.FAILED_TO_ADD_IDP_CREDENTIALS_FOR_USERNAME, userName), e);
			}
	}

	
}
