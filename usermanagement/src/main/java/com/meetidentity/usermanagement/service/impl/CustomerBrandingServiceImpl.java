package com.meetidentity.usermanagement.service.impl;

import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meetidentity.usermanagement.config.ApplicationProperties;
import com.meetidentity.usermanagement.domain.CustomerBrandingEntity;
import com.meetidentity.usermanagement.exception.security.CryptoOperationException;
import com.meetidentity.usermanagement.repository.CustomerBrandingRepository;
import com.meetidentity.usermanagement.repository.PairMobileDeviceConfigRepository;
import com.meetidentity.usermanagement.repository.UserIdentificationCodeRepository;
import com.meetidentity.usermanagement.repository.UserRepository;
import com.meetidentity.usermanagement.security.CryptoFunction;
import com.meetidentity.usermanagement.service.CustomerBrandingService;
import com.meetidentity.usermanagement.service.UserService;
import com.meetidentity.usermanagement.web.feign.client.ApduFeignClient;
import com.meetidentity.usermanagement.web.rest.model.CustomerBranding;
import com.meetidentity.usermanagement.web.rest.model.UserDeviceInfo;

@Service
@Transactional
public class CustomerBrandingServiceImpl implements CustomerBrandingService {

	
	private final Logger log = LoggerFactory.getLogger(CustomerBrandingServiceImpl.class);
			
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	CustomerBrandingRepository customerBrandingRepository;
	
	@Autowired
	UserIdentificationCodeRepository userIdentificationCodeRepository;
	
	@Autowired
	PairMobileDeviceConfigRepository pairMobileDeviceConfigRepository;
	
	@Autowired
	UserService userService;
	
	@Autowired
	ApplicationProperties applicationProperties;
	
	@Autowired
	CryptoFunction cryptoFunction;
	
	@Autowired
	ApduFeignClient apduFeignClient;
	
	@Override
	public CustomerBranding getCustomerBranding(String organizationName) throws CryptoOperationException{
		CustomerBranding customerBranding = new CustomerBranding();
		//String uuid = userService.decryptUserUUID( userDevice.getUuid()) ;	
		if(organizationName != null && !organizationName.isEmpty()) {
			try {				
				
				
				
//				if(userIdentificationCodeEntity.getUser().getOrganization() != null) {
//					CustomerBrandingEntity customerBrandingEntity = customerBrandingRepository.findCustomerBrandingByOrgId(userIdentificationCodeEntity.getUser().getOrganization().getId());
//					if(customerBrandingEntity != null) {
//						if(pairMobileDeviceConfigEntity.isDisableQRCodeExpiry() == false) {
//							userIdentificationCodeEntity.setStatus(false);
//						}
//						customerBranding.setLogo(Base64.getEncoder().encodeToString(customerBrandingEntity.getLogo()));
//						customerBranding.setBackground(Base64.getEncoder().encodeToString(customerBrandingEntity.getBackground()));
//						customerBranding.setPolicyStatement(customerBrandingEntity.getPolicyStatement());
//						customerBranding.setLanguage(customerBrandingEntity.getLanguage());
//						
//						return customerBranding;
//						}
//				}
				
				CustomerBrandingEntity customerBrandingEntity = customerBrandingRepository.findCustomerBrandingByOrgName(organizationName);
				String[] b64Logo = new String(customerBrandingEntity.getLogo()).split(",");
				customerBranding.setLogo(b64Logo[1]);
				String[] b64Background = new String(customerBrandingEntity.getBackground()).split(",");
				customerBranding.setBackground(b64Background[1]);
				customerBranding.setPolicyStatement(customerBrandingEntity.getPolicyStatement());
				customerBranding.setLanguage(customerBrandingEntity.getLanguage());
				
				UserDeviceInfo userDeviceInfo =apduFeignClient.getOrgIssuerCertificate(0l,organizationName);
				
				if(userDeviceInfo !=  null) {
					customerBranding.setIssuerCertificate(userDeviceInfo.getEncodedCertificate());
					customerBranding.setIssuerPublicKey(userDeviceInfo.getEncodedPubKey());
					customerBranding.setIssuerKeyAlgorithm(userDeviceInfo.getKeyGenerationAlgrthm());
				}
			
			} catch (Exception  e) {
				log.error(e.getMessage());
			}
		}
		return customerBranding;
	}

}
