package com.meetidentity.usermanagement.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.VisualTemplate;

public interface VisualTemplateService {
	
	List<VisualTemplate> getVisualTemplates(String organizationName);
	
	ResponseEntity<Void> createVisualTemplate(String organizationName, VisualTemplate visualTemplate) throws Exception;
	
	VisualTemplate getVisualTemplateByName(String organizationName, String visualTemplatename);
	
	ResponseEntity<String> deleteVisualTemplateByName(String organizationName, String visualTemplatename);
	
	ResponseEntity<String> updateVisualTemplate(String organizationName, String visualTemplateName, VisualTemplate VisualTemplate) throws Exception;
	
	ResultPage getAllVisualTemplates(String organizationName, String sortBy, String  order, int page, int size);
}
