package com.meetidentity.usermanagement.service;

import java.util.List;

import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;

public interface IdentityProviderClientService {

	void addRedirectURIs(String organizationName, List<String> redirectURIs) throws IdentityBrokerServiceException;

	List<String> getRedirectURIs(String organizationName);

	void deleteRedirectURIs(String organizationName, String redirectURI);
	
}
