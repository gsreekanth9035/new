package com.meetidentity.usermanagement.service;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import com.meetidentity.usermanagement.domain.APIGatewayInfoEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.exception.PairDeviceInfoServiceException;
import com.meetidentity.usermanagement.web.rest.model.PairMobileDeviceConfig;
import com.meetidentity.usermanagement.web.rest.model.WebauthnRegisterationConfig;



public interface UserPairDeviceInfoService {
	
	PairMobileDeviceConfig generateInfo(String organizationName, String userName)  throws PairDeviceInfoServiceException;
	
	PairMobileDeviceConfig generateUserIdentifierToPairMobile(UserEntity userEntity)  throws PairDeviceInfoServiceException;

	PairMobileDeviceConfig generateUserIdentifierAsEnrollmentToken(UserEntity userEntity, String organizationName) throws PairDeviceInfoServiceException;
	
	PairMobileDeviceConfig generateUserIdentifierAsEnrollmentToken(String userEmail, String organizationName) throws PairDeviceInfoServiceException;

	PairMobileDeviceConfig generateUUIDToPairMobileWalletInviteToken(UserEntity userEntity, String organizationName) throws PairDeviceInfoServiceException;
		
	ResponseEntity<Void> sendPairMobileDeviceNotification(HttpHeaders httpHeaders, String organizationName, String userName, String appName)  throws PairDeviceInfoServiceException;

	PairMobileDeviceConfig generateQRCodeToPairMobileServiceApp(String organizationName, APIGatewayInfoEntity apiGatewayInfoEntity) throws PairDeviceInfoServiceException;

	WebauthnRegisterationConfig executeWebauthn(String organizationName, String userName);

}
