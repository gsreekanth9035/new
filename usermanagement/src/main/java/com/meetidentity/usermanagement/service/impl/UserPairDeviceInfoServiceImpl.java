package com.meetidentity.usermanagement.service.impl;

import java.time.Instant;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.zxing.BarcodeFormat;
import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.config.ApplicationProperties;
import com.meetidentity.usermanagement.config.Constants;
import com.meetidentity.usermanagement.domain.APIGatewayInfoEntity;
import com.meetidentity.usermanagement.domain.MobilePairPolicyConfigEntity;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.PairMobileDeviceConfigEntity;
import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.domain.UserIdentificationCodeEntity;
import com.meetidentity.usermanagement.enums.MobilePairPolicyConfigEnum;
import com.meetidentity.usermanagement.enums.UserStatusEnum;
import com.meetidentity.usermanagement.exception.EntityNotFoundException;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.PairDeviceInfoServiceException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.keycloak.domain.UserRepresentation;
import com.meetidentity.usermanagement.repository.APIGatewayInfoRepository;
import com.meetidentity.usermanagement.repository.MobilePairPolicyConfigRepository;
import com.meetidentity.usermanagement.repository.OrganizationRepository;
import com.meetidentity.usermanagement.repository.PairMobileDeviceConfigRepository;
import com.meetidentity.usermanagement.repository.UserIdentificationCodeRepository;
import com.meetidentity.usermanagement.repository.UserRepository;
import com.meetidentity.usermanagement.security.CryptoFunction;
import com.meetidentity.usermanagement.service.KeycloakService;
import com.meetidentity.usermanagement.service.UserPairDeviceInfoService;
import com.meetidentity.usermanagement.service.UserService;
import com.meetidentity.usermanagement.service.helper.QRCodeHelper;
import com.meetidentity.usermanagement.service.helper.SecretKeyLength;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.errors.QRCodeException;
import com.meetidentity.usermanagement.web.rest.model.Organization;
import com.meetidentity.usermanagement.web.rest.model.PairMobileDeviceConfig;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.WebauthnRegisterationConfig;
import com.meetidentity.usermanagement.web.rest.model.notification.NotificationServiceHelper;
import com.meetidentity.usermanagement.web.rest.util.HeaderUtil;

@Service
@Transactional
public class UserPairDeviceInfoServiceImpl implements UserPairDeviceInfoService {

	private final Logger log = LoggerFactory.getLogger(UserPairDeviceInfoServiceImpl.class);

	@Autowired
	private APIGatewayInfoRepository aPIGatewayInfoRepository;

	@Autowired
	UserIdentificationCodeRepository userIdentificationCodeRepository;
	
	@Autowired
	private APIGatewayInfoRepository apiGatewayInfoRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	CryptoFunction cryptoFunction;

	@Autowired
	private ApplicationProperties applicationProperties;

	@Autowired
	private PairMobileDeviceConfigRepository pairMobileDeviceConfigRepository;

	@Autowired
	private MobilePairPolicyConfigRepository mobilePairPolicyConfigRepository;
	
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private Util util;
	
	@Autowired
	private NotificationServiceHelper notificationServiceHelper;
	
	@Autowired
	private KeycloakService keycloakService;
	
	@Autowired
	private UserService userService;

	private APIGatewayInfoEntity getAPIGatewayInfoByOrgName(String orgName) {
		return aPIGatewayInfoRepository.getAPIGatewayInfoByOrgName(orgName);
	}

	/**
	 * This method will create UUID and store information in user identification
	 * code table UUID will get expire in 5 mints
	 * 
	 * @param userID
	 * @return
	 */
	@Override
	public PairMobileDeviceConfig generateUserIdentifierToPairMobile(UserEntity userEntity) throws PairDeviceInfoServiceException {
		try {
			PairMobileDeviceConfigEntity pairMobileDeviceConfigEntity = pairMobileDeviceConfigRepository
					.findPMDConfigByOrg(userEntity.getOrganization().getId());

			String uuid = null;
			if (pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength() == SecretKeyLength.THIRTYTWO
					.getValue()) {
				uuid = UUID.randomUUID().toString();
			} else if (pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength() < SecretKeyLength.THIRTYTWO
			  		.getValue()) {
				uuid = Util.getAlphaNumericString(pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength());
			}
			UserIdentificationCodeEntity userIdentificationCodeEntity = new UserIdentificationCodeEntity();
			userIdentificationCodeEntity.setUuid(uuid);
			userIdentificationCodeEntity.setUser(userEntity);
			userIdentificationCodeEntity.setStatus(true);
			userIdentificationCodeEntity.setValidityFrom(Instant.now());
			userIdentificationCodeEntity.setValidityTill(new Date(
					System.currentTimeMillis() + (pairMobileDeviceConfigEntity.getQrCodeExpiryInMins()) * 60 * 1000)
							.toInstant());

			userIdentificationCodeRepository.save(userIdentificationCodeEntity);
			// log.info(ApplicationConstants.notifyCreate,
			// Util.format(ErrorMessages.IDENTIFICATION_CODE_IS_ADDED_FOR_USER_WITH_USERNAME,
			// userEntity.getReferenceName()));

			PairMobileDeviceConfig pairMobileDeviceConfig = new PairMobileDeviceConfig(
					pairMobileDeviceConfigEntity.getGooglePlayStoreAppUrl(),
					pairMobileDeviceConfigEntity.getAppleAppStoreAppUrl(),
					pairMobileDeviceConfigEntity.getQrCodeExpiryInMins(), pairMobileDeviceConfigEntity.getQrCodeWidth(),
					pairMobileDeviceConfigEntity.getQrCodeHeight(), null,
					pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength(), uuid,
					pairMobileDeviceConfigEntity.getPairMobileDeviceUrl(),
					pairMobileDeviceConfigEntity.isEnableDemoVideo(), pairMobileDeviceConfigEntity.isDisableQRCodeExpiry(), pairMobileDeviceConfigEntity.getStatus());
			
			return pairMobileDeviceConfig;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_ADD_IDENTIFICATION_CODE_FOR_USER_WITH_USERNAME,
							userEntity.getReferenceName()),
					cause);
			throw new PairDeviceInfoServiceException(cause,
					ErrorMessages.FAILED_TO_ADD_IDENTIFICATION_CODE_FOR_USER_WITH_USERNAME,
					userEntity.getReferenceName());
		}
	}

	
	/**
	 * This method will create UUID and store information in user identification
	 * code table UUID will get expire in 5 mints
	 * 
	 * @param userID
	 * @return
	 */
	@Override
	public PairMobileDeviceConfig generateUserIdentifierAsEnrollmentToken(UserEntity userEntity, String organizationName) throws PairDeviceInfoServiceException {
		try {
			PairMobileDeviceConfigEntity pairMobileDeviceConfigEntity = pairMobileDeviceConfigRepository
					.findPMDConfigByOrg(userEntity.getOrganization().getId());
			MobilePairPolicyConfigEntity mobilePairPolicyConfigEntity = mobilePairPolicyConfigRepository.getEnrollmentTokenExpiry(organizationName, MobilePairPolicyConfigEnum.ENROLLMENT_TOKEN_EXPIRY.getConfigName());
			APIGatewayInfoEntity aPIGatewayInfoEntity = getAPIGatewayInfoByOrgName(organizationName);
			
			String uuid = null;
			if (pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength() == SecretKeyLength.THIRTYTWO
					.getValue()) {
				uuid = UUID.randomUUID().toString();
			} else if (pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength() < SecretKeyLength.THIRTYTWO
			  		.getValue()) {
				uuid = Util.getAlphaNumericString(pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength());
			}
			UserIdentificationCodeEntity userIdentificationCodeEntity = new UserIdentificationCodeEntity();
			userIdentificationCodeEntity.setUuid(uuid);
			userIdentificationCodeEntity.setUser(userEntity);
			userIdentificationCodeEntity.setStatus(true);
			userIdentificationCodeEntity.setValidityFrom(Instant.now());
			userIdentificationCodeEntity.setValidityTill(Instant.now().plusSeconds(Long.valueOf(mobilePairPolicyConfigEntity.getValue())));
			userIdentificationCodeRepository.save(userIdentificationCodeEntity);
							
			PairMobileDeviceConfig pairMobileDeviceConfig = new PairMobileDeviceConfig(
					pairMobileDeviceConfigEntity.getGooglePlayStoreAppUrl(),
					pairMobileDeviceConfigEntity.getAppleAppStoreAppUrl(),
					pairMobileDeviceConfigEntity.getQrCodeExpiryInMins(), pairMobileDeviceConfigEntity.getQrCodeWidth(),
					pairMobileDeviceConfigEntity.getQrCodeHeight(), null,
					pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength(), uuid,
					pairMobileDeviceConfigEntity.getPairMobileDeviceUrl(),
					pairMobileDeviceConfigEntity.isEnableDemoVideo(), pairMobileDeviceConfigEntity.isDisableQRCodeExpiry(), pairMobileDeviceConfigEntity.getStatus());
			
			pairMobileDeviceConfig.setQrCode(Base64.getEncoder()
					.encodeToString(new QRCodeHelper().getQRCodeImageWithLogo(
							qrDataFormatter(organizationName, aPIGatewayInfoEntity.getHost(),
									aPIGatewayInfoEntity.getPort(), pairMobileDeviceConfig.getSecretKey()),
							BarcodeFormat.QR_CODE, pairMobileDeviceConfig.getQrCodeWidth(), pairMobileDeviceConfig.getQrCodeHeight())));

			System.out.println("\n\n"+pairMobileDeviceConfig.getQrCode()+"\n\n");
			return pairMobileDeviceConfig;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_ADD_IDENTIFICATION_CODE_FOR_USER_WITH_USERNAME,
							userEntity.getReferenceName()),
					cause);
			throw new PairDeviceInfoServiceException(cause,
					ErrorMessages.FAILED_TO_ADD_IDENTIFICATION_CODE_FOR_USER_WITH_USERNAME,
					userEntity.getReferenceName());
		}
	}
	
	/**
	 * This method will create UUID and store information in user identification
	 * code table UUID will get expire in 5 mints
	 * 
	 * @param userID
	 * @return
	 */
	@Override
	public PairMobileDeviceConfig generateUserIdentifierAsEnrollmentToken(String userEmail, String organizationName) throws PairDeviceInfoServiceException {
		try {
			log.info("step1");
			//RFC 5322
			// Regex : ^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$
			
			String regex = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(userEmail.trim());
			if(!matcher.matches()) {
				return null;
			}
			log.info("step1-1");
			ResultPage resultPage =  keycloakService.getUsersBySearchCriteria(organizationName, userEmail, 0, 2);
			log.info("step2");
			List<UserRepresentation> userRepresentations = (List<UserRepresentation>) resultPage.getContent();
			log.info("step3");
			if (userRepresentations != null && !userRepresentations.isEmpty() && userRepresentations.size() == 1) {
				UserRepresentation userRepresentation = userRepresentations.get(0);
				log.info("step4");
				if(userRepresentation == null) {
					return null;
				}
				log.info("step5");
				
				UserEntity userEntity = userService.getUserEntityByUserName(organizationName, userRepresentation.getUsername());
				log.info("step6");
				if(userEntity == null) {
					return null;
				}
				log.info("step7");
				if(!userRepresentation.isEnabled() || !(userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.ENROLLMENT_IN_PROGRESS.name()) || userEntity.getStatus().equalsIgnoreCase(UserStatusEnum.PENDING_ENROLLMENT.name()))) {
					PairMobileDeviceConfig pairMobileDeviceConfig = new PairMobileDeviceConfig();
					pairMobileDeviceConfig.setStatus(false);
					return pairMobileDeviceConfig;
				}
				log.info("step8");
				PairMobileDeviceConfigEntity pairMobileDeviceConfigEntity = pairMobileDeviceConfigRepository
						.findPMDConfigByOrg(userEntity.getOrganization().getId());
				MobilePairPolicyConfigEntity mobilePairPolicyConfigEntity = mobilePairPolicyConfigRepository.getEnrollmentTokenExpiry(organizationName, MobilePairPolicyConfigEnum.ENROLLMENT_TOKEN_EXPIRY.getConfigName());
				APIGatewayInfoEntity aPIGatewayInfoEntity = getAPIGatewayInfoByOrgName(organizationName);
				
				String uuid = null;
				log.info("step9");
				if (pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength() == SecretKeyLength.THIRTYTWO
						.getValue()) {
					uuid = UUID.randomUUID().toString();
				} else if (pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength() < SecretKeyLength.THIRTYTWO
				  		.getValue()) {
					uuid = Util.getAlphaNumericString(pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength());
				}
				log.info("step10");
				UserIdentificationCodeEntity userIdentificationCodeEntity = new UserIdentificationCodeEntity();
				userIdentificationCodeEntity.setUuid(uuid);
				userIdentificationCodeEntity.setUser(userEntity);
				userIdentificationCodeEntity.setStatus(true);
				userIdentificationCodeEntity.setValidityFrom(Instant.now());
				userIdentificationCodeEntity.setValidityTill(Instant.now().plusSeconds(Long.valueOf(mobilePairPolicyConfigEntity.getValue())));
				userIdentificationCodeRepository.save(userIdentificationCodeEntity);
								
				log.info("step11");
				PairMobileDeviceConfig pairMobileDeviceConfig = new PairMobileDeviceConfig(
						pairMobileDeviceConfigEntity.getGooglePlayStoreAppUrl(),
						pairMobileDeviceConfigEntity.getAppleAppStoreAppUrl(),
						pairMobileDeviceConfigEntity.getQrCodeExpiryInMins(), pairMobileDeviceConfigEntity.getQrCodeWidth(),
						pairMobileDeviceConfigEntity.getQrCodeHeight(), null,
						pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength(), uuid,
						pairMobileDeviceConfigEntity.getPairMobileDeviceUrl(),
						pairMobileDeviceConfigEntity.isEnableDemoVideo(), pairMobileDeviceConfigEntity.isDisableQRCodeExpiry(), pairMobileDeviceConfigEntity.getStatus());
				
				log.info("step12");
				pairMobileDeviceConfig.setQrCode(Base64.getEncoder()
						.encodeToString(new QRCodeHelper().getQRCodeImageWithLogo(
								qrDataFormatter(organizationName, aPIGatewayInfoEntity.getHost(),
										aPIGatewayInfoEntity.getPort(), pairMobileDeviceConfig.getSecretKey()),
								BarcodeFormat.QR_CODE, pairMobileDeviceConfig.getQrCodeWidth(), pairMobileDeviceConfig.getQrCodeHeight())));
	
				System.out.println("\n\n"+pairMobileDeviceConfig.getQrCode()+"\n\n");
				log.info("step13");
				log.info("step14");
				return pairMobileDeviceConfig;
			}
		} catch (Exception cause) {
			log.info("step16");
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_ADD_IDENTIFICATION_CODE_FOR_USER_WITH_USERNAME,
							userEmail),
					cause);
			throw new PairDeviceInfoServiceException(cause,
					ErrorMessages.FAILED_TO_ADD_IDENTIFICATION_CODE_FOR_USER_WITH_USERNAME,
					userEmail);
		}
		log.info("step15");
		return null;
	}
	
	/**
	 * This method will create UUID and store information in user identification
	 * code table UUID will get expire in X mints
	 * 
	 * @param userID
	 * @return
	 */
	@Override
	public PairMobileDeviceConfig generateUUIDToPairMobileWalletInviteToken(UserEntity userEntity, String organizationName) throws PairDeviceInfoServiceException {
		try {
			PairMobileDeviceConfigEntity pairMobileDeviceConfigEntity = pairMobileDeviceConfigRepository
					.findPMDConfigByOrg(userEntity.getOrganization().getId());
			MobilePairPolicyConfigEntity mobilePairPolicyConfigEntity = mobilePairPolicyConfigRepository.getEnrollmentTokenExpiry(organizationName, MobilePairPolicyConfigEnum.PAIR_MOBILE_WALLET_INVITE_TOKEN_EXPIRY.getConfigName());
			APIGatewayInfoEntity aPIGatewayInfoEntity = getAPIGatewayInfoByOrgName(organizationName);
			
			String uuid = null;
			if (pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength() == SecretKeyLength.THIRTYTWO
					.getValue()) {
				uuid = UUID.randomUUID().toString();
			} else if (pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength() < SecretKeyLength.THIRTYTWO
			  		.getValue()) {
				uuid = Util.getAlphaNumericString(pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength());
			}
			UserIdentificationCodeEntity userIdentificationCodeEntity = new UserIdentificationCodeEntity();
			userIdentificationCodeEntity.setUuid(uuid);
			userIdentificationCodeEntity.setUser(userEntity);
			userIdentificationCodeEntity.setStatus(true);
			userIdentificationCodeEntity.setValidityFrom(Instant.now());
			userIdentificationCodeEntity.setValidityTill(Instant.now().plusSeconds(Long.valueOf(mobilePairPolicyConfigEntity.getValue())));
			userIdentificationCodeRepository.save(userIdentificationCodeEntity);
							
			PairMobileDeviceConfig pairMobileDeviceConfig = new PairMobileDeviceConfig(
					pairMobileDeviceConfigEntity.getGooglePlayStoreAppUrl(),
					pairMobileDeviceConfigEntity.getAppleAppStoreAppUrl(),
					pairMobileDeviceConfigEntity.getQrCodeExpiryInMins(), pairMobileDeviceConfigEntity.getQrCodeWidth(),
					pairMobileDeviceConfigEntity.getQrCodeHeight(), null,
					pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength(), uuid,
					pairMobileDeviceConfigEntity.getPairMobileDeviceUrl(),
					pairMobileDeviceConfigEntity.isEnableDemoVideo(), pairMobileDeviceConfigEntity.isDisableQRCodeExpiry(), pairMobileDeviceConfigEntity.getStatus());
			
			pairMobileDeviceConfig.setQrCode(Base64.getEncoder()
					.encodeToString(new QRCodeHelper().getQRCodeImageWithLogo(
							qrDataFormatter(organizationName, aPIGatewayInfoEntity.getHost(),
									aPIGatewayInfoEntity.getPort(), pairMobileDeviceConfig.getSecretKey()),
							BarcodeFormat.QR_CODE, pairMobileDeviceConfig.getQrCodeWidth(), pairMobileDeviceConfig.getQrCodeHeight())));

			System.out.println("\n\n"+pairMobileDeviceConfig.getQrCode()+"\n\n");
			return pairMobileDeviceConfig;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_ADD_IDENTIFICATION_CODE_FOR_USER_WITH_USERNAME,
							userEntity.getReferenceName()),
					cause);
			throw new PairDeviceInfoServiceException(cause,
					ErrorMessages.FAILED_TO_ADD_IDENTIFICATION_CODE_FOR_USER_WITH_USERNAME,
					userEntity.getReferenceName());
		}
	}
	
	@Override
	public PairMobileDeviceConfig generateQRCodeToPairMobileServiceApp(String organizationName, APIGatewayInfoEntity apiGatewayInfoEntity_) throws PairDeviceInfoServiceException {
		try {
			APIGatewayInfoEntity apiGatewayInfoEntity = apiGatewayInfoEntity_;
			if(apiGatewayInfoEntity == null) {
				apiGatewayInfoEntity =apiGatewayInfoRepository.getAPIGatewayInfoByOrgName(organizationName);
			}
			
			OrganizationEntity organizationEntity =  organizationRepository.findByName(organizationName);
			PairMobileDeviceConfigEntity pairMobileDeviceConfigEntity = pairMobileDeviceConfigRepository
					.findPMDConfigByOrg(organizationEntity.getId());
							
			PairMobileDeviceConfig pairMobileDeviceConfig = new PairMobileDeviceConfig(
					pairMobileDeviceConfigEntity.getGooglePlayStoreAppUrl(),
					pairMobileDeviceConfigEntity.getAppleAppStoreAppUrl(),
					pairMobileDeviceConfigEntity.getQrCodeExpiryInMins(), pairMobileDeviceConfigEntity.getQrCodeWidth(),
					pairMobileDeviceConfigEntity.getQrCodeHeight(), null,
					pairMobileDeviceConfigEntity.getPairMobileDeviceSecretKeyLength(), null,
					pairMobileDeviceConfigEntity.getPairMobileDeviceUrl(),
					pairMobileDeviceConfigEntity.isEnableDemoVideo(), false, pairMobileDeviceConfigEntity.getStatus());
			
			pairMobileDeviceConfig.setQrCode(Base64.getEncoder()
					.encodeToString(new QRCodeHelper().getQRCodeImageWithLogo(
							qrDataFormatterForMobileService(organizationName, apiGatewayInfoEntity.getHost(), apiGatewayInfoEntity.getPort()),
							BarcodeFormat.QR_CODE, pairMobileDeviceConfigEntity.getQrCodeWidth(), pairMobileDeviceConfigEntity.getQrCodeHeight())));

			System.out.println("\n\n"+pairMobileDeviceConfig.getQrCode()+"\n\n");
			return pairMobileDeviceConfig;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_GENERATE_PAIR_MOBILE_SERVICE_QRCODE_FOR_ORGANIZATION,
							organizationName),
					cause);
			throw new PairDeviceInfoServiceException(cause,
					ErrorMessages.FAILED_TO_GENERATE_PAIR_MOBILE_SERVICE_QRCODE_FOR_ORGANIZATION,
					organizationName);
		}
	}
	/**
	 * This method will generate Pair Device ( QR Code + Secret key with URL)
	 * Information and return base64 string
	 * 
	 * @throws QRCodeException
	 */
	@Override
	public PairMobileDeviceConfig generateInfo(String organizationName, String userName)
			throws PairDeviceInfoServiceException {
		PairMobileDeviceConfig pairMobileDeviceConfig = null;
		try {
			// String decryptedUuid = null;
			APIGatewayInfoEntity aPIGatewayInfoEntity = getAPIGatewayInfoByOrgName(organizationName);
			UserEntity userEntity = userRepository.getUserByName(userName, organizationName);
			if (aPIGatewayInfoEntity != null && userEntity != null) {
				String encryptedUuid;
				pairMobileDeviceConfig = generateUserIdentifierToPairMobile(userEntity);
				// encryptedUuid =
				// aesCrypter.encrypt(applicationProperties.getQrcode().getIvParam().getBytes(),
				// applicationProperties.getQrcode().getAesKey().getBytes(),
				// generateUserIdentifier(userEntity).getBytes());
				pairMobileDeviceConfig.setQrCode(Base64.getEncoder()
						.encodeToString(new QRCodeHelper().getQRCodeImageWithLogo(
								qrDataFormatter(organizationName, aPIGatewayInfoEntity.getHost(),
										aPIGatewayInfoEntity.getPort(), pairMobileDeviceConfig.getSecretKey()),
								BarcodeFormat.QR_CODE, pairMobileDeviceConfig.getQrCodeWidth(), pairMobileDeviceConfig.getQrCodeHeight())));

				log.info(ApplicationConstants.notifyCreate,
						Util.format(ErrorMessages.QRCODE_IS_ADDED_FOR_USER_WITH_USERNAME, userName));
				return pairMobileDeviceConfig;
			} else {
				log.error(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.INVALID_GATEWAY_INFO, organizationName));
				throw new EntityNotFoundException(Util.format(ErrorMessages.INVALID_GATEWAY_INFO, organizationName));
			}

		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_ADD_QRCODE_FOR_USER_WITH_USERNAME, userName), cause);
			throw new PairDeviceInfoServiceException(cause, ErrorMessages.FAILED_TO_ADD_QRCODE_FOR_USER_WITH_USERNAME,
					userName);
		}
	}

	private String qrDataFormatter(String orgName, String host, String port, String uuid) {
		return String.format(orgName + "://pair?%s#%s#%s", host, port, uuid);
	}
	
	private String qrDataFormatterForMobileService(String orgName, String host, String port) {
		return String.format(orgName + "://pair?%s#%s", host, port);
	}

	@Override
	public ResponseEntity<Void> sendPairMobileDeviceNotification(HttpHeaders httpHeaders, String organizationName, String userName, String appName)
			throws PairDeviceInfoServiceException {
		try {
			String loggedInUser = null;
			Long loggedInUserId = null;
			HeaderUtil.validateRequiredHeaders(httpHeaders, Arrays.asList(Constants.LOGGED_IN_USER, Constants.LOGGED_IN_USER_ID));
			if (httpHeaders.containsKey(Constants.LOGGED_IN_USER)) {
				loggedInUser = httpHeaders.get(Constants.LOGGED_IN_USER).get(0);
			}
			if (httpHeaders.containsKey(Constants.LOGGED_IN_USER_ID)) {
				loggedInUserId = Long.valueOf(httpHeaders.get(Constants.LOGGED_IN_USER_ID).get(0));
			}
			
			UserEntity userEntity = userRepository.getUserByName(userName, organizationName);
			log.info("send Pair device notification for " +appName +"app");
			
			int responseStatusCode = 0; 
			if (appName.equalsIgnoreCase(Constants.MOBILE_WALLET)) {
				responseStatusCode =  notificationServiceHelper.sendPairYourDeviceInviteNotification(
						organizationName, loggedInUser, loggedInUserId, userEntity);

			} else if(appName.equalsIgnoreCase(Constants.MOBILE_SERVICE)) {
				responseStatusCode = notificationServiceHelper.sendPairMobileDeviceNotificationForMSApp(
						organizationName, loggedInUser, loggedInUserId, userEntity);
			}
			
			if (responseStatusCode != HttpStatus.OK.value()) {
				log.error(ApplicationConstants.notifyAdmin,
						Util.format(ErrorMessages.FAILED_TO_SEND_NOTIFICATION_FOR_PAIR_MOBILE_WITH_USERNAME, userName));
				return new ResponseEntity<Void>(HttpStatus.CONFLICT);
			}
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin,
					Util.format(ErrorMessages.FAILED_TO_SEND_NOTIFICATION_FOR_PAIR_MOBILE_WITH_USERNAME, userName), cause);
			throw new PairDeviceInfoServiceException(cause, ErrorMessages.FAILED_TO_SEND_NOTIFICATION_FOR_PAIR_MOBILE_WITH_USERNAME,
					userName);
		}
	}

	@Override
	public WebauthnRegisterationConfig executeWebauthn(String organizationName, String userName) {
		try {
			return keycloakService.executeWebauthn(organizationName, userName);
		} catch (IdentityBrokerServiceException e) {
			return null;
		}
	}

}
