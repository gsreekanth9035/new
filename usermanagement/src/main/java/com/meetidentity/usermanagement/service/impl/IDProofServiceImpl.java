package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.domain.IDProofIssuingAuthorityEntity;
import com.meetidentity.usermanagement.domain.IDProofTypeEntity;
import com.meetidentity.usermanagement.repository.IDProofIssuingAuthorityRepository;
import com.meetidentity.usermanagement.repository.IDProofTypeRepository;
import com.meetidentity.usermanagement.service.IDProofService;
import com.meetidentity.usermanagement.web.rest.model.IDProofIssuingAuthority;
import com.meetidentity.usermanagement.web.rest.model.IDProofType;

@Service
public class IDProofServiceImpl implements IDProofService{

	@Autowired
	private IDProofTypeRepository idProofTypeRepository;
	
	@Autowired
	private IDProofIssuingAuthorityRepository idProofIssuingAuthorityRepository;
	
	@Override
	public List<IDProofType> getIDProofTypes(String organizationName) {
		List<IDProofType> idProofTypes = new ArrayList<>();
		List<IDProofTypeEntity> idProofTypeEntities = idProofTypeRepository.findAllByOrganization(organizationName);
		if(idProofTypeEntities != null) {
			idProofTypeEntities.forEach(idProofTypeEntity -> {
				IDProofType idProofType = new IDProofType();
				idProofType.setId(idProofTypeEntity.getId());
				idProofType.setType(idProofTypeEntity.getType());
				idProofTypes.add(idProofType);
			});
		}
		return idProofTypes;
	}

	@Override
	public List<IDProofIssuingAuthority> getIssuingAuthorities(String organizationName, Long idProofTypeId) {
		List<IDProofIssuingAuthority> idProofIssuingAuths = new ArrayList<>();
		List<IDProofIssuingAuthorityEntity> issuingAUthsEntities = idProofIssuingAuthorityRepository.getByIDProofTypeID1(organizationName, idProofTypeId);
		issuingAUthsEntities.forEach(issuAuth -> {
			IDProofIssuingAuthority idProofIssuingAuthority = new IDProofIssuingAuthority();
			idProofIssuingAuthority.setId(issuAuth.getId());
			idProofIssuingAuthority.setIssuingAuthority(issuAuth.getIssuingAuthority());
			
			idProofIssuingAuths.add(idProofIssuingAuthority);
		});
		return idProofIssuingAuths;
	}


}
