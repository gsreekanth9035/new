package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.domain.WFMobileIDStepCertConfigEntity;
import com.meetidentity.usermanagement.domain.WFMobileIdStepCertificatesEntity;
import com.meetidentity.usermanagement.domain.WFStepCertConfigEntity;
import com.meetidentity.usermanagement.domain.WFStepConfigEntity;
import com.meetidentity.usermanagement.repository.WFMobileIDStepCertConfigRepository;
import com.meetidentity.usermanagement.repository.WFMobileIdStepCertificatesRepository;
import com.meetidentity.usermanagement.repository.WFStepCertConfigRepository;
import com.meetidentity.usermanagement.repository.WFStepConfigRepository;
import com.meetidentity.usermanagement.service.WFStepCertConfigService;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIdStepCertificate;
import com.meetidentity.usermanagement.web.rest.model.WFStepCertConfig;

@Service
public class WFStepCertConfigServiceImpl implements WFStepCertConfigService {

	@Autowired
	private WFStepCertConfigRepository wFStepCertConfigRepository;

	@Autowired
	private WFStepConfigRepository wfStepConfigRepository;

	@Autowired
	private WFMobileIdStepCertificatesRepository wfMobileIdStepCertRepository;
	
	@Autowired
	private WFMobileIDStepCertConfigRepository wfMobileIDStepCertConfigRepository;

	@Override
	public List<WFStepCertConfig> getWorkflowCerts(String stepName, String workflowName, String orgName) {
		List<WFStepCertConfig> wfStepCertConfigList = new ArrayList<>();
		List<WFStepCertConfigEntity> certList = wFStepCertConfigRepository.findWorkflowCertificates1(stepName,
				workflowName, orgName);
		if (certList.size() != 0) {
			Long wfStepId = certList.get(0).getWfStep().getId();
			WFStepConfigEntity wfStepConfigEntity = wfStepConfigRepository.findWorkflowStepConfigByConfigName(wfStepId, "NOTIFY_USER_ABOUT_CERT_EXPIRING_IN_DAYS");
			certList.forEach(cert -> {
				WFStepCertConfig wfStepCertConfig = new WFStepCertConfig();

				wfStepCertConfig.setId(cert.getId());
				wfStepCertConfig.setCertType(cert.getCertType());
				wfStepCertConfig.setCertTemplate(cert.getCertTemplate());
				wfStepCertConfig.setCaServer(cert.getCaServer());
				wfStepCertConfig.setKeyEscrow(cert.getKeyEscrow());
				wfStepCertConfig.setDisableRevoke(cert.getDisableRevoke());
				wfStepCertConfig.setNoOfDaysBeforeCertExpiry(Integer.parseInt(wfStepConfigEntity.getConfigValue().getValue()));
				wfStepCertConfig.setAlgorithm(cert.getAlgorithm());
				wfStepCertConfig.setKeysize(cert.getKeySize());
				wfStepCertConfig.setSubjectDN(cert.getSubjectDN());
				wfStepCertConfig.setSubjectAN(cert.getSubjectAN());

				wfStepCertConfigList.add(wfStepCertConfig);
			});
		}

		return wfStepCertConfigList;
	}

	@Override
	public WFStepCertConfig getWorkflowCertConfig(Long Id) {
		WFStepCertConfig wfStepCertConfig = null;
		WFStepCertConfigEntity wfCertConfig =wFStepCertConfigRepository.findOne(Id);
		if(wfCertConfig!=null) {
			Long wfStepId = wfCertConfig.getWfStep().getId();
			WFStepConfigEntity wfStepConfigEntity = wfStepConfigRepository.findWorkflowStepConfigByConfigName(wfStepId, "NOTIFY_USER_ABOUT_CERT_EXPIRING_IN_DAYS");
			wfStepCertConfig = new WFStepCertConfig();
			wfStepCertConfig.setId(wfCertConfig.getId());
			wfStepCertConfig.setCertType(wfCertConfig.getCertType());
			wfStepCertConfig.setCertTemplate(wfCertConfig.getCertTemplate());
			wfStepCertConfig.setCaServer(wfCertConfig.getCaServer());
			wfStepCertConfig.setKeyEscrow(wfCertConfig.getKeyEscrow());
			wfStepCertConfig.setDisableRevoke(wfCertConfig.getDisableRevoke());
		    wfStepCertConfig.setNoOfDaysBeforeCertExpiry(Integer.parseInt(wfStepConfigEntity.getConfigValue().getValue()));
			wfStepCertConfig.setAlgorithm(wfCertConfig.getAlgorithm());
			wfStepCertConfig.setKeysize(wfCertConfig.getKeySize());
			wfStepCertConfig.setSubjectDN(wfCertConfig.getSubjectDN());
			wfStepCertConfig.setSubjectAN(wfCertConfig.getSubjectAN());
		}
		return wfStepCertConfig;
	}

	@Override
	public WFMobileIdStepCertificate getWorkflowMobileCertConfig(Long id) {
		WFMobileIdStepCertificate wfMobileIdStepCertificate = null;
		WFMobileIdStepCertificatesEntity wfMobileIdCertEntity= wfMobileIdStepCertRepository.findOne(id);
		if(wfMobileIdCertEntity!=null) {
			Long wfMobileIdIssuanceId = wfMobileIdCertEntity.getWfMobileIDIdentitiesIssuance().getId();
			WFMobileIDStepCertConfigEntity wfMobileIDStepCertConfigEntity = wfMobileIDStepCertConfigRepository.getOne(wfMobileIdIssuanceId);
			wfMobileIdStepCertificate = new WFMobileIdStepCertificate();
			wfMobileIdStepCertificate.setWfMobileIdStepCertId(wfMobileIdCertEntity.getId());
			wfMobileIdStepCertificate.setCertType(wfMobileIdCertEntity.getCertType());
			wfMobileIdStepCertificate.setCertTemplate(wfMobileIdCertEntity.getCertTemplate());
			wfMobileIdStepCertificate.setCaServer(wfMobileIdCertEntity.getCaServer());
			wfMobileIdStepCertificate.setKeyEscrow(wfMobileIdCertEntity.getKeyEscrow());
			wfMobileIdStepCertificate.setDisableRevoke(wfMobileIdCertEntity.getDisableRevoke());
			
			// wfMobileIdStepCertificate.setNoOfDaysBeforeCertExpiry(Integer.parseInt(wfMobileIDStepCertConfigEntity.getNoOfDaysBeforeCertExpiry().getValue()));

		    wfMobileIdStepCertificate.setAlgorithm(wfMobileIdCertEntity.getAlgorithm());
			wfMobileIdStepCertificate.setKeysize(wfMobileIdCertEntity.getKeySize());
			wfMobileIdStepCertificate.setSubjectDN(wfMobileIdCertEntity.getSubjectDN());
			wfMobileIdStepCertificate.setSubjectAN(wfMobileIdCertEntity.getSubjectAN());
		}
		return wfMobileIdStepCertificate;
	}
}
