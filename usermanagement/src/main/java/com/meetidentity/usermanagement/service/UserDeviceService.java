package com.meetidentity.usermanagement.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.meetidentity.usermanagement.exception.EntityNotFoundException;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.security.CryptoOperationException;
import com.meetidentity.usermanagement.web.rest.model.RoleDeviceAction;
import com.meetidentity.usermanagement.web.rest.model.UserDevice;

public interface UserDeviceService {
	
	ResponseEntity<Void> registerUserDevice(UserDevice userDevice )  throws CryptoOperationException, EntityNotFoundException;
	
	ResponseEntity<String> userKeycloakAuthentication(String ussername, String password) throws IdentityBrokerServiceException;
	
	ResponseEntity<Void> sendUserOtp(String mobileNumber);
	
	List<RoleDeviceAction> getDeviceActionsByRole(String organizationName, String role);

}
