package com.meetidentity.usermanagement.service;


import com.meetidentity.usermanagement.domain.PushNotificationConfigEntity;

public interface PushNotificationConfigService {
	
	PushNotificationConfigEntity findCofigurationsByType(String type);

}
