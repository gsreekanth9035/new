package com.meetidentity.usermanagement.service;

import java.util.List;
import java.util.Map;

import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.web.rest.model.UCard;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserIdentityData;
import com.meetidentity.usermanagement.web.rest.model.WFStepVisualTemplateConfig;

public interface UserCredentialsService {

	List<WFStepVisualTemplateConfig> getConfiguredCredentialTypes(String requestFrom, Long userId);

	UCard getUserCredentialsWithTemplateID(String requestFrom, Long userId, String stepID, String userData);

	UCard generateVisualCredential(String requestFrom, UserEntity userEntity, String userName, String organizationName, String credentialTemplateID, User user, boolean transparentPhoto);
	
	Map<Class<?>, Object> getGroupAndWorkflowByUserName(String organizationName, String userName);

	List<WFStepVisualTemplateConfig> getWorkflowVisualTemplates(String requestFrom, String orgName, String userName);

	List<UserIdentityData> getUserIdentityWithSerialNumberAndTemplateID(String organizationName, Long userID, String identitySerialNumber,
			Long credentialTemplateID);

	List<UserIdentityData> getUserAgeWithID(String organizationName, Long userID);
}
