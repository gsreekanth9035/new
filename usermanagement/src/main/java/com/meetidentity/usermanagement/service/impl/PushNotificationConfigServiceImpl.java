package com.meetidentity.usermanagement.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.meetidentity.usermanagement.domain.PushNotificationConfigEntity;
import com.meetidentity.usermanagement.repository.PushNotificationConfigRepository;
import com.meetidentity.usermanagement.service.PushNotificationConfigService;

public class PushNotificationConfigServiceImpl implements PushNotificationConfigService {

	@Autowired
	private PushNotificationConfigRepository pushNotificationConfigRepository;
	
	@Override
	public PushNotificationConfigEntity findCofigurationsByType(String type) {

		return pushNotificationConfigRepository.findCofigurationsByType(type);
	}

}
