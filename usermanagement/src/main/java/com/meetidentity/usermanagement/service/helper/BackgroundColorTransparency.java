package com.meetidentity.usermanagement.service.helper;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.meetidentity.usermanagement.domain.RGBOffsetEntity;
import com.meetidentity.usermanagement.repository.RGBOffsetRepository;

@Component
public class BackgroundColorTransparency {
	
	@Autowired
	private RGBOffsetRepository rgbOffsetRepository;
	

//	public static void main(String[] args) throws IOException {
//		String imagePath = "D:\\meetidentity_workspace\\Project_Documents\\Team\\lal\\folder";
//		File inFile = new File(imagePath, "22.png");
//		BackgroundColorTransparency at = new BackgroundColorTransparency();
//		BufferedImage image = ImageIO.read(inFile);
//		List<Integer[]> rgb_range = at.init(image, 70, 80, 60, 45, 35, 55);
//
//		Image transpImg2 = at.transformColorToTransparency(image, rgb_range.get(0)[0], rgb_range.get(0)[1],
//				rgb_range.get(0)[2], rgb_range.get(1)[0], rgb_range.get(1)[1], rgb_range.get(1)[2]);
//		BufferedImage resultImage2 = at.imageToBufferedImage(transpImg2, image.getWidth(), image.getHeight());
//
//		File outFile2 = new File(imagePath, "map_with_transparency2.png");
//		ImageIO.write(resultImage2, "PNG", outFile2);
//	}
	
	public BufferedImage generate(BufferedImage image, String organizationName) throws IOException {

		RGBOffsetEntity rgbOffsetEntity = rgbOffsetRepository.findRGBOffsetByOrg(organizationName);
		int i = 70; int j =80 ; int k = 60; int l = 45; int m = 35; int n = 55;
		if(rgbOffsetEntity.getStatus() == true) {
			i = rgbOffsetEntity.getRedOnset();
			j = rgbOffsetEntity.getGreenOnset();
			k = rgbOffsetEntity.getBlueOnset();
			l = rgbOffsetEntity.getRedEnd();
			m = rgbOffsetEntity.getGreenEnd();
			n = rgbOffsetEntity.getBlueEnd();
		}else {
			return image;
		}
		
		List<Integer[]> rgb_range = new ArrayList<Integer[]>();		
		// calculate range of background colors from image to be made as transparent
		int rgb = image.getRGB(10, 10);
		int r = (rgb & 0xFF0000) >> 16;
		int g = (rgb & 0xFF00) >> 8;
		int b = rgb & 0xFF;
		Integer[] rgb_starting_range = new Integer[3];
		rgb_starting_range[0] = ((r - i) < 0) ? 0 : r - i;
		rgb_starting_range[1] = ((g - j) < 0) ? 0 : g - j;
		rgb_starting_range[2] = ((b - k) < 0) ? 0 : b - k;
		rgb_range.add(rgb_starting_range);

		Integer[] rgb_ending_range = new Integer[3];
		rgb_ending_range[0] = ((r + l) > 255) ? 255 : r + l;
		rgb_ending_range[1] = ((g + m) > 255) ? 255 : g + m;
		rgb_ending_range[2] = ((b + n) > 255) ? 255 : b + n;
		rgb_range.add(rgb_ending_range);

		Image transpImg2 = transformColorToTransparency(image, rgb_range.get(0)[0], rgb_range.get(0)[1],
				rgb_range.get(0)[2], rgb_range.get(1)[0], rgb_range.get(1)[1], rgb_range.get(1)[2]);
		BufferedImage resultImage2 = imageToBufferedImage(transpImg2, image.getWidth(), image.getHeight());

		return resultImage2;
	}
	
	public BufferedImage generate_photoghost(BufferedImage image, String organizationName) throws IOException {

		RGBOffsetEntity rgbOffsetEntity = rgbOffsetRepository.findRGBOffsetByOrg(organizationName);
		int i = 70; int j =80 ; int k = 60; int l = 45; int m = 35; int n = 55;
		if(rgbOffsetEntity.getStatus() == true) {
			i = rgbOffsetEntity.getRedOnset();
			j = rgbOffsetEntity.getGreenOnset();
			k = rgbOffsetEntity.getBlueOnset();
			l = rgbOffsetEntity.getRedEnd();
			m = rgbOffsetEntity.getGreenEnd();
			n = rgbOffsetEntity.getBlueEnd();
		}else {
			return image;
		}
		
		List<Integer[]> rgb_range = new ArrayList<Integer[]>();		
		// calculate range of background colors from image to be made as transparent
		int rgb = image.getRGB(2, 2);
		int r = (rgb & 0xFF0000) >> 16;
		int g = (rgb & 0xFF00) >> 8;
		int b = rgb & 0xFF;
		Integer[] rgb_starting_range = new Integer[3];
		rgb_starting_range[0] = ((r - i) < 0) ? 0 : r - i;
		rgb_starting_range[1] = ((g - j) < 0) ? 0 : g - j;
		rgb_starting_range[2] = ((b - k) < 0) ? 0 : b - k;
		rgb_range.add(rgb_starting_range);

		Integer[] rgb_ending_range = new Integer[3];
		rgb_ending_range[0] = ((r + l) > 255) ? 255 : r + l;
		rgb_ending_range[1] = ((g + m) > 255) ? 255 : g + m;
		rgb_ending_range[2] = ((b + n) > 255) ? 255 : b + n;
		rgb_range.add(rgb_ending_range);

		Image transpImg2 = transformColorToTransparency(image, rgb_range.get(0)[0], rgb_range.get(0)[1],
				rgb_range.get(0)[2], rgb_range.get(1)[0], rgb_range.get(1)[1], rgb_range.get(1)[2]);
		BufferedImage resultImage2 = imageToBufferedImage(transpImg2, image.getWidth(), image.getHeight());

		return resultImage2;
	}
	

	public List<Integer[]> init(BufferedImage image, int i, int j, int k, int l, int m, int n) throws IOException {

		List<Integer[]> rgb_range = new ArrayList<Integer[]>();

		// calculate range of background colors from image to be made as transparent
		int rgb = image.getRGB(25, 25);
		int r = (rgb & 0xFF0000) >> 16;
		int g = (rgb & 0xFF00) >> 8;
		int b = rgb & 0xFF;
		Integer[] rgb_starting_range = new Integer[3];
		rgb_starting_range[0] = ((r - i) < 0) ? 0 : r - i;
		rgb_starting_range[1] = ((g - j) < 0) ? 0 : g - j;
		rgb_starting_range[2] = ((b - k) < 0) ? 0 : b - k;
		rgb_range.add(rgb_starting_range);

		Integer[] rgb_ending_range = new Integer[3];
		rgb_ending_range[0] = ((r + l) > 255) ? 255 : r + l;
		rgb_ending_range[1] = ((g + m) > 255) ? 255 : g + m;
		rgb_ending_range[2] = ((b + n) > 255) ? 255 : b + n;
		rgb_range.add(rgb_ending_range);

		System.out.println("rgb1::" + rgb_starting_range[0] + "," + rgb_starting_range[1] + "," + rgb_starting_range[2]
				+ "||||||rgb2::" + rgb_ending_range[0] + "," + rgb_ending_range[1] + "," + rgb_ending_range[2]);
		return rgb_range;
	}

	public Image transformColorToTransparency(BufferedImage image, int r1, int g1, int b1, int r2, int g2, int b2) {

		ImageFilter filter = new RGBImageFilter() {

			public final int filterRGB(int x, int y, int rgb) {

				int r = (rgb & 0xFF0000) >> 16;
				int g = (rgb & 0xFF00) >> 8;
				int b = rgb & 0xFF;
				if (r >= r1 && r <= r2 && g >= g1 && g <= g2 && b >= b1 && b <= b2) {
					// Set fully transparent but keep color
					return rgb & 0xFFFFFF;
				}
				return rgb;
			}
		};

		ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
		return Toolkit.getDefaultToolkit().createImage(ip);
	}

	public BufferedImage imageToBufferedImage(Image image, int width, int height) {
		BufferedImage dest = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = dest.createGraphics();
		g2.drawImage(image, 0, 0, null);
		g2.dispose();
		return dest;
	}
}