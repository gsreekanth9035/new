package com.meetidentity.usermanagement.service;

import java.util.List;

import com.meetidentity.usermanagement.web.rest.model.DeviceType;

public interface DeviceTypeService {
	
	List<DeviceType> getVisualApplicableDeviceTypes();
}
