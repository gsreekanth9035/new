package com.meetidentity.usermanagement.service.helper;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.meetidentity.usermanagement.config.ApplicationProperties;
import com.meetidentity.usermanagement.config.Constants;
import com.meetidentity.usermanagement.config.ApplicationProperties.KeycloakConfiguration;
import com.meetidentity.usermanagement.web.rest.model.TokenDetails;


public class KeycloakServiceHelper {
	
	public static String getKeycloakAdminAccessToken(RestTemplate restTemplate, ApplicationProperties applicationProperties, String organizationName) throws NullPointerException, RestClientException {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		
		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(createRequestBodyToRetrieveAdminToken(applicationProperties, organizationName), httpHeaders);
		
		// TODO handle Exceptions
		ResponseEntity<TokenDetails> response = restTemplate.postForEntity(getKeycloakTokenUri(Constants.ADMIN_REALM, applicationProperties), requestEntity, TokenDetails.class);
		
		return response.getBody().getAccessToken();
	}
	
	public static String[] getKeycloakRefreshAccessTokens(RestTemplate restTemplate, ApplicationProperties applicationProperties, String organizationName) throws NullPointerException, RestClientException {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		
		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(createRequestBodyToRetrieveToken(applicationProperties, organizationName), httpHeaders);
		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		
		// TODO handle Exceptions
		ResponseEntity<TokenDetails> response = restTemplate.postForEntity(
				getKeycloakTokenUri(keycloakConfiguration.getRealm(), applicationProperties), requestEntity, TokenDetails.class);
		String[] tokens = {response.getBody().getAccessToken(), response.getBody().getRefreshToken()};
		return tokens;
	}
	public static TokenDetails getKeycloakMobileIDUserRefreshAccessTokens(RestTemplate restTemplate, ApplicationProperties applicationProperties, String organizationName) throws NullPointerException, RestClientException {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		
		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(getMobileIDUserToken(applicationProperties, organizationName), httpHeaders);
		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		
		// TODO handle Exceptions
		ResponseEntity<TokenDetails> response = restTemplate.postForEntity(
				getKeycloakTokenUri(keycloakConfiguration.getRealm(), applicationProperties), requestEntity, TokenDetails.class);
		return response.getBody();
	}
	public static String getKeycloakAccessToken(RestTemplate restTemplate, ApplicationProperties applicationProperties, String organizationName) throws NullPointerException, RestClientException {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		
		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(createRequestBodyToRetrieveToken(applicationProperties, organizationName), httpHeaders);
		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		
		// TODO handle Exceptions
		ResponseEntity<TokenDetails> response = restTemplate.postForEntity(
				getKeycloakTokenUri(keycloakConfiguration.getRealm(), applicationProperties), requestEntity, TokenDetails.class);
		return response.getBody().getAccessToken();
	}

	private static MultiValueMap<String, String> createRequestBodyToRetrieveAdminToken(ApplicationProperties applicationProperties, String organizationName) {
		MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();
		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		requestBody.add(Constants.GRANT_TYPE, keycloakConfiguration.getGrantType());
		requestBody.add(Constants.USERNAME, keycloakConfiguration.getUsername());
		requestBody.add(Constants.PASSWORD, keycloakConfiguration.getPassword());
		requestBody.add(Constants.CLIENT_ID, Constants.ADMIN_CLIENT_ID);
		return requestBody;
	}
	
	public static MultiValueMap<String, String> createRequestBodyToUserLogin(ApplicationProperties applicationProperties, String organizationName) {
		MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();
		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		requestBody.add(Constants.GRANT_TYPE, keycloakConfiguration.getGrantType());
		requestBody.add(Constants.USERNAME, keycloakConfiguration.getUsername());
		requestBody.add(Constants.PASSWORD, keycloakConfiguration.getPassword());
		requestBody.add(Constants.CLIENT_ID, keycloakConfiguration.getClientId());
		requestBody.add(Constants.CLIENT_SECRET, keycloakConfiguration.getClientSecret());
		return requestBody;
	}

	private static MultiValueMap<String, String> createRequestBodyToRetrieveToken(ApplicationProperties applicationProperties, String organizationName) {
		MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();
		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		requestBody.add(Constants.GRANT_TYPE, keycloakConfiguration.getGrantType());
		requestBody.add(Constants.USERNAME, keycloakConfiguration.getUsername());
		requestBody.add(Constants.PASSWORD, keycloakConfiguration.getPassword());
		requestBody.add(Constants.CLIENT_ID, keycloakConfiguration.getClientId());
		requestBody.add(Constants.CLIENT_SECRET, keycloakConfiguration.getClientSecret());
		return requestBody;
	}
	
	private static MultiValueMap<String, String> getMobileIDUserToken(ApplicationProperties applicationProperties, String organizationName) {
		MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();
		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		requestBody.add(Constants.GRANT_TYPE, keycloakConfiguration.getGrantType());
		requestBody.add(Constants.USERNAME, keycloakConfiguration.getMobileUserName());
		requestBody.add(Constants.PASSWORD, keycloakConfiguration.getMobileUserPassword());
		requestBody.add(Constants.CLIENT_ID, keycloakConfiguration.getClientId());
		requestBody.add(Constants.CLIENT_SECRET, keycloakConfiguration.getClientSecret());
		return requestBody;
	}
	
	public static MultiValueMap<String, String> createRequestBodyToUserLogout(ApplicationProperties applicationProperties, String organizationName, String refreshToken) {
		MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();
		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		requestBody.add(Constants.REFRESH_TOKEN, refreshToken);
		requestBody.add(Constants.CLIENT_ID, keycloakConfiguration.getClientId());
		requestBody.add(Constants.CLIENT_SECRET, keycloakConfiguration.getClientSecret());
		return requestBody;
	}

	private static String getKeycloakTokenUri(String realm, ApplicationProperties applicationProperties) {
		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(realm);
		return new StringBuilder(keycloakConfiguration.getBasePath())
				.append("/realms/")
				.append(realm)
				.append("/protocol/openid-connect/token").toString();
	}
	
	public static String getKeycloakTokenUri(ApplicationProperties applicationProperties, String organizationName) {
		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		return new StringBuilder(keycloakConfiguration.getBasePath())
				.append("/realms/")
				.append(keycloakConfiguration.getRealm())
				.append("/protocol/openid-connect/token").toString();
	}
	
	
	public static String getKeycloakLogoutUri(ApplicationProperties applicationProperties,String userID, String organizationName) {
		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		return new StringBuilder(keycloakConfiguration.getBasePath())
				.append("/realms/")
				.append(keycloakConfiguration.getRealm())
				.append("/users/").append(userID+"/logout").toString();
	}
	
	public static String getKeycloakLogoutNewAccessTokenUri(ApplicationProperties applicationProperties, String organizationName) {
		KeycloakConfiguration keycloakConfiguration = applicationProperties.getKeycloak(organizationName);
		return new StringBuilder(keycloakConfiguration.getBasePath())
				.append("/realms/")
				.append(keycloakConfiguration.getRealm())
				.append("/protocol/openid-connect/logout").toString();
	}

}
