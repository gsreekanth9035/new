package com.meetidentity.usermanagement.service.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

import com.meetidentity.usermanagement.web.feign.client.NotificationServiceFeignClient;
import com.meetidentity.usermanagement.web.rest.model.notification.DeliverReqBean;

public class NotificationTask implements Runnable {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationTask.class);


	DeliverReqBean deliverReqBean;
	HttpHeaders headers = new HttpHeaders();
	NotificationServiceFeignClient nsFeignClient;
	public NotificationTask(DeliverReqBean deliverReqBean, HttpHeaders headers, NotificationServiceFeignClient nsFeignClient) {
		super();
		this.deliverReqBean = deliverReqBean;
		this.headers = headers;
		this.nsFeignClient = nsFeignClient;
	}

	@Override
	public void run() {
		try {
			LOGGER.info("Thread Name b: " + Thread.currentThread().getName());
			nsFeignClient.deliver(headers, deliverReqBean);
			LOGGER.info("Thread Name a: " + Thread.currentThread().getName());
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
			e.printStackTrace();
		}
	}
}