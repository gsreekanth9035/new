package com.meetidentity.usermanagement.service.helper;

public enum Colors {

	 BLUE(0xFF40BAD0),
     RED(0xFFFF0000),
     PURPLE(0xFF8A4F9E),
     ORANGE(0xFFFF9800),
     WHITE(0xFFFFFFFF),
     BLACK(0xFF000000),
     GREEN(0xFF21D13B),
	 YELLOW(0xFFCACA0A),
	 GTO_BLUE(0xFF0048BB);
     private final int argb;

     Colors(final int argb){
         this.argb = argb;
     }

     public int getArgb(){
         return argb;
     }
}
