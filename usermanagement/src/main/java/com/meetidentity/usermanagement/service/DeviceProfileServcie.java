package com.meetidentity.usermanagement.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.meetidentity.usermanagement.domain.DeviceProfileEntity;
import com.meetidentity.usermanagement.exception.DeviceProfileServiceException;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfile;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfileConfig;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;

public interface DeviceProfileServcie {

	List<DeviceProfile> getDeviceProfiles(String organizationName) throws DeviceProfileServiceException;
	
	ResultPage getDeviceProfilesList(String organizationName, String sortBy, String  order, int page, int size) throws DeviceProfileServiceException;
	
	DeviceProfile getDeviceProfileById(Long deviceProfileID) throws DeviceProfileServiceException;

	ResponseEntity<String> saveDeviceProfile(String organizationName,DeviceProfile deviceProfile) throws DeviceProfileServiceException;

	ResponseEntity<Void> deleteDeviceProfile(String organizationName, Long devProfileID) throws DeviceProfileServiceException;

	ResponseEntity<String> updateDeviceProfile(Long devProfileID, DeviceProfile deviceProfile) throws DeviceProfileServiceException;
	
	DeviceProfile getDevProfFromEntity(DeviceProfileEntity deviceProfileEntity) throws DeviceProfileServiceException;

	String getDevProfName(Long devProfileID) throws DeviceProfileServiceException;
	
	DeviceProfileConfig getDeviceProfileConfigs(Long productValueId);
}
