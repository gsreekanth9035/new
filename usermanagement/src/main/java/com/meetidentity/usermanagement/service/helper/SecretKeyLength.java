package com.meetidentity.usermanagement.service.helper;

public enum SecretKeyLength {

	SIX(1), EIGHT(8), TEN(10), SIXTEEN(16), THIRTYTWO(32);

	private int value;

	private SecretKeyLength(int value) {
	        this.value = value;
	    }

	public int getValue() {
		return value;
	}

}
