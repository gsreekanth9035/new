package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.domain.OrgIdentityFieldConfigEntity;
import com.meetidentity.usermanagement.domain.OrganizationIdentitiesEntity;
import com.meetidentity.usermanagement.domain.VisualTemplateEntity;
import com.meetidentity.usermanagement.repository.OrgIdentityFieldConfigRepository;
import com.meetidentity.usermanagement.repository.OrganizationIdentitiesRepository;
import com.meetidentity.usermanagement.repository.VisualTemplateRepository;
import com.meetidentity.usermanagement.service.IdentityTypeService;
import com.meetidentity.usermanagement.web.rest.model.ConfigValue;
import com.meetidentity.usermanagement.web.rest.model.DeviceType;
import com.meetidentity.usermanagement.web.rest.model.OrganizationIdentity;
import com.meetidentity.usermanagement.web.rest.model.VisualTemplate;

@Transactional
@Service
public class IdentityTypeServiceImpl implements IdentityTypeService {

	@Autowired
	private OrganizationIdentitiesRepository orgIdentityTypeRepository;
	
	@Autowired
	private OrgIdentityFieldConfigRepository orgIdentityFieldConfigRepository;
	
	@Autowired
	private VisualTemplateRepository visualTemplateRepository;

	@Override
	public List<OrganizationIdentity> getOrgIssuingIdentityTypes(String organizationName) {
		List<OrganizationIdentity> orgIdentities = new ArrayList<>();
		List<OrganizationIdentitiesEntity> identityTypeEntities = orgIdentityTypeRepository.getOrgIssuingIdentities(organizationName, true);
		if (identityTypeEntities != null) {
			identityTypeEntities.forEach(orgIdentityTypeEntity -> {
				OrganizationIdentity identityType = new OrganizationIdentity();
				identityType.setId(orgIdentityTypeEntity.getId());
				identityType.setIdentityTypeName(orgIdentityTypeEntity.getIdentityType().getName());
				identityType.setIdentityTypeDetails(orgIdentityTypeEntity.getIdentityType().getNotes());
				orgIdentities.add(identityType);
			});
		}
		return orgIdentities;
	}

	@Override
	public List<OrganizationIdentity> getOrgTrustedIdentityTypes(String organizationName) {
		List<OrganizationIdentity> orgIdentities = new ArrayList<>();
		List<OrganizationIdentitiesEntity> identityTypeEntities = orgIdentityTypeRepository.getOrgTrustedExistingIdentities(organizationName, true);
		if (identityTypeEntities != null) {
			identityTypeEntities.forEach(orgIdentityTypeEntity -> {
				OrganizationIdentity identityType = new OrganizationIdentity();
				identityType.setId(orgIdentityTypeEntity.getId());
				identityType.setIdentityTypeName(orgIdentityTypeEntity.getIdentityType().getName());
				identityType.setIdentityTypeDetails(orgIdentityTypeEntity.getIdentityType().getNotes());
				identityType.setFileFormats(orgIdentityTypeEntity.getIdentityType().getFileFormats());
				orgIdentities.add(identityType);
			});
		}
		return orgIdentities;
	}
	
	@Override
	public List<ConfigValue> getIdentityFieldConfigs(String organizationName, String identityTypeName, String identityFieldName) {
		List<ConfigValue> configValues = new ArrayList<>();
		List<OrgIdentityFieldConfigEntity> fieldConfigs = orgIdentityFieldConfigRepository.findOrgFieldConfig(organizationName, identityTypeName, identityFieldName);
		if (fieldConfigs != null) {
			fieldConfigs.forEach(field -> {
				ConfigValue configValue = new ConfigValue();
				configValue.setId(field.getConfigValue().getId());
				configValue.setValue(field.getConfigValue().getValue());
				configValues.add(configValue);
			});
		}
		return configValues;
	}

	@Override
	public List<VisualTemplate> getVisualTemplatesByIdentityModel(String organizationName, String identityTypeName) {
		List<VisualTemplate> visualTemplates = new ArrayList<>();
		List<VisualTemplateEntity> visualTemplateEntities = visualTemplateRepository.fingVisualTemplateByIdentityType(organizationName, identityTypeName);
		if (visualTemplateEntities.size() > 0) {
			visualTemplateEntities.forEach(visualTemplateEntity -> {
				VisualTemplate visualTemplate = new VisualTemplate();
				visualTemplate.setId(visualTemplateEntity.getId());
				visualTemplate.setName(visualTemplateEntity.getName());
				List<DeviceType> deviceTypes = new ArrayList<>();
				visualTemplateEntity.getVisualTemplateDevices().forEach(deviceTypeEntity -> {
					DeviceType deviceType = new DeviceType();
					deviceType.setName(deviceTypeEntity.getDeviceType().getName());
					deviceTypes.add(deviceType);
					visualTemplate.getDeviceTypes().add(deviceType);
				});
				visualTemplates.add(visualTemplate);
			});
		}
		return visualTemplates;
	}

}
