package com.meetidentity.usermanagement.service;

import java.util.List;

import com.meetidentity.usermanagement.web.rest.model.IDProofIssuingAuthority;
import com.meetidentity.usermanagement.web.rest.model.IDProofType;

public interface IDProofService {
 
	List<IDProofType> getIDProofTypes(String organizationName);
	
	List<IDProofIssuingAuthority> getIssuingAuthorities(String organizationName, Long idProofTypeId);

}
