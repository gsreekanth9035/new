package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meetidentity.usermanagement.domain.OrganizationAdvertisementEntity;
import com.meetidentity.usermanagement.exception.security.CryptoOperationException;
import com.meetidentity.usermanagement.repository.OrganizationAdvertisementRepository;
import com.meetidentity.usermanagement.service.OrganizationAdvertisementService;
import com.meetidentity.usermanagement.web.rest.model.OrganizationAdvertisementInfo;

@Service
@Transactional
public class OrganizationAdvertisementServiceImpl implements OrganizationAdvertisementService {

	private final Logger log = LoggerFactory.getLogger(OrganizationAdvertisementServiceImpl.class);

	@Autowired
	OrganizationAdvertisementRepository organizationAdvertisementRepository;

	@Override
	public List<OrganizationAdvertisementInfo> getOrganizationAdvertisements(String organizationName)
			throws CryptoOperationException {
		log.info("Enter getOrganizationAdvertisements orgname: "+organizationName);
		List<OrganizationAdvertisementInfo> organizationAdvertisementInfoList = new ArrayList<OrganizationAdvertisementInfo>();
		if (organizationName != null && !organizationName.isEmpty()) {
			try {
				List<OrganizationAdvertisementEntity> organizationAdvertisementEntityList = organizationAdvertisementRepository
						.findrganizationAdvertisementByOrgName(organizationName);

				log.info("getOrganizationAdvertisements organizationAdvertisementEntityList: "+organizationAdvertisementEntityList);
				for (OrganizationAdvertisementEntity organizationAdvertisementEntity : organizationAdvertisementEntityList) {
					String[] b64Ad = new String(organizationAdvertisementEntity.getAdvertisementImage())
							.split(",");
					organizationAdvertisementInfoList.add(new OrganizationAdvertisementInfo(organizationAdvertisementEntity.getUimsId(), 
							organizationName, 
							organizationAdvertisementEntity.getAdvertisementName(), 
							b64Ad[1], 
							organizationAdvertisementEntity.getAdvertisementText(), 
							organizationAdvertisementEntity.getAdvertisementHyperLink(),
							organizationAdvertisementEntity.getAdvertisementCondition()));
				}
				log.info("getOrganizationAdvertisements organizationAdvertisementInfoList: "+organizationAdvertisementInfoList);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		log.info("return getOrganizationAdvertisements organizationAdvertisementInfoList: "+organizationAdvertisementInfoList);
		return organizationAdvertisementInfoList;
	}

}
