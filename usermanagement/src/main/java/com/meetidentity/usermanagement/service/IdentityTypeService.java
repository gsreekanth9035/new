package com.meetidentity.usermanagement.service;

import java.util.List;

import com.meetidentity.usermanagement.web.rest.model.ConfigValue;
import com.meetidentity.usermanagement.web.rest.model.OrganizationIdentity;
import com.meetidentity.usermanagement.web.rest.model.VisualTemplate;

public interface IdentityTypeService {

	List<OrganizationIdentity> getOrgIssuingIdentityTypes(String organizationName);

	List<OrganizationIdentity> getOrgTrustedIdentityTypes(String organizationName);
	
	List<ConfigValue> getIdentityFieldConfigs(String organizationName, String identityTypeName, String identityFieldName);
	
	List<VisualTemplate> getVisualTemplatesByIdentityModel(String organizationName, String identityTypeName);
}
