package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.domain.FingerprintDeviceConfigEntity;
import com.meetidentity.usermanagement.repository.FingerprintDeviceConfigRepository;
import com.meetidentity.usermanagement.service.FingerprintDeviceConfigService;
import com.meetidentity.usermanagement.web.rest.model.FingerprintDeviceConfig;

@Service
public class FingerprintDeviceConfigServiceimpl implements FingerprintDeviceConfigService{
	
	@Autowired
	private FingerprintDeviceConfigRepository fingerprintDeviceConfigRepository;

	@Override
	public ResponseEntity<FingerprintDeviceConfig> getDetailByDeviceName(String organizationName, String deviceName, String deviceManufacturer) {

		if(deviceManufacturer.equalsIgnoreCase("Secugen")&&deviceName.contains("SG_DEV")) {
			deviceName="SG_DEV";
		}		
		FingerprintDeviceConfigEntity fingerprintDeviceConfigEntity = fingerprintDeviceConfigRepository.getByDeviceName(deviceName, organizationName, deviceManufacturer);
		if (fingerprintDeviceConfigEntity != null) {
			FingerprintDeviceConfig fingerprintDeviceConfig = new FingerprintDeviceConfig();
			fingerprintDeviceConfig.setId(fingerprintDeviceConfigEntity.getId());
			fingerprintDeviceConfig.setOrganization(fingerprintDeviceConfigEntity.getOrganization());
			fingerprintDeviceConfig.setDeviceName(fingerprintDeviceConfigEntity.getDeviceName());
			fingerprintDeviceConfig.setDeviceManufacturer(fingerprintDeviceConfigEntity.getDeviceManufacturer());
			fingerprintDeviceConfig.setCaptureType(fingerprintDeviceConfigEntity.getCaptureType());
			fingerprintDeviceConfig.setRollSupport(fingerprintDeviceConfigEntity.getRollSupport());
			fingerprintDeviceConfig.setFlatSupport(fingerprintDeviceConfigEntity.getFlatSupport());
			fingerprintDeviceConfig.setAnsiTemplateSupport(fingerprintDeviceConfigEntity.getAnsiTemplateSupport());
			fingerprintDeviceConfig.setIsoTemplateSupport(fingerprintDeviceConfigEntity.getIsoTemplateSupport());

			return new ResponseEntity<FingerprintDeviceConfig>(fingerprintDeviceConfig, HttpStatus.OK);
		} else {

			return new ResponseEntity<FingerprintDeviceConfig>(HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public List<FingerprintDeviceConfig> getListOfDeviceByDeviceManufacturer(String organizationName,String deviceManufacturer) {
		List<FingerprintDeviceConfig> FingerprintDeviceConfigs = new ArrayList<>();

		List<FingerprintDeviceConfigEntity> fingerprintDeviceConfigEntitys = fingerprintDeviceConfigRepository.getByDeviceManufacturer(deviceManufacturer, organizationName);

		fingerprintDeviceConfigEntitys.stream().forEach(fingerprintDeviceConfigEntity -> {
			FingerprintDeviceConfig fingerprintDeviceConfig = new FingerprintDeviceConfig();
			fingerprintDeviceConfig.setId(fingerprintDeviceConfigEntity.getId());
			fingerprintDeviceConfig.setOrganization(fingerprintDeviceConfigEntity.getOrganization());
			fingerprintDeviceConfig.setDeviceName(fingerprintDeviceConfigEntity.getDeviceName());
			fingerprintDeviceConfig.setDeviceManufacturer(fingerprintDeviceConfigEntity.getDeviceManufacturer());
			fingerprintDeviceConfig.setCaptureType(fingerprintDeviceConfigEntity.getCaptureType());
			fingerprintDeviceConfig.setRollSupport(fingerprintDeviceConfigEntity.getRollSupport());
			fingerprintDeviceConfig.setFlatSupport(fingerprintDeviceConfigEntity.getFlatSupport());
			fingerprintDeviceConfig.setAnsiTemplateSupport(fingerprintDeviceConfigEntity.getAnsiTemplateSupport());
			fingerprintDeviceConfig.setIsoTemplateSupport(fingerprintDeviceConfigEntity.getIsoTemplateSupport());
			FingerprintDeviceConfigs.add(fingerprintDeviceConfig);
		});
		return FingerprintDeviceConfigs;
	}

}
