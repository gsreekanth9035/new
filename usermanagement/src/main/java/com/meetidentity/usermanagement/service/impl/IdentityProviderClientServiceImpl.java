package com.meetidentity.usermanagement.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.service.IdentityProviderClientService;
import com.meetidentity.usermanagement.service.KeycloakService;
import com.meetidentity.usermanagement.util.Util;

@Service
@Transactional
public class IdentityProviderClientServiceImpl implements IdentityProviderClientService {

	private final Logger log = LoggerFactory.getLogger(IdentityProviderClientServiceImpl.class);

	@Autowired
	private KeycloakService keycloakService;

	@Override
	public void addRedirectURIs(String organizationName, List<String> redirectURIs) {
		try {
			log.info("inside addRedirectURIs : " + redirectURIs);
			keycloakService.addRedirectURIs(organizationName, redirectURIs);
			log.info("exit addRedirectURIs");
			return;

		} catch (Exception e) {
			log.error(ApplicationConstants.notifySearch, Util.format(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION));
			throw new IdentityBrokerServiceException(Util.format(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION), e);
		}
	}

	@Override
	public List<String> getRedirectURIs(String organizationName) {
		try {
			log.info("inside getRedirectURIs");
			return keycloakService.getRedirectURIs(organizationName);

		} catch (Exception e) {
			log.error(ApplicationConstants.notifySearch, Util.format(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION));
			throw new IdentityBrokerServiceException(Util.format(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION), e);
		}
	}

	@Override
	public void deleteRedirectURIs(String organizationName, String redirectURI) {
		try {
			log.info("inside deleteRedirectURIs : " + redirectURI);
			keycloakService.deleteRedirectURIs(organizationName, redirectURI);
			log.info("exit deleteRedirectURIs");
			return;

		} catch (Exception e) {
			log.error(ApplicationConstants.notifySearch, Util.format(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION));
			throw new IdentityBrokerServiceException(Util.format(ErrorMessages.IDENTITYBROKER_SERVER_EXCEPTION), e);
		}
	}

}
