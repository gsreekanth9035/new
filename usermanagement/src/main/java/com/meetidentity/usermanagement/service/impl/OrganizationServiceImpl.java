package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.domain.OrganizationBrandingPolicyConfigEntity;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.RGBOffsetEntity;
import com.meetidentity.usermanagement.repository.AffiliationColorRepository;
import com.meetidentity.usermanagement.repository.BloodTypeRepository;
import com.meetidentity.usermanagement.repository.CardholderRankRepository;
import com.meetidentity.usermanagement.repository.DrivingLicenseEndorsementRepository;
import com.meetidentity.usermanagement.repository.EmployeeAffiliationRepository;
import com.meetidentity.usermanagement.repository.EnrollmentApproveRejectReasonsRepository;
import com.meetidentity.usermanagement.repository.GenderRepository;
import com.meetidentity.usermanagement.repository.HealthGroupPlanRepository;
import com.meetidentity.usermanagement.repository.OrganizationDepartmentRepository;
import com.meetidentity.usermanagement.repository.OrganizationRepository;
import com.meetidentity.usermanagement.repository.PhysicalCCEyeRepository;
import com.meetidentity.usermanagement.repository.PhysicalCCHairRepository;
import com.meetidentity.usermanagement.repository.PhysicalCCHeightRepository;
import com.meetidentity.usermanagement.repository.RGBOffsetRepository;
import com.meetidentity.usermanagement.service.OrganizationService;
import com.meetidentity.usermanagement.web.rest.model.EnrollmentApproveRejectReasons;
import com.meetidentity.usermanagement.web.rest.model.Organization;
import com.meetidentity.usermanagement.web.rest.model.OrganizationBrandingInfo;
import com.meetidentity.usermanagement.web.rest.model.RegistrationConfigDetails;

@Service
public class OrganizationServiceImpl implements OrganizationService {
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private CardholderRankRepository cardholderRankRepository;

	@Autowired
	private EmployeeAffiliationRepository employeeAffiliationRepository;

	@Autowired
	private OrganizationDepartmentRepository organizationDepartmentRepository;
	
	@Autowired
	private AffiliationColorRepository affiliationColorRepository;
	
	@Autowired
	private PhysicalCCEyeRepository physicalCCEyeRepository;

	@Autowired
	private PhysicalCCHairRepository physicalCCHairRepository;
	
	@Autowired
	private PhysicalCCHeightRepository physicalCCHeightRepository;
	
	@Autowired
	private GenderRepository genderRepository;
	
	@Autowired
	private BloodTypeRepository bloodTypeRepository;
	
	@Autowired
	private HealthGroupPlanRepository healthGroupPlanRepository;
	
	@Autowired
	private EnrollmentApproveRejectReasonsRepository enrollmentApproveRejectReasonsRepository;
	
	@Autowired
	private DrivingLicenseEndorsementRepository drivingLicenseEndorsementRepository;
	
	@Autowired
	private RGBOffsetRepository rgbOffsetRepository;
	
	@Override
	public Organization getOrgByName(String orgName) {
		OrganizationEntity organizationEntity = organizationRepository.findByNameIgnoreCase(orgName);

		Organization org = new Organization();
		org.setId(organizationEntity.getId());
		org.setName(organizationEntity.getName());
		org.setDisplayName(organizationEntity.getDisplayName());
		org.setIndustry(organizationEntity.getIndustry());
		org.setIssuerIdentificationNumber(organizationEntity.getIssuerIdentificationNumber());
		org.setIssuingAuthority(organizationEntity.getIssuingAuthority());
		org.setAgencyCode(organizationEntity.getAgencyCode());
		org.setAddressLine1(organizationEntity.getAddressLine1());
		org.setAddressLine2(organizationEntity.getAddressLine2());
		org.setSiteCd("9999");
		
		return org;
	}
	
	@Override
	public List<String> getOrgLogoByName(String orgName) {
		OrganizationEntity organizationEntity = organizationRepository.findByNameIgnoreCase(orgName);

		List<String> str = new ArrayList<String>();
		str.add(new String(organizationEntity.getLoginLogo()));
		str.add(new String(organizationEntity.getSidebarLogo()));
		str.add(organizationEntity.getAltName());
		return str;	
	}
	
	@Override
	public OrganizationBrandingInfo getOrgBrandingByName(String orgName) {
		OrganizationEntity organizationEntity = organizationRepository.findByNameIgnoreCase(orgName);
		System.out.println("organization name : ---------------------------------------------"+orgName);
		OrganizationBrandingInfo organizationBrandingInfo = new OrganizationBrandingInfo();
		organizationBrandingInfo.setName(organizationEntity.getName());
		organizationBrandingInfo.setDisplayName(organizationEntity.getDisplayName());
		organizationBrandingInfo.setAltName(organizationEntity.getAltName());
		organizationBrandingInfo.setDescription(organizationEntity.getDescription());
		organizationBrandingInfo.setLoginLogo(new String(organizationEntity.getLoginLogo()));
		organizationBrandingInfo.setLoginLogoWidth(new String(organizationEntity.getLoginLogoWidth()));
		organizationBrandingInfo.setLoginLogoHeight(new String(organizationEntity.getLoginLogoHeight()));
		organizationBrandingInfo.setSidebarLogo(new String(organizationEntity.getSidebarLogo()));
		organizationBrandingInfo.setSidebarLogoWidth(new String(organizationEntity.getSidebarLogoWidth()));
		organizationBrandingInfo.setSidebarLogoHeight(new String(organizationEntity.getSidebarLogoHeight()));
		organizationBrandingInfo.setPrimaryColorCode(organizationEntity.getPrimaryColorCode() == null ? null : new String(organizationEntity.getPrimaryColorCode()));
		organizationBrandingInfo.setSecondaryColorCode(organizationEntity.getSecondaryColorCode() == null ? null : new String(organizationEntity.getSecondaryColorCode()));
		organizationBrandingInfo.setTertiaryColorCode(organizationEntity.getTertiaryColorCode() == null ? null : new String(organizationEntity.getTertiaryColorCode()));
		organizationBrandingInfo.setQuaternaryColorCode(organizationEntity.getQuaternaryColorCode() == null ? null : new String(organizationEntity.getQuaternaryColorCode()));
		organizationBrandingInfo.setQuinaryColorCode(organizationEntity.getQuinaryColorCode() == null ? null : new String(organizationEntity.getQuinaryColorCode()));
		organizationBrandingInfo.setPrivacyPolicy(organizationEntity.getPrivacyPolicy());
		organizationBrandingInfo.setHideServiceName(organizationEntity.isHideServiceName());
		organizationBrandingInfo.setOrgId(organizationEntity.getId());
		organizationBrandingInfo.setEnableSchedulerInLoginScreen(organizationEntity.getEnableSchedulerInLoginScreen());
		if (organizationEntity.getFavIcon() != null)
			organizationBrandingInfo.setFavIcon(new String(organizationEntity.getFavIcon()));
		OrganizationBrandingPolicyConfigEntity orgBrnadingPolicyConfig = organizationEntity.getOrganizationBrandingPolicyConfig();
		if (orgBrnadingPolicyConfig != null) {
			organizationBrandingInfo.setPoweredBy(orgBrnadingPolicyConfig.getPoweredBy());
			organizationBrandingInfo.setLoginPageTitle(orgBrnadingPolicyConfig.getLoginPageTitle());
			organizationBrandingInfo.setMobileAppName(orgBrnadingPolicyConfig.getMobileAppName());
			organizationBrandingInfo.setMobileServicesAppName(orgBrnadingPolicyConfig.getMobileServicesAppName());
			organizationBrandingInfo.setContactUsSupportEmail(orgBrnadingPolicyConfig.getContactUsSupportEmail());
		}
		return organizationBrandingInfo;
	}
	

	@Override
	public List<String> getOrgLogo() {
		List<OrganizationEntity> organizationEntity = organizationRepository.findAll();
		List<String> str = new ArrayList<String>();
		str.add(new String(organizationEntity.get(0).getLoginLogo()));
		str.add(new String(organizationEntity.get(0).getSidebarLogo()));
		str.add(organizationEntity.get(0).getAltName());
		return str;	

	}
	
	@Override
	public RegistrationConfigDetails getRegistrationConfigDetailsByOrgName(String orgName, long id){
		
//		List<String> ranks = cardholderRankRepository.getCardholderRanksByOrgName(orgName);
//		List<String> employeeAffiliation = employeeAffiliationRepository.getEmployeeAffiliationsByOrgName(orgName);
//		List<String> physicalccEye = physicalCCEyeRepository.getPhysicalCCEyeTypesByOrgName(orgName);
//		List<String> physicalccHair = physicalCCHairRepository.getPhysicalCCHairTypesByOrgName(orgName);
//		List<String> physicalccHeight = physicalCCHeightRepository.getPhysicalCCHeightTypesByOrgName(orgName);
//		List<String> gender = genderRepository.getGenderByStatus();
//		List<String> bloodType = bloodTypeRepository.getBloodTypeByStatus();
		
		RegistrationConfigDetails config = new RegistrationConfigDetails();
		
		
		config.setRanks(cardholderRankRepository.getCardholderRanksByOrgName(orgName));
		config.setEmployeeAffiliation( employeeAffiliationRepository.getEmployeeAffiliationsByOrgName(orgName, id));
		config.setDepartment( organizationDepartmentRepository.getOrganizationDepartmentsByOrgName(orgName, id));
		config.setPhysicalccEye(physicalCCEyeRepository.getPhysicalCCEyeTypesByOrgName(orgName));
		config.setPhysicalccHair(physicalCCHairRepository.getPhysicalCCHairTypesByOrgName(orgName));
		config.setPhysicalccHeight(physicalCCHeightRepository.getPhysicalCCHeightTypesByOrgName(orgName));
		config.setGender(genderRepository.getGenderByStatus(orgName));
		config.setBloodType(bloodTypeRepository.getBloodTypeByStatus(orgName));
		config.setEmployeeColorCode(affiliationColorRepository.getAffiliationColorsByOrgName(orgName));
		config.setHealthGroupPlan( healthGroupPlanRepository.getHealthGroupPlansByOrgName(orgName, id));
		config.setDrivingLicenseEndorsements(drivingLicenseEndorsementRepository.getDrivingLicenseEndorsementsByOrgName(orgName, id));
		
		return config;
		
	}

	@Override
	public EnrollmentApproveRejectReasons getApprovalRejectionReasons(String organizationName) {
		// TODO Auto-generated method stub
		EnrollmentApproveRejectReasons config = new EnrollmentApproveRejectReasons();		
		
		config.setApprovalReasons(enrollmentApproveRejectReasonsRepository.getEnrollmentApprovalReasons(organizationName));
		config.setRejectionReasons(enrollmentApproveRejectReasonsRepository.getEnrollmentRejectionReasons(organizationName));
		return config;
	}

	@Override
	public List<Organization> getAllOrganizations() {
		List<OrganizationEntity> organizationEntities = organizationRepository.findAll();
		List<Organization> organizations = new ArrayList<>();
		if (organizationEntities.size() > 0) {
			organizationEntities.forEach(orgEntity -> {
				Organization organization = new Organization();
				organization.setName(orgEntity.getName());
				organization.setId(orgEntity.getId());
				organizations.add(organization);
			});
		}
		return organizations;
	}

	@Override
	public ResponseEntity<Boolean> checkTransparentImageEnabled(String organizationName) {
		RGBOffsetEntity rgbOffsetEntity = rgbOffsetRepository.findRGBOffsetByOrg(organizationName);
		if(rgbOffsetEntity != null) {
			return new ResponseEntity<Boolean>(rgbOffsetEntity.getStatus(), HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false, HttpStatus.OK);
	}
}
