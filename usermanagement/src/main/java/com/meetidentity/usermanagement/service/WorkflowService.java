package com.meetidentity.usermanagement.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.meetidentity.usermanagement.exception.WorkflowServiceException;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfile;
import com.meetidentity.usermanagement.web.rest.model.Finger;
import com.meetidentity.usermanagement.web.rest.model.Group;
import com.meetidentity.usermanagement.web.rest.model.OrgIdentityFieldIdNamePair;
import com.meetidentity.usermanagement.web.rest.model.OrganizationIdentity;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.WFStepGenericConfig;
import com.meetidentity.usermanagement.web.rest.model.WfStepAppletLoadingInfo;
import com.meetidentity.usermanagement.web.rest.model.Workflow;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStep;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStepDef;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStepDetail;

public interface WorkflowService {
	
	ResultPage getWorkflows(String organizationName, String sortBy, String  order, int page, int size) throws WorkflowServiceException ;
	
	ResponseEntity<String> createWorkflow(String organizationName, Workflow workflow) throws WorkflowServiceException ;
	
	ResponseEntity<String> updateWorkflow(String organizationName, String workflowName, Workflow workflow) throws WorkflowServiceException ;
	
	void deleteWorkflow(String organizationName, String workflowName) throws WorkflowServiceException ;

	List<WorkflowStepDef> getWorkflowStepDefs() throws WorkflowServiceException ;
	
	Workflow getWorkflowByRole(String organizationName, String roleName) throws WorkflowServiceException ;

	List<WorkflowStep> getStepsByWorkflow(String organizationName, String workflowName) throws WorkflowServiceException ;
	
	WorkflowStepDetail getStepDetails(String organizationName, String workflowName, String stepName) throws WorkflowServiceException ;
	
	List<DeviceProfile> getWorkflowDevProfiles(String organizationName, String wfName ) throws WorkflowServiceException ;
	
	List<DeviceProfile> getWorkflowDevProfileNames(String organizationName, String wfName ) throws WorkflowServiceException ;

	Workflow getWorkflowByName(String organizationName, String workflowName) throws WorkflowServiceException ;

	List<OrgIdentityFieldIdNamePair> getUserOrgIdentityFields(String organizationName, String identityTypeName) throws WorkflowServiceException ;

	Workflow getWorkflowValidityByName(String organizationName, String workflowName) throws WorkflowServiceException ;

	String getWorkflowNameByGroupID(String organizationName, String groupID) throws WorkflowServiceException ;
	
	List<Finger> getFingersByFingerCount(String organizationName, Integer count);

	Workflow getWorkflowValidityByGroupID(String organizationName, String groupID) throws WorkflowServiceException;

	String getIdentityTypeByGroupID(String organizationName, String groupID) throws WorkflowServiceException;

	List<Group> getGroupsWithWorkflow(String organizationName);
	
	boolean checkIfWFStepApprovalIsEnabled(String organizationName, String groupID, String groupName, String stepName) throws WorkflowServiceException;

	String[] getIdentityTypeAndWrkflowByGroupID(String organizationName, String groupID) throws WorkflowServiceException;

	List<OrganizationIdentity> getTrustedDocumentsForSelfServiceOnboard(String organizationName,
			String workflowName) throws WorkflowServiceException;
	
	WfStepAppletLoadingInfo getAppletLoadingInfo(String organizationName, String workflowName, String requestFrom);
	
    boolean isWorkflowExistsByGroupID(String organizationName, String groupID) throws WorkflowServiceException;

    List<WFStepGenericConfig> getWfStepDetails(String organizationName, String workflowName, String stepName);

	WorkflowStep getMobileIdentityIssuanceDetails(String organizationName, String workflowName)
			throws WorkflowServiceException;

	Workflow getDefaultWorkflow(String organizationName);

}