package com.meetidentity.usermanagement.service;

import com.meetidentity.usermanagement.web.rest.model.CustomerBranding;
import com.meetidentity.usermanagement.web.rest.model.UserDevice;

public interface CustomerBrandingService {
	
	public CustomerBranding getCustomerBranding(String organizationName);
}
