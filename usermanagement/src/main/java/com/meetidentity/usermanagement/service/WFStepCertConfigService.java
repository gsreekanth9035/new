package com.meetidentity.usermanagement.service;

import java.util.List;

import com.meetidentity.usermanagement.web.rest.model.WFMobileIdStepCertificate;
import com.meetidentity.usermanagement.web.rest.model.WFStepCertConfig;

public interface WFStepCertConfigService {
	
	List<WFStepCertConfig> getWorkflowCerts(String stepName, String workflowName, String orgName);

	WFStepCertConfig getWorkflowCertConfig(Long Id);
	
	WFMobileIdStepCertificate getWorkflowMobileCertConfig(Long id);
}
