package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.util.XMLResourceDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.config.Constants;
import com.meetidentity.usermanagement.domain.OrganizationIdentitiesEntity;
import com.meetidentity.usermanagement.domain.VisualTemplateDeviceTypeMappingEntity;
import com.meetidentity.usermanagement.domain.VisualTemplateEntity;
import com.meetidentity.usermanagement.domain.WorkflowEntity;
import com.meetidentity.usermanagement.domain.visual.credentials.dl.UtilityDL;
import com.meetidentity.usermanagement.domain.visual.credentials.eid.UtilityEid;
import com.meetidentity.usermanagement.domain.visual.credentials.hid.UtilityHID;
import com.meetidentity.usermanagement.domain.visual.credentials.mls.UtilityMLS;
import com.meetidentity.usermanagement.domain.visual.credentials.nid.UtilityNID;
import com.meetidentity.usermanagement.domain.visual.credentials.piv.UtilityPIV;
import com.meetidentity.usermanagement.domain.visual.credentials.policeid.UtilityPoliceId;
import com.meetidentity.usermanagement.domain.visual.credentials.prid.UtilityPRID;
import com.meetidentity.usermanagement.domain.visual.credentials.pzid.UtilityPZid;
import com.meetidentity.usermanagement.domain.visual.credentials.sid.UtilitySID;
import com.meetidentity.usermanagement.domain.visual.credentials.vid.UtilityVID;
import com.meetidentity.usermanagement.enums.DirectionEnum;
import com.meetidentity.usermanagement.exception.VisualCredentialException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.repository.DeviceTypeRepository;
import com.meetidentity.usermanagement.repository.OrganizationIdentitiesRepository;
import com.meetidentity.usermanagement.repository.OrganizationRepository;
import com.meetidentity.usermanagement.repository.VisualTemplateDeviceTypeMappingRepository;
import com.meetidentity.usermanagement.repository.VisualTemplateRepository;
import com.meetidentity.usermanagement.repository.WorkflowRepository;
import com.meetidentity.usermanagement.service.VisualTemplateService;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.DeviceType;
import com.meetidentity.usermanagement.web.rest.model.OrganizationIdentity;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.VisualTemplate;

@Service
@Transactional
public class VisualTemplateServiceImpl implements VisualTemplateService {
	
	private static final Logger log = LoggerFactory.getLogger(VisualTemplateServiceImpl.class);
	
	@Autowired
	private VisualTemplateRepository visualTemplateRepository;

	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private OrganizationIdentitiesRepository organizationIdentitiesRepository;

	@Autowired
	private WorkflowRepository workflowRepository;
	
	@Autowired
	private DeviceTypeRepository deviceTypeRepository;
	
	@Autowired
	private VisualTemplateDeviceTypeMappingRepository visualTemplateDeviceTypeMappingRepository;
	
	@Override
	public ResultPage getAllVisualTemplates(String organizationName, String sortBy, String order, int page, int size) throws VisualCredentialException {
		List<VisualTemplate> visualTemplates = new ArrayList<>();
		ResultPage resultPage = null;
		Sort sort = null;
		Direction direction = null;
			
			if(order.equalsIgnoreCase(DirectionEnum.ASC.getDirectionCode())) {
				direction = Direction.ASC;
			} else if(order.equalsIgnoreCase(DirectionEnum.DESC.getDirectionCode())) {
				direction = Direction.DESC;
			}
			
			if (direction != null && !sortBy.equalsIgnoreCase("undefined") && !(order.isEmpty())) {
				if (sortBy.equalsIgnoreCase("type")) {
					sortBy = "orgIdentityType.identityType";
					sort = new Sort(direction, sortBy).and(new Sort(direction, "name"));
				} else {
					sort = new Sort(new Sort.Order(direction, sortBy));
				}
			}
			try{
			Pageable pageable = new PageRequest(page, size, sort);
			Page<VisualTemplateEntity> pagedResult = visualTemplateRepository.findAllVisualTemplatesByOrganization(organizationName, pageable);
			if(pagedResult.getTotalElements() != 0) {
				pagedResult.forEach(visualTemplateEntity -> {
					VisualTemplate visualTemplate = new VisualTemplate();
					visualTemplate.setId(visualTemplateEntity.getId());
					visualTemplate.setName(visualTemplateEntity.getName());
					visualTemplate.setOrganizationIdentity(setOrgIdentity(visualTemplateEntity));
					visualTemplate.setDeviceTypes(setDeviceTypes(visualTemplateEntity));
					// TODO Add more attributes
					visualTemplates.add(visualTemplate);
				});
				resultPage = new ResultPage();
				resultPage.setContent(visualTemplates);
				resultPage.setFirst(pagedResult.isFirst());
				resultPage.setLast(pagedResult.isLast());
				resultPage.setNumber(pagedResult.getNumber());
				resultPage.setNumberOfElements(pagedResult.getNumberOfElements());
				resultPage.setSize(pagedResult.getSize());
				resultPage.setSort(pagedResult.getSort());
				resultPage.setTotalElements(pagedResult.getTotalElements());
				resultPage.setTotalPages(pagedResult.getTotalPages());
			}
			return resultPage;
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_FETCHING_VISUAL_CREDENTIALS_UNDER_ORGANIZATION, organizationName), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_FETCHING_VISUAL_CREDENTIALS_UNDER_ORGANIZATION, organizationName);
		}
	}
	
	@Override
	public List<VisualTemplate> getVisualTemplates(String organizationName) throws VisualCredentialException {
		try{
			List<VisualTemplate> visualTemplates = new ArrayList<>();
			visualTemplateRepository.findVisualTemplatesByOrganization(organizationName).stream().filter(Objects::nonNull).forEach(visualTempateEntity -> {
				VisualTemplate visualTemplate = new VisualTemplate();
				visualTemplate.setId(visualTempateEntity.getId());
				visualTemplate.setName(visualTempateEntity.getName());
				// TODO Add more attributes
				visualTemplates.add(visualTemplate);
			});
			return visualTemplates;
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_FETCHING_VISUAL_CREDENTIALS_UNDER_ORGANIZATION, organizationName), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_FETCHING_VISUAL_CREDENTIALS_UNDER_ORGANIZATION, organizationName);
		}
	}
	
    private ResponseEntity<Void> validateVisualTemplate(VisualTemplate visualTemplate) throws VisualCredentialException {
		String parser = XMLResourceDescriptor.getXMLParserClassName();
		SAXSVGDocumentFactory sAXSVGDocumentFactory = new SAXSVGDocumentFactory(parser);
		User user = new User();
		user.setId(Long.parseLong("1"));
		switch (visualTemplate.getOrganizationIdentity().getIdentityTypeName()) {
		
		case Constants.Driving_License:
			try {
				new UtilityDL().parseDLFront("validation", sAXSVGDocumentFactory,  null, null, user,visualTemplate.getFrontDesign(),visualTemplate.getAttributeMapping(),null, null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(), true);
				new UtilityDL().parseDLBack("validation", sAXSVGDocumentFactory,  null, null, user, "Mocking", visualTemplate.getBackDesign(),visualTemplate.getAttributeMapping(),null, null,visualTemplate.getOrganizationIdentity().getIdentityTypeName(), true);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}
			break;
		case Constants.PIV_and_PIV_1:
			try {
				new UtilityPIV().parsePIVFront("validation", sAXSVGDocumentFactory, null, null, user,visualTemplate.getFrontDesign(),visualTemplate.getAttributeMapping(),null, null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(),true);
				new UtilityPIV().parsePIVBack("validation", sAXSVGDocumentFactory, null, null, user, visualTemplate.getBackDesign(),visualTemplate.getAttributeMapping(),null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(),true);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}
			break;
		case Constants.police_id:
			try {
				new UtilityPoliceId().parsePoliceIdFront(sAXSVGDocumentFactory, user,visualTemplate.getFrontDesign(),visualTemplate.getAttributeMapping(),"Mocking", visualTemplate.getOrganizationIdentity().getIdentityTypeName());
				new UtilityPoliceId().parsePoliceIdBack(sAXSVGDocumentFactory, user, visualTemplate.getBackDesign(),visualTemplate.getAttributeMapping(), visualTemplate.getOrganizationIdentity().getIdentityTypeName());
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}
			break;
		case Constants.mls_id:
			try {
				new UtilityMLS().parseMLSFront(sAXSVGDocumentFactory, user,visualTemplate.getFrontDesign(),visualTemplate.getAttributeMapping(),"Mocking", visualTemplate.getOrganizationIdentity().getIdentityTypeName());
				new UtilityMLS().parseMLSBack(sAXSVGDocumentFactory, user,"Mocking", visualTemplate.getBackDesign(),visualTemplate.getAttributeMapping(), visualTemplate.getOrganizationIdentity().getIdentityTypeName());
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}
			break;
		case Constants.National_ID:
			try {
				new UtilityNID().parseNIDFront("validation", sAXSVGDocumentFactory, null, null, user, visualTemplate.getFrontDesign(),visualTemplate.getAttributeMapping(),null, null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(),true);
				new UtilityNID().parseNIDBack("validation", sAXSVGDocumentFactory, null, null, user, visualTemplate.getBackDesign(),visualTemplate.getAttributeMapping(), null, null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(),true);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}
			break;
		case Constants.Voter_ID:
			try {
				new UtilityVID().parseVIDFront("validation", sAXSVGDocumentFactory, null, null, user, visualTemplate.getFrontDesign(),visualTemplate.getAttributeMapping(),null, null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(),true);
				new UtilityVID().parseVIDBack("validation", sAXSVGDocumentFactory, null, null, user, visualTemplate.getBackDesign(),visualTemplate.getAttributeMapping(), null, null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(),true);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}
			break;
		case Constants.Permanent_Resident_ID:
			try {
				new UtilityPRID().parsePRIDFront("validation", sAXSVGDocumentFactory, null, null, user, visualTemplate.getFrontDesign(),visualTemplate.getAttributeMapping(),null, null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(),true);
				new UtilityPRID().parsePRIDBack("validation", sAXSVGDocumentFactory, null, null, user, visualTemplate.getBackDesign(),visualTemplate.getAttributeMapping(), null, null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(),true);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}
			break;
		case Constants.Student_ID:
			try {
				new UtilitySID().parseSidFront("validation",sAXSVGDocumentFactory, null, null, user,visualTemplate.getFrontDesign(),visualTemplate.getAttributeMapping(),"Mocking",null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(), true);
				new UtilitySID().parseSidBack("validation",sAXSVGDocumentFactory, null, null, user,"Mocking", visualTemplate.getBackDesign(),visualTemplate.getAttributeMapping(),null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(), true);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}
			break;
		case Constants.Health_ID:
			try {
				new UtilityHID().parseHIDFront("validation",sAXSVGDocumentFactory, null, null, user,visualTemplate.getFrontDesign(),visualTemplate.getAttributeMapping(),"Mocking",null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(), true);
				new UtilityHID().parseHIDBack("validation",sAXSVGDocumentFactory, null, null, user,"Mocking", visualTemplate.getBackDesign(),visualTemplate.getAttributeMapping(),null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(), true);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}
			break;
		case Constants.Employee_ID:
			try {
				new UtilityEid().parseEidFront("validation",sAXSVGDocumentFactory, null, null, user,visualTemplate.getFrontDesign(),visualTemplate.getAttributeMapping(),"Mocking",null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(), true);
				new UtilityEid().parseEidBack("validation",sAXSVGDocumentFactory, null, null, user,"Mocking", visualTemplate.getBackDesign(),visualTemplate.getAttributeMapping(),null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(), true);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}
			break;
			

		default:
			try {
				new UtilityPZid().parsePZidFront("validation",sAXSVGDocumentFactory, null, null, user,visualTemplate.getFrontDesign(),visualTemplate.getAttributeMapping(),"Mocking",null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(), true);
				new UtilityPZid().parsePZidBack("validation",sAXSVGDocumentFactory, null, null, user,"Mocking", visualTemplate.getBackDesign(),visualTemplate.getAttributeMapping(),null, visualTemplate.getOrganizationIdentity().getIdentityTypeName(), true);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}
			break;
		}
		return null; 
 }
	
	@Override
	public ResponseEntity<Void> createVisualTemplate(String organizationName, VisualTemplate visualTemplate) throws VisualCredentialException {
		ResponseEntity<Void> response = validateVisualTemplate(visualTemplate);
		try{
			if(response == null) {
				VisualTemplateEntity vtFromDb = visualTemplateRepository.findVisualTemplatesByOrgAndName(organizationName, visualTemplate.getName());
				if (vtFromDb != null) {
					if (vtFromDb.getName().equalsIgnoreCase(visualTemplate.getName())) {
						// log.debug("Visual Template with the name" + " " + visualTemplate.getName() + " " + "already exists");
						// throw new Exception(
							//	"Visual Template  with the name" + " " + visualTemplate.getName() + " " + "already exists");
						return new ResponseEntity<>(HttpStatus.CONFLICT);
					}
				}
				VisualTemplateEntity visualTemplateEntity = new VisualTemplateEntity();
				visualTemplateEntity.setOrganization(organizationRepository.findByName(organizationName));
				visualTemplateEntity.setName(visualTemplate.getName());
				visualTemplateEntity.setOrgIdentityType(organizationIdentitiesRepository.getOne(visualTemplate.getOrganizationIdentity().getId()));
				visualTemplate.getDeviceTypes().forEach(deviceType -> {
					VisualTemplateDeviceTypeMappingEntity visualTemplateDeviceEntity = new VisualTemplateDeviceTypeMappingEntity();
					visualTemplateDeviceEntity.setVisualTemplate(visualTemplateEntity);
					visualTemplateDeviceEntity.setDeviceType(deviceTypeRepository.findByName(deviceType.getName()));
					visualTemplateEntity.getVisualTemplateDevices().add(visualTemplateDeviceEntity);
				});
				visualTemplateEntity.setFrontDesign(visualTemplate.getFrontDesign());
				visualTemplateEntity.setBackDesign(visualTemplate.getBackDesign());
				visualTemplateEntity.setFrontOrientation(visualTemplate.getFrontOrientation());
				visualTemplateEntity.setBackOrientation(visualTemplate.getBackOrientation());
				visualTemplateEntity.setAttributeMapping(visualTemplate.getAttributeMapping());
				visualTemplateEntity.setCreatedBy("chetan");
				visualTemplateRepository.save(visualTemplateEntity);
				response = new ResponseEntity<>(HttpStatus.CREATED);
				return response;
			} else {
			 //	log.debug("Validation failed");
			 //	throw new Exception("Validation failed");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch(VisualCredentialException cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_SAVING_VISUAL_CREDENTIAL_UNDER_ORGANIZATION,visualTemplate.getOrganizationIdentity().getIdentityTypeName(), organizationName), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_SAVING_VISUAL_CREDENTIAL_UNDER_ORGANIZATION,visualTemplate.getOrganizationIdentity().getIdentityTypeName(), organizationName);
		}
		catch(Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_SAVING_VISUAL_CREDENTIAL_UNDER_ORGANIZATION,visualTemplate.getOrganizationIdentity().getIdentityTypeName(), organizationName), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_IN_SAVING_VISUAL_CREDENTIAL_WITH_REASON,visualTemplate.getOrganizationIdentity().getIdentityTypeName(),
					organizationName, cause.getMessage());
		}
	}

	@Override
	public VisualTemplate getVisualTemplateByName(String organizationName, String visualTemplateName) throws VisualCredentialException {
		try{
			VisualTemplate visualTemplate = new VisualTemplate();
			VisualTemplateEntity visualTemplateEntity = visualTemplateRepository.findVisualTemplatesByOrgAndName(organizationName, visualTemplateName);
			if (visualTemplateEntity != null) {
				visualTemplate.setName(visualTemplateEntity.getName());
				visualTemplate.setOrganizationIdentity(setOrgIdentity(visualTemplateEntity));
				visualTemplate.setDeviceTypes(setDeviceTypes(visualTemplateEntity));
				visualTemplate.setFrontOrientation(visualTemplateEntity.getFrontOrientation());
				visualTemplate.setBackOrientation(visualTemplateEntity.getBackOrientation());
				visualTemplate.setFrontDesign(visualTemplateEntity.getFrontDesign());
				visualTemplate.setBackDesign(visualTemplateEntity.getBackDesign());
				visualTemplate.setAttributeMapping(visualTemplateEntity.getAttributeMapping());
			}
			return visualTemplate;
		}catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_FETCHING_VISUAL_CREDENTIAL_WITH_NAME_UNDER_ORGANIZATION, visualTemplateName,organizationName), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_FETCHING_VISUAL_CREDENTIAL_WITH_NAME_UNDER_ORGANIZATION, visualTemplateName,organizationName);
		}
	}
	
	private OrganizationIdentity setOrgIdentity(VisualTemplateEntity visualTemplateEntity) {
		OrganizationIdentitiesEntity orgIdentityEntity = visualTemplateEntity.getOrgIdentityType();
		OrganizationIdentity orgIdentity = new OrganizationIdentity();
		orgIdentity.setId(orgIdentityEntity.getId());
		orgIdentity.setIdentityTypeName(orgIdentityEntity.getIdentityType().getName());
		orgIdentity.setIdentityTypeDetails(orgIdentityEntity.getIdentityType().getNotes());
		return orgIdentity;
	}
	
	private List<DeviceType> setDeviceTypes(VisualTemplateEntity visualTemplateEntity){
		List<DeviceType> deviceTypes = new ArrayList<>();
		visualTemplateEntity.getVisualTemplateDevices().forEach(deviceTypeEntity -> {
			DeviceType deviceType = new DeviceType();
			deviceType.setName(deviceTypeEntity.getDeviceType().getName());
			deviceTypes.add(deviceType);
		});
		return deviceTypes;
	}

	@Override
	public ResponseEntity<String> updateVisualTemplate(String organizationName, String visualTemplateName, VisualTemplate visualTemplate) throws VisualCredentialException {
		try {
		
			ResponseEntity<Void> validationResponse = validateVisualTemplate(visualTemplate);
			// System.out.println(visualTemplateName, visualTemplate.getName());
			ResponseEntity<String> response = null;
			if(validationResponse == null) {
				VisualTemplateEntity visualTemplateEntity = visualTemplateRepository.findVisualTemplatesByOrgAndName(organizationName, visualTemplateName);
				if(visualTemplateEntity != null) {
					VisualTemplateEntity vtFromDb = visualTemplateRepository.findVisualTemplatesByOrgAndName(organizationName, visualTemplate.getName());
					if (vtFromDb != null) {
						if (vtFromDb.getName() != visualTemplateEntity.getName()) {
							// log.debug("Visual Template with the name" + " " + visualTemplate.getName() + " " + "already exists");
							// throw new Exception("Visual Template with the name" + " " + visualTemplate.getName() + " " + "already exists");
							return new ResponseEntity<String>("", HttpStatus.CONFLICT);
						}
					}
					visualTemplateEntity.setOrganization(organizationRepository.findByName(organizationName));
					visualTemplateEntity.setName(visualTemplate.getName());
					visualTemplateEntity.setOrgIdentityType(organizationIdentitiesRepository.getOne(visualTemplate.getOrganizationIdentity().getId()));
					// update device types
					List<VisualTemplateDeviceTypeMappingEntity> vtDevTypeMappingsToRemove = new ArrayList<>();
					visualTemplateEntity.getVisualTemplateDevices().stream().forEach(vtEntity -> {
						List<DeviceType> deviceTypes = visualTemplate.getDeviceTypes().stream().filter(
								devType -> devType.getName().equalsIgnoreCase(vtEntity.getDeviceType().getName()))
								.collect(Collectors.toList());
						if (deviceTypes.size() == 0) {
							vtDevTypeMappingsToRemove.add(vtEntity);
						}
					});
					visualTemplate.getDeviceTypes().forEach(deviceType -> {
						VisualTemplateDeviceTypeMappingEntity visualTemplateDeviceEntity = new VisualTemplateDeviceTypeMappingEntity();
						List<VisualTemplateDeviceTypeMappingEntity> wfStepEntities = visualTemplateEntity
								.getVisualTemplateDevices().stream().filter(vtDevEntity -> vtDevEntity.getDeviceType()
										.getName().equalsIgnoreCase(deviceType.getName()))
								.collect(Collectors.toList());
						if (wfStepEntities.size() > 0) {
							visualTemplateDeviceEntity = wfStepEntities.get(0);
						}
						visualTemplateDeviceEntity.setVisualTemplate(visualTemplateEntity);
						visualTemplateDeviceEntity.setDeviceType(deviceTypeRepository.findByName(deviceType.getName()));
						visualTemplateDeviceTypeMappingRepository.save(visualTemplateDeviceEntity);
					});
					// delete unselected devices
					vtDevTypeMappingsToRemove.forEach(entity -> {
						visualTemplateEntity.removeVTDeviceTypeMapping(entity);
						visualTemplateDeviceTypeMappingRepository.delete(entity);
					});
					visualTemplateEntity.setFrontDesign(visualTemplate.getFrontDesign());
					visualTemplateEntity.setBackDesign(visualTemplate.getBackDesign());
					visualTemplateEntity.setFrontOrientation(visualTemplate.getFrontOrientation());
					visualTemplateEntity.setBackOrientation(visualTemplate.getBackOrientation());
					visualTemplateEntity.setAttributeMapping(visualTemplate.getAttributeMapping());
					visualTemplateRepository.save(visualTemplateEntity);
				} else {
						log.debug("Visual template with the given name not exists");
						throw new VisualCredentialException("Visual template with the given name " + visualTemplateName + "not exists");
					//TODO handle error
					// return new ResponseEntity<>("", HttpStatus.CONFLICT);
				}
				return new ResponseEntity<>("", HttpStatus.CREATED);
			} else {
				//	log.debug("Validation failed");
				//	throw new Exception("Validation failed");
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch(VisualCredentialException cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_UPDATING_VISUAL_CREDENTIAL_WITH_NAME_UNDER_ORGANIZATION,visualTemplate.getOrganizationIdentity().getIdentityTypeName(), organizationName), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_SAVING_VISUAL_CREDENTIAL_UNDER_ORGANIZATION,visualTemplate.getOrganizationIdentity().getIdentityTypeName(), organizationName);
		}
		catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_UPDATING_VISUAL_CREDENTIAL_WITH_NAME_UNDER_ORGANIZATION,visualTemplateName, organizationName), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_IN_UPDATING_VISUAL_CREDENTIAL_WITH_REASON, visualTemplateName, organizationName, cause.getMessage());
		}
	}
	
	@Override
	public ResponseEntity<String> deleteVisualTemplateByName(String organizationName, String visualTemplateName)throws VisualCredentialException {
		try {
			VisualTemplateEntity visualTemplateEntity = visualTemplateRepository.findVisualTemplatesByOrgAndName(organizationName, visualTemplateName);
			if (visualTemplateEntity != null) {
				List<WorkflowEntity> wfVt = workflowRepository.getByVisualTemplateId(organizationName,visualTemplateEntity.getId());
				if (wfVt.size() == 0) {
					visualTemplateRepository.delete(visualTemplateEntity.getId());
					return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
				} else {
					return new ResponseEntity<String>("Cannot delete visual template: "+visualTemplateName+ ", as it is mapped with workflow.", HttpStatus.CONFLICT);
				}
			}
			return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
		} catch(Exception cause){
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ERROR_WHILE_FETCHING_VISUAL_CREDENTIALS_UNDER_ORGANIZATION, organizationName), cause);
			throw new VisualCredentialException(cause, ErrorMessages.ERROR_WHILE_FETCHING_VISUAL_CREDENTIALS_UNDER_ORGANIZATION, organizationName);
		}
	}

}
