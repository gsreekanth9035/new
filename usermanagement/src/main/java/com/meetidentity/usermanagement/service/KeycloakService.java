package com.meetidentity.usermanagement.service;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.http.ResponseEntity;

import com.meetidentity.usermanagement.domain.APIGatewayInfoEntity;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.security.CryptoOperationException;
import com.meetidentity.usermanagement.keycloak.domain.CredentialRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.GroupRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.RoleRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.UserRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.WebAuthnAuthenticatorRegistration;
import com.meetidentity.usermanagement.keycloak.domain.WebAuthnPolicyDetails;
import com.meetidentity.usermanagement.web.rest.model.AuthResponse;
import com.meetidentity.usermanagement.web.rest.model.AuthenticatorClientInfo;
import com.meetidentity.usermanagement.web.rest.model.EnableSoftOTP2FA;
import com.meetidentity.usermanagement.web.rest.model.Group;
import com.meetidentity.usermanagement.web.rest.model.IDPUserCredentialConfigResponse;
import com.meetidentity.usermanagement.web.rest.model.IDPUserCredentials;
import com.meetidentity.usermanagement.web.rest.model.IdentityProviderUser;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.Role;
import com.meetidentity.usermanagement.web.rest.model.TokenDetails;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserRolesAndGroups;
import com.meetidentity.usermanagement.web.rest.model.WebauthnRegisterationConfig;

public interface KeycloakService {

	ResponseEntity<UserRepresentation> createUser(IdentityProviderUser identityProviderUser, String organizationName, boolean configureOTP) throws IdentityBrokerServiceException;

	ResponseEntity<String> userLogin(String userName, String password, String organizationName) throws IdentityBrokerServiceException;

	int createRole(Role role, String organizationName) throws IdentityBrokerServiceException;

	int createClientRole(Role role, String organizationName) throws IdentityBrokerServiceException;

	int deleteClientRole(String roleName, String organizationName) throws IdentityBrokerServiceException;

	RoleRepresentation getClientRoleByName(String roleName, String organizationName) throws IdentityBrokerServiceException;

	List<Group> getGroupsForWorkflow(String organizationName);

	List<Group> getGroupsWithWorkflow(String organizationName);

	List<Group> getAllGroups(String organizationName) throws IdentityBrokerServiceException;

	Group getGroupById(String organizationName, String groupId) throws IdentityBrokerServiceException;

	int assignRoleToGroup(String groupId, RoleRepresentation roleRepresentation, String organizationName) throws IdentityBrokerServiceException;

	void assignGroupToUser(String userId, String groupId, String organizationName) throws IdentityBrokerServiceException;
	
	int updateUserGroupByDeletingOldGroup(boolean deletedOldGroup, String oldGroupId, String userId, String groupId, String organizationName) throws IdentityBrokerServiceException;

	UserRepresentation getUserByUsername(String username, String organizationName) throws IdentityBrokerServiceException;

	ResultPage getUsersBySearchCriteria( String organizationName, String searchCriteria, int first, int max) throws IdentityBrokerServiceException;
	
	ResultPage getMembersOfAGroup( String organizationName, String groupID, int first, int max) throws IdentityBrokerServiceException;

	List<GroupRepresentation> getGroupsByUserID(String userID, String organizationName) throws IdentityBrokerServiceException;

	List<RoleRepresentation> getRolesByGroupID(String groupID, String organizationName) throws IdentityBrokerServiceException;

	void userLogout(String userID,  String organizationName) throws IdentityBrokerServiceException;

	int deleteUser(String userName, String organizationName) throws IdentityBrokerServiceException;
	
	int deleteUserByID(String userId, String organizationName) throws IdentityBrokerServiceException;
	
	int updateUser(String userKLKID, JSONObject userJsonObject, String organizationName, String userName) throws IdentityBrokerServiceException;
	
	int updateUser(UserRepresentation userRepresentation, String organizationName) throws IdentityBrokerServiceException;

	void enableSoftOTP2FA(String organizationName, String username, EnableSoftOTP2FA enableSoftOTP2FA);

	UserRepresentation validateUserSession(String organizationName, String userName);

	int assignRoleToUser(String userId, String[] roleIDs, String organizationName);
	
	int deleteClientRoleMappingsFromUserAndAssignNewRoles(String userKLKID, String[] authServiceRoleIDs, String organizationName);

	List<Role> getRolesOfClientFromAuthenticationService(String organizationName);
	
	RoleRepresentation getClientRoleByID(String roleID, String organizationName) throws IdentityBrokerServiceException;

	List<UserRepresentation> getUsersWithSpecifiedRole(String roleName, String organizationName);
	
	List<Role> getUserClientRoles(String userKLKID, String organizationName);

	ResponseEntity<Void> addGroup(String organizationName, Group group) throws IdentityBrokerServiceException;

	ResponseEntity<Void> updateGroup(Group group, String organizationName) throws IdentityBrokerServiceException;
	
	void resetMobileOTPCredential(String organizationName, String userName) throws IdentityBrokerServiceException;
	
	Map<String,Boolean> checkForCredentials(String organizationName, String userName)
			throws IdentityBrokerServiceException;

	void updateUsersRequiredActionsWithOTP(String organizationName, String userName, boolean configureOTP)
			throws IdentityBrokerServiceException;

	UserRolesAndGroups getGroupsAndRolesOfUserByID(String userID, String organizationName)
			throws IdentityBrokerServiceException;

	AuthenticatorClientInfo getAuthenticatorClientInfo(String organizationName, String publicKeyB64, String publicKeyAlgorithm)
			throws CryptoOperationException, IdentityBrokerServiceException;

	TokenDetails generateMobileIDUserTokens(String organizationName, String devicePublicKey, String devicePublicKeyAlgorithm);
	
	User getLdapUserAttributes(String identityType, User user, UserRepresentation userRepresentation);
	
	ResponseEntity<IDPUserCredentialConfigResponse> addUserCredentials(String organizationName, IDPUserCredentials idpUserCredentials) throws IdentityBrokerServiceException;

	void updateUserCredentials(String organizationName, IDPUserCredentials idpUserCredentials) throws IdentityBrokerServiceException;
	
	void deleteUserCredentials(String organizationName, IDPUserCredentials idpUserCredentials) throws IdentityBrokerServiceException;

	UserRepresentation getUserExistsByUsernameOREmail(String username, String organizationName)
			throws IdentityBrokerServiceException;

	AuthenticatorClientInfo getOrgOIDCClientCredentials(String organizationName, APIGatewayInfoEntity apiGatewayInfoByOrgName);
	
    ResponseEntity<String> deleteGroup(String organizationName, String groupId) throws IdentityBrokerServiceException;

	void addRedirectURIs(String organizationName, List<String> redirectURIs) throws IdentityBrokerServiceException;

	List<String> getRedirectURIs(String organizationName);

	void deleteRedirectURIs(String organizationName, String redirectURI);

	void validatePNLogin(String organizationName, AuthResponse authResponse);
	
	List<CredentialRepresentation> getCredentialsByUserId(String organizationName, String userId);
	
	ResponseEntity<String> moveCredentialAfterAnotherCredential(String organizationName, String userId, String credentialId, String newPreviousCredentialId);
	
	ResponseEntity<String> moveCredentialToFirst(String organizationName, String userId, String credentialId);

	ResponseEntity<WebAuthnPolicyDetails> getIDPWebAuthnRegisterPolicyDetails(String organizationName, String userName)
			throws IdentityBrokerServiceException;

	ResponseEntity<WebAuthnAuthenticatorRegistration> registerWebAuthnAuthenticator(String organizationName,
			String userName, WebAuthnAuthenticatorRegistration webAuthnAuthenticatorRegistration)
			throws IdentityBrokerServiceException;

	WebauthnRegisterationConfig executeWebauthn(String organizationName, String userName)
			throws IdentityBrokerServiceException;

}
