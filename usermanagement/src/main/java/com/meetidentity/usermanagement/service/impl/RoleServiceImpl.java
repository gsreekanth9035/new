package com.meetidentity.usermanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.RoleEntity;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.RoleServiceException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.repository.OrganizationRepository;
import com.meetidentity.usermanagement.repository.RoleRepository;
import com.meetidentity.usermanagement.service.KeycloakService;
import com.meetidentity.usermanagement.service.RoleService;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.Role;

@Service
public class RoleServiceImpl implements RoleService {
	
	private final Logger log = LoggerFactory.getLogger(RoleServiceImpl.class);
	
	@Autowired
	private KeycloakService keycloakService;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private OrganizationRepository organizationRepository;

	@Override
	public void createRole(String organizationName, Role role) throws IdentityBrokerServiceException{
		
		// TODO handle Exceptions
		// TODO Predefined Role also to be created in database
		keycloakService.createRole(role,organizationName);
	}

	@Override
	public List<Role> getRoles(String organizationName) throws RoleServiceException{
		try {
			List<RoleEntity> roleEntities = roleRepository.findAllRolesByOrganization(organizationName);
			
			List<Role> roles = new ArrayList<>();
			for (RoleEntity roleEntity: roleEntities) {
				Role role = new Role();
				role.setName(roleEntity.getName());
				role.setDescription(roleEntity.getDescription());
				
				roles.add(role);
			}
			
			return roles;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ROLE_NOT_FOUND_UNDER_ORGANIZATION, organizationName));
			throw new RoleServiceException(cause, Util.format(ErrorMessages.ROLE_NOT_FOUND_UNDER_ORGANIZATION, organizationName));
		}
	}

	@Override
	public Role getRoleByName(String organizationName, String roleName) throws RoleServiceException{
		try {
			RoleEntity roleEntity = roleRepository.findRoleByOrganization(organizationName, roleName);
			Role role = null;
			if (roleEntity != null) {
				role = new Role();
				role.setName(roleEntity.getName());
				role.setDescription(roleEntity.getDescription());
			}
			return role;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ROLE_NOT_FOUND_WITH_ROLENAME_UNDER_ORGANIZATION,roleName, organizationName));
			throw new RoleServiceException(cause, Util.format(ErrorMessages.ROLE_NOT_FOUND_WITH_ROLENAME_UNDER_ORGANIZATION,roleName, organizationName));
		}
	}

	@Override
	public List<String> getRoleNames(String organizationName) {
		try {
			List<String> roleNames = roleRepository.findAllRoleNamesByOrganization(organizationName);
		
			return roleNames;
		} catch (Exception cause) {
			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.ROLE_NOT_FOUND_UNDER_ORGANIZATION, organizationName));
			throw new RoleServiceException(cause, Util.format(ErrorMessages.ROLE_NOT_FOUND_UNDER_ORGANIZATION, organizationName));
		}
	}
	
	@Override
	public List<Role> getRoleWithColor(List<String> listOfRoles, String organization) {
		OrganizationEntity org = organizationRepository.findByName(organization);
		List<Role> listOfRoleData = new ArrayList<>();
		listOfRoles.forEach(role -> {
			Role roles = new Role();
			RoleEntity roleEntity = roleRepository.findRoleByOrganizations(org.getId(), role);
			if (roleEntity != null) {
				roles.setColor(roleEntity.getColor());
				roles.setName(roleEntity.getName());
				listOfRoleData.add(roles);
			} 
		});
		return listOfRoleData;
	}
}
