package com.meetidentity.usermanagement.service;

import java.util.List;

import com.meetidentity.usermanagement.web.rest.model.VehicleRegistration;

public interface UserVehicleService {

	void addVehicleToUser(String organizationName, Long userID, String vin);

	List<VehicleRegistration> getVehiclesByUser(String organizationName, Long userID);
	
}
