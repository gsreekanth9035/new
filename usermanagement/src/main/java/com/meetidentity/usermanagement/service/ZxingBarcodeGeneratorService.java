package com.meetidentity.usermanagement.service;

import java.awt.image.BufferedImage;

public interface ZxingBarcodeGeneratorService {
	public BufferedImage generateUPCABarcodeImage(String barcodeText) throws Exception;

	public BufferedImage generateEAN13BarcodeImage(String barcodeText) throws Exception;

	public BufferedImage generateCode128BarcodeImage(String barcodeText) throws Exception;

	public BufferedImage generatePDF417BarcodeImage(String barcodeText, int width, int height) throws Exception;

	public BufferedImage generateQRCodeImage(String barcodeText) throws Exception;
}
