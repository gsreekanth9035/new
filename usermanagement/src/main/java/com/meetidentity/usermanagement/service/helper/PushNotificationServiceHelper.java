package com.meetidentity.usermanagement.service.helper;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreatePlatformApplicationRequest;
import com.amazonaws.services.sns.model.CreatePlatformApplicationResult;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.DeletePlatformApplicationRequest;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.fasterxml.jackson.databind.ObjectMapper;




public class PushNotificationServiceHelper {
	 // This message is delivered if a platform specific message is not
    // specified for the end point. It must be set. It is received by the device
    // as the value of the key "default".
	private final Logger log = LoggerFactory.getLogger(PushNotificationServiceHelper.class);
	 
    private static final String defaultMessage = "Verification Notification";
    
    //AWS End-Point URL 
    static String AWS_ENDPOINT_URL = "https://sns.us-west-2.amazonaws.com";
    
  //This is AWS Credentials Please careful before sharing 
  	 static String AWS_ACCESS_KEY_ID = "AKIAJJBY2ER7JRVFTNQQ";
  	 static String AWS_SECRET_KEY  = "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";
  	 
  	 

    private final static ObjectMapper objectMapper = new ObjectMapper();

    public static enum Platform {
        // Apple Push Notification Service
        APNS,
        // Sandbox version of Apple Push Notification Service
        APNS_SANDBOX,
        // Amazon Device Messaging
        ADM,
        // Google Cloud Messaging
        GCM
    }

    private static AmazonSNS snsClient;
    
    public PushNotificationServiceHelper() {
        snsClient = awsCredentials(AWS_ACCESS_KEY_ID, AWS_SECRET_KEY);
    }
  

    public CreatePlatformApplicationResult createPlatformApplication(
            String applicationName, Platform platform, String principal, String credential) {
    	CreatePlatformApplicationRequest platformApplicationRequest = null;
    	CreatePlatformApplicationResult createPlatformApplicationResult = null;
    	try {
    		  platformApplicationRequest = new CreatePlatformApplicationRequest();
    	        Map<String, String> attributes = new HashMap<String, String>();
    	        attributes.put("PlatformPrincipal", principal);
    	        attributes.put("PlatformCredential", credential);
    	        platformApplicationRequest.setAttributes(attributes);
    	        platformApplicationRequest.setName(applicationName);
    	        platformApplicationRequest.setPlatform(platform.name());
    	        snsClient = awsCredentials(AWS_ACCESS_KEY_ID, AWS_SECRET_KEY);
    	        createPlatformApplicationResult = snsClient.createPlatformApplication(platformApplicationRequest);
    	      
    	}catch(Exception e) {
    		log.warn("Error : "+e.getMessage());
    	}
    	 
        return createPlatformApplicationResult;
    }

    public CreatePlatformEndpointResult createPlatformEndpoint(
            String customData, String platformToken, String applicationArn) {
        CreatePlatformEndpointRequest platformEndpointRequest = new CreatePlatformEndpointRequest();
        //setCustomUserData should always be the same : so simply put "customData" 
        /*****Do not remove setCustomUserData value*****/
        platformEndpointRequest.setCustomUserData("customData");
        platformEndpointRequest.setToken(platformToken);
        platformEndpointRequest.setPlatformApplicationArn(applicationArn);
        return snsClient.createPlatformEndpoint(platformEndpointRequest);
    }

    private String getPlatformSampleMessage(Platform platform, String message) {
        switch (platform) {
        case APNS:
            return getSampleAppleMessage(message);
        case APNS_SANDBOX:
            return getSampleAppleMessage(message);
        case GCM:
            return getSampleAndroidMessage(message);
        case ADM:
            return getSampleKindleMessage(message);
        default:
            throw new IllegalArgumentException("Platform Not supported : " + platform.name());
        }
    }

    private String getSampleAppleMessage( String message) {
        Map<String, Object> appleMessageMap = new HashMap<String, Object>();
        Map<String, Object> appMessageMap = new HashMap<String, Object>();
        appMessageMap.put("alert", "You have got email.");
        appMessageMap.put("data", getData(message));
        appMessageMap.put("badge", 9);
        appMessageMap.put("sound", "default");
        appleMessageMap.put("aps", appMessageMap);
        return jsonify(appleMessageMap);
    }

    private String getSampleKindleMessage( String message) {
        Map<String, Object> kindleMessageMap = new HashMap<String, Object>();
        kindleMessageMap.put("data", getData(message));
        kindleMessageMap.put("consolidationKey", "Welcome");
        kindleMessageMap.put("expiresAfter", 1000);
        return jsonify(kindleMessageMap);
    }

    private String getSampleAndroidMessage( String message) {
        Map<String, Object> androidMessageMap = new HashMap<String, Object>();
        androidMessageMap.put("collapse_key", "Welcome");
        androidMessageMap.put("data", getData(message));
        androidMessageMap.put("delay_while_idle", true);
        androidMessageMap.put("time_to_live", 125);
        androidMessageMap.put("dry_run", false);
        return jsonify(androidMessageMap);
    }

    private Map<String, String> getData(String message) {
        Map<String, String> payload = new HashMap<String, String>();
        payload.put("message", message);
        return payload;
    }

    public PublishResult publish(String endpointArn, Platform platform,String message) {
        PublishRequest publishRequest = new PublishRequest();
        Map<String, String> messageMap = new HashMap<String, String>();
        messageMap.put("default", defaultMessage);
        
        Map<String, String> notification = new HashMap<String, String>();
        notification.put("title", "Verification Notification!!");
        notification.put("body", "Message!!");        
        messageMap.put("notification", jsonify(notification));
        
        messageMap.put(platform.name(), getPlatformSampleMessage(platform, message));
        // messageMap.put(platform.name(), message);

        // For direct publish to mobile end points, topicArn is not relevant.
        publishRequest.setTargetArn(endpointArn);
        publishRequest.setMessageStructure("json");
        message = jsonify(messageMap);

        publishRequest.setMessage(message);
        return snsClient.publish(publishRequest);
    }

    void deletePlatformApplication(String applicationArn) {
        DeletePlatformApplicationRequest request = new DeletePlatformApplicationRequest();
        request.setPlatformApplicationArn(applicationArn);
        snsClient.deletePlatformApplication(request);  
    }

    private  String jsonify(Object message) {
        try {
            return objectMapper.writeValueAsString(message);
        } catch (Exception e) {
        	log.warn("Warning : "+e.getMessage());
        	return null;
        }
    }
    
   
	
	public String getPlatform(CreatePlatformApplicationResult createPlatformApplicationResult) {
		return createPlatformApplicationResult.getPlatformApplicationArn();
	}
	
	// AWS SNS Credential 
	@SuppressWarnings({ "unused", "deprecation" })
	public   AmazonSNS awsCredentials(String AWS_ACCESS_KEY_ID, String AWS_SECRET_KEY ) {

		AWSCredentials credentials = new BasicAWSCredentials(AWS_ACCESS_KEY_ID,
				AWS_SECRET_KEY);
		AmazonSNS sns = new AmazonSNSClient(credentials);
		sns.setEndpoint(AWS_ENDPOINT_URL);
		return sns;
	}
}

