package com.meetidentity.usermanagement.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.VehicleClassification;
import com.meetidentity.usermanagement.web.rest.model.VehicleRegistration;
import com.meetidentity.usermanagement.web.rest.model.VehicleRegistrations;

public interface VehicleService {

	VehicleRegistration getVehicleRegistrationDetails(String organizationName, String licensePlateNumber);

	ResponseEntity<Void> createVehicleRegistrations(String organizationName, VehicleRegistrations vehicleRegistrations) throws Exception;

	ResultPage getAllVehicleRegs(String organizationName, String sortBy, String  order, int page, int size);

	ResponseEntity<String> deleteVehicleRegistration(String organizationName, String vin);

	ResponseEntity<String> updateVehicleRegistration(String organizationName, String licensePlateNumber,
			VehicleRegistration vehicleRegistration) throws Exception;

	List<VehicleClassification> getVehicleClassifications(String organizationName);

}
