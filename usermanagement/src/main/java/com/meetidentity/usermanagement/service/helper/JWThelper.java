package com.meetidentity.usermanagement.service.helper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;

import javax.security.auth.x500.X500Principal;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWThelper {
	
	public X509Certificate getcertificateFromP12(String p12Path) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException, UnrecoverableKeyException {
		 KeyStore p12 = KeyStore.getInstance("pkcs12");
	     p12.load(new FileInputStream(p12Path), "Unifyia".toCharArray());
	     
	        while (p12.aliases().hasMoreElements()) {
	            String alias = (String) p12.aliases().nextElement();
	            return (X509Certificate) p12.getCertificate(alias);
	        }
		return null;
		
	}
	
	private PrivateKey getPrivateKey(String p12Path) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException, UnrecoverableKeyException {
		 KeyStore p12 = KeyStore.getInstance("pkcs12");
	     p12.load(new FileInputStream(p12Path), "Unifyia".toCharArray());
	     
	        while (p12.aliases().hasMoreElements()) {
	            String alias = (String) p12.aliases().nextElement();
	            return (PrivateKey)p12.getKey(alias, "Unifyia".toCharArray());
	        }
	        return null;
	}
	public String getSignDataUsingCertificate(X509Certificate x509Certificate,String toSign,String p12Path ) throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException {
		
		 X500Principal issuer = x509Certificate.getIssuerX500Principal();
		 SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.RS256;
	        Calendar expires = Calendar.getInstance();
	        expires.roll(Calendar.HOUR, 2);
	        JwtBuilder builder = Jwts.builder()
	                .setIssuedAt(new Date())
	                .setSubject(toSign)
	                .setIssuer(issuer.getName())
	                .setExpiration(expires.getTime())
	                .signWith(signatureAlgorithm, getPrivateKey(p12Path));	
	      
		return builder.compact();
	}
	

}
