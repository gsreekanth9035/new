package com.meetidentity.usermanagement.service;

import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.meetidentity.usermanagement.domain.UserEntity;
import com.meetidentity.usermanagement.exception.EntityCreateException;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.UIMSException;
import com.meetidentity.usermanagement.keycloak.domain.CredentialRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.WebAuthnAuthenticatorRegistration;
import com.meetidentity.usermanagement.keycloak.domain.WebAuthnPolicyDetails;
import com.meetidentity.usermanagement.web.rest.model.IDPUserCredentialConfigResponse;
import com.meetidentity.usermanagement.web.rest.model.IDPUserCredentials;
import com.meetidentity.usermanagement.web.rest.model.IdentityProviderUser;
import com.meetidentity.usermanagement.web.rest.model.User;

public interface IdentityProviderUserService {

	ResponseEntity<User> createUserAndAssignGroupAndRoles(HttpHeaders httpHeaders, String organizationName, IdentityProviderUser identityProviderUser,
			UserEntity userEntity) throws EntityCreateException, IdentityBrokerServiceException, UIMSException;
	
	void updateUserRoles(String organizationName, IdentityProviderUser identityProviderUser) throws EntityCreateException, IdentityBrokerServiceException, UIMSException;

	ResponseEntity<IdentityProviderUser> getOnboardUserDetails(String organizationName, String userName);

	ResponseEntity<User> updateUserAndAssignGroupAndRoles(HttpHeaders httpHeaders, String organizationName, IdentityProviderUser identityProviderUser)
			throws EntityCreateException, IdentityBrokerServiceException, JSONException, UIMSException;
	
	String listUserCredentials(Long userId, String organizationName);

	ResponseEntity<IDPUserCredentialConfigResponse> addUserCredentials(Long userId, String organizationName, IDPUserCredentials idpUserCredentials);

	void updateUserCredentials(Long userId, String organizationName, IDPUserCredentials idpUserCredentials);

	void deleteUserCredentials(Long userId, String organizationName, IDPUserCredentials idpUserCredentials);
	
	List<CredentialRepresentation> getCredentialsByUserId(String organizationName, String userId);
	
	ResponseEntity<String> moveCredentialAfterAnotherCredential(String organizationName, String userId, String credentialId, String newPreviousCredentialId);
	
	ResponseEntity<String> moveCredentialToFirst(String organizationName, String userId, String credentialId);

	ResponseEntity<HttpStatus> createUserLocallyByIDP(IdentityProviderUser identityProviderUser) throws EntityCreateException;

	ResponseEntity<WebAuthnPolicyDetails> getIDPWebAuthnRegisterPolicyDetails(String userName, String organizationName);

	ResponseEntity<WebAuthnAuthenticatorRegistration> registerWebAuthnAuthenticator(String userName, String organizationName,
			WebAuthnAuthenticatorRegistration webAuthnAuthenticatorRegistration);
}
