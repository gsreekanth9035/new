package com.meetidentity.usermanagement.service;

import org.springframework.http.HttpHeaders;

import com.meetidentity.usermanagement.web.rest.model.notification.DeliverReqBean;

public interface AsynchronousService {
	public void executeAsynchronously(DeliverReqBean deliverReqBean, HttpHeaders headers) throws Exception;
}
