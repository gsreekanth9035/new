package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.OrganizationDepartmentEntity;
import com.meetidentity.usermanagement.web.rest.model.TypeDescription;

public interface OrganizationDepartmentRepository  extends JpaRepository<OrganizationDepartmentEntity, Long> {

	@Query("SELECT  new com.meetidentity.usermanagement.web.rest.model.TypeDescription(od.type, od.description)  FROM OrganizationDepartmentEntity od "
			+ "WHERE UPPER(od.organization.name) = UPPER(:orgName) and od.identityTpeId.id = :identity_type_id and od.status = 0")
	List<TypeDescription> getOrganizationDepartmentsByOrgName(@Param("orgName") String orgName, @Param("identity_type_id")long identity_type_id);
	
}
