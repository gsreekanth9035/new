package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WFMobileIdStepCertificatesEntity;

@Repository
public interface WFMobileIdStepCertificatesRepository extends JpaRepository<WFMobileIdStepCertificatesEntity, Long>,
		CrudRepository<WFMobileIdStepCertificatesEntity, Long> {
}
