package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.VisualTemplateEntity;

@Repository
public interface VisualTemplateRepository extends JpaRepository<VisualTemplateEntity, Long> {

	@Query("SELECT vt FROM VisualTemplateEntity vt WHERE UPPER(vt.organization.name) = UPPER(:organizationName)")
	List<VisualTemplateEntity> findVisualTemplatesByOrganization(@Param("organizationName") String organizationName);

	@Query("SELECT vt FROM VisualTemplateEntity vt WHERE UPPER(vt.organization.name) = UPPER(:organizationName) and  UPPER(vt.name) = UPPER(:name)")
	VisualTemplateEntity findVisualTemplatesByOrgAndName(@Param("organizationName") String organizationName,
			@Param("name") String name);
	
	VisualTemplateEntity findByName(@Param("name") String name);
	
	@Query("SELECT vt FROM VisualTemplateEntity vt WHERE UPPER(vt.organization.name) = UPPER(:organizationName)")
	Page<VisualTemplateEntity> findAllVisualTemplatesByOrganization(@Param("organizationName") String organizationName, Pageable page);
	
	@Query("SELECT vt FROM VisualTemplateEntity vt WHERE UPPER(vt.organization.name) = UPPER(:organizationName) AND UPPER(vt.orgIdentityType.identityType.name)= UPPER(:identityTypeName)")
	List<VisualTemplateEntity> fingVisualTemplateByIdentityType(@Param("organizationName") String organizationName, @Param("identityTypeName") String identityTypeName);
}
