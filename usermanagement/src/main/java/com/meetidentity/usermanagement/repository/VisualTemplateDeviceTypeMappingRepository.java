package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.VisualTemplateDeviceTypeMappingEntity;

@Repository
public interface VisualTemplateDeviceTypeMappingRepository extends JpaRepository<VisualTemplateDeviceTypeMappingEntity, Long> {

}
