package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.SequenceNumberEntity;

@Repository
public interface SequenceNumberRepository extends JpaRepository<SequenceNumberEntity, Long>{
		
//	public SequenceNumberEntity findByClassName(String className);
	
	public SequenceNumberEntity findByOrgNameIgnoreCaseAndClassNameIgnoreCase(String orgName, String className);
 
}
