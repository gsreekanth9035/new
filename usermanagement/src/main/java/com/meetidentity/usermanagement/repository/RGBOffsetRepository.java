package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.RGBOffsetEntity;

@Repository
public interface RGBOffsetRepository extends JpaRepository<RGBOffsetEntity, Long> {
	@Query("SELECT rgboffset FROM RGBOffsetEntity rgboffset WHERE UPPER(rgboffset.organization.name) = UPPER(:organizationName) ")
	RGBOffsetEntity findRGBOffsetByOrg(@Param("organizationName") String organizationName);
}
