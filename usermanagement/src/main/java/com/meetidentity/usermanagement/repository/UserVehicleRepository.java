package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserVehicleEntity;
import com.meetidentity.usermanagement.domain.VehicleEntity;

@Repository
public interface UserVehicleRepository extends JpaRepository<UserVehicleEntity, Long> {
	
	@Query("SELECT uv.vehicle FROM UserVehicleEntity uv WHERE uv.user.id = :userID")
	List<VehicleEntity> findVehicleByUser(@Param("userID") Long userID);

}
