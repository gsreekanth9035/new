package com.meetidentity.usermanagement.repository.specification;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;

import com.meetidentity.usermanagement.domain.OrganizationEntity;
import com.meetidentity.usermanagement.domain.UserEntity;

public class UserSpecification {
	public static Specification<UserEntity> userSpecifications(String orgName, String groupID, String status, Date startDate, Date endDate) {
		return (root, query, builder) -> {
			List<Predicate> predicates = new ArrayList<>();
			if (orgName != null) {
				Join<UserEntity, OrganizationEntity> organization = root.join("organization");
				predicates.add(builder.equal(organization.get("name"), orgName));
			}
			/*
			 * if (groupID != null) {
			 * predicates.add(builder.equal(root.get("refApprovalGroupId"), groupID)); }
			 */
			if (status != null) {
				predicates.add(builder.equal(root.get("status"), status));
			}
			if (startDate != null) {
				predicates.add(builder.greaterThanOrEqualTo(root.get("createdDate"), startDate.toInstant()));
			}
			if (endDate != null) {
				predicates.add(builder.lessThanOrEqualTo(root.get("createdDate"), endDate.toInstant()));
			}
			Predicate[] p = predicates.toArray(new Predicate[predicates.size()]);
			return p.length == 0 ? null : p.length == 1 ? p[0] : builder.and(p);
		};
	}

}
