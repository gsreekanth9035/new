package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.OrganizationEntity;

@Repository
public interface OrganizationRepository extends JpaRepository<OrganizationEntity, Long> { 

	OrganizationEntity findByNameIgnoreCase(String name);
	
	OrganizationEntity findByName(String name);
	
	
}
