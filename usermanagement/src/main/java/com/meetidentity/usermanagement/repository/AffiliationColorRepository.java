package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.AffiliationColorEntity;
import com.meetidentity.usermanagement.web.rest.model.TypeDescription;

public interface AffiliationColorRepository  extends JpaRepository<AffiliationColorEntity, Long> {

	@Query("SELECT  new com.meetidentity.usermanagement.web.rest.model.TypeDescription(ac.type, ac.description) FROM AffiliationColorEntity ac "
			+ "WHERE UPPER(ac.organization.name) = UPPER(:orgName) and ac.status = 0")
	List<TypeDescription> getAffiliationColorsByOrgName(@Param("orgName") String orgName);
	
}
