package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.UserPermanentResidentIdEntity;

public interface UserPermanentResidentIdRepository extends JpaRepository<UserPermanentResidentIdEntity, Long> {

	@Query("SELECT prc FROM UserPermanentResidentIdEntity prc WHERE prc.user.id = (:userid)")
	UserPermanentResidentIdEntity findByUserID(@Param("userid") Long userid);

}
