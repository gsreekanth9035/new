package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.meetidentity.usermanagement.domain.UserAddressEntity;

public interface UserAddressRepository  extends JpaRepository<UserAddressEntity, Long> {

}
