package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.meetidentity.usermanagement.domain.EnrollmentStepsStatusEntity;

public interface EnrollmentStepStatusRepository extends JpaRepository<EnrollmentStepsStatusEntity, Long> {


}
