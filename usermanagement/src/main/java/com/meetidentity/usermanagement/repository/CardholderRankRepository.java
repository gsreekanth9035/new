package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.CardholderRankEntity;
import com.meetidentity.usermanagement.web.rest.model.TypeDescription;

public interface CardholderRankRepository  extends JpaRepository<CardholderRankEntity, Long> {

	@Query("SELECT new com.meetidentity.usermanagement.web.rest.model.TypeDescription(chr.type, chr.description) FROM CardholderRankEntity chr "
			+ "WHERE UPPER(chr.organization.name) = UPPER(:orgName) and chr.status = 0")
	List<TypeDescription> getCardholderRanksByOrgName(@Param("orgName") String orgName);
	
}
