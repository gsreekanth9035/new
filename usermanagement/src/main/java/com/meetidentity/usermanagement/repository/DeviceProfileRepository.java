package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.DeviceProfileEntity;

@Repository
public interface DeviceProfileRepository
		extends JpaRepository<DeviceProfileEntity, Long>, CrudRepository<DeviceProfileEntity, Long> {

	@Query("SELECT devProf FROM DeviceProfileEntity devProf LEFT JOIN FETCH devProf.devProfKeyMgr LEFT JOIN FETCH devProf.devProfHsmInfo WHERE devProf.id = :deviceProfileID")
	DeviceProfileEntity findDeviceProfileByID(@Param("deviceProfileID") Long deviceProfileID);

	DeviceProfileEntity findByName(@Param("name") String name);
	
	@Query("SELECT devProf FROM DeviceProfileEntity devProf  WHERE UPPER(devProf.organization.name) = UPPER(:organizationName) AND UPPER(devProf.name) = UPPER(:name) ")
	DeviceProfileEntity findByNameAndOrganization(@Param("organizationName") String organizationName, @Param("name") String name);

	@Query("SELECT wfDp.deviceProfile FROM WFDeviceProfileEntity wfDp WHERE UPPER(wfDp.workflow.name) = UPPER(:wfName) AND UPPER(wfDp.workflow.organization.name) = UPPER(:organizationName)")
	List<DeviceProfileEntity> findWorkflowDevProfiles(@Param("organizationName") String organizationName, @Param("wfName") String wfName);

	@Query("SELECT devProf FROM DeviceProfileEntity devProf  WHERE UPPER(devProf.organization.name) = UPPER(:organizationName)")
	Page<DeviceProfileEntity> findAllByOrg(@Param("organizationName") String organizationName, Pageable pageable);

	@Query("SELECT devProf FROM DeviceProfileEntity devProf  WHERE UPPER(devProf.organization.name) = UPPER(:organizationName)")
	List<DeviceProfileEntity> findAllByOrgName(@Param("organizationName") String organizationName);

}
