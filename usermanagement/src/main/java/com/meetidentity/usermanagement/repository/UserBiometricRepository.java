package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserBiometricEntity;

@Repository
public interface UserBiometricRepository extends JpaRepository<UserBiometricEntity, Long> {
	@Query("SELECT userBiometrics FROM UserBiometricEntity userBiometrics WHERE userBiometrics.user.id = (:id) AND userBiometrics.type =(:type)")
	List<UserBiometricEntity> getUserBiometricsByType(@Param("id") Long id,@Param("type") String type);
	
	@Modifying
	@Query("DELETE  FROM UserBiometricEntity userBiometrics WHERE userBiometrics.id = (:id)")
	void deleteById(@Param("id") Long id);	
}
