package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WfStepChipEncodeGroupConfigEntity;

@Repository
public interface WfStepChipEncodeGroupConfigRepository extends JpaRepository<WfStepChipEncodeGroupConfigEntity, Long> {
	@Modifying
	@Query(value="UPDATE wf_step_chip_encode_group_config we JOIN wf_step ws ON ws.id= wf_step_id Join workflow wf ON wf.id=ws.workflow_id INNER JOIN organization o ON o.id=wf.organization_id SET we.reference_chip_encode_group_Name = :groupName WHERE we.reference_chip_encode_group_id= :groupId AND o.name = :organizationName", nativeQuery = true)
	void updateGroupName(@Param("groupId") String groupId, @Param("groupName") String groupName, @Param("organizationName") String organizationName );
}
