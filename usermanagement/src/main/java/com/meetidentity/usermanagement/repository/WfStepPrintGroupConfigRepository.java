package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WfStepPrintGroupConfigEntity;

@Repository
public interface WfStepPrintGroupConfigRepository extends JpaRepository<WfStepPrintGroupConfigEntity, Long> {
	@Modifying
	@Query(value="UPDATE wf_step_print_group_config wi JOIN wf_step ws ON ws.id= wf_step_id Join workflow wf ON wf.id=ws.workflow_id INNER JOIN organization o ON o.id=wf.organization_id SET wi.reference_print_group_Name = :groupName WHERE wi.reference_print_group_id= :groupId AND o.name = :organizationName", nativeQuery = true)
	void updateGroupName(@Param("groupId") String groupId, @Param("groupName") String groupName, @Param("organizationName") String organizationName );
}
