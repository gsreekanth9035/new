package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WFStepCertConfigEntity;
@Repository
public interface WFStepCertConfigRepository
     extends JpaRepository<WFStepCertConfigEntity, Long>, CrudRepository<WFStepCertConfigEntity, Long>{
	
	@Query("SELECT wfCerts FROM WFStepCertConfigEntity wfCerts "
			+ "JOIN wfCerts.wfStep wfStep "
			+ "JOIN wfStep.workflow workflow "
			+ "JOIN wfStep.wfStepDef wfStepDef "
			+ "WHERE wfStepDef.name = UPPER(:stepName) "
			+ "AND workflow.name = UPPER(:workflowName) "
			+ "AND workflow.organization.name = UPPER(:orgName) AND wfCerts.status='ACTIVE' ")
	List<WFStepCertConfigEntity> findWorkflowCertificates1(@Param("stepName") String stepName,
			 @Param("workflowName") String workflowName, @Param("orgName") String orgName);
}
