package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.DrivingLicenseRestrictionEntity;

@Repository
public interface DrivingLicenseRestrictionsRepository extends JpaRepository<DrivingLicenseRestrictionEntity, Long> {

	@Query("SELECT drivingLicenseRestrictionEntity FROM DrivingLicenseRestrictionEntity drivingLicenseRestrictionEntity "
			+ "WHERE drivingLicenseRestrictionEntity.type = (:restrictionsType)")
	DrivingLicenseRestrictionEntity findOneByType(@Param("restrictionsType")  String restrictionsType);
	
	@Query("SELECT dlr FROM DrivingLicenseRestrictionEntity dlr "
			+ "WHERE dlr.type = (:restrictionsType) AND UPPER(dlr.organization.name) = UPPER(:organizationName)")
	DrivingLicenseRestrictionEntity findOneByTypeAndOrganization(@Param("restrictionsType")  String restrictionsType, @Param("organizationName")  String organizationName);
	
	@Query("SELECT dlr FROM DrivingLicenseRestrictionEntity dlr "
			+ "WHERE UPPER(dlr.organization.name) = UPPER(:organizationName)")
	List<DrivingLicenseRestrictionEntity> findByOrganizationName(@Param("organizationName")  String organizationName);

}
