package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.MobilePairPolicyConfigEntity;

@Repository
public interface MobilePairPolicyConfigRepository extends JpaRepository<MobilePairPolicyConfigEntity, Long> {
	@Query("SELECT mppconfig FROM MobilePairPolicyConfigEntity mppconfig WHERE UPPER(mppconfig.organization.name) = UPPER(:organizationName) and mppconfig.name = UPPER(:enrollmentTokenExpiryConfigName) ")
	MobilePairPolicyConfigEntity getEnrollmentTokenExpiry(@Param("organizationName") String organizationName, @Param("enrollmentTokenExpiryConfigName") String enrollmentTokenExpiryConfigName);
	

	@Query("SELECT mppconfig FROM MobilePairPolicyConfigEntity mppconfig WHERE mppconfig.organization.id = :organizationID ")
	MobilePairPolicyConfigEntity getEnrollmentTokenExpiry(@Param("organizationID") Long organizationID);


}
