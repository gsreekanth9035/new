package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WFMobileIDStepCertConfigEntity;

@Repository
public interface WFMobileIDStepCertConfigRepository extends JpaRepository<WFMobileIDStepCertConfigEntity, Long> {
}
