package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.UserGenderEntity;
import com.meetidentity.usermanagement.web.rest.model.TypeDescription;

public interface GenderRepository  extends JpaRepository<UserGenderEntity, Long> {

	@Query("SELECT  new com.meetidentity.usermanagement.web.rest.model.TypeDescription(gender.type, gender.description) FROM UserGenderEntity gender "
			+ " WHERE UPPER(gender.organization.name) = UPPER(:orgName) AND gender.status = 0")
	List<TypeDescription> getGenderByStatus(@Param("orgName") String orgName);
	
}
