package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.meetidentity.usermanagement.domain.UserIDProofDocumentEntity;

public interface UserIDProofDocumentRepository extends JpaRepository<UserIDProofDocumentEntity, Long>{
	
	@Query("SELECT ude FROM UserIDProofDocumentEntity ude WHERE UPPER(ude.user.referenceName) = UPPER(:userName)")
	List<UserIDProofDocumentEntity> getUserIdProofDocsList(String userName);

}
