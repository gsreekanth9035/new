package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WFGroupEntity;

@Repository
public interface WFGroupRepository extends JpaRepository<WFGroupEntity, Long> { 

	@Query("SELECT wfgroup FROM WFGroupEntity wfgroup  WHERE UPPER(wfgroup.referenceGroupId) = UPPER(:groupID) AND UPPER(wfgroup.workflow.organization.name) = UPPER(:organizationName) ")
	WFGroupEntity findWorkflowByGroupID(@Param("organizationName") String organizationName, @Param("groupID") String groupID);
	
	@Query("SELECT CASE WHEN COUNT(wfgroup) = 0 THEN false ELSE true END FROM WFGroupEntity wfgroup WHERE "
			+ "EXISTS (SELECT 1 FROM WFGroupEntity a WHERE a.referenceGroupId = :groupID) "
			+ "OR EXISTS (SELECT 1 FROM WFMobileIdStepVisualIDGroupConfigEntity b WHERE b.referenceGroupId = :groupID) "
			+ "OR EXISTS (SELECT 1 FROM WFStepAdjudicationGroupConfigEntity c WHERE c.referenceAdjudicationGroupId = :groupID) "
			+ "OR EXISTS (SELECT 1 FROM WFStepApprovalGroupConfigEntity d WHERE d.referenceApprovalGroupId = :groupID) "
			+ "OR EXISTS (SELECT 1 FROM WfStepChipEncodeAndVIDPrintGroupConfigEntity e WHERE e.referenceChipEncodeVIDPrintGroupId = :groupID) "
			+ "OR EXISTS (SELECT 1 FROM WfStepChipEncodeGroupConfigEntity f WHERE f.referenceChipEncodeGroupId = :groupID) "
			+ "OR EXISTS (SELECT 1 FROM WfStepPrintGroupConfigEntity f WHERE f.referencePrintGroupId = :groupID) AND UPPER(wfgroup.workflow.organization.name) = UPPER(:organizationName) ")
    boolean  isWorkflowExistsByGroupID(@Param("organizationName") String organizationName, @Param("groupID") String groupID);
	
	@Modifying
	@Query(value = "UPDATE wf_group wg JOIN workflow wf ON wf.id = workflow_id INNER JOIN organization o ON o.id = wf.organization_id SET wg.reference_group_name = :groupName WHERE wg.reference_group_id= :groupId AND o.name = :organizationName", nativeQuery = true)
	void updateGroupName(@Param("groupId") String groupId, @Param("groupName") String groupName, @Param("organizationName") String organizationName );        
}
