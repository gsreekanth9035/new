package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.RoleEntity;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> { 

	RoleEntity findByName(String name);
	
	@Query("SELECT role FROM RoleEntity role WHERE UPPER(role.name) = UPPER(:roleName) AND UPPER(role.organization.name) = UPPER(:organizationName) ")
	RoleEntity findRoleByOrganization(@Param("organizationName") String organizationName, @Param("roleName") String roleName);
	
	@Query("SELECT role FROM RoleEntity role WHERE UPPER(role.workflow.name) = UPPER(:workflowName) AND UPPER(role.organization.name) = UPPER(:organizationName) ")
	RoleEntity findRoleByWorkflow(@Param("organizationName") String organizationName, @Param("workflowName") String workflowName);

	@Query("SELECT role FROM RoleEntity role WHERE UPPER(role.organization.name) = UPPER(:organizationName)  ORDER BY role.name DESC ")
	List<RoleEntity> findAllRolesByOrganization(@Param("organizationName") String organizationName);

	@Query("SELECT role.name FROM RoleEntity role WHERE UPPER(role.organization.name) = UPPER(:organizationName) and role.type= 'ROLE' ORDER BY role.name DESC ")
	List<String> findAllRoleNamesByOrganization(@Param("organizationName") String organizationName);

	@Query("SELECT role FROM RoleEntity role WHERE UPPER(role.organization.name) = UPPER(:organizationName) and role.type= 'ROLE'  ORDER BY role.name DESC ")
	List<RoleEntity> getActiveUserRolesByOrganization(@Param("organizationName") String organizationName);
	
	@Query("SELECT role FROM RoleEntity role WHERE UPPER(role.name) = UPPER(:roleName) AND UPPER(role.organization.id) = UPPER(:orgId) ")
	RoleEntity findRoleByOrganizations(@Param("orgId") Long orgId, @Param("roleName") String roleName);
}
