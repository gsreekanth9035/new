package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserStudentIDEntity;

@Repository
public interface UserStudentIDRepository extends JpaRepository<UserStudentIDEntity, Long> {
	
	@Query("SELECT studentID FROM UserStudentIDEntity studentID WHERE studentID.user.id = (:userid)")
	UserStudentIDEntity findByUserID(@Param("userid") Long userid);

}
