package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.UserBloodTypeEntity;
import com.meetidentity.usermanagement.web.rest.model.TypeDescription;

public interface BloodTypeRepository  extends JpaRepository<UserBloodTypeEntity, Long> {

	@Query("SELECT  new com.meetidentity.usermanagement.web.rest.model.TypeDescription(bt.type, bt.description) FROM UserBloodTypeEntity bt "
			+ " WHERE UPPER(bt.organization.name) = UPPER(:orgName) AND bt.status = 0")
	List<TypeDescription> getBloodTypeByStatus(@Param("orgName") String orgName);
	
}
