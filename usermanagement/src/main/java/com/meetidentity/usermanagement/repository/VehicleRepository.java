package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.VehicleEntity;

@Repository
public interface VehicleRepository extends JpaRepository<VehicleEntity, Long> {

	@Query("SELECT vehicle FROM VehicleEntity vehicle WHERE UPPER(vehicle.licensePlateNumber) = UPPER(:licensePlateNumber) AND UPPER(vehicle.organization.name) = UPPER(:organizationName)")
	VehicleEntity findVehicleByLicensePlateNumber(@Param("organizationName") String organizationName, @Param("licensePlateNumber") String licensePlateNumber);

	@Query("SELECT vehicle FROM VehicleEntity vehicle WHERE UPPER(vehicle.vin) = UPPER(:vin) AND UPPER(vehicle.organization.name) = UPPER(:organizationName)")
	VehicleEntity findVehicleByVin(@Param("organizationName") String organizationName, @Param("vin") String vin);

	@Query("SELECT vehicle FROM VehicleEntity vehicle WHERE UPPER(vehicle.organization.name) = UPPER(:organizationName) AND UPPER(vehicle.id) = (SELECT vie.vehicle.id FROM VehicleInsuranceEntity vie WHERE vie.policyNumber =  UPPER(:policyNumber))")
	VehicleEntity findVehicleByPolicyNumber(@Param("organizationName") String organizationName, @Param("policyNumber") String policyNumber);
}
