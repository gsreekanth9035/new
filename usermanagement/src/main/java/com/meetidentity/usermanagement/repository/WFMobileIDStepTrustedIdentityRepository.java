package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.WFMobileIDStepTrustedIdentityEntity;

public interface WFMobileIDStepTrustedIdentityRepository extends JpaRepository<WFMobileIDStepTrustedIdentityEntity, Long> {

	@Query("SELECT wfMobileIDStepTrustedIdentity FROM WFMobileIDStepTrustedIdentityEntity wfMobileIDStepTrustedIdentity WHERE UPPER(wfMobileIDStepTrustedIdentity.organizationIdentities.organization.name) = UPPER(:organizationName) and UPPER(wfMobileIDStepTrustedIdentity.wfStep.workflow.name) = UPPER(:workflowName) ")
	java.util.List<WFMobileIDStepTrustedIdentityEntity> findSelfServiceTrustedIdentitiesByWorkflow(@Param("organizationName") String organizationName, @Param("workflowName") String workflowName); 
	
}
