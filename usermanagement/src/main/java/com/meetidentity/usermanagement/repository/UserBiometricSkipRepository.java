package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserBiometricSkipEntity;

@Repository
public interface UserBiometricSkipRepository extends JpaRepository<UserBiometricSkipEntity, Long> {
	
	
}
