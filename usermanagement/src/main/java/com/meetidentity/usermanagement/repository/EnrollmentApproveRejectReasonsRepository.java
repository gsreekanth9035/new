package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.EnrollmentApproveRejectReasonsEntity;

public interface EnrollmentApproveRejectReasonsRepository  extends JpaRepository<EnrollmentApproveRejectReasonsEntity, Long> {

	@Query("SELECT earr.reason FROM EnrollmentApproveRejectReasonsEntity earr "
			+ "WHERE  UPPER(earr.organization.name) = UPPER(:orgName) and earr.forApproval = 1 and earr.forRejection = 0  and earr.status = 1")
	List<String> getEnrollmentApprovalReasons(@Param("orgName") String orgName);
	
	@Query("SELECT earr.reason FROM EnrollmentApproveRejectReasonsEntity earr "
			+ "WHERE  UPPER(earr.organization.name) = UPPER(:orgName) and earr.forApproval = 0 and earr.forRejection = 1  and earr.status = 1")
	List<String> getEnrollmentRejectionReasons(@Param("orgName") String orgName);
}
