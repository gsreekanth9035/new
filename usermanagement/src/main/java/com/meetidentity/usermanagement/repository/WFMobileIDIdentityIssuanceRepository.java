package com.meetidentity.usermanagement.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WFMobileIDIdentityIssuanceEntity;

@Repository
public interface WFMobileIDIdentityIssuanceRepository extends JpaRepository<WFMobileIDIdentityIssuanceEntity, Long> {
	
	@Query("SELECT wmi FROM WFMobileIDIdentityIssuanceEntity wmi WHERE UPPER(wmi.friendlyName)=UPPER( :friendlyName) AND UPPER(wmi.wfStep.workflow.organization.name) =UPPER(:orgName)")
	List<WFMobileIDIdentityIssuanceEntity> getFriendlyName(@Param("friendlyName") String name,@Param("orgName") String organizationName);

}
