package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.OrgIdentityFieldConfigEntity;

@Repository
public interface OrgIdentityFieldConfigRepository extends JpaRepository<OrgIdentityFieldConfigEntity, Long> {

	@Query("SELECT oIdfe FROM OrgIdentityFieldConfigEntity oIdfe JOIN oIdfe.OrganizationIdentities oids "
			+ "JOIN oIdfe.identityField idField "
			+ "WHERE oids.organization.name = UPPER(:orgName) AND oids.identityType.name = UPPER(:identityTypeName) "
			+ "AND idField.name = UPPER(:identityFieldName) ")
	List<OrgIdentityFieldConfigEntity> findOrgFieldConfig(@Param("orgName") String orgName,
			@Param("identityTypeName") String identityTypeName, @Param("identityFieldName") String identityFieldName);
}
