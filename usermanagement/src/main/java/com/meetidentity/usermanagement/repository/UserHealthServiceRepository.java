package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserHealthServiceEntity;



@Repository
public interface UserHealthServiceRepository extends JpaRepository<UserHealthServiceEntity, Long> {
	
	@Query("SELECT hs FROM UserHealthServiceEntity hs WHERE hs.user.id = (:userid)")
	UserHealthServiceEntity findByUserID(@Param("userid") Long userid);

}
