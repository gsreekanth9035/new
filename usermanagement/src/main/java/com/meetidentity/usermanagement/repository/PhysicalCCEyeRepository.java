package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.PhysicalCCEyeEntity;
import com.meetidentity.usermanagement.web.rest.model.TypeDescription;

public interface PhysicalCCEyeRepository  extends JpaRepository<PhysicalCCEyeEntity, Long> {

	@Query("SELECT  new com.meetidentity.usermanagement.web.rest.model.TypeDescription(pcce.type, pcce.description)  FROM PhysicalCCEyeEntity pcce "
			+ "WHERE UPPER(pcce.organization.name) = UPPER(:orgName) and pcce.status = 0")
	List<TypeDescription> getPhysicalCCEyeTypesByOrgName(@Param("orgName") String orgName);
	
}
