package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserHealthIDEntity;



@Repository
public interface UserHealthIDRepository extends JpaRepository<UserHealthIDEntity, Long> {
	
	@Query("SELECT hid FROM UserHealthIDEntity hid WHERE hid.user.id = (:userid)")
	UserHealthIDEntity findByUserID(@Param("userid") Long userid);

}
