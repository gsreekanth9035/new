package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.PushNotificationDeviceEntity;

@Repository
public interface  PushNotificationDeviceRepository extends JpaRepository<PushNotificationDeviceEntity, Long> {
	
	List<PushNotificationDeviceEntity> findByUserName(@Param("user_name") String userName);
	

	@Query("SELECT pnde  FROM PushNotificationDeviceEntity pnde where pnde.uimsId = :uimsId ")
	PushNotificationDeviceEntity findByUIMSID(@Param("uimsId") String uimsId);

}
