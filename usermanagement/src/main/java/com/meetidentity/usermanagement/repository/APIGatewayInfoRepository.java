package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.APIGatewayInfoEntity;

public interface APIGatewayInfoRepository  extends JpaRepository<APIGatewayInfoEntity, Long> {

	@Query("SELECT aPIGatewayInfo FROM APIGatewayInfoEntity aPIGatewayInfo "
			+ "WHERE UPPER(aPIGatewayInfo.organizationEntity.name) = UPPER(:orgName)")
	APIGatewayInfoEntity getAPIGatewayInfoByOrgName(@Param("orgName") String orgName);
	
}
