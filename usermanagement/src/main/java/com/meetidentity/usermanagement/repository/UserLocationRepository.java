package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserLocationEntity;
import com.meetidentity.usermanagement.service.UserLocationCustomService;
import com.vividsolutions.jts.geom.Point;


@Repository
public interface UserLocationRepository extends JpaRepository<UserLocationEntity, Long>, UserLocationCustomService { 

	UserLocationEntity findByName(String name);

	@Query(value = "SELECT se FROM UserLocationEntity se WHERE within(se.location, :bounds) = true ")
    List<UserLocationEntity> findWithinLocation(@Param("bounds") Point bounds);

	@Query(value = "SELECT se FROM UserLocationEntity se WHERE se.user.id IN (:markedUserIDs)")
	List<UserLocationEntity>  findByID(@Param("markedUserIDs")List<Long> markedUserIDs);

	@Query(value = "SELECT se FROM UserLocationEntity se WHERE se.user.id = :userID")
	UserLocationEntity  findByUserID(@Param("userID") Long userID);

	/*
	 * @Query("SELECT * FROM UserLocationEntity userlocation WHERE UPPER(userlocation.name) = UPPER(:roleName) AND UPPER(role.organization.name) = UPPER(:organizationName) "
	 * ) RoleEntity findRoleByOrganization(@Param("organizationName") String
	 * organizationName, @Param("roleName") String roleName);
	 * 
	 * @Query("SELECT role FROM RoleEntity role WHERE UPPER(role.workflow.name) = UPPER(:workflowName) AND UPPER(role.organization.name) = UPPER(:organizationName) "
	 * ) RoleEntity findRoleByWorkflow(@Param("organizationName") String
	 * organizationName, @Param("workflowName") String workflowName);
	 * 
	 * @Query("SELECT role FROM RoleEntity role WHERE UPPER(role.organization.name) = UPPER(:organizationName) "
	 * ) List<RoleEntity> findAllRolesByOrganization(@Param("organizationName")
	 * String organizationName);
	 */
}
