package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.DeviceTypeActionsMappingEntity;

@Repository
public interface DeviceTypeActionsMappingRepository extends JpaRepository<DeviceTypeActionsMappingEntity, Long> {

	@Query("SELECT rda.role.name, dt.name, dtam.deviceActions.name, dtam.isConnected, das.deviceStatus.name FROM "
			+ "DeviceTypeActionsMappingEntity dtam " + "JOIN DeviceTypeEntity dt ON dtam.deviceType.id = dt.id "
			+ "JOIN RoleDeviceActionsEntity rda ON dtam.deviceActions.id = rda.deviceActions.id "
			+ "JOIN  DeviceActionStatusRelationEntity das ON dtam.deviceActions.id = das.deviceActions.id "
			+ "JOIN DeviceActionsEntity da ON dtam.deviceActions.id = da.id "
			+ "WHERE rda.role.name = UPPER(:roleName) AND rda.role.organization.name = UPPER(:organizationName) AND dt.id IN (SELECT rdt.deviceType.id "
			+ "FROM RoleDeviceTypeEntity rdt WHERE rdt.role.name = UPPER(:roleName)) ORDER BY dt.id")
	List<Object[]> getAllDevicesActionsByStatusAndRole(@Param("organizationName") String organizationName, @Param("roleName") String roleName);
}
