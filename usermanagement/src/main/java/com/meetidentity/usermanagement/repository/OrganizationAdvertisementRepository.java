package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.OrganizationAdvertisementEntity;

public interface OrganizationAdvertisementRepository extends JpaRepository<OrganizationAdvertisementEntity, Long> {

	/*
	 * @Query("SELECT oa FROM OrganizationAdvertisementEntity oa WHERE oa.organizationEntity.id = :organizationId "
	 * ) List<OrganizationAdvertisementEntity>
	 * findrganizationAdvertisementByOrgId(@Param("organizationId") String
	 * organizationId);
	 */
	
	@Query("SELECT oa FROM OrganizationAdvertisementEntity oa WHERE UPPER(oa.organizationEntity.name) = UPPER(:organizationName) AND oa.status = 0")	
	List<OrganizationAdvertisementEntity> findrganizationAdvertisementByOrgName(@Param("organizationName") String organizationName);
	

}
