package com.meetidentity.usermanagement.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WFStepEntity;

@Repository
public interface WFStepRepository extends JpaRepository<WFStepEntity, Long> { 

	@Query("SELECT wfStp FROM WFStepEntity wfStp JOIN FETCH wfStp.wfStepDef wfStpDef WHERE UPPER(wfStp.workflow.name) = UPPER(:workflowName) AND UPPER(wfStp.workflow.organization.name) = UPPER(:organizationName) AND wfStp.status = 'ACTIVE' ORDER BY wfStp.wfStepDef.stepDefOrder ")
	List<WFStepEntity> findStepsByWorkflow(@Param("organizationName") String organizationName, @Param("workflowName") String workflowName);
	
	@Query("SELECT wfStp FROM WFStepEntity wfStp "
			+ "JOIN FETCH wfStp.wfStepDef wfStpDef WHERE UPPER(wfStp.wfStepDef.name) = UPPER(:stepName) AND UPPER(wfStp.workflow.name) = UPPER(:workflowName) AND UPPER(wfStp.workflow.organization.name) = UPPER(:organizationName) ORDER by wfStp.id ")
	Optional<WFStepEntity> findStepDetailByName(@Param("organizationName") String organizationName, @Param("workflowName") String workflowName, @Param("stepName") String stepName);
	
}
