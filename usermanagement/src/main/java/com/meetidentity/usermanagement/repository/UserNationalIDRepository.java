package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.UserNationalIdEntity;

public interface UserNationalIDRepository extends JpaRepository<UserNationalIdEntity, Long>{
	
	@Query("SELECT nid FROM UserNationalIdEntity nid WHERE nid.user.id = (:userid)")
	UserNationalIdEntity findByUserID(@Param("userid") Long userid);

}
