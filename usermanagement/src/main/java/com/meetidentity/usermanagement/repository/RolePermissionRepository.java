package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.RolePermissionEntity;

@Repository
public interface RolePermissionRepository extends JpaRepository<RolePermissionEntity, Long> {
	
	@Query("SELECT rp  FROM RolePermissionEntity rp WHERE UPPER(rp.role.name) = UPPER(:roleName) AND UPPER(rp.role.organization.name) = UPPER(:organizationName)")
	List<RolePermissionEntity> findAllWithMapResult (@Param("organizationName") String organizationName, @Param("roleName") String roleName);

}
