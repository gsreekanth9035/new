package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserDrivingLicenseEntity;

@Repository
public interface UserDrivingLicenseRepository extends JpaRepository<UserDrivingLicenseEntity, Long> {

	@Query("SELECT dl FROM UserDrivingLicenseEntity dl WHERE dl.user.id = (:userid)")
	UserDrivingLicenseEntity findByUserID(@Param("userid") Long userid);


}
