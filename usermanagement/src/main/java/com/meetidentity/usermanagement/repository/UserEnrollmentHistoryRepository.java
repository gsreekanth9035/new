package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserEnrollmentHistoryEntity;

@Repository
public interface UserEnrollmentHistoryRepository extends JpaRepository<UserEnrollmentHistoryEntity, Long> {
	
	@Modifying
	@Query("update UserEnrollmentHistoryEntity ueh set ueh.approvalStatus = 'Approved', ueh.reason = :reason  "
			+ " WHERE ueh.user.referenceName = :userName and UPPER(ueh.user.organization.name) = UPPER(:organizationName)")
	void approve(@Param("organizationName") String organizationName,@Param("userName")  String userName,@Param("reason")  String reason);

	@Modifying
	@Query("update UserEnrollmentHistoryEntity ueh set ueh.approvalStatus = 'Rejected', ueh.reason = :reason  "
			+ " WHERE ueh.user.referenceName = :userName and UPPER(ueh.user.organization.name) = UPPER(:organizationName)")
	void reject(@Param("organizationName") String organizationName,@Param("userName") String userName,@Param("reason") String reason);

	@Query("SELECT ueh FROM UserEnrollmentHistoryEntity ueh WHERE ueh.user.id = :userId")
	List<UserEnrollmentHistoryEntity> getByUserId(@Param("userId") Long userId);
}
