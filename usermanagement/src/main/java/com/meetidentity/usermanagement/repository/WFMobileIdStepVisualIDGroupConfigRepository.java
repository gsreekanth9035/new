package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.WFMobileIdStepVisualIDGroupConfigEntity;

public interface WFMobileIdStepVisualIDGroupConfigRepository extends JpaRepository<WFMobileIdStepVisualIDGroupConfigEntity, Long> { 
	@Modifying
	@Query(value="UPDATE wf_mobile_id_step_visual_id_group_config wm JOIN wf_mobile_id_identity_issuance wt ON wt.id=wf_mobile_identity_issuance_id JOIN wf_step ws ON ws.id= wt.wf_step_id JOIN workflow wf ON wf.id=ws.workflow_id INNER JOIN organization o ON o.id=wf.organization_id SET wm.reference_group_Name = :groupName WHERE wm.reference_group_id= :groupId AND o.name = :organizationName", nativeQuery = true)
	void updateGroupName(@Param("groupId") String groupId, @Param("groupName") String groupName, @Param("organizationName") String organizationName);
}
