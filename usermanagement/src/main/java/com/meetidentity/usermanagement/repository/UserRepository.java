package com.meetidentity.usermanagement.repository;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long>, JpaSpecificationExecutor<UserEntity>  {

	@Query("SELECT user FROM UserEntity user WHERE UPPER(user.referenceName) = UPPER(:referenceName) AND UPPER(user.organization.name)=UPPER(:organizationName)")
	UserEntity getUserByName(@Param("referenceName") String referenceName, @Param("organizationName") String organizationName);

	@Query("SELECT user FROM UserEntity user WHERE UPPER(user.organization.name)=UPPER(:organizationName)")
	List<UserEntity> findUserByOrganization(@Param("organizationName") String organizationName);

	@Query("SELECT user FROM UserEntity user WHERE UPPER(user.organization.name)=UPPER(:organizationName) AND user.status=(:status)")
	List<UserEntity> findPendingEnrollemntStat(@Param("organizationName") String organizationName, @Param("status") String status);

	@Query("SELECT user FROM UserEntity user WHERE UPPER(user.organization.name)=UPPER(:organizationName) AND user.status=(:status)")
	List<UserEntity> findPendingIssuanceStat(@Param("organizationName") String organizationName, @Param("status") String status);

	
	@Query("SELECT user FROM UserEntity user WHERE UPPER(user.organization.name)=UPPER(:organizationName) AND "
			+ "(user.createdDate BETWEEN :startDate AND :endDate ) AND "
			+ "(:userName is null or user.name like %:userName%) AND "
			+ "(:status is null or  user.status IN (:status))")
	List<UserEntity> findUsersByFilters(@Param("organizationName") String organizationName,
			@Param("userName") String userName, @Param("status") List<String> status,
			@Param("startDate") Instant startDate, @Param("endDate") Instant endDate);

	@Query("SELECT user.id FROM UserEntity user WHERE  UPPER(user.organization.name) = UPPER(:organizationName) AND user.referenceName = UPPER(:referenceName)")
	Long getUserIDByName(@Param("organizationName") String organizationName, @Param("referenceName") String referenceName);

	@Query("SELECT user.referenceName FROM UserEntity user WHERE user.id = :userid  AND  UPPER(user.organization.name)=UPPER(:organizationName) ")
	String getUserNameByUserID(@Param("userid") long userid, @Param("organizationName") String organizationName);


	@Query("SELECT user FROM UserEntity user WHERE UPPER(user.organization.name)=UPPER(:organizationName) AND user.id IN :userIDs AND user.status!=(:status)")
	List<UserEntity>  updateTheseUsersUnderOrganization(@Param("organizationName") String organizationName, @Param("userIDs") Set<Long> userIDs, @Param("status") String status);

	@Query("SELECT user FROM UserEntity user WHERE UPPER(user.organization.name)=UPPER(:organizationName) AND user.id = :userID AND UPPER(user.status) != UPPER(:status)")
	UserEntity  getUserwithoutPendingApprovalState(@Param("organizationName") String organizationName, @Param("userID") Long userID, @Param("status") String status);

	
	@Query("select count(distinct ue) from UserEntity ue where  UPPER(ue.organization.name) = UPPER(:organizationName) AND ue.status NOT IN (:userstatus) AND ue.referenceName IN :userNames ")
	Long getUserCountByStatus(@Param("userNames") List<String> userNames, @Param("organizationName") String organizationName, @Param("userstatus") List<String> userstatus);
	
	@Query("select count(distinct ue) from UserEntity ue where  UPPER(ue.organization.name) = UPPER(:organizationName) AND UPPER(ue.userStatus) = UPPER(:userstatus) AND ue.referenceName IN :userNames ")
	Long getUserCountByUserStatus(@Param("userNames") List<String> userNames, @Param("organizationName") String organizationName, @Param("userstatus") String userstatus);
	
	@Query("SELECT user.id FROM UserEntity user WHERE user.id IN (:userIdList) AND UPPER(user.organization.name)=UPPER(:organizationName)")
	List<Long> findOrgUserIds(@Param("userIdList") List<Long> userIdList, @Param("organizationName") String organizationName);

	@Query("SELECT user FROM UserEntity user WHERE UPPER(user.organization.name)=UPPER(:organizationName) AND (UPPER(user.referenceName) = UPPER(:userNameOrEmail) OR UPPER(user.userAttribute.email) = UPPER(:userNameOrEmail)) ")
	UserEntity getUserByUserNameOrEmail(@Param("organizationName") String organizationName, @Param("userNameOrEmail") String userNameOrEmail);
}
