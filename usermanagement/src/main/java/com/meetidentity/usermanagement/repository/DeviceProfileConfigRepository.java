package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.DeviceProfileConfigEntity;

public interface DeviceProfileConfigRepository extends JpaRepository<DeviceProfileConfigEntity, Long> {

	@Query("SELECT dpc FROM DeviceProfileConfigEntity dpc WHERE dpc.configValue.id = :configValueId")
	DeviceProfileConfigEntity getDevProfConfigByConfigValueID(@Param("configValueId") Long configValueId);

	@Query("SELECT dpc.isMobileId, dpc.isPlasticId, dpc.isRfidCard, dpc.isAppletLoadingRequired FROM DeviceProfileConfigEntity dpc WHERE dpc.configValue.id = :configValueId")
	List<Object[]> getDevProfConfig( @Param("configValueId") Long configValueId);
}
