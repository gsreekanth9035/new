package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.FingerprintDeviceConfigEntity;

public interface FingerprintDeviceConfigRepository extends JpaRepository<FingerprintDeviceConfigEntity, Long> {

	@Query("SELECT fpDeviceConfig FROM FingerprintDeviceConfigEntity fpDeviceConfig WHERE UPPER(fpDeviceConfig.deviceName) = UPPER(:deviceName) AND UPPER(fpDeviceConfig.organization.name)=UPPER(:organizationName) AND UPPER(fpDeviceConfig.deviceManufacturer) = UPPER(:deviceManufacturer) ")
	FingerprintDeviceConfigEntity getByDeviceName(@Param("deviceName") String deviceName,@Param("organizationName") String organizationName,@Param("deviceManufacturer") String deviceManufacturer);

	@Query("SELECT fpDeviceConfig FROM FingerprintDeviceConfigEntity fpDeviceConfig WHERE UPPER(fpDeviceConfig.deviceManufacturer) = UPPER(:deviceManufacturer) AND UPPER(fpDeviceConfig.organization.name)=UPPER(:organizationName)")
	List<FingerprintDeviceConfigEntity> getByDeviceManufacturer(@Param("deviceManufacturer") String deviceManufacturer,@Param("organizationName") String organizationName);
	
}
