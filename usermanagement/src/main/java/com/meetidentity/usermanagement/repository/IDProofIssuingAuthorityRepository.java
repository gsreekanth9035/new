package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.IDProofIssuingAuthorityEntity;
@Repository
public interface IDProofIssuingAuthorityRepository extends JpaRepository<IDProofIssuingAuthorityEntity, Long> {
	
	@Query("SELECT idPIA FROM IDProofIssuingAuthorityEntity idPIA WHERE idPIA.organizationIdentity.id = UPPER(:idProofTypeId) AND UPPER(idPIA.organizationIdentity.organization.name) = UPPER(:organizationName) ")
	List<IDProofIssuingAuthorityEntity> getByIDProofTypeID1(@Param("organizationName") String organizationName, @Param("idProofTypeId") Long idProofTypeId);
}
