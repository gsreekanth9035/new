package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WFStepDefEntity;

@Repository
public interface WFStepDefRepository extends JpaRepository<WFStepDefEntity, Long> { 

	WFStepDefEntity findByName(@Param("name") String name);
	
}
