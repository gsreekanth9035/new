package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.HealthGroupPlanEntity;
import com.meetidentity.usermanagement.web.rest.model.TypeDescription;

public interface HealthGroupPlanRepository  extends JpaRepository<HealthGroupPlanEntity, Long> {

	@Query("SELECT  new com.meetidentity.usermanagement.web.rest.model.TypeDescription(hgp.type, hgp.description) FROM HealthGroupPlanEntity hgp "
			+ "WHERE UPPER(hgp.organization.name) = UPPER(:orgName) and hgp.identityTpeId.id = :identity_type_id and hgp.status = 0")
	List<TypeDescription> getHealthGroupPlansByOrgName(@Param("orgName") String orgName, @Param("identity_type_id")long identity_type_id);
	
}
