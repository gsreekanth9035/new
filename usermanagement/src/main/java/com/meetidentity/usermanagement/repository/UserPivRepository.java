package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserPivEntity;

@Repository
public interface UserPivRepository extends JpaRepository<UserPivEntity, Long> {
	
	@Query("SELECT piv FROM UserPivEntity piv WHERE piv.user.id = (:userid)")
	UserPivEntity findByUserID(@Param("userid") Long userid);

}
