package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WFStepRegistrationConfigEntity;

@Repository
public interface WFStepRegistrationConfigRepository extends JpaRepository<WFStepRegistrationConfigEntity, Long> {

}
