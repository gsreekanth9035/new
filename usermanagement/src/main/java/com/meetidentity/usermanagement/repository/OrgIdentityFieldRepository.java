package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.OrgIdentityFieldEntity;

@Repository
public interface OrgIdentityFieldRepository extends JpaRepository<OrgIdentityFieldEntity, Long> {

	@Query("SELECT orgIdentFld FROM OrgIdentityFieldEntity orgIdentFld JOIN FETCH orgIdentFld.identityField WHERE UPPER(orgIdentFld.organization.name) = UPPER(:organizationName) AND "
			+ "(orgIdentFld.identityField.identityType.name)= UPPER(:identityTypeName) AND UPPER(orgIdentFld.identityField.stakeHolder.value)= UPPER(:stakeHolder)")
	public List<OrgIdentityFieldEntity> findUserOrgIdentityFieldByOrganization(
			@Param("organizationName") String organizationName, @Param("identityTypeName") String identityTypeName,  @Param("stakeHolder") String stakeHolder);

}
