package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserEmployeeIDEntity;

@Repository
public interface UserEmployeeIDRepository extends JpaRepository<UserEmployeeIDEntity, Long> {
	
	@Query("SELECT empID FROM UserEmployeeIDEntity empID WHERE empID.user.id = (:userid)")
	UserEmployeeIDEntity findByUserID(@Param("userid") Long userid);

}
