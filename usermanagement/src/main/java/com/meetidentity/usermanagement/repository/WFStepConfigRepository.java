package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.WFStepConfigEntity;

public interface WFStepConfigRepository extends JpaRepository<WFStepConfigEntity, Long> {

	@Query("SELECT wfStpConfg FROM WFStepConfigEntity wfStpConfg JOIN wfStpConfg.configValue configValue "
			+ "WHERE wfStpConfg.wfStep.id = :wfStepId AND configValue.config.name = UPPER(:configName)")
	WFStepConfigEntity findWorkflowStepConfigByConfigName(@Param("wfStepId") Long wfStepId,
			@Param("configName") String configName);

}
