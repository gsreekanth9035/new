package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.DeviceTypeEntity;

public interface DeviceTypeRepository extends JpaRepository<DeviceTypeEntity, Long> {

	@Query("SELECT dt FROM DeviceTypeEntity dt WHERE dt.visualApplicable = true")
	List<DeviceTypeEntity> findVisualApplicableDevices();

	DeviceTypeEntity findByName(@Param("name") String name);

}
