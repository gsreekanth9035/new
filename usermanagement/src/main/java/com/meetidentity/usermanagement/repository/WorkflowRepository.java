package com.meetidentity.usermanagement.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WFGroupEntity;
import com.meetidentity.usermanagement.domain.WorkflowEntity;
import com.meetidentity.usermanagement.web.rest.model.Group;

@Repository
public interface WorkflowRepository extends JpaRepository<WorkflowEntity, Long> { 

	@Query("SELECT wf FROM WorkflowEntity wf WHERE UPPER(wf.role.name) = UPPER(:roleName) AND UPPER(wf.organization.name) = UPPER(:organizationName)")
	Optional<WorkflowEntity> findWorkflowByRole(@Param("organizationName") String organizationName, @Param("roleName") String roleName);

	@Query("SELECT wf FROM WorkflowEntity wf WHERE UPPER(wf.organization.name) = UPPER(:organizationName) AND wf.status = 'ACTIVE'")
	Page<WorkflowEntity> findWorkflowsByOrganization(@Param("organizationName") String organizationName, Pageable pageable);
	
	@Query("SELECT wf FROM WorkflowEntity wf WHERE UPPER(wf.name) = UPPER(:workflowName) AND UPPER(wf.organization.name) = UPPER(:organizationName)")
	WorkflowEntity findByName(@Param("organizationName") String organizationName, @Param("workflowName") String workflowName);

	@Query("SELECT wfDevProf.deviceProfile.name FROM WorkflowEntity wf JOIN wf.wfDeviceProfiles wfDevProf WHERE UPPER(wf.name) = UPPER(:workflowName) AND UPPER(wf.organization.name) = UPPER(:organizationName)")
	List<String> findWorkflowDeviceProfiles(@Param("organizationName") String organizationName, @Param("workflowName") String workflowName);
	
	WorkflowEntity findByName(@Param("name") String name);
	
	@Query("SELECT wf FROM WorkflowEntity wf JOIN wf.wfGroups wfGroup "
			+ "JOIN wf.wfSteps wfSteps "
			+ "JOIN wfSteps.wfMobileIDStepIdentityConfigs mobileIdentity "
			+ "JOIN mobileIdentity.wfMobileIdStepVisualIDGroupConfigs mobileVds "
			+ "WHERE wfGroup.visualTemplate.id = :visualTemplateId OR mobileVds.visualTemplate.id = :visualTemplateId AND UPPER(wf.organization.name) = UPPER(:organizationName)")
	List<WorkflowEntity> getByVisualTemplateId(@Param("organizationName") String organizationName, @Param("visualTemplateId") Long visualTemplateId);

	@Query("SELECT wfge FROM WFGroupEntity wfge WHERE UPPER(wfge.workflow.organization.name) = UPPER(:organizationName) AND (wfge.referenceGroupId) = (:groupId) ")
	WFGroupEntity findWorkflowGroup(@Param("organizationName") String organizationName, @Param("groupId") String groupId);

	@Query("SELECT new com.meetidentity.usermanagement.web.rest.model.Group(wfge.referenceGroupId, wfge.referenceGroupName)  FROM WFGroupEntity wfge WHERE UPPER(wfge.workflow.organization.name) = UPPER(:organizationName) ")
	List<Group> findGroupsWithWorkflow(@Param("organizationName") String organizationName);

	@Query("SELECT wf FROM WorkflowEntity wf JOIN wf.wfDeviceProfiles wfDevProfs WHERE wfDevProfs.deviceProfile.id = :deviceProfileId AND UPPER(wf.organization.name) = UPPER(:organizationName)")
	List<WorkflowEntity> getByDeviceProfileId(@Param("organizationName") String organizationName, @Param("deviceProfileId") Long deviceProfileId);
		
}
