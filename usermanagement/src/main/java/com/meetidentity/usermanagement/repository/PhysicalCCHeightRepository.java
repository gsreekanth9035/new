package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.PhysicalCCHeightEntity;
import com.meetidentity.usermanagement.web.rest.model.TypeDescription;

public interface PhysicalCCHeightRepository  extends JpaRepository<PhysicalCCHeightEntity, Long> {

	@Query("SELECT  new com.meetidentity.usermanagement.web.rest.model.TypeDescription(pcch.height, pcch.description) FROM PhysicalCCHeightEntity pcch "
			+ "WHERE UPPER(pcch.organization.name) = UPPER(:orgName) and pcch.status = 0")
	List<TypeDescription> getPhysicalCCHeightTypesByOrgName(@Param("orgName") String orgName);
	
}
