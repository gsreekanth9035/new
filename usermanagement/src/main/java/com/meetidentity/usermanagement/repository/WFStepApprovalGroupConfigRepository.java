package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.WFStepApprovalGroupConfigEntity;

public interface WFStepApprovalGroupConfigRepository extends JpaRepository<WFStepApprovalGroupConfigEntity, Long> { 
	@Modifying
	@Query(value="UPDATE wf_step_approval_group_config wa JOIN wf_step ws ON ws.id= wf_step_id Join workflow wf ON wf.id=ws.workflow_id INNER JOIN organization o ON o.id=wf.organization_id SET wa.reference_approval_group_Name = :groupName WHERE wa.reference_approval_group_id= :groupId AND o.name = :organizationName", nativeQuery = true)
	void updateGroupName(@Param("groupId") String groupId, @Param("groupName") String groupName, @Param("organizationName") String organizationName );
}
