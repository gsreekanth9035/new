package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.ConfigValueEntity;

@Repository
public interface ConfigValueRepository extends JpaRepository<ConfigValueEntity, Long> {

	@Query("SELECT cv from ConfigValueEntity cv WHERE UPPER(cv.config.name) = UPPER(:configName) AND UPPER(cv.config.organization.name) = UPPER(:organizationName)")
	List<ConfigValueEntity> findByConfigName(@Param("organizationName") String organizationName, @Param("configName") String configName);

	@Query("SELECT cv from ConfigValueEntity cv WHERE parentConfigValueEntity.id = :configValueId AND UPPER(cv.config.organization.name) = UPPER(:organizationName)")
	List<ConfigValueEntity> findByParentConfigValue(@Param("organizationName") String organizationName, @Param("configValueId") Long configValueId);

	@Query("SELECT cv from ConfigValueEntity cv WHERE UPPER(cv.value) = UPPER(:value) AND UPPER(cv.config.organization.name) = UPPER(:organizationName)")
	ConfigValueEntity findByOrgAndValue(@Param("organizationName") String organizationName, @Param("value") String value);
	
	@Query("SELECT cv from ConfigValueEntity cv WHERE UPPER(cv.config.name) = UPPER(:configName) AND UPPER(cv.value) = UPPER(:configValue) AND UPPER(cv.config.organization.name) = UPPER(:organizationName)")
	ConfigValueEntity findByConfigNameAndValue(@Param("organizationName") String organizationName, @Param("configName") String configName, 
			@Param("configValue") String configValue);
	
	@Query("SELECT cv from ConfigValueEntity cv WHERE cv.value = UPPER(:value) AND UPPER(cv.config.organization.name) = UPPER(:organizationName) AND parentConfigValueEntity.value = UPPER(:parentConfigValue)")
	ConfigValueEntity findByConfigValueAndParentConfigValue(@Param("organizationName") String organizationName, @Param("value") String value, @Param("parentConfigValue") String parentConfigValue);

}
