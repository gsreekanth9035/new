package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserAttributeEntity;

@Repository
public interface UserAttributeRepository extends JpaRepository<UserAttributeEntity, Long> {
	
	
}
