package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.IDProofTypeEntity;

public interface IDProofTypeRepository extends JpaRepository<IDProofTypeEntity, Long>{
	
	@Query("SELECT idPTE FROM IDProofTypeEntity idPTE WHERE UPPER(idPTE.organization.name) = UPPER(:organizationName)")
	List<IDProofTypeEntity> findAllByOrganization(@Param("organizationName") String organizationName);

	@Query("SELECT idPTE FROM IDProofTypeEntity idPTE WHERE UPPER(idPTE.organization.name) = UPPER(:organizationName) AND UPPER(idPTE.type) = UPPER(:type)")
	IDProofTypeEntity findIdPrrofTypeByTypeAndOrg(@Param("organizationName") String organizationName, @Param("type") String type);
}
