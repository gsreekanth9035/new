package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.CustomerBrandingEntity;

public interface CustomerBrandingRepository extends JpaRepository<CustomerBrandingEntity, Long> {
	
	@Query("SELECT cb FROM CustomerBrandingEntity cb WHERE cb.organizationEntity.id = :organizationId")
	CustomerBrandingEntity findCustomerBrandingByOrgId(@Param("organizationId") Long organizationId);
	
	@Query("SELECT cb FROM CustomerBrandingEntity cb WHERE UPPER(cb.organizationEntity.name) = UPPER(:organizationName)")
	CustomerBrandingEntity findCustomerBrandingByOrgName(@Param("organizationName") String organizationName);
	

}
