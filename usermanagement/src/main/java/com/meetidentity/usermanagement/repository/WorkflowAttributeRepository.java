package com.meetidentity.usermanagement.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WorkflowAttributeEntity;
import com.meetidentity.usermanagement.domain.WorkflowEntity;

@Repository
public interface WorkflowAttributeRepository extends JpaRepository<WorkflowAttributeEntity, Long> { 

	@Query("SELECT wfa FROM WorkflowAttributeEntity wfa WHERE UPPER(wfa.workflow.id) = UPPER(:workflowId) AND UPPER(wfa.organization.id) = UPPER(:organizationId)")
	List<WorkflowAttributeEntity> checkIfDefaultWorkflowExists(@Param("workflowId") Long workflowId, @Param("organizationId") Long organizationId);

	@Query("SELECT wfa FROM WorkflowAttributeEntity wfa WHERE UPPER(wfa.organization.name) = UPPER(:organizationName) AND UPPER(wfa.name) = UPPER(:attributeName)")
	List<WorkflowAttributeEntity> checkIfDefaultWorkflowExists(@Param("organizationName") String organizationName, @Param("attributeName") String attributeName);

	@Query("SELECT wfa.workflow FROM WorkflowAttributeEntity wfa WHERE UPPER(wfa.organization.name) = UPPER(:organizationName) AND UPPER(wfa.name) = UPPER(:attributeName)")
	Optional<WorkflowEntity> findDefaultWorkflow(@Param("organizationName") String organizationName, @Param("attributeName") String attributeName);
	
}
