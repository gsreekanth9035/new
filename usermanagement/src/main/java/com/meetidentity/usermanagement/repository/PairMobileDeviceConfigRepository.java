package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.PairMobileDeviceConfigEntity;

@Repository
public interface PairMobileDeviceConfigRepository extends JpaRepository<PairMobileDeviceConfigEntity, Long> {
	@Query("SELECT pmdconfig FROM PairMobileDeviceConfigEntity pmdconfig WHERE UPPER(pmdconfig.organization.name) = UPPER(:organizationName) ")
	PairMobileDeviceConfigEntity findPMDConfigByOrg(@Param("organizationName") String organizationName);
	

	@Query("SELECT pmdconfig FROM PairMobileDeviceConfigEntity pmdconfig WHERE UPPER(pmdconfig.organization.id) = :organizationID ")
	PairMobileDeviceConfigEntity findPMDConfigByOrg(@Param("organizationID") Long organizationID);
}
