package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.OrganizationIdentitiesEntity;

@Repository
public interface OrganizationIdentitiesRepository extends JpaRepository<OrganizationIdentitiesEntity, Long> {

	@Query("SELECT orgIds FROM OrganizationIdentitiesEntity orgIds JOIN orgIds.identityType idType WHERE UPPER(orgIds.organization.name) = UPPER(:organizationName) AND orgIds.canBeIssued = UPPER(:canBeIssued)")
	List<OrganizationIdentitiesEntity> getOrgIssuingIdentities(@Param("organizationName") String organizationName,
			@Param("canBeIssued") Boolean canBeIssued);

	@Query("SELECT orgIds FROM OrganizationIdentitiesEntity orgIds JOIN orgIds.identityType idType WHERE UPPER(orgIds.organization.name) = UPPER(:organizationName) AND orgIds.isTrusted = UPPER(:isTrusted)")
	List<OrganizationIdentitiesEntity> getOrgTrustedExistingIdentities(@Param("organizationName") String organizationName,
			@Param("isTrusted") Boolean isTrusted);

	@Query("SELECT issuedIdentityExpiry FROM OrganizationIdentitiesEntity orgIds where orgIds.identityType.id = :identityTypeId AND (orgIds.organization.name) = UPPER(:organizationName)")
	Long getOrganizationIdentityExpiryDate(@Param("organizationName") String organizationName, @Param("identityTypeId") Long identityTypeId);
	
	@Query("SELECT orgIds FROM OrganizationIdentitiesEntity orgIds where UPPER(orgIds.identityType.name) = UPPER(:identityTypeName) AND (orgIds.organization.name) = UPPER(:organizationName)")
	OrganizationIdentitiesEntity getByIdentityTypeNameAndOrg(@Param("organizationName") String organizationName, @Param("identityTypeName") String identityTypeName);
}
