package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserAppearanceEntity;

@Repository
public interface UserAppearanceRepository extends JpaRepository<UserAppearanceEntity, Long> {
	
	
}
