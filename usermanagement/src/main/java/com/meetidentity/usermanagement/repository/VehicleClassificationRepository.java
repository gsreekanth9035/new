package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.VehicleClassificationEntity;

@Repository
public interface VehicleClassificationRepository extends JpaRepository<VehicleClassificationEntity, Long> {

	
	@Query("SELECT vehicleClassificationEntity FROM VehicleClassificationEntity vehicleClassificationEntity "
			+ "WHERE vehicleClassificationEntity.type = (:vehicleClassType)")
	VehicleClassificationEntity findOneByType(@Param("vehicleClassType")  String vehicleClassType);

	@Query("SELECT vehicleClassificationEntity FROM VehicleClassificationEntity vehicleClassificationEntity "
			+ "WHERE vehicleClassificationEntity.type = (:vehicleClassType) AND vehicleClassificationEntity.organization.name=UPPER(:organizationName)" )
	VehicleClassificationEntity findOneByTypeAndOrganization(@Param("vehicleClassType")  String vehicleClassType, @Param("organizationName")  String  organizationName);

	@Query("SELECT vehicleClassificationEntity FROM VehicleClassificationEntity vehicleClassificationEntity "
			+ "WHERE vehicleClassificationEntity.organization.name=UPPER(:organizationName)" )
	List<VehicleClassificationEntity> findByOrgName(@Param("organizationName")  String  organizationName);

}
