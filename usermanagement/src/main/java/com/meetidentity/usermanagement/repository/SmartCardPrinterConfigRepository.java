package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.SmartCardPrinterConfigEntity;

@Repository
public interface SmartCardPrinterConfigRepository extends JpaRepository<SmartCardPrinterConfigEntity, Long> {
	@Query("SELECT scprinterconfig FROM SmartCardPrinterConfigEntity scprinterconfig WHERE UPPER(scprinterconfig.organization.name) = UPPER(:organizationName) ")
	SmartCardPrinterConfigEntity findSmartCardPrinterConfigByOrg(@Param("organizationName") String organizationName);
}
