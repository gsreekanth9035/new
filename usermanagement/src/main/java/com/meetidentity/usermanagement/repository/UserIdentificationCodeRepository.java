package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserIdentificationCodeEntity;

@Repository
public interface UserIdentificationCodeRepository extends JpaRepository<UserIdentificationCodeEntity, Long> {
	
	@Query("SELECT uic FROM UserIdentificationCodeEntity uic "
			+ "WHERE UPPER(uic.uuid) = UPPER(:uuid) AND UPPER(uic.user.organization.name) = UPPER(:organizationName) AND uic.status = true")
	UserIdentificationCodeEntity getUserIdentificationCodeByUUID(@Param("uuid") String uuid, @Param("organizationName") String organizationName);
}
