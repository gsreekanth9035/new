package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserEmployeeIDEntity;
import com.meetidentity.usermanagement.domain.UserPersonalizedIDEntity;

@Repository
public interface UserPersonalizedIDRepository extends JpaRepository<UserPersonalizedIDEntity, Long> {
	
	@Query("SELECT pzID FROM UserPersonalizedIDEntity pzID WHERE pzID.user.id = (:userid)")
	UserPersonalizedIDEntity findByUserID(@Param("userid") Long userid);

}
