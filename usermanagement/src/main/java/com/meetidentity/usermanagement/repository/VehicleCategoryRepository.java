package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.VehicleCategoryEntity;

@Repository
public interface VehicleCategoryRepository extends JpaRepository< VehicleCategoryEntity, Long> {

	@Query("SELECT vehicleCategoryEntity FROM VehicleCategoryEntity vehicleCategoryEntity "
			+ "WHERE vehicleCategoryEntity.vehicleClassification.id = :vehicle_classification_id")
	List<VehicleCategoryEntity> findByVehicleClassificationID(@Param("vehicle_classification_id")  long vehicle_classification_id);
	
	@Query("SELECT vehicleCategoryEntity FROM VehicleCategoryEntity vehicleCategoryEntity "
			+ "WHERE vehicleCategoryEntity.vehicleClassification.type = UPPER(:vehicleClassType)")
	List<VehicleCategoryEntity> findByVehicleClassificationType(@Param("vehicleClassType")  String vehicleClassType);

}
