package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.UserVoterIDEntity;

public interface UserVoterIDRepository extends JpaRepository<UserVoterIDEntity, Long>{
	
	@Query("SELECT vid FROM UserVoterIDEntity vid WHERE vid.user.id = (:userid)")
	UserVoterIDEntity findByUserID(@Param("userid") Long userid);

}
