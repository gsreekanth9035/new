package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.PushNotificationConfigEntity;

public interface PushNotificationConfigRepository  extends JpaRepository<PushNotificationConfigEntity, Long> {

	@Query("SELECT pnc FROM PushNotificationConfigEntity pnc  WHERE UPPER(pnc.type) = UPPER(:type)")
	PushNotificationConfigEntity findCofigurationsByType(@Param("type") String type);

}
