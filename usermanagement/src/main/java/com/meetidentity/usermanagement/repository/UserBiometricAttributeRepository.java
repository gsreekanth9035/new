package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserBiometricAttributeEntity;

@Repository
public interface UserBiometricAttributeRepository extends JpaRepository<UserBiometricAttributeEntity, Long> {

}
