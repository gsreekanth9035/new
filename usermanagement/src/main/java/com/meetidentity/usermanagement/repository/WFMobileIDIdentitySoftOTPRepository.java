package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.WFMobileIDIdentitySoftOTPEntity;

@Repository
public interface WFMobileIDIdentitySoftOTPRepository extends JpaRepository<WFMobileIDIdentitySoftOTPEntity, Long> {

}
