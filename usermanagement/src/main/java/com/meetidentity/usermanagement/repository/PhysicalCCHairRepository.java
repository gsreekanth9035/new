package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.PhysicalCCHairEntity;
import com.meetidentity.usermanagement.web.rest.model.TypeDescription;

public interface PhysicalCCHairRepository  extends JpaRepository<PhysicalCCHairEntity, Long> {

	@Query("SELECT  new com.meetidentity.usermanagement.web.rest.model.TypeDescription(pcch.type, pcch.description) FROM PhysicalCCHairEntity pcch "
			+ "WHERE UPPER(pcch.organization.name) = UPPER(:orgName) and pcch.status = 0")
	List<TypeDescription> getPhysicalCCHairTypesByOrgName(@Param("orgName") String orgName);
	
}
