package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.UserOtpEntity;

@Repository
public interface UserOtpRepository extends JpaRepository<UserOtpEntity, Long> {

	@Query("SELECT uoe FROM UserOtpEntity uoe "
			+ "WHERE UPPER(uoe.mobileNumber) = UPPER(:mobileNumber) AND UPPER(uoe.otp) = UPPER(:otp)")
	UserOtpEntity getUserOtpByPhoneNumber(@Param("mobileNumber") String phoneNumber, @Param("otp") String otp);
}
