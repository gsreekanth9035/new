package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.IdentityTypeEntity;

@Repository
public interface IdentityTypeRepository extends JpaRepository<IdentityTypeEntity, Long> { 

	
	@Query("SELECT idt FROM IdentityTypeEntity idt WHERE UPPER(idt.name) = UPPER(:identityName)")
	IdentityTypeEntity findIdentityTypeByName(@Param("identityName") String identityName);
	

}
