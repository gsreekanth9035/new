package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.meetidentity.usermanagement.domain.FingerTypeEntity;

@Repository
public interface FingerTypeRepository extends JpaRepository<FingerTypeEntity, Long> {
	@Query("SELECT ft FROM FingerTypeEntity ft WHERE UPPER(ft.organization.name) = UPPER(:organizationName) and ft.fingerCount = :fingerCount")
	List<FingerTypeEntity> findFingersByOrgAndCount(@Param("organizationName") String organizationName,
			@Param("fingerCount") Integer fingerCount);
}
