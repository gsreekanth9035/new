package com.meetidentity.usermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.OrganizationEmailConfigEntity;

public interface OrganizationEmailConfigRepository  extends JpaRepository< OrganizationEmailConfigEntity, Long> {

	@Query("SELECT oec FROM OrganizationEmailConfigEntity oec "
			+ "WHERE UPPER(oec.organization.name) = UPPER(:orgName) and oec.status = 1")
	OrganizationEmailConfigEntity getOrganizationEmailConfigEntity(@Param("orgName") String orgName);
	
}
