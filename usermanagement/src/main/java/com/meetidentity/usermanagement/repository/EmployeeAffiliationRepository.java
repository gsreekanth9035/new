package com.meetidentity.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.meetidentity.usermanagement.domain.EmployeeAffiliationEntity;
import com.meetidentity.usermanagement.web.rest.model.TypeDescription;

public interface EmployeeAffiliationRepository  extends JpaRepository<EmployeeAffiliationEntity, Long> {

	@Query("SELECT new com.meetidentity.usermanagement.web.rest.model.TypeDescription(ea.type, ea.description) FROM EmployeeAffiliationEntity ea "
			+ "WHERE UPPER(ea.organization.name) = UPPER(:orgName) and ea.identityTpeId.id = :identity_type_id and ea.status = 0")
	List<TypeDescription> getEmployeeAffiliationsByOrgName(@Param("orgName") String orgName, @Param("identity_type_id")long identity_type_id);
	
}
