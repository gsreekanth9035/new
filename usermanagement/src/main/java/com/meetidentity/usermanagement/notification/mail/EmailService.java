package com.meetidentity.usermanagement.notification.mail;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.domain.OrganizationEmailConfigEntity;
import com.meetidentity.usermanagement.exception.EmailServiceException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.repository.OrganizationEmailConfigRepository;
import com.meetidentity.usermanagement.util.Util;

@Component
public class EmailService {

	private static final Logger log = LoggerFactory.getLogger(EmailService.class);

	@Autowired
	private JavaMailSender sender;

	@Autowired
	private Environment env;
	
	@Autowired
	private OrganizationEmailConfigRepository organizationEmailConfigRepository;

	public int sendTemporaryPassword(String organizationName, String username, String useremail, String userpassword) throws EmailServiceException {

		OrganizationEmailConfigEntity organizationEmailConfigEntity = organizationEmailConfigRepository.getOrganizationEmailConfigEntity(organizationName);
		
		Properties props = new Properties();
		props.put("mail.smtp.host", organizationEmailConfigEntity.getHost());
		props.put("mail.stmp.user", organizationEmailConfigEntity.getUsername());

		// To use TLS
		props.put("mail.smtp.auth", organizationEmailConfigEntity.isEnableAuthentication()?"true":"false");
		props.put("mail.smtp.starttls.enable", organizationEmailConfigEntity.isEnableStartTls()?"true":"false");
		props.put("mail.smtp.password", organizationEmailConfigEntity.getPassword());
		// To use SSL
		props.put("mail.smtp.socketFactory.port",  organizationEmailConfigEntity.getPort());
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", organizationEmailConfigEntity.isEnableAuthentication()?"true":"false");
		props.put("mail.smtp.port", organizationEmailConfigEntity.getPort());

		try {

			// Create a mail object
			Session session = Session.getInstance(props, new Authenticator() {

				// Set the account information session，transport will send mail
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(organizationEmailConfigEntity.getUsername(), organizationEmailConfigEntity.getPassword());
				}
			});
			session.setDebug(true);

			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(organizationEmailConfigEntity.getFrom()));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(useremail));
			msg.setSubject("Password token");
			String content = "Hello " + username + ",\n\nThe temporary password for your account is: " + userpassword
					+ "\n\nRequest you to login with this temporary password to access your account at https://"+organizationName+".meetidentity.net \n\nFor issues with login, please visit our Support HelpDesk \n\nThanks \nUIA Demo";
			msg.setContent(content, "text/plain;charset=utf-8");

			Transport.send(msg);

		} catch (MessagingException cause) {
			log.error(ApplicationConstants.notifyAdmin,ErrorMessages.FAILED_TO_SEND_TEMPORARY_PWD, username, useremail);
			throw new EmailServiceException(cause, ErrorMessages.FAILED_TO_SEND_TEMPORARY_PWD, username, useremail );
		}
		log.info(ApplicationConstants.notifySend,Util.format(ErrorMessages.SUCCESSFULLY_SENT_TEMPORARY_PWD, username, useremail));
		
		return ApplicationConstants.SC_EMAIL_SENT;
	}

	public String sendMailAttachment() throws MessagingException {
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		try {
			helper.setTo("c.kumar@meetidentity.com");
			helper.setText("Greetings :)\n Please find the attached docuemnt for your reference.");
			helper.setSubject("Mail From Spring Boot");
			ClassPathResource file = new ClassPathResource("front.PNG");
			helper.addAttachment("front.PNG", file);
		} catch (MessagingException e) {
			e.printStackTrace();
			return "Error while sending mail ..";
		}
		sender.send(message);
		return "Mail Sent Success!";
	}

	void sendEmailWithAttachment() throws MessagingException, IOException {

		MimeMessage msg = sender.createMimeMessage();

		// true = multipart message
		MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		helper.setTo("c.kumar@meetidentity.com");

		helper.setSubject("Testing from Spring Boot");

		// default = text/plain
		// helper.setText("Check attachment for image!");

		// true = text/html
		helper.setText("<h1>Check attachment for image!</h1>", true);

		// FileSystemResource file = new FileSystemResource(new
		// File("classpath:android.png"));

		// Resource resource = new ClassPathResource("android.png");
		// InputStream input = resource.getInputStream();

		// ResourceUtils.getFile("classpath:android.png");

		helper.addAttachment("my_photo.png", new ClassPathResource("android.png"));

		sender.send(msg);

	}

}
