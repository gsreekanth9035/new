package com.meetidentity.usermanagement.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String OPERATOR = "ROLE_OPERATOR";

    public static final String ADJUDICATOR = "ROLE_ADJUDICATOR";
    
    public static final String USER = "ROLE_USER";
    
    public static final String MOBILEID_USER = "ROLE_MOBILEID_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {
    }
}
