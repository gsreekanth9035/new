package com.meetidentity.usermanagement.security;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;


@Service
@Component
public class CryptoFunction {
	
	public SecretKey generateAES256Key() throws NoSuchAlgorithmException {
		
		KeyGenerator keygen = KeyGenerator.getInstance("AES");
		keygen.init(256);
		
		return keygen.generateKey();
	}
	
	public SecretKey generateAES256Key(byte[] decodedKey) throws NoSuchAlgorithmException {
		
		SecretKey secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");		
		return secretKey;
	}
	 

	public String encrypt(byte[] ivParamKey,byte[] aeskey,byte[] inputData) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		SecretKeySpec secretKey1 = new SecretKeySpec(Base64.getDecoder().decode(aeskey), "AES");
		IvParameterSpec iv = new IvParameterSpec(Base64.getDecoder().decode(ivParamKey));
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	    cipher.init(Cipher.ENCRYPT_MODE, secretKey1,iv);
		byte[] encrypted = cipher.doFinal(inputData);
		System.out.println("Encrypted Key "+(Base64.getEncoder().encodeToString(encrypted)) );
		return (Base64.getEncoder().encodeToString(encrypted));
	}
	
	
	public String decrypt(byte[] ivParamKey,byte[] aeskey,byte[] encryptData) throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		SecretKeySpec secretKey1 = new SecretKeySpec(Base64.getDecoder().decode(aeskey), "AES");
		IvParameterSpec iv = new IvParameterSpec(Base64.getDecoder().decode(ivParamKey));
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, secretKey1,iv);
		return new String(cipher.doFinal(encryptData));
	}
	

	public byte[] encryptWithRSA(String publicKeyAlgorithm, PublicKey key, String plaintext) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
	{
	    Cipher rsaCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");   
	    rsaCipher.init(Cipher.ENCRYPT_MODE, key);  
	    return (rsaCipher.doFinal(plaintext.getBytes()));
	}
	
	public byte[] encryptWithRSA(String publicKeyAlgorithm, PublicKey key, byte[] plaintext) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
	{
	    Cipher rsaCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");   
	    rsaCipher.init(Cipher.ENCRYPT_MODE, key);  
	    return (rsaCipher.doFinal(plaintext));
	}
	
	public byte[] encryptWithAES(Key key, String plaintext) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
	{
	    Cipher aesCipher = Cipher.getInstance("AES");   
	    aesCipher.init(Cipher.ENCRYPT_MODE, key);  
	    return (aesCipher.doFinal(plaintext.getBytes()));
	}

	public String encryptWithAES(Key key, String initVector, String plaintext, String enccipherAlgorithm) {
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));

			Cipher cipher = Cipher.getInstance(enccipherAlgorithm);
			cipher.init(Cipher.ENCRYPT_MODE, key, iv);

			byte[] encrypted = cipher.doFinal(plaintext.getBytes());
			return initVector + Base64Utils.encodeToString(encrypted);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public String decryptWithAES256CBCPKCS5Padding(Key key, String encrypted, String decipherAlgorithm) {
		try {
			IvParameterSpec iv = new IvParameterSpec(encrypted.substring(0, 16).getBytes("UTF-8"));

			Cipher cipher = Cipher.getInstance(decipherAlgorithm);
			cipher.init(Cipher.DECRYPT_MODE, key, iv);
			byte[] original = cipher.doFinal(Base64Utils.decodeFromString(encrypted.substring(16)));

			return new String(original);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}
}
