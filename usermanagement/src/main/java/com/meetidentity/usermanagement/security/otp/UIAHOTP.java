package com.meetidentity.usermanagement.security.otp;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Formatter;
import java.util.TimeZone;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
/**
 * Based on IETF RFC 4226 (http://tools.ietf.org/html/rfc4226)
 * 
 */
public class UIAHOTP {

	private static final String HMAC_SHA1_ALGORITHM = "HmacSha1";
	private static final String HMAC_SHA256_ALGORITHM = "HmacSha256";
	private static final String HMAC_SHA384_ALGORITHM = "HmacSha384";
	private static final String HMAC_SHA512_ALGORITHM = "HmacSha512";
	private static final int[] doubleDigits = { 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 };

	// 0   1   2     3    4     5      6       7        8         9
	private static final int[] DIGITS_POWER = { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000 };
	private static final int MILISECOND_BUFFER = 50; 
	   
	public static String getHmacSha1_toHexString(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA1_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
		mac.init(signingKey);
		return toHexString(mac.doFinal(counter.getBytes()));
	}

	public static byte[] getHmacSha1(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA1_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
		mac.init(signingKey);
		return mac.doFinal(counter.getBytes());
	}
	
	
	/**
	    * This method uses the JCE to provide the HMAC-SHA-1 algorithm.
	    * HMAC computes a Hashed Message Authentication Code and
	    * in this case SHA1 is the hash algorithm used.
	    *
	    * @param secret		the bytes to use for the HMAC-SHA-1 key
	    * @param text       the message or text to be authenticated.
	    *
	    * @throws NoSuchAlgorithmException if no provider makes
	    *       either HmacSHA1 or HMAC-SHA-1
	    *       digest algorithms available.
	    * @throws InvalidKeyException
	    *       The secret provided was not a valid HMAC-SHA-1 key.
	    *
	    */ 
	private static byte[] getHmacSha1(byte[] secret, byte[] counter) 
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secret, HMAC_SHA1_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
		mac.init(signingKey);
		return mac.doFinal(counter);
	}

	public static String getHmacSha256_toHexString(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA256_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(signingKey);
		return toHexString(mac.doFinal(counter.getBytes()));
	}

	public static byte[] getHmacSha256(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA256_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(signingKey);
		return mac.doFinal(counter.getBytes());
	}
	
	private static byte[] getHmacSha256(byte[] secret, byte[] text)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secret, HMAC_SHA256_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(signingKey);
		return mac.doFinal(text);
	}

	public static String getHmacSha384_toHexString(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA384_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA384_ALGORITHM);
		mac.init(signingKey);
		return toHexString(mac.doFinal(counter.getBytes()));
	}

	public static byte[] getHmacSha384(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA384_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA384_ALGORITHM);
		mac.init(signingKey);
		return mac.doFinal(counter.getBytes());
	}

	public static String getHmacSha512_toHexString(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA512_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA512_ALGORITHM);
		mac.init(signingKey);
		return toHexString(mac.doFinal(counter.getBytes()));
	}

	public static byte[] getHmacSha512(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA512_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA512_ALGORITHM);
		mac.init(signingKey);
		return mac.doFinal(counter.getBytes());
	}

	private static String toHexString(byte[] bytes) {
		Formatter formatter = new Formatter();

		for (byte b : bytes) {
			formatter.format("%02x", b);
		}

		return formatter.toString();
	}

	public static String getRandomKey(String hmacHashAlgorithm) throws NoSuchAlgorithmException {
		SecureRandom secureRandom = new SecureRandom(SecureRandom.getSeed(32));
		KeyGenerator keyGen = KeyGenerator.getInstance(hmacHashAlgorithm);
		keyGen.init(secureRandom);
		SecretKey secretKey = keyGen.generateKey();
		String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
		return encodedKey;
	}

//	public static void main(String[] args) throws Exception {
//		String HmacSha1_known_secret = getHmacSha1_toHexString("seretkey", "counter");
//		String HmacSha1 = getHmacSha1_toHexString(getRandomKey(HMAC_SHA1_ALGORITHM), "counter");
//		String HmacSha256 = getHmacSha256_toHexString(getRandomKey(HMAC_SHA256_ALGORITHM), "counter");
//		String HmacSha384 = getHmacSha384_toHexString(getRandomKey(HMAC_SHA384_ALGORITHM), "counter");
//		String HmacSha512 = getHmacSha512_toHexString(getRandomKey(HMAC_SHA512_ALGORITHM), "counter");
//
//		System.out.println(HmacSha1_known_secret + " --> " + HmacSha1_known_secret.length() / 2 + "bytes --> "
//				+ HmacSha1_known_secret.equals("51d101c65595a73460e4b7959a921c12080911de"));
//		System.out.println(HmacSha1 + " --> " + HmacSha1.length() / 2 + "bytes ");
//		System.out.println(HmacSha256 + " --> " + HmacSha256.length() / 2 + "bytes ");
//		System.out.println(HmacSha384 + " --> " + HmacSha384.length() / 2 + "bytes ");
//		System.out.println(HmacSha512 + " --> " + HmacSha512.length() / 2 + "bytes ");
//
//	}
	
	/**
	    * Calculates the checksum using the credit card algorithm.
	    * This algorithm has the advantage that it detects any single
	    * mistyped digit and any single transposition of
	    * adjacent digits.
	    *
	    * @param num the number to calculate the checksum for
	    * @param digits number of significant places in the number
	    *
	    * @return the checksum of num
	    */
	private static int calcChecksum(long num, int digits) 
	   {
	      boolean doubleDigit = true;

	      int     total = 0;

	      while (0 < digits--) 
	      {
	         int digit = (int) (num % 10);
	         num /= 10;
	         if (doubleDigit) 
	         {
	            digit = doubleDigits[digit];
	         }
	         total += digit;
	         doubleDigit = !doubleDigit;
	      }

	      int result = total % 10;

	      if (result > 0) 
	      {
	         result = 10 - result;
	      }
	      return result;
	   }

	/**
	    * This method generates an OTP value for the given set of parameters.
	    *
	    * @param secret       the shared secret
	    * @param movingFactor the counter, time, or other value that
	    *                     changes on a per use basis.
	    * @param codeDigits   the number of digits in the OTP, not
	    *                     including the checksum, if any.
	    * @param addChecksum  a flag that indicates if a checksum digit

	    *                     should be appended to the OTP.
	    * @param truncationOffset the offset into the MAC result to
	    *                     begin truncation.  If this value is out of
	    *                     the range of 0 ... 15, then dynamic
	    *                     truncation  will be used.
	    *                     Dynamic truncation is when the last 4
	    *                     bits of the last byte of the MAC are
	    *                     used to determine the start offset.
	    * @throws NoSuchAlgorithmException if no provider makes
	    *                     either HmacSHA256 or HMAC-SHA-256
	    *                     digest algorithms available.
	    * @throws InvalidKeyException
	    *                     The secret provided was not
	    *                     a valid HMAC-SHA-256 key.
	    */
	public static String generateOTP(byte[] secret, long movingFactor, int codeDigits, boolean addChecksum,
			int truncationOffset) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
		// put movingFactor value into text byte array
	      String result = null;
	      int digits = addChecksum ? (codeDigits + 1) : codeDigits;
	      byte[] text = new byte[8];

	      for (int i = text.length - 1; i >= 0; i--) 
	      {
	         text[i] = (byte) (movingFactor & 0xff);
	         movingFactor >>= 8;
	      }

	      // compute hmac hash
	      byte[] hash = getHmacSha256(secret, text);

	      // put selected bytes into result int
	      int offset = hash[hash.length - 1] & 0xf;
	      if ( (0<=truncationOffset) &&
	            (truncationOffset<(hash.length-4)) ) 
	      {
	         offset = truncationOffset;
	      }
	      
	      int binary =
	         ((hash[offset] & 0x7f) << 24)
	         | ((hash[offset + 1] & 0xff) << 16)
	         | ((hash[offset + 2] & 0xff) << 8)
	         | (hash[offset + 3] & 0xff);

	      int otp = binary % DIGITS_POWER[codeDigits];
	      if (addChecksum) 
	      {
	         otp =  (otp * 10) + calcChecksum(otp, codeDigits);
	      }
	      result = Integer.toString(otp);
	      
	      while (result.length() < digits) 
	      {
	         result = "0" + result;
	      }
	      return result;
	}



	/**
	    * Validate a submitted OTP string
	    * @param submittedOTP OTP string to validate
	    * @param secret Shared secret
	    * @param timeValueInMins How many mins back we need to check?
	    * @return
	    * @throws GeneralSecurityException 
	    */
	   public static boolean validate( String submittedOTP, byte[] secret, int timeValueInMins , int codeDigits, boolean addChecksum,
				int truncationOffset ) throws GeneralSecurityException
	   {
	      
	      TimeZone utc = TimeZone.getTimeZone( "UTC" );
	      Calendar currentDateTime = Calendar.getInstance( utc );
	      
	      long timeInMilis = currentDateTime.getTimeInMillis();
	      long movingFactor = timeInMilis;
	             
	      String otp = generateOTP( secret, movingFactor, codeDigits, addChecksum, truncationOffset );
	      
	      if( otp.equals( submittedOTP ) ) {
	    	  System.out.println("they are same");
	         return true;
	      }
		/* int endLimit = (int)(0.00001f * 60* 1000 + 1); */
	      int endLimit =  timeValueInMins* 60* 1000 + MILISECOND_BUFFER;
	      System.out.println(endLimit);
	      for( int i = 1; i < endLimit ; i++ )
	      {
	         movingFactor --; 
	         otp = generateOTP( secret, movingFactor, codeDigits, addChecksum, truncationOffset );

	    	  System.out.println("checking with "+ movingFactor);
	         if( otp.equals( submittedOTP ) ) {
		    	  System.out.println("they are same now");
			         return true;
			 }
	      }
	      System.out.println("they are not same now");
	      return false; 
	   } 
}
