package com.meetidentity.usermanagement.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import com.meetidentity.usermanagement.config.Constants;

/**
 * Implementation of AuditorAware based on Spring Security.
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

	@Autowired
	private HttpServletRequest httpServletRequest;
	
    @Override
    public String getCurrentAuditor() {
//        return SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT);
    	return httpServletRequest.getHeader("loggedInUser")!=null ? httpServletRequest.getHeader("loggedInUser") : Constants.SYSTEM_ACCOUNT;
    }
    

    public String getCurrentAuditorGroupID() {
//        return SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT);
    	return httpServletRequest.getHeader("loggedInUserGroupID")!=null ? httpServletRequest.getHeader("loggedInUserGroupID") : Constants.SYSTEM_ACCOUNT;
    }

    public String getCurrentAuditorGroupName() {
//        return SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT);
    	return httpServletRequest.getHeader("loggedInUserGroupName")!=null ? httpServletRequest.getHeader("loggedInUserGroupName") : Constants.SYSTEM_ACCOUNT;
    }

    public String getCurrentAuditorAuthorization() {
//        return SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT);
    	return httpServletRequest.getHeader("Authorization")!=null ? httpServletRequest.getHeader("Authorization") : Constants.SYSTEM_ACCOUNT;
    }
    
    public String getCurrentContentType() {
//      return SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT);
  	return httpServletRequest.getHeader("Content-type")!=null ? httpServletRequest.getHeader("Content-type") : Constants.SYSTEM_ACCOUNT;
  }
}
