package com.meetidentity.usermanagement.security.otp;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Formatter;
import java.util.Locale;
import java.util.TimeZone;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.amazonaws.util.Base32;
/**
 * TOTP: Time-based One-time Password Algorithm
 * Based on http://tools.ietf.org/html/draft-mraihi-totp-timebased-06
 * 
 */
public class UIATOTP {

	private static final String HMAC_SHA1_ALGORITHM = "HmacSha1";
	private static final String HMAC_SHA256_ALGORITHM = "HmacSha256";
	private static final String HMAC_SHA384_ALGORITHM = "HmacSha384";
	private static final String HMAC_SHA512_ALGORITHM = "HmacSha512";
	private static final int[] doubleDigits = { 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 };

	// 0   1   2     3    4     5      6       7        8         9
	private static final int[] DIGITS_POWER = { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000 };
	private static final int MILISECOND_BUFFER = 50; 
	private static int TIME_SLICE_X = 30000;
	private static int TIME_ZERO = 0;
	private static long TIME_INTERVAL = 30 * 1000; //30 secs
	   
	public static String getHmacSha1_toHexString(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA1_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
		mac.init(signingKey);
		return toHexString(mac.doFinal(counter.getBytes()));
	}

	public static byte[] getHmacSha1(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA1_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
		mac.init(signingKey);
		return mac.doFinal(counter.getBytes());
	}
	
	
	/**
	    * This method uses the JCE to provide the HMAC-SHA-1 algorithm.
	    * HMAC computes a Hashed Message Authentication Code and
	    * in this case SHA1 is the hash algorithm used.
	    *
	    * @param secret		the bytes to use for the HMAC-SHA-1 key
	    * @param text       the message or text to be authenticated.
	    *
	    * @throws NoSuchAlgorithmException if no provider makes
	    *       either HmacSHA1 or HMAC-SHA-1
	    *       digest algorithms available.
	    * @throws InvalidKeyException
	    *       The secret provided was not a valid HMAC-SHA-1 key.
	    *
	    */ 
	private static byte[] getHmacSha1(byte[] secret, byte[] counter) 
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secret, HMAC_SHA1_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
		mac.init(signingKey);
		return mac.doFinal(counter);
	}

	public static String getHmacSha256_toHexString(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA256_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(signingKey);
		return toHexString(mac.doFinal(counter.getBytes()));
	}

	public static byte[] getHmacSha256(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA256_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(signingKey);
		return mac.doFinal(counter.getBytes());
	}
	
	private static byte[] getHmacSha256(byte[] secret, byte[] text)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secret, HMAC_SHA256_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(signingKey);
		return mac.doFinal(text);
	}

	public static String getHmacSha384_toHexString(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA384_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA384_ALGORITHM);
		mac.init(signingKey);
		return toHexString(mac.doFinal(counter.getBytes()));
	}

	public static byte[] getHmacSha384(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA384_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA384_ALGORITHM);
		mac.init(signingKey);
		return mac.doFinal(counter.getBytes());
	}

	public static String getHmacSha512_toHexString(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA512_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA512_ALGORITHM);
		mac.init(signingKey);
		return toHexString(mac.doFinal(counter.getBytes()));
	}

	public static byte[] getHmacSha512(String secretKey, String counter)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA512_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA512_ALGORITHM);
		mac.init(signingKey);
		return mac.doFinal(counter.getBytes());
	}
	
	public static byte[] getHmacShaXXX(byte[] secretKey, byte[] counter, String algorithm)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec signingKey = new SecretKeySpec(secretKey, algorithm);
		Mac mac = Mac.getInstance(algorithm);
		mac.init(signingKey);
		return mac.doFinal(counter);
	}

	private static String toHexString(byte[] bytes) {
		Formatter formatter = new Formatter();

		for (byte b : bytes) {
			formatter.format("%02x", b);
		}

		return formatter.toString();
	}

	public static String getRandomKey(String hmacHashAlgorithm) throws NoSuchAlgorithmException {
		SecureRandom secureRandom = new SecureRandom(SecureRandom.getSeed(32));
		KeyGenerator keyGen = KeyGenerator.getInstance(hmacHashAlgorithm);
		keyGen.init(secureRandom);
		SecretKey secretKey = keyGen.generateKey();
		System.out.println( (secretKey.toString()));
		System.out.println((Base32.encodeAsString(secretKey.getEncoded())));
		String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
		return encodedKey;
	}
	
	public static String getKeyInBase64(String hmacHashAlgorithm, String seed) throws NoSuchAlgorithmException {
		SecureRandom secureRandom = new SecureRandom(seed.getBytes());
		KeyGenerator keyGen = KeyGenerator.getInstance(hmacHashAlgorithm);
		keyGen.init(secureRandom);
		SecretKey secretKey = keyGen.generateKey();
		String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
		return encodedKey;
	}
	
	public static Key getKey(String hmacHashAlgorithm, String seed) throws NoSuchAlgorithmException {
		SecureRandom secureRandom = new SecureRandom(seed.getBytes());
		KeyGenerator keyGen = KeyGenerator.getInstance(hmacHashAlgorithm);
		keyGen.init(secureRandom);
		return keyGen.generateKey();
	}

//	public static void main(String[] args) throws Exception {
//		String HmacSha1_known_secret = getHmacSha1_toHexString("seretkey", "counter");
//		String HmacSha1 = getHmacSha1_toHexString(getRandomKey(HMAC_SHA1_ALGORITHM), "counter");
//		String HmacSha256 = getHmacSha256_toHexString(getRandomKey(HMAC_SHA256_ALGORITHM), "counter");
//		String HmacSha384 = getHmacSha384_toHexString(getRandomKey(HMAC_SHA384_ALGORITHM), "counter");
//		String HmacSha512 = getHmacSha512_toHexString(getRandomKey(HMAC_SHA512_ALGORITHM), "counter");
//
//		System.out.println(HmacSha1_known_secret + " --> " + HmacSha1_known_secret.length() / 2 + "bytes --> "
//				+ HmacSha1_known_secret.equals("51d101c65595a73460e4b7959a921c12080911de"));
//		System.out.println(HmacSha1 + " --> " + HmacSha1.length() / 2 + "bytes ");
//		System.out.println(HmacSha256 + " --> " + HmacSha256.length() / 2 + "bytes ");
//		System.out.println(HmacSha384 + " --> " + HmacSha384.length() / 2 + "bytes ");
//		System.out.println(HmacSha512 + " --> " + HmacSha512.length() / 2 + "bytes ");
//
//	}
	
	/**
	    * Calculates the checksum using the credit card algorithm.
	    * This algorithm has the advantage that it detects any single
	    * mistyped digit and any single transposition of
	    * adjacent digits.
	    *
	    * @param num the number to calculate the checksum for
	    * @param digits number of significant places in the number
	    *
	    * @return the checksum of num
	    */
	private static int calcChecksum(long num, int digits) 
	   {
	      boolean doubleDigit = true;

	      int     total = 0;

	      while (0 < digits--) 
	      {
	         int digit = (int) (num % 10);
	         num /= 10;
	         if (doubleDigit) 
	         {
	            digit = doubleDigits[digit];
	         }
	         total += digit;
	         doubleDigit = !doubleDigit;
	      }

	      int result = total % 10;

	      if (result > 0) 
	      {
	         result = 10 - result;
	      }
	      return result;
	   }

	/**
	    * Generate a TOTP value using HMAC_SHA1
	    * @param key
	    * @param returnDigits
	    * @return
	    * @throws GeneralSecurityException
	    */
	public static String generateTOTP( String key, int returnDigits ) throws GeneralSecurityException
	   {
	      TimeZone utc = TimeZone.getTimeZone( "UTC" );
	      Calendar currentDateTime = Calendar.getInstance( utc );
	      long timeInMilis = currentDateTime.getTimeInMillis();
	       
	      String steps = "0";
	      long T = ( timeInMilis - TIME_ZERO ) /  TIME_SLICE_X ; 
	      steps = Long.toHexString( T ).toUpperCase(Locale.ENGLISH);
	      
	      // Just get a 16 digit string
	      while(steps.length() < 16) 
	         steps = "0" + steps;
	      return UIATOTP.generateTOTP( key, steps, returnDigits); 
	   }
	   
	   /**
	    * Generate a TOTP value using HMAC_SHA256
	    * @param key
	    * @param returnDigits
	    * @return
	    * @throws GeneralSecurityException
	    */
	   public static String generateTOTP256( String key, int returnDigits ) throws GeneralSecurityException
	   {
	      TimeZone utc = TimeZone.getTimeZone( "UTC" );
	      Calendar currentDateTime = Calendar.getInstance( utc );
	      long timeInMilis = currentDateTime.getTimeInMillis();
	       
	      String steps = "0";
	      long T = ( timeInMilis - TIME_ZERO ) /  TIME_SLICE_X ; 
	      steps = Long.toHexString( T ).toUpperCase(Locale.ENGLISH);
	      
	      // Just get a 16 digit string
	      while(steps.length() < 16) 
	         steps = "0" + steps;
	      return UIATOTP.generateTOTP256( key, steps, returnDigits); 
	   }
	   
	   /**
	    * Generate a TOTP value using HMAC_SHA512
	    * @param key
	    * @param returnDigits
	    * @return
	    * @throws GeneralSecurityException
	    */
	   public static String generateTOTP512( String key, int returnDigits ) throws GeneralSecurityException
	   {
	      TimeZone utc = TimeZone.getTimeZone( "UTC" );
	      Calendar currentDateTime = Calendar.getInstance( utc );
	      long timeInMilis = currentDateTime.getTimeInMillis();
	       
	      String steps = "0";
	      long T = ( timeInMilis - TIME_ZERO ) /  TIME_SLICE_X ; 
	      steps = Long.toHexString( T ).toUpperCase(Locale.ENGLISH);
	      
	      // Just get a 16 digit string
	      while(steps.length() < 16) 
	         steps = "0" + steps;
	      return UIATOTP.generateTOTP512( key, steps, returnDigits); 
	   }
	   
	   /**
	    * This method generates an TOTP value for the given
	    * set of parameters.
	    *
	    * @param key   the shared secret, HEX encoded
	    * @param time     a value that reflects a time
	    * @param returnDigits     number of digits to return
	    *
	    * @return      A numeric String in base 10 that includes
	    *              {@link truncationDigits} digits
	    * @throws GeneralSecurityException 
	    */
	   public static String generateTOTP( String key,   String time, int returnDigits ) throws GeneralSecurityException
	   {
	      return generateTOTP( key, time, returnDigits, HMAC_SHA1_ALGORITHM );
	   }


	   /**
	    * This method generates an TOTP value for the given
	    * set of parameters.
	    *
	    * @param key   the shared secret, HEX encoded
	    * @param time     a value that reflects a time
	    * @param returnDigits     number of digits to return
	    *
	    * @return      A numeric String in base 10 that includes
	    *              {@link truncationDigits} digits
	    * @throws GeneralSecurityException 
	    */
	   public static String generateTOTP256(String key, String time, int returnDigits) throws GeneralSecurityException
	   {
	      return generateTOTP( key, time, returnDigits, HMAC_SHA256_ALGORITHM );
	   }


	   /**
	    * This method generates an TOTP value for the given
	    * set of parameters.
	    *
	    * @param key   the shared secret, HEX encoded
	    * @param time     a value that reflects a time
	    * @param returnDigits     number of digits to return
	    *
	    * @return      A numeric String in base 10 that includes
	    *              {@link truncationDigits} digits
	    * @throws GeneralSecurityException 
	    */
	   public static String generateTOTP512(String key, String time, int returnDigits) throws GeneralSecurityException
	   {
	      return generateTOTP( key, time, returnDigits, HMAC_SHA512_ALGORITHM );
	   }
	  
	   /**
	    * This method generates an TOTP value for the given
	    * set of parameters.
	    *
	    * @param key   the shared secret, HEX encoded
	    * @param time     a value that reflects a time
	    * @param returnDigits     number of digits to return
	    * @param crypto    the crypto function to use
	    *
	    * @return      A numeric String in base 10 that includes
	    *              {@link truncationDigits} digits
	    * @throws GeneralSecurityException 
	    */
	   public static String generateTOTP(String key, String time,  int returnDigits, String crypto) throws GeneralSecurityException
	   { 
	      String result = null;
	      byte[] hash;

	      // Using the counter
	      // First 8 bytes are for the movingFactor
	      // Complaint with base RFC 4226 (HOTP)
	      while(time.length() < 16 )
	         time = "0" + time;

	      // Get the HEX in a Byte[]
	      byte[] msg = hexStr2Bytes(time);

	      // Adding one byte to get the right conversion
	      byte[] k = key.getBytes();

	      hash = getHmacShaXXX(k, msg, crypto);

	      // put selected bytes into result int
	      int offset = hash[hash.length - 1] & 0xf;

	      int binary =
	         ((hash[offset] & 0x7f) << 24) |
	         ((hash[offset + 1] & 0xff) << 16) |
	         ((hash[offset + 2] & 0xff) << 8) |
	         (hash[offset + 3] & 0xff);

	      int otp = binary % DIGITS_POWER[ returnDigits ];

	      result = Integer.toString(otp);
	      while (result.length() < returnDigits ) {
	         result = "0" + result;
	      }
	      return result;
	   }

	   /**
	    * This method converts HEX string to Byte[]
	    *
	    * @param hex   the HEX string
	    *
	    * @return      A byte array
	    */
	   private static byte[] hexStr2Bytes(String hex)
	   {
	      // Adding one byte to get the right conversion
	      // values starting with "0" can be converted
	      byte[] bArray = new BigInteger("10" + hex,16).toByteArray();

	      // Copy all the REAL bytes, not the "first"
	      byte[] ret = new byte[bArray.length - 1];
	      for (int i = 0; i < ret.length ; i++)
	         ret[i] = bArray[i+1];
	      return ret;
	   }

	   /**
	    * Validate a submitted OTP string
	    * @param submittedOTP OTP string to validate
	    * @param secret Shared secret 
	    * @return 
	    * @throws GeneralSecurityException
	    */
	   public static boolean validate( String submittedOTP, byte[] secret, int numDigits ) throws GeneralSecurityException
	   {
	      TimeZone utc = TimeZone.getTimeZone( "UTC" );
	      Calendar currentDateTime = Calendar.getInstance( utc );
	      
	      String generatedTOTP = UIATOTP.generateTOTP( new String( secret ) , numDigits );
	      boolean result =  generatedTOTP.equals( submittedOTP );

	      long timeInMilis = currentDateTime.getTimeInMillis();
	      
	      if( !result )
	      {   
	         timeInMilis -= TIME_INTERVAL;
	         
	         generatedTOTP = UIATOTP.generateTOTP( new String( secret ) , "" + timeInMilis, numDigits );
	         result =  generatedTOTP.equals( submittedOTP );
	      }
	      
	      if( !result )
	      {
	         timeInMilis += TIME_INTERVAL;
	         generatedTOTP = UIATOTP.generateTOTP( new String( secret ) , "" + timeInMilis, numDigits );
	         result =  generatedTOTP.equals( submittedOTP );
	      }
	      
	      return result;
	   }
	   
	   /**
	    * Validate a submitted OTP string using HMAC_256
	    * @param submittedOTP OTP string to validate
	    * @param secret Shared secret 
	    * @return 
	    * @throws GeneralSecurityException
	    */
	   public static boolean validate256( String submittedOTP, byte[] secret, int numDigits ) throws GeneralSecurityException
	   {
	      TimeZone utc = TimeZone.getTimeZone( "UTC" );
	      Calendar currentDateTime = Calendar.getInstance( utc );
	      
	      String generatedTOTP = UIATOTP.generateTOTP256( new String( secret ) , numDigits );
	      boolean result =  generatedTOTP.equals( submittedOTP );
	      
	      if( !result )
	      {
	         //Step back time interval
	         long timeInMilis = currentDateTime.getTimeInMillis();
	         timeInMilis -= TIME_INTERVAL;
	         
	         generatedTOTP = UIATOTP.generateTOTP256( new String( secret ) , "" + timeInMilis, numDigits );
	         result =  generatedTOTP.equals( submittedOTP );
	      }
	      
	      if( !result )
	      {
	         //Step ahead time interval
	         long timeInMilis = currentDateTime.getTimeInMillis();
	         timeInMilis += TIME_INTERVAL;
	         
	         generatedTOTP = UIATOTP.generateTOTP256( new String( secret ) , "" + timeInMilis, numDigits );
	         result =  generatedTOTP.equals( submittedOTP );
	      }
	      
	      return result;
	   }
	   
	   /**
	    * Validate a submitted OTP string using HMAC_512
	    * @param submittedOTP OTP string to validate
	    * @param secret Shared secret 
	    * @return 
	    * @throws GeneralSecurityException
	    */
	   public static boolean validate512( String submittedOTP, byte[] secret, int numDigits ) throws GeneralSecurityException
	   {
	      TimeZone utc = TimeZone.getTimeZone( "UTC" );
	      Calendar currentDateTime = Calendar.getInstance( utc );
	      
	      String generatedTOTP = UIATOTP.generateTOTP512( new String( secret ) , numDigits );
	      boolean result =  generatedTOTP.equals( submittedOTP );
	      
	      if( !result )
	      {
	         //Step back time interval
	         long timeInMilis = currentDateTime.getTimeInMillis();
	         timeInMilis -= TIME_INTERVAL;
	         
	         generatedTOTP = UIATOTP.generateTOTP512( new String( secret ) , "" + timeInMilis, numDigits );
	         result =  generatedTOTP.equals( submittedOTP );
	      }
	      
	      if( !result )
	      {
	         //Step ahead time interval
	         long timeInMilis = currentDateTime.getTimeInMillis();
	         timeInMilis += TIME_INTERVAL;
	         
	         generatedTOTP = UIATOTP.generateTOTP512( new String( secret ) , "" + timeInMilis, numDigits );
	         result =  generatedTOTP.equals( submittedOTP );
	      }
	      
	      return result;
	   }
}
