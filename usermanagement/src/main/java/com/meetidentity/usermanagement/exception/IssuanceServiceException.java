package com.meetidentity.usermanagement.exception;

import com.meetidentity.usermanagement.util.Util;

public class IssuanceServiceException extends UIMSException {

	private static final long serialVersionUID = 1L;

	private final int errorCode;
	
	public IssuanceServiceException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}
	
	public IssuanceServiceException(int errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	public IssuanceServiceException(String errorMessage, int errorCode) {
		super(errorMessage, errorCode);
		this.errorCode = errorCode;
	}
	
	public IssuanceServiceException(String causeorMessage, Throwable cause) {
		super(causeorMessage, cause);
		this.errorCode = -1;
	}
	
	public IssuanceServiceException(Throwable cause, String errorMessage, Object... insertMsgs) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	

	public IssuanceServiceException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause, errorCode);
		this.errorCode = errorCode;
	}

	public IssuanceServiceException(Throwable cause, int errorCode) {
		super(cause, errorCode);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
