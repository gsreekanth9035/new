package com.meetidentity.usermanagement.exception;

import com.meetidentity.usermanagement.util.Util;

public class InconsistentDataException extends UIMSRuntimeException {

	private static final long serialVersionUID = 1L;

	private final int errorCode;
	
	public InconsistentDataException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}

	public InconsistentDataException(int errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	public InconsistentDataException(String errorMessage, int errorCode) {
		super(errorMessage, errorCode);
		this.errorCode = errorCode;
	}

	public InconsistentDataException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = -1;
	}
	
	public InconsistentDataException(Throwable cause, String errorMessage, Object... insertMsgs) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	
	
	public InconsistentDataException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause, errorCode);
		this.errorCode = errorCode;
	}

	public InconsistentDataException(Throwable cause, int errorCode) {
		super(cause, errorCode);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
