package com.meetidentity.usermanagement.exception;

import com.meetidentity.usermanagement.util.Util;

public class EntityAlreadyExistsException extends UIMSRuntimeException {

	private static final long serialVersionUID = 1L;

	private final int errorCode;
	
	public EntityAlreadyExistsException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}

	public EntityAlreadyExistsException(int errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	public EntityAlreadyExistsException(String errorMessage, int errorCode) {
		super(errorMessage, errorCode);
		this.errorCode = errorCode;
	}

	public EntityAlreadyExistsException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = -1;
	}
	
	public EntityAlreadyExistsException(Throwable cause, String errorMessage, Object... insertMsgs ) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	
	public EntityAlreadyExistsException(String errorMessage, String insertMsgs , Throwable cause) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	
	public EntityAlreadyExistsException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause, errorCode);
		this.errorCode = errorCode;
	}

	public EntityAlreadyExistsException(Throwable cause, int errorCode) {
		super(cause, errorCode);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
