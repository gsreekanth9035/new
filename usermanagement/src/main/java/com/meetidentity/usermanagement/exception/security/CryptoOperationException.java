package com.meetidentity.usermanagement.exception.security;

import com.meetidentity.usermanagement.exception.UIMSRuntimeException;
import com.meetidentity.usermanagement.util.Util;

public class CryptoOperationException extends UIMSRuntimeException {

	private static final long serialVersionUID = 1L;

	private final int errorCode;
	
	public CryptoOperationException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}

	public CryptoOperationException(int errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	public CryptoOperationException(String errorMessage, int errorCode) {
		super(errorMessage, errorCode);
		this.errorCode = errorCode;
	}

	public CryptoOperationException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = -1;
	}
	
	public CryptoOperationException(Throwable cause, String errorMessage, Object... insertMsgs) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	
	
	public CryptoOperationException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause, errorCode);
		this.errorCode = errorCode;
	}

	public CryptoOperationException(Throwable cause, int errorCode) {
		super(cause, errorCode);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
