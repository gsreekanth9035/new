package com.meetidentity.usermanagement.exception;

import com.meetidentity.usermanagement.util.Util;

public class PeopleSearchException extends SearchServiceException {

	private static final long serialVersionUID = 1L;

	private final int errorCode;
	
	public PeopleSearchException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}

	public PeopleSearchException(int errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	public PeopleSearchException(String errorMessage, int errorCode) {
		super(errorMessage, errorCode);
		this.errorCode = errorCode;
	}

	public PeopleSearchException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = -1;
	}
	
	public PeopleSearchException(Throwable cause, String errorMessage, Object... insertMsgs) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	
	public PeopleSearchException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause, errorCode);
		this.errorCode = errorCode;
	}

	public PeopleSearchException(Throwable cause, int errorCode) {
		super(cause, errorCode);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
