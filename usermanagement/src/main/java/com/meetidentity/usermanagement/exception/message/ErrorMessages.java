package com.meetidentity.usermanagement.exception.message;

public interface ErrorMessages {

	// Generic
	public static String ENTITY_NOT_FOUND = "Entity not found";
	

	// Notify
	public static String FAILED_TO_SEND_INVITE_FOR_ENROLLMENT = "Failed to Send Invite for Enrollment";

	// Email
	public static String EMAIL_FAILED_TO_SEND = "EMAIL failed to Send";
	public static String EMAIL_SENT = "EMAIL Sent";

	public static String FAILED_TO_SEND_TEMPORARY_PWD = "Failed to send Temporary Password over Email Service to User with username '<>' and emailID '<>'";
	public static String SUCCESSFULLY_SENT_TEMPORARY_PWD = "Temporary Password is sent Successfully over Email Service to User with username '<>' and emailID '<>'";

	// User Status

	// Workflow
	public static String WORKFLOW_CREATED_WITH_NAME_UNDER_ORGANIZATION = "Workflow is created with name '<>' under Organization '<>'";
	public static String FAILED_TO_CREATE_WORKFLOW_UNDER_ORGANIZATION = "Failed to create Workflow with name '<>' under Organization '<>'";
	public static String FAILED_TO_UPDATE_WORKFLOW_UNDER_ORGANIZATION = "Failed to update Workflow with name '<>' under Organization '<>'";
	public static String FAILED_TO_DELETE_WORKFLOW_UNDER_ORGANIZATION = "Failed to delete Workflow with name '<>' under Organization '<>'";
	public static String FAILED_TO_ASSIGN_STEPS_TO_WORKFLOW_UNDER_ORGANIZATION = "Failed to assign steps to Workflow with name '<>' under Organization '<>'";
	
	public static String WORKFLOW_FOUND_FOR_USER = "Workflow found for the User with username '<>'";
	public static String WORKFLOW_FOUND_FOR_USERID = "Workflow found for the User with user ID '<>'";
	public static String WORKFLOW_FOUND_FOR_USER_UNDER_ORGANIZATION = "Workflow found for the User with username '<>' under Organization '<>'";
	public static String WORKFLOW_NOT_FOUND_UNDER_ORGANIZATION = "No Workflow found under Organization '<>'";
	public static String WORKFLOW_NOT_FOUND_FOR_WORKFLOWNAME_UNDER_ORGANIZATION = "No Workflow found with workflow name '<>' under Organization '<>'";
	public static String WORKFLOW_FOUND_FOR_USERID_UNDER_ORGANIZATION = "Workflow found for the User with user ID '<>' under Organization '<>'";
	public static String WORKFLOW_NOT_FOUND_FOR_USER = "No Workflow found for the User with username '<>'";
	public static String WORKFLOW_NOT_FOUND_FOR_USERID = "No Workflow found for the User with user ID '<>'";
	public static String WORKFLOW_NOT_FOUND_FOR_GROUPID_UNDER_ORGANIZATION = "No Workflow found for the Groupd with groupd ID '<>' under Organization '<>'";
	public static String WORKFLOW_NOT_FOUND_FOR_USER_UNDER_ORGANIZATION = "No Workflow found for the User with username '<>' under Organization '<>'";
	public static String WORKFLOW_NOT_FOUND_FOR_USERID_UNDER_ORGANIZATION = "No Workflow found for the User with user ID '<>' under Organization '<>'";

	public static String FAILED_TO_GET_WORKFLOWS_UNDER_ORGANIZATION = "Failed to get Workflow(s) under Organization '<>'";
	public static String FAILED_TO_GET_WORKFLOW_WITH_WORKFLOWNAME_UNDER_ORGANIZATION = "Failed to get Workflow with workflow name '<>' under Organization '<>'";
	public static String FAILED_TO_GET_VALIDITY_OF_WORKFLOW_WITH_WORKFLOWNAME_UNDER_ORGANIZATION = "Failed to get Validity of Workflow with workflow name '<>' under Organization '<>'";
	public static String FAILED_TO_GET_VALIDITY_OF_WORKFLOW_WITH_GROUPID_UNDER_ORGANIZATION = "Failed to get Validity of Workflow with Group ID '<>' under Organization '<>'";
	public static String FAILED_TO_GET_IDENTITYTYPE_WITH_GROUPID_UNDER_ORGANIZATION = "Failed to get Identity Type of Workflow with Group ID '<>' under Organization '<>'";
	public static String FAILED_TO_GET_WORKFLOW_AND_IDENTITYTYPE_WITH_GROUPID_UNDER_ORGANIZATION = "Failed to get Workflow and Identity Type with Group ID '<>' under Organization '<>'";
	public static String FAILED_TO_GET_WORKFLOW_STEP_DEFINITIONS = "Failed to get Workflow Step definitions";
	public static String FAILED_TO_GET_WORKFLOW_BY_ROLE_UNDER_ORGANIZATION = "Failed to get Workflow by Role under Organization '<>'";
	public static String FAILED_TO_GET_DEFAULT_WORKFLOW_UNDER_ORGANIZATION = "Failed to get Default Workflow under Organization '<>'";
	public static String FAILED_TO_GET_WORKFLOW_STEPS_OF_WORKFLOW_UNDER_ORGANIZATION = "Failed to get Workflow Steps of Workflow '<>' under Organization '<>'";
	public static String FAILED_TO_GET_STEP_DETAILS_OF_WORKFLOW_STEP_UNDER_ORGANIZATION = "Failed to get Step details for stepname '<>' of Workflow '<>' under Organization '<>'";
	public static String FAILED_TO_GET_STEP_DETAILS_OF_WORKFLOW_STEP_FOR_GROUP_UNDER_ORGANIZATION = "Failed to get Step details for stepname '<>' of Workflow for Group'<>' under Organization '<>'";
	public static String FAILED_TO_GET_DEVICEPROFILES_FOR_WORKFLOW_UNDER_ORGANIZATION = "Failed to get Device Profile(s) of Workflow '<>' under Organization '<>'";
	public static String FAILED_TO_GET_GROUPS_WITH_WORKFLOW_UNDER_ORGANIZATION = "Failed to get Group with Workflow under Organization '<>'";
	
	public static String WORKFLOW_ATTRIBUTE_CREATED_WITH_NAME_UNDER_ORGANIZATION_AS_DEFAULT = "Workflow Attribute is created with name '<>' under Organization '<>' as DEFAULT_WORKFLOW";
	public static String WORKFLOW_ATTRIBUTE_DELETED_WITH_NAME_UNDER_ORGANIZATION_AS_DEFAULT = "Workflow Attribute is deleted with name '<>' under Organization '<>' as DEFAULT_WORKFLOW";
	
	//Device Profile
	public static String FAILED_TO_CREATE_DEVICEPROFILE = "Failed to create Device Profile with name '<>'";
	public static String FAILED_TO_GET_DEVICEPROFILES = "Failed to get Device Profiles";
	public static String FAILED_TO_GET_DEVICEPROFILE_FOR_ID = "Failed to get Device Profile for ID '<>'";
	public static String FAILED_TO_DELETE_DEVICEPROFILE_WITH_ID = "Failed to delete Device Profile with ID '<>'";
	public static String FAILED_TO_GET_DEVICEPROFILE_FOR_DEVICEPROFILEENTITY = "Failed to get Device Profile for DeviceProfileEntity with name '<>'";
	public static String DEVICEPROFILE_NOT_FOUND_WITH_ID = "No Device Profile is found with ID '<>'";
	public static String DEVICEPROFILE_ALREADY_EXISTS_WITH_SAME_NAME = "Device Profile aleady exists with same name '<>'";
	public static String FAILED_TO_UPDATE_DEVICEPROFILE = "Failed to update Device Profile with name '<>'";
	public static String FAILED_TO_ASSIGN_DEVICE_PROFILES_TO_WORKFLOW_UNDER_ORGANIZATION = "Failed to assign Device Profiles to Workflow '<>' under organization '<>'";
	
	//Organization
	public static String ORGANIZATION_NOT_FOUND_WITH_NAME = "No Organization found with name '<>'";
	public static String FAILED_TO_GET_ORGANIZATION_IDENTITY_FIELDS_OF_ORGANIZATION = "Failed to get Organization Identity Fields of Organization '<>'";
	
	// Role
	public static String ROLE_CREATED_WITH_NAME_FOR_WORKFLOW = "Role is created with name '<>' for Workflow '<>'";
	
	public static String ROLE_FOUND_WITH_ROLENAME = "Role found with Role Name '<>'";
	public static String ROLE_NOT_FOUND_WITH_ROLENAME = "No Role found with Role Name '<>'";
	public static String ROLE_NOT_FOUND_FOR_CLIENT = "No Role found for Client ID '<>'";
	public static String ROLE_NOT_FOUND_WITH_ROLENAME_UNDER_ORGANIZATION = "No Role found with Role Name '<>' under Organization '<>'";
	public static String ROLE_FOUND_WITH_ROLEID = "Role found with Role ID '<>'";
	public static String ROLE_NOT_FOUND_WITH_ROLEID = "No Role found with Role ID '<>'";
	public static String ROLE_FOUND_WITH_GORUPID = "Role found with group ID '<>'";
	public static String ROLE_NOT_FOUND_WITH_GORUPID = "No Role found with group ID '<>'";
	public static String ROLE_FOUND_FOR_USER = "Role found for the User with username '<>'";
	public static String ROLE_FOUND_FOR_USERID = "Role found for the User with user ID '<>'";
	public static String ROLE_FOUND_FOR_USER_UNDER_ORGANIZATION = "Role found for the User with username '<>' under Organization '<>'";
	public static String ROLE_FOUND_FOR_USERID_UNDER_ORGANIZATION = "Role found for the User with user ID '<>' under Organization '<>'";
	public static String ROLE_NOT_FOUND_FOR_USER = "No Role found for the User with username '<>'";
	public static String ROLE_NOT_FOUND_UNDER_ORGANIZATION = "No Role found under Organization '<>'";
	public static String ROLE_NOT_FOUND_FOR_USERID = "No Role found for the User with user ID '<>'";
	public static String ROLE_NOT_FOUND_FOR_USER_UNDER_ORGANIZATION = "No Role found for the User with username '<>' under Organization '<>'";
	public static String ROLE_NOT_FOUND_FOR_USERID_UNDER_ORGANIZATION = "No Role found for the User with user ID '<>' under Organization '<>'";

	// Group
	public static String GROUP_FAILED_TO_CREATE = "Group '<>' failed to create under Organization '<>'";
	public static String GROUP_FAILED_TO_UPDATE = "Group '<>' failed to update under Organization '<>'";
	public static String GROUP_FOUND_WITH_GORUPID = "Group found with group ID '<>'";
	public static String GROUP_NOT_FOUND_WITH_GROUPID = "No Group found with group ID '<>'";
	public static String GROUP_FOUND_FOR_USER = "Group found for the User with username '<>'";
	public static String GROUP_FOUND_FOR_USERID = "Group found for the User with user ID '<>'";
	public static String GROUP_FOUND_FOR_USER_UNDER_ORGANIZATION = "Group found for the User with username '<>' under Organization '<>'";
	public static String GROUP_FOUND_FOR_USERID_UNDER_ORGANIZATION = "Group found for the User with user ID '<>' under Organization '<>'";
	public static String GROUP_NOT_FOUND_FOR_USER = "No Group found for the User with username '<>'";	
	public static String GROUP_NOT_FOUND_FOR_USERID = "No Group found for the User with user ID '<>'";
	public static String GROUP_AND_ROLES_NOT_FOUND_FOR_USERID = "No Group and Roles are found for the User with user ID '<>'";
	public static String GROUP_NOT_FOUND_FOR_USER_UNDER_ORGANIZATION = "No Group found for the User with username '<>' under Organization '<>'";
	public static String GROUP_NOT_FOUND_FOR_USERID_UNDER_ORGANIZATION = "No Group found for the User with user ID '<>' under Organization '<>'";
	public static String GROUP_NOT_FOUND_UNDER_ORGANIZATION = "No Group(s) found under Organization '<>'";
	public static String GROUP_FOUND_WITH_GORUPID_UNDER_ORGANIZATION = "Group found with group ID '<>' under Organization '<>'";
	public static String GROUP_NOT_FOUND_WITH_GROUPID_UNDER_ORGANIZATION = "No Group found with group ID '<>' under Organization '<>'";
	public static String UNABLE_TO_ASSIGN_ROLE_TO_USERID = "Unable to Assign Role with role name '<>' to User with userID '<>' ";
	public static String UNABLE_TO_ASSIGN_GROUP_TO_USERID = "Unable to Assign Group with groudID '<>' to User with userID '<>' ";
	public static String FAILED_TO_ASSIGN_GROUPS_TO_WORKFLOW_UNDER_ORGANIZATION = "Failed to assign Groups to Workflow '<>' under organization '<>'";
	
	// Identity Broker
	public static String IDENTITYBROKER_SERVER_EXCEPTION = "Unable to Communicate with Identity Broker Server'";
	public static String APPLICATION_IDENTITYBROKER_INCONSISTANCY_FOR_USER = "Inconsistent User details: User with username '<>' exists in Application but not in Identity Broker ";
	public static String IDENTITYBROKER_USER_DETAILS_CONFLICT = "Conflict User details: User already exists";
	public static String IDENTITYBROKER_APPLICATION_INCONSISTANCY_FOR_USER = "Inconsistent User details: User with username '<>' exists in Identity Broker but not in Application ";
	public static String IDENTITYBROKER_SUCCESSFULLY_CREATED_USER_ACCOUNT = "Successfully Created User Account in Identity Broker for User with username '<>' ";
	public static String IDENTITYBROKER_SUCCESSFULLY_ASSIGNED_GROUP_TO_USER_ACCOUNT = "Successfully assigned Group with groupID '<>' to the User Account with username '<>' in Identity Broker ";
	public static String IDENTITYBROKER_SUCCESSFULLY_ASSIGNED_ROLE_TO_USER_ACCOUNT = "Successfully assigned Role with RoleID '<>' to the User Account with username '<>' in Identity Broker ";
	public static String IDENTITYBROKER_FAILED_TO_CREATEUSER_FOR_USER = "Failed to Create User Account in Identity Broker for User with username '<>' ";
	public static String IDENTITYBROKER_FAILED_TO_UPDATEUSER_FOR_USER = "Failed to Update User Account in Identity Broker for User with username '<>' ";
	public static String IDENTITYBROKER_FAILED_TO_DELETE_CLIENT_ROLE_USING_WORKFLOW_UNDER_ORGANIZATION = "Failed to Delete client role in Identity Broker for the workflow '<>'  under Organization '<>'";
	public static String IDENTITYBROKER_USER_ACCOUNT_DELETED = "User account with username '<>' is deleted from Identity Broker";
	public static String IDENTITYBROKER_USER_ACCOUNT_NOT_DELETED = "User account with username '<>' if failed to get deleted from Identity Broker";
	public static String IDENTITYBROKER_SUCCESSFULLY_CREATED_AND_ASSIGNED_ROLE_TO_GROUP_OF_WORKFLOW_UNDER_ORGANIZATION = "Successfully created and assigned Role '<>' to Group of Workflow '<>' under Organization '<>' in Identity Broker ";
	public static String IDENTITYBROKER_FAILED_TO_CREATE_AND_ASSIGN_ROLE_TO_GROUP_OF_WORKFLOW_UNDER_ORGANIZATION = "Failed to create and assigne Role '<>' to Group of Workflow '<>' under Organization '<>' in Identity Broker ";
	public static String IDENTITYBROKER_FAILED_TO_GET_ROLE_USING_WORKFLOW_UNDER_ORGANIZATION = "Failed to get Role in Identity Broker for the workflow '<>'  under Organization '<>'";
	public static String IDENTITYBROKER_FAILED_TO_DELETE_ROLE_USING_WORKFLOW_UNDER_ORGANIZATION = "Failed to delete Role in Identity Broker for the workflow '<>'  under Organization '<>'";
	public static String FEDERATED_USER_ENROLLMENT_FAILED = "Failed to enroll the federated user with username '<>'";
	
	// Retrun Values
	public static String SUCCESS_MSG = "Success";
	public static String FAILURE_MSG = "Failure";

	// Method entry exit
	public static String ENTER_METHOD = "Enter {} method";
	public static String EXIT_METHOD = "EXIT {} method";

	// User
	public static String USER_ENROLLED = "User with username '<>' is successfully Enrolled";
	public static String USER_NOT_ENROLLED = "User with username '<>' is failed to get Enrolled: Reason: '<>'";
	
	
	public static String USER_FOUND_WITH_USERNAME = "User found with username '<>'";
	public static String USER_FOUND_WITH_USERID = "User found with ID '<>'";
	public static String USER_FOUND_WITH_UUID = "User found with user's UUID '<>'";
	public static String USER_FOUND_UNDER_ORGANIZATION = "User(s) found under Organization '<>'";
	public static String USER_NOT_FOUND_WITH_USERNAME = "No User found with username '<>'";
	public static String USER_NOT_FOUND_WITH_ROLE = "No User found with Role '<>'";
	public static String USER_SESSION_NOT_FOUND_WITH_USERNAME = "No User session found with username '<>'";
	public static String USER_NOT_FOUND_WITH_USERID = "No User found with user ID '<>'";
	public static String USER_NOT_FOUND_WITH_UUID = "No User found with user's UUID '<>'";
	public static String USER_NOT_FOUND_UNDER_ORGANIZATION = "No User(s) found under Organization '<>'";
	public static String USER_FOUND_WITH_SEARCHCRITERIA = "'<>' user(s) found with searchcriteria '<>' under Organization '<>'";
	public static String USER_FOUND_FOR_GROUPID = "'<>' user(s) found for Group ID '<>' under Organization '<>'";
	public static String USER_NOT_FOUND_WITH_SEARCHCRITERIA = "No User found with searchcriteria '<>' under Organization '<>'";
	public static String USER_NOT_FOUND_FOR_GROUPID = "No User found for Group ID '<>' under Organization '<>'";
	public static String USER_WITH_USERNAME_UPDATED_STATUS = "User with username '<>' is updated with status '<>' ";
	public static String USER_WITH_USERNAME_NOT_UPDATED_STATUS = "User with username '<>' is failed to update with status '<>' ";
	public static String UPDATE_USER_STATUS = "User with user ID '<>' is updated with status '<>' ";
	public static String FAILED_TO_UPDATE_USER_STATUS = "User with user ID '<>' is failed to update with status '<>' ";
	public static String FAILED_TO_UPDATE_USER_STATUS_AFTER_DEVICE_REVOKE = "Failed to update with status to READY_FOR_REISSUANCE after Device Revoke ";
	public static String FAILED_TO_UPDATE_USER_STATUS_AFTER_DEVICE_UNPAIR = "Failed to update with status to READY_FOR_REISSUANCE after Device Unpair ";
	public static String FAILED_TO_UPDATE_USER_STATUS_AFTER_DEVICE_REVOKE_AS_NOT_ALL_USERS_ARE_UPDATED = "Failed to update with status to READY_FOR_REISSUANCE after Device Revoke as only <>/<> records are updated ";
	public static String USER_ADDED_WITH_USERNAME = "User Entity is created with username '<>'";
	public static String FAILED_TO_ADD_USER_WITH_USERNAME = "Failed to create User Entity with username '<>'";

	public static String FAILED_TO_DELETE_USER_WITH_USERNAME = "Failed to delete User with username '<>'";
	public static String FAILED_TO_SEARCH_USER = "Failed to Search for UserName/Email under organization '<>'";
	
	public static String USER_ENROLLMENTHISTORY_NOT_FOUND_WITH_USERNAME = "No User EnrollmentHistory found with username '<>'";
	
	public static String USER_ATTRIBUTES_NOT_FOUND_WITH_USERNAME = "No User Attributes found with username '<>'";
	public static String USER_APPEARANCE_NOT_FOUND_WITH_USERNAME = "No User Appearance found with username '<>'";
	public static String USER_BIOMETRICS_NOT_FOUND_WITH_USERNAME = "No User Biometrics found with username '<>'";
	public static String USER_DRIVINGLICENSE_NOT_FOUND_WITH_USERNAME = "No User Driving License found with username '<>'";
	public static String USER_EMPLOYEEID_NOT_FOUND_WITH_USERNAME = "No User Employee ID found with username '<>'";
	public static String USER_STUDENTID_NOT_FOUND_WITH_USERNAME = "No User Student ID found with username '<>'";
	public static String USER_PIVID_NOT_FOUND_WITH_USERNAME = "No User PIV/Workforce ID found with username '<>'";
	public static String USER_NATIONALID_NOT_FOUND_WITH_USERNAME = "No User National ID found with username '<>'";
	public static String USER_PERMANENT_RESIDENT_ID_NOT_FOUND_WITH_USERNAME = "No User Permanent Resident ID found with username '<>'";
	public static String USER_VOTERID_NOT_FOUND_WITH_USERNAME = "No User Voter ID found with username '<>'";
	public static String USER_HEALTHID_NOT_FOUND_WITH_USERNAME = "No User Health ID found with username '<>'";
	public static String USER_HEALTHSERVICEID_NOT_FOUND_WITH_USERNAME = "No User Health Service found with username '<>'";

	public static String UNABLE_TO_VALIDATE_UUID = "Unable to validate UUID  '<>' of the user";
	public static String INVALID_UUID_EITHER_EXPIRED_OR_NOT_ACTIVE = "Invalid UUID  '<>' of the user, either Expired or not Active";
	public static String UUID_VALIDATED = "UUID  '<>' of the user is validated successfully";
	public static String USER_IDENTIFICATION_CODE_NOT_FOUND_WITH_UUID = "No UserIdentificationCode found with user's UUID '<>'";
	public static String FAILED_TO_ADD_IDENTIFICATION_CODE_FOR_USER_WITH_USERNAME = "Failed to add UserIdentificationCode for User with username '<>'";
	public static String IDENTIFICATION_CODE_IS_ADDED_FOR_USER_WITH_USERNAME = "Successfully generated UserIdentificationCode for User with username '<>'";
	public static String QRCODE_IS_GENERATED_WITH_LOGO = "Successfully generated QRCode with Organization Logo";
	public static String QRCODE_IS_ADDED_FOR_USER_WITH_USERNAME = "Successfully generated QRCode for User with username '<>'";
	public static String FAILED_TO_ADD_QRCODE_FOR_USER_WITH_USERNAME = "Failed to add QRCode for User with username '<>'";
	public static String FAILED_TO_GENERATE_PAIR_MOBILE_SERVICE_QRCODE_FOR_ORGANIZATION = "Failed to generate Pair Mobile Service QRCode for organization '<>'";
	public static String FAILED_TO_GENERATE_QRCODE_WITH_LOGO = "Failed to generated QRCode with Organization Logo";
	public static String INVALID_GATEWAY_INFO = "Invalid Gateway info under Organization '<>'";
	
	//Use device
	public static String UNABLE_TO_REGISTER_USERDEVICE = "Unable to Register User Device  '<>' of the user with ID '<>'";
	public static String UNABLE_TO_FIND_USER_DEVICES = "Unable to find User Device of user with username '<>'";
	
	//IDP Credentials
	public static String FAILED_TO_ADD_IDP_CREDENTIALS_FOR_USERNAME = "Failed to add Credentials for User Name '<>' in IDP";
	public static String FAILED_TO_ADD_IDP_CREDENTIALS_FOR_USERID = "Failed to add Credentials for User ID '<>' in IDP";
	public static String FAILED_TO_DELETE_IDP_CREDENTIALS_FOR_USERID = "Failed to delete Credentials for User ID '<>' in IDP";
	public static String FAILED_TO_UPDATE_IDP_CREDENTIALS_FOR_USERID = "Failed to update Credentials for User ID '<>' in IDP";
	public static String FAILED_TO_LIST_IDP_CREDENTIALS_FOR_USERID = "Failed to list Credentials for User ID '<>' in IDP";
	
	//WebAuthn
	public static String FAILED_TO_LIST_IDP_WEB_AUTHN_POLICY_DETAILS_FOR_USERNAME = "Failed to list WebAuthn Policy Details for User Name '<>' in IDP";
	public static String FAILED_TO_EXECUTE_IDP_WEB_AUTHN_REGISTER_FOR_USERNAME = "Failed to execute WebAuthn Registration for User Name '<>' in IDP";
	
	//Push Notification
	public static String FAILED_TO_ADD_PUSHNOTIFICATION_DEVICE = "Failed to add PushNotification Devices of User";
	public static String FAILED_TO_SEND_PUSHNOTIFICATION_TO_USER_DEVICE = "Failed to send PushNotification to Device of User with name '<>'";
	
	//SMS
	public static String FAILED_TO_SEND_SMS_TO_USER_DEVICE = "Failed to send SMS to Device of User with name '<>'";
	
	
	public static String OTP_NOT_FOUND_FOR_USER = "OTP '<>' validation failed for User";
	public static String OTP_VERIFICATION_FAILED_FOR_USER = "Failed to validate OTP of User";
	
	//SVG
	public static String VISUAL_CREDENTIAL_CONVERTED_FROM_SVG_TO_PNG = "Successfully generated '<>' Visual Credential using SVG to PNG conversion";
	public static String ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL_FROM_SVG_TO_PNG = "Exception occured while generating '<>' Visual Credential using SVG to PNG conversion";
	public static String ERROR_WHILE_GENERATING_VISUAL_CREDENTIAL = "Exception occured while generating '<>' Visual Credential Entity for User with username '<>'";
	public static String BASE64_CONVERSION_OF_PNG_FAILED_FOR_VISUAL_CREDENTIAL = "Base64 encoded value of PNG format for '<>' Visual Credentail is NULL";
	public static String FAILED_TO_GET_WORKFLOW_FOR_USERID = "Failed to get Workflow for userID '<>'";
	public static String FAILED_TO_GET_VISUAL_CREDENTIALS_FOR_USERID = "Failed to get Visual Credential for userID '<>'";
	public static String ERROR_WHILE_FETCHING_VISUAL_CREDENTIALS_OF_WORKFLOW_UNDER_ORGANIZATION = "Failed to fetch Visual Credentials of Workflow with workflow name '<>' under Organization '<>'";
	public static String ERROR_WHILE_SAVING_VISUAL_CREDENTIAL_UNDER_ORGANIZATION = "Failed to save '<>' Visual Credential under Organization '<>'";
	public static String ERROR_IN_SAVING_VISUAL_CREDENTIAL_WITH_REASON = "Failed to save '<>' Visual Credential under Organization '<>': Reason: '<>'";
	public static String ERROR_WHILE_FETCHING_VISUAL_CREDENTIALS_UNDER_ORGANIZATION = "Failed to fetch Visual Credentials under Organization '<>'";
	public static String ERROR_WHILE_FETCHING_VISUAL_CREDENTIAL_WITH_NAME_UNDER_ORGANIZATION = "Failed to fetch Visual Credential with name '<>' under Organization '<>'";
	public static String ERROR_WHILE_UPDATING_VISUAL_CREDENTIAL_WITH_NAME_UNDER_ORGANIZATION = "Failed to update Visual Credential with name '<>' under Organization '<>'";
	public static String ERROR_IN_UPDATING_VISUAL_CREDENTIAL_WITH_REASON = "Failed to update '<>' Visual Credential under Organization '<>': Reason: '<>'";
	public static String ERROR_WHILE_GENERATING_VISUAL_CREDENTIALS_FOR_USERID = "Failed to generate Visual Credential for userID '<>'";
	public static String ERROR_WHILE_GENERATING_VISUAL_CREDENTIALS_FOR_CREDENTIALTEMPLATEID = "Failed to generate Visual Credential for credentialTemplateID '<>'";
	
	public static String ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS_WITH_KEY = "Exception occured while parsing '<>' Visual Credential details with KEY <>";
	public static String ERROR_WHILE_PARSING_VISUAL_CREDENTIAL_DETAILS = "Exception occured while parsing '<>' Visual Credential details";
	public static String ERROR_WHILE_VALIDATING_VISUAL_CREDENTIAL_DETAILS = "Exception occured while parsing '<>' Visual Credential details";
	

	
	//Dashboard
	public static String FAILED_TO_FETCH_PENDINGENROLLMENTS_UNDER_ORGANIZATION = "Failed to fetch Pending Enrollment user details under Organization '<>'";
	public static String FAILED_TO_FETCH_PENDINGISSUANCES_UNDER_ORGANIZATION = "Failed to fetch Pending Issuance user details under Organization '<>'";
	public static String FAILED_TO_FETCH_PENDINGAPPROVAL_UNDER_ORGANIZATION = "Failed to fetch Pending Approval user details under Organization '<>'";
	public static String FAILED_TO_FETCH_PENDING_ENROLLMENTS_ISSUANCES_UNDER_ORGANIZATION = "Failed to fetch Pending Enrollment and Issuance user stats under Organization '<>'";
	public static String FAILED_TO_APPROVE_REQUEST = "Failed to approve request under Organization '<>'";
	public static String FAILED_TO_REJECT_REQUEST = "Failed to reject request under Organization '<>'";
	
	//Client Info

	public static String CLIENT_NOT_FOUND = "Client details missing under Organization '<>'";
	public static String INVALID_PUBLICKEY = "Invalid public key sent in Device Informatiom with device uid '<>' for User ID '<>' under Organization '<>'";
	
	//Crypto Info

	public static String INVALID_KEY_OR_ALGORITHM_USED = "Invalid Key or Algorithm is used to secure the data under Organization '<>'";

	// Rest API headers
	public static String HEADER_REQUIRED = "Required header: <>";
	public static String HEADER_VALUE_REQUIRED = "Required header value for key: <>";
	public static String FAILED_TO_SEND_NOTIFICATION_FOR_PAIR_MOBILE_WITH_USERNAME = "Failed to send notification email for pair mobile device for User with username '<>'";
	
	// FriendlyName
	public static String FRIENDLYNAME_ALREADY_EXISTS_WITH_SAME_NAME = "Friendly Name aleady exists with same name '<>'";
	public static String ERROR_WHILE_CHECKING_FOR_FRIENDLY_NAME_WITH_NAME = "Error while checking for friendly name with <>";
}
