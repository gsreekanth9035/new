package com.meetidentity.usermanagement.exception;

import com.meetidentity.usermanagement.util.Util;

public class UIMSException extends Exception {

	private static final long serialVersionUID = 1L;

	public final int errorCode;

	public UIMSException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}
	
	public UIMSException(int errorCode) {
		super();
		this.errorCode = errorCode;
	}

	public UIMSException(String errorMessage, int errorCode) {
		super(errorMessage);
		this.errorCode = errorCode;
	}
	
	public UIMSException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = -1;
	}
	
	public UIMSException(String errorMessage, String insertMsgs , Throwable cause) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}

	public UIMSException(Throwable cause, String errorMessage, Object... insertMsgs) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	public UIMSException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause);
		this.errorCode = errorCode;
	}

	public UIMSException(Throwable cause, int errorCode) {
		super(cause);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
