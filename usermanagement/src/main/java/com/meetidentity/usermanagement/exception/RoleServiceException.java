package com.meetidentity.usermanagement.exception;

import com.meetidentity.usermanagement.util.Util;

public class RoleServiceException extends UIMSRuntimeException {

	private static final long serialVersionUID = 1L;

	private final int errorCode;
	
	public RoleServiceException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}

	public RoleServiceException(int errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	public RoleServiceException(String errorMessage, int errorCode) {
		super(errorMessage, errorCode);
		this.errorCode = errorCode;
	}

	public RoleServiceException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = -1;
	}
	
	public RoleServiceException(Throwable cause, String errorMessage, Object... insertMsgs ) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	
	public RoleServiceException(String errorMessage, String insertMsgs , Throwable cause) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	
	public RoleServiceException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause, errorCode);
		this.errorCode = errorCode;
	}

	public RoleServiceException(Throwable cause, int errorCode) {
		super(cause, errorCode);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
