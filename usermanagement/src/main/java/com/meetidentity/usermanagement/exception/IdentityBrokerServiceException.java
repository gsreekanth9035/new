package com.meetidentity.usermanagement.exception;

import com.meetidentity.usermanagement.util.Util;

public class IdentityBrokerServiceException extends UIMSRuntimeException {

	private static final long serialVersionUID = 1L;

	public final int errorCode;

	public IdentityBrokerServiceException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}
	
	public IdentityBrokerServiceException(int errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	public IdentityBrokerServiceException(String errorMessage, int errorCode) {
		super(errorMessage, errorCode);
		this.errorCode = errorCode;
	}
	
	public IdentityBrokerServiceException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = -1;
	}
	
	public IdentityBrokerServiceException(Throwable cause, String errorMessage, Object... insertMsgs) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}

	public IdentityBrokerServiceException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause, errorCode);
		this.errorCode = errorCode;
	}

	public IdentityBrokerServiceException(Throwable cause, int errorCode) {
		super(cause, errorCode);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
