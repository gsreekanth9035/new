package com.meetidentity.usermanagement.exception;

import com.meetidentity.usermanagement.util.Util;

public class UIMSRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public final int errorCode;

	public UIMSRuntimeException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}
	
	public UIMSRuntimeException(int errorCode) {
		super();
		this.errorCode = errorCode;
	}

	public UIMSRuntimeException(String errorMessage, int errorCode) {
		super(errorMessage);
		this.errorCode = errorCode;
	}
	
	public UIMSRuntimeException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = -1;
	}

	public UIMSRuntimeException(String errorMessage, String insertMsgs , Throwable cause) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	public UIMSRuntimeException(Throwable cause, String errorMessage, Object... insertMsgs ) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;	
	}
	
	public UIMSRuntimeException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause);
		this.errorCode = errorCode;
	}

	public UIMSRuntimeException(Throwable cause, int errorCode) {
		super(cause);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
