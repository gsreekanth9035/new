package com.meetidentity.usermanagement.exception;

import com.meetidentity.usermanagement.util.Util;

public class DeviceProfileServiceException extends UIMSRuntimeException {

	private static final long serialVersionUID = 1L;

	private final int errorCode;
	
	public DeviceProfileServiceException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}

	public DeviceProfileServiceException(int errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	public DeviceProfileServiceException(String errorMessage, int errorCode) {
		super(errorMessage, errorCode);
		this.errorCode = errorCode;
	}

	public DeviceProfileServiceException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = -1;
	}
	
	public DeviceProfileServiceException(Throwable cause, String errorMessage, Object... insertMsgs ) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	
	public DeviceProfileServiceException(String errorMessage, String insertMsgs , Throwable cause) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	
	public DeviceProfileServiceException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause, errorCode);
		this.errorCode = errorCode;
	}

	public DeviceProfileServiceException(Throwable cause, int errorCode) {
		super(cause, errorCode);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
