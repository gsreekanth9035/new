package com.meetidentity.usermanagement.exception;

import com.meetidentity.usermanagement.util.Util;

public class EntityUpdateException extends UIMSRuntimeException {

	private static final long serialVersionUID = 1L;

	private final int errorCode;
	
	public EntityUpdateException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}

	public EntityUpdateException(int errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	public EntityUpdateException(String errorMessage, int errorCode) {
		super(errorMessage, errorCode);
		this.errorCode = errorCode;
	}

	public EntityUpdateException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = -1;
	}
	
	public EntityUpdateException(Throwable cause, String errorMessage, Object... insertMsgs ) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
		
	public EntityUpdateException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause, errorCode);
		this.errorCode = errorCode;
	}

	public EntityUpdateException(Throwable cause, int errorCode) {
		super(cause, errorCode);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
