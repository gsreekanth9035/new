package com.meetidentity.usermanagement.exception;

import com.meetidentity.usermanagement.util.Util;

public class NotifyServiceException extends UIMSRuntimeException {

	private static final long serialVersionUID = 1L;

	private final int errorCode;
	
	public NotifyServiceException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}

	public NotifyServiceException(int errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	public NotifyServiceException(String errorMessage, int errorCode) {
		super(errorMessage, errorCode);
		this.errorCode = errorCode;
	}

	public NotifyServiceException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = -1;
	}
	
	public NotifyServiceException(Throwable cause, String errorMessage, Object... insertMsgs) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	
	public NotifyServiceException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause, errorCode);
		this.errorCode = errorCode;
	}

	public NotifyServiceException(Throwable cause, int errorCode) {
		super(cause, errorCode);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
