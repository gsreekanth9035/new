package com.meetidentity.usermanagement.exception;

import com.meetidentity.usermanagement.util.Util;

public class EnrollmentServiceException extends UIMSException {

	private static final long serialVersionUID = 1L;

	private final int errorCode;
	
	public EnrollmentServiceException(String errorMessage) {
		super(errorMessage);
		this.errorCode = -1;
	}

	public EnrollmentServiceException(int errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	public EnrollmentServiceException(String errorMessage, int errorCode) {
		super(errorMessage, errorCode);
		this.errorCode = errorCode;
	}

	public EnrollmentServiceException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCode = -1;
	}
	
	public EnrollmentServiceException(Throwable cause, String errorMessage, Object... insertMsgs) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	
	public EnrollmentServiceException(String errorMessage, Object... insertMsgs) {
		super(Util.format(errorMessage, insertMsgs));
		this.errorCode = -1;
	}
	
	public EnrollmentServiceException(String errorMessage, String insertMsgs , Throwable cause) {
		super(Util.format(errorMessage, insertMsgs), cause);
		this.errorCode = -1;
	}
	
	public EnrollmentServiceException(String errorMessage, Throwable cause, int errorCode) {
		super(errorMessage, cause, errorCode);
		this.errorCode = errorCode;
	}

	public EnrollmentServiceException(Throwable cause, int errorCode) {
		super(cause, errorCode);
		this.errorCode = errorCode;
	}

	public int getCode() {
		return this.errorCode;
	}
}
