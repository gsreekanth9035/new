package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;

public class UserHealthID {

	private String uimsId;

	private String idNumber;

	private String primarySubscriber;

	private String primarySubscriberID;

	private String pcp;

	private String pcpPhone;

	private String policyNumber;

	private String groupPlan;

	private String healthPlanNumber;
	
	private String issuerIN;

	private String agencyCardSN;

	private Date dateOfIssuance;

	private Date dateOfExpiry;


	
	
	
	public UserHealthID(String uimsId, String idNumber, String primarySubscriber, String primarySubscriberID,
			String pcp, String pcpPhone, String policyNumber, String groupPlan, String healthPlanNumber,
			String issuerIN, String agencyCardSN, Date dateOfIssuance, Date dateOfExpiry) {
		super();
		this.uimsId = uimsId;
		this.idNumber = idNumber;
		this.primarySubscriber = primarySubscriber;
		this.primarySubscriberID = primarySubscriberID;
		this.pcp = pcp;
		this.pcpPhone = pcpPhone;
		this.policyNumber = policyNumber;
		this.groupPlan = groupPlan;
		this.healthPlanNumber = healthPlanNumber;
		this.issuerIN = issuerIN;
		this.agencyCardSN = agencyCardSN;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
	}


	public UserHealthID() {
		
	}


	public String getUimsId() {
		return uimsId;
	}


	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}


	public String getIdNumber() {
		return idNumber;
	}


	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}


	public String getPrimarySubscriber() {
		return primarySubscriber;
	}


	public void setPrimarySubscriber(String primarySubscriber) {
		this.primarySubscriber = primarySubscriber;
	}


	public String getPrimarySubscriberID() {
		return primarySubscriberID;
	}


	public void setPrimarySubscriberID(String primarySubscriberID) {
		this.primarySubscriberID = primarySubscriberID;
	}


	public String getPcp() {
		return pcp;
	}


	public void setPcp(String pcp) {
		this.pcp = pcp;
	}


	public String getPcpPhone() {
		return pcpPhone;
	}


	public void setPcpPhone(String pcpPhone) {
		this.pcpPhone = pcpPhone;
	}


	public String getPolicyNumber() {
		return policyNumber;
	}


	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}


	public String getGroupPlan() {
		return groupPlan;
	}


	public void setGroupPlan(String groupPlan) {
		this.groupPlan = groupPlan;
	}


	public String getHealthPlanNumber() {
		return healthPlanNumber;
	}


	public void setHealthPlanNumber(String healthPlanNumber) {
		this.healthPlanNumber = healthPlanNumber;
	}


	public Date getDateOfIssuance() {
		return dateOfIssuance;
	}


	public void setDateOfIssuance(Date dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}


	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}


	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}


	public String getIssuerIN() {
		return issuerIN;
	}


	public void setIssuerIN(String issuerIN) {
		this.issuerIN = issuerIN;
	}


	public String getAgencyCardSN() {
		return agencyCardSN;
	}


	public void setAgencyCardSN(String agencyCardSN) {
		this.agencyCardSN = agencyCardSN;
	}

	
	
}
