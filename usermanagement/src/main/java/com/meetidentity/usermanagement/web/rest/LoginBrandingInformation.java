package com.meetidentity.usermanagement.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.OrganizationService;
import com.meetidentity.usermanagement.web.rest.model.OrganizationBrandingInfo;

@RestController
@RequestMapping("/api/v1/getOrganizationBrandingInfo")
public class LoginBrandingInformation {
	
	@Autowired
	private OrganizationService organizationService;
	
	@GetMapping("/{organizationName}")
	OrganizationBrandingInfo brandingInformation(@PathVariable("organizationName") String organizationName) {
		OrganizationBrandingInfo organizationBrandingInfo = organizationService.getOrgBrandingByName(organizationName);
		return organizationBrandingInfo;
	}
}
