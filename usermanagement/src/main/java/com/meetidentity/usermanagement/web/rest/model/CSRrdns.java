package com.meetidentity.usermanagement.web.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CSRrdns {
	private String CN;
	private String OU;
	private String O;
	private String L;
	private String S;
	private String C;
	private String E;
	private String SERIALNUMBER;

	public String getCN() {
		return CN;
	}

	public void setCN(String cN) {
		CN = cN;
	}

	public String getOU() {
		return OU;
	}

	public void setOU(String oU) {
		OU = oU;
	}

	public String getO() {
		return O;
	}

	public void setO(String o) {
		O = o;
	}

	public String getL() {
		return L;
	}

	public void setL(String l) {
		L = l;
	}

	public String getS() {
		return S;
	}

	public void setS(String s) {
		S = s;
	}

	public String getC() {
		return C;
	}

	public void setC(String c) {
		C = c;
	}

	public String getE() {
		return E;
	}

	public void setE(String e) {
		E = e;
	}

	public String getSERIALNUMBER() {
		return SERIALNUMBER;
	}

	public void setSERIALNUMBER(String sERIALNUMBER) {
		SERIALNUMBER = sERIALNUMBER;
	}

}