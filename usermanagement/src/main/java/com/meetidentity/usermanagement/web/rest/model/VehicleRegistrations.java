package com.meetidentity.usermanagement.web.rest.model;

import java.util.List;

public class VehicleRegistrations {
	
	private List<VehicleRegistration> vehicleRegistrations;

	public List<VehicleRegistration> getVehicleRegistrations() {
		return vehicleRegistrations;
	}

	public void setVehicleRegistrations(List<VehicleRegistration> vehicleRegistrations) {
		this.vehicleRegistrations = vehicleRegistrations;
	}
	
	

}
