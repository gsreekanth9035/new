package com.meetidentity.usermanagement.web.rest.model;

public class PushNotificationPayLoad {

	private String data;
	private String key;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
