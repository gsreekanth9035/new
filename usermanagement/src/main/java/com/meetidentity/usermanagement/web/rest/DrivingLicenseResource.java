package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.DrivingLicenseService;
import com.meetidentity.usermanagement.web.rest.model.DrivingLicenseRestriction;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/drivingLicense")
public class DrivingLicenseResource {
	
	@Autowired
	private DrivingLicenseService drivingLicenseService;

	
	@GetMapping("/restrictions")
	public List<DrivingLicenseRestriction> getdrivingLicenseRestrictions(@PathVariable("organizationName") String organizationName) {
		return drivingLicenseService.getDrivingLicenseRestrictions(organizationName);
	}
	

	
}
