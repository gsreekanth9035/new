package com.meetidentity.usermanagement.web.rest.model;

public class DeviceProfile {
	private Long id;
	private String name;
	private String description;
	private ConfigValue configValueCategory;
	private ConfigValue configValueSupplier;
	private ConfigValue configValueProductName;
	private String keyStorageType;
	private DeviceProfileKeyManager devProfKeyMgr;
	private DeviceProfileHsmInfo devProfHsmInfo;
	private DeviceProfileConfig deviceProfileConfig;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getKeyStorageType() {
		return keyStorageType;
	}

	public ConfigValue getConfigValueCategory() {
		return configValueCategory;
	}

	public void setConfigValueCategory(ConfigValue configValueCategory) {
		this.configValueCategory = configValueCategory;
	}

	public ConfigValue getConfigValueSupplier() {
		return configValueSupplier;
	}

	public void setConfigValueSupplier(ConfigValue configValueSupplier) {
		this.configValueSupplier = configValueSupplier;
	}

	public ConfigValue getConfigValueProductName() {
		return configValueProductName;
	}

	public void setConfigValueProductName(ConfigValue configValueProductName) {
		this.configValueProductName = configValueProductName;
	}

	public void setKeyStorageType(String storageType) {
		this.keyStorageType = storageType;
	}

	public DeviceProfileKeyManager getDevProfKeyMgr() {
		return devProfKeyMgr;
	}

	public void setDevProfKeyMgr(DeviceProfileKeyManager devProfKeyMgr) {
		this.devProfKeyMgr = devProfKeyMgr;
	}

	public DeviceProfileHsmInfo getDevProfHsmInfo() {
		return devProfHsmInfo;
	}

	public void setDevProfHsmInfo(DeviceProfileHsmInfo devProfHsmInfo) {
		this.devProfHsmInfo = devProfHsmInfo;
	}

	public DeviceProfileConfig getDeviceProfileConfig() {
		return deviceProfileConfig;
	}

	public void setDeviceProfileConfig(DeviceProfileConfig deviceProfileConfig) {
		this.deviceProfileConfig = deviceProfileConfig;
	}

}
