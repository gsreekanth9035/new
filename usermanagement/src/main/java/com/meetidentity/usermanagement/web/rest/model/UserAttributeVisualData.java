package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;

public class UserAttributeVisualData {

	private String nationality;
	private Date dateOfBirth;
	private String email;	
	private String gender;
	private String address;

	public UserAttributeVisualData() {

	}
	
	

	public UserAttributeVisualData(Date dateOfBirth, String nationality, String placeOfBirth, String gender, long contact,
			String address, String veteran, String organDonor, String bloodType) {
		super();
		this.dateOfBirth = dateOfBirth;
		this.nationality = nationality;
		this.gender = gender;
		this.address = address;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
