package com.meetidentity.usermanagement.web.rest;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.keycloak.domain.CredentialRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.WebAuthnAuthenticatorRegistration;
import com.meetidentity.usermanagement.keycloak.domain.WebAuthnPolicyDetails;
import com.meetidentity.usermanagement.security.SpringSecurityAuditorAware;
import com.meetidentity.usermanagement.service.IdentityProviderUserService;
import com.meetidentity.usermanagement.service.KeycloakService;
import com.meetidentity.usermanagement.web.rest.model.IDPUserCredentialConfigResponse;
import com.meetidentity.usermanagement.web.rest.model.IDPUserCredentials;
import com.meetidentity.usermanagement.web.rest.model.IdentityProviderUser;
import com.meetidentity.usermanagement.web.rest.model.User;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/idp-users")
public class IdentityProviderUserResource {

    private final Logger log = LoggerFactory.getLogger(IdentityProviderUserResource.class);
	@Autowired
	private IdentityProviderUserService identityProviderUserService;
	@Autowired
	private SpringSecurityAuditorAware springSecurityAuditorAware;
	
	@Autowired
	private KeycloakService keycloakService;
	
	@PostMapping
	public ResponseEntity<User> createIdentityProviderUser(@RequestHeader HttpHeaders httpHeaders, @PathVariable("organizationName") String organizationName, @RequestBody IdentityProviderUser identityProviderUser) throws Exception {
		User user = null;
		if(identityProviderUser.checkForEmpty()) {
			return new ResponseEntity<User>(user, HttpStatus.BAD_REQUEST);
		}
		/*
		 * System.out.println(springSecurityAuditorAware.getCurrentAuditor());
		 * System.out.println(springSecurityAuditorAware.getCurrentAuditorGroupID());
		 * System.out.println(springSecurityAuditorAware.getCurrentAuditorGroupName());
		 * System.out.println(springSecurityAuditorAware.getCurrentAuditorAuthorization(
		 * )); System.out.println(springSecurityAuditorAware.getCurrentContentType());
		 * 
		 */
		return identityProviderUserService.createUserAndAssignGroupAndRoles(httpHeaders, organizationName, identityProviderUser, null);
		
	}
	
	@PostMapping("/roles")
	public void updateUserRoles(@PathVariable("organizationName") String organizationName, @RequestBody IdentityProviderUser identityProviderUser) throws Exception {

		identityProviderUserService.updateUserRoles(organizationName, identityProviderUser);
		
	}
	
	@PostMapping("/local-register")
	public void localRegistration(@RequestBody IdentityProviderUser identityProviderUser) throws Exception {
		identityProviderUserService.createUserLocallyByIDP(identityProviderUser);
	}

	@PutMapping
	public ResponseEntity<User> updateIdentityProviderUser(@RequestHeader HttpHeaders httpHeaders, @PathVariable("organizationName") String organizationName, @RequestBody IdentityProviderUser identityProviderUser) throws Exception {
		User user = null;
		if(identityProviderUser.checkForEmpty()) {
			return new ResponseEntity<User>(user, HttpStatus.BAD_REQUEST);
		}
		return identityProviderUserService.updateUserAndAssignGroupAndRoles(httpHeaders, organizationName, identityProviderUser);
		
	}
	@GetMapping("/{userName}/onboardUserDetails")
	public ResponseEntity<IdentityProviderUser> getOnboardDetailsOfUserByName(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		return identityProviderUserService.getOnboardUserDetails(organizationName, userName);
	}
	
	@PostMapping("/{userId}/user-credentials/config")
	public ResponseEntity<IDPUserCredentialConfigResponse> addUserCredentials(@PathVariable("organizationName") String organizationName, @PathVariable("userId") Long userId, @RequestBody IDPUserCredentials idpUserCredentials) throws Exception {
		return identityProviderUserService.addUserCredentials(userId, organizationName, idpUserCredentials);
	}
	
	@PostMapping("/{userId}/user-credentials/update")
	public void updateUserCredentials(@PathVariable("organizationName") String organizationName, @PathVariable("userId") Long userId, @RequestBody IDPUserCredentials idpUserCredentials) throws Exception {
		identityProviderUserService.updateUserCredentials(userId, organizationName, idpUserCredentials);
	}
	
	@PostMapping("/{userId}/user-credentials/delete")
	public void deleteUserCredentials(@PathVariable("organizationName") String organizationName, @PathVariable("userId") Long userId,  @RequestBody IDPUserCredentials idpUserCredentials) throws Exception {
		identityProviderUserService.deleteUserCredentials(userId, organizationName, idpUserCredentials);
	}
	
	@GetMapping("/{userId}/user-credentials/list")
	public String listUserCredentials(@PathVariable("organizationName") String organizationName, @PathVariable("userId") Long userId) throws Exception {
		return identityProviderUserService.listUserCredentials(userId, organizationName);
	}

	@GetMapping("/{userId}/user-credentials")
	public List<CredentialRepresentation> getUserCredentialsById(@PathVariable("organizationName") String organizationName, @PathVariable("userId") String userId) {
		return keycloakService.getCredentialsByUserId(organizationName, userId);
	}
	
	@GetMapping("/{userId}/user-credentials/{credentialId}/moveAfter/{newPreviousCredentialId}")
	public ResponseEntity<String> moveCredentialAfterAnotherCredential(@PathVariable("organizationName") String organizationName, @PathVariable("userId") String userId,
			@PathVariable("credentialId") String credentialId, @PathVariable("newPreviousCredentialId") String newPreviousCredentialId) {
		return keycloakService.moveCredentialAfterAnotherCredential(organizationName, userId, credentialId, newPreviousCredentialId);
	}
	
	@GetMapping("/{userId}/user-credentials/{credentialId}/moveToFirst")
	public ResponseEntity<String> moveCredentialToFirst(@PathVariable("organizationName") String organizationName, @PathVariable("userId") String userId,
			@PathVariable("credentialId") String credentialId) {
		return keycloakService.moveCredentialToFirst(organizationName, userId, credentialId);
	}
	
	@GetMapping("/{userName}/webauthn-policy-details")
	public WebAuthnPolicyDetails getIDPWebAuthnRegisterPolicyDetails(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) throws Exception {
		ResponseEntity<WebAuthnPolicyDetails> response = identityProviderUserService.getIDPWebAuthnRegisterPolicyDetails(userName, organizationName);
		log.info(response.getBody().toString());
		return response.getBody();
	}
		
	@PostMapping("/{userName}/webauthn-register-authenticator")
	public WebAuthnAuthenticatorRegistration registerWebAuthnAuthenticator(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName,  @RequestBody WebAuthnAuthenticatorRegistration webAuthnAuthenticatorRegistration) throws Exception {
		ResponseEntity<WebAuthnAuthenticatorRegistration> response = identityProviderUserService.registerWebAuthnAuthenticator(userName, organizationName, webAuthnAuthenticatorRegistration);
		log.info(response.getBody().toString());
		return response.getBody();
	}
}
