package com.meetidentity.usermanagement.web.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for view and managing Log Level at runtime.
 */
@RestController
public class AuthResource {

    @GetMapping("/login_error")
    public String loginError() {
    	return "/";
    }

}
