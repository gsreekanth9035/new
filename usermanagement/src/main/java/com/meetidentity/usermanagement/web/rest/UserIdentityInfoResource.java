package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.exception.PairDeviceInfoServiceException;
import com.meetidentity.usermanagement.service.UserCredentialsService;
import com.meetidentity.usermanagement.service.UserPairDeviceInfoService;
import com.meetidentity.usermanagement.web.rest.model.PairMobileDeviceConfig;
import com.meetidentity.usermanagement.web.rest.model.UserIdentityData;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/users/{userID}")
public class UserIdentityInfoResource {

	@Autowired
	private UserCredentialsService userCredentialsService;

	@GetMapping("/userIdentity/{identitySerialNumber}/credentials/{credentialTemplateID}")
	public List<UserIdentityData> getUserIdentityWithSerialNumberAndTemplateID(
			@PathVariable("organizationName") String organizationName, @PathVariable("userID") Long userID,
			@PathVariable("identitySerialNumber") String identitySerialNumber,
			@PathVariable("credentialTemplateID") Long credentialTemplateID) {
		return userCredentialsService.getUserIdentityWithSerialNumberAndTemplateID(organizationName, userID,
				identitySerialNumber, credentialTemplateID);
	}

	@GetMapping("/userAge")
	public List<UserIdentityData> getUserAgeWithID(@PathVariable("organizationName") String organizationName,
			@PathVariable("userID") Long userID) {
		return userCredentialsService.getUserAgeWithID(organizationName, userID);
	}
}
