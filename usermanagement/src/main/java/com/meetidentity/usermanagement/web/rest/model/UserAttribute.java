package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;

public class UserAttribute {

	private String firstName;

	private String middleName;

	private String lastName;

	private String mothersMaidenName;

	private String email;

	private Date dateOfBirth;

	private String nationality;

	private String placeOfBirth;

	private String gender;

	private String countryCode;
	
	private String contact;

	private String address;

	private boolean veteran;

	private boolean organDonor;

	private String bloodType;

	public UserAttribute() {

	}

	public UserAttribute(Date dateOfBirth, String nationality, String placeOfBirth, String gender, String contact,
			String address, boolean veteran, boolean organDonor, String bloodType) {
		super();
		this.dateOfBirth = dateOfBirth;
		this.nationality = nationality;
		this.placeOfBirth = placeOfBirth;
		this.gender = gender;
		this.contact = contact;
		this.address = address;
		this.veteran = veteran;
		this.organDonor = organDonor;
		this.bloodType = bloodType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMothersMaidenName() {
		return mothersMaidenName;
	}

	public void setMothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isVeteran() {
		return veteran;
	}

	public void setVeteran(boolean veteran) {
		this.veteran = veteran;
	}

	public boolean isOrganDonor() {
		return organDonor;
	}

	public void setOrganDonor(boolean organDonor) {
		this.organDonor = organDonor;
	}

	public String getBloodType() {
		return bloodType;
	}

	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
