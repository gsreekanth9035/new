package com.meetidentity.usermanagement.web.rest.model;

public class BiometricVerificationResponse {

	private String statusMessage;
	private String sessionId;
	//private BiometricDuplicateInformation bioDuplicateInfo;

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/*public BiometricDuplicateInformation getBioDuplicateInfo() {
		return bioDuplicateInfo;
	}

	public void setBioDuplicateInfo(BiometricDuplicateInformation bioDuplicateInfo) {
		this.bioDuplicateInfo = bioDuplicateInfo;
	}*/
	
}
