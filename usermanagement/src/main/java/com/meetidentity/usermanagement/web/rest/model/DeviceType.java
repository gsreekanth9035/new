package com.meetidentity.usermanagement.web.rest.model;

public class DeviceType {
	private Long id;
	private String name;
	private Boolean visualApplicable;
	private Boolean active;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getVisualApplicable() {
		return visualApplicable;
	}

	public void setVisualApplicable(Boolean visualApplicable) {
		this.visualApplicable = visualApplicable;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
