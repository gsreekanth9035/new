package com.meetidentity.usermanagement.web.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.biopost.PrepareProcess;
import com.meetidentity.usermanagement.biopost.ProcessTriada;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/p-triada")
public class ProcessTriadaProviderResource {

	@PostMapping
	public ResponseEntity<String> saveTriada(@PathVariable("organizationName") String organizationName,
			@RequestBody ProcessTriada processTriada) throws Exception {

		try {

			PrepareProcess.init(processTriada);
			PrepareProcess.post();
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>("success", HttpStatus.OK);
	}

}
