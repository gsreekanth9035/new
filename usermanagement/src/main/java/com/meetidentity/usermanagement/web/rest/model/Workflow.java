package com.meetidentity.usermanagement.web.rest.model;

import java.time.Instant;
import java.util.List;

public class Workflow {

	private String name;
	private String description;
	private boolean defaultWorkflow;
	private OrganizationIdentity organizationIdentities;
	private List<DeviceProfile> deviceProfiles;
	private List<Group> groups;
	private boolean allowedDevicesApplicable;
	private Integer allowedDevices;
	private Integer expirationInMonths;
	private Integer expirationInDays;
	private List<WorkflowStep> workflowSteps;

	private boolean canEdit;
	private boolean canDelete;
	private boolean disableExpirationEnforcement;
	private boolean revokeEncryptionCertificate;

	private Boolean requiredEnrollment;
	private Boolean requiredTokenCardIssuance;
	private Boolean requiredMobileIssuance;

	private Instant createdDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isDefaultWorkflow() {
		return defaultWorkflow;
	}

	public void setDefaultWorkflow(boolean defaultWorkflow) {
		this.defaultWorkflow = defaultWorkflow;
	}

	public List<DeviceProfile> getDeviceProfiles() {
		return deviceProfiles;
	}

	public void setDeviceProfiles(List<DeviceProfile> deviceProfiles) {
		this.deviceProfiles = deviceProfiles;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public Integer getAllowedDevices() {
		return allowedDevices;
	}

	public void setAllowedDevices(Integer allowedDevices) {
		this.allowedDevices = allowedDevices;
	}

	public Integer getExpirationInMonths() {
		return expirationInMonths;
	}

	public void setExpirationInMonths(Integer expirationInMonths) {
		this.expirationInMonths = expirationInMonths;
	}

	public Integer getExpirationInDays() {
		return expirationInDays;
	}

	public void setExpirationInDays(Integer expirationInDays) {
		this.expirationInDays = expirationInDays;
	}

	public List<WorkflowStep> getWorkflowSteps() {
		return workflowSteps;
	}

	public void setWorkflowSteps(List<WorkflowStep> workflowSteps) {
		this.workflowSteps = workflowSteps;
	}

	public boolean isCanEdit() {
		return canEdit;
	}

	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	public boolean isCanDelete() {
		return canDelete;
	}

	public void setCanDelete(boolean canDelete) {
		this.canDelete = canDelete;
	}

	public void setCeatedDate(Instant createdDate) {
		this.createdDate = (createdDate);
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public boolean isDisableExpirationEnforcement() {
		return disableExpirationEnforcement;
	}

	public void setDisableExpirationEnforcement(boolean disableExpirationEnforcement) {
		this.disableExpirationEnforcement = disableExpirationEnforcement;
	}

	public boolean isRevokeEncryptionCertificate() {
		return revokeEncryptionCertificate;
	}

	public void setRevokeEncryptionCertificate(boolean revokeEncryptionCertificate) {
		this.revokeEncryptionCertificate = revokeEncryptionCertificate;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public OrganizationIdentity getOrganizationIdentities() {
		return organizationIdentities;
	}

	public void setOrganizationIdentities(OrganizationIdentity organizationIdentities) {
		this.organizationIdentities = organizationIdentities;
	}

	public Boolean getRequiredEnrollment() {
		return requiredEnrollment;
	}

	public void setRequiredEnrollment(Boolean requiredEnrollment) {
		this.requiredEnrollment = requiredEnrollment;
	}

	public Boolean getRequiredTokenCardIssuance() {
		return requiredTokenCardIssuance;
	}

	public void setRequiredTokenCardIssuance(Boolean requiredTokenCardIssuance) {
		this.requiredTokenCardIssuance = requiredTokenCardIssuance;
	}

	public Boolean getRequiredMobileIssuance() {
		return requiredMobileIssuance;
	}

	public void setRequiredMobileIssuance(Boolean requiredMobileIssuance) {
		this.requiredMobileIssuance = requiredMobileIssuance;
	}

	public boolean isAllowedDevicesApplicable() {
		return allowedDevicesApplicable;
	}

	public void setAllowedDevicesApplicable(boolean allowedDevicesApplicable) {
		this.allowedDevicesApplicable = allowedDevicesApplicable;
	}

}
