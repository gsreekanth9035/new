package com.meetidentity.usermanagement.web.rest.model;

public class CustomerBranding {

	private String logo;
	private String background;
	private String policyStatement;
	private String language;
	private String issuerCertificate;
	private String issuerPublicKey;
	private String issuerKeyAlgorithm;
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	
	public String getPolicyStatement() {
		return policyStatement;
	}
	public void setPolicyStatement(String policyStatement) {
		this.policyStatement = policyStatement;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}	
	public String getIssuerCertificate() {
		return issuerCertificate;
	}
	public void setIssuerCertificate(String issuerCertificate) {
		this.issuerCertificate = issuerCertificate;
	}
	public String getIssuerPublicKey() {
		return issuerPublicKey;
	}
	public void setIssuerPublicKey(String issuerPublicKey) {
		this.issuerPublicKey = issuerPublicKey;
	}
	public String getIssuerKeyAlgorithm() {
		return issuerKeyAlgorithm;
	}
	public void setIssuerKeyAlgorithm(String issuerKeyAlgorithm) {
		this.issuerKeyAlgorithm = issuerKeyAlgorithm;
	}
	
	
	
	
}
