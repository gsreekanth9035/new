package com.meetidentity.usermanagement.web.rest.model;

public class WFStepCertConfig {

	private Long id;
	private String certType;
	private String caServer;
	private String certTemplate;
	private boolean keyEscrow;
	private boolean disableRevoke;
	private int noOfDaysBeforeCertExpiry;
	private String algorithm;
	private int keysize;
	private String subjectDN;
	private String subjectAN;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getCaServer() {
		return caServer;
	}

	public void setCaServer(String caServer) {
		this.caServer = caServer;
	}

	public String getCertTemplate() {
		return certTemplate;
	}

	public void setCertTemplate(String certTemplate) {
		this.certTemplate = certTemplate;
	}

	public boolean isKeyEscrow() {
		return keyEscrow;
	}

	public void setKeyEscrow(boolean keyEscrow) {
		this.keyEscrow = keyEscrow;
	}

	public boolean isDisableRevoke() {
		return disableRevoke;
	}

	public void setDisableRevoke(boolean disableRevoke) {
		this.disableRevoke = disableRevoke;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public int getKeysize() {
		return keysize;
	}

	public void setKeysize(int keysize) {
		this.keysize = keysize;
	}

	public String getSubjectDN() {
		return subjectDN;
	}

	public void setSubjectDN(String subjectDN) {
		this.subjectDN = subjectDN;
	}

	public String getSubjectAN() {
		return subjectAN;
	}

	public void setSubjectAN(String subjectAN) {
		this.subjectAN = subjectAN;
	}

	public int getNoOfDaysBeforeCertExpiry() {
		return noOfDaysBeforeCertExpiry;
	}

	public void setNoOfDaysBeforeCertExpiry(int noOfDaysBeforeCertExpiry) {
		this.noOfDaysBeforeCertExpiry = noOfDaysBeforeCertExpiry;
	}

}
