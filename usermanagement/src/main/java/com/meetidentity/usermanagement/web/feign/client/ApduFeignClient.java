package com.meetidentity.usermanagement.web.feign.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.meetidentity.usermanagement.web.rest.model.UserDevice;
import com.meetidentity.usermanagement.web.rest.model.UserDeviceInfo;

@FeignClient(name = "apdu", url = "http://apdu:10004")
public interface ApduFeignClient {

	@PostMapping("/api/v1/encrypt")
	String encrypt(@RequestBody String toEncrypt);

	@PostMapping("/api/v1/decrypt")
	String decrypt(@RequestBody String toDecrypt);
	
	@PostMapping("/api/v1/devices")
	ResponseEntity<Void> pairUserDevice(@RequestBody UserDevice requestUserDevice );
	
	@GetMapping("/api/v1/users/{userID}/devices/getOrgIssuerPubKey")
	public UserDeviceInfo getOrgIssuerPubKey(@PathVariable("userID") String userID);
	
	@GetMapping("/api/v1/users/{userID}/devices/organizations/{organizationName}/orgIssuerCertificate")
	public UserDeviceInfo getOrgIssuerCertificate(@PathVariable("userID") Long userID, @PathVariable("organizationName") String organizationName);

	@DeleteMapping("/api/v1/devices/users/byID/{userID}")
	ResponseEntity<Void> deleteUserById(@PathVariable("userID") Long userID);

	@PostMapping("/{appName}/users/{userID}/status/{statusID}/sendStatusChangeNotification")
	void sendStatusChangeNotification(@PathVariable("appName") String appName, @PathVariable("userID") Long userID, @PathVariable("statusID") Long  statusID, @RequestBody UserDevice requestUserDevice);

	@GetMapping("/api/v1/users/{userID}/devices/organizations/{organizationName}/issuedUserDevices")
	public long getCountOfIssuedUserDevices(@PathVariable("userID") Long userID, @PathVariable("organizationName") String organizationName);

	@GetMapping("/api/v1/users/{userID}/devices/organizations/{organizationName}/issuedUserCredentials")
	public long getissuedUserCredentialsCount(@PathVariable("userID") Long userID, @PathVariable("organizationName") String organizationName);

	
}
