package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.IdentityTypeService;
import com.meetidentity.usermanagement.web.rest.model.ConfigValue;
import com.meetidentity.usermanagement.web.rest.model.OrganizationIdentity;
import com.meetidentity.usermanagement.web.rest.model.VisualTemplate;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}")
public class IdentityTypeResource {

	@Autowired
	private IdentityTypeService identityTypeService;
	
	@GetMapping("/issuingIdentityTypes")
	public List<OrganizationIdentity> getOrgIssuingIdentityModels(@PathVariable("organizationName") String organizationName) {
		return identityTypeService.getOrgIssuingIdentityTypes(organizationName);
	}

	@GetMapping("/trustedExistingIdentityTypes")
	public List<OrganizationIdentity> getOrgTrustedIdentityModels(@PathVariable("organizationName") String organizationName) {
		return identityTypeService.getOrgTrustedIdentityTypes(organizationName);
	}
	
	@GetMapping("/{identityTypeName}/{identityFieldName}/configs")
	List<ConfigValue> getUserOrgFaceCropSizes(@PathVariable("organizationName") String organizationName, 
			@PathVariable("identityTypeName") String identityTypeName, @PathVariable("identityFieldName") String identityFieldName) {
		return identityTypeService.getIdentityFieldConfigs(organizationName, identityTypeName, identityFieldName);
	}
	
	@GetMapping("/{identityTypeName}/visual-templates")
	List<VisualTemplate> getVisualTemplatesByIdentityModel(@PathVariable("organizationName") String organizationName, 
			@PathVariable("identityTypeName") String identityTypeName) {
		return identityTypeService.getVisualTemplatesByIdentityModel(organizationName, identityTypeName);
	}
}
