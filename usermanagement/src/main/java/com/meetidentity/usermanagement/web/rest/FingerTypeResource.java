package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.WorkflowService;
import com.meetidentity.usermanagement.web.rest.model.Finger;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/fingersByCount/{fingerCount}")
public class FingerTypeResource {

	@Autowired
	private WorkflowService workflowService;

	@GetMapping()
	public List<Finger> getWorkflow(@PathVariable("organizationName") String organizationName,
			@PathVariable("fingerCount") Integer fingerCount) {
		return workflowService.getFingersByFingerCount(organizationName, fingerCount);
	}

}
