package com.meetidentity.usermanagement.web.rest.model.notification;

import java.util.List;

public class DeliverReqBean {

	// org of the user to whom message is send
	private Long orgId;
	private String orgName;
	private String serviceURL;
	// User Id to whom message is intended to
	private Long userId;
	private String userFullName;
	private String userName;
	private String userFirstName;

	private Long templateCategoryId;

	private Long localeId;
	// sync or async : true or false
	boolean isAsync;
	private int retryCnt;
	// Caller will send this id to identify complete transaction
	private String clientId;

	// private ConfigSettingModel conf = null;
	private EmailModel email = null;
	private PortalModel portal = null;
	private SmsModel sms = null;
	// private PushModel push = null;
	private List<TemplateVariablesModel> templateVarList = null;
	private String messageKey;
	private String additionalInfo;

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getMessageKey() {
		return messageKey;
	}

	public void setMessageKey(String messageKey) {
		this.messageKey = messageKey;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public boolean isAsync() {
		return isAsync;
	}

	public void setAsync(boolean isAsync) {
		this.isAsync = isAsync;
	}

	public int getRetryCnt() {
		return retryCnt;
	}

	public void setRetryCnt(int retryCnt) {
		this.retryCnt = retryCnt;
	}

	public EmailModel getEmail() {
		return email;
	}

	public void setEmail(EmailModel email) {
		this.email = email;
	}

	public PortalModel getPortal() {
		return portal;
	}

	public void setPortal(PortalModel portal) {
		this.portal = portal;
	}

	public SmsModel getSms() {
		return sms;
	}

	public void setSms(SmsModel sms) {
		this.sms = sms;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public List<TemplateVariablesModel> getTemplateVarList() {
		return templateVarList;
	}

	public void setTemplateVarList(List<TemplateVariablesModel> templateVarList) {
		this.templateVarList = templateVarList;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	public Long getTemplateCategoryId() {
		return templateCategoryId;
	}

	public void setTemplateCategoryId(Long templateCategoryId) {
		this.templateCategoryId = templateCategoryId;
	}

	public Long getLocaleId() {
		return localeId;
	}

	public void setLocaleId(Long localeId) {
		this.localeId = localeId;
	}

	public String getServiceURL() {
		return serviceURL;
	}

	public void setServiceURL(String serviceURL) {
		this.serviceURL = serviceURL;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

//	public static void main(String[] args) {
//
//		DeliverReqBean bean = new DeliverReqBean();
//		bean.setOrgId(1L);
//		bean.setAsync(false);
//		bean.setClientId("clientid");
//		EmailModel email = new EmailModel();
//		email.setFromAddress("");
//		email.setMessageBody("messageBody");
//		email.setProvider("provider");
//		email.setSubject("subject");
//		List<String> toaddresses = new ArrayList<String>();
//		toaddresses.add("k.reddy@meetidentity.com");
//		email.setToAddress(toaddresses);
//		email.setMessageKey("messageKey");
//		email.setReplyAddress("replyAddress");
//		bean.setEmail(email);
////		bean.setPortal(email);
////		bean.setSms(sms);
//		List<TemplateVariablesModel> templateVars = new ArrayList<>();
//		TemplateVariablesModel tvm = new TemplateVariablesModel();
//		tvm.setKey("$$user$$");
//		tvm.setValue("Username");
//		templateVars.add(tvm);
//		bean.setTemplateVarList(templateVars);
//		bean.setUserId(1L);
//		Gson gson = new Gson();
//		String va = gson.toJson(bean);
//		System.out.println(va);
//	}
}
