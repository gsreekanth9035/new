package com.meetidentity.usermanagement.web.rest.model.notification;

public class TemplateVariablesModel {

	private String key;
	private String value;
	private Boolean pii;

	
	
	public TemplateVariablesModel(String key, String value, Boolean pii) {
		super();
		this.key = key;
		this.value = value;
		this.pii = pii;
	}

	public TemplateVariablesModel() {
		// TODO Auto-generated constructor stub
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Boolean getPii() {
		return pii;
	}

	public void setPii(Boolean pii) {
		this.pii = pii;
	}
}
