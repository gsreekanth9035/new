package com.meetidentity.usermanagement.web.rest.model;

public class DeviceProfileKeyManager {
	private Long id;
	private String diversify;
	private String masterKey;
	private Boolean isAdminKeySameAsMasterKey;
	private String adminKey;
	private String customerMasterKey;
	private String customerAdminKey;
	private String factoryManagementKey;
	private String customerManagementKey;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDiversify() {
		return diversify;
	}

	public void setDiversify(String diversify) {
		this.diversify = diversify;
	}

	public String getMasterKey() {
		return masterKey;
	}

	public void setMasterKey(String masterKey) {
		this.masterKey = masterKey;
	}

	public String getAdminKey() {
		return adminKey;
	}

	public void setAdminKey(String adminKey) {
		this.adminKey = adminKey;
	}

	public Boolean getIsAdminKeySameAsMasterKey() {
		return isAdminKeySameAsMasterKey;
	}

	public void setIsAdminKeySameAsMasterKey(Boolean isAdminKeySameAsMasterKey) {
		this.isAdminKeySameAsMasterKey = isAdminKeySameAsMasterKey;
	}

	public String getCustomerMasterKey() {
		return customerMasterKey;
	}

	public void setCustomerMasterKey(String customerMasterKey) {
		this.customerMasterKey = customerMasterKey;
	}

	public String getFactoryManagementKey() {
		return factoryManagementKey;
	}

	public void setFactoryManagementKey(String factoryManagementKey) {
		this.factoryManagementKey = factoryManagementKey;
	}

	public String getCustomerManagementKey() {
		return customerManagementKey;
	}

	public void setCustomerManagementKey(String customerManagementKey) {
		this.customerManagementKey = customerManagementKey;
	}

	public String getCustomerAdminKey() {
		return customerAdminKey;
	}

	public void setCustomerAdminKey(String customerAdminKey) {
		this.customerAdminKey = customerAdminKey;
	}

	
	
}
