package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.WFStepCertConfigService;
import com.meetidentity.usermanagement.web.rest.model.WFMobileIdStepCertificate;
import com.meetidentity.usermanagement.web.rest.model.WFStepCertConfig;

@RestController
@RequestMapping("/api/v1")
public class WFStepCertConfigResource {
	@Autowired
	private WFStepCertConfigService wFStepCertConfigService;

	@GetMapping("/organizations/{orgName}/workflows/{workflowName}/steps/{stepName}")
	public List<WFStepCertConfig> getWfCerts(@PathVariable("stepName") String stepName,
			@PathVariable("workflowName") String workflowName, @PathVariable("orgName") String orgName) {
		return wFStepCertConfigService.getWorkflowCerts(stepName, workflowName, orgName);
	}
	
	@GetMapping("/workflowCertConfigId/{configId}")
	public WFStepCertConfig getWorkflowCertConfig(@PathVariable("configId") Long configId) {
		return wFStepCertConfigService.getWorkflowCertConfig(configId);
	}
	
	@GetMapping("/workflowMobileIdCertConfigId/{configId}")
	public WFMobileIdStepCertificate getWorkflowMobileCertConfig(@PathVariable("configId") Long configId) {
		return wFStepCertConfigService.getWorkflowMobileCertConfig(configId);
	}
}
