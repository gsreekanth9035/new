package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.FingerprintDeviceConfigService;
import com.meetidentity.usermanagement.web.rest.model.FingerprintDeviceConfig;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/")
public class FingerprintDeviceConfigResource {
	@Autowired
	private FingerprintDeviceConfigService fingerprintDeviceConfigService;
	
	@GetMapping("deviceManufacturer/{deviceManufacturer}/deviceName/{devicename}")
	public ResponseEntity<FingerprintDeviceConfig> getDeviceByDeviceName(@PathVariable("organizationName") String organizationName,@PathVariable("devicename") String deviceName,@PathVariable("deviceManufacturer") String deviceManufacturer){
		return fingerprintDeviceConfigService.getDetailByDeviceName(organizationName,deviceName,deviceManufacturer);
	}
	
	@GetMapping("deviceManufacturer/{deviceManufacturer}")
	public List<FingerprintDeviceConfig> getListOfDeviceByDeviceManufacturer(@PathVariable("organizationName") String organizationName,@PathVariable("deviceManufacturer") String deviceManufacturer){
		return fingerprintDeviceConfigService.getListOfDeviceByDeviceManufacturer(organizationName,deviceManufacturer);
	}
}
