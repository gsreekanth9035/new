package com.meetidentity.usermanagement.web.rest.model;

import java.io.Serializable;
import java.util.List;

public class RegistrationConfigDetails implements Serializable{

	private static final long serialVersionUID = 1L;

	
	List<TypeDescription> ranks ;
	List<TypeDescription> employeeAffiliation ;
	List<TypeDescription> department ;
	List<TypeDescription> employeeColorCode ;
	List<TypeDescription> physicalccEye ;
	List<TypeDescription> physicalccHair ;
	List<TypeDescription> physicalccHeight ;
	List<TypeDescription> gender ;
	List<TypeDescription> bloodType ;
	List<TypeDescription> healthGroupPlan ;
	List<TypeDescription> drivingLicenseEndorsements;
	
	public List<TypeDescription> getRanks() {
		return ranks;
	}
	public void setRanks(List<TypeDescription> ranks) {
		this.ranks = ranks;
	}	
	public List<TypeDescription> getDepartment() {
		return department;
	}
	public void setDepartment(List<TypeDescription> department) {
		this.department = department;
	}
	public List<TypeDescription> getEmployeeAffiliation() {
		return employeeAffiliation;
	}
	public void setEmployeeAffiliation(List<TypeDescription> employeeAffiliation) {
		this.employeeAffiliation = employeeAffiliation;
	}
	public List<TypeDescription> getPhysicalccEye() {
		return physicalccEye;
	}
	public void setPhysicalccEye(List<TypeDescription> physicalccEye) {
		this.physicalccEye = physicalccEye;
	}
	public List<TypeDescription> getPhysicalccHair() {
		return physicalccHair;
	}
	public void setPhysicalccHair(List<TypeDescription> physicalccHair) {
		this.physicalccHair = physicalccHair;
	}
	public List<TypeDescription> getPhysicalccHeight() {
		return physicalccHeight;
	}
	public void setPhysicalccHeight(List<TypeDescription> physicalccHeight) {
		this.physicalccHeight = physicalccHeight;
	}
	public List<TypeDescription> getGender() {
		return gender;
	}
	public void setGender(List<TypeDescription> gender) {
		this.gender = gender;
	}
	public List<TypeDescription> getBloodType() {
		return bloodType;
	}
	public void setBloodType(List<TypeDescription> bloodType) {
		this.bloodType = bloodType;
	}
	public List<TypeDescription> getEmployeeColorCode() {
		return employeeColorCode;
	}
	public void setEmployeeColorCode(List<TypeDescription> employeeColorCode) {
		this.employeeColorCode = employeeColorCode;
	}
	public List<TypeDescription> getHealthGroupPlan() {
		return healthGroupPlan;
	}
	public void setHealthGroupPlan(List<TypeDescription> healthGroupPlan) {
		this.healthGroupPlan = healthGroupPlan;
	}
	public List<TypeDescription> getDrivingLicenseEndorsements() {
		return drivingLicenseEndorsements;
	}
	public void setDrivingLicenseEndorsements(List<TypeDescription> drivingLicenseEndorsements) {
		this.drivingLicenseEndorsements = drivingLicenseEndorsements;
	}

	
}
