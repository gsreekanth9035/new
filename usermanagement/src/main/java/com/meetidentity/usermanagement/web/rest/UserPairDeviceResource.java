package com.meetidentity.usermanagement.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.exception.PairDeviceInfoServiceException;
import com.meetidentity.usermanagement.service.UserPairDeviceInfoService;
import com.meetidentity.usermanagement.web.rest.model.PairMobileDeviceConfig;
@RestController
@RequestMapping("/api/v1/organizations/{organizationName}")
public class UserPairDeviceResource {
	
	@Autowired
	private UserPairDeviceInfoService userPairDeviceInfoService;
	
	
	@GetMapping("/users/{userName}/pairDevice")
	public PairMobileDeviceConfig generatePairDeviceInfo(@PathVariable("organizationName") String organizationName,@PathVariable("userName") String userName)  {
		 try {
			return  userPairDeviceInfoService.generateInfo(organizationName, userName);
		} catch (PairDeviceInfoServiceException e) {
			return new PairMobileDeviceConfig();
		}
	}
	
	@GetMapping("/users/{userName}/pairDevice/send-notification")
	public ResponseEntity<Void> sendPairMobileDeviceNotificationForMIWalletApp(@RequestHeader HttpHeaders httpHeaders,
			@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName,
			@RequestParam(name = "appName") String appName) {
		try {
			return userPairDeviceInfoService.sendPairMobileDeviceNotification(httpHeaders, organizationName, userName,
					appName);
		} catch (PairDeviceInfoServiceException e) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
	}
	
	@GetMapping("/pair-mobile-service")
	public PairMobileDeviceConfig generatePairMobileServiceQRCode(@PathVariable("organizationName") String organizationName) {
		try {
			return userPairDeviceInfoService.generateQRCodeToPairMobileServiceApp(organizationName, null);
		} catch (PairDeviceInfoServiceException e) {
			return new PairMobileDeviceConfig();
		}
	}

}
