package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;

public class UserPersonalizedID {

	private long userId;
	private String uimsId;
	private String idNumber;
	private String identityType;
	private Date dateOfIssuance;
	private Date dateOfExpiry;
	private String issuerIN;
	private String agencyCardSn;

	public UserPersonalizedID(long userId, String uimsId, String idNumber, String identityType, Date dateOfIssuance,
			Date dateOfExpiry, String affiliation, String department, String issuerIN, String agencyCardSn) {
		super();
		this.uimsId = uimsId;
		this.idNumber = idNumber;
		this.identityType = identityType;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
		this.issuerIN = issuerIN;
		this.agencyCardSn = agencyCardSn;
	}

	public UserPersonalizedID() {
		// TODO Auto-generated constructor stub
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}

	public Date getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Date dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIssuerIN() {
		return issuerIN;
	}

	public void setIssuerIN(String issuerIN) {
		this.issuerIN = issuerIN;
	}

	public String getAgencyCardSn() {
		return agencyCardSn;
	}

	public void setAgencyCardSn(String agencyCardSn) {
		this.agencyCardSn = agencyCardSn;
	}

}
