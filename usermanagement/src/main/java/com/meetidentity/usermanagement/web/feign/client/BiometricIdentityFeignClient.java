package com.meetidentity.usermanagement.web.feign.client;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans.FingerprintTemplate;
import com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans.UserBioInfo;
import com.meetidentity.usermanagement.web.rest.model.BiometricVerificationResponse;
@FeignClient(name = "biometricverification", url = "http://biometricverification:10006")
//@FeignClient(name="biometricidentity", url="http://${application.biometricidentity.host}:${application.biometricidentity.port}")
public interface BiometricIdentityFeignClient {
	
	@PostMapping("/api/v1/organizations/{organizationName}/biometricIdentity/enroll")
	public ResponseEntity<BiometricVerificationResponse> enroll(@PathVariable("organizationName") String organizationName, @RequestBody UserBioInfo userBioInfo);
	
	@PostMapping("/api/v1/organizations/{organizationName}/biometricIdentity/fetchDuplicates")
	public ResponseEntity<BiometricVerificationResponse> fetchDuplicates(@PathVariable("organizationName") String organizationName, @RequestBody UserBioInfo userBioInfo);
	
	@PostMapping("/api/v1/organizations/{organizationName}/biometricIdentity/verify")
	public String verifyUserByID(@PathVariable("organizationName") String organizationName, @RequestBody UserBioInfo userBioInfo);
		
	@PostMapping("/api/v1/organizations/{organizationName}/biometricIdentity/delete")
	public ResponseEntity<BiometricVerificationResponse> delete(@PathVariable("organizationName") String organizationName, @RequestBody UserBioInfo userBioInfo);
	
	@PostMapping("/api/v1/organizations/{organizationName}/biometricIdentity/sdkProvider/{sdkProvider}/extractANSI")
	public List<FingerprintTemplate> extractANSIData(@PathVariable("organizationName") String organizationName,@PathVariable("sdkProvider") String sdkProvider,@RequestBody UserBioInfo userBioInfo);	

}
