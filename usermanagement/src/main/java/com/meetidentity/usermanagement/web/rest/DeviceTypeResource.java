package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.DeviceTypeService;
import com.meetidentity.usermanagement.web.rest.model.DeviceType;

@RestController
@RequestMapping("/api/v1/device-types")
public class DeviceTypeResource {

	@Autowired
	private DeviceTypeService deviceTypeResource;

	@GetMapping("/visual-applicable")
	public List<DeviceType> getVisualApplicableDeviceTypes() {
		return deviceTypeResource.getVisualApplicableDeviceTypes();
	}

}
