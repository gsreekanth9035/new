package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;

public class UserHealthServiceVaccineInfo {

	private String uimsId;

	private String vaccine;

	private String route;

	private String site;

	private Date date;

	private String administeredBy;

	public UserHealthServiceVaccineInfo() {

	}

	public UserHealthServiceVaccineInfo(String vaccine, String route, String site, Date date, String administeredBy) {
		super();
		this.vaccine = vaccine;
		this.route = route;
		this.site = site;
		this.date = date;
		this.administeredBy = administeredBy;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public String getVaccine() {
		return vaccine;
	}

	public void setVaccine(String vaccine) {
		this.vaccine = vaccine;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAdministeredBy() {
		return administeredBy;
	}

	public void setAdministeredBy(String administeredBy) {
		this.administeredBy = administeredBy;
	}

}
