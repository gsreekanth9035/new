package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.meetidentity.usermanagement.service.DeviceProfileServcie;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfile;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfileConfig;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/device-profiles")
public class DeviceProfileResource {

	private static final Gson gson = new Gson();

	@Autowired
	private DeviceProfileServcie deviceProfileServcie;

	@GetMapping
	public ResultPage getDeviceProfiles(@PathVariable("organizationName") String organizationName, @RequestParam("sort") String sort, @RequestParam("order") String order,
			@RequestParam("page") int page, @RequestParam("size") int size) {
		return deviceProfileServcie.getDeviceProfilesList(organizationName, sort, order, page, size);
	}
	
	@GetMapping("/list")
	public List<DeviceProfile> getDeviceProfilesList(@PathVariable("organizationName") String organizationName) {
		return deviceProfileServcie.getDeviceProfiles(organizationName);
	}

	@GetMapping("/{deviceProfileID}")
	public DeviceProfile getDeviceProfileById(@PathVariable("organizationName") String organizationName,@PathVariable("deviceProfileID") Long deviceProfileID) {
		return deviceProfileServcie.getDeviceProfileById(deviceProfileID);
	}
	
	@PostMapping
	public ResponseEntity<String> saveDeviceProfile(@PathVariable("organizationName") String organizationName,@RequestBody DeviceProfile devProfile) throws Exception {
		return deviceProfileServcie.saveDeviceProfile(organizationName,devProfile);
	}

	@DeleteMapping("/{devProfileID}")
	public ResponseEntity<Void> deleteDeviceprofile(@PathVariable("organizationName") String organizationName, @PathVariable("devProfileID") Long devProfileID) {
		return deviceProfileServcie.deleteDeviceProfile(organizationName, devProfileID);
	}

	@PutMapping("/{devProfileID}")
	public ResponseEntity<String> updateDeviceProfile(@PathVariable("organizationName") String organizationName,@PathVariable("devProfileID") Long devProfileID,
			@RequestBody DeviceProfile devProfile) throws Exception {
		return deviceProfileServcie.updateDeviceProfile(devProfileID, devProfile);
	}
	
	@GetMapping("/{deviceProfileID}/devProfName")
	public String getDevProfName(@PathVariable("organizationName") String organizationName,@PathVariable("deviceProfileID") Long deviceProfileID) {
		return deviceProfileServcie.getDevProfName(deviceProfileID);
	}
	
	@GetMapping("/{productValueId}/config")
	public DeviceProfileConfig getDeviceProfileConfigs(@PathVariable("organizationName") String organizationName,@PathVariable("productValueId") Long productValueId) {
		return deviceProfileServcie.getDeviceProfileConfigs(productValueId);
	}
}
