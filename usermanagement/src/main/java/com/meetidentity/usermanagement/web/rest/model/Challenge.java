package com.meetidentity.usermanagement.web.rest.model;

public class Challenge {

	private String token;
	private MobileServiceUserInfo mobileServiceUserInfo;
	private String notificationID;
	private String typeOfConsent;
	private String consentValue;
	private String identitySerialNumber;
	private String userName;
	private String organizationName;
	private Long userId;
	private String appName;
	private String operatingSystem;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public MobileServiceUserInfo getMobileServiceUserInfo() {
		return mobileServiceUserInfo;
	}

	public void setMobileServiceUserInfo(MobileServiceUserInfo mobileServiceUserInfo) {
		this.mobileServiceUserInfo = mobileServiceUserInfo;
	}

	public String getNotificationID() {
		return notificationID;
	}

	public void setNotificationID(String notificationID) {
		this.notificationID = notificationID;
	}

	public String getTypeOfConsent() {
		return typeOfConsent;
	}

	public void setTypeOfConsent(String typeOfConsent) {
		this.typeOfConsent = typeOfConsent;
	}

	public String getConsentValue() {
		return consentValue;
	}

	public void setConsentValue(String consentValue) {
		this.consentValue = consentValue;
	}

	public String getIdentitySerialNumber() {
		return identitySerialNumber;
	}

	public void setIdentitySerialNumber(String identitySerialNumber) {
		this.identitySerialNumber = identitySerialNumber;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

}
