package com.meetidentity.usermanagement.web.rest;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.config.ApplicationConstants;
import com.meetidentity.usermanagement.exception.EntityNotFoundException;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.exception.security.CryptoOperationException;
import com.meetidentity.usermanagement.service.UserDeviceService;
import com.meetidentity.usermanagement.service.UserService;
import com.meetidentity.usermanagement.util.Util;
import com.meetidentity.usermanagement.web.rest.model.AuthenticatorClientInfo;
import com.meetidentity.usermanagement.web.rest.model.RoleDeviceAction;
import com.meetidentity.usermanagement.web.rest.model.UserDetails;
import com.meetidentity.usermanagement.web.rest.model.UserDevice;

@RestController
@RequestMapping("/api/v1/devices")
public class UserDeviceResource {
	
	private final Logger log = LoggerFactory.getLogger(UserDeviceResource.class);

	
	@Autowired
	UserDeviceService userDeviceService;
	
	@Autowired
	UserService userService;
	
	@PostMapping
	public ResponseEntity<Void> registerUserDevice(@RequestBody UserDevice userDevice) {
		
		try {
			ResponseEntity<Void> response = userDeviceService.registerUserDevice(userDevice);
		
			if (response.getStatusCode() == HttpStatus.OK) {
				log.debug("registerUserDevice success ");
				return new ResponseEntity<>(HttpStatus.OK);
			} else {
				log.debug("registerUserDevice failure  " + response.toString());
				return new ResponseEntity<>(HttpStatus.CONFLICT);
			}
		}catch (CryptoOperationException | EntityNotFoundException  e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}
	
	@PostMapping(value="/login",consumes = org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE )
	public ResponseEntity<String> userLogin(@RequestBody MultiValueMap<?, ?> formData) {	
		
		return userDeviceService.userKeycloakAuthentication(((LinkedList<String>)formData.get("username")).get(0), ((LinkedList<String>)formData.get("password")).get(0));
	}
	
	@PostMapping("/getUserByUUID")
	public UserDetails getUserDetails(@RequestBody UserDevice userDevice ) {
		try {
			if(Boolean.TRUE.equals(userDevice.getIdentitySerialNumberVerified()) && userDevice.getUserId() != null) {
				return userService.getUserByIDOrEntity(userDevice.getUserId(), null);
			}else {
				if(userDevice.getUuid()!=null) {
					return userService.getUserByIDOrEntity(userService.getUserbyUUID(userDevice.getOrganizationName(),userDevice.getUuid()).getId(),null);
				}else if(userDevice.getMobileNumber()!=null) {
					// If User dont have QR option , He/She can get the credentials by mobile Number... 
					// Fist user should request for OTP
					// Then for the verification this metod will execute
					// User need to pass registered mobile number and OTP 
					
					return userService.getUserByIDOrEntity(userService.verifyOtp(userDevice.getMobileNumber(),userDevice.getOtp()).getId(), null);
				}
			}
		}catch (CryptoOperationException | EntityNotFoundException cause) {
			return null;
		}
		return null;
		
	}
	
	@GetMapping("/mobile-service/organizations/{organizationName}/client-credentials")
	public AuthenticatorClientInfo getOrgOIDCClientCredentials(@PathVariable("organizationName") String organizationName) {
		return  userService.getOrgClientCredentials(organizationName);
		
	}
	
	@PostMapping("/getUserAndClientInfoByUUID")
	public UserDetails getUserAndClientInfoByUUID(@RequestBody UserDevice userDevice ) {
		long userID = 0l;
		try {
			if(userDevice.getUuid()!=null) {
				if(userDevice.getPublicKeyB64()!=null && userDevice.getPublicKeyAlgorithm() != null) {
					userID = userService.getUserbyUUID(userDevice.getOrganizationName(),userDevice.getUuid()).getId();
					
					UserDetails userDetails = userService.getUserByIDOrEntity(userID,null);
					AuthenticatorClientInfo authenticatorClientInfo = userService.getAuthenticatorClientInfo(userDetails.getOrganisationName(), userDevice.getPublicKeyB64(), userDevice.getPublicKeyAlgorithm());
					
					userDetails.setAuthenticatorClientInfo(authenticatorClientInfo);
					return userDetails;
				}
			}else if(userDevice.getMobileNumber()!=null) {
				// If User dont have QR option , He/She can get the credentials by mobile Number... 
				// Fist user should request for OTP
				// Then for the verification this metod will execute
				// User need to pass registered mobile number and OTP 
				
				return userService.getUserByIDOrEntity(userService.verifyOtp(userDevice.getMobileNumber(),userDevice.getOtp()).getId(), null);
			}
		}catch (CryptoOperationException cause) {

			log.error(ApplicationConstants.notifyAdmin, Util.format(ErrorMessages.INVALID_PUBLICKEY, userDevice.getUniqueIdentifier(), userID,userDevice.getOrganizationName()));
			throw new CryptoOperationException(cause, ErrorMessages.INVALID_PUBLICKEY,userDevice.getUniqueIdentifier(), userID,userDevice.getOrganizationName());
		}catch (IdentityBrokerServiceException cause) {
			return null;
		}
		return null;
		
	}
	
	@GetMapping("/organizations/{organizationName}/syncUserDetails/{userID}")
	public UserDetails getSyncUserDetails(@PathVariable("organizationName") String organizationName, @PathVariable("userID") Long userID) {
		try {

			return userService.getUserByIDOrEntity(userID, null);

		}catch (CryptoOperationException | EntityNotFoundException cause) {
			return null;
		}
		
	}
	
	@GetMapping("/organizations/{organizationName}/getUserIDByName/{name}")
	public Long getUserIDByName(@PathVariable("organizationName") String organizationName, @PathVariable("name") String name) {
		if (name != null) {
			return userService.getUserIDByName(organizationName, name);
		}
		return null;
	}
	
	@PostMapping("/sendUserOtp")
	public ResponseEntity<Void> sendUserOtp(@RequestBody UserDevice userDevice ) {
		if(userDevice.getMobileNumber()!=null) {
			return userDeviceService.sendUserOtp(userDevice.getMobileNumber());
		}else {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/organizations/{organizationName}/actionsByRole/{role}")
	public List<RoleDeviceAction> getDeviceActionsByRole(@PathVariable("organizationName") String organizationName, @PathVariable("role") String role) {
		return userDeviceService.getDeviceActionsByRole(organizationName, role);
	}
	
}
