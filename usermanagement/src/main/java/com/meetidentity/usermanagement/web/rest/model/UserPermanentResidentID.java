package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;

public class UserPermanentResidentID {

	private long userId;
	private String uimsId;
	private Date dateOfIssuance;
	private Date dateOfExpiry;
	private String docNumber;
	private String personalCode;
	private String issuingAuthority;
	private String passportNumber;
	private String residentcardTypeNumber;
	private String residentcardTypeCode;

	public UserPermanentResidentID(long userId, String uimsId, Date dateOfIssuance, Date dateOfExpiry, String docNumber,
			String personalCode, String issuingAuthority, String passportNumber, String residentcardTypeNumber,
			String residentcardTypeCode) {
		super();
		this.userId = userId;
		this.uimsId = uimsId;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
		this.docNumber = docNumber;
		this.personalCode = personalCode;
		this.issuingAuthority = issuingAuthority;
		this.passportNumber = passportNumber;
		this.residentcardTypeNumber = residentcardTypeNumber;
		this.residentcardTypeCode = residentcardTypeCode;
	}

	public UserPermanentResidentID() {
		// TODO Auto-generated constructor stub
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public Date getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Date dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getPersonalCode() {
		return personalCode;
	}

	public void setPersonalCode(String personalCode) {
		this.personalCode = personalCode;
	}

	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getResidentcardTypeNumber() {
		return residentcardTypeNumber;
	}

	public void setResidentcardTypeNumber(String residentcardTypeNumber) {
		this.residentcardTypeNumber = residentcardTypeNumber;
	}

	public String getResidentcardTypeCode() {
		return residentcardTypeCode;
	}

	public void setResidentcardTypeCode(String residentcardTypeCode) {
		this.residentcardTypeCode = residentcardTypeCode;
	}

}
