package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.meetidentity.usermanagement.service.VisualTemplateService;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.VisualTemplate;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/visualtemplates")
public class VisualTemplateResource {
	Gson gson = new Gson();
	@Autowired
	private VisualTemplateService visualTemplateService;

	@GetMapping
	public ResultPage getAllVisualTemplates(@PathVariable("organizationName") String organizationName, @RequestParam("sort") String sort, @RequestParam("order") String order,
			@RequestParam("page") int page, @RequestParam("size") int size) {
		return visualTemplateService.getAllVisualTemplates(organizationName, sort, order, page, size);
	}

	@GetMapping("/list")
	public List<VisualTemplate> getAllVisualTemplates(@PathVariable("organizationName") String organizationName) {
		return visualTemplateService.getVisualTemplates(organizationName);
	}

	@PostMapping
	public ResponseEntity<Void> createVisualTemplate(@PathVariable("organizationName") String organizationName, @RequestBody VisualTemplate visualTemplate) throws Exception {
		return visualTemplateService.createVisualTemplate(organizationName, visualTemplate);
	}

	@GetMapping("/{visualTemplateName}")
	public VisualTemplate getVisualTemplateByName(@PathVariable("organizationName") String organizationName, @PathVariable("visualTemplateName") String visualTemplateName ) {
		return visualTemplateService.getVisualTemplateByName(organizationName, visualTemplateName);
	}
	
	@DeleteMapping("/{visualTemplateName}")
	public ResponseEntity<String> deleteVisualTemplateByName(@PathVariable("organizationName") String organizationName,
			@PathVariable("visualTemplateName") String visualTemplateName) {
		return visualTemplateService.deleteVisualTemplateByName(organizationName, visualTemplateName);
	}
	
	@PutMapping("/{visualTemplateName}")
	public ResponseEntity<String> updateVisualTemplate(@PathVariable("organizationName") String organizationName, @PathVariable("visualTemplateName") String visualTemplateName, 
			@RequestBody VisualTemplate visualTemplate) throws Exception {
		return visualTemplateService.updateVisualTemplate(organizationName,visualTemplateName, visualTemplate);
	} 
	
}
