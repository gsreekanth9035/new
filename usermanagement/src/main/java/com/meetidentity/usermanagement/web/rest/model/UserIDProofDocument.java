package com.meetidentity.usermanagement.web.rest.model;

public class UserIDProofDocument {
	private Long id;
	private String name;
	private byte[] frontImg;
	private byte[] backImg;
	private Long idProofTypeId;
	private Long issuingAuthorityId;
	private byte[] file;
	private IDProofIssuingAuthority idProofIssuingAuthority;
	private OrganizationIdentity idProofType;
	private String idProofTypeName;
	private String frontFileType;
	private String backFileType;
	private String frontFileName;
	private String backFileName;

	public UserIDProofDocument() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getFrontImg() {
		return frontImg;
	}

	public void setFrontImg(byte[] frontImg) {
		this.frontImg = frontImg;
	}

	public byte[] getBackImg() {
		return backImg;
	}

	public void setBackImg(byte[] backImg) {
		this.backImg = backImg;
	}

	public Long getIdProofTypeId() {
		return idProofTypeId;
	}

	public void setIdProofTypeId(Long idProofTypeId) {
		this.idProofTypeId = idProofTypeId;
	}

	public Long getIssuingAuthorityId() {
		return issuingAuthorityId;
	}

	public void setIssuingAuthorityId(Long issuingAuthorityId) {
		this.issuingAuthorityId = issuingAuthorityId;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public IDProofIssuingAuthority getIdProofIssuingAuthority() {
		return idProofIssuingAuthority;
	}

	public void setIdProofIssuingAuthority(IDProofIssuingAuthority idProofIssuingAuthority) {
		this.idProofIssuingAuthority = idProofIssuingAuthority;
	}

//	public IDProofType getIdProofType() {
//		return idProofType;
//	}
//
//	public void setIdProofType(IDProofType idProofType) {
//		this.idProofType = idProofType;
//	}
	
	public Long getId() {
		return id;
	}

	public OrganizationIdentity getIdProofType() {
		return idProofType;
	}

	public void setIdProofType(OrganizationIdentity idProofType) {
		this.idProofType = idProofType;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFrontFileType() {
		return frontFileType;
	}

	public void setFrontFileType(String frontFileType) {
		this.frontFileType = frontFileType;
	}

	public String getBackFileType() {
		return backFileType;
	}

	public void setBackFileType(String backFileType) {
		this.backFileType = backFileType;
	}

	public String getFrontFileName() {
		return frontFileName;
	}

	public void setFrontFileName(String frontFileName) {
		this.frontFileName = frontFileName;
	}

	public String getBackFileName() {
		return backFileName;
	}

	public void setBackFileName(String backFileName) {
		this.backFileName = backFileName;
	}

	public String getIdProofTypeName() {
		return idProofTypeName;
	}

	public void setIdProofTypeName(String idProofTypeName) {
		this.idProofTypeName = idProofTypeName;
	}

}
