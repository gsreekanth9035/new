package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;
import java.util.List;

public class UserHealthService {

	private String uimsId;

	private long idNumber;

	private String issuingAuthority;

	private String primarySubscriber;

	private long primarySubscriberID;

	private String primaryDoctor;

	private long primaryDoctorPhone;

	private Date dateOfIssuance;

	private Date dateOfExpiry;

	private List<UserHealthServiceVaccineInfo> userHealthServiceVaccineInfos;

	public UserHealthService() {
		
	}

	public UserHealthService(String uimsId, long idNumber, String issuingAuthority, String primarySubscriber,
			long primarySubscriberID, String primaryDoctor, long primaryDoctorPhone, Date dateOfIssuance,
			Date dateOfExpiry, List<UserHealthServiceVaccineInfo> userHealthServiceVaccineInfos) {
		super();
		this.uimsId = uimsId;
		this.idNumber = idNumber;
		this.issuingAuthority = issuingAuthority;
		this.primarySubscriber = primarySubscriber;
		this.primarySubscriberID = primarySubscriberID;
		this.primaryDoctor = primaryDoctor;
		this.primaryDoctorPhone = primaryDoctorPhone;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
		this.userHealthServiceVaccineInfos = userHealthServiceVaccineInfos;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public long getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(long idNumber) {
		this.idNumber = idNumber;
	}

	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public String getPrimarySubscriber() {
		return primarySubscriber;
	}

	public void setPrimarySubscriber(String primarySubscriber) {
		this.primarySubscriber = primarySubscriber;
	}

	public long getPrimarySubscriberID() {
		return primarySubscriberID;
	}

	public void setPrimarySubscriberID(long primarySubscriberID) {
		this.primarySubscriberID = primarySubscriberID;
	}

	public String getPrimaryDoctor() {
		return primaryDoctor;
	}

	public void setPrimaryDoctor(String primaryDoctor) {
		this.primaryDoctor = primaryDoctor;
	}

	public long getPrimaryDoctorPhone() {
		return primaryDoctorPhone;
	}

	public void setPrimaryDoctorPhone(long primaryDoctorPhone) {
		this.primaryDoctorPhone = primaryDoctorPhone;
	}

	public Date getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Date dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public List<UserHealthServiceVaccineInfo> getUserHealthServiceVaccineInfos() {
		return userHealthServiceVaccineInfos;
	}

	public void setUserHealthServiceVaccineInfos(List<UserHealthServiceVaccineInfo> userHealthServiceVaccineInfos) {
		this.userHealthServiceVaccineInfos = userHealthServiceVaccineInfos;
	}

	
	
}
