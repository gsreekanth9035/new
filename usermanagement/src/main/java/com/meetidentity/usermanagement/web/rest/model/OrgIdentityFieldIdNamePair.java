package com.meetidentity.usermanagement.web.rest.model;

public class OrgIdentityFieldIdNamePair {

	private Long orgIdentityFieldId;
	private String fieldDisplayName;
	private boolean mandatory;
	
	public Long getOrgIdentityFieldId() {
		return orgIdentityFieldId;
	}
	public void setOrgIdentityFieldId(Long orgIdentityFieldId) {
		this.orgIdentityFieldId = orgIdentityFieldId;
	}
	public String getFieldDisplayName() {
		return fieldDisplayName;
	}
	public void setFieldDisplayName(String fieldDisplayName) {
		this.fieldDisplayName = fieldDisplayName;
	}
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	
}
