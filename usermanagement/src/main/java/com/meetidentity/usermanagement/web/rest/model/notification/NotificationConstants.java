package com.meetidentity.usermanagement.web.rest.model.notification;

public interface NotificationConstants {
	String USER_ONBOARD_SUCCESS = "USER_ONBOARD_SUCCESS";
	String INVITE_PAIR_MOBILE_WALLET_FOR_CREDENTIALS = "INVITE_PAIR_MOBILE_WALLET_FOR_CREDENTIALS";
	String ONBOARD_STATUS = "ONBOARD_STATUS";
	String UIMS_URL = "UIMS_URL";
	String USER_TEMP_PWD = "USER_TEMP_PWD";
	String SUCCESS = "SUCCESS";
	String INVITE_ENROLL = "INVITE_ENROLL";
	String INVITE_LINK = "INVITE_LINK";
	String QR_CODE = "qrcodeimage";
	String ORG_IMG = "orgimage";

	Long USER_ONBOARD = 1L;
	
	String PAIR_MOBILE_SERVICES = "PAIR_MOBILE_DEVICE_FOR_MOBILE_SERVICES_APP";
	String GOOGLE_PLAY_STORE_APP_URL = "GOOGLE_PLAY_STORE_APP_URL";
	String GOOGLE_PLAY_STORE_APP_IMG = "googlePlayStoreimage";
	String APPLE_APP_STORE_URL = "APPLE_APP_STORE_URL";
	String APPLE_APP_STORE_IMG = "appleAppStoreimage";
}
