package com.meetidentity.usermanagement.web.rest.model;

public class DeviceProfileConfig {
	private Long id;
	private Long configValueId;
	private Boolean enableKeyManager;
	private Boolean enableDiversify;
	private Boolean enableMasterKey;
	private Boolean enableAkIsIdenticalToMkCheck;
	private Boolean enableAdminKey;

	private Boolean enableCustomerMasterKey;
	private Boolean enableCustomerAdminKey;

	private Boolean enableFactoryManagementKey;
	private Boolean enableCustomerManagementKey;

	private int keyLength;
	private Integer masterKeyLength;
	private Boolean isMobileId;
	private Boolean isPlasticId;
	private Boolean isRfidCard;
	private Boolean isAppletLoadingRequired;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getConfigValueId() {
		return configValueId;
	}

	public void setConfigValueId(Long configValueId) {
		this.configValueId = configValueId;
	}

	public Boolean getEnableKeyManager() {
		return enableKeyManager;
	}

	public void setEnableKeyManager(Boolean enableKeyManager) {
		this.enableKeyManager = enableKeyManager;
	}

	public Boolean getEnableDiversify() {
		return enableDiversify;
	}

	public void setEnableDiversify(Boolean enableDiversify) {
		this.enableDiversify = enableDiversify;
	}

	public Boolean getEnableMasterKey() {
		return enableMasterKey;
	}

	public void setEnableMasterKey(Boolean enableMasterKey) {
		this.enableMasterKey = enableMasterKey;
	}

	public Boolean getEnableAkIsIdenticalToMkCheck() {
		return enableAkIsIdenticalToMkCheck;
	}

	public void setEnableAkIsIdenticalToMkCheck(Boolean enableAkIsIdenticalToMkCheck) {
		this.enableAkIsIdenticalToMkCheck = enableAkIsIdenticalToMkCheck;
	}

	public Boolean getEnableAdminKey() {
		return enableAdminKey;
	}

	public void setEnableAdminKey(Boolean enableAdminKey) {
		this.enableAdminKey = enableAdminKey;
	}

	public Boolean getEnableCustomerMasterKey() {
		return enableCustomerMasterKey;
	}

	public void setEnableCustomerMasterKey(Boolean enableCustomerMasterKey) {
		this.enableCustomerMasterKey = enableCustomerMasterKey;
	}
	
	public Boolean getEnableCustomerAdminKey() {
		return enableCustomerAdminKey;
	}

	public void setEnableCustomerAdminKey(Boolean enableCustomerAdminKey) {
		this.enableCustomerAdminKey = enableCustomerAdminKey;
	}

	public Boolean getIsMobileId() {
		return isMobileId;
	}

	public void setIsMobileId(Boolean isMobileId) {
		this.isMobileId = isMobileId;
	}

	public Boolean getIsPlasticId() {
		return isPlasticId;
	}

	public void setIsPlasticId(Boolean isPlasticId) {
		this.isPlasticId = isPlasticId;
	}

	public int getKeyLength() {
		return keyLength;
	}

	public void setKeyLength(int keyLength) {
		this.keyLength = keyLength;
	}
	
	public Boolean getEnableFactoryManagementKey() {
		return enableFactoryManagementKey;
	}

	public void setEnableFactoryManagementKey(Boolean enableFactoryManagementKey) {
		this.enableFactoryManagementKey = enableFactoryManagementKey;
	}

	public Boolean getEnableCustomerManagementKey() {
		return enableCustomerManagementKey;
	}

	public void setEnableCustomerManagementKey(Boolean enableCustomerManagementKey) {
		this.enableCustomerManagementKey = enableCustomerManagementKey;
	}

	public Integer getMasterKeyLength() {
		return masterKeyLength;
	}

	public void setMasterKeyLength(Integer masterKeyLength) {
		this.masterKeyLength = masterKeyLength;
	}

	public Boolean getIsRfidCard() {
		return isRfidCard;
	}

	public void setIsRfidCard(Boolean isRfidCard) {
		this.isRfidCard = isRfidCard;
	}

	public Boolean getIsAppletLoadingRequired() {
		return isAppletLoadingRequired;
	}

	public void setIsAppletLoadingRequired(Boolean isAppletLoadingRequired) {
		this.isAppletLoadingRequired = isAppletLoadingRequired;
	}

}
