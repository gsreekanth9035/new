package com.meetidentity.usermanagement.web.rest.model;

public class OrganizationAdvertisementInfo {
	private Long id;

	private String uimsId;

	private String organizationName;

	private String advertisementName;

	private String advertisementImageString;

	private String advertisementText;

	private String advertisementHyperLink;

	private String advertisementCondition;

	private boolean status;

	public OrganizationAdvertisementInfo() {

	}

	public OrganizationAdvertisementInfo(String uimsId, String organizationName, String advertisementName,
			String advertisementImageString, String advertisementText, String advertisementHyperLink, String advertisementCondition) {
		super();
		this.uimsId = uimsId;
		this.organizationName = organizationName;
		this.advertisementName = advertisementName;
		this.advertisementImageString = advertisementImageString;
		this.advertisementText = advertisementText;
		this.advertisementHyperLink = advertisementHyperLink;
		this.advertisementCondition = advertisementCondition;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getAdvertisementName() {
		return advertisementName;
	}

	public void setAdvertisementName(String advertisementName) {
		this.advertisementName = advertisementName;
	}

	public String getAdvertisementImageString() {
		return advertisementImageString;
	}

	public void setAdvertisementImageString(String advertisementImageString) {
		this.advertisementImageString = advertisementImageString;
	}

	public String getAdvertisementText() {
		return advertisementText;
	}

	public void setAdvertisementText(String advertisementText) {
		this.advertisementText = advertisementText;
	}

	public String getAdvertisementHyperLink() {
		return advertisementHyperLink;
	}

	public void setAdvertisementHyperLink(String advertisementHyperLink) {
		this.advertisementHyperLink = advertisementHyperLink;
	}

	public String getAdvertisementCondition() {
		return advertisementCondition;
	}

	public void setAdvertisementCondition(String advertisementCondition) {
		this.advertisementCondition = advertisementCondition;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
