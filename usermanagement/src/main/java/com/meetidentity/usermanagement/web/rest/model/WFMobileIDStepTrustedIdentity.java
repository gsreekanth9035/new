package com.meetidentity.usermanagement.web.rest.model;

public class WFMobileIDStepTrustedIdentity {

	private Long id;

	private WorkflowStep wfStep;

	private OrganizationIdentity organizationIdentity;

	private int version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public WorkflowStep getWfStep() {
		return wfStep;
	}

	public void setWfStep(WorkflowStep wfStep) {
		this.wfStep = wfStep;
	}

	public OrganizationIdentity getOrganizationIdentity() {
		return organizationIdentity;
	}

	public void setOrganizationIdentity(OrganizationIdentity organizationIdentity) {
		this.organizationIdentity = organizationIdentity;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
