package com.meetidentity.usermanagement.web.rest.model;

import java.util.List;

public class SearchUserResult {

	private Long userID;
	private String sub;
	private String userName;
	private String firstName;
	private String lastName;
	private String email;
	private boolean canEnroll;
	private boolean canIssue;
	private boolean canLifeCycle;
	private boolean canDisplayEnrollDetails;
	private boolean canApprove;
	private boolean mobileIDUser;
	private boolean admin;
	private boolean operator;
	private boolean user;
	private boolean adjudicator;
	private String groupId;
	private String groupName;
	private List<Role> roles;
	private List<String> roleIDs;
	private String identityTypeName;
	private String workflowName;
	private boolean enabled;
	private String location;
	private String date;
	private int status;
	private String userStatus;
	private String onboardSelfService;
	private Boolean isFederated;
	private Boolean isEnrollmentInProgress;
	

	public SearchUserResult() {

	}

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isCanEnroll() {
		return canEnroll;
	}

	public void setCanEnroll(boolean canEnroll) {
		this.canEnroll = canEnroll;
	}

	public boolean isCanIssue() {
		return canIssue;
	}

	public void setCanIssue(boolean canIssue) {
		this.canIssue = canIssue;
	}

	public boolean isCanLifeCycle() {
		return canLifeCycle;
	}

	public void setCanLifeCycle(boolean canLifeCycle) {
		this.canLifeCycle = canLifeCycle;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isCanDisplayEnrollDetails() {
		return canDisplayEnrollDetails;
	}

	public void setCanDisplayEnrollDetails(boolean canDisplayEnrollDetails) {
		this.canDisplayEnrollDetails = canDisplayEnrollDetails;
	}

	public String getLocation() {
		return location;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public boolean isCanApprove() {
		return canApprove;
	}

	public void setCanApprove(boolean canApprove) {
		this.canApprove = canApprove;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getIdentityTypeName() {
		return identityTypeName;
	}

	public void setIdentityTypeName(String identityTypeName) {
		this.identityTypeName = identityTypeName;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isOperator() {
		return operator;
	}

	public void setOperator(boolean operator) {
		this.operator = operator;
	}

	public boolean isUser() {
		return user;
	}

	public void setUser(boolean user) {
		this.user = user;
	}

	public boolean isAdjudicator() {
		return adjudicator;
	}

	public void setAdjudicator(boolean adjudicator) {
		this.adjudicator = adjudicator;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public boolean isMobileIDUser() {
		return mobileIDUser;
	}

	public void setMobileIDUser(boolean mobileIDUser) {
		this.mobileIDUser = mobileIDUser;
	}

	public String getOnboardSelfService() {
		return onboardSelfService;
	}

	public void setOnboardSelfService(String onboardSelfService) {
		this.onboardSelfService = onboardSelfService;
	}

	public Boolean getIsFederated() {
		return isFederated;
	}

	public void setIsFederated(Boolean isFederated) {
		this.isFederated = isFederated;
	}

	public Boolean getIsEnrollmentInProgress() {
		return isEnrollmentInProgress;
	}

	public void setIsEnrollmentInProgress(Boolean isEnrollmentInProgress) {
		this.isEnrollmentInProgress = isEnrollmentInProgress;
	}

	public List<String> getRoleIDs() {
		return roleIDs;
	}

	public void setRoleIDs(List<String> roleIDs) {
		this.roleIDs = roleIDs;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

}
