package com.meetidentity.usermanagement.web.rest.model;

public class SequenceNumberModel {
	
	
	private long currentValue;
	
	private String seqPrefix;
	
	private String seqSuffix;
	
	private int idLength;

	public long getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(long currentValue) {
		this.currentValue = currentValue;
	}

	public String getSeqPrefix() {
		return seqPrefix;
	}

	public void setSeqPrefix(String seqPrefix) {
		this.seqPrefix = seqPrefix;
	}

	public String getSeqSuffix() {
		return seqSuffix;
	}

	public void setSeqSuffix(String seqSuffix) {
		this.seqSuffix = seqSuffix;
	}

	public int getIdLength() {
		return idLength;
	}

	public void setIdLength(int idLength) {
		this.idLength = idLength;
	}
}
