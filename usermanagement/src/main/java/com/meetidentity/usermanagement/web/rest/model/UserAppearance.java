package com.meetidentity.usermanagement.web.rest.model;

public class UserAppearance {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String eyeColor;

	private String height;

	private String weight;

	private String hairColor;

	public UserAppearance(String eyeColor, String height, String weight, String hairColor) {
		super();
		this.eyeColor = eyeColor;
		this.height = height;
		this.weight = weight;
		this.hairColor = hairColor;
	}

	public UserAppearance() {
		// TODO Auto-generated constructor stub
	}

	public String getEyeColor() {
		return eyeColor;
	}

	public void setEyeColor(String eyeColor) {
		this.eyeColor = eyeColor;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getHairColor() {
		return hairColor;
	}

	public void setHairColor(String hairColor) {
		this.hairColor = hairColor;
	}

}
