package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;

public class VehicleRegistration {
	// Vehicle Info
	private String vin;
	private String make;
	private Integer year;
	private String model;
	private String color;
	private String licensePlateNumber;
	private Integer capacity;
	// @JsonFormat(pattern="yyyy-MM-dd")
	private Date dateOfSale;
	private String dealerName;
	private String dealerLicenseNumber;
	private String dealerInvoiceNumber;
	
	// Owner
	private String ownerName;
	private String ownerAddress;
	
	// Insurance
	private String policyNumber;
	private String insuranceCompanyName;
	private String insuranceAgentName;
	
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getLicensePlateNumber() {
		return licensePlateNumber;
	}
	public void setLicensePlateNumber(String licensePlateNumber) {
		this.licensePlateNumber = licensePlateNumber;
	}
	public Integer getCapacity() {
		return capacity;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	public Date getDateOfSale() {
		return dateOfSale;
	}
	public void setDateOfSale(Date dateOfSale) {
		this.dateOfSale = dateOfSale;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getDealerLicenseNumber() {
		return dealerLicenseNumber;
	}
	public void setDealerLicenseNumber(String dealerLicenseNumber) {
		this.dealerLicenseNumber = dealerLicenseNumber;
	}
	public String getDealerInvoiceNumber() {
		return dealerInvoiceNumber;
	}
	public void setDealerInvoiceNumber(String dealerInvoiceNumber) {
		this.dealerInvoiceNumber = dealerInvoiceNumber;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getOwnerAddress() {
		return ownerAddress;
	}
	public void setOwnerAddress(String ownerAddress) {
		this.ownerAddress = ownerAddress;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}
	public void setInsuranceCompanyName(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}
	public String getInsuranceAgentName() {
		return insuranceAgentName;
	}
	public void setInsuranceAgentName(String insuranceAgentName) {
		this.insuranceAgentName = insuranceAgentName;
	}
	
}
