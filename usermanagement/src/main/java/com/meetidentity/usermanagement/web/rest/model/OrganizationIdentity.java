package com.meetidentity.usermanagement.web.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;

public class OrganizationIdentity {
	private Long id;
	private String identityTypeName;
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String identityTypeDetails;
	private String fileFormats;

	public OrganizationIdentity() {
	}
	
	public OrganizationIdentity(Long id, String identityTypeName, String identityTypeDetails) {
		super();
		this.id = id;
		this.identityTypeName = identityTypeName;
		this.identityTypeDetails = identityTypeDetails;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdentityTypeName() {
		return identityTypeName;
	}

	public void setIdentityTypeName(String identityTypeName) {
		this.identityTypeName = identityTypeName;
	}

	public String getIdentityTypeDetails() {
		return identityTypeDetails;
	}

	public void setIdentityTypeDetails(String identityTypeDetails) {
		this.identityTypeDetails = identityTypeDetails;
	}

	public String getFileFormats() {
		return fileFormats;
	}

	public void setFileFormats(String fileFormats) {
		this.fileFormats = fileFormats;
	}

}
