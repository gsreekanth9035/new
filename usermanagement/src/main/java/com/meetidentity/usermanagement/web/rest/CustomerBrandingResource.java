package com.meetidentity.usermanagement.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.CustomerBrandingService;
import com.meetidentity.usermanagement.web.rest.model.CustomerBranding;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}")
public class CustomerBrandingResource {
	
	@Autowired
	CustomerBrandingService customerBrandingService;
	
	@GetMapping("/brandingInfo")
	public CustomerBranding getCustomerBranding(@PathVariable("organizationName") String organizationName){
		return customerBrandingService.getCustomerBranding(organizationName);
	}
}
