package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;

public class UserStudentID {

	private long userId;
	private String uimsId;
	private String studentIDNumber;
	private Date dateOfIssuance;
	private Date dateOfExpiry;
	private String institution;
	private String department;
	private String issuerIN;
	private String agencyCardSN;

	public UserStudentID(long userId, String uimsId, String studentIDNumber, Date dateOfIssuance, Date dateOfExpiry,
			String institution, String department, String issuerIN, String agencyCardSN) {
		super();
		this.uimsId = uimsId;
		this.studentIDNumber = studentIDNumber;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
		this.institution = institution;
		this.department = department;
		this.issuerIN = issuerIN;
		this.agencyCardSN = agencyCardSN;
	}

	public UserStudentID() {
		// TODO Auto-generated constructor stub
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public String getStudentIDNumber() {
		return studentIDNumber;
	}

	public void setStudentIDNumber(String studentIDNumber) {
		this.studentIDNumber = studentIDNumber;
	}

	public Date getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Date dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getIssuerIN() {
		return issuerIN;
	}

	public void setIssuerIN(String issuerIN) {
		this.issuerIN = issuerIN;
	}

	public String getAgencyCardSN() {
		return agencyCardSN;
	}

	public void setAgencyCardSN(String agencyCardSN) {
		this.agencyCardSN = agencyCardSN;
	}

}
