package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.UserCredentialsService;
import com.meetidentity.usermanagement.web.rest.model.UCard;
import com.meetidentity.usermanagement.web.rest.model.UserDevice;
import com.meetidentity.usermanagement.web.rest.model.WFStepVisualTemplateConfig;

@RestController
@RequestMapping("/api/v1/{requestFrom}/users/{userID}/credentials")
public class UserCredentialsResource {
	private final Logger log = LoggerFactory.getLogger(UserCredentialsResource.class);

	@Autowired
	private UserCredentialsService userCredentialsService;
	
	@PostMapping("/{credentialTemplateID}")
	public UCard  getUserCredentialsWithTemplateID(@PathVariable("requestFrom") String requestFrom, @PathVariable("userID") Long userID, @PathVariable("credentialTemplateID") String credentialTemplateID, @RequestBody UserDevice userDevice) {
		return userCredentialsService.getUserCredentialsWithTemplateID(requestFrom, userID, credentialTemplateID, userDevice.getUserData());
	}
	
	@GetMapping("/configuredCredentialTemplates")
	public List<WFStepVisualTemplateConfig> getUserIssuedCredentialsNames(@PathVariable("requestFrom") String requestFrom, @PathVariable("userID") Long userID) {
		return userCredentialsService.getConfiguredCredentialTypes(requestFrom, userID);
	}
	
}
