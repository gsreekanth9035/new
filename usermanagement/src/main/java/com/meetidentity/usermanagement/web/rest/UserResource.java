package com.meetidentity.usermanagement.web.rest;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.Response;

import org.hibernate.TransactionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meetidentity.usermanagement.domain.express.enroll.ExpressEnrollmentSchema;
import com.meetidentity.usermanagement.exception.EnrollmentServiceException;
import com.meetidentity.usermanagement.exception.IdentityBrokerServiceException;
import com.meetidentity.usermanagement.exception.PeopleSearchException;
import com.meetidentity.usermanagement.keycloak.domain.ApprovalAttributes;
import com.meetidentity.usermanagement.keycloak.domain.UserStats;
import com.meetidentity.usermanagement.service.UserCredentialsService;
import com.meetidentity.usermanagement.service.UserPairDeviceInfoService;
import com.meetidentity.usermanagement.service.UserService;
import com.meetidentity.usermanagement.thirdparty.libraries.neuro.bioBeans.UserBioInfo;
import com.meetidentity.usermanagement.web.rest.model.EnableSoftOTP2FA;
import com.meetidentity.usermanagement.web.rest.model.EnrollmentStepsStatus;
import com.meetidentity.usermanagement.web.rest.model.ExpressEnrollForm;
import com.meetidentity.usermanagement.web.rest.model.ExpressEnrollmentResponse;
import com.meetidentity.usermanagement.web.rest.model.MobileCredential;
import com.meetidentity.usermanagement.web.rest.model.PairMobileDeviceConfig;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.SearchUserResult;
import com.meetidentity.usermanagement.web.rest.model.UCard;
import com.meetidentity.usermanagement.web.rest.model.User;
import com.meetidentity.usermanagement.web.rest.model.UserAvailability;
import com.meetidentity.usermanagement.web.rest.model.UserDetails;
import com.meetidentity.usermanagement.web.rest.model.UserDevice;
import com.meetidentity.usermanagement.web.rest.model.UserIdNamePair;
import com.meetidentity.usermanagement.web.rest.model.UserIssuanceStatus;
import com.meetidentity.usermanagement.web.rest.model.UserRequest;
import com.meetidentity.usermanagement.web.rest.model.WFStepVisualTemplateConfig;
import com.meetidentity.usermanagement.web.rest.model.WfMobileIDStepDetails;
import com.meetidentity.usermanagement.web.rest.model.Workflow;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/users")
public class UserResource {
	
	private static int enablePush = 0;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserCredentialsService userCredentialsService;
	
	@Autowired
	private UserPairDeviceInfoService userPairDeviceInfoService;
	
	@PostMapping
	public ResponseEntity<String> registerUser(@PathVariable("organizationName") String organizationName,
			@RequestBody User user) throws Exception {
		userService.registerUser(organizationName, user);
		/*try {
			userService.registerUser(organizationName, user);
		} catch (EnrollmentServiceException enrollmentException) {
			// TODO Auto-generated catch block
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}*/
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@PostMapping("/register/via-mobile-device")
	public ResponseEntity<String> enrollMobileSeviceUser(@PathVariable("organizationName") String organizationName,
			@RequestBody User user) throws Exception {
		userService.registerMobileDeviceUser(organizationName, user);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@PostMapping("/register")
	public ResponseEntity<User> registerLoggedInUser(@PathVariable("organizationName") String organizationName,
			@RequestBody User user) throws Exception {
		return userService.registerLoggedInUser(organizationName, user);
	}
	
	@PostMapping("/saveIDProofDocs")
	public ResponseEntity<HttpStatus>  saveIDProofDocs(@RequestHeader HttpHeaders httpHeaders, @PathVariable("organizationName") String organizationName,
			@RequestBody User user) throws Exception {
		try {
			userService.saveIDProofDocs(httpHeaders, organizationName, user);
		} catch (EnrollmentServiceException enrollmentException) {
			// TODO Auto-generated catch block
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping("/saveRegistration")
	public ResponseEntity<HttpStatus>  saveRegistration(@RequestHeader HttpHeaders httpHeaders, @PathVariable("organizationName") String organizationName,
			@RequestBody User user) throws Exception {
		try {
			userService.saveRegistration(httpHeaders, organizationName, user);
		} catch (EnrollmentServiceException enrollmentException) {
			// TODO Auto-generated catch block
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping("/saveUserBiometrics/{biometricType}")
	public ResponseEntity<HttpStatus>  saveUserBiometrics(@RequestHeader HttpHeaders httpHeaders, @PathVariable("organizationName") String organizationName, @PathVariable("biometricType") String biometricType,
			@RequestBody User user) throws Exception {
		try {
			userService.saveUserBiometrics(httpHeaders, organizationName, user, biometricType);
		} catch (EnrollmentServiceException enrollmentException) {
			// TODO Auto-generated catch block
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}	
	
	@PostMapping("/completeEnrollmentInProgress")
	public ResponseEntity<HttpStatus>  completeEnrollmentInProgress(@RequestHeader HttpHeaders httpHeaders, @PathVariable("organizationName") String organizationName,
			@RequestBody User user) throws Exception {
		try {
			userService.completeEnrollmentInProgress(httpHeaders, null, organizationName, user, true);
		} catch (EnrollmentServiceException enrollmentException) {
			// TODO Auto-generated catch block
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}	

	@GetMapping("/{userName}/enrollmentStepsStatus")
	public EnrollmentStepsStatus  enrollmentStepsStatus(@PathVariable("organizationName") String organizationName,
			@PathVariable("userName") String userName) throws Exception {

			return userService.enrollmentStepsStatus(organizationName, userName);
	}	
	
	@PostMapping("/enroll-qrcode")
	public PairMobileDeviceConfig  enrollmentQRCode(@PathVariable("organizationName") String organizationName, @RequestBody  Map<String,String> inputparam) throws Exception {
		
		return userPairDeviceInfoService.generateUserIdentifierAsEnrollmentToken(inputparam.get("emailid"),organizationName);
	}	
	
	@PutMapping
	public ResponseEntity<HttpStatus> updateUser(@PathVariable("organizationName") String organizationName,
			@RequestBody User user) {
		
		try {
			userService.updateUser(organizationName, user);
		} catch (EnrollmentServiceException enrollmentException) {
			// TODO Auto-generated catch block
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping
	public ResultPage getUsersBySearchCriteria(@PathVariable("organizationName") String organizationName,
			@RequestParam("searchCriteria") String searchCriteria, @RequestParam("page") int page,
			@RequestParam("size") int size) {

		ResultPage restultpage = null;
		try {
			restultpage = userService.getUsersBySearchCriteria(organizationName, searchCriteria, page, size);
		} catch (PeopleSearchException peopleSearchException) {
			// TODO Auto-generated catch block
		}
		return restultpage;
	}

	@PostMapping("/usersByFilters")
	public ResultPage getUsersByFilters(@PathVariable("organizationName") String organizationName,
			@RequestBody UserRequest userRequest, @RequestParam("page") int page, @RequestParam("size") int size) {

		ResultPage restultpage = null;
		try {
			restultpage = userService.getUsersByFilters(organizationName, userRequest, page, size);
		} catch (PeopleSearchException peopleSearchException) {
			// TODO throw exception
		}
		return restultpage;
	}
	
//	@GetMapping("/usersByFilters")
//	public ResultPage getUsersByFilters(@PathVariable("organizationName") String organizationName,
//			@RequestParam("userName") String userName, @RequestParam("status") String status,
//			@RequestParam("startDate") Instant startDate, @RequestParam("endDate") Instant endDate,
//			@RequestParam("page") int page, @RequestParam("size") int size) {
//
//		ResultPage restultpage = null;
//		try {
//			restultpage = userService.getUsersByFilters(organizationName, userName, status, startDate, endDate, page,
//					size);
//		} catch (PeopleSearchException peopleSearchException) {
//			// TODO throw exception
//		}
//		return restultpage;
//	}

	@GetMapping("/searchByLocation")
	public ResultPage getUsersByLocation(@PathVariable("organizationName") String organizationName,
			@RequestParam("searchCriteria") String searchCriteria, @RequestParam("page") int page,
			@RequestParam("size") int size) {

		ResultPage restultpage = null;
		try {
			restultpage = userService.getUsersByLocation(organizationName, searchCriteria, page, size);
		} catch (PeopleSearchException peopleSearchException) {
			// TODO Auto-generated catch block
		}
		return restultpage;
	}
	
	
	@PostMapping("/enablePush/{status}")
	public ResponseEntity<HttpStatus> enablePush(@PathVariable("status") int status) {

		try {
			
				enablePush = status;
			System.out.println(enablePush);

		} catch (PeopleSearchException peopleSearchException) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	

	@PostMapping("/updateUsersLocationStatus/{status}")
	public ResponseEntity<HttpStatus> updateUsersLocationStatus(@PathVariable("organizationName") String organizationName, @PathVariable("status") int status, @RequestBody List<Long> markedUsers) {

		try {
			 userService.updateUsersLocationStatus(organizationName, status, markedUsers, enablePush);

		} catch (PeopleSearchException peopleSearchException) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping("/groups")
	public ResultPage getMembersOfAGroup(@PathVariable("organizationName") String organizationName,
			@RequestParam("groupID") String groupID, @RequestParam("page") int page,
			@RequestParam("size") int size) {

		ResultPage restultpage = null;
		try {
			restultpage = userService.getMembersOfAGroup(organizationName, groupID, page, size);
		} catch (PeopleSearchException peopleSearchException) {
			// TODO Auto-generated catch block
		}
		return restultpage;
	}
	@GetMapping("/{userName}/user-details")
	public UserDetails getUserDetailsByUserName(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		UserDetails userDetails = userService.getUserDetailsByName(organizationName, userName);
		
		return userDetails;
	}

	@GetMapping("/{userName}")
	public User getUserByName(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		return userService.getUserByName(organizationName, userName);
	}
	
	@GetMapping("/{userName}/validateUserSession")
	public UserDevice validateUserSession(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		return userService.validateUserSession(organizationName, userName);
	}
	
	@GetMapping("/{userName}/uniqueuid")
	public UserDevice getUUID(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		return userService.getUUID(organizationName, userName);
	}
	
	@GetMapping("/{userName}/issuanceStatus")
	public UserIssuanceStatus getIssuanceStatus(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		UserIssuanceStatus  response =  userService.getIssuanceStatus(organizationName, userName);
		return response;
	}
	
	@PutMapping("/{userName}/approveEnrollment")
	public ResponseEntity<Void> approveEnrollment(@RequestHeader HttpHeaders httpHeaders, @PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName,
			@RequestBody ApprovalAttributes approveAttr) {
		return userService.approveEnrollment(httpHeaders, organizationName, userName, approveAttr);
	}
	
	@PutMapping("/{userName}/rejectEnrollment")
	public ResponseEntity<Void> rejectEnrollment(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName,
			@RequestBody ApprovalAttributes approveAttr) {
		return userService.rejectEnrollment(organizationName, userName, approveAttr);
	}

	@GetMapping("/{userName}/details")
	public User getAllUserDetailsByName(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		return userService.getAllUserDetails(organizationName, userName);
	}
	
	@DeleteMapping("/{userName}")
	public ResponseEntity<HttpStatus> deleteUserByName(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName,
			@RequestParam(name = "isFederated", required = false) boolean isFederated) {
		return userService.deleteUserByName(organizationName, userName, isFederated);
	}
	
	@GetMapping("/{userName}/workflow")
	public String getWorkflowByUser(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		return userService.getWorkflowByUserName(organizationName, userName).getName();
	}
	
	@GetMapping("/{userName}/workflowGeneralDetails")
	public Workflow getWorkflowGeneralDetailsByUserName(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		return userService.getWorkflowGeneralDetailsByUserName(organizationName, userName);
	}

	@GetMapping("/{userName}/workflow-credential-templates")
	public List<WFStepVisualTemplateConfig> getWorkflowCredentialTemplates(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		return userService.getWorkflowVisualTemplates(organizationName, userName);
	}
	
	@PostMapping("/{userName}/visual-credentials/{credentialTemplateID}")
	public UCard getVisualCredentialByTemplateID(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName, 
			@PathVariable("credentialTemplateID") String credentialTemplateID, @RequestBody User user, @RequestParam(name = "transparentPhoto", required = false) boolean transparentPhoto) {
		
		return userCredentialsService.generateVisualCredential("web_portal_enroll_summary", null, userName, organizationName, credentialTemplateID, user, transparentPhoto);
	}
		
	@PostMapping("/{userName}/visual-credentials-on-summary/{credentialTemplateID}")
	public UCard getVisualCredentialByTemplateIDOnSummaryPage(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName, 
			@PathVariable("credentialTemplateID") String credentialTemplateID, @RequestBody User user, @RequestParam(name = "transparentPhoto", required = false) boolean transparentPhoto) {
		return userCredentialsService.generateVisualCredential("web_portal_summary", null, userName, organizationName, credentialTemplateID, user, transparentPhoto);
	}
	
	@GetMapping("/userIdNames")
	public List<UserIdNamePair> getUserIdNames(@PathVariable("organizationName") String organizationName) {
		return userService.getUserIdNames(organizationName);
	}
	
	@PutMapping("/{userID}")
	public ResponseEntity<HttpStatus> updateUserStatusAfterIssuance(@PathVariable("organizationName") String organizationName, @PathVariable("userID") Long userID,
			 @RequestBody User user){
		userService.updateUserStatusAfterIssuance(userID, user);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@PutMapping("/{userName}/resetMobileOTPCredential")
	public ResponseEntity<HttpStatus> resetMobileOTPCredential(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName){
		userService.resetMobileOTPCredential(organizationName, userName);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}	
	
	
	@PutMapping("/updateUserStatus/toReissuance")
	public ResponseEntity<HttpStatus> updateUserStatusToReissuance(@PathVariable("organizationName") String organizationName, @RequestBody Set<Long>  userIDs){
		userService.updateUserStatustoReissuance(organizationName, userIDs);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}
	
	@PutMapping("/updateUserStatusAfterDeleteCred")
	public ResponseEntity<HttpStatus> updateUserStatusAfterDeleteCred(@PathVariable("organizationName") String organizationName, @RequestBody Map<Long,String>  userStatus){
		userService.updateUserStatusAfterDeleteCred(organizationName, userStatus);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/groups/{groupID}/pendingEnrollmentStats")
	public ResultPage pendingEnrollmentStat(@PathVariable("organizationName") String organizationName, @PathVariable("groupID") String groupID,
			@RequestParam("sort") String sort, @RequestParam("order") String order, @RequestParam("page") int page,
			@RequestParam("size") int size, @RequestParam(name = "startDate", required = false) String startDate,
			@RequestParam(name = "endDate", required = false) String endDate) throws ParseException {
		Date createdDateStart;
		Date createdDateEnd;
		if (startDate.equals("null") || startDate == "") {
			createdDateStart = null;
		} else {
			createdDateStart = new SimpleDateFormat("dd/MM/yyyy").parse(startDate);
		}
		if (endDate.equals("null") || endDate == "") {
			createdDateEnd = null;
		} else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new SimpleDateFormat("dd/MM/yyyy").parse(endDate));
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			calendar.set(Calendar.MILLISECOND, 999);
			createdDateEnd = calendar.getTime();
		}
		return userService.pendingEnrollmentStat(organizationName, groupID, sort, order, page, size, createdDateStart, createdDateEnd);
	}
	
	
	@GetMapping("/groups/{groupID}/pendingIssuanceStats")
	public ResultPage pendingIssuanceStat(@PathVariable("organizationName") String organizationName, @PathVariable("groupID") String groupID, @RequestParam("sort") String sort, @RequestParam("order") String order,
			@RequestParam("page") int page, @RequestParam("size") int size, @RequestParam(name = "startDate", required = false) String startDate,
			@RequestParam(name = "endDate", required = false) String endDate) throws ParseException {
		Date createdDateStart;
		Date createdDateEnd;
		if (startDate.equals("null") || startDate == "") {
			createdDateStart = null;
		} else {
			createdDateStart = new SimpleDateFormat("dd/MM/yyyy").parse(startDate);
		}
		if (endDate.equals("null") || endDate == "") {
			createdDateEnd = null;
		} else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new SimpleDateFormat("dd/MM/yyyy").parse(endDate));
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			calendar.set(Calendar.MILLISECOND, 999);
			createdDateEnd = calendar.getTime();
		}
		return userService.pendingIssuanceStat(organizationName, groupID, sort, order, page, size, createdDateStart, createdDateEnd);
	}
	
	@GetMapping("/groups/{groupID}/pendingApprovalStats")
	public ResultPage pendingApprovalStat(@PathVariable("organizationName") String organizationName, @PathVariable("groupID") String groupID,
			@RequestParam("sort") String sort, @RequestParam("order") String order, @RequestParam("page") int page,
			@RequestParam("size") int size, @RequestParam(name = "startDate", required = false) String startDate,
			@RequestParam(name = "endDate", required = false) String endDate) throws ParseException {
		Date createdDateStart;
		Date createdDateEnd;
		if (startDate.equals("null") || startDate == "") {
			createdDateStart = null;
		} else {
			createdDateStart = new SimpleDateFormat("dd/MM/yyyy").parse(startDate);
		}
		if (endDate.equals("null") || endDate == "") {
			createdDateEnd = null;
		} else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new SimpleDateFormat("dd/MM/yyyy").parse(endDate));
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			calendar.set(Calendar.MILLISECOND, 999);
			createdDateEnd = calendar.getTime();
		}
		return userService.pendingApprovalStat(organizationName, groupID, sort, order, page, size, createdDateStart, createdDateEnd);
	}
	
	@GetMapping("/userStats")
	public List<UserStats> userStats(@PathVariable("organizationName") String organizationName){
		return userService.userStats(organizationName);
	}
	
	@GetMapping("/{userName}/logout")
	public void userLogout(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName){
		 userService.userLogout(organizationName, userName);
		 return ;
	}
	
	@PostMapping(path = "/express-enroll/groups/{groupID}")
	public ResponseEntity<HttpStatus> experssEnroll(@RequestHeader HttpHeaders httpHeaders, @PathVariable("organizationName") String organizationName,@PathVariable("groupID") String groupID, @RequestBody ExpressEnrollmentSchema schema) throws EnrollmentServiceException {
		return new ResponseEntity(HttpStatus.valueOf(userService.experssEnroll(httpHeaders, organizationName, null, groupID, null, null, schema)));
	}
	
	@PostMapping(path = "/express-enroll",  consumes= "multipart/form-data")
    public ResponseEntity<HttpStatus> expressFilesUpload(@RequestHeader HttpHeaders httpHeaders, 
    													@PathVariable("organizationName") String organizationName,
											    		@RequestParam(value = "groupID", required = true) String groupID,
											            @RequestParam(value = "groupName", required = true) String groupName,
											            @RequestParam(value = "roleIDs", required = true) String roleIDs,
											            @RequestParam(value = "enrolledBy", required = true) String enrolledBy,
											    		@RequestParam("expressFiles") MultipartFile[] expressFiles,
											    		RedirectAttributes redirectAttributes) {

        if (expressFiles.length == 0) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }else {
        	try {
        		for (MultipartFile expressFile : expressFiles) {
        			ExpressEnrollmentSchema schema = new ObjectMapper().readValue(expressFile.getBytes(), ExpressEnrollmentSchema.class);
        			userService.experssEnroll(httpHeaders, organizationName, enrolledBy, groupID, groupName, roleIDs, schema);
        		}
			} catch (IOException | EnrollmentServiceException | IdentityBrokerServiceException | JpaSystemException | TransactionException e) {
				// TODO Auto-generated catch block
				 return new ResponseEntity(HttpStatus.CONFLICT);
			}
        }
     
        return new ResponseEntity(HttpStatus.OK);
    }
	@GetMapping("/{userId}/mobileIdentities")
	public List<MobileCredential> getUserMobileIdentities(@PathVariable("organizationName") String organizationName, @PathVariable("userId") Long userId) {
		return userService.getUserMobileIDIdentities(userId);
	}
	
	@GetMapping("/{userId}/wf-mobileIdStep-details")
	public WfMobileIDStepDetails getUserWfMobileIDStepDetails(@PathVariable("organizationName") String organizationName, @PathVariable("userId") Long userId) {
		return userService.getUserWfMobileIDStepDetails(userId);
	}
	
	@GetMapping("/getByID/{userId}")
	public User getUserRefNameOrgByUserId(@PathVariable("organizationName") String organizationName, @PathVariable("userId") Long userId) {
		return userService.getUserRefNameOrgByUserId(userId);
	}
	
	@PostMapping("/{userId}/enableSoftOTP2FA")
	public ResponseEntity<Void> enableSoftOTP2FA(@PathVariable("organizationName") String organizationName, @PathVariable("userId") Long userId, @RequestBody  EnableSoftOTP2FA enableSoftOTP2FA) {
		if(enableSoftOTP2FA !=null) {
			return userService.enableSoftOTP2FA(organizationName, enableSoftOTP2FA);
		}else {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/{userId}/biometrics")
	public UserBioInfo getUserBiometricsByUserId(@PathVariable("organizationName") String organizationName, @PathVariable("userId") Long userId) {
		return userService.getUserBiometricsByUserId(userId);
	}
	
	@GetMapping("/{userId}/biometrics/{biometricType}")
	public UserBioInfo getUserBiometricsByUserId(@PathVariable("organizationName") String organizationName, @PathVariable("userId") Long userId,@PathVariable("biometricType") String biometricType) {
		return userService.getUserBioByUserIdAndBioType(userId, biometricType);
	}
	
	@GetMapping("/{userId}/uuid")
	public String generateSessionId(@PathVariable("organizationName") String organizationName, @PathVariable("userId") Long userId) {
		return userService.generateSessionId(organizationName, userId);
	}
	
	@GetMapping("/{userName}/issuedUserDevices")
	public Long getIssuedDevicesByUserName(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		return userService.getIssuedDevicesByUserName(userName, organizationName);
	}
	
	@GetMapping("/{userName}/enrollment-details")
	public ResponseEntity<UserDetails> getUserEnrollmentDetails(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		return userService.getUserEnrollmentDetails(organizationName, userName);
	}
	
	@GetMapping("/{userName}/user-status")
	public ResponseEntity<UserDetails> getUserStatus(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		return userService.getUserStatus(organizationName, userName);
	}
	
	@GetMapping("/getByID/{userId}/details")
	public User getUserDetailsByUserId(@PathVariable("organizationName") String organizationName, @PathVariable("userId") Long userId) {
		return userService.getUserDetailsByUserId(organizationName, userId);
	}
	
	@PostMapping("/orgUserIds")
	public List<Long> getOrgUsersFromUserIds(@PathVariable("organizationName") String organizationName, @RequestBody List<Long> userIdList) {
		return userService.getOrgUsersFromUserIds(organizationName, userIdList);
	}
	
	@PostMapping("/biometrics/face/transparent")
	public User getTransperantFacialImage(@RequestHeader HttpHeaders httpHeaders, @PathVariable("organizationName") String organizationName, @RequestBody User user) {
		return userService.getTransparentFacialImage(httpHeaders, organizationName, user);
	}	
	
	@PostMapping("/express-enroll/rp")
	public ResponseEntity<ExpressEnrollmentResponse> expressEnroll( @PathVariable("organizationName") String organizationName, @RequestBody ExpressEnrollForm expressEnrollForm) throws EnrollmentServiceException, JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();			
		System.out.println(expressEnrollForm.getUserBiometrics().size());
		if(expressEnrollForm.getUserBiometrics().size()>0){
			System.out.println(mapper.writeValueAsString(expressEnrollForm.getUserBiometrics().get(0)));
		}
		return userService.expressEnroll(organizationName, expressEnrollForm);
	}	
	
	@PostMapping("/search/invite")
	public ResponseEntity<HttpStatus> searchUser_SendInvite(@PathVariable("organizationName") String organizationName,  @RequestParam(name = "email") String email,  @RequestParam(name = "groupName", required = false) String groupName, @RequestParam(name = "roleId", required = false) String roleId) throws EnrollmentServiceException {
		return userService.searchUser_SendInvite(organizationName, email, groupName, roleId);
	}
	
	@GetMapping("/{userName}/access/{status}")
	public ResponseEntity<HttpStatus> changeUserAccess(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName, @PathVariable("status") String status) {
		return userService.changeUserAccess(organizationName, userName, status);
	}
	
	@PostMapping("/find")
	public ResponseEntity<UserAvailability> findUserAvailability(@PathVariable("organizationName") String organizationName, @RequestBody UserAvailability userAvailability) {
		return userService.findUserAvailability(organizationName, userAvailability);
	}
	
	@GetMapping("/search/{userNameOrEmail}")
	public ResponseEntity<SearchUserResult> getUserByUserNameOrEmail(@PathVariable("organizationName") String organizationName, @PathVariable("userNameOrEmail") String userNameOrEmail) {
		return userService.getUserByUserNameOrEmail(organizationName, userNameOrEmail);
	}
}
