package com.meetidentity.usermanagement.web.rest.model.notification;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class NotificationResult {
	// private Response.Status statusCode;
	// private JsonNode responseBody;

	private String result;
	@JsonInclude(Include.NON_NULL)
	private String message;
//	@JsonInclude(Include.NON_NULL)
//	private List<NewCookie> cookies;

	public NotificationResult(/* Response.Status code, */String result) {
		// this.statusCode = code;
		this.result = result;
	}

	public NotificationResult() {
		
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

//	public List<NewCookie> getCookies() {
//		return cookies;
//	}
//
//	public void addCookies(List<NewCookie> cookiesListParam) {
//		cookies = cookiesListParam;
//	}

}
