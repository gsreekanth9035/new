package com.meetidentity.usermanagement.web.rest.model;

public class ConfigValue {
	private Long id;
	private String value;

	public ConfigValue() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConfigValue(Long id, String value) {
		super();
		this.id = id;
		this.value = value;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
