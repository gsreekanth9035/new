package com.meetidentity.usermanagement.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.UserService;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/logout")
public class LogoutResource {
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/{userName}")
	void userLogout(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		userService.userLogout(organizationName, userName);
		return;
	}
}
