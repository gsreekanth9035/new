package com.meetidentity.usermanagement.web.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.web.rest.model.BuildInfo;


@RestController
@RequestMapping("/api/version")
public class BuildInfoResource {

	private static final Logger log = LoggerFactory.getLogger(BuildInfoResource.class);

	@GetMapping("/getBuildInfo")
	public ResponseEntity<BuildInfo> getBuildInfo() {

		BuildInfo buildInfo = new BuildInfo();

		log.info("Fetching Build information from Manifest Metadata...");
		InputStream manifestStream = BuildInfoResource.class.getClassLoader()
				.getResourceAsStream("META-INF/MANIFEST.MF");
		if (manifestStream != null) {
			Manifest manifest = null;
			try {
				manifest = new Manifest(manifestStream);
				Attributes attributes = manifest.getMainAttributes();
				log.info(
						"************************************ Build Information from Manifest Metadata ************************************");

				log.info("artifactId: " + attributes.getValue("artifactId"));
				log.info("version: " + attributes.getValue("version"));
				log.info("buildNumber: " + attributes.getValue("buildNumber"));
				log.info("commitId: " + attributes.getValue("commitId"));
				log.info("branch: " + attributes.getValue("branch"));
				log.info("prId: " + attributes.getValue("prId"));
				log.info("prDestinationBranch: " + attributes.getValue("prDestinationBranch"));

				buildInfo.setArtifactId(attributes.getValue("artifactId"));
				buildInfo.setVersion(attributes.getValue("version"));
				buildInfo.setBuildNumber(attributes.getValue("buildNumber"));
				buildInfo.setCommitId(attributes.getValue("commitId"));
				buildInfo.setBranch(attributes.getValue("branch"));
				buildInfo.setPrId(attributes.getValue("prId"));
				buildInfo.setPrDestinationBranch(attributes.getValue("prDestinationBranch"));

			} catch (IOException e) {
				log.error("Exception occured while fetching build information from Manifest Metadata: {}",
						e.getMessage());
			}
		}

		return ResponseEntity.ok(buildInfo);

	}

}
