package com.meetidentity.usermanagement.web.rest.model;

public class WfStepAppletLoadingInfo {
//	private Long id;
	private Boolean pivAppletEnabled;
	private Boolean fido2AppletEnabled;
	private String attestationCert;
	private String attestationCertPrivateKey;
	private String aaguid;

	public Boolean getPivAppletEnabled() {
		return pivAppletEnabled;
	}

	public void setPivAppletEnabled(Boolean pivAppletEnabled) {
		this.pivAppletEnabled = pivAppletEnabled;
	}

	public Boolean getFido2AppletEnabled() {
		return fido2AppletEnabled;
	}

	public void setFido2AppletEnabled(Boolean fido2AppletEnabled) {
		this.fido2AppletEnabled = fido2AppletEnabled;
	}

	public String getAttestationCert() {
		return attestationCert;
	}

	public void setAttestationCert(String attestationCert) {
		this.attestationCert = attestationCert;
	}

	public String getAttestationCertPrivateKey() {
		return attestationCertPrivateKey;
	}

	public void setAttestationCertPrivateKey(String attestationCertPrivateKey) {
		this.attestationCertPrivateKey = attestationCertPrivateKey;
	}

	public String getAaguid() {
		return aaguid;
	}

	public void setAaguid(String aaguid) {
		this.aaguid = aaguid;
	}

}
