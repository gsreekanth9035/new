package com.meetidentity.usermanagement.web.rest.model;

public class AuthResponse {
	private String username;
	private String signature;
	private String nonce;
	private boolean approved;
	private String pushVerifyCredentialId;
	private String pkiCredentialId;

	public AuthResponse(String username, String nonce, boolean approved, String pushVerifyCredentialId, String pkiCredentialId, String signature) {
		this.username = username;
		this.nonce = nonce;
		this.signature = signature;
		this.approved = approved;
		this.pushVerifyCredentialId = pushVerifyCredentialId;
		this.pkiCredentialId = pkiCredentialId;
	}

	public AuthResponse() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getPushVerifyCredentialId() {
		return pushVerifyCredentialId;
	}

	public void setPushVerifyCredentialId(String pushVerifyCredentialId) {
		this.pushVerifyCredentialId = pushVerifyCredentialId;
	}

	public String getPkiCredentialId() {
		return pkiCredentialId;
	}

	public void setPkiCredentialId(String pkiCredentialId) {
		this.pkiCredentialId = pkiCredentialId;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

}
