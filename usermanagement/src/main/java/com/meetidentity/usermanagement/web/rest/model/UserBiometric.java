package com.meetidentity.usermanagement.web.rest.model;

import java.util.ArrayList;
import java.util.List;

public class UserBiometric {
	
	private String type;
	private String position;
	private String format;
	private String bioData;
	private byte[] data;
	private byte[] wsqData;
	private String base64OfTransparentImageData;
	private String ansi;
	private int imageQuality;
	
	private List<UserBiometricAttribute> userBiometricAttributes = new ArrayList<>();	

	public UserBiometric(String type, String position, String format, String bioData, byte[] data,
			String base64OfTransparentImageData, String ansi, int imageQuality,
			List<UserBiometricAttribute> userBiometricAttributes) {
		super();
		this.type = type;
		this.position = position;
		this.format = format;
		this.bioData = bioData;
		this.data = data;
		this.base64OfTransparentImageData = base64OfTransparentImageData;
		this.ansi = ansi;
		this.imageQuality = imageQuality;
		this.userBiometricAttributes = userBiometricAttributes;
	}

	public UserBiometric() {
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}	

	public String getBioData() {
		return bioData;
	}

	public void setBioData(String bioData) {
		this.bioData = bioData;
	}

	public String getAnsi() {
		return ansi;
	}

	public void setAnsi(String ansi) {
		this.ansi = ansi;
	}

	public int getImageQuality() {
		return imageQuality;
	}

	public void setImageQuality(int imageQuality) {
		this.imageQuality = imageQuality;
	}

	public List<UserBiometricAttribute> getUserBiometricAttributes() {
		return userBiometricAttributes;
	}

	public void addUserBiometricAttribute(UserBiometricAttribute userBiometricAttribute) {
		this.userBiometricAttributes.add(userBiometricAttribute);
	}

	public void setUserBiometricAttributes(List<UserBiometricAttribute> userBiometricAttributes) {
		this.userBiometricAttributes = userBiometricAttributes;
	}

	public String getBase64OfTransparentImageData() {
		return base64OfTransparentImageData;
	}

	public void setBase64OfTransparentImageData(String base64OfTransparentImageData) {
		this.base64OfTransparentImageData = base64OfTransparentImageData;
	}

	public byte[] getWsqData() {
		return wsqData;
	}

	public void setWsqData(byte[] wsqData) {
		this.wsqData = wsqData;
	}

}
