package com.meetidentity.usermanagement.web.rest.model;

import java.util.ArrayList;
import java.util.List;

public class WFMobileIDStepIdentityConfig {
	private Long wfMobileIDStepIdentityConfigId;
	private String friendlyName;
	private boolean pushVerify;
	private boolean softOTP;
	private boolean fido2;
	private boolean pkiCertificatesEnabled;
	private String identityTypeName;

	private Long wfMobileIdStepVisualIDGroupConfigId;
	private List<WFMobileIdStepVisualIDGroupConfig> wfMobileIdStepVisualIDGroupConfigs = new ArrayList<>();
	private List<WFMobileIdStepCertificate> wfMobileIdStepCertificates = new ArrayList<>();
	private WFMobileIDStepCertConfig wfMobileIDStepCertConfig = new WFMobileIDStepCertConfig();
	private WFMobileIDIdentitySoftOTPConfig wfMobileIDIdentitySoftOTPConfig = new WFMobileIDIdentitySoftOTPConfig();

	// required for mobile ID app
	private boolean isContentSigning;
	private String noOfDaysBeforeCertExpiry;
	private String emailNotificationFreq;

	
	
	public Long getWfMobileIdStepVisualIDGroupConfigId() {
		return wfMobileIdStepVisualIDGroupConfigId;
	}

	public void setWfMobileIdStepVisualIDGroupConfigId(Long wfMobileIdStepVisualIDGroupConfigId) {
		this.wfMobileIdStepVisualIDGroupConfigId = wfMobileIdStepVisualIDGroupConfigId;
	}

	public Long getWfMobileIDStepIdentityConfigId() {
		return wfMobileIDStepIdentityConfigId;
	}

	public void setWfMobileIDStepIdentityConfigId(Long wfMobileIDStepIdentityConfigId) {
		this.wfMobileIDStepIdentityConfigId = wfMobileIDStepIdentityConfigId;
	}

	public String getFriendlyName() {
		return friendlyName;
	}

	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}

	public Boolean getPushVerify() {
		return pushVerify;
	}

	public void setPushVerify(Boolean pushVerify) {
		this.pushVerify = pushVerify;
	}

	public Boolean getSoftOTP() {
		return softOTP;
	}

	public void setSoftOTP(Boolean softOTP) {
		this.softOTP = softOTP;
	}

	public Boolean getFido2() {
		return fido2;
	}

	public void setFido2(Boolean fido2) {
		this.fido2 = fido2;
	}

	public WFMobileIDStepCertConfig getWfMobileIDStepCertConfig() {
		return wfMobileIDStepCertConfig;
	}

	public void setWfMobileIDStepCertConfig(WFMobileIDStepCertConfig wfMobileIDStepCertConfig) {
		this.wfMobileIDStepCertConfig = wfMobileIDStepCertConfig;
	}

	public List<WFMobileIdStepVisualIDGroupConfig> getWfMobileIdStepVisualIDGroupConfigs() {
		return wfMobileIdStepVisualIDGroupConfigs;
	}

	public void setWfMobileIdStepVisualIDGroupConfigs(
			List<WFMobileIdStepVisualIDGroupConfig> wfMobileIdStepVisualIDGroupConfigs) {
		this.wfMobileIdStepVisualIDGroupConfigs = wfMobileIdStepVisualIDGroupConfigs;
	}

	public void addWfMobileIdStepVisualIDGroupConfigs(
			WFMobileIdStepVisualIDGroupConfig wfMobileIdStepVisualIDGroupConfig) {
		this.wfMobileIdStepVisualIDGroupConfigs.add(wfMobileIdStepVisualIDGroupConfig);
	}

	public List<WFMobileIdStepCertificate> getWfMobileIdStepCertificates() {
		return wfMobileIdStepCertificates;
	}

	public void setWfMobileIdStepCertificates(List<WFMobileIdStepCertificate> wfMobileIdStepCertificates) {
		this.wfMobileIdStepCertificates = wfMobileIdStepCertificates;
	}

	public void addWfMobileIdStepCertificate(WFMobileIdStepCertificate wfMobileIdStepCertificate) {
		this.wfMobileIdStepCertificates.add(wfMobileIdStepCertificate);
	}

	public Boolean getIsContentSigning() {
		return isContentSigning;
	}

	public void setIsContentSigning(Boolean isContentSigning) {
		this.isContentSigning = isContentSigning;
	}

	public String getNoOfDaysBeforeCertExpiry() {
		return noOfDaysBeforeCertExpiry;
	}

	public void setNoOfDaysBeforeCertExpiry(String noOfDaysBeforeCertExpiry) {
		this.noOfDaysBeforeCertExpiry = noOfDaysBeforeCertExpiry;
	}

	public String getEmailNotificationFreq() {
		return emailNotificationFreq;
	}

	public void setEmailNotificationFreq(String emailNotificationFreq) {
		this.emailNotificationFreq = emailNotificationFreq;
	}

	public String getIdentityTypeName() {
		return identityTypeName;
	}

	public void setIdentityTypeName(String identityTypeName) {
		this.identityTypeName = identityTypeName;
	}

	public void setPushVerify(boolean pushVerify) {
		this.pushVerify = pushVerify;
	}

	public void setSoftOTP(boolean softOTP) {
		this.softOTP = softOTP;
	}
	
	public void setFido2(boolean fido2) {
		this.fido2 = fido2;
	}

	public void setContentSigning(boolean isContentSigning) {
		this.isContentSigning = isContentSigning;
	}

	public WFMobileIDIdentitySoftOTPConfig getWfMobileIDIdentitySoftOTPConfig() {
		return wfMobileIDIdentitySoftOTPConfig;
	}

	public void setWfMobileIDIdentitySoftOTPConfig(
			WFMobileIDIdentitySoftOTPConfig wfMobileIDIdentitySoftOTPConfig) {
		this.wfMobileIDIdentitySoftOTPConfig = wfMobileIDIdentitySoftOTPConfig;
	}

	public Boolean getPkiCertificatesEnabled() {
		return pkiCertificatesEnabled;
	}

	public void setPkiCertificatesEnabled(Boolean pkiCertificatesEnabled) {
		this.pkiCertificatesEnabled = pkiCertificatesEnabled;
	}

}
