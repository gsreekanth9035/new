package com.meetidentity.usermanagement.web.rest.model;

import java.io.Serializable;

public class UserDevice implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String friendlyName;
	private String type;
	private String uniqueIdentifier;
	private String identitySerialNumber;
	private Boolean identitySerialNumberVerified;
	private String operatingSystem;
	private String osVersion;
	private String uuid;
	private String userData;
	private Long userId;
	private String mobileNumber;
	private String otp;
	private String gpsCoordinates;
	private String challenge;
	private String organizationName;
	private String publicKeyB64;
	private String publicKeyAlgorithm;
	private String devicePublicKeyB64;
	private String devicePublicKeyAlgorithm;
	
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getFriendlyName() {
		return friendlyName;
	}
	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUniqueIdentifier() {
		return uniqueIdentifier;
	}
	public void setUniqueIdentifier(String uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}
	public String getOperatingSystem() {
		return operatingSystem;
	}
	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	public String getUuid() {
		return uuid;
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getUserData() {
		return userData;
	}
	public void setUserData(String userData) {
		this.userData = userData;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getGpsCoordinates() {
		return gpsCoordinates;
	}
	public void setGpsCoordinates(String gpsCoordinates) {
		this.gpsCoordinates = gpsCoordinates;
	}
	public String getChallenge() {
		return challenge;
	}
	public void setChallenge(String challenge) {
		this.challenge = challenge;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getPublicKeyB64() {
		return publicKeyB64;
	}
	public void setPublicKeyB64(String publicKeyB64) {
		this.publicKeyB64 = publicKeyB64;
	}
	public String getPublicKeyAlgorithm() {
		return publicKeyAlgorithm;
	}
	public void setPublicKeyAlgorithm(String publicKeyAlgorithm) {
		this.publicKeyAlgorithm = publicKeyAlgorithm;
	}
	public String getDevicePublicKeyB64() {
		return devicePublicKeyB64;
	}
	public void setDevicePublicKeyB64(String devicePublicKeyB64) {
		this.devicePublicKeyB64 = devicePublicKeyB64;
	}
	public String getDevicePublicKeyAlgorithm() {
		return devicePublicKeyAlgorithm;
	}
	public void setDevicePublicKeyAlgorithm(String devicePublicKeyAlgorithm) {
		this.devicePublicKeyAlgorithm = devicePublicKeyAlgorithm;
	}
	public String getIdentitySerialNumber() {
		return identitySerialNumber;
	}
	public void setIdentitySerialNumber(String identitySerialNumber) {
		this.identitySerialNumber = identitySerialNumber;
	}
	
	public Boolean getIdentitySerialNumberVerified() {
		return identitySerialNumberVerified;
	}
	public void setIdentitySerialNumberVerified(Boolean identitySerialNumberVerified) {
		this.identitySerialNumberVerified = identitySerialNumberVerified;
	}

	
	
	
	

}
