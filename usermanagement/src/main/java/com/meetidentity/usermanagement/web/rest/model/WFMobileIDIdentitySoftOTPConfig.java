package com.meetidentity.usermanagement.web.rest.model;

public class WFMobileIDIdentitySoftOTPConfig {
	private long id;
	private String softOTPType;
	private String secretKey;
	private String algorithm;
	private int digits;
	private int interval;
	private long counter;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSoftOTPType() {
		return softOTPType;
	}
	public void setSoftOTPType(String softOTPType) {
		this.softOTPType = softOTPType;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public String getAlgorithm() {
		return algorithm;
	}
	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}
	public int getDigits() {
		return digits;
	}
	public void setDigits(int digits) {
		this.digits = digits;
	}
	public int getInterval() {
		return interval;
	}
	public void setInterval(int interval) {
		this.interval = interval;
	}
	public long getCounter() {
		return counter;
	}
	public void setCounter(long counter) {
		this.counter = counter;
	}

	

}
