package com.meetidentity.usermanagement.web.rest.model;

import java.util.ArrayList;
import java.util.List;

public class UCard {

	private String front;
	private String back;
	private String qrCodeData;
	private String userData;
	private List<UserIdentityData> userIdentityData = new ArrayList<>();
	private String identitySerialNumber;
	
	private String templateName;
	private String identityTypeName;

	public String getFront() {
		return front;
	}

	public void setFront(String front) {
		this.front = front;
	}

	public String getBack() {
		return back;
	}

	public void setBack(String back) {
		this.back = back;
	}

	public String getUserData() {
		return userData;
	}

	public void setUserData(String userData) {
		this.userData = userData;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getQrCodeData() {
		return qrCodeData;
	}

	public void setQrCodeData(String qrCodeData) {
		this.qrCodeData = qrCodeData;
	}

	public List<UserIdentityData> getUserIdentityData() {
		return userIdentityData;
	}

	public void setUserIdentityData(List<UserIdentityData> userIdentityData) {
		this.userIdentityData = userIdentityData;
	}

	public String getIdentityTypeName() {
		return identityTypeName;
	}

	public void setIdentityTypeName(String identityTypeName) {
		this.identityTypeName = identityTypeName;
	}

	public String getIdentitySerialNumber() {
		return identitySerialNumber;
	}

	public void setIdentitySerialNumber(String identitySerialNumber) {
		this.identitySerialNumber = identitySerialNumber;
	}

	
}
