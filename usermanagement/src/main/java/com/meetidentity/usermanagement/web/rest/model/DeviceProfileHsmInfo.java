package com.meetidentity.usermanagement.web.rest.model;

public class DeviceProfileHsmInfo {
	private Long id;
	private Long hsmTypeId;
	private String partitionName;
	private String partitionSerialNumber;
	private String partitionPassword;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getHsmTypeId() {
		return hsmTypeId;
	}

	public void setHsmTypeId(Long hsmTypeId) {
		this.hsmTypeId = hsmTypeId;
	}

	public String getPartitionName() {
		return partitionName;
	}

	public void setPartitionName(String partitionName) {
		this.partitionName = partitionName;
	}

	public String getPartitionSerialNumber() {
		return partitionSerialNumber;
	}

	public void setPartitionSerialNumber(String partitionSerialNumber) {
		this.partitionSerialNumber = partitionSerialNumber;
	}

	public String getPartitionPassword() {
		return partitionPassword;
	}

	public void setPartitionPassword(String partitionPassword) {
		this.partitionPassword = partitionPassword;
	}

}
