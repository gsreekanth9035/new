package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;

public class UserPIVModel {

	private Long id;

	private String uimsID;
	
	private String pivIdNumber;	

	private String employeeAffiliation;

	private String affiliationOptionalLine;

	private String employeeAffiliationColorCode;

	private String employeePhotoBorderColor;

	private String agencyCardSerialNumber;

	private String agency;

	private String agencyAbbreviation;

	private String issuerIdentificationNumber;

	private String agencySpecificData;

	private String agencySpecificText;

	private String dateOfIssuance;

	private String dateOfExpiry;

	private String cardExpiry;

	private String rank;

	private byte[] pdf417BarCode;

	private byte[] linearBarCode;
	
	
	public UserPIVModel() {};
	
	

	public UserPIVModel(Long id, String uimsID, String pivIdNumber, String employeeAffiliation,
			String employeeAffiliationColorCode, String agencyCardSerialNumber, String agency,
			String issuerIdentificationNumber, String dateOfIssuance, String dateOfExpiry, String cardExpiry, String rank) {
		super();
		this.id = id;
		this.uimsID = uimsID;
		this.pivIdNumber = pivIdNumber;
		this.employeeAffiliation = employeeAffiliation;
		this.employeeAffiliationColorCode = employeeAffiliationColorCode;
		this.agencyCardSerialNumber = agencyCardSerialNumber;
		this.agency = agency;
		this.issuerIdentificationNumber = issuerIdentificationNumber;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
		this.cardExpiry = cardExpiry;
		this.rank = rank;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUimsID() {
		return uimsID;
	}

	public String getPivIdNumber() {
		return pivIdNumber;
	}

	public void setPivIdNumber(String pivIdNumber) {
		this.pivIdNumber = pivIdNumber;
	}

	public void setUimsID(String uimsID) {
		this.uimsID = uimsID;
	}

	public String getEmployeeAffiliation() {
		return employeeAffiliation;
	}

	public void setEmployeeAffiliation(String employeeAffiliation) {
		this.employeeAffiliation = employeeAffiliation;
	}

	public String getAffiliationOptionalLine() {
		return affiliationOptionalLine;
	}

	public void setAffiliationOptionalLine(String affiliationOptionalLine) {
		this.affiliationOptionalLine = affiliationOptionalLine;
	}

	public String getEmployeeAffiliationColorCode() {
		return employeeAffiliationColorCode;
	}

	public void setEmployeeAffiliationColorCode(String employeeAffiliationColorCode) {
		this.employeeAffiliationColorCode = employeeAffiliationColorCode;
	}

	public String getAgencyCardSerialNumber() {
		return agencyCardSerialNumber;
	}

	public void setAgencyCardSerialNumber(String agencyCardSerialNumber) {
		this.agencyCardSerialNumber = agencyCardSerialNumber;
	}

	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public String getAgencySpecificData() {
		return agencySpecificData;
	}

	public void setAgencySpecificData(String agencySpecificData) {
		this.agencySpecificData = agencySpecificData;
	}

	public String getAgencySpecificText() {
		return agencySpecificText;
	}

	public void setAgencySpecificText(String agencySpecificText) {
		this.agencySpecificText = agencySpecificText;
	}

	public String getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(String dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public String getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(String dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getCardExpiry() {
		return cardExpiry;
	}

	public void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public byte[] getPdf417BarCode() {
		return pdf417BarCode;
	}

	public void setPdf417BarCode(byte[] pdf417BarCode) {
		this.pdf417BarCode = pdf417BarCode;
	}

	public byte[] getLinearBarCode() {
		return linearBarCode;
	}

	public void setLinearBarCode(byte[] linearBarCode) {
		this.linearBarCode = linearBarCode;
	}

	public String getAgencyAbbreviation() {
		return agencyAbbreviation;
	}

	public void setAgencyAbbreviation(String agencyAbbreviation) {
		this.agencyAbbreviation = agencyAbbreviation;
	}

	public String getIssuerIdentificationNumber() {
		return issuerIdentificationNumber;
	}

	public void setIssuerIdentificationNumber(String issuerIdentificationNumber) {
		this.issuerIdentificationNumber = issuerIdentificationNumber;
	}

	public String getEmployeePhotoBorderColor() {
		return employeePhotoBorderColor;
	}

	public void setEmployeePhotoBorderColor(String employeePhotoBorderColor) {
		this.employeePhotoBorderColor = employeePhotoBorderColor;
	}

}
