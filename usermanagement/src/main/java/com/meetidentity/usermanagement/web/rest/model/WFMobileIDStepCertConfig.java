package com.meetidentity.usermanagement.web.rest.model;

public class WFMobileIDStepCertConfig {
	private WFStepGenericConfig noOfDaysBeforeCertExpiry;
	private WFStepGenericConfig emailNotificationFreq;

	public WFStepGenericConfig getNoOfDaysBeforeCertExpiry() {
		return noOfDaysBeforeCertExpiry;
	}

	public void setNoOfDaysBeforeCertExpiry(WFStepGenericConfig noOfDaysBeforeCertExpiry) {
		this.noOfDaysBeforeCertExpiry = noOfDaysBeforeCertExpiry;
	}

	public WFStepGenericConfig getEmailNotificationFreq() {
		return emailNotificationFreq;
	}

	public void setEmailNotificationFreq(WFStepGenericConfig emailNotificationFreq) {
		this.emailNotificationFreq = emailNotificationFreq;
	}

}
