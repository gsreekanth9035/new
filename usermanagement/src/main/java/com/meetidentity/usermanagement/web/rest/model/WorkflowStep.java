package com.meetidentity.usermanagement.web.rest.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class WorkflowStep {

	private String name;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String description;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String displayName;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Integer order;
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<IDProofType> idProofTypes = new ArrayList<>();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<WFStepGenericConfig> wfStepGenericConfigs = new ArrayList<>();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<WFStepRegistrationConfig> wfStepRegistrationConfigs = new ArrayList<>();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<WFStepApprovalGroupConfig> wfStepApprovalGroupConfigs = new ArrayList<>();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<WFStepVisualTemplateConfig> wfStepVisualTemplateConfigs = new ArrayList<>();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<WFStepCertConfig> wfStepCertConfigs = new ArrayList<>();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<WfStepGroupConfig> wfStepChipEncodeVIDPrintGroupConfigs = new ArrayList<>();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<WfStepGroupConfig> wfStepChipEncodeGroupConfigs = new ArrayList<>();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<WfStepGroupConfig> wfStepPrintGroupConfigs = new ArrayList<>();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdentityConfigs = new ArrayList<>();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<Group> wfStepAdjudicationGroupConfigs = new ArrayList<>();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<OrganizationIdentity> wfMobileIDStepTrustedIdentities = new ArrayList<>();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private WfStepAppletLoadingInfo wfStepAppletLoadingInfo;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public List<WFStepGenericConfig> getWfStepGenericConfigs() {
		return wfStepGenericConfigs;
	}

	public void setWfStepGenericConfigs(List<WFStepGenericConfig> wfStepGenericConfigs) {
		this.wfStepGenericConfigs = wfStepGenericConfigs;
	}

	public List<WFStepRegistrationConfig> getWfStepRegistrationConfigs() {
		return wfStepRegistrationConfigs;
	}

	public void setWfStepRegistrationConfigs(List<WFStepRegistrationConfig> wfStepRegistrationConfigs) {
		this.wfStepRegistrationConfigs = wfStepRegistrationConfigs;
	}

	public List<WFStepApprovalGroupConfig> getWfStepApprovalGroupConfigs() {
		return wfStepApprovalGroupConfigs;
	}

	public void setWfStepApprovalGroupConfigs(List<WFStepApprovalGroupConfig> wfStepApprovalGroupConfigs) {
		this.wfStepApprovalGroupConfigs = wfStepApprovalGroupConfigs;
	}

	public List<WFStepVisualTemplateConfig> getWfStepVisualTemplateConfigs() {
		return wfStepVisualTemplateConfigs;
	}

	public void setWfStepVisualTemplateConfigs(List<WFStepVisualTemplateConfig> wfStepVisualTemplateConfigs) {
		this.wfStepVisualTemplateConfigs = wfStepVisualTemplateConfigs;
	}

	public List<WFStepCertConfig> getWfStepCertConfigs() {
		return wfStepCertConfigs;
	}

	public void setWfStepCertConfigs(List<WFStepCertConfig> wfStepCertConfigs) {
		this.wfStepCertConfigs = wfStepCertConfigs;
	}

	public List<WfStepGroupConfig> getWfStepPrintGroupConfigs() {
		return wfStepPrintGroupConfigs;
	}

	public void setWfStepPrintGroupConfigs(List<WfStepGroupConfig> wfStepPrintGroupConfigs) {
		this.wfStepPrintGroupConfigs = wfStepPrintGroupConfigs;
	}

	public List<WFMobileIDStepIdentityConfig> getWfMobileIDStepIdentityConfigs() {
		return wfMobileIDStepIdentityConfigs;
	}

	public void setWfMobileIDStepIdentityConfigs(List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdentityConfigs) {
		this.wfMobileIDStepIdentityConfigs = wfMobileIDStepIdentityConfigs;
	}

	public List<Group> getWfStepAdjudicationGroupConfigs() {
		return wfStepAdjudicationGroupConfigs;
	}

	public void setWfStepAdjudicationGroupConfigs(List<Group> wfStepAdjudicationGroupConfigs) {
		this.wfStepAdjudicationGroupConfigs = wfStepAdjudicationGroupConfigs;
	}

	public List<OrganizationIdentity> getWfMobileIDStepTrustedIdentities() {
		return wfMobileIDStepTrustedIdentities;
	}

	public void setWfMobileIDStepTrustedIdentities(List<OrganizationIdentity> wfMobileIDStepTrustedIdentities) {
		this.wfMobileIDStepTrustedIdentities = wfMobileIDStepTrustedIdentities;
	}

	public List<WfStepGroupConfig> getWfStepChipEncodeVIDPrintGroupConfigs() {
		return wfStepChipEncodeVIDPrintGroupConfigs;
	}

	public void setWfStepChipEncodeVIDPrintGroupConfigs(List<WfStepGroupConfig> wfStepChipEncodeVIDPrintGroupConfigs) {
		this.wfStepChipEncodeVIDPrintGroupConfigs = wfStepChipEncodeVIDPrintGroupConfigs;
	}

	public List<WfStepGroupConfig> getWfStepChipEncodeGroupConfigs() {
		return wfStepChipEncodeGroupConfigs;
	}

	public void setWfStepChipEncodeGroupConfigs(List<WfStepGroupConfig> wfStepChipEncodeGroupConfigs) {
		this.wfStepChipEncodeGroupConfigs = wfStepChipEncodeGroupConfigs;
	}

	public List<IDProofType> getIdProofTypes() {
		return idProofTypes;
	}

	public void setIdProofTypes(List<IDProofType> idProofTypes) {
		this.idProofTypes = idProofTypes;
	}

	public WfStepAppletLoadingInfo getWfStepAppletLoadingInfo() {
		return wfStepAppletLoadingInfo;
	}

	public void setWfStepAppletLoadingInfo(WfStepAppletLoadingInfo wfStepAppletLoadingInfo) {
		this.wfStepAppletLoadingInfo = wfStepAppletLoadingInfo;
	}

}