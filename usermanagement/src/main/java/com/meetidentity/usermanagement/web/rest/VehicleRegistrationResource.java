package com.meetidentity.usermanagement.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.VehicleService;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.VehicleRegistration;
import com.meetidentity.usermanagement.web.rest.model.VehicleRegistrations;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/vehicle-registrations")
public class VehicleRegistrationResource {
	
	@Autowired
	private VehicleService vehicleRegistrationService;
	
	@GetMapping
	public ResultPage getAllVehicleRegistrations(@PathVariable("organizationName") String organizationName, @RequestParam("sort") String sort, @RequestParam("order") String order,
			@RequestParam("page") int page, @RequestParam("size") int size) {
		return vehicleRegistrationService.getAllVehicleRegs(organizationName, sort, order, page, size);
	}
	
	@GetMapping("/{licensePlateNumber}")
	public VehicleRegistration getVehicleRegistrationDetails(@PathVariable("organizationName") String organizationName, @PathVariable("licensePlateNumber") String licensePlateNumber) {
		return vehicleRegistrationService.getVehicleRegistrationDetails(organizationName, licensePlateNumber);
	}
	
	@PostMapping
	public ResponseEntity<Void> registerVehicles(@PathVariable("organizationName") String organizationName, @RequestBody VehicleRegistrations vehicleRegistrations) throws Exception {
		return vehicleRegistrationService.createVehicleRegistrations(organizationName, vehicleRegistrations);
	}
	
	@PutMapping("/{licensePlateNumber}")
	public ResponseEntity<String> editVehicleRegistration(@PathVariable("organizationName") String organizationName, @PathVariable("licensePlateNumber") String licensePlateNumber,
			@RequestBody VehicleRegistration vehicleRegistration) throws Exception {
		System.out.println("edit vehicle called");
		return vehicleRegistrationService.updateVehicleRegistration(organizationName, licensePlateNumber, vehicleRegistration);
	}
	
	@DeleteMapping("/{vin}")
	public ResponseEntity<String> deleteVehicleRegistration(@PathVariable("organizationName") String organizationName,@PathVariable("vin") String vin) {
		return vehicleRegistrationService.deleteVehicleRegistration(organizationName, vin);
		
	}
	
}
