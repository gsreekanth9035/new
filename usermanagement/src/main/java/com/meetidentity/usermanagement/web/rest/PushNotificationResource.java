package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.PushNotificationDeviceService;
import com.meetidentity.usermanagement.web.rest.model.PushNotificationDevice;
import com.meetidentity.usermanagement.web.rest.model.PushNotificationPayLoad;


@RestController
@RequestMapping("/api/v1/push-notification-devices")
public class PushNotificationResource {

	@Autowired
	private PushNotificationDeviceService pushNotificationDeviceService;
	
	 @PostMapping
	 public ResponseEntity<Void>  createPlatformEndpoint(@RequestBody  List<PushNotificationDevice> pushNotificationDevices) {
		 return pushNotificationDeviceService.addPushNotificationDevice(pushNotificationDevices);
	 }
	 
	 @PostMapping("/payLoad")
	 public PushNotificationPayLoad getPushNotificationPayLoadData(@RequestBody PushNotificationDevice pushNotificationDevice) {
		 return pushNotificationDeviceService.getPushNotificationPayLoadData(pushNotificationDevice);
	 }

}
