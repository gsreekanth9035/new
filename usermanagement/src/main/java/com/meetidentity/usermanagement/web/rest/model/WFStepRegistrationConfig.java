package com.meetidentity.usermanagement.web.rest.model;

import java.util.List;

public class WFStepRegistrationConfig {

	private Long id;
	private Long orgFieldId;
	private String fieldName;
	private String fieldLabel;
	private boolean required;
	private String type;
	private List<?> dropDownList;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOrgFieldId() {
		return orgFieldId;
	}
	public void setOrgFieldId(Long orgFieldId) {
		this.orgFieldId = orgFieldId;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldLabel() {
		return fieldLabel;
	}
	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}
	public boolean isRequired() {
		return required;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<?> getDropDownList() {
		return dropDownList;
	}
	public void setDropDownList(List<?> dropDownList) {
		this.dropDownList = dropDownList;
	}
	
}
