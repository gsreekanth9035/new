package com.meetidentity.usermanagement.web.rest.model;

public class AuthenticatorClientInfo {

	private String clientID;
	private String clientSecret;
	private OpenIDConfiguration openIDConfiguration;

	public AuthenticatorClientInfo() {

	}

	public AuthenticatorClientInfo(String clientID, String clientSecret, OpenIDConfiguration openIDConfiguration) {
		super();
		this.clientID = clientID;
		this.clientSecret = clientSecret;
		this.openIDConfiguration = openIDConfiguration;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public OpenIDConfiguration getOpenIDConfiguration() {
		return openIDConfiguration;
	}

	public void setOpenIDConfiguration(OpenIDConfiguration openIDConfiguration) {
		this.openIDConfiguration = openIDConfiguration;
	}

}
