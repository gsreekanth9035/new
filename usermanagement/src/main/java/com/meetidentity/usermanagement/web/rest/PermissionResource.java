package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.PermissionService;
import com.meetidentity.usermanagement.web.rest.model.Permission;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/roles/{roleName}/permissions")
public class PermissionResource {
	
	@Autowired
	private PermissionService permissionService;

	@GetMapping
	List<Permission> getPermissionsByRole(@PathVariable("organizationName") String organizationName, @PathVariable("roleName") String roleName) {
		return permissionService.getPermissionsByRole(organizationName, roleName);
	}
}
