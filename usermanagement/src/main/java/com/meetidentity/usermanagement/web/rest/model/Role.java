package com.meetidentity.usermanagement.web.rest.model;

public class Role {
	
	private String name;
	private String description;
	private String authServiceRoleID;
	private String color;
	
	public Role(String name, String description, String authServiceRoleID, String color) {
		super();
		this.name = name;
		this.description = description;
		this.authServiceRoleID = authServiceRoleID;
		this.color = color;
	}

	public Role() {
		
	}	

	public Role(String name, String description, String authServiceRoleID) {
		super();
		this.name = name;
		this.description = description;
		this.authServiceRoleID = authServiceRoleID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthServiceRoleID() {
		return authServiceRoleID;
	}

	public void setAuthServiceRoleID(String authServiceRoleID) {
		this.authServiceRoleID = authServiceRoleID;
	}
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
}
