package com.meetidentity.usermanagement.web.rest.model;

public class WFMobileIdStepVisualIDGroupConfig {
	private Long id;
	private Long workflowMobileIdentityIssuanceId;
	private String referenceGroupId;
	private String referenceGroupName;
	private Long visualTemplateId;
	private String identityTypeName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getWorkflowMobileIdentityIssuanceId() {
		return workflowMobileIdentityIssuanceId;
	}

	public void setWorkflowMobileIdentityIssuanceId(Long workflowMobileIdentityIssuanceId) {
		this.workflowMobileIdentityIssuanceId = workflowMobileIdentityIssuanceId;
	}

	public String getReferenceGroupId() {
		return referenceGroupId;
	}

	public void setReferenceGroupId(String referenceGroupId) {
		this.referenceGroupId = referenceGroupId;
	}

	public String getReferenceGroupName() {
		return referenceGroupName;
	}

	public void setReferenceGroupName(String referenceGroupName) {
		this.referenceGroupName = referenceGroupName;
	}

	public Long getVisualTemplateId() {
		return visualTemplateId;
	}

	public void setVisualTemplateId(Long visualTemplateId) {
		this.visualTemplateId = visualTemplateId;
	}

	public String getIdentityTypeName() {
		return identityTypeName;
	}

	public void setIdentityTypeName(String identityTypeName) {
		this.identityTypeName = identityTypeName;
	}

}
