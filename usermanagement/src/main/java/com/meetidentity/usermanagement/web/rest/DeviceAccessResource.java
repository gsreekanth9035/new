package com.meetidentity.usermanagement.web.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.UserService;
import com.meetidentity.usermanagement.web.rest.model.AuthResponse;
import com.meetidentity.usermanagement.web.rest.model.TokenDetails;

@RestController
@RequestMapping("/api/v1")
public class DeviceAccessResource {

	@Autowired
	private UserService userservice;


	@PostMapping("/organizations/{organizationName}/mobile-device/token")
	public ResponseEntity<TokenDetails> getMobileIDAccessToken( @PathVariable("organizationName") String  organizationName, 
			@RequestBody Map<String, String>  publicKey){		
		return userservice.generateMobileIDUserTokens(organizationName,publicKey.get("publicKeyB64"),publicKey.get("publicKeyAlgorithm"));
		
	}
	
	@PostMapping("/organizations/{organizationName}/mobile-device/pn-login")
	public void validatePNLogin( @PathVariable("organizationName") String  organizationName, @RequestBody AuthResponse authResponse) throws Exception{		
		userservice.validatePNLogin(organizationName, authResponse);		
	}
}
