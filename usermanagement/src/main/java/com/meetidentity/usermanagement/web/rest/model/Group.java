package com.meetidentity.usermanagement.web.rest.model;

import java.util.List;

public class Group {
	
	private String id;
	private String name;
	private Long visualTemplateID;

	private String path;
	private List<String> realmRoles;
	private List<Group> subGroups;
	
	public Group() {
		
	}
	
	public Group(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	

	public Group(String id, String name, Long visualTemplateID, String path, List<String> realmRoles) {
		super();
		this.id = id;
		this.name = name;
		this.visualTemplateID = visualTemplateID;
		this.path = path;
		this.realmRoles = realmRoles;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getVisualTemplateID() {
		return visualTemplateID;
	}

	public void setVisualTemplateID(Long visualTemplateID) {
		this.visualTemplateID = visualTemplateID;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<String> getRealmRoles() {
		return realmRoles;
	}

	public void setRealmRoles(List<String> realmRoles) {
		this.realmRoles = realmRoles;
	}

	public List<Group> getSubGroups() {
		return subGroups;
	}

	public void setSubGroups(List<Group> subGroups) {
		this.subGroups = subGroups;
	}
	
	

}
