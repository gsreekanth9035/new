package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.OrganizationAdvertisementService;
import com.meetidentity.usermanagement.web.rest.model.OrganizationAdvertisementInfo;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}")
public class OrganizationAdvertisementResource {
	
	@Autowired
	OrganizationAdvertisementService organizationAdvertisementService;
	
	@GetMapping("/advertisement-info")
	public List<OrganizationAdvertisementInfo> getAdvertisementInfo(@PathVariable("organizationName") String organizationName){
		return organizationAdvertisementService.getOrganizationAdvertisements(organizationName);
	}
}
