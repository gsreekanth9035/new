package com.meetidentity.usermanagement.web.rest.model;

public class UserAvailability {

	private String username;
	private boolean username_available;
	private String email;
	private boolean email_available;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isUsername_available() {
		return username_available;
	}

	public void setUsername_available(boolean username_available) {
		this.username_available = username_available;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEmail_available() {
		return email_available;
	}

	public void setEmail_available(boolean email_available) {
		this.email_available = email_available;
	}

}
