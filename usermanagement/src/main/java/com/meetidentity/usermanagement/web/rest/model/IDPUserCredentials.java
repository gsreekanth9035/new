package com.meetidentity.usermanagement.web.rest.model;

import java.util.Map;


public class IDPUserCredentials {

	private String username;
	private String pushToken;
	private String publicKey;
	private String publicKeyCert;
	private String otpSecret;
	private String deviceName;
	private String deviceOS;
	private boolean wedAuthnCredEnabled;
	private String status;
	private String credentialId;
	private Map<String, String> tbdeletedCredentialIds;
	private Map<String, String> tbupdatedCredentialIds;
	private String tagValue;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPushToken() {
		return pushToken;
	}

	public void setPushToken(String pushToken) {
		this.pushToken = pushToken;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceOS() {
		return deviceOS;
	}

	public void setDeviceOS(String deviceOS) {
		this.deviceOS = deviceOS;
	}

	public String getCredentialId() {
		return credentialId;
	}

	public void setCredentialId(String credentialId) {
		this.credentialId = credentialId;
	}
	
	public String getPublicKeyCert() {
		return publicKeyCert;
	}

	public void setPublicKeyCert(String publicKeyCert) {
		this.publicKeyCert = publicKeyCert;
	}

	public String getOtpSecret() {
		return otpSecret;
	}

	public void setOtpSecret(String otpSecret) {
		this.otpSecret = otpSecret;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Map<String, String> getTbdeletedCredentialIds() {
		return tbdeletedCredentialIds;
	}

	public void setTbdeletedCredentialIds(Map<String, String> tbdeletedCredentialIds) {
		this.tbdeletedCredentialIds = tbdeletedCredentialIds;
	}

	public Map<String, String> getTbupdatedCredentialIds() {
		return tbupdatedCredentialIds;
	}

	public void setTbupdatedCredentialIds(Map<String, String> tbupdatedCredentialIds) {
		this.tbupdatedCredentialIds = tbupdatedCredentialIds;
	}

	public String getTagValue() {
		return tagValue;
	}

	public void setTagValue(String tagValue) {
		this.tagValue = tagValue;
	}

	public boolean isWedAuthnCredEnabled() {
		return wedAuthnCredEnabled;
	}

	public void setWedAuthnCredEnabled(boolean wedAuthnCredEnabled) {
		this.wedAuthnCredEnabled = wedAuthnCredEnabled;
	}



}
