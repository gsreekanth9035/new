package com.meetidentity.usermanagement.web.rest.model;

public class IDPUserCredentialConfigResponse {

	private String pushVerifyCredentialId;
	private String pkiCredentialId;
	private String otpCredentialId;
	private String fidoCredentialId;

	public IDPUserCredentialConfigResponse(String pushVerifyCredentialId, String pkiCredentialId,
			String otpCredentialId, String fidoCredentialId) {

		this.pushVerifyCredentialId = pushVerifyCredentialId;
		this.pkiCredentialId = pkiCredentialId;
		this.otpCredentialId = otpCredentialId;
		this.fidoCredentialId = fidoCredentialId;
	}

	public IDPUserCredentialConfigResponse() {
	}

	public String getPushVerifyCredentialId() {
		return pushVerifyCredentialId;
	}

	public void setPushVerifyCredentialId(String pushVerifyCredentialId) {
		this.pushVerifyCredentialId = pushVerifyCredentialId;
	}

	public String getPkiCredentialId() {
		return pkiCredentialId;
	}

	public void setPkiCredentialId(String pkiCredentialId) {
		this.pkiCredentialId = pkiCredentialId;
	}

	public String getOtpCredentialId() {
		return otpCredentialId;
	}

	public void setOtpCredentialId(String otpCredentialId) {
		this.otpCredentialId = otpCredentialId;
	}

	public String getFidoCredentialId() {
		return fidoCredentialId;
	}

	public void setFidoCredentialId(String fidoCredentialId) {
		this.fidoCredentialId = fidoCredentialId;
	}

}
