package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;



public class UserEmployeeID {
	
	private long userId;	
	private String uimsId;
	private String employeeIDNumber;
	private Date dateOfIssuance;
	private Date dateOfExpiry;
	private String affiliation;
	private String department;
	private String issuerIN;
	private String agencyCardSn;
	
	public UserEmployeeID(long userId, String uimsId, String employeeIDNumber, Date dateOfIssuance, Date dateOfExpiry,String affiliation, String department,String issuerIN,String agencyCardSn) {
		super();
		this.uimsId = uimsId;
		this.employeeIDNumber = employeeIDNumber;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
		this.affiliation = affiliation;
		this.department = department;
		this.issuerIN = issuerIN;
		this.agencyCardSn = agencyCardSn;
	}

	public UserEmployeeID() {
		// TODO Auto-generated constructor stub
	}

	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}
	public Date getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Date dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getEmployeeIDNumber() {
		return employeeIDNumber;
	}

	public void setEmployeeIDNumber(String employeeIDNumber) {
		this.employeeIDNumber = employeeIDNumber;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getIssuerIN() {
		return issuerIN;
	}

	public void setIssuerIN(String issuerIN) {
		this.issuerIN = issuerIN;
	}

	public String getAgencyCardSn() {
		return agencyCardSn;
	}

	public void setAgencyCardSn(String agencyCardSn) {
		this.agencyCardSn = agencyCardSn;
	}
	
	
	

}
