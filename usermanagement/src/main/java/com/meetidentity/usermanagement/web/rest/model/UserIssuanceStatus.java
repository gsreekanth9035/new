package com.meetidentity.usermanagement.web.rest.model;

import java.io.Serializable;

public class UserIssuanceStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean isIssuanceAllowed;
	private String username;
	private String organizationName;
	

	public UserIssuanceStatus() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public UserIssuanceStatus(boolean isIssuanceAllowed, String username, String organizationName) {
		super();
		this.isIssuanceAllowed = isIssuanceAllowed;
		this.username = username;
		this.organizationName = organizationName;
	}

	public boolean isIssuanceAllowed() {
		return isIssuanceAllowed;
	}
	
	public void setIssuanceAllowed(boolean isIssuanceAllowed) {
		this.isIssuanceAllowed = isIssuanceAllowed;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	
	

}
