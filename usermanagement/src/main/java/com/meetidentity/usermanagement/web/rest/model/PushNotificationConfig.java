package com.meetidentity.usermanagement.web.rest.model;

public class PushNotificationConfig {

	public Long id;
	public String type;
	public String serviceProviderName;
	public String appName;
	public String apiKey;
	public String apiSecurityKey;
	public String serverKey;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getServiceProviderName() {
		return serviceProviderName;
	}
	public void setServiceProviderName(String serviceProviderName) {
		this.serviceProviderName = serviceProviderName;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getApiSecurityKey() {
		return apiSecurityKey;
	}
	public void setApiSecurityKey(String apiSecurityKey) {
		this.apiSecurityKey = apiSecurityKey;
	}
	public String getServerKey() {
		return serverKey;
	}
	public void setServerKey(String serverKey) {
		this.serverKey = serverKey;
	}
	
	
}
