package com.meetidentity.usermanagement.web.rest.util;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

import com.meetidentity.usermanagement.config.Constants;
import com.meetidentity.usermanagement.exception.UIMSException;
import com.meetidentity.usermanagement.exception.message.ErrorMessages;
import com.meetidentity.usermanagement.util.Util;

/**
 * Utility class for HTTP headers creation.
 */
public final class HeaderUtil {

    private static final Logger log = LoggerFactory.getLogger(HeaderUtil.class);

    private static final String APPLICATION_NAME = "usermanagementApp";

    private HeaderUtil() {
    }

    public static HttpHeaders createAlert(String message, String param) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-usermanagementApp-alert", message);
        headers.add("X-usermanagementApp-params", param);
        return headers;
    }

    public static HttpHeaders createEntityCreationAlert(String entityName, String param) {
        return createAlert(APPLICATION_NAME + "." + entityName + ".created", param);
    }

    public static HttpHeaders createEntityUpdateAlert(String entityName, String param) {
        return createAlert(APPLICATION_NAME + "." + entityName + ".updated", param);
    }

    public static HttpHeaders createEntityDeletionAlert(String entityName, String param) {
        return createAlert(APPLICATION_NAME + "." + entityName + ".deleted", param);
    }

    public static HttpHeaders createFailureAlert(String entityName, String errorKey, String defaultMessage) {
        log.error("Entity processing failed, {}", defaultMessage);
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-usermanagementApp-error", "error." + errorKey);
        headers.add("X-usermanagementApp-params", entityName);
        return headers;
    }
    
    public static boolean validateRequiredHeadersForOnboard(HttpHeaders httpHeaders) throws UIMSException {
    	validateHeaderParam(httpHeaders, Constants.LOGGED_IN_USER, null, true);
		validateHeaderParam(httpHeaders, Constants.UIMS_URL, null, true);
    	return true;
    }
    
    public static boolean validateRequiredHeaders(HttpHeaders httpHeaders, List<String> keys) throws UIMSException {
    	if(keys != null) {
    		keys.forEach(key -> {
        		try {
    				validateHeaderParam(httpHeaders, key, null, true);
    				validateHeaderParam(httpHeaders, key, null, true);
    			} catch (UIMSException e) {
    				e.printStackTrace();
    			}
        	});
        	return true;
    	}
    	return false;
    }
   
    public static boolean validateHeaderParam(HttpHeaders httpHeaders, String key, String defaultVal,
			boolean isMandatory) throws UIMSException {
		if (httpHeaders.containsKey(key)) {
			if (httpHeaders.get(key) != null) {
				return true;
			} else {
				if (defaultVal != null) {
					httpHeaders.set(key, defaultVal);
				} else {
					throw new UIMSException( Util.format(ErrorMessages.HEADER_VALUE_REQUIRED, key));
				}
			}
		} else {
			if (isMandatory) {
				throw new UIMSException( Util.format(ErrorMessages.HEADER_REQUIRED, key));
			} else {
				return false;
			}
		}
		return true;
	}
}
