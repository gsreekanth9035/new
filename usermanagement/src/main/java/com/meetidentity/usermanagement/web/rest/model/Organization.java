package com.meetidentity.usermanagement.web.rest.model;

public class Organization {
	private Long id;
	private String name;
	private String displayName;
	private String industry;
	private String issuerIdentificationNumber;
	private String agencyCode;
	private String issuingAuthority;
	private String addressLine1;
	private String addressLine2;
	private String siteCd;
	private Boolean enableSchedulerInLoginScreen;

	public Organization() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getIssuerIdentificationNumber() {
		return issuerIdentificationNumber;
	}

	public void setIssuerIdentificationNumber(String issuerIdentificationNumber) {
		this.issuerIdentificationNumber = issuerIdentificationNumber;
	}

	public String getAgencyCode() {
		return agencyCode;
	}

	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}

	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getSiteCd() {
		return siteCd;
	}

	public void setSiteCd(String siteCd) {
		this.siteCd = siteCd;
	}

	public Boolean getEnableSchedulerInLoginScreen() {
		return enableSchedulerInLoginScreen;
	}

	public void setEnableSchedulerInLoginScreen(Boolean enableSchedulerInLoginScreen) {
		this.enableSchedulerInLoginScreen = enableSchedulerInLoginScreen;
	}

}
