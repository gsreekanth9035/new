package com.meetidentity.usermanagement.web.rest.model;

public class UserIdentityData {
	private String labelName;
	private Object value;

	public String getLabelName() {
		return labelName;
	}

	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
