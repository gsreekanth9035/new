package com.meetidentity.usermanagement.web.rest.model;

import java.util.List;

public class WFMobileIDStepOnboardAndEnrollConfig {
	private List<OrganizationIdentity> trustedExistingIdentities;
	private Boolean scanQrCode;
	private Boolean tapAndReadThroughNFC;
	private Boolean captureFrontDocument;
	private Boolean captureBackDocument;

	public List<OrganizationIdentity> getTrustedExistingIdentities() {
		return trustedExistingIdentities;
	}

	public void setTrustedExistingIdentities(List<OrganizationIdentity> trustedExistingIdentities) {
		this.trustedExistingIdentities = trustedExistingIdentities;
	}

	public Boolean getScanQrCode() {
		return scanQrCode;
	}

	public void setScanQrCode(Boolean scanQrCode) {
		this.scanQrCode = scanQrCode;
	}

	public Boolean getTapAndReadThroughNFC() {
		return tapAndReadThroughNFC;
	}

	public void setTapAndReadThroughNFC(Boolean tapAndReadThroughNFC) {
		this.tapAndReadThroughNFC = tapAndReadThroughNFC;
	}

	public Boolean getCaptureFrontDocument() {
		return captureFrontDocument;
	}

	public void setCaptureFrontDocument(Boolean captureFrontDocument) {
		this.captureFrontDocument = captureFrontDocument;
	}

	public Boolean getCaptureBackDocument() {
		return captureBackDocument;
	}

	public void setCaptureBackDocument(Boolean captureBackDocument) {
		this.captureBackDocument = captureBackDocument;
	}

}
