package com.meetidentity.usermanagement.web.rest.model;

public class OrganizationBrandingInfo {

	private String name;

	private String description;

	private String displayName;

	private String altName;

	private String loginLogo;

	private String loginLogoWidth;

	private String loginLogoHeight;

	private String sidebarLogo;

	private String sidebarLogoWidth;

	private String sidebarLogoHeight;

	private String primaryColorCode;

	private String secondaryColorCode;

	private String tertiaryColorCode;

	private String quaternaryColorCode;

	private String quinaryColorCode;
	
	private String privacyPolicy;
	
	private boolean hideServiceName;
	
	private long orgId;
	
	private Boolean enableSchedulerInLoginScreen;
	
	private String favIcon;
	
	private String poweredBy;
	private String loginPageTitle;
	private String mobileAppName;
	private String mobileServicesAppName;
	private String contactUsSupportEmail;;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getAltName() {
		return altName;
	}

	public void setAltName(String altName) {
		this.altName = altName;
	}

	public String getLoginLogo() {
		return loginLogo;
	}

	public void setLoginLogo(String loginLogo) {
		this.loginLogo = loginLogo;
	}

	public String getSidebarLogo() {
		return sidebarLogo;
	}

	public void setSidebarLogo(String sidebarLogo) {
		this.sidebarLogo = sidebarLogo;
	}

	public String getLoginLogoWidth() {
		return loginLogoWidth;
	}

	public void setLoginLogoWidth(String loginLogoWidth) {
		this.loginLogoWidth = loginLogoWidth;
	}

	public String getLoginLogoHeight() {
		return loginLogoHeight;
	}

	public void setLoginLogoHeight(String loginLogoHeight) {
		this.loginLogoHeight = loginLogoHeight;
	}

	public String getSidebarLogoWidth() {
		return sidebarLogoWidth;
	}

	public void setSidebarLogoWidth(String sidebarLogoWidth) {
		this.sidebarLogoWidth = sidebarLogoWidth;
	}

	public String getSidebarLogoHeight() {
		return sidebarLogoHeight;
	}

	public void setSidebarLogoHeight(String sidebarLogoHeight) {
		this.sidebarLogoHeight = sidebarLogoHeight;
	}

	public String getPrimaryColorCode() {
		return primaryColorCode;
	}

	public void setPrimaryColorCode(String primaryColorCode) {
		this.primaryColorCode = primaryColorCode;
	}

	public String getSecondaryColorCode() {
		return secondaryColorCode;
	}

	public void setSecondaryColorCode(String secondaryColorCode) {
		this.secondaryColorCode = secondaryColorCode;
	}

	public String getTertiaryColorCode() {
		return tertiaryColorCode;
	}

	public void setTertiaryColorCode(String tertiaryColorCode) {
		this.tertiaryColorCode = tertiaryColorCode;
	}

	public String getQuaternaryColorCode() {
		return quaternaryColorCode;
	}

	public void setQuaternaryColorCode(String quaternaryColorCode) {
		this.quaternaryColorCode = quaternaryColorCode;
	}

	public String getQuinaryColorCode() {
		return quinaryColorCode;
	}

	public void setQuinaryColorCode(String quinaryColorCode) {
		this.quinaryColorCode = quinaryColorCode;
	}

	public String getPrivacyPolicy() {
		return privacyPolicy;
	}

	public void setPrivacyPolicy(String privacyPolicy) {
		this.privacyPolicy = privacyPolicy;
	}

	public boolean getHideServiceName() {
		return hideServiceName;
	}

	public void setHideServiceName(boolean hideServiceName) {
		this.hideServiceName = hideServiceName;
	}

	public long getOrgId() {
		return orgId;
	}

	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}

	public Boolean getEnableSchedulerInLoginScreen() {
		return enableSchedulerInLoginScreen;
	}

	public void setEnableSchedulerInLoginScreen(Boolean enableSchedulerInLoginScreen) {
		this.enableSchedulerInLoginScreen = enableSchedulerInLoginScreen;
	}

	public String getFavIcon() {
		return favIcon;
	}

	public void setFavIcon(String favIcon) {
		this.favIcon = favIcon;
	}

	public String getPoweredBy() {
		return poweredBy;
	}

	public void setPoweredBy(String poweredBy) {
		this.poweredBy = poweredBy;
	}

	public String getLoginPageTitle() {
		return loginPageTitle;
	}

	public void setLoginPageTitle(String loginPageTitle) {
		this.loginPageTitle = loginPageTitle;
	}

	public String getMobileAppName() {
		return mobileAppName;
	}

	public void setMobileAppName(String mobileAppName) {
		this.mobileAppName = mobileAppName;
	}

	public String getMobileServicesAppName() {
		return mobileServicesAppName;
	}

	public void setMobileServicesAppName(String mobileServicesAppName) {
		this.mobileServicesAppName = mobileServicesAppName;
	}

	public String getContactUsSupportEmail() {
		return contactUsSupportEmail;
	}

	public void setContactUsSupportEmail(String contactUsSupportEmail) {
		this.contactUsSupportEmail = contactUsSupportEmail;
	}
	
}
