package com.meetidentity.usermanagement.web.rest.model;

public class PushNotificationDevice {

	public String userName;
	public String customData;
	public String type;
	public String platformArn;
	public String principle;
	public String gcmRegistrationId;
	public String platformAppArn;
	public String appEndPointArn;
	public String uimsId;
	public String userDevicePublicKey;
	public String encryptionAlgorithm;
	public Challenge challenge;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCustomData() {
		return customData;
	}

	public void setCustomData(String customData) {
		this.customData = customData;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPlatformArn() {
		return platformArn;
	}

	public void setPlatformArn(String platformArn) {
		this.platformArn = platformArn;
	}

	public String getPrinciple() {
		return principle;
	}

	public void setPrinciple(String principle) {
		this.principle = principle;
	}

	public String getGcmRegistrationId() {
		return gcmRegistrationId;
	}

	public void setGcmRegistrationId(String gcmRegistrationId) {
		this.gcmRegistrationId = gcmRegistrationId;
	}

	public String getPlatformAppArn() {
		return platformAppArn;
	}

	public void setPlatformAppArn(String platformAppArn) {
		this.platformAppArn = platformAppArn;
	}

	public String getAppEndPointArn() {
		return appEndPointArn;
	}

	public void setAppEndPointArn(String appEndPointArn) {
		this.appEndPointArn = appEndPointArn;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public String getUserDevicePublicKey() {
		return userDevicePublicKey;
	}

	public void setUserDevicePublicKey(String userDevicePublicKey) {
		this.userDevicePublicKey = userDevicePublicKey;
	}

	public String getEncryptionAlgorithm() {
		return encryptionAlgorithm;
	}

	public void setEncryptionAlgorithm(String encryptionAlgorithm) {
		this.encryptionAlgorithm = encryptionAlgorithm;
	}

	public Challenge getChallenge() {
		return challenge;
	}

	public void setChallenge(Challenge challenge) {
		this.challenge = challenge;
	}

}
