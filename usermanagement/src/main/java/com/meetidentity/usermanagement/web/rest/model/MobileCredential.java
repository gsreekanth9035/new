package com.meetidentity.usermanagement.web.rest.model;

public class MobileCredential {
	private Long wfMobileIDStepIdentityConfigId;
	private Long credentialTemplateID;
	private Long userId;

	private String friendlyName;
	private String visualIdNumber;
	private Boolean softOTP;
	private String otpConfigStatus;
	private String OtpCode;
	private Boolean pkiCertificatesEnabled;
	private int noOfCertificates;
	private Boolean pushVerify;
	private Boolean fido2;
	private String pushVerifyNumber;
	private WFMobileIDIdentitySoftOTPConfig wfMobileIDIdentitySoftOTPConfig;
	private Long wfMobileIDVisualConfigId;
	private String pushCredentialId;

	public Long getWfMobileIDStepIdentityConfigId() {
		return wfMobileIDStepIdentityConfigId;
	}

	public void setWfMobileIDStepIdentityConfigId(Long wfMobileIDStepIdentityConfigId) {
		this.wfMobileIDStepIdentityConfigId = wfMobileIDStepIdentityConfigId;
	}

	public Long getCredentialTemplateID() {
		return credentialTemplateID;
	}

	public void setCredentialTemplateID(Long credentialTemplateID) {
		this.credentialTemplateID = credentialTemplateID;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFriendlyName() {
		return friendlyName;
	}

	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}

	public String getVisualIdNumber() {
		return visualIdNumber;
	}

	public void setVisualIdNumber(String visualIdNumber) {
		this.visualIdNumber = visualIdNumber;
	}

	public Boolean getSoftOTP() {
		return softOTP;
	}

	public void setSoftOTP(Boolean softOTP) {
		this.softOTP = softOTP;
	}

	public String getOtpCode() {
		return OtpCode;
	}

	public void setOtpCode(String otpCode) {
		OtpCode = otpCode;
	}

	public Boolean getPkiCertificatesEnabled() {
		return pkiCertificatesEnabled;
	}

	public void setPkiCertificatesEnabled(Boolean pkiCertificatesEnabled) {
		this.pkiCertificatesEnabled = pkiCertificatesEnabled;
	}

	public int getNoOfCertificates() {
		return noOfCertificates;
	}

	public void setNoOfCertificates(int noOfCertificates) {
		this.noOfCertificates = noOfCertificates;
	}

	public Boolean getPushVerify() {
		return pushVerify;
	}

	public void setPushVerify(Boolean pushVerify) {
		this.pushVerify = pushVerify;
	}

	public Boolean isFido2() {
		return fido2;
	}

	public void setFido2(Boolean fido2) {
		this.fido2 = fido2;
	}

	public String getPushVerifyNumber() {
		return pushVerifyNumber;
	}

	public void setPushVerifyNumber(String pushVerifyNumber) {
		this.pushVerifyNumber = pushVerifyNumber;
	}

	public WFMobileIDIdentitySoftOTPConfig getWfMobileIDIdentitySoftOTPConfig() {
		return wfMobileIDIdentitySoftOTPConfig;
	}

	public void setWfMobileIDIdentitySoftOTPConfig(WFMobileIDIdentitySoftOTPConfig wfMobileIDIdentitySoftOTPConfig) {
		this.wfMobileIDIdentitySoftOTPConfig = wfMobileIDIdentitySoftOTPConfig;
	}

	public String getOtpConfigStatus() {
		return otpConfigStatus;
	}

	public void setOtpConfigStatus(String otpConfigStatus) {
		this.otpConfigStatus = otpConfigStatus;
	}

	public Long getWfMobileIDVisualConfigId() {
		return wfMobileIDVisualConfigId;
	}

	public void setWfMobileIDVisualConfigId(Long wfMobileIDVisualConfigId) {
		this.wfMobileIDVisualConfigId = wfMobileIDVisualConfigId;
	}

	public String getPushCredentialId() {
		return pushCredentialId;
	}

	public void setPushCredentialId(String pushCredentialId) {
		this.pushCredentialId = pushCredentialId;
	}

}
