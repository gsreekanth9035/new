package com.meetidentity.usermanagement.web.rest.model;

import java.time.Instant;
import java.util.List;

/**
 * 
 * @author Kiranmayi
 *
 */
public class UserRequest {

	private String name;
	private String organisationName;
	private List<String> status;
	private String enrolledBy;
	private Instant startDate = Instant.now();
	private Instant endDate = Instant.now();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrganisationName() {
		return organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	public String getEnrolledBy() {
		return enrolledBy;
	}

	public void setEnrolledBy(String enrolledBy) {
		this.enrolledBy = enrolledBy;
	}

	public Instant getStartDate() {
		return startDate;
	}

	public void setStartDate(Instant startDate) {
		this.startDate = startDate;
	}

	public Instant getEndDate() {
		return endDate;
	}

	public void setEndDate(Instant endDate) {
		this.endDate = endDate;
	}

	public List<String> getStatus() {
		return status;
	}

	public void setStatus(List<String> status) {
		this.status = status;
	}
}
