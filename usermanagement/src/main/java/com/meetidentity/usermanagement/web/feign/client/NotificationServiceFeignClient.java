package com.meetidentity.usermanagement.web.feign.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.meetidentity.usermanagement.web.rest.model.notification.DeliverReqBean;
import com.meetidentity.usermanagement.web.rest.model.notification.NotificationResult;

@FeignClient(name = "notificationservice", url = "http://notificationservice:10008")
//@FeignClient(name="notificationservice", url="http://${application.notificationservice.host}:${application.notificationservice.port}")
public interface NotificationServiceFeignClient {

	@PostMapping("/notify/deliver")
	public NotificationResult deliver(@RequestHeader HttpHeaders httpHeaders,
			@RequestBody DeliverReqBean deliverReqBean);
	
}
