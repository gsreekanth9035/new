package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.IdentityProviderClientService;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/idp-client")
public class IdentityProviderClientResource {

	@Autowired
	private IdentityProviderClientService identityProviderClientService;

	@PutMapping("/redirect-uri")
	public void addRedirectURIs(@PathVariable("organizationName") String organizationName,
			@RequestBody List<String> redirectURIs) {
		identityProviderClientService.addRedirectURIs(organizationName, redirectURIs);
	}
	
	@DeleteMapping("/redirect-uri")
	public void deleteRedirectURIs(@PathVariable("organizationName") String organizationName,
			@RequestParam(name="redirectURI") String redirectURI) {
		identityProviderClientService.deleteRedirectURIs(organizationName, redirectURI);
	}
	
	@GetMapping("/redirect-uri")
	public List<String> getRedirectURIs(@PathVariable("organizationName") String organizationName) {
		return identityProviderClientService.getRedirectURIs(organizationName);
	}

}
