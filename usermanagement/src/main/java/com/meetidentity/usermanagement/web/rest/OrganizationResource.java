package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.OrganizationService;
import com.meetidentity.usermanagement.web.rest.model.EnrollmentApproveRejectReasons;
import com.meetidentity.usermanagement.web.rest.model.Organization;
import com.meetidentity.usermanagement.web.rest.model.RegistrationConfigDetails;

@RestController
@RequestMapping("/api/v1/organizations")
public class OrganizationResource {
	@Autowired
	private OrganizationService organizationService;

	@GetMapping("/{name}")
	public Organization getOrgByName(@PathVariable("name") String organizationName) {
		return organizationService.getOrgByName(organizationName);
	}
	
	@GetMapping
	public List<Organization> getAllOrganizatins() {
		return organizationService.getAllOrganizations();
	}
	
	@GetMapping("/{organizationName}/registrationConfigDetails/identitymodel/{id}")
//	@Cacheable(value="RegistrationConfig")
	public RegistrationConfigDetails getRegistrationConfigDetailsByOrgName(@PathVariable("organizationName") String organizationName, @PathVariable("id") long id) {
		return organizationService.getRegistrationConfigDetailsByOrgName(organizationName,id);
	}
	
	@GetMapping("/{organizationName}/getApprovalRejectionReasons")
	public  EnrollmentApproveRejectReasons getApprovalRejectionReasons(@PathVariable("organizationName") String organizationName) {
		return organizationService.getApprovalRejectionReasons(organizationName);
	}
	
	@GetMapping("/{organizationName}/transparent-image-enabled-check")
	public ResponseEntity<Boolean> checkTransparentImageEnabled(@PathVariable("organizationName") String organizationName) {
		return organizationService.checkTransparentImageEnabled(organizationName);
	}

}
