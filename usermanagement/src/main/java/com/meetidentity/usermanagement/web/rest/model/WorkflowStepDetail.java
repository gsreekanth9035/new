package com.meetidentity.usermanagement.web.rest.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class WorkflowStepDetail {

	private List<WFStepGenericConfig> wfStepGenericConfigs = new ArrayList<>();
	private List<WFStepRegistrationConfig> wfStepRegistrationConfigs = new ArrayList<>();
	private List<WFStepApprovalGroupConfig> wfStepApprovalGroupConfigs = new ArrayList<>();
	private List<WFStepCertConfig> wfStepCertConfigs = new ArrayList<>();
	private List<WfStepGroupConfig> wfStepChipEncodeVIDPrintGroupConfigs = new ArrayList<>();
	private List<WfStepGroupConfig> wfStepChipEncodeGroupConfigs = new ArrayList<>();
	private List<WfStepGroupConfig> wfStepPrintGroupConfigs = new ArrayList<>();
	private List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdentityConfigs = new ArrayList<>();
	private List<Group> wfStepAdjudicationGroupConfigs = new ArrayList<>();
	private List<OrganizationIdentity> wfMobileIDStepTrustedIdentities = new ArrayList<>();
	private WfStepAppletLoadingInfo wfStepAppletLoadingInfo;

	public List<WFStepGenericConfig> getWfStepGenericConfigs() {
		return wfStepGenericConfigs;
	}

	public void setWfStepGenericConfigs(List<WFStepGenericConfig> wfStepGenericConfigs) {
		this.wfStepGenericConfigs = wfStepGenericConfigs;
	}

	public List<WFStepRegistrationConfig> getWfStepRegistrationConfigs() {
		return wfStepRegistrationConfigs;
	}

	public void setWfStepRegistrationConfigs(List<WFStepRegistrationConfig> wfStepRegistrationConfigs) {
		this.wfStepRegistrationConfigs = wfStepRegistrationConfigs;
	}

	public List<WFStepCertConfig> getWfStepCertConfigs() {
		return wfStepCertConfigs;
	}

	public void setWfStepCertConfigs(List<WFStepCertConfig> wfStepCertConfigs) {
		this.wfStepCertConfigs = wfStepCertConfigs;
	}

	public List<WFStepApprovalGroupConfig> getWfStepApprovalGroupConfigs() {
		return wfStepApprovalGroupConfigs;
	}

	public void setWfStepApprovalGroupConfigs(List<WFStepApprovalGroupConfig> wfStepApprovalGroupConfigs) {
		this.wfStepApprovalGroupConfigs = wfStepApprovalGroupConfigs;
	}

	public List<WfStepGroupConfig> getWfStepPrintGroupConfigs() {
		return wfStepPrintGroupConfigs;
	}

	public void setWfStepPrintGroupConfigs(List<WfStepGroupConfig> wfStepPrintGroupConfigs) {
		this.wfStepPrintGroupConfigs = wfStepPrintGroupConfigs;
	}

	public List<WFMobileIDStepIdentityConfig> getWfMobileIDStepIdentityConfigs() {
		return wfMobileIDStepIdentityConfigs;
	}

	public void setWfMobileIDStepIdentityConfigs(List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdentityConfigs) {
		this.wfMobileIDStepIdentityConfigs = wfMobileIDStepIdentityConfigs;
	}

	public List<Group> getWfStepAdjudicationGroupConfigs() {
		return wfStepAdjudicationGroupConfigs;
	}

	public void setWfStepAdjudicationGroupConfigs(List<Group> wfStepAdjudicationGroupConfigs) {
		this.wfStepAdjudicationGroupConfigs = wfStepAdjudicationGroupConfigs;
	}

	public List<OrganizationIdentity> getWfMobileIDStepTrustedIdentities() {
		return wfMobileIDStepTrustedIdentities;
	}

	public void setWfMobileIDStepTrustedIdentities(List<OrganizationIdentity> wfMobileIDStepTrustedIdentities) {
		this.wfMobileIDStepTrustedIdentities = wfMobileIDStepTrustedIdentities;
	}

	public List<WfStepGroupConfig> getWfStepChipEncodeVIDPrintGroupConfigs() {
		return wfStepChipEncodeVIDPrintGroupConfigs;
	}

	public void setWfStepChipEncodeVIDPrintGroupConfigs(List<WfStepGroupConfig> wfStepChipEncodeVIDPrintGroupConfigs) {
		this.wfStepChipEncodeVIDPrintGroupConfigs = wfStepChipEncodeVIDPrintGroupConfigs;
	}

	public List<WfStepGroupConfig> getWfStepChipEncodeGroupConfigs() {
		return wfStepChipEncodeGroupConfigs;
	}

	public void setWfStepChipEncodeGroupConfigs(List<WfStepGroupConfig> wfStepChipEncodeGroupConfigs) {
		this.wfStepChipEncodeGroupConfigs = wfStepChipEncodeGroupConfigs;
	}

	public WfStepAppletLoadingInfo getWfStepAppletLoadingInfo() {
		return wfStepAppletLoadingInfo;
	}

	public void setWfStepAppletLoadingInfo(WfStepAppletLoadingInfo wfStepAppletLoadingInfo) {
		this.wfStepAppletLoadingInfo = wfStepAppletLoadingInfo;
	}

}
