package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;

import com.meetidentity.usermanagement.domain.DrivingLicenseRestrictionEntity;

public class UserDrivingLicense  {

	private String licenseNumber;

	private String documentDescriminator;

	private String issuingAuthority;

	private String licenseCategory;
	
	private Date dateOfIssuance;
	
	private Date dateOfExpiry;
	
	private String vehicleClass;

	private String restrictions;
	
	private String endorsements;
	
	private String vehicleClassification;
	
	private long restriction_id;

	private long vehicle_classification_id;
	
	private Date under18Until;
	
	private Date under19Until;
	
	private Date under21Until;
	
	private String auditInformation;
	private String uimsID;


	public UserDrivingLicense(String licenseNumber, String documentDescriminator, String issuingAuthority,
			String licenseCategory, Date dateOfIssuance, Date dateOfExpiry, String vehicleClass, String restrictions,
			String endorsements, String vehicleClassification, long restriction_id, long vehicle_classification_id,
			Date under18Until, Date under19Until, Date under21Until, String auditInformation, String uimsID) {
		super();
		this.licenseNumber = licenseNumber;
		this.documentDescriminator = documentDescriminator;
		this.issuingAuthority = issuingAuthority;
		this.licenseCategory = licenseCategory;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
		this.vehicleClass = vehicleClass;
		this.restrictions = restrictions;
		this.endorsements = endorsements;
		this.vehicleClassification = vehicleClassification;
		this.restriction_id = restriction_id;
		this.vehicle_classification_id = vehicle_classification_id;
		this.under18Until = under18Until;
		this.under19Until = under19Until;
		this.under21Until = under21Until;
		this.auditInformation = auditInformation;
		this.uimsID = uimsID;
	}


	public UserDrivingLicense() {
		// TODO Auto-generated constructor stub
	}
	

	public UserDrivingLicense(String uimsId2, String licenseNumber2, String documentDescriminator2,
			String issuingAuthority2, String licenseCategory2, Date dateOfIssuance2, Date dateOfExpiry2,
			DrivingLicenseRestrictionEntity restrictions2, String endorsements2,
			String vehicleClassification2, Date under18Until2, Date under19Until2,
			Date under21Until2, String auditInformation2) {
		super();
		this.licenseNumber = licenseNumber2;
		this.documentDescriminator = documentDescriminator2;
		this.issuingAuthority = issuingAuthority2;
		this.licenseCategory = licenseCategory2;
		this.dateOfIssuance = dateOfIssuance2;
		this.dateOfExpiry = dateOfExpiry2;
		this.restrictions = restrictions2.getType();
		this.endorsements = endorsements2;
		this.vehicleClass = vehicleClassification2;
		this.under18Until = under18Until2;
		this.under19Until = under19Until2;
		this.under21Until = under21Until2;
		this.auditInformation = auditInformation2;
		this.uimsID = uimsId2;
	}


	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getDocumentDescriminator() {
		return documentDescriminator;
	}

	public void setDocumentDescriminator(String documentDescriminator) {
		this.documentDescriminator = documentDescriminator;
	}

	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public String getLicenseCategory() {
		return licenseCategory;
	}

	public void setLicenseCategory(String licenseCategory) {
		this.licenseCategory = licenseCategory;
	}

	public Date getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Date dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getRestrictions() {
		return restrictions;
	}

	public void setRestrictions(String restrictions) {
		this.restrictions = restrictions;
	}

	public String getEndorsements() {
		return endorsements;
	}

	public void setEndorsements(String endorsements) {
		this.endorsements = endorsements;
	}

	public String getVehicleClassification() {
		return vehicleClassification;
	}

	public void setVehicleClassification(String vehicleClassification) {
		this.vehicleClassification = vehicleClassification;
	}

	public Date getUnder18Until() {
		return under18Until;
	}

	public void setUnder18Until(Date under18Until) {
		this.under18Until = under18Until;
	}

	public Date getUnder19Until() {
		return under19Until;
	}

	public void setUnder19Until(Date under19Until) {
		this.under19Until = under19Until;
	}

	public Date getUnder21Until() {
		return under21Until;
	}

	public void setUnder21Until(Date under21Until) {
		this.under21Until = under21Until;
	}

	public String getAuditInformation() {
		return auditInformation;
	}

	public void setAuditInformation(String auditInformation) {
		this.auditInformation = auditInformation;
	}

	public String getVehicleClass() {
		return vehicleClass;
	}

	public void setVehicleClass(String vehicleClass) {
		this.vehicleClass = vehicleClass;
	}

	public String getUimsID() {
		return uimsID;
	}

	public void setUimsID(String uimsID) {
		this.uimsID = uimsID;
	}
	
	public long getRestriction_id() {
		return restriction_id;
	}

	public void setRestriction_id(long restriction_id) {
		this.restriction_id = restriction_id;
	}

	public long getVehicle_classification_id() {
		return vehicle_classification_id;
	}

	public void setVehicle_classification_id(long vehicle_classification_id) {
		this.vehicle_classification_id = vehicle_classification_id;
	}
	

}
