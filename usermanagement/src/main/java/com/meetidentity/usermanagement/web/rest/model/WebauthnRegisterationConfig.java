package com.meetidentity.usermanagement.web.rest.model;

public class WebauthnRegisterationConfig {

	public String base64url;
	public String qrcode;

	public String getBase64url() {
		return base64url;
	}

	public void setBase64url(String base64url) {
		this.base64url = base64url;
	}

	public String getQrcode() {
		return qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}

}
