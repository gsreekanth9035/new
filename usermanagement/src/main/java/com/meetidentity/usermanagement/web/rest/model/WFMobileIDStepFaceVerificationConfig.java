package com.meetidentity.usermanagement.web.rest.model;

public class WFMobileIDStepFaceVerificationConfig {
	private Boolean saveCapturedFaceForVisualID;
	private String cropSize;

	public Boolean getSaveCapturedFaceForVisualID() {
		return saveCapturedFaceForVisualID;
	}

	public void setSaveCapturedFaceForVisualID(Boolean saveCapturedFaceForVisualID) {
		this.saveCapturedFaceForVisualID = saveCapturedFaceForVisualID;
	}

	public String getCropSize() {
		return cropSize;
	}

	public void setCropSize(String cropSize) {
		this.cropSize = cropSize;
	}

}
