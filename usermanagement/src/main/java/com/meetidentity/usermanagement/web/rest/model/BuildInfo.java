package com.meetidentity.usermanagement.web.rest.model;

public class BuildInfo {

	private String artifactId;
	private String version;
	private String buildNumber;
	private String commitId;
	private String branch;
	private String prId;
	private String prDestinationBranch;

	public BuildInfo() {
		super();
	}

	public BuildInfo(String artifactId, String version, String buildNumber, String commitId, String branch, String prId,
			String prDestinationBranch) {
		super();
		this.artifactId = artifactId;
		this.version = version;
		this.buildNumber = buildNumber;
		this.commitId = commitId;
		this.branch = branch;
		this.prId = prId;
		this.prDestinationBranch = prDestinationBranch;
	}

	public String getArtifactId() {
		return artifactId;
	}

	public void setArtifactId(String artifactId) {
		this.artifactId = artifactId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getBuildNumber() {
		return buildNumber;
	}

	public void setBuildNumber(String buildNumber) {
		this.buildNumber = buildNumber;
	}

	public String getCommitId() {
		return commitId;
	}

	public void setCommitId(String commitId) {
		this.commitId = commitId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getPrId() {
		return prId;
	}

	public void setPrId(String prId) {
		this.prId = prId;
	}

	public String getPrDestinationBranch() {
		return prDestinationBranch;
	}

	public void setPrDestinationBranch(String prDestinationBranch) {
		this.prDestinationBranch = prDestinationBranch;
	}
}
