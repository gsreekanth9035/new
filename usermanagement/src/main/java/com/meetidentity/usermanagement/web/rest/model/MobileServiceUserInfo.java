package com.meetidentity.usermanagement.web.rest.model;

public class MobileServiceUserInfo {

	private String location;
	private String locationIp;
	private String email;
	private String uimsId;
	private String deviceUuid;
	private String name;
	private String time;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocationIp() {
		return locationIp;
	}

	public void setLocationIp(String locationIp) {
		this.locationIp = locationIp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public String getDeviceUuid() {
		return deviceUuid;
	}

	public void setDeviceUuid(String deviceUuid) {
		this.deviceUuid = deviceUuid;
	}

	
	
}
