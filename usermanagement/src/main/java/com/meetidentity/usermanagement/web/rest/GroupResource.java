package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.keycloak.domain.GroupRepresentation;
import com.meetidentity.usermanagement.security.SpringSecurityAuditorAware;
import com.meetidentity.usermanagement.service.KeycloakService;
import com.meetidentity.usermanagement.service.WorkflowService;
import com.meetidentity.usermanagement.web.rest.model.Group;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/groups")
public class GroupResource {
	
	@Autowired
	private KeycloakService keycloakService;
	
	@Autowired
	private WorkflowService workflowService;
	
	@Autowired
	private SpringSecurityAuditorAware springSecurityAuditorAware;

	@GetMapping
	public List<Group> getAllGroups(@PathVariable("organizationName") String organizationName) {
		return keycloakService.getAllGroups(organizationName);
	}

	@GetMapping("/workflowGroups")
	public List<Group> getGroupsFoWorkflow(@PathVariable("organizationName") String organizationName) {
		return keycloakService.getGroupsForWorkflow(organizationName);
	}
	
	@PostMapping()
	public ResponseEntity<Void> addGroup(@PathVariable("organizationName") String organizationName, @RequestBody Group group) {
		return keycloakService.addGroup(organizationName, group);
	}
	
	@PutMapping()
	public ResponseEntity<Void> updateGroup(@PathVariable("organizationName") String organizationName, @RequestBody Group group) {
		return keycloakService.updateGroup(group,organizationName);
	}
	
	@GetMapping("/groupsWithWorkflow")
	public List<Group> getGroupsWithWorkflow(@PathVariable("organizationName") String organizationName) {
		return workflowService.getGroupsWithWorkflow(organizationName);
	}
	
	@GetMapping("/{groupId}")
	public Group getGroupById(@PathVariable("organizationName") String organizationName, @PathVariable("groupId") String groupId) {
		return keycloakService.getGroupById(organizationName, groupId);
	}
	
	@DeleteMapping("/{groupId}")
    public  ResponseEntity<String> deleteGroup(@PathVariable("organizationName") String organizationName, @PathVariable("groupId") String groupId) {
         return keycloakService.deleteGroup(organizationName, groupId); 
    }
	
	@GetMapping("/users/{UserKCLKID}")
	public Group[] getGroupsOfUserKCLCId(@PathVariable("organizationName") String organizationName, @PathVariable("UserKCLKID") String UserKCLKID) {
		 List<GroupRepresentation> groupRepresentations = keycloakService.getGroupsByUserID(UserKCLKID, organizationName);
		 Group[] groups = new Group[groupRepresentations.size()];
		if (groupRepresentations != null) {
			int i = 0;
			for (GroupRepresentation groupRepresentation : groupRepresentations) {
				groups[i] = new Group(groupRepresentation.getId(), groupRepresentation.getName());
				i++;
			}
		}
		 return groups;
	}
}
