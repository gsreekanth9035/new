package com.meetidentity.usermanagement.web.rest.model;

import java.util.ArrayList;
import java.util.List;

public class ExpressEnrollForm {

	private String userName;
	private String groupID;
	private String groupName;

	private String status;
	private boolean portalAccessEnabled = false;

	private String roleID;
	private String roleName;

	private String userStatus;

	private UserAppearance userAppearance;

	private UserAddress userAddress;

	private UserAttribute userAttribute;

	private List<UserBiometric> userBiometrics = new ArrayList<>();

	private String operatorName;

	public ExpressEnrollForm() {

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public boolean isPortalAccessEnabled() {
		return portalAccessEnabled;
	}

	public void setPortalAccessEnabled(boolean portalAccessEnabled) {
		this.portalAccessEnabled = portalAccessEnabled;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public UserAppearance getUserAppearance() {
		return userAppearance;
	}

	public void setUserAppearance(UserAppearance userAppearance) {
		this.userAppearance = userAppearance;
	}

	public UserAttribute getUserAttribute() {
		return userAttribute;
	}

	public void setUserAttribute(UserAttribute userAttribute) {
		this.userAttribute = userAttribute;
	}

	public List<UserBiometric> getUserBiometrics() {
		return userBiometrics;
	}

	public void setUserBiometrics(List<UserBiometric> userBiometrics) {
		this.userBiometrics = userBiometrics;
	}

	public UserAddress getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(UserAddress userAddress) {
		this.userAddress = userAddress;
	}

	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	public String getRoleID() {
		return roleID;
	}

	public void setRoleID(String roleID) {
		this.roleID = roleID;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

}
