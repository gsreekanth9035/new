package com.meetidentity.usermanagement.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.UserPairDeviceInfoService;
import com.meetidentity.usermanagement.web.rest.model.WebauthnRegisterationConfig;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}")
public class UserWebAuthnConfigResource {
	
	@Autowired
	private UserPairDeviceInfoService userPairDeviceInfoService;
	
	
	@GetMapping("/users/{userName}/execute-webauthn")
	public WebauthnRegisterationConfig executeWebauthn(@PathVariable("organizationName") String organizationName,@PathVariable("userName") String userName)  {
		 try {
			return  userPairDeviceInfoService.executeWebauthn(organizationName, userName);
		} catch (Exception e) {
			return null;
		}
	}

}
