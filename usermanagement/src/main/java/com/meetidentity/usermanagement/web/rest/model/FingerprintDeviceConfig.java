package com.meetidentity.usermanagement.web.rest.model;

import com.meetidentity.usermanagement.domain.OrganizationEntity;

public class FingerprintDeviceConfig {
	
	private Long id;
	private OrganizationEntity organization;
	private String deviceName;
	private String deviceManufacturer;
	private String captureType;
	private Boolean rollSupport;
	private Boolean flatSupport;
	private Boolean ansiTemplateSupport;
	private Boolean isoTemplateSupport;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public OrganizationEntity getOrganization() {
		return organization;
	}
	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getDeviceManufacturer() {
		return deviceManufacturer;
	}
	public void setDeviceManufacturer(String deviceManufacturer) {
		this.deviceManufacturer = deviceManufacturer;
	}
	public String getCaptureType() {
		return captureType;
	}
	public void setCaptureType(String captureType) {
		this.captureType = captureType;
	}
	public Boolean getRollSupport() {
		return rollSupport;
	}
	public void setRollSupport(Boolean rollSupport) {
		this.rollSupport = rollSupport;
	}
	public Boolean getFlatSupport() {
		return flatSupport;
	}
	public void setFlatSupport(Boolean flatSupport) {
		this.flatSupport = flatSupport;
	}
	public Boolean getAnsiTemplateSupport() {
		return ansiTemplateSupport;
	}
	public void setAnsiTemplateSupport(Boolean ansiTemplateSupport) {
		this.ansiTemplateSupport = ansiTemplateSupport;
	}
	public Boolean getIsoTemplateSupport() {
		return isoTemplateSupport;
	}
	public void setIsoTemplateSupport(Boolean isoTemplateSupport) {
		this.isoTemplateSupport = isoTemplateSupport;
	}
}
