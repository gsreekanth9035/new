package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.domain.RoleEntity;
import com.meetidentity.usermanagement.service.KeycloakService;
import com.meetidentity.usermanagement.service.RoleService;
import com.meetidentity.usermanagement.service.WorkflowService;
import com.meetidentity.usermanagement.web.rest.model.Role;
import com.meetidentity.usermanagement.web.rest.model.Workflow;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/roles")
public class RoleResource {

	@Autowired
	private RoleService roleService;
	
	@Autowired
	private WorkflowService workflowService;	
	
	@Autowired
	private KeycloakService keycloakService;
	
	@PostMapping
	public ResponseEntity<Void> createRole(@PathVariable("organizationName") String organizationName, @RequestBody Role role) {
		roleService.createRole(organizationName, role);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping
	public List<Role> getRoles(@PathVariable("organizationName") String organizationName) {
		return roleService.getRoles(organizationName);
	}
	
	@GetMapping("/names")
	public List<String> getRoleNames(@PathVariable("organizationName") String organizationName) {
		return roleService.getRoleNames(organizationName);
	}

	@GetMapping("/fromAuthService")
	public List<Role> getRolesFromAuthenticationService(@PathVariable("organizationName") String organizationName) {
		return keycloakService.getRolesOfClientFromAuthenticationService(organizationName);
	}
	
	@GetMapping("/{roleName}")
	public Role getRoleByName(@PathVariable("organizationName") String organizationName, @PathVariable("roleName") String roleName) {
		return roleService.getRoleByName(organizationName, roleName);
	}
	
	@GetMapping("/{roleName}/workflow")
	Workflow getWorkflowByRole(@PathVariable("organizationName") String organizationName, @PathVariable("roleName") String roleName) {
		return workflowService.getWorkflowByRole(organizationName, roleName);
	}
	
	@GetMapping("/{roleNames}/color")
	public List<Role> getRoleWithColor(@PathVariable("roleNames") List<String> listOfRoles,@PathVariable("organizationName")  String organization){
		return roleService.getRoleWithColor(listOfRoles,organization);
	}

}
