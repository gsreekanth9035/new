package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.WorkflowService;
import com.meetidentity.usermanagement.web.rest.model.OrgIdentityFieldIdNamePair;
import com.meetidentity.usermanagement.web.rest.model.WFStepGenericConfig;
import com.meetidentity.usermanagement.web.rest.model.WfStepAppletLoadingInfo;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStep;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStepDetail;

@RestController

@RequestMapping("/api/v1/organizations/{organizationName}/workflows/{workflowName}/steps")
public class WorkflowStepResource {
	
	@Autowired
	private WorkflowService workflowService;
	
	@GetMapping
	List<WorkflowStep> getStepsByWorkflow(@PathVariable("organizationName") String organizationName, @PathVariable("workflowName") String workflowName) {
		return workflowService.getStepsByWorkflow(organizationName, workflowName);
	}
	
	@GetMapping("/{stepName}/details")
	WorkflowStepDetail getStepDetails(@PathVariable("organizationName") String organizationName, @PathVariable("workflowName") String workflowName, @PathVariable("stepName") String stepName) {
		return workflowService.getStepDetails(organizationName, workflowName, stepName);
	}
	
	@GetMapping("/{stepName}/genericConfigs")
	List<WFStepGenericConfig> getWfStepDetails(@PathVariable("organizationName") String organizationName, @PathVariable("workflowName") String workflowName, @PathVariable("stepName") String stepName) {
		return workflowService.getWfStepDetails(organizationName, workflowName, stepName);
	}
	
	@GetMapping("/{stepName}/{identityTypeName}/orgIdentityFieldIdNames")
	List<OrgIdentityFieldIdNamePair> getUserOrgIdentityFields(@PathVariable("organizationName") String organizationName, @PathVariable("workflowName") String workflowName, @PathVariable("stepName") String stepName,
			@PathVariable("identityTypeName") String identityTypeName) {
		return workflowService.getUserOrgIdentityFields(organizationName, identityTypeName);
	}
	
	@GetMapping("/perso-and-issuance-step/applet-loading-info")
	WfStepAppletLoadingInfo getAppletLoadingInfo(@PathVariable("organizationName") String organizationName, @PathVariable("workflowName") String workflowName, @RequestParam(name="requestFrom") String requestFrom) {
		return workflowService.getAppletLoadingInfo(organizationName, workflowName, requestFrom);
	}
	
}
