package com.meetidentity.usermanagement.web.rest.model;

import java.io.Serializable;
import java.util.List;

public class EnrollmentApproveRejectReasons implements Serializable{

	private static final long serialVersionUID = 1L;

	
	List<String> approvalReasons ;
	List<String> rejectionReasons ;
	
	public List<String> getApprovalReasons() {
		return approvalReasons;
	}
	public void setApprovalReasons(List<String> approvalReasons) {
		this.approvalReasons = approvalReasons;
	}
	public List<String> getRejectionReasons() {
		return rejectionReasons;
	}
	public void setRejectionReasons(List<String> rejectionReasons) {
		this.rejectionReasons = rejectionReasons;
	}
	
	
	
}
