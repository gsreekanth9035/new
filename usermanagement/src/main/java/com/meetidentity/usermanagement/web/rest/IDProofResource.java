package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.IDProofService;
import com.meetidentity.usermanagement.web.rest.model.IDProofIssuingAuthority;
import com.meetidentity.usermanagement.web.rest.model.IDProofType;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/idProof")
public class IDProofResource {

	@Autowired
	private IDProofService idProofTypeService;

	@GetMapping("/idProofTypes")
	public List<IDProofType> getIDProofDocTypes(@PathVariable("organizationName") String organizationName) {
		return idProofTypeService.getIDProofTypes(organizationName);
	}

	@GetMapping("/idProofTypes/{idProofTypeId}/issuingAuthorities")
	public List<IDProofIssuingAuthority> getIDProofIssuingAuthorities(@PathVariable("organizationName") String organizationName,@PathVariable("idProofTypeId") Long idProofTypeId) {
		return idProofTypeService.getIssuingAuthorities(organizationName, idProofTypeId);
	}

}
