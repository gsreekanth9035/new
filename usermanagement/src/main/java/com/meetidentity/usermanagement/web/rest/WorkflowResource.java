package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.WorkflowService;
import com.meetidentity.usermanagement.web.rest.model.DeviceProfile;
import com.meetidentity.usermanagement.web.rest.model.ResultPage;
import com.meetidentity.usermanagement.web.rest.model.Workflow;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStep;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStepDetail;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/workflows")
public class WorkflowResource {
	
	@Autowired
	private WorkflowService workflowService;
	
	@GetMapping
	public ResultPage getWorkflows(@PathVariable("organizationName") String organizationName, @RequestParam("sort") String sort, @RequestParam("order") String order,
			@RequestParam("page") int page, @RequestParam("size") int size) {
		return workflowService.getWorkflows(organizationName, sort, order, page, size);
	}

	@GetMapping("/{workflowName}")
	public Workflow getWorkflow(@PathVariable("organizationName") String organizationName, @PathVariable("workflowName") String workflowName) {
		return workflowService.getWorkflowByName(organizationName, workflowName);
	}

	@GetMapping("/default")
	public Workflow getDefaultWorkflow(@PathVariable("organizationName") String organizationName) {
		return workflowService.getDefaultWorkflow(organizationName);
	}

	@PostMapping
	public ResponseEntity<String> createWorkflow(@PathVariable("organizationName") String organizationName, @RequestBody Workflow workflow) {
		return workflowService.createWorkflow(organizationName, workflow);
	}

	@PutMapping("/{workflowName}")
	public ResponseEntity<String> updateWorkflow(@PathVariable("organizationName") String organizationName, @PathVariable("workflowName") String workflowName, @RequestBody Workflow workflow) {
		return	workflowService.updateWorkflow(organizationName, workflowName, workflow);
	}

	@DeleteMapping("/{workflowName}")
	public ResponseEntity<Void> deleteWorkflow(@PathVariable("organizationName") String organizationName, @PathVariable("workflowName") String workflowName) {
		workflowService.deleteWorkflow(organizationName, workflowName);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("/{wfName}/device-profiles")
	public List<DeviceProfile> getWorkflowDevProfiles(@PathVariable("organizationName") String organizationName, @PathVariable("wfName") String wfName) {
		return workflowService.getWorkflowDevProfiles(organizationName, wfName);
	}
	
	@GetMapping("/{wfName}/device-profiles-names")
	public List<DeviceProfile> getWorkflowDevProfileNames(@PathVariable("organizationName") String organizationName, @PathVariable("wfName") String wfName) {
		return workflowService.getWorkflowDevProfileNames(organizationName, wfName);
	}
		
	//MOBILE_ID_IDENTITY_ISSUANCE
	@GetMapping("/{workflowName}/mobile-issuance-details")
	WorkflowStep getMobileIdentityIssuanceDetails(@PathVariable("organizationName") String organizationName, @PathVariable("workflowName") String workflowName) {
		return workflowService.getMobileIdentityIssuanceDetails(organizationName, workflowName);
	}
}
