package com.meetidentity.usermanagement.web.rest.model;

public class EnrollmentStepsStatus {

	private Long id;

	private Long userId;

	private int idproofStatus;

	private int registrationFormStatus;

	private int faceStatus;

	private int irisStatus;
	
	private int rollFingerprintStatus;

	private int fingerprintStatus;

	private int signatureStatus;
	
	private int summaryStatus;

	private String comments;

	private int version;
	
	

	public EnrollmentStepsStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EnrollmentStepsStatus(Long id, Long userId, int idproofStatus, int registrationFormStatus, int faceStatus,
			int irisStatus, int rollFingerprintStatus, int fingerprintStatus, int signatureStatus, int summaryStatus, String comments, int version) {
		super();
		this.id = id;
		this.userId = userId;
		this.idproofStatus = idproofStatus;
		this.registrationFormStatus = registrationFormStatus;
		this.faceStatus = faceStatus;
		this.irisStatus = irisStatus;
		this.rollFingerprintStatus = rollFingerprintStatus;
		this.fingerprintStatus = fingerprintStatus;
		this.signatureStatus = signatureStatus;
		this.summaryStatus = summaryStatus;
		this.comments = comments;
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getIdproofStatus() {
		return idproofStatus;
	}

	public void setIdproofStatus(int idproofStatus) {
		this.idproofStatus = idproofStatus;
	}

	public int getRegistrationFormStatus() {
		return registrationFormStatus;
	}

	public void setRegistrationFormStatus(int registrationFormStatus) {
		this.registrationFormStatus = registrationFormStatus;
	}

	public int getFaceStatus() {
		return faceStatus;
	}

	public void setFaceStatus(int faceStatus) {
		this.faceStatus = faceStatus;
	}

	public int getIrisStatus() {
		return irisStatus;
	}

	public void setIrisStatus(int irisStatus) {
		this.irisStatus = irisStatus;
	}

	public int getFingerprintStatus() {
		return fingerprintStatus;
	}

	public void setFingerprintStatus(int fingerprintStatus) {
		this.fingerprintStatus = fingerprintStatus;
	}

	public int getSignatureStatus() {
		return signatureStatus;
	}

	public void setSignatureStatus(int signatureStatus) {
		this.signatureStatus = signatureStatus;
	}
	
	

	public int getSummaryStatus() {
		return summaryStatus;
	}

	public void setSummaryStatus(int summaryStatus) {
		this.summaryStatus = summaryStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getRollFingerprintStatus() {
		return rollFingerprintStatus;
	}

	public void setRollFingerprintStatus(int rollFingerprintStatus) {
		this.rollFingerprintStatus = rollFingerprintStatus;
	}

}
