package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.WorkflowService;
import com.meetidentity.usermanagement.web.rest.model.WorkflowStepDef;

@RestController

//TODO : Need to think about inclusion of Organization in the REST URI
@RequestMapping("/api/v1/workflow-step-defs")
public class WorkflowStepDefResource {
	
	@Autowired
	private WorkflowService workflowService;
	
	@GetMapping
	List<WorkflowStepDef> getWorkflowStepDefs() {
		return workflowService.getWorkflowStepDefs();
	}
}
