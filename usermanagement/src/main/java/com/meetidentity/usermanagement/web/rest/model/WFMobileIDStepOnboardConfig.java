package com.meetidentity.usermanagement.web.rest.model;

public class WFMobileIDStepOnboardConfig {

	private Boolean requireOnboardAndEnroll = false;
	private WFMobileIDStepOnboardAndEnrollConfig onboardAndEnroll;
	private Boolean requireFaceVerification = false;
	private WFMobileIDStepFaceVerificationConfig faceVerification;
	private Boolean requireAdjudicationOrAprroval = false;
	private WFMobileIDStepAdjudicationConfig adjudication;

	public Boolean getRequireOnboardAndEnroll() {
		return requireOnboardAndEnroll;
	}

	public void setRequireOnboardAndEnroll(Boolean requireOnboardAndEnroll) {
		this.requireOnboardAndEnroll = requireOnboardAndEnroll;
	}

	public WFMobileIDStepOnboardAndEnrollConfig getOnboardAndEnroll() {
		return onboardAndEnroll;
	}

	public void setOnboardAndEnroll(WFMobileIDStepOnboardAndEnrollConfig onboardAndEnroll) {
		this.onboardAndEnroll = onboardAndEnroll;
	}

	public Boolean getRequireFaceVerification() {
		return requireFaceVerification;
	}

	public void setRequireFaceVerification(Boolean requireFaceVerification) {
		this.requireFaceVerification = requireFaceVerification;
	}

	public WFMobileIDStepFaceVerificationConfig getFaceVerification() {
		return faceVerification;
	}

	public void setFaceVerification(WFMobileIDStepFaceVerificationConfig faceVerification) {
		this.faceVerification = faceVerification;
	}

	public Boolean getRequireAdjudicationOrAprroval() {
		return requireAdjudicationOrAprroval;
	}

	public void setRequireAdjudicationOrAprroval(Boolean requireAdjudicationOrAprroval) {
		this.requireAdjudicationOrAprroval = requireAdjudicationOrAprroval;
	}

	public WFMobileIDStepAdjudicationConfig getAdjudication() {
		return adjudication;
	}

	public void setAdjudication(WFMobileIDStepAdjudicationConfig adjudication) {
		this.adjudication = adjudication;
	}

}
