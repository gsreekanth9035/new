package com.meetidentity.usermanagement.web.rest.model;

import java.util.List;

import com.meetidentity.usermanagement.keycloak.domain.GroupRepresentation;
import com.meetidentity.usermanagement.keycloak.domain.RoleRepresentation;

public class UserRolesAndGroups {

	private String userID;
	private List<GroupRepresentation> groupRepresentations = null;
	private List<RoleRepresentation> roleRepresentations = null;
		
	public UserRolesAndGroups(String userID, List<GroupRepresentation> groupRepresentations,
			List<RoleRepresentation> roleRepresentations) {
		this.userID = userID;
		this.groupRepresentations = groupRepresentations;
		this.roleRepresentations = roleRepresentations;
	}	
	
	public UserRolesAndGroups() {
	}

	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public List<GroupRepresentation> getGroupRepresentations() {
		return groupRepresentations;
	}
	public void setGroupRepresentations(List<GroupRepresentation> groupRepresentations) {
		this.groupRepresentations = groupRepresentations;
	}
	public List<RoleRepresentation> getRoleRepresentations() {
		return roleRepresentations;
	}
	public void setRoleRepresentations(List<RoleRepresentation> roleRepresentations) {
		this.roleRepresentations = roleRepresentations;
	}
	

}
