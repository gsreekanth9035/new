package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.ConfigValueService;
import com.meetidentity.usermanagement.web.rest.model.ConfigValue;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/config/{configName}/values")
public class ConfigValueResource {

	@Autowired
	private ConfigValueService configValueService;

	@GetMapping
	public List<ConfigValue> getConfigValues(@PathVariable("organizationName") String organizationName, @PathVariable("configName") String configName,
			@RequestParam(name = "parentConfigValueId", required = false) Long parentConfigValueId) {
		return configValueService.getConfigValues(organizationName, configName, parentConfigValueId);
	}

}
