package com.meetidentity.usermanagement.web.rest.model;

public class Finger {
	private Integer fingerCount;
	private String fingerConfigValueName;

	public Integer getFingerCount() {
		return fingerCount;
	}

	public void setFingerCount(Integer fingerCount) {
		this.fingerCount = fingerCount;
	}

	public String getFingerConfigValueName() {
		return fingerConfigValueName;
	}

	public void setFingerConfigValueName(String fingerConfigValueName) {
		this.fingerConfigValueName = fingerConfigValueName;
	}

}
