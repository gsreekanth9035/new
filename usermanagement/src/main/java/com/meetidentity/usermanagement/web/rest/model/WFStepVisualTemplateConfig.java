package com.meetidentity.usermanagement.web.rest.model;

public class WFStepVisualTemplateConfig {

	private Long id;
	private Long workflowMobileIdentityIssuanceId;
	private Long visualTemplateID;
	private String visualTemplateName;
	private String frontOrientation;
	private String backOrientation;
	private String identityTypeName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getWorkflowMobileIdentityIssuanceId() {
		return workflowMobileIdentityIssuanceId;
	}

	public void setWorkflowMobileIdentityIssuanceId(Long workflowMobileIdentityIssuanceId) {
		this.workflowMobileIdentityIssuanceId = workflowMobileIdentityIssuanceId;
	}

	public Long getVisualTemplateID() {
		return visualTemplateID;
	}

	public void setVisualTemplateID(Long visualTemplateID) {
		this.visualTemplateID = visualTemplateID;
	}

	public String getVisualTemplateName() {
		return visualTemplateName;
	}

	public void setVisualTemplateName(String visualTemplateName) {
		this.visualTemplateName = visualTemplateName;
	}

	public String getFrontOrientation() {
		return frontOrientation;
	}

	public void setFrontOrientation(String frontOrientation) {
		this.frontOrientation = frontOrientation;
	}

	public String getBackOrientation() {
		return backOrientation;
	}

	public void setBackOrientation(String backOrientation) {
		this.backOrientation = backOrientation;
	}

	public String getIdentityTypeName() {
		return identityTypeName;
	}

	public void setIdentityTypeName(String identityTypeName) {
		this.identityTypeName = identityTypeName;
	}

}
