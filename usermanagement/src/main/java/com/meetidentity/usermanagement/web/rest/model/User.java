package com.meetidentity.usermanagement.web.rest.model;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class User {

	private Long id;
	private String uimsId;
	private String representationId;
	private String name;
	private String fullName;
	private String organisationName;
	private Long organisationId;
	private String groupId;
	private String groupName;
	private String refApprovalGroupId;

	private String status;
	// TODO Find a way to retrieve user workflow based on user name from keycloak
	private String workflowName;
	private Workflow workflow;

	private String firstName;
	private String middleName;
	private String lastName;
	private String mothersName;
	private String email;
	private String contactNumber;
	private String userPrincipalName;
	private String country;

	private String enrollmentStatus;
	private String userStatus;
	private boolean canIssueIdentity;
	private boolean isNewLdapUser;
	private boolean isFederated;
	private boolean isEnrollmentInProgress;
	private boolean expressMobileEnrollment;
	private EnrollmentStepsStatus enrollmentStepsStatus;

	private String createdBy;

	private Instant createdDate = Instant.now();

	private String lastModifiedBy;

	private Instant lastModifiedDate = Instant.now();

	private List<DeviceProfile> wfDeviceProfiles = new ArrayList<>();

	private UserPIVModel userPIV;

	private UserAppearance userAppearance;

	private UserAttribute userAttribute;

	private UserAddress userAddress;

	private List<UserBiometric> userBiometrics = new ArrayList<>();

	private List<UserBiometricSkip> userBiometricSkips = new ArrayList<>();

	private List<UserIDProofDocument> userIDProofDocuments = new ArrayList<>();

	private UserDrivingLicense userDrivingLicense;

	private UserEmployeeID userEmployeeID;

	private UserPersonalizedID userPersonalizedID;

	private UserStudentID userStudentID;

	private UserHealthID userHealthID;

	private UserNationalID userNationalID;

	private UserVoterID userVoterID;

	private UserPermanentResidentID userPermanentResidentID;

	private UserHealthService userHealthService;

	private WfMobileIDStepDetails wfMobileIDStepDetails;

	private UserEnrollmentHistory userEnrollmentHistory;
	
	private List<UserEnrollmentHistory> userEnrollmentHistoryList;

	private Boolean isTransparentPhotoRequired;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Instant getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Instant lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public User() {

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMothersName() {
		return mothersName;
	}

	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserAppearance getUserAppearance() {
		return userAppearance;
	}

	public void setUserAppearance(UserAppearance userAppearance) {
		this.userAppearance = userAppearance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public String getRepresentationId() {
		return representationId;
	}

	public void setRepresentationId(String representationId) {
		this.representationId = representationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrganisationName() {
		return organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	public Long getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getRefApprovalGroupId() {
		return refApprovalGroupId;
	}

	public void setRefApprovalGroupId(String refApprovalGroupId) {
		this.refApprovalGroupId = refApprovalGroupId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public UserPIVModel getUserPIV() {
		return userPIV;
	}

	public void setUserPIV(UserPIVModel userPIV) {
		this.userPIV = userPIV;
	}

	public UserAttribute getUserAttribute() {
		return userAttribute;
	}

	public void setUserAttribute(UserAttribute userAttribute) {
		this.userAttribute = userAttribute;
	}

	public List<UserBiometric> getUserBiometrics() {
		return userBiometrics;
	}

	public void setUserBiometrics(List<UserBiometric> userBiometrics) {
		this.userBiometrics = userBiometrics;
	}

	public List<UserBiometricSkip> getUserBiometricSkips() {
		return userBiometricSkips;
	}

	public void setUserBiometricSkips(List<UserBiometricSkip> userBiometricSkips) {
		this.userBiometricSkips = userBiometricSkips;
	}

	public List<UserIDProofDocument> getUserIDProofDocuments() {
		return userIDProofDocuments;
	}

	public void setUserIDProofDocuments(List<UserIDProofDocument> userIDProofDocuments) {
		this.userIDProofDocuments = userIDProofDocuments;
	}

	public UserDrivingLicense getUserDrivingLicense() {
		return userDrivingLicense;
	}

	public void setUserDrivingLicense(UserDrivingLicense userDrivingLicense) {
		this.userDrivingLicense = userDrivingLicense;
	}

	public UserNationalID getUserNationalID() {
		return userNationalID;
	}

	public void setUserNationalID(UserNationalID userNationalID) {
		this.userNationalID = userNationalID;
	}

	public UserHealthService getUserHealthService() {
		return userHealthService;
	}

	public void setUserHealthService(UserHealthService userHealthService) {
		this.userHealthService = userHealthService;
	}

	public WfMobileIDStepDetails getWfMobileIDStepDetails() {
		return wfMobileIDStepDetails;
	}

	public void setWfMobileIDStepDetails(WfMobileIDStepDetails wfMobileIDStepDetails) {
		this.wfMobileIDStepDetails = wfMobileIDStepDetails;
	}

	public String getUserPrincipalName() {
		return userPrincipalName;
	}

	public void setUserPrincipalName(String userPrincipalName) {
		this.userPrincipalName = userPrincipalName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<DeviceProfile> getWfDeviceProfiles() {
		return wfDeviceProfiles;
	}

	public void setWfDeviceProfiles(List<DeviceProfile> wfDeviceProfiles) {
		this.wfDeviceProfiles = wfDeviceProfiles;
	}

	public UserEnrollmentHistory getUserEnrollmentHistory() {
		return userEnrollmentHistory;
	}

	public void setUserEnrollmentHistory(UserEnrollmentHistory userEnrollmentHistory) {
		this.userEnrollmentHistory = userEnrollmentHistory;
	}

	public UserEmployeeID getUserEmployeeID() {
		return userEmployeeID;
	}

	public void setUserEmployeeID(UserEmployeeID userEmployeeID) {
		this.userEmployeeID = userEmployeeID;
	}

	public UserPersonalizedID getUserPersonalizedID() {
		return userPersonalizedID;
	}

	public void setUserPersonalizedID(UserPersonalizedID userPersonalizedID) {
		this.userPersonalizedID = userPersonalizedID;
	}

	public UserStudentID getUserStudentID() {
		return userStudentID;
	}

	public void setUserStudentID(UserStudentID userStudentID) {
		this.userStudentID = userStudentID;
	}

	public UserHealthID getUserHealthID() {
		return userHealthID;
	}

	public void setUserHealthID(UserHealthID userHealthID) {
		this.userHealthID = userHealthID;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Workflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public boolean isCanIssueIdentity() {
		return canIssueIdentity;
	}

	public void setCanIssueIdentity(boolean canIssueIdentity) {
		this.canIssueIdentity = canIssueIdentity;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public boolean getIsNewLdapUser() {
		return isNewLdapUser;
	}

	public void setIsNewLdapUser(boolean isNewLdapUser) {
		this.isNewLdapUser = isNewLdapUser;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public boolean getIsFederated() {
		return isFederated;
	}

	public void setIsFederated(boolean isFederated) {
		this.isFederated = isFederated;
	}

	public boolean getIsEnrollmentInProgress() {
		return isEnrollmentInProgress;
	}

	public void setIsEnrollmentInProgress(boolean isEnrollmentInProgress) {
		this.isEnrollmentInProgress = isEnrollmentInProgress;
	}

	public EnrollmentStepsStatus getEnrollmentStepsStatus() {
		return enrollmentStepsStatus;
	}

	public void setEnrollmentStepsStatus(EnrollmentStepsStatus enrollmentStepsStatus) {
		this.enrollmentStepsStatus = enrollmentStepsStatus;
	}

	public UserAddress getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(UserAddress userAddress) {
		this.userAddress = userAddress;
	}

	public UserPermanentResidentID getUserPermanentResidentID() {
		return userPermanentResidentID;
	}

	public void setUserPermanentResidentID(UserPermanentResidentID userPermanentResidentID) {
		this.userPermanentResidentID = userPermanentResidentID;
	}

	public Boolean getIsTransparentPhotoRequired() {
		return isTransparentPhotoRequired;
	}

	public void setIsTransparentPhotoRequired(Boolean isTransparentPhotoRequired) {
		this.isTransparentPhotoRequired = isTransparentPhotoRequired;
	}

	public UserVoterID getUserVoterID() {
		return userVoterID;
	}

	public void setUserVoterID(UserVoterID userVoterID) {
		this.userVoterID = userVoterID;
	}

	public List<UserEnrollmentHistory> getUserEnrollmentHistoryList() {
		return userEnrollmentHistoryList;
	}

	public void setUserEnrollmentHistoryList(List<UserEnrollmentHistory> userEnrollmentHistoryList) {
		this.userEnrollmentHistoryList = userEnrollmentHistoryList;
	}

	public boolean isExpressMobileEnrollment() {
		return expressMobileEnrollment;
	}

	public void setExpressMobileEnrollment(boolean expressMobileEnrollment) {
		this.expressMobileEnrollment = expressMobileEnrollment;
	}

	
}
