package com.meetidentity.usermanagement.web.rest.model;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class UserDetails {

	private Long id;
	private String name;
	private String fullName;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String organisationName;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long organisationId;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String groupId;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String groupName;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String refApprovalGroupId;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String status;
	// TODO Find a way to retrieve user workflow based on user name from keycloak

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String workflowName;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Workflow workflow;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String firstName;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String middleName;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String lastName;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String mothersName;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String email;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String userPrincipalName;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String country;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String enrollmentStatus;

	private Boolean enrolled;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String userStatus;

	private boolean canIssueIdentity;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String createdBy;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Instant createdDate = Instant.now();

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String lastModifiedBy;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Instant lastModifiedDate = Instant.now();

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<UserBiometric> userBiometrics = new ArrayList<>();

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private WfMobileIDStepDetails wfMobileIDStepDetails = new WfMobileIDStepDetails();

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private AuthenticatorClientInfo authenticatorClientInfo = null;

	private Boolean canEnroll;
	private Boolean canIssue;
	private Boolean canLifeCycle;
	private Boolean canDisplayEnrollDetails;
	private Boolean canApprove;

	private String onboardSelfService;
	private Boolean isFederated;
	
	private Boolean enabled;

	public UserDetails() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getOrganisationName() {
		return organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	public Long getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getRefApprovalGroupId() {
		return refApprovalGroupId;
	}

	public void setRefApprovalGroupId(String refApprovalGroupId) {
		this.refApprovalGroupId = refApprovalGroupId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public Workflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMothersName() {
		return mothersName;
	}

	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserPrincipalName() {
		return userPrincipalName;
	}

	public void setUserPrincipalName(String userPrincipalName) {
		this.userPrincipalName = userPrincipalName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public boolean isCanIssueIdentity() {
		return canIssueIdentity;
	}

	public void setCanIssueIdentity(boolean canIssueIdentity) {
		this.canIssueIdentity = canIssueIdentity;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Instant getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Instant lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public List<UserBiometric> getUserBiometrics() {
		return userBiometrics;
	}

	public void setUserBiometrics(List<UserBiometric> userBiometrics) {
		this.userBiometrics = userBiometrics;
	}

	public WfMobileIDStepDetails getWfMobileIDStepDetails() {
		return wfMobileIDStepDetails;
	}

	public void setWfMobileIDStepDetails(WfMobileIDStepDetails wfMobileIDStepDetails) {
		this.wfMobileIDStepDetails = wfMobileIDStepDetails;
	}

	public AuthenticatorClientInfo getAuthenticatorClientInfo() {
		return authenticatorClientInfo;
	}

	public void setAuthenticatorClientInfo(AuthenticatorClientInfo authenticatorClientInfo) {
		this.authenticatorClientInfo = authenticatorClientInfo;
	}

	public Boolean getEnrolled() {
		return enrolled;
	}

	public void setEnrolled(Boolean enrolled) {
		this.enrolled = enrolled;
	}

	public Boolean getCanEnroll() {
		return canEnroll;
	}

	public void setCanEnroll(Boolean canEnroll) {
		this.canEnroll = canEnroll;
	}

	public Boolean getCanIssue() {
		return canIssue;
	}

	public void setCanIssue(Boolean canIssue) {
		this.canIssue = canIssue;
	}

	public Boolean getCanLifeCycle() {
		return canLifeCycle;
	}

	public void setCanLifeCycle(Boolean canLifeCycle) {
		this.canLifeCycle = canLifeCycle;
	}

	public Boolean getCanDisplayEnrollDetails() {
		return canDisplayEnrollDetails;
	}

	public void setCanDisplayEnrollDetails(Boolean canDisplayEnrollDetails) {
		this.canDisplayEnrollDetails = canDisplayEnrollDetails;
	}

	public Boolean getCanApprove() {
		return canApprove;
	}

	public void setCanApprove(Boolean canApprove) {
		this.canApprove = canApprove;
	}

	public String getOnboardSelfService() {
		return onboardSelfService;
	}

	public void setOnboardSelfService(String onboardSelfService) {
		this.onboardSelfService = onboardSelfService;
	}

	public Boolean getIsFederated() {
		return isFederated;
	}

	public void setIsFederated(Boolean isFederated) {
		this.isFederated = isFederated;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
}
