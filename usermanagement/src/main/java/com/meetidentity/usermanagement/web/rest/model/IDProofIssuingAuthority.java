package com.meetidentity.usermanagement.web.rest.model;

public class IDProofIssuingAuthority {
	
	private Long id;
	private String issuingAuthority;

	public IDProofIssuingAuthority() {
	}

	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
