package com.meetidentity.usermanagement.web.rest.model.notification;

import java.util.List;

public class SmsModel {

	private String provider;
	private List<String> toPhone;
	private String message;
	private String fromPhone;
	private String fromName;
	private String subject;

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public List<String> getToPhone() {
		return toPhone;
	}

	public void setToPhone(List<String> toPhone) {
		this.toPhone = toPhone;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFromPhone() {
		return fromPhone;
	}

	public void setFromPhone(String fromPhone) {
		this.fromPhone = fromPhone;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
}
