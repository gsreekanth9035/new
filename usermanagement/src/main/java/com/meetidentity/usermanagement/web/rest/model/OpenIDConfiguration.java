package com.meetidentity.usermanagement.web.rest.model;

import java.util.List;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder({
	"issuer",
	"authorization_endpoint",
	"token_endpoint",
	"introspection_endpoint",
	"userinfo_endpoint",
	"end_session_endpoint",
	"jwks_uri",
	"check_session_iframe",
	"grant_types_supported",
	"response_types_supported",
	"subject_types_supported",
	"id_token_signing_alg_values_supported",
	"id_token_encryption_alg_values_supported",
	"id_token_encryption_enc_values_supported",
	"userinfo_signing_alg_values_supported",
	"request_object_signing_alg_values_supported",
	"response_modes_supported",
	"registration_endpoint",
	"token_endpoint_auth_methods_supported",
	"token_endpoint_auth_signing_alg_values_supported",
	"claims_supported",
	"claim_types_supported",
	"claims_parameter_supported",
	"scopes_supported",
	"request_parameter_supported",
	"request_uri_parameter_supported",
	"code_challenge_methods_supported",
	"tls_client_certificate_bound_access_tokens"
	})
	@Generated("jsonschema2pojo")
	public class OpenIDConfiguration {

	@JsonProperty("issuer")
	private String issuer;
	@JsonProperty("authorization_endpoint")
	private String authorizationEndpoint;
	@JsonProperty("token_endpoint")
	private String tokenEndpoint;
	@JsonProperty("introspection_endpoint")
	private String introspectionEndpoint;
	@JsonProperty("userinfo_endpoint")
	private String userinfoEndpoint;
	@JsonProperty("end_session_endpoint")
	private String endSessionEndpoint;
	@JsonProperty("jwks_uri")
	private String jwksUri;
	@JsonProperty("check_session_iframe")
	private String checkSessionIframe;
	@JsonProperty("grant_types_supported")
	private List<String> grantTypesSupported = null;
	@JsonProperty("response_types_supported")
	private List<String> responseTypesSupported = null;
	@JsonProperty("subject_types_supported")
	private List<String> subjectTypesSupported = null;
	@JsonProperty("id_token_signing_alg_values_supported")
	private List<String> idTokenSigningAlgValuesSupported = null;
	@JsonProperty("id_token_encryption_alg_values_supported")
	private List<String> idTokenEncryptionAlgValuesSupported = null;
	@JsonProperty("id_token_encryption_enc_values_supported")
	private List<String> idTokenEncryptionEncValuesSupported = null;
	@JsonProperty("userinfo_signing_alg_values_supported")
	private List<String> userinfoSigningAlgValuesSupported = null;
	@JsonProperty("request_object_signing_alg_values_supported")
	private List<String> requestObjectSigningAlgValuesSupported = null;
	@JsonProperty("response_modes_supported")
	private List<String> responseModesSupported = null;
	@JsonProperty("registration_endpoint")
	private String registrationEndpoint;
	@JsonProperty("token_endpoint_auth_methods_supported")
	private List<String> tokenEndpointAuthMethodsSupported = null;
	@JsonProperty("token_endpoint_auth_signing_alg_values_supported")
	private List<String> tokenEndpointAuthSigningAlgValuesSupported = null;
	@JsonProperty("claims_supported")
	private List<String> claimsSupported = null;
	@JsonProperty("claim_types_supported")
	private List<String> claimTypesSupported = null;
	@JsonProperty("claims_parameter_supported")
	private Boolean claimsParameterSupported;
	@JsonProperty("scopes_supported")
	private List<String> scopesSupported = null;
	@JsonProperty("request_parameter_supported")
	private Boolean requestParameterSupported;
	@JsonProperty("request_uri_parameter_supported")
	private Boolean requestUriParameterSupported;
	@JsonProperty("code_challenge_methods_supported")
	private List<String> codeChallengeMethodsSupported = null;
	@JsonProperty("tls_client_certificate_bound_access_tokens")
	private Boolean tlsClientCertificateBoundAccessTokens;

	@JsonProperty("issuer")
	public String getIssuer() {
	return issuer;
	}

	@JsonProperty("issuer")
	public void setIssuer(String issuer) {
	this.issuer = issuer;
	}

	@JsonProperty("authorization_endpoint")
	public String getAuthorizationEndpoint() {
	return authorizationEndpoint;
	}

	@JsonProperty("authorization_endpoint")
	public void setAuthorizationEndpoint(String authorizationEndpoint) {
	this.authorizationEndpoint = authorizationEndpoint;
	}

	@JsonProperty("token_endpoint")
	public String getTokenEndpoint() {
	return tokenEndpoint;
	}

	@JsonProperty("token_endpoint")
	public void setTokenEndpoint(String tokenEndpoint) {
	this.tokenEndpoint = tokenEndpoint;
	}

	@JsonProperty("introspection_endpoint")
	public String getIntrospectionEndpoint() {
	return introspectionEndpoint;
	}

	@JsonProperty("introspection_endpoint")
	public void setIntrospectionEndpoint(String introspectionEndpoint) {
	this.introspectionEndpoint = introspectionEndpoint;
	}

	@JsonProperty("userinfo_endpoint")
	public String getUserinfoEndpoint() {
	return userinfoEndpoint;
	}

	@JsonProperty("userinfo_endpoint")
	public void setUserinfoEndpoint(String userinfoEndpoint) {
	this.userinfoEndpoint = userinfoEndpoint;
	}

	@JsonProperty("end_session_endpoint")
	public String getEndSessionEndpoint() {
	return endSessionEndpoint;
	}

	@JsonProperty("end_session_endpoint")
	public void setEndSessionEndpoint(String endSessionEndpoint) {
	this.endSessionEndpoint = endSessionEndpoint;
	}

	@JsonProperty("jwks_uri")
	public String getJwksUri() {
	return jwksUri;
	}

	@JsonProperty("jwks_uri")
	public void setJwksUri(String jwksUri) {
	this.jwksUri = jwksUri;
	}

	@JsonProperty("check_session_iframe")
	public String getCheckSessionIframe() {
	return checkSessionIframe;
	}

	@JsonProperty("check_session_iframe")
	public void setCheckSessionIframe(String checkSessionIframe) {
	this.checkSessionIframe = checkSessionIframe;
	}

	@JsonProperty("grant_types_supported")
	public List<String> getGrantTypesSupported() {
	return grantTypesSupported;
	}

	@JsonProperty("grant_types_supported")
	public void setGrantTypesSupported(List<String> grantTypesSupported) {
	this.grantTypesSupported = grantTypesSupported;
	}

	@JsonProperty("response_types_supported")
	public List<String> getResponseTypesSupported() {
	return responseTypesSupported;
	}

	@JsonProperty("response_types_supported")
	public void setResponseTypesSupported(List<String> responseTypesSupported) {
	this.responseTypesSupported = responseTypesSupported;
	}

	@JsonProperty("subject_types_supported")
	public List<String> getSubjectTypesSupported() {
	return subjectTypesSupported;
	}

	@JsonProperty("subject_types_supported")
	public void setSubjectTypesSupported(List<String> subjectTypesSupported) {
	this.subjectTypesSupported = subjectTypesSupported;
	}

	@JsonProperty("id_token_signing_alg_values_supported")
	public List<String> getIdTokenSigningAlgValuesSupported() {
	return idTokenSigningAlgValuesSupported;
	}

	@JsonProperty("id_token_signing_alg_values_supported")
	public void setIdTokenSigningAlgValuesSupported(List<String> idTokenSigningAlgValuesSupported) {
	this.idTokenSigningAlgValuesSupported = idTokenSigningAlgValuesSupported;
	}

	@JsonProperty("id_token_encryption_alg_values_supported")
	public List<String> getIdTokenEncryptionAlgValuesSupported() {
	return idTokenEncryptionAlgValuesSupported;
	}

	@JsonProperty("id_token_encryption_alg_values_supported")
	public void setIdTokenEncryptionAlgValuesSupported(List<String> idTokenEncryptionAlgValuesSupported) {
	this.idTokenEncryptionAlgValuesSupported = idTokenEncryptionAlgValuesSupported;
	}

	@JsonProperty("id_token_encryption_enc_values_supported")
	public List<String> getIdTokenEncryptionEncValuesSupported() {
	return idTokenEncryptionEncValuesSupported;
	}

	@JsonProperty("id_token_encryption_enc_values_supported")
	public void setIdTokenEncryptionEncValuesSupported(List<String> idTokenEncryptionEncValuesSupported) {
	this.idTokenEncryptionEncValuesSupported = idTokenEncryptionEncValuesSupported;
	}

	@JsonProperty("userinfo_signing_alg_values_supported")
	public List<String> getUserinfoSigningAlgValuesSupported() {
	return userinfoSigningAlgValuesSupported;
	}

	@JsonProperty("userinfo_signing_alg_values_supported")
	public void setUserinfoSigningAlgValuesSupported(List<String> userinfoSigningAlgValuesSupported) {
	this.userinfoSigningAlgValuesSupported = userinfoSigningAlgValuesSupported;
	}

	@JsonProperty("request_object_signing_alg_values_supported")
	public List<String> getRequestObjectSigningAlgValuesSupported() {
	return requestObjectSigningAlgValuesSupported;
	}

	@JsonProperty("request_object_signing_alg_values_supported")
	public void setRequestObjectSigningAlgValuesSupported(List<String> requestObjectSigningAlgValuesSupported) {
	this.requestObjectSigningAlgValuesSupported = requestObjectSigningAlgValuesSupported;
	}

	@JsonProperty("response_modes_supported")
	public List<String> getResponseModesSupported() {
	return responseModesSupported;
	}

	@JsonProperty("response_modes_supported")
	public void setResponseModesSupported(List<String> responseModesSupported) {
	this.responseModesSupported = responseModesSupported;
	}

	@JsonProperty("registration_endpoint")
	public String getRegistrationEndpoint() {
	return registrationEndpoint;
	}

	@JsonProperty("registration_endpoint")
	public void setRegistrationEndpoint(String registrationEndpoint) {
	this.registrationEndpoint = registrationEndpoint;
	}

	@JsonProperty("token_endpoint_auth_methods_supported")
	public List<String> getTokenEndpointAuthMethodsSupported() {
	return tokenEndpointAuthMethodsSupported;
	}

	@JsonProperty("token_endpoint_auth_methods_supported")
	public void setTokenEndpointAuthMethodsSupported(List<String> tokenEndpointAuthMethodsSupported) {
	this.tokenEndpointAuthMethodsSupported = tokenEndpointAuthMethodsSupported;
	}

	@JsonProperty("token_endpoint_auth_signing_alg_values_supported")
	public List<String> getTokenEndpointAuthSigningAlgValuesSupported() {
	return tokenEndpointAuthSigningAlgValuesSupported;
	}

	@JsonProperty("token_endpoint_auth_signing_alg_values_supported")
	public void setTokenEndpointAuthSigningAlgValuesSupported(List<String> tokenEndpointAuthSigningAlgValuesSupported) {
	this.tokenEndpointAuthSigningAlgValuesSupported = tokenEndpointAuthSigningAlgValuesSupported;
	}

	@JsonProperty("claims_supported")
	public List<String> getClaimsSupported() {
	return claimsSupported;
	}

	@JsonProperty("claims_supported")
	public void setClaimsSupported(List<String> claimsSupported) {
	this.claimsSupported = claimsSupported;
	}

	@JsonProperty("claim_types_supported")
	public List<String> getClaimTypesSupported() {
	return claimTypesSupported;
	}

	@JsonProperty("claim_types_supported")
	public void setClaimTypesSupported(List<String> claimTypesSupported) {
	this.claimTypesSupported = claimTypesSupported;
	}

	@JsonProperty("claims_parameter_supported")
	public Boolean getClaimsParameterSupported() {
	return claimsParameterSupported;
	}

	@JsonProperty("claims_parameter_supported")
	public void setClaimsParameterSupported(Boolean claimsParameterSupported) {
	this.claimsParameterSupported = claimsParameterSupported;
	}

	@JsonProperty("scopes_supported")
	public List<String> getScopesSupported() {
	return scopesSupported;
	}

	@JsonProperty("scopes_supported")
	public void setScopesSupported(List<String> scopesSupported) {
	this.scopesSupported = scopesSupported;
	}

	@JsonProperty("request_parameter_supported")
	public Boolean getRequestParameterSupported() {
	return requestParameterSupported;
	}

	@JsonProperty("request_parameter_supported")
	public void setRequestParameterSupported(Boolean requestParameterSupported) {
	this.requestParameterSupported = requestParameterSupported;
	}

	@JsonProperty("request_uri_parameter_supported")
	public Boolean getRequestUriParameterSupported() {
	return requestUriParameterSupported;
	}

	@JsonProperty("request_uri_parameter_supported")
	public void setRequestUriParameterSupported(Boolean requestUriParameterSupported) {
	this.requestUriParameterSupported = requestUriParameterSupported;
	}

	@JsonProperty("code_challenge_methods_supported")
	public List<String> getCodeChallengeMethodsSupported() {
	return codeChallengeMethodsSupported;
	}

	@JsonProperty("code_challenge_methods_supported")
	public void setCodeChallengeMethodsSupported(List<String> codeChallengeMethodsSupported) {
	this.codeChallengeMethodsSupported = codeChallengeMethodsSupported;
	}

	@JsonProperty("tls_client_certificate_bound_access_tokens")
	public Boolean getTlsClientCertificateBoundAccessTokens() {
	return tlsClientCertificateBoundAccessTokens;
	}

	@JsonProperty("tls_client_certificate_bound_access_tokens")
	public void setTlsClientCertificateBoundAccessTokens(Boolean tlsClientCertificateBoundAccessTokens) {
	this.tlsClientCertificateBoundAccessTokens = tlsClientCertificateBoundAccessTokens;
	}

	}