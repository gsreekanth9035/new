package com.meetidentity.usermanagement.web.rest.model;

public class SmartCardPrinterConfig {

	private Long id;

	private String printerName;

	private String descriptionn;

	private String deviceIdentifier;

	private String ipadderss;

	private boolean status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrinterName() {
		return printerName;
	}

	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}

	public String getDescriptionn() {
		return descriptionn;
	}

	public void setDescriptionn(String descriptionn) {
		this.descriptionn = descriptionn;
	}

	public String getDeviceIdentifier() {
		return deviceIdentifier;
	}

	public void setDeviceIdentifier(String deviceIdentifier) {
		this.deviceIdentifier = deviceIdentifier;
	}

	public String getIpadderss() {
		return ipadderss;
	}

	public void setIpadderss(String ipadderss) {
		this.ipadderss = ipadderss;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
