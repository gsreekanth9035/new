package com.meetidentity.usermanagement.web.rest.model;

public class WebAuthnCred {
	private boolean enabled;
	

	public WebAuthnCred(boolean enabled) {
		super();
		this.enabled = enabled;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
