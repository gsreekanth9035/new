package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;

public class UserVoterID {

	private long userId;
	private String uimsId;
	private Date dateOfIssuance;
	private Date dateOfExpiry;
	private String curp;
	private String registrationYear;
	private String electorKey;

	public UserVoterID(long userId, String uimsId, Date dateOfIssuance, Date dateOfExpiry, String curp,
			String registrationYear, String electorKey) {
		this.userId = userId;
		this.uimsId = uimsId;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
		this.curp = curp;
		this.registrationYear = registrationYear;
		this.electorKey = electorKey;
	}

	public UserVoterID() {
		// TODO Auto-generated constructor stub
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUimsId() {
		return uimsId;
	}

	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}

	public Date getDateOfIssuance() {
		return dateOfIssuance;
	}

	public void setDateOfIssuance(Date dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}

	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getRegistrationYear() {
		return registrationYear;
	}

	public void setRegistrationYear(String registrationYear) {
		this.registrationYear = registrationYear;
	}

	public String getElectorKey() {
		return electorKey;
	}

	public void setElectorKey(String electorKey) {
		this.electorKey = electorKey;
	}

}
