package com.meetidentity.usermanagement.web.rest.model;

import java.util.Date;

import com.vividsolutions.jts.geom.Point;

public class UserNationalID {

	private long userId;
	private String uimsId;
	private Date dateOfIssuance;
	private Date dateOfExpiry;
	private String docNumber;
	private String personalCode;
	private String issuingAuthority;
    private String vaccineName;
    private String vaccinationLocationName;
	private Point vaccinationLocation;
    private Date vaccinationDate;
	public UserNationalID(long userId, String uimsId, Date dateOfIssuance, Date dateOfExpiry,
			String docNumber,  String personalCode, String vaccineName, String vaccinationLocationName, Point  vaccinationLocation, Date vaccinationDate) {
		super();
		this.userId = userId;
		this.uimsId = uimsId;
		this.dateOfIssuance = dateOfIssuance;
		this.dateOfExpiry = dateOfExpiry;
		this.docNumber = docNumber;
		this.personalCode = personalCode;
		this.vaccineName = vaccineName;
		this.vaccinationLocationName = vaccinationLocationName;
		this.vaccinationLocation = vaccinationLocation;
		this.vaccinationDate = vaccinationDate;
	}
	public UserNationalID() {
		// TODO Auto-generated constructor stub
	}
	
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUimsId() {
		return uimsId;
	}
	public void setUimsId(String uimsId) {
		this.uimsId = uimsId;
	}
	public Date getDateOfIssuance() {
		return dateOfIssuance;
	}
	public void setDateOfIssuance(Date dateOfIssuance) {
		this.dateOfIssuance = dateOfIssuance;
	}
	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}
	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}
	public String getDocNumber() {
		return docNumber;
	}
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	public String getPersonalCode() {
		return personalCode;
	}
	public void setPersonalCode(String personalCode) {
		this.personalCode = personalCode;
	}
	public String getIssuingAuthority() {
		return issuingAuthority;
	}
	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}
	public String getVaccineName() {
		return vaccineName;
	}
	public void setVaccineName(String vaccineName) {
		this.vaccineName = vaccineName;
	}
	public String getVaccinationLocationName() {
		return vaccinationLocationName;
	}
	public void setVaccinationLocationName(String vaccinationLocationName) {
		this.vaccinationLocationName = vaccinationLocationName;
	}
	public Point getVaccinationLocation() {
		return vaccinationLocation;
	}
	public void setVaccinationLocation(Point vaccinationLocation) {
		this.vaccinationLocation = vaccinationLocation;
	}
	public Date getVaccinationDate() {
		return vaccinationDate;
	}
	public void setVaccinationDate(Date vaccinationDate) {
		this.vaccinationDate = vaccinationDate;
	}
	
	
	

}
