package com.meetidentity.usermanagement.web.rest.model;

import java.util.ArrayList;
import java.util.List;

public class VisualTemplate {

	private Long id;
	private String name;
	private OrganizationIdentity organizationIdentity;
	private List<DeviceType> deviceTypes = new ArrayList<>();
	private byte[] frontDesign;
	private byte[] backDesign;
	private String frontOrientation;
	private String backOrientation;
	private byte[] attributeMapping;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getFrontDesign() {
		return frontDesign;
	}

	public void setFrontDesign(byte[] frontDesign) {
		this.frontDesign = frontDesign;
	}

	public byte[] getBackDesign() {
		return backDesign;
	}

	public void setBackDesign(byte[] backDesign) {
		this.backDesign = backDesign;
	}

	public String getFrontOrientation() {
		return frontOrientation;
	}

	public void setFrontOrientation(String frontOrientation) {
		this.frontOrientation = frontOrientation;
	}

	public String getBackOrientation() {
		return backOrientation;
	}

	public void setBackOrientation(String backOrientation) {
		this.backOrientation = backOrientation;
	}

	public byte[] getAttributeMapping() {
		return attributeMapping;
	}

	public void setAttributeMapping(byte[] attributeMapping) {
		this.attributeMapping = attributeMapping;
	}

	public OrganizationIdentity getOrganizationIdentity() {
		return organizationIdentity;
	}

	public void setOrganizationIdentity(OrganizationIdentity organizationIdentity) {
		this.organizationIdentity = organizationIdentity;
	}

	public List<DeviceType> getDeviceTypes() {
		return deviceTypes;
	}

	public void setDeviceTypes(List<DeviceType> deviceTypes) {
		this.deviceTypes = deviceTypes;
	}

}
