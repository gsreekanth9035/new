package com.meetidentity.usermanagement.web.rest.model;

import java.util.ArrayList;
import java.util.List;

public class IdentityProviderUser {

	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private boolean enabled;
	private boolean sendInvite;
	private String sendInviteVia;
	private String countryCode;
	private String contactNumber;

	private String userName;
	private String credentialType;
	private String credentialValue;
	private boolean credentialTemporary;

	private String groupId;
	private String groupName;
	private String[] authServiceRoleID;
	private List<Role> roles = new ArrayList<>();
	private String userKLKId;
	private String host;
	private String loggedInUser;
	private Long orgId;
	private String  orgName;
	private Long loggedInUserId;
	private String userStatus;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}	

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isSendInvite() {
		return sendInvite;
	}

	public void setSendInvite(boolean sendInvite) {
		this.sendInvite = sendInvite;
	}

	public String getSendInviteVia() {
		return sendInviteVia;
	}

	public void setSendInviteVia(String sendInviteVia) {
		this.sendInviteVia = sendInviteVia;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCredentialType() {
		return credentialType;
	}

	public void setCredentialType(String credentialType) {
		this.credentialType = credentialType;
	}

	public String getCredentialValue() {
		return credentialValue;
	}

	public void setCredentialValue(String credentialValue) {
		this.credentialValue = credentialValue;
	}

	public boolean isCredentialTemporary() {
		return credentialTemporary;
	}

	public void setCredentialTemporary(boolean credentialTemporary) {
		this.credentialTemporary = credentialTemporary;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public boolean checkForEmpty() {

		if ("".equals(this.firstName.trim()) || "".equals(this.lastName.trim()) || "".equals(this.userName.trim())
				|| "".equals(this.email.trim()) || "".equals(this.groupId.trim()) || "".equals(this.groupName.trim())) {
			return true;
		} else {
			return false;
		}

	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String[] getAuthServiceRoleID() {
		return authServiceRoleID;
	}

	public void setAuthServiceRoleID(String[] authServiceRoleID) {
		this.authServiceRoleID = authServiceRoleID;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public String getUserKLKId() {
		return userKLKId;
	}

	public void setUserKLKId(String userKLKId) {
		this.userKLKId = userKLKId;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getLoggedInUserId() {
		return loggedInUserId;
	}

	public void setLoggedInUserId(Long loggedInUserId) {
		this.loggedInUserId = loggedInUserId;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

}
