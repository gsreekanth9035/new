package com.meetidentity.usermanagement.web.rest.model;

public class EnableSoftOTP2FA {
	
	private long id;
	private String softOTPType;
	private String secretKey;
	private String algorithm;
	private int digits;
	private int interval;
	private long counter;
	
	private String organizationName;
	private long userid;
	private long wfMobileIDSoftOTPIssuanceId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSoftOTPType() {
		return softOTPType;
	}
	public void setSoftOTPType(String softOTPType) {
		this.softOTPType = softOTPType;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public String getAlgorithm() {
		return algorithm;
	}
	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}
	public int getDigits() {
		return digits;
	}
	public void setDigits(int digits) {
		this.digits = digits;
	}
	public int getInterval() {
		return interval;
	}
	public void setInterval(int interval) {
		this.interval = interval;
	}
	public long getCounter() {
		return counter;
	}
	public void setCounter(long counter) {
		this.counter = counter;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public long getWfMobileIDSoftOTPIssuanceId() {
		return wfMobileIDSoftOTPIssuanceId;
	}
	public void setWfMobileIDSoftOTPIssuanceId(long wfMobileIDSoftOTPIssuanceId) {
		this.wfMobileIDSoftOTPIssuanceId = wfMobileIDSoftOTPIssuanceId;
	}

	

}
