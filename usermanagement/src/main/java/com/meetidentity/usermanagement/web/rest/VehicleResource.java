package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.VehicleService;
import com.meetidentity.usermanagement.web.rest.model.VehicleClassification;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/vehicle")
public class VehicleResource {
	
	@Autowired
	private VehicleService vehicleRegistrationService;

	
	@GetMapping("/vehicleClassifications")
	public List<VehicleClassification> getVehicleClassifications(@PathVariable("organizationName") String organizationName) {
		return vehicleRegistrationService.getVehicleClassifications(organizationName);
	}
	

	
}
