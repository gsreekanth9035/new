/**
 * View Models used by Spring MVC REST controllers.
 */
package com.meetidentity.usermanagement.web.rest.model;
