package com.meetidentity.usermanagement.web.rest.model;

public class ExpressEnrollmentResponse {

	private Long id;
	private String userID;
	private String userName;

	private String status;
	private String userStatus;

	public ExpressEnrollmentResponse() {

	}

	public ExpressEnrollmentResponse(Long id, String userID, String userName, String status, String userStatus) {
		super();
		this.id = id;
		this.userID = userID;
		this.userName = userName;
		this.status = status;
		this.userStatus = userStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

}
