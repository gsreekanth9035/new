package com.meetidentity.usermanagement.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.usermanagement.service.UserVehicleService;
import com.meetidentity.usermanagement.web.rest.model.VehicleRegistration;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/users/{userID}/vehicles")
public class UserVehicleResource {
	
	@Autowired
	private UserVehicleService userVehicleService;

	@PostMapping("/{vin}")
	public ResponseEntity<Void> addVehicleToUser(@PathVariable("organizationName") String organizationName,@PathVariable("userID") Long userID, @PathVariable("vin") String vin) {
		userVehicleService.addVehicleToUser(organizationName, userID, vin);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping
	public List<VehicleRegistration> getVehiclesMappedToUser(@PathVariable("organizationName") String organizationName, @PathVariable("userID") Long userID) {
		return userVehicleService.getVehiclesByUser(organizationName, userID);
	}

}
