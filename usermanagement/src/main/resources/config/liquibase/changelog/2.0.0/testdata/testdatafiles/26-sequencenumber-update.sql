
-- sequence_number
ALTER TABLE `sequence_number` CHANGE COLUMN `created_by` `created_by` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `seq_suffix`;