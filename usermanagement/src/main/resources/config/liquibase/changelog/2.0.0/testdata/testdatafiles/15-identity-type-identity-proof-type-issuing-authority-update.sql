UPDATE `usermanagement`.`identity_type` SET `notes`='front,back,barcode,pdf417,chip' WHERE  `id`=1;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,back,qrcode' WHERE  `id`=2;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,back,qrcode' WHERE  `id`=3;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,back,qrcode' WHERE  `id`=4;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,back,qrcode,barcode,chip' WHERE  `id`=5;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,back,qrcode' WHERE  `id`=6;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,back,qrcode' WHERE  `id`=7;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,back,qrcode' WHERE  `id`=8;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,qrcode' WHERE  `id`=9;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,qrcode' WHERE  `id`=10;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,qrcode' WHERE  `id`=11;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,qrcode' WHERE  `id`=12;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,mrztd1' WHERE  `id`=13;
UPDATE `usermanagement`.`identity_type` SET `notes`='front,back,qrcode' WHERE  `id`=14;

DELETE FROM `usermanagement`.`id_proof_type` WHERE  `id`=3;
DELETE FROM `usermanagement`.`id_proof_type` WHERE  `id`=5;

INSERT INTO `usermanagement`.`id_proof_type` (`id`, `type`, `description`, `organization_id`, `version`, `created_by`, `created_date`) VALUES ('3', 'National ID', 'National ID', '1', '0', 'ADMiN', '2020-12-01 15:55:54');
INSERT INTO `usermanagement`.`id_proof_type` (`id`, `type`, `description`, `organization_id`, `version`, `created_by`, `created_date`) VALUES ('5', 'Voter ID', 'Voter ID', '1', '0', 'ADMiN', '2020-12-01 15:55:54');
INSERT INTO `usermanagement`.`id_proof_issuing_authority` (`id`, `document_type_id`, `issuing_authority`, `version`, `created_by`, `created_date`) VALUES ('4', '3', 'Governemnt of EpicIdentity', '0', 'ADMIN', '2019-11-15 15:01:48');
INSERT INTO `usermanagement`.`id_proof_issuing_authority` (`id`, `document_type_id`, `issuing_authority`, `version`, `created_by`, `created_date`) VALUES ('5', '5', 'Governemnt of EpicIdentity', '0', 'ADMIN', '2019-11-15 15:01:48');

INSERT INTO `usermanagement`.`mobile_pair_policy_config` (`id`, `name`, `value`, `organization_id`, `status`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES ('1', 'ENROLLMENT_TOKEN_EXPIRY', '86400', '1', b'1', 'ADMIN', '2020-11-30 20:00:48', 'ADMIN', '2020-11-30 18:26:43');
