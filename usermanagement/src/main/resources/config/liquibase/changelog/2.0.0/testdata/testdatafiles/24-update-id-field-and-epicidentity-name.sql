-- identity_field
UPDATE identity_field idf SET idf.mandatory = 0 WHERE idf.name ='Contact Number' AND idf.mandatory = 1;

-- device_action_status_relation
DELETE FROM device_action_status_relation  WHERE device_actions_id = 11 AND device_status_id = 5;

-- config_value
UPDATE config_value cv SET cv.value = 'MeetIdentity' WHERE cv.value = 'EpicIdentity';
UPDATE config_value cv SET cv.value = 'MeetIdentity PVC ID' WHERE cv.value = 'EpicIdentity PVC ID';
UPDATE config_value cv SET cv.value = 'MeetIdentity Wallet' WHERE cv.value = 'EpicIdentity Mobile ID';