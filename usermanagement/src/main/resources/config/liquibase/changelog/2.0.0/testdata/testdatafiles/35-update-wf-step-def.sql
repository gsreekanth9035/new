-- wf_step_def
INSERT INTO `wf_step_def` (`name`, `description`, `display_name`, `active`, `step_def_order`, `ref_ui_page`, `ref_table_name`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES ('FINGERPRINT_ROLL_CAPTURE', 'Fingerprint Roll Capture', 'Fingerprint Roll Capture', b'1', 5, NULL, NULL, 0, 'USER', '2019-10-18 14:43:33', NULL, NULL);

-- update stepdef order
UPDATE wf_step_def SET step_def_order = 6 WHERE NAME = 'FINGERPRINT_CAPTURE';
UPDATE wf_step_def SET step_def_order = 7 WHERE NAME = 'SIGNATURE_CAPTURE';
UPDATE wf_step_def SET step_def_order = 8 WHERE NAME = 'ENROLL_SUMMARY';
UPDATE wf_step_def SET step_def_order = 9 WHERE NAME = 'APPROVAL_BEFORE_ISSUANCE';
UPDATE wf_step_def SET step_def_order = 10 WHERE NAME = 'PERSO_AND_ISSUANCE';
UPDATE wf_step_def SET step_def_order = 13 WHERE NAME = 'PKI_CERTIFICATES';
UPDATE wf_step_def SET step_def_order = 16 WHERE NAME = 'POST_PERSO_AND_ISSUANCE';
UPDATE wf_step_def SET step_def_order = 17 WHERE NAME = 'MOBILE_ID_HARDWARE_BACKED_KEYSTORE';
UPDATE wf_step_def SET step_def_order = 18 WHERE NAME = 'MOBILE_ID_ONBOARDING_CONFIG';
UPDATE wf_step_def SET step_def_order = 19 WHERE NAME = 'MOBILE_ID_IDENTITY_ISSUANCE';
