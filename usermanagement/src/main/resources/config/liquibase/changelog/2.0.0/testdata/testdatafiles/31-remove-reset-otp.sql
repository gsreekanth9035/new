DELETE FROM role_device_actions  WHERE device_actions_id = (SELECT id FROM device_actions da WHERE da.name='Reset OTP');
DELETE FROM device_type_actions_mapping WHERE device_actions_id = (SELECT id FROM device_actions da WHERE da.name='Reset OTP');
DELETE FROM device_action_status_relation WHERE device_actions_id = (SELECT id FROM device_actions da WHERE da.name='Reset OTP');
DELETE FROM device_actions WHERE name='Reset OTP';