UPDATE device_profile_config dpc SET enable_master_key =1, enable_admin_key =1, enable_customer_master_key = 1, enable_customer_admin_key=1, dpc.master_key_length=96 WHERE dpc.config_value_id IN (SELECT id FROM config_value WHERE VALUE = 'G+D SCE 7.0 with PIV Applet V 1.0');
UPDATE device_profile_config dpc SET enable_master_key =1, enable_admin_key =1, enable_customer_master_key = 1, enable_customer_admin_key=1, dpc.master_key_length=96 WHERE dpc.config_value_id IN (SELECT id FROM config_value WHERE VALUE = 'NXP JCOP 3 with Unifyia PIV Applet v2.0');
UPDATE device_profile_config dpc SET enable_master_key =1, enable_admin_key =1, enable_customer_master_key = 1, enable_customer_admin_key=1, dpc.master_key_length=96, dpc.is_applet_loading_required=1 WHERE dpc.config_value_id IN (SELECT id FROM config_value WHERE VALUE = 'NXP JCOP 4 with Unifyia PIV Applet V 4.0');

UPDATE device_profile_config dpc SET enable_master_key =1, enable_admin_key =1, enable_customer_master_key = 1, enable_customer_admin_key=1, dpc.enable_ak_is_identical_to_mk_check = 0 WHERE dpc.config_value_id IN (SELECT id FROM config_value WHERE VALUE = 'ID-One PIV v 2.3.4 on Cosmo V7');
UPDATE device_profile_config dpc SET enable_master_key =1, enable_admin_key =1, enable_customer_master_key = 1, enable_customer_admin_key=1, dpc.enable_ak_is_identical_to_mk_check = 0  WHERE dpc.config_value_id IN (SELECT id FROM config_value WHERE VALUE = 'Thales IDPrime PIV v3.0');


UPDATE device_profile_config dpc SET dpc.enable_customer_management_key = 1, dpc.is_rfid_card = 1 WHERE dpc.config_value_id IN (SELECT id FROM config_value WHERE VALUE = 'MIFARE Classic 1k');
UPDATE device_profile_config dpc SET dpc.enable_customer_management_key = 1, dpc.is_rfid_card = 1 WHERE dpc.config_value_id IN (SELECT id FROM config_value WHERE VALUE = 'DESFire EV1/EV2');
UPDATE device_profile_config dpc SET dpc.enable_customer_management_key = 1, dpc.is_rfid_card = 1 WHERE dpc.config_value_id IN (SELECT id FROM config_value WHERE VALUE = 'MIFARE Classic 4k');

UPDATE device_profile_config dpc SET dpc.enable_customer_admin_key = 1 WHERE dpc.config_value_id IN (SELECT id FROM config_value WHERE VALUE = 'YubiKey 5');

-- permission
INSERT INTO `permission` (`id`, `name`, `display_name`, `description`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (39, 'Menu_Factory_Reset', 'Menu Factory Reset', 'Factory Reset', 0, 'admin', '2020-06-17 12:12:12', NULL, NULL);

-- role_permission
INSERT INTO `role_permission` (`role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (1, 39, 0, 'Admin', '2021-10-27 20:04:42', NULL, NULL);
INSERT INTO `role_permission` (`role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (2, 39, 0, 'Admin', '2021-10-27 20:04:42', NULL, NULL);
INSERT INTO `role_permission` (`role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (3, 39, 0, 'Admin', '2021-10-27 20:04:42', NULL, NULL);

-- device_profile_key_manager
UPDATE device_profile_key_manager dpkm SET dpkm.master_key = 'cc+c4SDvwnSvYvfRmlidyIv6o9BkSTniyR4fqJrrOKdxz5zhIO/CdK9i99GaWJ3Ii/qj0GRJOeLJHh+omus4p3HPnOEg78J0r2L30ZpYnciL+qPQZEk54skeH6ia6zinxEXe27obUfEuj2Apojbnwg==', dpkm.admin_key = 'cc+c4SDvwnSvYvfRmlidyIv6o9BkSTniyR4fqJrrOKfERd7buhtR8S6PYCmiNufC', dpkm.customer_master_key = 'cc+c4SDvwnSvYvfRmlidyIv6o9BkSTniyR4fqJrrOKdxz5zhIO/CdK9i99GaWJ3Ii/qj0GRJOeLJHh+omus4p3HPnOEg78J0r2L30ZpYnciL+qPQZEk54skeH6ia6zinxEXe27obUfEuj2Apojbnwg==', dpkm.customer_master_key = 'cc+c4SDvwnSvYvfRmlidyIv6o9BkSTniyR4fqJrrOKdxz5zhIO/CdK9i99GaWJ3Ii/qj0GRJOeLJHh+omus4p3HPnOEg78J0r2L30ZpYnciL+qPQZEk54skeH6ia6zinxEXe27obUfEuj2Apojbnwg==', dpkm.customer_admin_key = 'cc+c4SDvwnSvYvfRmlidyIv6o9BkSTniyR4fqJrrOKfERd7buhtR8S6PYCmiNufC' where id = 2;