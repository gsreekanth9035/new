-- config
INSERT INTO `config` (`organization_id`, `config_type_id`, `name`, `description`, `display_type`, `active`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (1, 1, 'TRANSPARENT_PHOTO_ALLOWED', 'Transparent image allowed', 'CHECKBOX', b'1', 0, 'USER', '2021-05-03 11:03:04', NULL, NULL);
 
-- config_value
INSERT INTO `config_value` (`config_id`, `value`, `active`, `parent_config_value_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (59, 'Y', b'1', NULL, 0, 'USER', '2021-05-03 11:06:28', NULL, NULL);
INSERT INTO `config_value` (`config_id`, `value`, `active`, `parent_config_value_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (59, 'N', b'1', NULL, 0, 'USER', '2021-05-03 11:05:37', NULL, NULL);
