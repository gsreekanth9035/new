-- config-value
INSERT INTO `config_value` (`config_id`, `value`, `active`, `parent_config_value_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES ( 4, 'NXP JCOP 4 with Unifyia PIV Applet V 4.0', b'1', 40, 0, 'USER', '2020-12-24 13:55:23', NULL, NULL);

UPDATE config_value SET value = 'Mobile' WHERE value = 'Mobile Identity Verification';
UPDATE config_value SET value = 'Plastic' WHERE value = 'PVC Identity verification';
UPDATE config_value SET value = 'MIFARE DESFire' WHERE value = 'Identity';
