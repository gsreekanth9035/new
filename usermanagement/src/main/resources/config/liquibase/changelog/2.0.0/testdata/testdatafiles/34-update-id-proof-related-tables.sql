-- id_proof_issuing_authority
UPDATE id_proof_issuing_authority ida set ida.org_identities_id = 9 WHERE id =1;
UPDATE id_proof_issuing_authority ida set ida.org_identities_id = 13 WHERE id =2;
UPDATE id_proof_issuing_authority ida set ida.org_identities_id = 4 WHERE id =3;
UPDATE id_proof_issuing_authority ida set ida.org_identities_id = 3 WHERE id =4;
UPDATE id_proof_issuing_authority ida set ida.org_identities_id = 7 WHERE id =5;
UPDATE id_proof_issuing_authority ida SET ida.issuing_authority='Government of MeetIdentity' WHERE issuing_authority ='Governemnt of EpicIdentity';

-- user_id_proof_document
UPDATE user_id_proof_document SET org_identities_id = 9 WHERE org_identities_id = 1;
UPDATE user_id_proof_document SET org_identities_id = 13 WHERE org_identities_id = 2;
UPDATE user_id_proof_document SET org_identities_id = 7 WHERE org_identities_id = 5;
UPDATE user_id_proof_document SET org_identities_id = 3 WHERE org_identities_id = 4 AND issuing_authority_id = 3;
UPDATE user_id_proof_document SET org_identities_id = 4 WHERE org_identities_id = 3 AND issuing_authority_id = 4;

-- identity_type
UPDATE identity_type idd SET idd.file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='PIV and PIV 1';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='Employee ID';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='National ID';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='Driving License';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='Vehicle Identification ID';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='Health ID';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='Voter ID';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='Student ID';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg,application/pdf' WHERE idd.name='Birth Certificate';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='Marriage Certificate';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='Divorce Certificate';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='Death Certificate';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='Passport';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='Health Service ID';
UPDATE identity_type idd SET file_formats = 'image/png,image/jpg,image/jpeg' WHERE idd.name='Permanent Resident ID';