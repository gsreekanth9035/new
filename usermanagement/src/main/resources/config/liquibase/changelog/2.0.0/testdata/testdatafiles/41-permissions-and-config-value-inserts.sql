-- permission
INSERT INTO `permission` (`id`, `name`, `display_name`, `description`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (38, 'Menu_ID_Reader', 'Menu ID Reader', 'Id Reader', 0, 'admin', '2020-06-17 12:12:12', NULL, NULL);

-- role_permission
INSERT INTO `role_permission` (`role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (1, 38, 0, 'Admin', '2021-06-17 19:26:12', NULL, NULL);
INSERT INTO `role_permission` (`role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (2, 38, 0, 'Admin', '2021-06-17 19:26:12', NULL, NULL);
INSERT INTO `role_permission` (`role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (3, 38, 0, 'Admin', '2021-09-02 14:38:45', NULL, NULL);
INSERT INTO `role_permission` (`role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (4, 38, 0, 'Admin', '2021-09-02 14:38:47', NULL, NULL);

-- user_biometric
UPDATE user_biometric SET FORMAT='png' WHERE FORMAT='BASE64';

-- changing names for mifare, desfire
UPDATE config_value cv SET cv.value = 'MIFARE Classic 1k' WHERE cv.value = 'Mifare Classic 1k';
UPDATE config_value cv SET cv.value = 'DESFire EV1/EV2' WHERE cv.value = 'Desfire EV1/EV2';
INSERT INTO `config_value` (`config_id`, `value`, `active`, `parent_config_value_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (4, 'MIFARE Classic 4k', b'1', 161, 0, 'USER', '2020-05-21 11:16:46', NULL, NULL);


INSERT INTO `device_actions` (`id`, `name`, `description`, `active`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (13, 'Read ID', NULL, b'1', 0, 'USER', '2020-07-18 15:46:32', NULL, NULL);
INSERT INTO `device_action_status_relation` (`device_status_id`, `device_actions_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (2, 13, 0, 'USER', '2020-07-18 15:49:13', NULL, NULL);

INSERT INTO `device_type_actions_mapping` (`device_type_id`, `device_actions_id`, `is_connected`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (1, 13, b'1', 0, 'USER', '2020-07-18 16:16:24', NULL, NULL);

INSERT INTO `role_device_actions` (`role_id`, `device_actions_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (1, 13, 0, 'USER', '2020-07-18 15:49:49', NULL, NULL);
INSERT INTO `role_device_actions` (`role_id`, `device_actions_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (2, 13, 0, 'USER', '2020-07-18 15:49:49', NULL, NULL);
INSERT INTO `role_device_actions` (`role_id`, `device_actions_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (3, 13, 0, 'USER', '2020-07-18 15:49:49', NULL, NULL);
INSERT INTO `role_device_actions` (`role_id`, `device_actions_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (4, 13, 0, 'USER', '2020-07-18 15:49:49', NULL, NULL);
