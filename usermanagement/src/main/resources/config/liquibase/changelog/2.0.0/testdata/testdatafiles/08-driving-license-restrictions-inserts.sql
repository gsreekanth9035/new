-- driving_license_restrictions
INSERT INTO `driving_license_restriction` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (1, '1', 'None', 1, 'ADMIN', '2019-12-04 15:19:41', 'ADMIN', '2019-12-04 15:19:45');
INSERT INTO `driving_license_restriction` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (2, '2', 'Glasses', 1, 'ADMIN', '2019-12-04 15:19:41', 'ADMIN', '2019-12-04 15:19:45');
INSERT INTO `driving_license_restriction` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (3, '3', 'Prosthesis', 1, 'ADMIN', '2019-12-04 15:19:41', 'ADMIN', '2019-12-04 15:19:45');
INSERT INTO `driving_license_restriction` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (4, '4', 'Hypertension', 1, 'ADMIN', '2019-12-04 15:19:41', 'ADMIN', '2019-12-04 15:19:45');
INSERT INTO `driving_license_restriction` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (5, '5', 'Diabetes', 1, 'ADMIN', '2019-12-04 15:19:41', 'ADMIN', '2019-12-04 15:19:45');

