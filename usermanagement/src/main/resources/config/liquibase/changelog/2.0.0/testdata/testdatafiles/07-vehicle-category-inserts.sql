-- vehicle_categories
INSERT INTO `vehicle_category` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (1, 'Tractor', 'Tractor', 1, 'ADMIN', '2019-12-04 15:19:41', 'ADMIN', '2019-12-04 15:19:45');
INSERT INTO `vehicle_category` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (2, 'Motor Cycle/Scooter', 'Motor Cycle/Scooter', 1, 'ADMIN', '2019-12-04 15:19:42', 'ADMIN', '2019-12-04 15:19:45');
INSERT INTO `vehicle_category` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (3, 'Car', 'Car', 1, 'ADMIN', '2019-12-04 15:19:43', 'ADMIN', '2019-12-04 15:19:46');
INSERT INTO `vehicle_category` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (4, 'Convertible Car', 'Convertible Car', 1, 'ADMIN', '2019-12-04 15:19:43', 'ADMIN', '2019-12-04 15:19:46');
INSERT INTO `vehicle_category` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (5, 'Truck', 'Truck', 1, 'ADMIN', '2019-12-04 15:19:41', 'ADMIN', '2019-12-04 15:19:45');
INSERT INTO `vehicle_category` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (6, 'Crane', 'Crane', 1, 'ADMIN', '2019-12-04 15:19:42', 'ADMIN', '2019-12-04 15:19:45');
INSERT INTO `vehicle_category` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (7, 'Special Transport', 'Special Transport', 1, 'ADMIN', '2019-12-04 15:19:43', 'ADMIN', '2019-12-04 15:19:46');
INSERT INTO `vehicle_category` (`id`, `type`, `description`, `organization_id`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (8, 'Bus', 'Bus', 1, 'ADMIN', '2019-12-04 15:19:43', 'ADMIN', '2019-12-04 15:19:46');

UPDATE `usermanagement`.`vehicle_category` SET `vehicle_classification_id`='3' WHERE  `id`=1;
UPDATE `usermanagement`.`vehicle_category` SET `vehicle_classification_id`='4' WHERE  `id`=2;
UPDATE `usermanagement`.`vehicle_category` SET `vehicle_classification_id`='1' WHERE  `id`=3;
UPDATE `usermanagement`.`vehicle_category` SET `vehicle_classification_id`='1' WHERE  `id`=4;
UPDATE `usermanagement`.`vehicle_category` SET `vehicle_classification_id`='3' WHERE  `id`=5;
UPDATE `usermanagement`.`vehicle_category` SET `vehicle_classification_id`='3' WHERE  `id`=6;
UPDATE `usermanagement`.`vehicle_category` SET `vehicle_classification_id`='2' WHERE  `id`=7;
UPDATE `usermanagement`.`vehicle_category` SET `vehicle_classification_id`='2' WHERE  `id`=8;