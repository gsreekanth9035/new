-- permission
INSERT INTO `permission` (`name`, `display_name`, `description`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES ('Schedule_Appointment', 'Schedule Appointment', 'Schedule Appointment', 0, 'admin', '2020-01-08 12:51:26', NULL, NULL);

UPDATE org_branding_policy_config SET contact_us_support_email = 'uia.support@unifyia.com'; 