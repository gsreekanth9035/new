-- device_actions
INSERT INTO `device_actions` (`id`, `name`, `active`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (13, 'Reset OTP', b'1', 0, 'USER', '2020-07-18 15:46:32', NULL, NULL);

-- device_type_actions_mapping
INSERT INTO `device_type_actions_mapping` (`id`, `device_type_id`, `device_actions_id`, `is_connected`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (28, 3, 13, b'1', 0, 'USER', '2020-04-21 14:15:37', NULL, NULL);

--  role_device_actions
INSERT INTO `role_device_actions` (`id`, `role_id`, `device_actions_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (21, 1, 13, 0, 'USER', '2020-07-18 15:49:49', NULL, NULL);
INSERT INTO `role_device_actions` (`id`, `role_id`, `device_actions_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (22, 2, 13, 0, 'USER', '2020-07-18 15:49:49', NULL, NULL);
INSERT INTO `role_device_actions` (`id`, `role_id`, `device_actions_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (23, 3, 13, 0, 'USER', '2020-07-18 15:49:49', NULL, NULL);
INSERT INTO `role_device_actions` (`id`, `role_id`, `device_actions_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (24, 4, 13, 0, 'USER', '2020-07-18 15:49:49', NULL, NULL);

-- device_action_status_relation
INSERT INTO `device_action_status_relation` (`id`, `device_status_id`, `device_actions_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (18, 2, 13, 0, 'USER', '2020-04-21 10:49:41', NULL, NULL);
