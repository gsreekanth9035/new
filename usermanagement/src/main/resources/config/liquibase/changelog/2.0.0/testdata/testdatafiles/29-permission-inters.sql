-- permission
INSERT INTO `permission` (`id`, `name`, `display_name`, `description`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (35, 'Menu_Administrator', 'Menu Administrator', 'Administrator', 0, 'admin', '2020-06-17 12:12:12', NULL, NULL);
INSERT INTO `permission` (`id`, `name`, `display_name`, `description`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (36, 'Menu_MobileServiceBranding', 'Menu MobileService Branding', 'MobileService Branding', 0, 'admin', '2020-06-17 12:12:12', NULL, NULL);

-- role_permission
INSERT INTO `role_permission` (`role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (1, 35, 0, 'Admin', '2021-05-17 11:07:08', NULL, NULL);
INSERT INTO `role_permission` (`role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (1, 36, 0, 'Admin', '2021-06-17 19:26:12', NULL, NULL);
