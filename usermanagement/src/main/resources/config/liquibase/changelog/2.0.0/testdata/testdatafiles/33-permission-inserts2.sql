-- permission
INSERT INTO `permission` (`id`, `name`, `display_name`, `description`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (37, 'Menu_Oauth_Client_RedirectURI', 'Menu_Oauth_Client_RedirectURI', 'Client RedirectURI', 0, 'admin', '2020-06-17 12:12:12', NULL, NULL);

-- role_permission
INSERT INTO `role_permission` (`role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (1, 37, 0, 'Admin', '2021-05-17 11:07:08', NULL, NULL);
