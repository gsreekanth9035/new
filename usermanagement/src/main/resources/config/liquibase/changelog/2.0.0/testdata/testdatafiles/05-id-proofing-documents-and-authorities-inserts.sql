-- id_proof_type
INSERT INTO `id_proof_type` (`id`, `type`, `description`, `organization_id`,  `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (1, 'Birth Certificate', 'Date of Birth', 1, 0, 'ADMIN', '2019-11-06 17:41:46', NULL, NULL);
INSERT INTO `id_proof_type` (`id`, `type`, `description`, `organization_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (2, 'Passport', 'Passport', 1, 0, 'ADMIN', '2019-11-06 17:42:16', NULL, NULL);
INSERT INTO `id_proof_type` (`id`, `type`, `description`, `organization_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (3, 'Credential ID', 'ID Card', 1, 0, 'ADMIN', '2019-11-06 17:42:35', NULL, NULL);
INSERT INTO `id_proof_type` (`id`, `type`, `description`, `organization_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (4, 'Driving License', 'Driving License', 1, 0, 'ADMIN', '2019-11-06 17:42:56', NULL, NULL);
INSERT INTO `id_proof_type` (`id`, `type`, `description`, `organization_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (5, 'Others', 'Others', 1, 0, 'ADMIN', '2019-11-06 22:34:30', NULL, NULL);

-- id_proof_issuing_authority
INSERT INTO `id_proof_issuing_authority` (`id`, `document_type_id`, `issuing_authority`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (1, 2, 'Ministry of External Affairs', 0, 'ADMIN', '2019-11-15 14:59:49', NULL, NULL);
INSERT INTO `id_proof_issuing_authority` (`id`, `document_type_id`, `issuing_authority`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (2, 1, 'Municipal Corporation', 0, 'ADMIN', '2019-11-15 15:02:33', NULL, NULL);
INSERT INTO `id_proof_issuing_authority` (`id`, `document_type_id`, `issuing_authority`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (3, 4, 'RTA', 0, 'ADMIN', '2019-11-15 15:01:48', NULL, NULL);
