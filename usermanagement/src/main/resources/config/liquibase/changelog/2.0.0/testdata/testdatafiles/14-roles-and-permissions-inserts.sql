-- Permission
INSERT INTO `permission` (`id`, `name`, `display_name`, `description`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (33, 'Menu_OnboardUsers_Via_MobileID', 'OnboardUsers Via MobileID', 'OnboardUsers Via MobileID', 0, 'admin', '2020-01-08 12:51:26', NULL, NULL);
INSERT INTO `permission` (`id`, `name`, `display_name`, `description`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES (34, 'Menu_Express_Self_Service', 'Express Self Service', 'Express Self Service', 0, 'admin', '2020-01-08 12:51:26', NULL, NULL);

-- Role_Permission
INSERT INTO `role_permission` ( `role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES ( 4, 33, 0, 'Admin', '2020-06-29 20:03:36', NULL, NULL);
INSERT INTO `role_permission` ( `role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES ( 1, 34, 0, 'Admin', '2020-06-29 20:03:36', NULL, NULL);
INSERT INTO `role_permission` ( `role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES ( 2, 34, 0, 'Admin', '2020-06-29 20:03:36', NULL, NULL);
INSERT INTO `role_permission` ( `role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES ( 3, 34, 0, 'Admin', '2020-06-29 20:03:36', NULL, NULL);
INSERT INTO `role_permission` ( `role_id`, `permission_id`, `version`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES ( 4, 34, 0, 'Admin', '2020-06-29 20:03:36', NULL, NULL);
