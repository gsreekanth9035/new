import { AddDeviceProfileModule } from 'app/device-profile/add/add-device-profile.module';

describe('AddDeviceProfileModule', () => {
  let addDeviceProfileModule: AddDeviceProfileModule;

  beforeEach(() => {
    addDeviceProfileModule = new AddDeviceProfileModule();
  });

  it('should create an instance', () => {
    expect(addDeviceProfileModule).toBeTruthy();
  });
});
