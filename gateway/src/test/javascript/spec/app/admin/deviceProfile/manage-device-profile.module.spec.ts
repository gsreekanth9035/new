import { ManageDeviceProfileModule } from 'app/device-profile/manage/manage-device-profile.module';

describe('ManageDeviceProfileModule', () => {
  let manageDeviceProfileModule: ManageDeviceProfileModule;

  beforeEach(() => {
    manageDeviceProfileModule = new ManageDeviceProfileModule();
  });

  it('should create an instance', () => {
    expect(manageDeviceProfileModule).toBeTruthy();
  });
});
