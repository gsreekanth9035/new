import { CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { User } from 'app/enroll/user.model';
import { EnrollService } from 'app/enroll/enroll.service';
import { Observable } from 'rxjs/Observable';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { EnrollComponent } from "app/enroll/enroll.component";
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { DebugElement } from "@angular/core";
import { By, BrowserModule } from '@angular/platform-browser';
import { EnrollModule } from 'app/enroll/enroll.module';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ENROLL_ROUTE } from 'app/enroll/enroll.route';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Principal } from 'app/core';

class MockedSevice extends EnrollService{
  registerUser(user: User): Observable<any> {
    return Observable.of('Mocked reg user')
  }  

//  captureFP(): Observable<any> {
//    return Observable.of('MockedService');
//  }
}
class MockedPricipalService extends Principal{
   user = new User();
  identity(force?: boolean): Promise<any> {
    let promise = Promise.resolve(this.user);
    return promise;
  }

}
describe('EnrollComponent test cases', () => {
    let component: EnrollComponent;
    let fixture: ComponentFixture<EnrollComponent>;
    let enrollService:EnrollService;
    let principal: Principal;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
      //declarations: [ EnrollComponent],
        imports:[
            BrowserModule,FormsModule,ReactiveFormsModule,
            RouterTestingModule,HttpClientTestingModule,EnrollModule
        ],
        providers: [
          {provide: EnrollService, useClass: MockedSevice},
          {provide: Principal, useClass: MockedPricipalService},
          ],
        schemas: [ CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA],
      })
      .compileComponents();
    }));
  
    beforeEach(async(()  => {
      const fixture = TestBed.createComponent(EnrollComponent);
      component = fixture.componentInstance;
      enrollService = fixture.debugElement.injector.get(EnrollService);
      principal = fixture.debugElement.injector.get(Principal);
    }));
    
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should inject enroll servcie',
      inject([EnrollService],(injectService:EnrollService)=>{
      expect(injectService).toBeInstanceOf(MockedSevice);
      expect(injectService).toEqual(enrollService);
    }))

//    it ('should test captureFP method with spyOn',()=>{
//      spyOn(enrollService,'captureFP').and.returnValue({ subscribe: () => {} });
//      component.captureFP();
//      expect(enrollService.captureFP()).toBeDefined();
//     })

//    it ('should test captureFP method with Mocked Service',()=>{
//      enrollService.captureFP().subscribe((result)=>{
//        expect(result).toEqual('MockedService');
//      })
//    })

    it('should check state field is disabled',()=>{
      let stateOfOrigin=component.enrollForm.controls['stateOfOrigin'];
      expect(stateOfOrigin.disabled).toBeTruthy();
    })
   
    it('should check email field validity',()=>{
      let email = component.enrollForm.controls['email'];
    
      expect(email.value).toBe("test@gmail.com");
     // expect(email.valid).toBeFalsy();
     // let errors={};
     // errors=email.errors || {};
    // expect(errors['required']).toBeTruthy();
     })

     it('should check email validty',()=>{
      component.enrollForm.controls['email'].setValue('k.bharath@abc.in');
      expect(component.enrollForm.controls['email'].valid).toBeTruthy();
     })
  });
