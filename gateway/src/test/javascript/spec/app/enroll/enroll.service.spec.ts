import { TestBed, inject } from '@angular/core/testing';
import { EnrollService } from 'app/enroll/enroll.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { User } from 'app/enroll/user.model';
import { UserAttribute } from 'app/enroll/userattribute.model';
import { UserBiometric } from 'app/enroll/userbiometric.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

describe('Enroll Service', () => {
    let service: EnrollService;
    let httpMock: HttpTestingController;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [EnrollService],
            imports: [HttpClientTestingModule]
        }).compileComponents();

        service = TestBed.get(EnrollService);
        httpMock = TestBed.get(HttpTestingController);
    });

    afterEach(
        inject([HttpTestingController], (httpMockk: HttpTestingController) => {
            httpMockk.verify();
        })
    );

    it(
        'should test dependency injection',
        inject([EnrollService], (injectService: EnrollService) => {
            expect(service).toBe(injectService);
        })
    );

    it(
        'should test register User method',
        inject([HttpClient, HttpTestingController], (http: HttpClient, httpMockk: HttpTestingController) => {
            const dummyUser = new User();
            const userAttribute1 = new UserAttribute();
            const mock = httpMockk;
            // userAttribute1.push(new UserAttribute('firstName','Kalyani'));
            dummyUser.userAttribute = userAttribute1;
            service.registerUser(dummyUser).subscribe(result => {
                expect(result).toBeTruthy();
            });
            // const request = httpMock.expectOne(service.url);
            // expect(request.request.method).toEqual('POST');
            // expect(request.request.detectContentTypeHeader).toBeTruthy();
            // expect(request.request.responseType).toBe('json');
            // request.flush(dummyUser);
        })
    );
    /*      it('should test the captureFP method',()=>{
        let dummyData:UserBiometric=new UserBiometric();
        dummyData.data=[1234,12345];
        dummyData.format='asad',
        dummyData.imageQuality=123456;
        dummyData.position='asdfg';
        dummyData.type='sdfgh';
         service.captureFP().subscribe(result=>{
            expect(result).toEqual(dummyData);
          })
          const request = httpMock.expectOne('https://localhost:8443/SGIFPCapture',
          'Timeout=10000&Quality=50&templateFormat=ISO');
          expect(request.request.method).toEqual('POST');
          request.flush(dummyData);
      })    */

    /*      it('should throw an error message when captureFP returns an error',()=>{
        service.captureFP().subscribe(response => fail('should fail with 500 status'),
        (error: HttpErrorResponse) => {
        expect(error).toBeTruthy();
        expect(error.status).toEqual(500);
          })
       const request = httpMock.expectOne('https://localhost:8443/SGIFPCapture',
       'Timeout=10000&Quality=50&templateFormat=ISO');
       expect(request.request.method).toEqual('POST');
       request.flush({ errorMessage: 'Error!!' }, { status: 500, statusText: 'Server Error' });
      })    */
});
