import { IssuanceComponent } from 'app/issuance/issuance.component';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { IssuanceService } from 'app/issuance/issuance.service';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { IssuanceModule } from 'app/issuance/issuance.module';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

fdescribe('Issuance Component',()=>{
    let component: IssuanceComponent;
    let fixture: ComponentFixture<IssuanceComponent>;
    let issuanceService:IssuanceService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
          imports:[
              BrowserModule,FormsModule,ReactiveFormsModule,
              RouterTestingModule,HttpClientTestingModule,IssuanceModule
          ],
          providers: [IssuanceService ],
          schemas: [ CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA],
        })
        .compileComponents();
      }));

      beforeEach(async(()  => {
        const fixture = TestBed.createComponent(IssuanceComponent);
        component = fixture.componentInstance;
        issuanceService = fixture.debugElement.injector.get(IssuanceService);
       // principal = fixture.debugElement.injector.get(Principal);
      }));
      it('should create', () => {
        expect(component).toBeTruthy();
      });
})
