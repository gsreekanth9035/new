import { IssuanceComponent } from "app/issuance/issuance.component";
import { IssuanceService } from 'app/issuance/issuance.service';
import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('Issuance Service',()=>{

    let issuanceService:IssuanceService;
    let httpMock:HttpTestingController;

    beforeEach(async() => {
        TestBed.configureTestingModule({
            providers:[IssuanceService],
            imports:[HttpClientTestingModule]
        }).compileComponents();

        issuanceService = TestBed.get(IssuanceService);
       httpMock = TestBed.get(HttpTestingController);
    })

     afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
        httpMock.verify();
      }));

      it('should test dependency injection',
      inject([IssuanceService],(injectService:IssuanceService) => {
      expect(issuanceService).toBe(injectService);
    }))

    it('should call the getCardList method',()=>{

    })
})