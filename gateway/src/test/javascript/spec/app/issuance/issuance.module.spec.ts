import { IssuanceModule } from 'app/issuance/issuance.module';

describe('IssuanceModule', () => {
  let issuanceModule: IssuanceModule;

  beforeEach(() => {
    issuanceModule = new IssuanceModule();
  });

  it('should create an instance', () => {
    expect(issuanceModule).toBeTruthy();
  });
});
