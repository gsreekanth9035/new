package com.meetidentity.gateway.client;

import org.springframework.context.annotation.Bean;

import com.meetidentity.gateway.security.oauth2.AuthorizationHeaderUtil;

import feign.RequestInterceptor;

public class OAuth2InterceptedFeignConfiguration {

    @Bean(name = "oauth2RequestInterceptor")
    public RequestInterceptor getOAuth2RequestInterceptor(AuthorizationHeaderUtil authorizationHeaderUtil) {
        return new TokenRelayRequestInterceptor(authorizationHeaderUtil);
    }
}
