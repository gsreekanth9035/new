package com.meetidentity.gateway.web.rest;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.gateway.web.feign.client.APDUFeignClient;
import com.meetidentity.gateway.web.feign.client.BiometricVerificationFeignClient;
import com.meetidentity.gateway.web.feign.client.UserManagementFeignClient;
import com.meetidentity.gateway.web.feign.client.model.Permission;
import com.meetidentity.gateway.web.feign.client.model.User;
import com.meetidentity.gateway.web.rest.model.BiometricVerificationResponse;
import com.meetidentity.gateway.web.rest.model.CustomerBranding;
import com.meetidentity.gateway.web.rest.model.Group;
import com.meetidentity.gateway.web.rest.model.IdentityProviderUser;
import com.meetidentity.gateway.web.rest.model.LastSeen;
import com.meetidentity.gateway.web.rest.model.MobileCredential;
import com.meetidentity.gateway.web.rest.model.MobileCredentialTemplate;
import com.meetidentity.gateway.web.rest.model.PrintedCredential;
import com.meetidentity.gateway.web.rest.model.ResultPage;
import com.meetidentity.gateway.web.rest.model.Role;
import com.meetidentity.gateway.web.rest.model.TokenDetails;
import com.meetidentity.gateway.web.rest.model.UserDetails;
import com.meetidentity.gateway.web.rest.model.UserDevice;
import com.meetidentity.gateway.web.rest.model.UserDeviceInfo;
import com.meetidentity.gateway.web.rest.model.UserIdentityData;
import com.meetidentity.gateway.web.rest.model.VehicleRegistration;
import com.meetidentity.gateway.web.rest.model.neuro.bioBeans.UserBioInfo;

/**
 * REST controller for managing Gateway configuration.
 */
@RestController
@RequestMapping("/api/v1")
public class MobileServiceResource {
	
	@Autowired
	private APDUFeignClient apduFeignClient;

	@Autowired
	private UserManagementFeignClient userManagementFeignClient;

	@Autowired
	private BiometricVerificationFeignClient biometricVerificationFeignClient;

	
	//Apdu Service
	
	@PostMapping("/devices")
	public ResponseEntity<User> registerMobileDevice(@RequestBody UserDevice requestUserDevice)  {
		return apduFeignClient.registerMobileDevice(requestUserDevice);	
	}	
	
	@GetMapping("/mobile-device/(deviceID)/token")
	public TokenDetails generateMobileIDUserTokens(@PathVariable("deviceID") String deviceID, @RequestParam("x-tentant") String organizationName)  {
		return apduFeignClient.generateMobileIDUserTokens(deviceID, organizationName);	
	}
	
	@GetMapping("/organizations/{organizationName}/orgIssuerCertificate")
	public UserDeviceInfo getOrgIssuerCertificate(@PathVariable("organizationName") String organizationName) {
		return apduFeignClient.getOrgIssuerCertificate(organizationName);
	}	

	@GetMapping("/organizations/{organizationName}/mobile-devices/{deviceID}/users/{userID}/credentials/initial")
	public ResponseEntity<List<MobileCredential>> getCredentialsByDeviceAndUserID_Initial(@PathVariable("organizationName") String organizationName,@PathVariable("deviceID") String deviceID, @PathVariable("userID") Long userID) {
		return apduFeignClient.getCredentialsByDeviceAndUserID_Initial(organizationName,deviceID, userID);
	}

	@GetMapping("/organizations/{organizationName}/mobile-devices/{deviceID}/users/{userID}/credentials")
	public ResponseEntity<List<MobileCredential>> getCredentialsByDeviceAndUserID(@PathVariable("organizationName") String organizationName,@PathVariable("deviceID") String deviceID, @PathVariable("userID") Long userID) {
		return apduFeignClient.getCredentialsByDeviceAndUserID(organizationName,deviceID, userID);
	}  
	
	@DeleteMapping("/organizations/{organizationName}/mobile-devices/{deviceID}/credentials/{credentialTemplateID}")
	public ResponseEntity<Void> deleteCredentialByTemplateID(@PathVariable("organizationName") String organizationName,@PathVariable("deviceID") String deviceID,
			@PathVariable("credentialTemplateID") Long credentialTemplateID) {
		return apduFeignClient.deleteCredentialByTemplateID(organizationName, deviceID, credentialTemplateID);
	}
	
	@PostMapping("/devices/{appName}/users/{userID}/sendNotification")
	public ResponseEntity<Void> sendNotificationByID(@PathVariable("appName") String appName,@PathVariable("userID") Long userID,@RequestBody UserDevice requestUserDevice){
		return apduFeignClient.sendNotificationByID(appName,userID,requestUserDevice);
	}
	
	@PostMapping("/organizations/{organizationName}/mobile-devices/{deviceID}/credentials/deleteSeletedCredentials")
	public ResponseEntity<Void> deleteCredentialByTemplateIDs(@PathVariable("organizationName") String organizationName,@PathVariable("deviceID") String deviceID,
			@RequestBody MobileCredentialTemplate mobileCredentialTemplate) {
		return apduFeignClient.deleteCredentialByTemplateIDs(organizationName,deviceID, mobileCredentialTemplate);
	}
		
	@PostMapping("/organizations/{organizationName}/mobile-devices/{deviceID}/credentials/deleteSeletedCredentialsWithUnpair")
	public ResponseEntity<Void> deleteCredentialByTemplateIDsWithUnpair(@PathVariable("organizationName") String organizationName,@PathVariable("deviceID") String deviceID,
			@RequestBody MobileCredentialTemplate mobileCredentialTemplate) {
		return apduFeignClient.deleteCredentialByTemplateIDsWithUnpair(organizationName,deviceID, mobileCredentialTemplate);
	}
	
	@GetMapping("/organizations/{organizationName}/mobile-devices/{deviceID}/credentials/sync")
	public ResponseEntity<List<MobileCredential>> generateOrRetrieveCredentialsByDeviceID_sync(@PathVariable("organizationName") String organizationName,@PathVariable("deviceID") String deviceID) {
		return apduFeignClient.generateOrRetrieveCredentialsByDeviceID_sync(organizationName,deviceID);
	}	
	
	@GetMapping("/organizations/{organizationName}/{requestFrom}/users/{userID}/credentials/{credentialTemplateID}")
	public PrintedCredential getOrIssueCredentialByTemplateID(@RequestHeader HttpHeaders httpHeaders, @PathVariable("organizationName") String organizationName, @PathVariable("requestFrom") String requestFrom, @PathVariable("userID") Long userID, @PathVariable("credentialTemplateID") Long credentialTemplateID) {
		return apduFeignClient.getCredentialByTemplateID(httpHeaders, organizationName, requestFrom, userID, credentialTemplateID);
	}	
	
	@PostMapping("/devices/mobileService")
	public ResponseEntity<UserDetails> registerMobileServiceDevice(@RequestBody UserDevice requestUserDevice)  {
		return apduFeignClient.registerMobileServiceDevice(requestUserDevice);	
	}
	
	@GetMapping("/organizations/{organizationName}/userIdentity/{identitySerialNumber}")
	public ResponseEntity<List<UserIdentityData>> getUserIdentity(@PathVariable("organizationName") String organizationName, @PathVariable("identitySerialNumber") String identitySerialNumber ) throws ParseException {
		return apduFeignClient.getUserIdentityBySerialNumber(organizationName, identitySerialNumber);
	}
	
	@GetMapping("/organizations/{organizationName}/userAge/{identitySerialNumber}")
	public ResponseEntity<List<UserIdentityData>> getUserAge(@PathVariable("organizationName") String organizationName, @PathVariable("identitySerialNumber") String identitySerialNumber ) throws ParseException {
		return apduFeignClient.getUserAgeBySerialNumber(organizationName, identitySerialNumber);
	}

	@PostMapping("/organizations/{organizationName}/device/lastSeen")
	public ResponseEntity<String> updateLastSeen(@PathVariable("organizationName") String organizationName, @RequestBody LastSeen lastSeen ) throws ParseException {
		return apduFeignClient.updateLastSeen(organizationName, lastSeen);
	}
	
	// Using verifier App
	@GetMapping("/organizations/{organizationName}/mobile-devices/{deviceID}/credentials/{appName}/users/{userID}/credentials/{credentialTemplateID}")
	public MobileCredential getOrIssueCredentialReqByRelyingParty(@PathVariable("organizationName") String organizationName, @PathVariable("deviceID") String deviceID,
			@PathVariable("appName") String appName,@PathVariable("userID") Long userID,
			@PathVariable("credentialTemplateID") Long credentialTemplateID) {
		return apduFeignClient.getOrIssueCredentialReqByRelyingParty(organizationName, deviceID, appName, userID, credentialTemplateID);
	}
	
	
	
	
	
	
	//Usermanagement Service
	
	@GetMapping("/organizations/{organizationName}/vehicle-registrations/{licensePlateNumber}")
	public VehicleRegistration getVehicleRegistrationDetails(@PathVariable("organizationName") String organizationName, @PathVariable("licensePlateNumber") String licensePlateNumber) {
		return userManagementFeignClient.getVehicleRegistrationDetails(organizationName, licensePlateNumber);
	}
    
	@GetMapping("/organizations/{organizationName}/brandingInfo")
	public CustomerBranding getCustomerBranding(@PathVariable("organizationName") String organizationName) {
		return userManagementFeignClient.getCustomerBranding(organizationName);			
	}	
	
	@GetMapping("/organizations/{organizationName}/roles/{roleName}/permissions")
	public List<Permission>  getRoleBasedPermissions(@PathVariable("organizationName") String organizationName, @PathVariable("roleName") String roleName) {
		return userManagementFeignClient.getRoleBasedPermissions(organizationName, roleName);			
	}
	
	@GetMapping("/organizations/{organizationName}/groups/groupsWithWorkflow")
	public List<Group> getGroupsWithWorkflow(@PathVariable("organizationName") String organizationName) {
		return userManagementFeignClient.getGroupsWithWorkflow(organizationName);
	}
	
	@GetMapping("/organizations/{organizationName}/roles/fromAuthService")
	public List<Role> getRolesFromAuthenticationService(@PathVariable("organizationName") String organizationName) {
		return userManagementFeignClient.getRolesFromAuthenticationService(organizationName);
	}
	
	@PostMapping("/organizations/{organizationName}/idp-users")
	public ResponseEntity<User> createIdentityProviderUser(@PathVariable("organizationName") String organizationName, @RequestBody IdentityProviderUser identityProviderUser) throws Exception {
		return userManagementFeignClient.createIdentityProviderUser(organizationName, identityProviderUser);
	}
	
	@GetMapping("/organizations/{organizationName}/users/groups/{groupID}/pendingEnrollmentStats")
	public ResultPage pendingEnrollmentStat(@PathVariable("organizationName") String organizationName, @PathVariable("groupID") String groupID,
			@RequestParam("sort") String sort, @RequestParam("order") String order, @RequestParam("page") int page,
			@RequestParam("size") int size, @RequestParam(name = "startDate", required = false) String startDate,
			@RequestParam(name = "endDate", required = false) String endDate) throws ParseException {
	
		return userManagementFeignClient.pendingEnrollmentStat(organizationName, groupID, sort, order, page, size, startDate, endDate);
	}
	
	@GetMapping("/organizations/{organizationName}/users/{userName}/enrollment-details")
	public ResponseEntity<UserDetails> getUserEnrollmentDetails(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName) {
		return userManagementFeignClient.getUserEnrollmentDetails(organizationName, userName);
	}
	
	
	
	
	
	//BiometricVerification	Service
	
	@PostMapping("/organizations/{organizationName}/biometricIdentity/verify")
	public ResponseEntity<BiometricVerificationResponse> verifyUserByID(
			@PathVariable("organizationName") String organizationName, @RequestBody UserBioInfo userBioInfo) {
		return biometricVerificationFeignClient.verifyUserByID(organizationName,userBioInfo);
	}
	
}
