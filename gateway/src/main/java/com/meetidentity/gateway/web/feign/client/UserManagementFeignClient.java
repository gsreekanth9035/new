package com.meetidentity.gateway.web.feign.client;

import java.util.List;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.meetidentity.gateway.web.feign.client.model.PairMobileDeviceConfig;
import com.meetidentity.gateway.web.feign.client.model.Permission;
import com.meetidentity.gateway.web.feign.client.model.User;
import com.meetidentity.gateway.web.feign.client.model.WebAuthnAuthenticatorRegistration;
import com.meetidentity.gateway.web.feign.client.model.WebAuthnPolicyDetails;
import com.meetidentity.gateway.web.feign.client.model.Workflow;
import com.meetidentity.gateway.web.feign.client.model.WorkflowStep;
import com.meetidentity.gateway.web.rest.model.CustomerBranding;
import com.meetidentity.gateway.web.rest.model.Group;
import com.meetidentity.gateway.web.rest.model.IdentityProviderUser;
import com.meetidentity.gateway.web.rest.model.ResultPage;
import com.meetidentity.gateway.web.rest.model.Role;
import com.meetidentity.gateway.web.rest.model.UserDetails;
import com.meetidentity.gateway.web.rest.model.VehicleRegistration;

@FeignClient(name="usermanagement", url="http://${application.usermanagement.host}:${application.usermanagement.port}")
public interface UserManagementFeignClient {
	
	@GetMapping("/api/v1/organizations/{organizationName}/roles/{roleName}/permissions")
	List<Permission> getPermissionsByRole(@PathVariable("organizationName") String organizationName, @PathVariable("roleName") String roleName);
	
	@GetMapping("/api/v1/organizations/{organizationName}/roles/{roleName}/permissionMode")
	Map<String, Long> getRolePermissionModeByRole(@PathVariable("organizationName") String organizationName, @PathVariable("roleName") String roleName);
	
	@GetMapping("/api/v1/organizations/{organizationName}/roles/{roleName}/workflow")
	Workflow getWorkflowByRole(@PathVariable("organizationName") String organizationName, @PathVariable("roleName") String roleName);
	
	@GetMapping("/api/v1/organizations/{organizationName}/workflows/{workflowName}/steps")
	List<WorkflowStep> getStepsByWorkflow(@PathVariable("organizationName") String organizationName, @PathVariable("workflowName") String workflowName);

	@PostMapping("/api/v1/organizations/{organizationName}/logout/{userName}")
	void userLogout(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName);
	
	@GetMapping("/api/v1/organizations/{organizationName}/groups/users/{userKCLCId}")
	Group[] getGroupsOfUserKCLCId(@PathVariable("organizationName") String organizationName, @PathVariable("userKCLCId") String userKCLCId);
	
	@GetMapping("/api/v1/organizations/{organizationName}/brandingInfo")
	CustomerBranding getCustomerBranding(@PathVariable("organizationName") String organizationName);

	@GetMapping("/api/v1/organizations/{organizationName}/vehicle-registrations/{licensePlateNumber}")
	VehicleRegistration getVehicleRegistrationDetails(@PathVariable("organizationName") String organizationName, @PathVariable("licensePlateNumber") String licensePlateNumber);
	
	@GetMapping("/api/v1/organizations/{organizationName}/roles/names")
	List<String> getRoleNames(@PathVariable("organizationName") String organizationName);

	@GetMapping("/api/v1/organizations/{organizationName}/roles/{roleName}/permissions")
	List<Permission> getRoleBasedPermissions(@PathVariable("organizationName") String organizationName,@PathVariable("roleName") String roleName);

	@GetMapping("/api/v1/organizations/{organizationName}/groups/groupsWithWorkflow")
	List<Group> getGroupsWithWorkflow(@PathVariable("organizationName") String organizationName);

	@GetMapping("/api/v1/organizations/{organizationName}/roles/fromAuthService")
	List<Role> getRolesFromAuthenticationService(@PathVariable("organizationName") String organizationName);

	@PostMapping("/api/v1/organizations/{organizationName}/idp-users")
	ResponseEntity<User> createIdentityProviderUser(@PathVariable("organizationName") String organizationName, @RequestBody IdentityProviderUser  identityProviderUser);

	@GetMapping("/api/v1/organizations/{organizationName}/users/groups/{groupID}/pendingEnrollmentStats")
	ResultPage pendingEnrollmentStat(@PathVariable("organizationName") String organizationName, @PathVariable("groupID") String groupID,
			@RequestParam("sort") String sort, @RequestParam("order") String order, @RequestParam("page") int page,
			@RequestParam("size") int size, @RequestParam(name = "startDate", required = false) String startDate,
			@RequestParam(name = "endDate", required = false) String endDate);
	
	@PostMapping("/api/v1/organizations/{organizationName}/users/register")
	public ResponseEntity<User> registerLoggedInUser(@PathVariable("organizationName") String organizationName,
			@RequestBody com.meetidentity.gateway.web.feign.client.model.User user) throws Exception;

	@GetMapping("/api/v1/organizations/{organizationName}/users/{userName}/enrollment-details")
	public ResponseEntity<UserDetails> getUserEnrollmentDetails(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName);

	@GetMapping("/api/v1/organizations/{organizationName}/users/{userName}/user-status")
	public ResponseEntity<UserDetails> getUserStatus(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName);

	@GetMapping("/api/v1/organizations/{organizationName}/pair-mobile-service")
	public PairMobileDeviceConfig pairMobileService(@PathVariable("organizationName") String organizationName);
	

	@GetMapping("/api/v1/organizations/{organizationName}/idp-users/{userName}/webauthn-policy-details")
	public ResponseEntity<WebAuthnPolicyDetails> webauthnPolicyDetails(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName);
	
	@PostMapping("/api/v1/organizations/{organizationName}/idp-users/{userName}/webauthn-register-authenticator")
	public ResponseEntity<WebAuthnAuthenticatorRegistration> registerWebAuthnAuthenticator(@PathVariable("organizationName") String organizationName, @PathVariable("userName") String userName,  @RequestBody WebAuthnAuthenticatorRegistration webAuthnAuthenticatorRegistration);
	
	@GetMapping("/api/v1/organizations/{organizationName}/roles/{roleNames}/color")
	public List<Role> getRoleWithColor(@PathVariable("roleNames") List<String> listOfRoles,@PathVariable("organizationName")  String organization);

}
