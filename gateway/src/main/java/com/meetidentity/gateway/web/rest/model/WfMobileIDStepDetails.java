package com.meetidentity.gateway.web.rest.model;

import java.util.ArrayList;
import java.util.List;

public class WfMobileIDStepDetails {
	private WFMobileIDStepOnboardConfig wfMobileIDStepOnboardConfig;
	private boolean hardwareBackedAuthentication;
	private List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdentityConfigs = new ArrayList<>();

	public List<WFMobileIDStepIdentityConfig> getWfMobileIDStepIdentityConfigs() {
		return wfMobileIDStepIdentityConfigs;
	}

	public void setWfMobileIDStepIdentityConfigs(List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdentityConfigs) {
		this.wfMobileIDStepIdentityConfigs = wfMobileIDStepIdentityConfigs;
	}

	public boolean isHardwareBackedAuthentication() {
		return hardwareBackedAuthentication;
	}

	public void setHardwareBackedAuthentication(boolean hardwareBackedAuthentication) {
		this.hardwareBackedAuthentication = hardwareBackedAuthentication;
	}

	public WFMobileIDStepOnboardConfig getWfMobileIDStepOnboardConfig() {
		return wfMobileIDStepOnboardConfig;
	}

	public void setWfMobileIDStepOnboardConfig(WFMobileIDStepOnboardConfig wfMobileIDStepOnboardConfig) {
		this.wfMobileIDStepOnboardConfig = wfMobileIDStepOnboardConfig;
	}

}
