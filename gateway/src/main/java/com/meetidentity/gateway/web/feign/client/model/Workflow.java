package com.meetidentity.gateway.web.feign.client.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.meetidentity.gateway.web.rest.model.OrganizationIdentity;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Workflow {

	private String name;
	private String description;
	private OrganizationIdentity organizationIdentities;
	private List<DeviceProfile> deviceProfiles;
	private List<WorkflowStep> workflowSteps = new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DeviceProfile> getDeviceProfiles() {
		return deviceProfiles;
	}

	public void setDeviceProfiles(List<DeviceProfile> deviceProfiles) {
		this.deviceProfiles = deviceProfiles;
	}

	public OrganizationIdentity getOrganizationIdentities() {
		return organizationIdentities;
	}

	public void setOrganizationIdentities(OrganizationIdentity organizationIdentities) {
		this.organizationIdentities = organizationIdentities;
	}

	public List<WorkflowStep> getWorkflowSteps() {
		return workflowSteps;
	}

	public void setWorkflowSteps(List<WorkflowStep> workflowSteps) {
		this.workflowSteps = workflowSteps;
	}

}

class DeviceProfile {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
