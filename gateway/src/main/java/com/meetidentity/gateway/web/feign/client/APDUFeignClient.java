package com.meetidentity.gateway.web.feign.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import com.meetidentity.gateway.web.feign.client.model.User;
import com.meetidentity.gateway.web.rest.model.LastSeen;
import com.meetidentity.gateway.web.rest.model.MobileCredential;
import com.meetidentity.gateway.web.rest.model.MobileCredentialTemplate;
import com.meetidentity.gateway.web.rest.model.PrintedCredential;
import com.meetidentity.gateway.web.rest.model.TokenDetails;
import com.meetidentity.gateway.web.rest.model.UserDetails;
import com.meetidentity.gateway.web.rest.model.UserDevice;
import com.meetidentity.gateway.web.rest.model.UserDeviceInfo;
import com.meetidentity.gateway.web.rest.model.UserIdentityData;

@FeignClient(name="apdu", url="http://${application.apdu.host}:${application.apdu.port}")
public interface APDUFeignClient {
	
	
	@PostMapping("/api/v1/devices")
	ResponseEntity<User> registerMobileDevice(@RequestBody UserDevice requestUserDevice);

	@GetMapping("/api/v1/organizations/{organizationName}/mobile-devices/{deviceID}/users/{userID}/credentials/initial")
	ResponseEntity<List<MobileCredential>> getCredentialsByDeviceAndUserID_Initial(@PathVariable("organizationName") String organizationName,@PathVariable("deviceID") String deviceID, @PathVariable("userID") Long userID);
	
	@GetMapping("/api/v1/organizations/{organizationName}/mobile-devices/{deviceID}/users/{userID}/credentials")
	ResponseEntity<List<MobileCredential>> getCredentialsByDeviceAndUserID(@PathVariable("organizationName") String organizationName,@PathVariable("deviceID") String deviceID, @PathVariable("userID") Long userID);
		
	@DeleteMapping("/api/v1/organizations/{organizationName}/mobile-devices/{deviceID}/credentials/{credentialTemplateID}")
	ResponseEntity<Void> deleteCredentialByTemplateID(@PathVariable("organizationName") String organizationName,@PathVariable("deviceID") String deviceID,
			@PathVariable("credentialTemplateID") Long credentialTemplateID);

	@PostMapping("/api/v1/devices/{appName}/users/{userID}/sendNotification")
	ResponseEntity<Void> sendNotificationByID(@PathVariable("appName") String appName,@PathVariable("userID") Long userID,@RequestBody UserDevice requestUserDevice);

	@PostMapping("/api/v1/organizations/{organizationName}/mobile-devices/{deviceID}/credentials/deleteSeletedCredentials")
	ResponseEntity<Void> deleteCredentialByTemplateIDs(@PathVariable("organizationName") String organizationName,@PathVariable("deviceID") String deviceID,
			@RequestBody MobileCredentialTemplate mobileCredentialTemplate);

	@PostMapping("/api/v1/organizations/{organizationName}/mobile-devices/{deviceID}/credentials/deleteSeletedCredentialsWithUnpair")
	ResponseEntity<Void> deleteCredentialByTemplateIDsWithUnpair(@PathVariable("organizationName") String organizationName,@PathVariable("deviceID") String deviceID,
			@RequestBody MobileCredentialTemplate mobileCredentialTemplate);

	@GetMapping("/api/v1/organizations/{organizationName}/mobile-devices/{deviceID}/credentials/sync")
	ResponseEntity<List<MobileCredential>> generateOrRetrieveCredentialsByDeviceID_sync(@PathVariable("organizationName") String organizationName,@PathVariable("deviceID") String deviceID);
	
	@GetMapping("/api/v1/organizations/{organizationName}/{requestFrom}/users/{userID}/credentials/{credentialTemplateID}")
	PrintedCredential getCredentialByTemplateID(@RequestHeader HttpHeaders httpHeaders, @PathVariable("organizationName") String organizationName, @PathVariable("requestFrom") String requestFrom, @PathVariable("userID") Long userID, @PathVariable("credentialTemplateID") Long credentialTemplateID);

	@GetMapping("/api/v1/organizations/{organizationName}/userIdentity/{identitySerialNumber}")
	ResponseEntity<List<UserIdentityData>> getUserIdentityBySerialNumber(@PathVariable("organizationName") String organizationName,
			@PathVariable("identitySerialNumber") String identitySerialNumber);

	@GetMapping("/api/v1/organizations/{organizationName}/userAge/{identitySerialNumber}")
	ResponseEntity<List<UserIdentityData>> getUserAgeBySerialNumber(@PathVariable("organizationName") String organizationName,
			@PathVariable("identitySerialNumber") String identitySerialNumber);

	@PostMapping("/api/v1/devices/mobileService")
	ResponseEntity<UserDetails> registerMobileServiceDevice(@RequestBody UserDevice requestUserDevice);
	
	@GetMapping("/api/v1/organizations/{organizationName}/orgIssuerCertificate")
	UserDeviceInfo getOrgIssuerCertificate(@PathVariable("organizationName") String organizationName);

	@PostMapping("/api/v1/organizations/{organizationName}/device/lastSeen")
	ResponseEntity<String> updateLastSeen(@PathVariable("organizationName") String organizationName, @RequestBody LastSeen lastSeen);

	@GetMapping("/api/v1/organizations/{organizationName}/mobile-devices/{deviceID}/credentials/{appName}/users/{userID}/credentials/{credentialTemplateID}")
	MobileCredential getOrIssueCredentialReqByRelyingParty(@PathVariable("organizationName") String organizationName, @PathVariable("deviceID") String deviceID,
			@PathVariable("appName") String appName,@PathVariable("userID") Long userID,
			@PathVariable("credentialTemplateID") Long credentialTemplateID);

	@GetMapping("/api/v1/mobile-device/(deviceID)/token")
	TokenDetails generateMobileIDUserTokens(@PathVariable("deviceID") String deviceID, @RequestParam("x-tentant") String organizationName);

	@GetMapping("/api/v1/{organizationName}/deviceService/securityCheck")
	public Boolean getDeviceServiceSecurityCheck(@PathVariable("organizationName") String organizationName);
	
}
