package com.meetidentity.gateway.web.rest.model;

public class UserIdentityData {
	private String labelName;
	private String value;

	public String getLabelName() {
		return labelName;
	}

	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
