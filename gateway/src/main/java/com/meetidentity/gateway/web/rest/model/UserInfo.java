package com.meetidentity.gateway.web.rest.model;

import java.time.Instant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.meetidentity.gateway.domain.User;
import com.meetidentity.gateway.web.feign.client.model.UserBiometric;

public class UserInfo {

	private String sub;

	private String username;

	private String firstName;

	private String lastName;

	private String email;

	private Instant exp;
	
	private String organization;

	private Long orgId;

	private String workflow;

	private Set<String> authorities;

	private Map<String, Set<String>> roleBasedAuthorities;

	private List<String> workflowSteps;

	private List<String> roles;

	private Group[] groups;

	private List<Long> authoritiesServicemode;

	private String id;

	private String login;

	private String imageUrl;

	private boolean activated = false;

	private String langKey;

	private String createdBy;

	private Instant createdDate;

	private String lastModifiedBy;

	private Instant lastModifiedDate;

	private Long userId;

	private String status;

	private String userStatus;

	private boolean enrolled;

	private Boolean canEnroll;

	private Boolean canIssue;

	private Boolean canLifeCycle;

	private Boolean canDisplayEnrollDetails;

	private Boolean canApprove;

	private Boolean isFederated;
	
	private Boolean isEnrollmentInProgress;
	
	private List<UserBiometric> userBiometrics = new ArrayList<>();
	
	private Boolean isDeviceClientSecurityEnabled;
	
	private String identityType;
	
	private List<Role> roleWithColor;	

	public UserInfo() {

	}

	public UserInfo(User user) {
		this.id = user.getId();
		this.login = user.getLogin();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.email = user.getEmail();
		this.activated = user.getActivated();
		this.imageUrl = user.getImageUrl();
		this.langKey = user.getLangKey();
		this.createdBy = user.getCreatedBy();
		this.createdDate = user.getCreatedDate();
		this.lastModifiedBy = user.getLastModifiedBy();
		this.lastModifiedDate = user.getLastModifiedDate();
		
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getWorkflow() {
		return workflow;
	}

	public void setWorkflow(String workflow) {
		this.workflow = workflow;
	}

	public Set<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<String> authorities) {
		this.authorities = authorities;
	}

	public List<String> getWorkflowSteps() {
		return workflowSteps;
	}

	public void setWorkflowSteps(List<String> workflowSteps) {
		this.workflowSteps = workflowSteps;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public List<Long> getAuthoritiesServicemode() {
		return authoritiesServicemode;
	}

	public void setAuthoritiesServicemode(List<Long> permissionsServicemode) {
		this.authoritiesServicemode = permissionsServicemode;
	}

	public Group[] getGroups() {
		return groups;
	}

	public void setGroups(Group[] groups) {
		this.groups = groups;
	}

	public Map<String, Set<String>> getRoleBasedAuthorities() {
		return roleBasedAuthorities;
	}

	public void setRoleBasedAuthorities(Map<String, Set<String>> roleBasedAuthorities) {
		this.roleBasedAuthorities = roleBasedAuthorities;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public String getLangKey() {
		return langKey;
	}

	public void setLangKey(String langKey) {
		this.langKey = langKey;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Instant getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Instant lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public boolean isEnrolled() {
		return enrolled;
	}

	public void setEnrolled(boolean enrolled) {
		this.enrolled = enrolled;
	}

	public Boolean getCanEnroll() {
		return canEnroll;
	}

	public void setCanEnroll(Boolean canEnroll) {
		this.canEnroll = canEnroll;
	}

	public Boolean getCanIssue() {
		return canIssue;
	}

	public void setCanIssue(Boolean canIssue) {
		this.canIssue = canIssue;
	}

	public Boolean getCanLifeCycle() {
		return canLifeCycle;
	}

	public void setCanLifeCycle(Boolean canLifeCycle) {
		this.canLifeCycle = canLifeCycle;
	}

	public Boolean getCanDisplayEnrollDetails() {
		return canDisplayEnrollDetails;
	}

	public void setCanDisplayEnrollDetails(Boolean canDisplayEnrollDetails) {
		this.canDisplayEnrollDetails = canDisplayEnrollDetails;
	}

	public Boolean getCanApprove() {
		return canApprove;
	}

	public void setCanApprove(Boolean canApprove) {
		this.canApprove = canApprove;
	}

	public Boolean getIsFederated() {
		return isFederated;
	}

	public void setIsFederated(Boolean isFederated) {
		this.isFederated = isFederated;
	}

	public List<UserBiometric> getUserBiometrics() {
		return userBiometrics;
	}

	public void setUserBiometrics(List<UserBiometric> userBiometrics) {
		this.userBiometrics = userBiometrics;
	}

	public Boolean getIsDeviceClientSecurityEnabled() {
		return isDeviceClientSecurityEnabled;
	}

	public void setIsDeviceClientSecurityEnabled(Boolean isDeviceClientSecurityEnabled) {
		this.isDeviceClientSecurityEnabled = isDeviceClientSecurityEnabled;
	}

	public Instant getExp() {
		return exp;
	}

	public void setExp(Instant exp) {
		this.exp = exp;
	}

	public Boolean getIsEnrollmentInProgress() {
		return isEnrollmentInProgress;
	}

	public void setIsEnrollmentInProgress(Boolean isEnrollmentInProgress) {
		this.isEnrollmentInProgress = isEnrollmentInProgress;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}
	
	public List<Role> getRoleWithColor() {
		return roleWithColor;
	}

	public void setRoleWithColor(List<Role> roleWithColor) {
		this.roleWithColor = roleWithColor;
	}

}
