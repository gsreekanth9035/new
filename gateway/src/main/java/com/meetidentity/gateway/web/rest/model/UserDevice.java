package com.meetidentity.gateway.web.rest.model;

import java.io.Serializable;
import java.util.Date;

public class UserDevice implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String friendlyName;
	private String type;
	private String uniqueIdentifier;
	private String operatingSystem;
	private String osVersion;
	private String uuid;
	private String userData;
	private Long userId;
	private String mobileNumber;
	private String appName;
	private String appRegID;
	
	private String osApiVersion;
	private String appVersion;
	private String deviceModelName;
	private String deviceManufacturerName;
	
	private Date issuedDate;
	private Date activationDate;
	private Date expirationDate;
	private String status;
	private Boolean chipEncodedFlag;
	private Boolean printedFlag;
	private Boolean recycledFlag;
	
	private String challenge;
	
	private String gpsCoordinates;

	private String organizationName;
	
	private String publicKeyAlgorithm;
	private String publicKeyB64;
	private String devicePublicKeyB64;
	private String devicePublicKeyAlgorithm;
	
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getFriendlyName() {
		return friendlyName;
	}
	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUniqueIdentifier() {
		return uniqueIdentifier;
	}
	public void setUniqueIdentifier(String uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}
	public String getOperatingSystem() {
		return operatingSystem;
	}
	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getUserData() {
		return userData;
	}
	public void setUserData(String userData) {
		this.userData = userData;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}	
	public Date getIssuedDate() {
		return issuedDate;
	}
	public void setIssuedDate(Date issuedDate) {
		this.issuedDate = issuedDate;
	}
	public Date getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getChipEncodedFlag() {
		return chipEncodedFlag;
	}
	public void setChipEncodedFlag(Boolean chipEncodedFlag) {
		this.chipEncodedFlag = chipEncodedFlag;
	}
	public Boolean getPrintedFlag() {
		return printedFlag;
	}
	public void setPrintedFlag(Boolean printedFlag) {
		this.printedFlag = printedFlag;
	}
	public Boolean getRecycledFlag() {
		return recycledFlag;
	}
	public void setRecycledFlag(Boolean recycledFlag) {
		this.recycledFlag = recycledFlag;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getAppRegID() {
		return appRegID;
	}
	public void setAppRegID(String appRegID) {
		this.appRegID = appRegID;
	}
	public String getChallenge() {
		return challenge;
	}
	public void setChallenge(String challenge) {
		this.challenge = challenge;
	}
	public String getOsApiVersion() {
		return osApiVersion;
	}
	public void setOsApiVersion(String osApiVersion) {
		this.osApiVersion = osApiVersion;
	}
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public String getDeviceModelName() {
		return deviceModelName;
	}
	public void setDeviceModelName(String deviceModelName) {
		this.deviceModelName = deviceModelName;
	}
	public String getDeviceManufacturerName() {
		return deviceManufacturerName;
	}
	public void setDeviceManufacturerName(String deviceManufacturerName) {
		this.deviceManufacturerName = deviceManufacturerName;
	}
	public String getGpsCoordinates() {
		return gpsCoordinates;
	}
	public void setGpsCoordinates(String gpsCoordinates) {
		this.gpsCoordinates = gpsCoordinates;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getPublicKeyB64() {
		return publicKeyB64;
	}
	public void setPublicKeyB64(String publicKeyB64) {
		this.publicKeyB64 = publicKeyB64;
	}
	public String getPublicKeyAlgorithm() {
		return publicKeyAlgorithm;
	}
	public void setPublicKeyAlgorithm(String publicKeyAlgorithm) {
		this.publicKeyAlgorithm = publicKeyAlgorithm;
	}
	public String getDevicePublicKeyB64() {
		return devicePublicKeyB64;
	}
	public void setDevicePublicKeyB64(String devicePublicKeyB64) {
		this.devicePublicKeyB64 = devicePublicKeyB64;
	}
	public String getDevicePublicKeyAlgorithm() {
		return devicePublicKeyAlgorithm;
	}
	public void setDevicePublicKeyAlgorithm(String devicePublicKeyAlgorithm) {
		this.devicePublicKeyAlgorithm = devicePublicKeyAlgorithm;
	}
	
	
	
	
	
}
