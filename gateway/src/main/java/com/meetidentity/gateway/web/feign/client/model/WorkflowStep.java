package com.meetidentity.gateway.web.feign.client.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.meetidentity.gateway.web.rest.model.Group;
import com.meetidentity.gateway.web.rest.model.OrganizationIdentity;
import com.meetidentity.gateway.web.rest.model.WFMobileIDStepIdentityConfig;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class WorkflowStep {
	
	private String name;
	private String description;
	private String displayName;
	private Integer order;
	
	private List<IDProofType> idProofTypes = new ArrayList<>();
	private List<WFStepGenericConfig> wfStepGenericConfigs = new ArrayList<>();
	private List<WFStepRegistrationConfig> wfStepRegistrationConfigs = new ArrayList<>();
	private List<WFStepApprovalGroupConfig> wfStepApprovalGroupConfigs = new ArrayList<>();
	/*private List<WFStepVisualTemplateConfig> wfStepVisualTemplateConfigs = new ArrayList<>();
	private List<WFStepCertConfig> wfStepCertConfigs = new ArrayList<>();
	private List<WfStepGroupConfig> wfStepChipEncodeVIDPrintGroupConfigs = new ArrayList<>();
	private List<WfStepGroupConfig> wfStepChipEncodeGroupConfigs = new ArrayList<>();
	private List<WfStepGroupConfig> wfStepPrintGroupConfigs = new ArrayList<>();*/
	private List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdentityConfigs = new ArrayList<>();
	private List<Group> wfStepAdjudicationGroupConfigs = new ArrayList<>();
	private List<OrganizationIdentity> wfMobileIDStepTrustedIdentities = new ArrayList<>();
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public List<WFStepGenericConfig> getWfStepGenericConfigs() {
		return wfStepGenericConfigs;
	}
	public void setWfStepGenericConfigs(List<WFStepGenericConfig> wfStepGenericConfigs) {
		this.wfStepGenericConfigs = wfStepGenericConfigs;
	}
	public List<WFStepRegistrationConfig> getWfStepRegistrationConfigs() {
		return wfStepRegistrationConfigs;
	}
	public void setWfStepRegistrationConfigs(List<WFStepRegistrationConfig> wfStepRegistrationConfigs) {
		this.wfStepRegistrationConfigs = wfStepRegistrationConfigs;
	}
	public List<WFStepApprovalGroupConfig> getWfStepApprovalGroupConfigs() {
		return wfStepApprovalGroupConfigs;
	}
	public void setWfStepApprovalGroupConfigs(List<WFStepApprovalGroupConfig> wfStepApprovalGroupConfigs) {
		this.wfStepApprovalGroupConfigs = wfStepApprovalGroupConfigs;
	}
	public List<WFMobileIDStepIdentityConfig> getWfMobileIDStepIdentityConfigs() {
		return wfMobileIDStepIdentityConfigs;
	}
	public void setWfMobileIDStepIdentityConfigs(List<WFMobileIDStepIdentityConfig> wfMobileIDStepIdentityConfigs) {
		this.wfMobileIDStepIdentityConfigs = wfMobileIDStepIdentityConfigs;
	}
	public List<Group> getWfStepAdjudicationGroupConfigs() {
		return wfStepAdjudicationGroupConfigs;
	}
	public void setWfStepAdjudicationGroupConfigs(List<Group> wfStepAdjudicationGroupConfigs) {
		this.wfStepAdjudicationGroupConfigs = wfStepAdjudicationGroupConfigs;
	}
	public List<OrganizationIdentity> getWfMobileIDStepTrustedIdentities() {
		return wfMobileIDStepTrustedIdentities;
	}
	public void setWfMobileIDStepTrustedIdentities(List<OrganizationIdentity> wfMobileIDStepTrustedIdentities) {
		this.wfMobileIDStepTrustedIdentities = wfMobileIDStepTrustedIdentities;
	}
	public List<IDProofType> getIdProofTypes() {
		return idProofTypes;
	}
	public void setIdProofTypes(List<IDProofType> idProofTypes) {
		this.idProofTypes = idProofTypes;
	}
	
}