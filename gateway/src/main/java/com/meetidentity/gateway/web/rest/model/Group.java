package com.meetidentity.gateway.web.rest.model;

public class Group {
	
	private String id;
	private String name;
	private Long visualTemplateID;
	
	public Group() {
		
	}
	
	public Group(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getVisualTemplateID() {
		return visualTemplateID;
	}

	public void setVisualTemplateID(Long visualTemplateID) {
		this.visualTemplateID = visualTemplateID;
	}

}
