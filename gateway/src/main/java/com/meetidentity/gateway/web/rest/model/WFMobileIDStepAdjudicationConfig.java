package com.meetidentity.gateway.web.rest.model;

import java.util.List;

public class WFMobileIDStepAdjudicationConfig {
	private List<Group> adjudicationGroups;

	public List<Group> getAdjudicationGroups() {
		return adjudicationGroups;
	}

	public void setAdjudicationGroups(List<Group> adjudicationGroups) {
		this.adjudicationGroups = adjudicationGroups;
	}

}