package com.meetidentity.gateway.web.feign.client.model;

public class UserCredential {
	
	private String type;
	private String value;
	
	public UserCredential() {
		
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
