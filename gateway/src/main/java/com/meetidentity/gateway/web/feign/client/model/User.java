package com.meetidentity.gateway.web.feign.client.model;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import com.meetidentity.gateway.web.rest.model.WfMobileIDStepDetails;

public class User {

	private Long id;
	private String name;
	private String organisationName;

	private Long organisationId;
	private String fullName;
	private String status;
	// TODO Find a way to retrieve user workflow based on user name from keycloak
	private String workflowName;
	private Workflow workflow;
	
	private String firstName;
	private String middleName;
	private String lastName;
	private String mothersName;
	private String email;

	private String userPrincipalName;
	private String country;

	private boolean enabled;

	private String groupId;
	private String groupName;
	private String refApprovalGroupId;
	
	private String enrollmentStatus;
	private String userStatus;
	private boolean canIssueIdentity;
	private Boolean isFederated;
	
	private String createdBy;

	private Instant createdDate = Instant.now();

	private String lastModifiedBy;

	private Instant lastModifiedDate = Instant.now();
	
	private UserCredential userCredential;

	private List<UserAttribute> userAttributes = new ArrayList<>();

	private List<UserBiometric> userBiometrics = new ArrayList<>();

	private WfMobileIDStepDetails wfMobileIDStepDetails = new WfMobileIDStepDetails();

	public User() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public UserCredential getUserCredential() {
		return userCredential;
	}

	public void setUserCredential(UserCredential userCredential) {
		this.userCredential = userCredential;
	}

	public List<UserAttribute> getUserAttributes() {
		return userAttributes;
	}

	public void setUserAttributes(List<UserAttribute> userAttributes) {
		this.userAttributes = userAttributes;
	}

	public List<UserBiometric> getUserBiometrics() {
		return userBiometrics;
	}

	public void setUserBiometrics(List<UserBiometric> userBiometrics) {
		this.userBiometrics = userBiometrics;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrganisationName() {
		return organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMothersName() {
		return mothersName;
	}

	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}

	public WfMobileIDStepDetails getWfMobileIDStepDetails() {
		return wfMobileIDStepDetails;
	}

	public void setWfMobileIDStepDetails(WfMobileIDStepDetails wfMobileIDStepDetails) {
		this.wfMobileIDStepDetails = wfMobileIDStepDetails;
	}

	public String getUserPrincipalName() {
		return userPrincipalName;
	}

	public void setUserPrincipalName(String userPrincipalName) {
		this.userPrincipalName = userPrincipalName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Workflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	public String getRefApprovalGroupId() {
		return refApprovalGroupId;
	}

	public void setRefApprovalGroupId(String refApprovalGroupId) {
		this.refApprovalGroupId = refApprovalGroupId;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public boolean isCanIssueIdentity() {
		return canIssueIdentity;
	}

	public void setCanIssueIdentity(boolean canIssueIdentity) {
		this.canIssueIdentity = canIssueIdentity;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Instant getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Instant lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Boolean getIsFederated() {
		return isFederated;
	}

	public void setIsFederated(Boolean isFederated) {
		this.isFederated = isFederated;
	}

}
