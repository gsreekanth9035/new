package com.meetidentity.gateway.web.feign.client.model;

public class UserBiometricAttribute {
	
	private String name;
	private String value;
	
	public UserBiometricAttribute() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}


}
