package com.meetidentity.gateway.web.feign.client.model;

import java.util.ArrayList;
import java.util.List;

public class UserBiometric {
	
	private String type;
	private String position;
	private String format;
	private byte[] data;
	private byte[] wsqData;
	private String ansi;
	private int imageQuality;
	
	private List<UserBiometricAttribute> userBiometricAttributes = new ArrayList<>();	
	
	public UserBiometric() {
		
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	
	public String getAnsi() {
		return ansi;
	}

	public void setAnsi(String ansi) {
		this.ansi = ansi;
	}

	public int getImageQuality() {
		return imageQuality;
	}

	public void setImageQuality(int imageQuality) {
		this.imageQuality = imageQuality;
	}

	public List<UserBiometricAttribute> getUserBiometricAttributes() {
		return userBiometricAttributes;
	}

	public void setUserBiometricAttributes(List<UserBiometricAttribute> userBiometricAttributes) {
		this.userBiometricAttributes = userBiometricAttributes;
	}

	public byte[] getWsqData() {
		return wsqData;
	}

	public void setWsqData(byte[] wsqData) {
		this.wsqData = wsqData;
	}

}
