package com.meetidentity.gateway.web.rest.model;

import java.io.Serializable;
import java.util.Date;

public class LastSeen  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String uniqueIdentifier;
	private String date;
	private String location;
	private String appName;
	
	public String getUniqueIdentifier() {
		return uniqueIdentifier;
	}
	public void setUniqueIdentifier(String uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	

}
