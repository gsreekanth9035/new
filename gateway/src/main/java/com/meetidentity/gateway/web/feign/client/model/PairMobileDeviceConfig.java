package com.meetidentity.gateway.web.feign.client.model;

public class PairMobileDeviceConfig {

	private Long id;

	private String googlePlayStoreAppUrl;

	private String appleAppStoreAppUrl;

	private int qrCodeExpiryInMins;

	private String qrCode;

	private int qrCodeWidth;

	private int qrCodeHeight;

	private String secretKey;

	private int pairMobileDeviceSecretKeyLength;

	private String pairMobileDeviceUrl;

	private boolean enableDemoVideo;

	private boolean disableQRCodeExpiry = false;

	private boolean status;

	public PairMobileDeviceConfig(String googlePlayStoreAppUrl, String appleAppStoreAppUrl, int qrCodeExpiryInMins,
			int qrCodeWidth, int qrCodeHeight, String qrCode, int pairMobileDeviceSecretKeyLength, String secretKey,
			String pairMobileDeviceUrl, boolean enableDemoVideo, boolean disableQRCodeExpiry, boolean status) {
		super();
		this.googlePlayStoreAppUrl = googlePlayStoreAppUrl;
		this.appleAppStoreAppUrl = appleAppStoreAppUrl;
		this.qrCodeExpiryInMins = qrCodeExpiryInMins;
		this.qrCodeWidth = qrCodeWidth;
		this.qrCodeHeight = qrCodeHeight;
		this.qrCode = qrCode;
		this.pairMobileDeviceSecretKeyLength = pairMobileDeviceSecretKeyLength;
		this.secretKey = secretKey;
		this.pairMobileDeviceUrl = pairMobileDeviceUrl;
		this.enableDemoVideo = enableDemoVideo;
		this.disableQRCodeExpiry = disableQRCodeExpiry;
		this.status = status;
	}

	public PairMobileDeviceConfig() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGooglePlayStoreAppUrl() {
		return googlePlayStoreAppUrl;
	}

	public void setGooglePlayStoreAppUrl(String googlePlayStoreAppUrl) {
		this.googlePlayStoreAppUrl = googlePlayStoreAppUrl;
	}

	public String getAppleAppStoreAppUrl() {
		return appleAppStoreAppUrl;
	}

	public void setAppleAppStoreAppUrl(String appleAppStoreAppUrl) {
		this.appleAppStoreAppUrl = appleAppStoreAppUrl;
	}

	public int getQrCodeExpiryInMins() {
		return qrCodeExpiryInMins;
	}

	public void setQrCodeExpiryInMins(int qrCodeExpiryInMins) {
		this.qrCodeExpiryInMins = qrCodeExpiryInMins;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public int getQrCodeWidth() {
		return qrCodeWidth;
	}

	public void setQrCodeWidth(int qrCodeWidth) {
		this.qrCodeWidth = qrCodeWidth;
	}

	public int getQrCodeHeight() {
		return qrCodeHeight;
	}

	public void setQrCodeHeight(int qrCodeHeight) {
		this.qrCodeHeight = qrCodeHeight;
	}

	public int getPairMobileDeviceSecretKeyLength() {
		return pairMobileDeviceSecretKeyLength;
	}

	public void setPairMobileDeviceSecretKeyLength(int pairMobileDeviceSecretKeyLength) {
		this.pairMobileDeviceSecretKeyLength = pairMobileDeviceSecretKeyLength;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getPairMobileDeviceUrl() {
		return pairMobileDeviceUrl;
	}

	public void setPairMobileDeviceUrl(String pairMobileDeviceUrl) {
		this.pairMobileDeviceUrl = pairMobileDeviceUrl;
	}

	public boolean isEnableDemoVideo() {
		return enableDemoVideo;
	}

	public void setEnableDemoVideo(boolean enableDemoVideo) {
		this.enableDemoVideo = enableDemoVideo;
	}

	public boolean isDisableQRCodeExpiry() {
		return disableQRCodeExpiry;
	}

	public void setDisableQRCodeExpiry(boolean disableQRCodeExpiry) {
		this.disableQRCodeExpiry = disableQRCodeExpiry;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
