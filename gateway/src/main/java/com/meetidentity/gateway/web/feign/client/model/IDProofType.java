package com.meetidentity.gateway.web.feign.client.model;

public class IDProofType {

	private Long id;
	private String type;

	public IDProofType() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
