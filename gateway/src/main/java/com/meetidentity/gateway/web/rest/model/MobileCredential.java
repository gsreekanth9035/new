package com.meetidentity.gateway.web.rest.model;

import java.util.ArrayList;
import java.util.List;

import com.meetidentity.gateway.web.feign.client.model.VehicleRegistration;


public class MobileCredential {

	private Long userDeviceMobileCredentialID;
	private String status;
	private List<RoleDeviceAction> roleDeviceActions;
	private List<String> deviceConnectedActions;
	private List<String> deviceNotConnectedActions;
	private Long credentialTemplateID;
	private String credentialTemplateName;
	private String identityTypeName;

	private String base64FrontCredential;
	private String base64BackCredential;

	private String userData;
	private String visualData;

	private String visualSignedData;

	private String qrCodeData;

	private Long wfMobileIDStepIdentityConfigId;
	private Long userId;
	private String friendlyName;
	private String visualIdNumber;
	private Boolean softOTP;
	private OTPConfig otpConfig;
	private String OtpCode;
	private Boolean isCertificates;
	private int noOfCertificates;
	private Boolean pushVerify;
	private Boolean fido2;
	private String pushVerifyNumber;

	private List<VehicleRegistration> vehicleRegistrations;
	private List<WFMobileIdStepCertificate> wfMobileIdStepCertificates = new ArrayList<>();
	// ADD additional attributes if any
	private String frontOrientation;
	private String backOrientation;
	private List<UserIdentityData> userIdentityData = new ArrayList<>();

	public Long getCredentialTemplateID() {
		return credentialTemplateID;
	}

	public void setCredentialTemplateID(Long credentialTemplateID) {
		this.credentialTemplateID = credentialTemplateID;
	}

	public String getCredentialTemplateName() {
		return credentialTemplateName;
	}

	public void setCredentialTemplateName(String credentialTemplateName) {
		this.credentialTemplateName = credentialTemplateName;
	}

	public String getBase64FrontCredential() {
		return base64FrontCredential;
	}

	public void setBase64FrontCredential(String base64FrontCredential) {
		this.base64FrontCredential = base64FrontCredential;
	}

	public String getBase64BackCredential() {
		return base64BackCredential;
	}

	public void setBase64BackCredential(String base64BackCredential) {
		this.base64BackCredential = base64BackCredential;
	}

	public List<VehicleRegistration> getVehicleRegistrations() {
		return vehicleRegistrations;
	}

	public void setVehicleRegistrations(List<VehicleRegistration> vehicleRegistrations) {
		this.vehicleRegistrations = vehicleRegistrations;
	}

	public Long getUserDeviceMobileCredentialID() {
		return userDeviceMobileCredentialID;
	}

	public void setUserDeviceMobileCredentialID(Long userDeviceMobileCredentialID) {
		this.userDeviceMobileCredentialID = userDeviceMobileCredentialID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<RoleDeviceAction> getRoleDeviceActions() {
		return roleDeviceActions;
	}

	public void setRoleDeviceActions(List<RoleDeviceAction> roleDeviceActions) {
		this.roleDeviceActions = roleDeviceActions;
	}

	public String getUserData() {
		return userData;
	}

	public void setUserData(String userDaata) {
		this.userData = userDaata;
	}

	public String getVisualData() {
		return visualData;
	}

	public void setVisualData(String visualData) {
		this.visualData = visualData;
	}

	public String getVisualSignedData() {
		return visualSignedData;
	}

	public void setVisualSignedData(String visualSignedData) {
		this.visualSignedData = visualSignedData;
	}

	public String getQrCodeData() {
		return qrCodeData;
	}

	public void setQrCodeData(String qrCodeData) {
		this.qrCodeData = qrCodeData;
	}

	public String getFriendlyName() {
		return friendlyName;
	}

	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}

	public String getVisualIdNumber() {
		return visualIdNumber;
	}

	public void setVisualIdNumber(String visualIdNumber) {
		this.visualIdNumber = visualIdNumber;
	}

	public String getOtpCode() {
		return OtpCode;
	}

	public void setOtpCode(String otpCode) {
		OtpCode = otpCode;
	}

	public int getNoOfCertificates() {
		return noOfCertificates;
	}

	public void setNoOfCertificates(int noOfCertificates) {
		this.noOfCertificates = noOfCertificates;
	}

	public String getPushVerifyNumber() {
		return pushVerifyNumber;
	}

	public void setPushVerifyNumber(String pushVerifyNumber) {
		this.pushVerifyNumber = pushVerifyNumber;
	}

	public List<WFMobileIdStepCertificate> getWfMobileIdStepCertificates() {
		return wfMobileIdStepCertificates;
	}

	public void setWfMobileIdStepCertificates(List<WFMobileIdStepCertificate> wfMobileIdStepCertificates) {
		this.wfMobileIdStepCertificates = wfMobileIdStepCertificates;
	}

	public String getIdentityTypeName() {
		return identityTypeName;
	}

	public void setIdentityTypeName(String identityTypeName) {
		this.identityTypeName = identityTypeName;
	}

	public List<String> getDeviceConnectedActions() {
		return deviceConnectedActions;
	}

	public void setDeviceConnectedActions(List<String> deviceConnectedActions) {
		this.deviceConnectedActions = deviceConnectedActions;
	}

	public List<String> getDeviceNotConnectedActions() {
		return deviceNotConnectedActions;
	}

	public void setDeviceNotConnectedActions(List<String> deviceNotConnectedActions) {
		this.deviceNotConnectedActions = deviceNotConnectedActions;
	}

	public Long getWfMobileIDStepIdentityConfigId() {
		return wfMobileIDStepIdentityConfigId;
	}

	public void setWfMobileIDStepIdentityConfigId(Long wfMobileIDStepIdentityConfigId) {
		this.wfMobileIDStepIdentityConfigId = wfMobileIDStepIdentityConfigId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Boolean getSoftOTP() {
		return softOTP;
	}

	public void setSoftOTP(Boolean softOTP) {
		this.softOTP = softOTP;
	}

	public Boolean getIsCertificates() {
		return isCertificates;
	}

	public void setIsCertificates(Boolean isCertificates) {
		this.isCertificates = isCertificates;
	}

	public Boolean getPushVerify() {
		return pushVerify;
	}

	public void setPushVerify(Boolean pushVerify) {
		this.pushVerify = pushVerify;
	}

	public Boolean getFido2() {
		return fido2;
	}

	public void setFido2(Boolean fido2) {
		this.fido2 = fido2;
	}

	public String getFrontOrientation() {
		return frontOrientation;
	}

	public void setFrontOrientation(String frontOrientation) {
		this.frontOrientation = frontOrientation;
	}

	public String getBackOrientation() {
		return backOrientation;
	}

	public void setBackOrientation(String backOrientation) {
		this.backOrientation = backOrientation;
	}

	public OTPConfig getOtpConfig() {
		return otpConfig;
	}

	public void setOtpConfig(OTPConfig otpConfig) {
		this.otpConfig = otpConfig;
	}

	public List<UserIdentityData> getUserIdentityData() {
		return userIdentityData;
	}

	public void setUserIdentityData(List<UserIdentityData> userIdentityData) {
		this.userIdentityData = userIdentityData;
	}

}
