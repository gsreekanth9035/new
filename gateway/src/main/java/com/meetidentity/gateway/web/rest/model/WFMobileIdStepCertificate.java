package com.meetidentity.gateway.web.rest.model;

public class WFMobileIdStepCertificate {
	private Long wfMobileIdStepCertId;
	private String caServer;
	private String certTemplate;
	private boolean keyEscrow;
	private boolean disableRevoke;
	private String algorithm;
	private int keysize;
	private String subjectDN;
	private CSRrdns csrRdns;
	private String subjectAN;
	private String templateOid;
	private String templateMajorVer;
	private String templateMinorVer;

	private String csr;
	private String certificateContent;

	public Long getWfMobileIdStepCertId() {
		return wfMobileIdStepCertId;
	}

	public void setWfMobileIdStepCertId(Long wfMobileIdStepCertId) {
		this.wfMobileIdStepCertId = wfMobileIdStepCertId;
	}

	public String getCaServer() {
		return caServer;
	}

	public void setCaServer(String caServer) {
		this.caServer = caServer;
	}

	public String getCertTemplate() {
		return certTemplate;
	}

	public void setCertTemplate(String certTemplate) {
		this.certTemplate = certTemplate;
	}

	public boolean isKeyEscrow() {
		return keyEscrow;
	}

	public void setKeyEscrow(boolean keyEscrow) {
		this.keyEscrow = keyEscrow;
	}

	public boolean isDisableRevoke() {
		return disableRevoke;
	}

	public void setDisableRevoke(boolean disableRevoke) {
		this.disableRevoke = disableRevoke;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public int getKeysize() {
		return keysize;
	}

	public void setKeysize(int keysize) {
		this.keysize = keysize;
	}

	public String getSubjectDN() {
		return subjectDN;
	}

	public CSRrdns getCsrRdns() {
		return csrRdns;
	}

	public void setCsrRdns(CSRrdns csrRdns) {
		this.csrRdns = csrRdns;
	}

	public void setSubjectDN(String subjectDN) {
		this.subjectDN = subjectDN;
	}

	public String getSubjectAN() {
		return subjectAN;
	}

	public void setSubjectAN(String subjectAN) {
		this.subjectAN = subjectAN;
	}

	public String getCsr() {
		return csr;
	}

	public void setCsr(String csr) {
		this.csr = csr;
	}

	public String getCertificateContent() {
		return certificateContent;
	}

	public void setCertificateContent(String certificateContent) {
		this.certificateContent = certificateContent;
	}

	public String getTemplateOid() {
		return templateOid;
	}

	public void setTemplateOid(String templateOid) {
		this.templateOid = templateOid;
	}

	public String getTemplateMajorVer() {
		return templateMajorVer;
	}

	public void setTemplateMajorVer(String templateMajorVer) {
		this.templateMajorVer = templateMajorVer;
	}

	public String getTemplateMinorVer() {
		return templateMinorVer;
	}

	public void setTemplateMinorVer(String templateMinorVer) {
		this.templateMinorVer = templateMinorVer;
	}

}
