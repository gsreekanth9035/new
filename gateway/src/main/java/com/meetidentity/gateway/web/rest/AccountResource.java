package com.meetidentity.gateway.web.rest;

import java.security.Principal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meetidentity.gateway.config.Constants;
import com.meetidentity.gateway.repository.UserRepository;
import com.meetidentity.gateway.service.UserService;
import com.meetidentity.gateway.web.feign.client.APDUFeignClient;
import com.meetidentity.gateway.web.feign.client.UserManagementFeignClient;
import com.meetidentity.gateway.web.feign.client.model.Permission;
import com.meetidentity.gateway.web.feign.client.model.User;
import com.meetidentity.gateway.web.feign.client.model.Workflow;
import com.meetidentity.gateway.web.rest.model.Group;
import com.meetidentity.gateway.web.rest.model.Role;
import com.meetidentity.gateway.web.rest.model.UserDetails;
import com.meetidentity.gateway.web.rest.model.UserInfo;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    @Autowired
    private UserManagementFeignClient userManagementFeignClient;

    @Autowired
    private APDUFeignClient ApduFeignClient;
    
    private static class AccountResourceException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        private AccountResourceException(String message) {
            super(message);
        }
    }

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final UserService userService;
    
    private final UserRepository userRepository;

    public AccountResource(UserService userService, UserRepository userRepository) {
        this.userService = userService;
		this.userRepository = userRepository;
    }

    /**
     * {@code GET  /account} : get the current user.
     *
     * @param principal the current user; resolves to {@code null} if not authenticated.
     * @return the current user.
     * @throws AccountResourceException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/account")
    @SuppressWarnings("unchecked")
    public UserInfo getAccount(Principal principal) {
//    	if(principal != null){    	
//	        if (principal instanceof AbstractAuthenticationToken) {
//	            return userService.getUserFromAuthentication((AbstractAuthenticationToken) principal);
//	        } else {
//	            throw new AccountResourceException("User could not be found");
//	        }
//    	}else {
//    		 throw new AccountResourceException("User could not be found");
//    	}
    	Map<String, Object> attributes;
    	if (principal instanceof AbstractAuthenticationToken) {
    		AbstractAuthenticationToken authToken = (AbstractAuthenticationToken) principal;
    		 
    	        if (authToken instanceof OAuth2AuthenticationToken) {
    	            attributes = ((OAuth2AuthenticationToken) authToken).getPrincipal().getAttributes();
    	        } else if (authToken instanceof JwtAuthenticationToken) {
    	            attributes = ((JwtAuthenticationToken) authToken).getTokenAttributes();
    	        } else {
    	            throw new IllegalArgumentException("AuthenticationToken is not OAuth2 or JWT!");
    	        }
    	    log.info("attributes : "+attributes);
    		String userKCLCId = (String)attributes.get("sub");
            String organization = (String) attributes.get("organization");
            Group[] groups = userManagementFeignClient.getGroupsOfUserKCLCId(organization, userKCLCId);
            
            // Load Permissions and Workflow from User Management Service for all the user roles
            Set<String> permissions = new HashSet();
            Map<String, Set<String>> roleBasedPermissions = new HashMap<String, Set<String>>();
            List<Long> serviceModes = new ArrayList<Long>();
            Map<String, Long> permissionsServicemodes = new HashMap<String,Long>();
            List<Workflow> workflows = new ArrayList<>();
            
            List<String> roles = (List<String>) attributes.get("roles");
            List<String> rolesList = new ArrayList<>();    
            List<Role> roleWithColor = new ArrayList<>();
			if (roles != null) {
             roles = roles.stream() 
             .distinct() 
             .collect(Collectors.toList()); 
             roles.forEach(role -> {
            	if (role.endsWith("WORKFLOW") || role.endsWith("workflow") || role.endsWith("Workflow")) {
            		workflows.add(userManagementFeignClient.getWorkflowByRole(organization, role));
            	} else {
            		rolesList.add(role);
            		List<Permission> permissionslist = userManagementFeignClient.getPermissionsByRole(organization, role);
					/*
					 * Map<String, Long> permissionsServicemode =
					 * userManagementFeignClient.getRolePermissionModeByRole(organization, role);
					 * Collection<Long> serviceMode_c = permissionsServicemode.values(); for (Long
					 * serviceMode : serviceMode_c) { serviceModes.add(serviceMode); }
					 */
					/*
					 * for (Map.Entry<String, Long> entry : entrySet) {
					 * permissionsServicemodes.put(entry.getKey(), entry.getValue()); }
					 */
            		 Set<String> dup_permissions = new HashSet<>();
            		for (Permission permission : permissionslist) {
            			permissions.add(permission.getName());
            			dup_permissions.add(permission.getName());
					}
            		roleBasedPermissions.put(role.toLowerCase(), dup_permissions);
            	}
              });
            }
			List<Role> roleFromFeign = userManagementFeignClient.getRoleWithColor(rolesList, organization);
			roleFromFeign.forEach(roleFromFeigns -> {
				Role roleData = new Role();
				roleData.setColor(roleFromFeigns.getColor());
				roleData.setName(roleFromFeigns.getName());
				roleWithColor.add(roleData);
			});
            
            String workflow = null;
            List<String> workflowSteps = new ArrayList<>();
            if (workflows.size() > 0) {
            	workflow = workflows.get(0).getName();
            	if(workflow != null) {
                	userManagementFeignClient.getStepsByWorkflow(organization, workflow)
                		.forEach(step -> workflowSteps.add(step.getName()));
            	}
            }
            ResponseEntity<UserDetails> responseEntity = userManagementFeignClient.getUserStatus(organization, (String) attributes.get("preferred_username"));
            UserDetails userDetails = responseEntity.getBody();
            ObjectMapper objectMapper = new ObjectMapper();
            UserInfo userInfo = new UserInfo();
            userInfo.setRoleWithColor(roleWithColor);
            userInfo.setSub(userKCLCId);
            userInfo.setUsername((String) attributes.get("preferred_username"));
            userInfo.setFirstName((String) attributes.get("given_name"));
            userInfo.setLastName((String) attributes.get("family_name"));
            userInfo.setEmail((String) attributes.get("email"));
            userInfo.setExp((Instant)attributes.get("exp"));
            userInfo.setOrganization(organization);
            userInfo.setWorkflow(workflow);
            userInfo.setAuthorities(permissions);
			userInfo.setRoleBasedAuthorities(roleBasedPermissions);
            userInfo.setUserBiometrics(userDetails.getUserBiometrics());
			userInfo.setCanEnroll(userDetails.getCanEnroll());
			userInfo.setStatus(userDetails.getStatus());
			userInfo.setUserStatus(userDetails.getUserStatus());
			userInfo.setCanDisplayEnrollDetails(userDetails.getCanDisplayEnrollDetails());
			userInfo.setCanApprove(userDetails.getCanApprove());
			userInfo.setCanLifeCycle(userDetails.getCanLifeCycle());
			userInfo.setCanIssue(userDetails.getCanIssue());
			userInfo.setIsFederated(userDetails.getIsFederated());
				/*
				 * String json = objectMapper.writeValueAsString(permissionsServicemodes);
				 * System.out.println(json);
				 */
				// userInfo.setAuthoritiesServicemode(serviceModes);
			
            userInfo.setWorkflowSteps(workflowSteps);
            userInfo.setRoles(rolesList);
            userInfo.setGroups(groups);
            User user = new User();
            user.setFirstName(userInfo.getFirstName());
            user.setLastName(userInfo.getLastName());
            user.setEmail(userInfo.getEmail());
            user.setName(userInfo.getUsername());
            user.setIsFederated(userDetails.getIsFederated());
            if(userInfo.getGroups()[0] != null) {
            	 user.setGroupId(userInfo.getGroups()[0].getId());
                 user.setGroupName(userInfo.getGroups()[0].getName());
            }
            if(workflows.size() != 0) {
            	userInfo.setIdentityType(workflows.get(0).getOrganizationIdentities().getIdentityTypeName());
            }
            User returnedUser = new User();
            try {
            	returnedUser = userManagementFeignClient.registerLoggedInUser(userInfo.getOrganization(), user).getBody();
			} catch (Exception e) {
				e.printStackTrace();
			}
            userInfo.setOrgId(returnedUser.getOrganisationId());
            userInfo.setUserId(returnedUser.getId());

			Optional<com.meetidentity.gateway.domain.User> users = userRepository.findById(userKCLCId);
			if (users.isPresent()) {
				userService.updateUserDetails( userInfo, userDetails.getEnabled());
			} else {
	            com.meetidentity.gateway.domain.User userInJhi = new com.meetidentity.gateway.domain.User();
				userInJhi.setId(userKCLCId);
				userInJhi.setUserId(returnedUser.getId());
				userInJhi.setOrganizationName(organization);
				userInJhi.setOrganizationId(returnedUser.getOrganisationId());
				userInJhi.setLogin(userInfo.getUsername());
				userInJhi.setFirstName(userInfo.getFirstName());
				userInJhi.setLastName(userInfo.getLastName());
				userInJhi.setEmail(userInfo.getEmail());
				userInJhi.setImageUrl(userInfo.getImageUrl());
				userInJhi.setActivated(userDetails.getEnabled());
	            // set langKey to default
				userInJhi.setLangKey(Constants.DEFAULT_LANGUAGE);
				userRepository.save(userInJhi);
			}

            userInfo.setUserStatus(returnedUser.getStatus());
            userInfo.setIsDeviceClientSecurityEnabled(ApduFeignClient.getDeviceServiceSecurityCheck(userInfo.getOrganization()));
            return userInfo;
        }else {
        	// throw new AccountResourceException("User could not be found");
        	return new UserInfo();
        }
    	
    	
    	 /*  return Optional.ofNullable(principal)
    	            .filter(principalit -> principalit instanceof OAuth2Authentication)
    	            .map(principalit -> ((OAuth2Authentication) principalit).getUserAuthentication())*/
    	            
    }
    
    @GetMapping("/refreshAccessToken")
    public ResponseEntity<Void> refreshAccessToken() {
    	log.info("-+------------------------------------------------------------refresh-------------------------------------------------------+-");
		return new ResponseEntity<Void>(HttpStatus.OK);    
    }
}
