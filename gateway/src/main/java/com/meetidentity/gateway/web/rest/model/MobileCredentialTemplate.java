package com.meetidentity.gateway.web.rest.model;

import java.util.List;

public class MobileCredentialTemplate {
	
	private List<Long> credentialTemplateIDs;

	public List<Long> getCredentialTemplateIDs() {
		return credentialTemplateIDs;
	}

	public void setCredentialTemplateIDs(List<Long> credentialTemplateIDs) {
		this.credentialTemplateIDs = credentialTemplateIDs;
	}
	
}
