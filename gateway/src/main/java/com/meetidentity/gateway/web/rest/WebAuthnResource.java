package com.meetidentity.gateway.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meetidentity.gateway.web.feign.client.UserManagementFeignClient;
import com.meetidentity.gateway.web.feign.client.model.WebAuthnAuthenticatorRegistration;
import com.meetidentity.gateway.web.feign.client.model.WebAuthnPolicyDetails;

@RestController
@RequestMapping("/api/v1/organizations/{organizationName}/webauthn")
public class WebAuthnResource {

    private final Logger log = LoggerFactory.getLogger(WebAuthnResource.class);

    @Autowired
    private UserManagementFeignClient userManagementFeignClient;

   
    /**
     * {@code GET /policy-details/users/{userName}} : get webauthn policy-details.
     *
     * @param organizationName, userName.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body all WebAuthnPolicyDetails.
     */
    @GetMapping("/policy-details/users/{userName}")
    public WebAuthnPolicyDetails webAuthnPolicyDetails(@PathVariable String organizationName, @PathVariable String userName) {
        final ResponseEntity<WebAuthnPolicyDetails> webAuthnPolicyDetails = userManagementFeignClient.webauthnPolicyDetails(organizationName, userName);
        log.info(webAuthnPolicyDetails.getBody().toString());
        return webAuthnPolicyDetails.getBody();
    }

    @PostMapping("/register-authenticator/users/{userName}")
    public WebAuthnAuthenticatorRegistration registerWebAuthnAuthenticator(@PathVariable String organizationName, @PathVariable String userName, @RequestBody WebAuthnAuthenticatorRegistration webAuthnAuthenticatorRegistration) {
        final ResponseEntity<WebAuthnAuthenticatorRegistration> webAuthnAuthenticatorRegistration_ = userManagementFeignClient.registerWebAuthnAuthenticator(organizationName, userName, webAuthnAuthenticatorRegistration);
        log.info(webAuthnAuthenticatorRegistration_.getBody().toString());
        return webAuthnAuthenticatorRegistration_.getBody();
    }
}
