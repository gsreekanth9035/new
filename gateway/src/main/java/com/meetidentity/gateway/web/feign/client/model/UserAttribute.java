package com.meetidentity.gateway.web.feign.client.model;

public class UserAttribute {
	
	private String name;
	private String value;
	
	public UserAttribute() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}


}
