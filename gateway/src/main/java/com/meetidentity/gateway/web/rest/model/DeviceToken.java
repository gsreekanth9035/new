package com.meetidentity.gateway.web.rest.model;

public class DeviceToken {
	
	private String tokenType;
	private String encAccessToken;
	
	public DeviceToken() {
		
	}
	
	public DeviceToken(String tokenType, String encAccessToken) {
		super();
		this.tokenType = tokenType;
		this.encAccessToken = encAccessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public String getEncAccessToken() {
		return encAccessToken;
	}

	public void setEncAccessToken(String encAccessToken) {
		this.encAccessToken = encAccessToken;
	}

	

}
