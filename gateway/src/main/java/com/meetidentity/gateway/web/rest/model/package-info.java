/**
 * View Models used by Spring MVC REST controllers.
 */
package com.meetidentity.gateway.web.rest.model;
