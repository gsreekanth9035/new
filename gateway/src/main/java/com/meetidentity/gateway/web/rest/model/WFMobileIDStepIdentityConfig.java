package com.meetidentity.gateway.web.rest.model;

import java.util.ArrayList;
import java.util.List;


public class WFMobileIDStepIdentityConfig {
	private Long wfMobileIDStepIdentityConfigId;
	private String friendlyName;
	private boolean pushVerify;
	private boolean softOTP;
	private boolean fido2;
	private String identityTypeName;

	private List<Long> visualTemplateIds = new ArrayList<>();
	private List<WFMobileIdStepCertificate> wfMobileIdStepCertificates = new ArrayList<>();
	private WFMobileIDIdentitySoftOTPConfig wfMobileIDIdentitySoftOTPConfig = new WFMobileIDIdentitySoftOTPConfig();

	private boolean isContentSigning;
	private String noOfDaysBeforeCertExpiry;
	private String emailNotificationFreq;

	public Long getWfMobileIDStepIdentityConfigId() {
		return wfMobileIDStepIdentityConfigId;
	}

	public void setWfMobileIDStepIdentityConfigId(Long wfMobileIDStepIdentityConfigId) {
		this.wfMobileIDStepIdentityConfigId = wfMobileIDStepIdentityConfigId;
	}

	public void setPushVerify(boolean pushVerify) {
		this.pushVerify = pushVerify;
	}

	public void setSoftOTP(boolean softOTP) {
		this.softOTP = softOTP;
	}

	public void setContentSigning(boolean isContentSigning) {
		this.isContentSigning = isContentSigning;
	}

	public String getFriendlyName() {
		return friendlyName;
	}

	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}

	public Boolean getPushVerify() {
		return pushVerify;
	}

	public void setPushVerify(Boolean pushVerify) {
		this.pushVerify = pushVerify;
	}

	public Boolean getSoftOTP() {
		return softOTP;
	}

	public void setSoftOTP(Boolean softOTP) {
		this.softOTP = softOTP;
	}

	public Boolean getFido2() {
		return fido2;
	}

	public void setFido2(Boolean fido2) {
		this.fido2 = fido2;
	}

	public List<Long> getVisualTemplateIds() {
		return visualTemplateIds;
	}

	public void setVisualTemplateIds(List<Long> visualTemplateIds) {
		this.visualTemplateIds = visualTemplateIds;
	}

	public List<WFMobileIdStepCertificate> getWfMobileIdStepCertificates() {
		return wfMobileIdStepCertificates;
	}

	public void setWfMobileIdStepCertificates(List<WFMobileIdStepCertificate> wfMobileIdStepCertificates) {
		this.wfMobileIdStepCertificates = wfMobileIdStepCertificates;
	}

	public Boolean getIsContentSigning() {
		return isContentSigning;
	}

	public void setIsContentSigning(Boolean isContentSigning) {
		this.isContentSigning = isContentSigning;
	}

	public String getNoOfDaysBeforeCertExpiry() {
		return noOfDaysBeforeCertExpiry;
	}

	public void setNoOfDaysBeforeCertExpiry(String noOfDaysBeforeCertExpiry) {
		this.noOfDaysBeforeCertExpiry = noOfDaysBeforeCertExpiry;
	}

	public String getEmailNotificationFreq() {
		return emailNotificationFreq;
	}

	public void setEmailNotificationFreq(String emailNotificationFreq) {
		this.emailNotificationFreq = emailNotificationFreq;
	}

	public String getIdentityTypeName() {
		return identityTypeName;
	}

	public void setIdentityTypeName(String identityTypeName) {
		this.identityTypeName = identityTypeName;
	}

	public WFMobileIDIdentitySoftOTPConfig getWfMobileIDIdentitySoftOTPConfig() {
		return wfMobileIDIdentitySoftOTPConfig;
	}

	public void setWfMobileIDIdentitySoftOTPConfig(WFMobileIDIdentitySoftOTPConfig wfMobileIDIdentitySoftOTPConfig) {
		this.wfMobileIDIdentitySoftOTPConfig = wfMobileIDIdentitySoftOTPConfig;
	}

}
