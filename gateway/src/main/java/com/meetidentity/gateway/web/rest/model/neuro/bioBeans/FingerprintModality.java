package com.meetidentity.gateway.web.rest.model.neuro.bioBeans;

import java.io.Serializable;

public class FingerprintModality implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fingerInputData;
	private String fingerPosition ; // Ex NFPosition.LEFT_INDEX_FINGER
   
	public String getFingerInputData() {
		return fingerInputData;
	}
	public void setFingerInputData(String fingerInputData) {
		this.fingerInputData = fingerInputData;
	}
	
	public String getFingerPosition() {
		return fingerPosition;
	}
	public void setFingerPosition(String fingerPosition) {
		this.fingerPosition = fingerPosition;
	}    
}
