package com.meetidentity.gateway.web.rest;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * REST controller for managing global OIDC logout.
 */
@RestController
public class LogoutResource {
	
	@Autowired
	private  RestTemplate restTemplate;
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {

	    return builder.setConnectTimeout(Duration.ofMillis(300000))
	     .setReadTimeout(Duration.ofMillis(300000)).build();
	}

    //private final ClientRegistration registration;
	private ClientRegistrationRepository registrations;
    public LogoutResource(ClientRegistrationRepository registrations) {
        this.registrations = registrations;
    }

    /**
     * {@code POST  /api/logout} : logout the current user.
     *
     * @param request the {@link HttpServletRequest}.
     * @param idToken the ID token.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and a body with a global logout URL and ID token.
     */
    @PostMapping("/api/logout/{orgid}")
    public ResponseEntity<?> logout(HttpServletRequest request,
                                    @AuthenticationPrincipal(expression = "idToken") OidcIdToken idToken,@PathVariable String orgid) {
    	ClientRegistration registration = registrations.findByRegistrationId(orgid);
/*        String logoutUrl = registration.getProviderDetails()
            .getConfigurationMetadata().get("end_session_endpoint").toString();*/
        String tokennUri = registration.getProviderDetails().getTokenUri();
        String endSessionEndpoint = tokennUri.replace("token", "logout"); 
        Map<String, String> logoutDetails = new HashMap<>();
        logoutDetails.put("logoutUrl", endSessionEndpoint);
        logoutDetails.put("idToken", idToken.getTokenValue());
        logoutDetails.put("clientid", registration.getClientId());
        request.getSession().invalidate();
        
        UriComponentsBuilder builder = UriComponentsBuilder //
				.fromUriString(endSessionEndpoint) //
				.queryParam("id_token_hint", idToken.getTokenValue());
        
        ResponseEntity<String> logoutResponse = restTemplate.getForEntity(builder.toUriString(), String.class);
        if (logoutResponse.getStatusCode().is2xxSuccessful()) {
			System.out.println("Successfulley logged out in Keycloak");
		} else {
			System.out.println("Could not propagate logout to Keycloak");
		}
        return ResponseEntity.ok().body(logoutDetails);
    }
}
