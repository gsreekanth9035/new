package com.meetidentity.gateway.web.rest.model;

public class UserDeviceInfo {

	private String encodedCertificate;
	private String encodedPubKey;
	private String keyGenerationAlgrthm;
	
	
	public String getEncodedCertificate() {
		return encodedCertificate;
	}
	public void setEncodedCertificate(String encodedCertificate) {
		this.encodedCertificate = encodedCertificate;
	}
	public String getEncodedPubKey() {
		return encodedPubKey;
	}
	public void setEncodedPubKey(String encodedPubKey) {
		this.encodedPubKey = encodedPubKey;
	}
	public String getKeyGenerationAlgrthm() {
		return keyGenerationAlgrthm;
	}
	public void setKeyGenerationAlgrthm(String keyGenerationAlgrthm) {
		this.keyGenerationAlgrthm = keyGenerationAlgrthm;
	}
	
	

}
