package com.meetidentity.gateway.web.rest.model;

public class AuthenticatorClientInfo {
	
	private String clientID;
	private String clientSecret;
	
	public AuthenticatorClientInfo() {
		
	}
	
	public AuthenticatorClientInfo(String clientID, String clientSecret) {
		super();
		this.clientID = clientID;
		this.clientSecret = clientSecret;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	
}
