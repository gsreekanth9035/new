package com.meetidentity.gateway.web.feign.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.meetidentity.gateway.web.feign.client.model.Permission;
import com.meetidentity.gateway.web.rest.model.BiometricVerificationResponse;
import com.meetidentity.gateway.web.rest.model.neuro.bioBeans.UserBioInfo;

@FeignClient(name="biometricverification", url="http://${application.biometricverification.host}:${application.biometricverification.port}")
public interface BiometricVerificationFeignClient {
	
	@GetMapping("/api/v1/organizations/{organizationName}/biometricidentity/")
	List<Permission> getBiometricidentity(@PathVariable("organizationName") String organizationName);
	

	@PostMapping("/api/v1/organizations/{organizationName}/biometricIdentity/verify")
	ResponseEntity<BiometricVerificationResponse> verifyUserByID(
			@PathVariable("organizationName") String organizationName, @RequestBody UserBioInfo userBioInfo);
}
