

package com.meetidentity.gateway.web.feign.client.model;

public class WebAuthnPolicyDetails {

    protected String challengeValue;
    protected String userId;
    protected String username;
    protected String rpEntityName;
    protected String signatureAlgorithms;
    protected String rpId;
    protected String attestationConveyancePreference;
    protected String authenticatorAttachment;
    protected String requireResidentKey;
    protected String userVerificationRequirement;
    protected int createTimeout = 0; // not specified as option
    protected String excludeCredentialIds;
    protected boolean avoidSameAuthenticatorRegister = false;
    protected String isSetRetry;
    protected String uuid;

    public WebAuthnPolicyDetails() {
    }

	public String getChallengeValue() {
		return challengeValue;
	}

	public void setChallengeValue(String challengeValue) {
		this.challengeValue = challengeValue;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRpEntityName() {
		return rpEntityName;
	}

	public void setRpEntityName(String rpEntityName) {
		this.rpEntityName = rpEntityName;
	}

	public String getSignatureAlgorithms() {
		return signatureAlgorithms;
	}

	public void setSignatureAlgorithms(String signatureAlgorithms) {
		this.signatureAlgorithms = signatureAlgorithms;
	}

	public String getRpId() {
		return rpId;
	}

	public void setRpId(String rpId) {
		this.rpId = rpId;
	}

	public String getAttestationConveyancePreference() {
		return attestationConveyancePreference;
	}

	public void setAttestationConveyancePreference(String attestationConveyancePreference) {
		this.attestationConveyancePreference = attestationConveyancePreference;
	}

	public String getAuthenticatorAttachment() {
		return authenticatorAttachment;
	}

	public void setAuthenticatorAttachment(String authenticatorAttachment) {
		this.authenticatorAttachment = authenticatorAttachment;
	}

	public String getRequireResidentKey() {
		return requireResidentKey;
	}

	public void setRequireResidentKey(String requireResidentKey) {
		this.requireResidentKey = requireResidentKey;
	}

	public String getUserVerificationRequirement() {
		return userVerificationRequirement;
	}

	public void setUserVerificationRequirement(String userVerificationRequirement) {
		this.userVerificationRequirement = userVerificationRequirement;
	}

	public int getCreateTimeout() {
		return createTimeout;
	}

	public void setCreateTimeout(int createTimeout) {
		this.createTimeout = createTimeout;
	}

	public String getExcludeCredentialIds() {
		return excludeCredentialIds;
	}

	public void setExcludeCredentialIds(String excludeCredentialIds) {
		this.excludeCredentialIds = excludeCredentialIds;
	}

	public boolean isAvoidSameAuthenticatorRegister() {
		return avoidSameAuthenticatorRegister;
	}

	public void setAvoidSameAuthenticatorRegister(boolean avoidSameAuthenticatorRegister) {
		this.avoidSameAuthenticatorRegister = avoidSameAuthenticatorRegister;
	}

	public String getIsSetRetry() {
		return isSetRetry;
	}

	public void setIsSetRetry(String isSetRetry) {
		this.isSetRetry = isSetRetry;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public String toString() {
		return "WebAuthnPolicyDetails [challengeValue=" + challengeValue + ", userId=" + userId + ", username="
				+ username + ", rpEntityName=" + rpEntityName + ", signatureAlgorithms=" + signatureAlgorithms
				+ ", rpId=" + rpId + ", attestationConveyancePreference=" + attestationConveyancePreference
				+ ", authenticatorAttachment=" + authenticatorAttachment + ", requireResidentKey=" + requireResidentKey
				+ ", userVerificationRequirement=" + userVerificationRequirement + ", createTimeout=" + createTimeout
				+ ", excludeCredentialIds=" + excludeCredentialIds + ", avoidSameAuthenticatorRegister="
				+ avoidSameAuthenticatorRegister + ", isSetRetry=" + isSetRetry + ", uuid=" + uuid + "]";
	}

    

}
