package com.meetidentity.gateway.web.rest.model;

public class PrintedCredential {
	
	private Long credentialTemplateID;
	private String base64FrontCredential;
	private String base64BackCredential;
	// ADD additional attributes if any
	
	
	public Long getCredentialTemplateID() {
		return credentialTemplateID;
	}
	public void setCredentialTemplateID(Long credentialTemplateID) {
		this.credentialTemplateID = credentialTemplateID;
	}
	public String getBase64FrontCredential() {
		return base64FrontCredential;
	}
	public void setBase64FrontCredential(String base64FrontCredential) {
		this.base64FrontCredential = base64FrontCredential;
	}
	public String getBase64BackCredential() {
		return base64BackCredential;
	}
	public void setBase64BackCredential(String base64BackCredential) {
		this.base64BackCredential = base64BackCredential;
	}
	
}
