package com.meetidentity.gateway.web.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CSRrdns {
	private String CN;
	private String OU;
	private String O;
	private String L;
	private String S;
	private String C;
	private String E;
	private String SERIALNUMBER;
	private String subjectDN;

	public String getCN() {
		return CN;
	}

	public void setCN(String CN) {
		this.CN = CN;
	}

	public String getOU() {
		return OU;
	}

	public void setOU(String OU) {
		this.OU = OU;
	}

	public String getO() {
		return O;
	}

	public void setO(String O) {
		this.O = O;
	}

	public String getL() {
		return L;
	}

	public void setL(String L) {
		this.L = L;
	}

	public String getS() {
		return S;
	}

	public void setS(String S) {
		this.S = S;
	}

	public String getC() {
		return C;
	}

	public void setC(String C) {
		this.C = C;
	}

	public String getE() {
		return E;
	}

	public void setE(String E) {
		this.E = E;
	}

	public String getSERIALNUMBER() {
		return SERIALNUMBER;
	}

	public void setSERIALNUMBER(String SERIALNUMBER) {
		this.SERIALNUMBER = SERIALNUMBER;
	}

	public String getSubjectDN() {
		return subjectDN;
	}

	public void setSubjectDN(String subjectDN) {
		this.subjectDN = subjectDN;
	}

}
