package com.meetidentity.gateway.web.feign.client.model;

public class WebAuthnAuthenticatorRegistration{

	protected String id;
	protected String clientDataJSON;
	protected String attestationObject;
	protected String publicKeyCredentialId;
	protected String authenticatorLabel;
	protected String error;
	protected String uuid;

	public WebAuthnAuthenticatorRegistration() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientDataJSON() {
		return clientDataJSON;
	}

	public void setClientDataJSON(String clientDataJSON) {
		this.clientDataJSON = clientDataJSON;
	}

	public String getAttestationObject() {
		return attestationObject;
	}

	public void setAttestationObject(String attestationObject) {
		this.attestationObject = attestationObject;
	}

	public String getPublicKeyCredentialId() {
		return publicKeyCredentialId;
	}

	public void setPublicKeyCredentialId(String publicKeyCredentialId) {
		this.publicKeyCredentialId = publicKeyCredentialId;
	}

	public String getAuthenticatorLabel() {
		return authenticatorLabel;
	}

	public void setAuthenticatorLabel(String authenticatorLabel) {
		this.authenticatorLabel = authenticatorLabel;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "WebAuthnAuthenticatorRegistration [id=" + id + ", clientDataJSON=" + clientDataJSON
				+ ", attestationObject=" + attestationObject + ", publicKeyCredentialId=" + publicKeyCredentialId
				+ ", authenticatorLabel=" + authenticatorLabel + ", error=" + error + ", uuid=" + uuid + "]";
	}

}
