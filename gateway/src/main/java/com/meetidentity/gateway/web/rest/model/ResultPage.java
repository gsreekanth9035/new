package com.meetidentity.gateway.web.rest.model;

import java.util.List;

import org.springframework.data.domain.Sort;

public class ResultPage {

	private List<?> content;
	private long totalElements;
	private boolean last;
	private int totalPages;
	private int size;
	private int number;
	private Sort sort;
	private boolean first;
	private int numberOfElements;

	public ResultPage() {

	}

	public List<?> getContent() {
		return content;
	}

	public void setContent(List<?> content) {

		this.content = content;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(long l) {
		this.totalElements = l;
	}

	public boolean isLast() {
		return last;
	}

	public void setLast(boolean last) {
		this.last = last;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public Sort getSort() {
		return sort;
	}

	public void setSort(Sort sort) {
		this.sort = sort;
	}

	public boolean isFirst() {
		return first;
	}

	public void setFirst(boolean first) {
		this.first = first;
	}

	public int getNumberOfElements() {
		return numberOfElements;
	}

	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}

}
