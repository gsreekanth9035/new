package com.meetidentity.gateway.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.meetidentity.gateway.domain.User;

/**
 * Spring Data Elasticsearch repository for the User entity.
 */
@Repository
public interface UserSearchRepository extends ElasticsearchRepository<User, String> {
}
