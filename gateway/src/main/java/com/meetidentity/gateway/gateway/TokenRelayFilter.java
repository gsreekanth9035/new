package com.meetidentity.gateway.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.meetidentity.gateway.security.oauth2.AuthorizationHeaderUtil;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

@Component
public class TokenRelayFilter extends ZuulFilter {
	
	@Autowired
	AuthorizationHeaderUtil authorizationHeaderUtil;

    public static final String AUTHORIZATION_HEADER = "Authorization";

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        System.out.println("----------------"+ctx.getRequest().getRequestURI());
        if( ctx.getRequest().getRequestURI().contains("refreshAccessToken")) {
	        // Add specific authorization headers for OAuth2
        	if (authorizationHeaderUtil.getAuthorizationHeader(true).isPresent()) {
                ctx.addZuulRequestHeader(AUTHORIZATION_HEADER,
                		authorizationHeaderUtil.getAuthorizationHeader().get());

            }
        }else {
	        // Add specific authorization headers for OAuth2
	        if (authorizationHeaderUtil.getAuthorizationHeader().isPresent()) {
	            ctx.addZuulRequestHeader(AUTHORIZATION_HEADER,
	            		authorizationHeaderUtil.getAuthorizationHeader().get());
	
	        }
        }
        return null;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 10000;
    }
}
