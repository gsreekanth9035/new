package com.meetidentity.gateway.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

@Component
@Order(1)
public class OAuthValidationFilter  extends ZuulFilter {
    private static final Logger LOG = LoggerFactory.getLogger(OAuthValidationFilter.class);

    private static final String FILTER_TYPE = "error";
    private static final String THROWABLE_KEY = "throwable";
    private static final int FILTER_ORDER = -1;

    @Override
    public String filterType() {
        return FILTER_TYPE;
    }

    @Override
    public int filterOrder() {
        return FILTER_ORDER;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        final RequestContext context = RequestContext.getCurrentContext();
        final Object throwable = context.get(THROWABLE_KEY);
        
        

        if (throwable instanceof ZuulException) {
            ZuulException zuulException = (ZuulException) throwable;
            Throwable throwables = zuulException.getCause();
            while(throwables != null) {
            	
            	System.out.println(throwables.getMessage());
            	if(throwables instanceof OAuth2AuthenticationException) {
            		OAuth2Error oauth2error = ((OAuth2AuthenticationException)throwables).getError();
            		
            		System.out.println("inside "+ ((OAuth2AuthenticationException)throwables).getMessage());
            		 context.remove(THROWABLE_KEY);
                     // populate context with new response values
                     context.setResponseBody("authentication failure");
                     context.getResponse().setContentType("application/json");
                     // can set any error code as excepted
                     if(oauth2error.getErrorCode().equalsIgnoreCase("access_token_expired")) {
                         context.setResponseStatusCode(408);
                     }else if (oauth2error.getErrorCode().equalsIgnoreCase("access_denied")) {
                         context.setResponseStatusCode(401);
                     }
                     System.out.println("break;");
                     break;
            	}
            	throwables = throwables.getCause();
            	
            }
            
//            
//            // remove error code to prevent further error handling in follow up filters
//            context.remove(THROWABLE_KEY);
//
//            // populate context with new response values
//            context.setResponseBody("Overriding Zuul Exception Body");
//            context.getResponse().setContentType("application/json");
//            // can set any error code as excepted
//            context.setResponseStatusCode(503);
        }
        return null;
    }
}

