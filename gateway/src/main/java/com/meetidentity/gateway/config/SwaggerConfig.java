package com.meetidentity.gateway.config;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import springfox.documentation.builders.AuthorizationCodeGrantBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.service.TokenEndpoint;
import springfox.documentation.service.TokenRequestEndpoint;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.swagger.web.SecurityConfiguration;

@Configuration
@EnableSwagger2
//@Profile({ "!prod && swagger" })
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("public-api").select()
				.apis(RequestHandlerSelectors.any()).paths(PathSelectors.ant("/*")).build().apiInfo(apiInfo())
				.securitySchemes(Arrays.asList(securityScheme())).securityContexts(Arrays.asList(securityContext()));
	}

	private ApiInfo apiInfo() {
		return new ApiInfo("MeetIdentity API", "MeetIdentity API reference for developers", "https://meetidentity.com",
				"Terms of service", new Contact("MeetIdentity", "www.meetidentity.com", "contact@meetidentity.com"),
				"License of API", "https://meetidentity.com/license", Collections.emptyList());
	}

	@Bean

	public SecurityConfiguration security() {

		return SecurityConfigurationBuilder.builder()

				.clientId("web_app")

				.clientSecret("b5222343-3c60-438f-bf79-d1220d460cbe")

				.scopeSeparator(" ")

				.useBasicAuthenticationWithAccessCodeGrant(true)

				.build();

	}
	
	private SecurityScheme securityScheme() {

		GrantType grantType = new AuthorizationCodeGrantBuilder()

		.tokenEndpoint(new TokenEndpoint("https://iam-dev.meetidentity.net:8443/auth/realms/unifyia-dev/protocol/openid-connect" + "/token", "oauthtoken"))

		.tokenRequestEndpoint(

		new TokenRequestEndpoint("https://iam-dev.meetidentity.net:8443/auth/realms/unifyia-dev/protocol/openid-connect" + "/authorize", "web_app", "web_app"))

		.build();

		SecurityScheme oauth = new OAuthBuilder().name("spring_oauth")

		.grantTypes(Arrays.asList(grantType))

		.scopes(Arrays.asList(scopes()))

		.build();

		return oauth;

		}
	
	
	private AuthorizationScope[] scopes() {

		AuthorizationScope[] scopes = {

		new AuthorizationScope("read", "for read operations"),

		new AuthorizationScope("write", "for write operations"),

		new AuthorizationScope("/*", "Access all APIs") };

		return scopes;

		}
	
	private SecurityContext securityContext() {

		return SecurityContext.builder()

		.securityReferences(

		Arrays.asList(new SecurityReference("spring_oauth", scopes())))

		.forPaths(PathSelectors.regex("/.*"))

		.build();

		}

}
