package com.meetidentity.gateway.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties.Provider;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.jwt.JwtException;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationProvider;
import org.springframework.security.oauth2.server.resource.authentication.JwtBearerTokenAuthenticationConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


@Component
public class TenantAwareDynamicJwtDecoder implements JwtDecoder {
	
	private Map<String, JwtDecoder> jwtDecoders = new HashMap<String, JwtDecoder>();
	//private final JwtDecoder demo2JwtDecoder;

	public TenantAwareDynamicJwtDecoder(OAuth2ClientProperties oauth2ClientProperties) {
		// TODO hard coded tenant aware jwt decoders for now... replace with lazily
		// computed cache
		Map<String, Provider> providers = oauth2ClientProperties.getProvider();

		for (Entry<String, Provider> provider_entry : providers.entrySet()) {
			
			jwtDecoders.put(provider_entry.getKey(), JwtDecoders.fromIssuerLocation(provider_entry.getValue().getIssuerUri()));
			//jwt decoders can also be created using jwt-set-uri of the issuer(which points to issuer keys and certs)
			//NimbusJwtDecoder jwtDecoder = (NimbusJwtDecoder.withJwkSetUri(jwkSetUri));
			
			//jwt decoders can also be created using RSAPublickey of the issuer (which points to RSA Public key)
			//NimbusJwtDecoder jwtDecoder = (NimbusJwtDecoder.withPublicKey(RSAPublickey));			

			//jwt decoders can also be created using secretKey of the issuer (which points to secretKey)
			//NimbusJwtDecoder jwtDecoder = (NimbusJwtDecoder.withSecretKey(secretKey));
		}
		
	}

	@Override
	public Jwt decode(String token) throws JwtException {
		return selectDecoder().decode(token);
	}

	private JwtDecoder selectDecoder() {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
			System.out.println(request.getServerName());
			JwtDecoder jwtDecoder = null;
			jwtDecoder = jwtDecoders.get(request.getHeader("x-tenant"));
		if (jwtDecoder != null) {
			return jwtDecoder;
		}


		throw new IllegalArgumentException("unknown tenant");
	}
}
