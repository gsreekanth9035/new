package com.meetidentity.gateway.config;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableJpaRepositories("com.meetidentity.gateway.repository")
@EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorAware")
@EnableTransactionManagement
@EnableElasticsearchRepositories("com.meetidentity.gateway.repository.search")
public class DatabaseConfiguration {

	private final Logger log = LoggerFactory.getLogger(DatabaseConfiguration.class);

	private final Environment env;

	public DatabaseConfiguration(Environment env) {
		this.env = env;
	}

	/**
	 * Open the TCP port for the H2 database, so it is available remotely.
	 *
	 * @return the H2 database TCP server.
	 * @throws SQLException if the server failed to start.
	 */
	DataSourceProperties DataSourceProperties = null;

	/*
	 * @Bean
	 * 
	 * @Primary
	 * 
	 * @ConfigurationProperties(prefix="spring.datasource") public
	 * DataSourceProperties dataSourceProperties() { return
	 * this.DataSourceProperties = new DataSourceProperties(); }
	 * 
	 * @Bean public DataSource dataSource() {
	 * log.debug("Configuring Master Datasource");
	 * 
	 * DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
	 * dataSourceBuilder.type(dataSourceProperties.getType());
	 * dataSourceBuilder.url(dataSourceProperties.getUrl());
	 * dataSourceBuilder.username(dataSourceProperties.getUsername());
	 * dataSourceBuilder.password(dataSourceProperties.getPassword());
	 * 
	 * HikariDataSource dataSource=new HikariDataSource();
	 * dataSource.setDataSource(dataSourceBuilder.build()); return dataSource;
	 * 
	 * HikariConfig config = new HikariConfig();
	 * config.setDataSource(DataSourceProperties.initializeDataSourceBuilder().build
	 * ()); HikariDataSource dataSource=new HikariDataSource(config);
	 * //dataSource.setDataSource(dataSourceProperties().initializeDataSourceBuilder
	 * ().build()); return dataSource; }
	 */
	
//	@Bean
//	@ConfigurationProperties("spring.datasource")
//	public HikariDataSource dataSource() {
//	    return (HikariDataSource) DataSourceBuilder.create()
//	            .type(HikariDataSource.class).build();
//	}
	
	@Bean
	@Primary
	@ConfigurationProperties("spring.datasource")
	public DataSourceProperties dataSourceProperties() {
	    return new DataSourceProperties();
	}

	@Bean
	@ConfigurationProperties("spring.datasource")
	public HikariDataSource dataSource(DataSourceProperties properties) {
	    return (HikariDataSource) properties.initializeDataSourceBuilder()
	            .type(HikariDataSource.class).build();
	}

}
