package com.meetidentity.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Gateway.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
	
	private Usermanagement usermanagement = new Usermanagement();
	private Biometricverification biometricverification = new Biometricverification();
	private Apdu Apdu = new Apdu();

	public static class Usermanagement {
		
		private String host;
		private String port;
		
		public String getHost() {
			return host;
		}
		public void setHost(String host) {
			this.host = host;
		}
		public String getPort() {
			return port;
		}
		public void setPort(String port) {
			this.port = port;
		}
		
	}
	
	public static class Biometricverification {
		
		private String host;
		private String port;
		
		public String getHost() {
			return host;
		}
		public void setHost(String host) {
			this.host = host;
		}
		public String getPort() {
			return port;
		}
		public void setPort(String port) {
			this.port = port;
		}
		
	}
	
	public static class Apdu {
		
		private String host;
		private String port;
		
		public String getHost() {
			return host;
		}
		public void setHost(String host) {
			this.host = host;
		}
		public String getPort() {
			return port;
		}
		public void setPort(String port) {
			this.port = port;
		}
		
	}

	public Usermanagement getUsermanagement() {
		return usermanagement;
	}

	public void setUsermanagement(Usermanagement usermanagement) {
		this.usermanagement = usermanagement;
	}

	public Biometricverification getBiometricverification() {
		return biometricverification;
	}

	public void setBiometricverification(Biometricverification biometricverification) {
		this.biometricverification = biometricverification;
	}

	public Apdu getApdu() {
		return Apdu;
	}

	public void setApdu(Apdu apdu) {
		Apdu = apdu;
	}
	
	
	
}
