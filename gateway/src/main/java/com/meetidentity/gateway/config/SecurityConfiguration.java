package com.meetidentity.gateway.config;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtException;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.UriComponentsBuilder;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

import com.meetidentity.gateway.security.AuthoritiesConstants;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Import(SecurityProblemSupport.class)
class SecurityConfiguration {

		@Bean
		public WebSecurityConfigurerAdapter webSecurityConfigurer( //
//				@Value("${kc.realm}") String realm, //
				KeycloakOauth2UserService keycloakOidcUserService, //
				KeycloakLogoutHandler keycloakLogoutHandler, //
				KeycloakAuthenticationFailureHandler keycloakAuthenticationFailureHandler
		) {
			return new WebSecurityConfigurerAdapter() {
				@Override
				public void configure(HttpSecurity http) throws Exception {

					http.cors().and().csrf().disable()
							.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS).and()
							.authorizeRequests()							
							.antMatchers("/api/v1/devices").permitAll()			
							.antMatchers("/ping/ping").permitAll()
							.antMatchers("/api/v1/devices/*/token").permitAll()
							.antMatchers("/api/version/getBuildInfo").permitAll()
							.antMatchers("/api/v1/organizations/*/webauthn/**").permitAll()
							.antMatchers("/api/**").hasAnyAuthority(AuthoritiesConstants.ADMIN,AuthoritiesConstants.OPERATOR,AuthoritiesConstants.ADJUDICATOR,AuthoritiesConstants.USER,AuthoritiesConstants.MOBILEID_USER, 
									AuthoritiesConstants.OPERATOR_SPONSOR, AuthoritiesConstants.OPERATOR_REGISTRAR, AuthoritiesConstants.OPERATOR_ISSUER, AuthoritiesConstants.OPERATOR_DIGITAL_SIGNATORY, AuthoritiesConstants.OPERATOR_APPROVAL_AUTHORITY)
							.anyRequest().permitAll().and()
		
							
							.logout().addLogoutHandler(keycloakLogoutHandler).and()
							.oauth2Login().userInfoEndpoint().oidcUserService(keycloakOidcUserService).and().failureHandler(keycloakAuthenticationFailureHandler)
//							.and()
//				            .oauth2ResourceServer()
//				            .jwt()
//			                .jwtAuthenticationConverter(authenticationConverter())
//			                .and().and().oauth2Client()
							
							// .and().failureUrl("/")
//							.and()
//							// I don't want a page with different clients as login options
//							// So i use the constant from OAuth2AuthorizationRequestRedirectFilter
//							// plus the configured realm as immediate redirect to Keycloak
//							.loginPage(DEFAULT_AUTHORIZATION_REQUEST_BASE_URI + "/" + realm)
							;
				}
				
//				Converter<Jwt, AbstractAuthenticationToken> authenticationConverter() {
//		        	JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
//		        	jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(new JwtGrantedAuthorityConverter());
//		       		 return jwtAuthenticationConverter;
//	    		}
			};
		}
		
		@Bean
	    CorsConfigurationSource corsConfigurationSource() {
	        CorsConfiguration configuration = new CorsConfiguration();
	        configuration.setAllowedOrigins(Arrays.asList("*"));
	        configuration.setAllowedMethods(Arrays.asList("*"));
	        configuration.setAllowedHeaders(Arrays.asList("*"));
	        configuration.setAllowCredentials(true);
	        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	        source.registerCorsConfiguration("/**", configuration);
	        return source;
	    }

		@Bean
		KeycloakOauth2UserService keycloakOidcUserService(/*OAuth2ClientProperties oauth2ClientProperties*/) {

			// TODO use default JwtDecoder - where to grab?
//			NimbusJwtDecoder jwtDecoder = new NimbusJwtDecoder(
//					oauth2ClientProperties.getProvider().get("keycloak").getJwkSetUri());
			NimbusJwtDecoder jwtDecoder = null;
			SimpleAuthorityMapper authoritiesMapper = new SimpleAuthorityMapper();
			authoritiesMapper.setConvertToUpperCase(true);

			return new KeycloakOauth2UserService(jwtDecoder, authoritiesMapper);
		}

		@Bean
		KeycloakLogoutHandler keycloakLogoutHandler() {
			return new KeycloakLogoutHandler(new RestTemplate());
		}
		
		@Bean
		KeycloakAuthenticationFailureHandler keycloakAuthenticationFailureHandler() {
			return new KeycloakAuthenticationFailureHandler();
		}
	}

	@RequiredArgsConstructor
	class KeycloakOauth2UserService extends OidcUserService {

		private final OAuth2Error INVALID_REQUEST = new OAuth2Error(OAuth2ErrorCodes.INVALID_REQUEST);

		private JwtDecoder jwtDecoder;

		private final GrantedAuthoritiesMapper authoritiesMapper;
		
//		 @Autowired
//		 private OAuth2ClientProperties oauth2ClientProperties;
	//	

		public KeycloakOauth2UserService(NimbusJwtDecoder jwtDecoder2, SimpleAuthorityMapper authoritiesMapper2) {
			this.jwtDecoder = jwtDecoder2;
			this.authoritiesMapper = authoritiesMapper2;
		}

		/**
		 * Augments {@link OidcUserService#loadUser(OidcUserRequest)} to add authorities
		 * provided by Keycloak.
		 * 
		 * Needed because {@link OidcUserService#loadUser(OidcUserRequest)} (currently)
		 * does not provide a hook for adding custom authorities from a
		 * {@link OidcUserRequest}.
		 */
		@Override
		public OidcUser loadUser(OidcUserRequest userRequest) throws OAuth2AuthenticationException {
			
			NimbusJwtDecoder jwtDecoder = NimbusJwtDecoder.withJwkSetUri(userRequest.getClientRegistration().getProviderDetails().getJwkSetUri()).build();
			OidcUser user = super.loadUser(userRequest);
			this.jwtDecoder = jwtDecoder;
			
			Set<GrantedAuthority> authorities = new LinkedHashSet<>();
			authorities.addAll(user.getAuthorities());
			authorities.addAll(extractKeycloakAuthorities(userRequest));

			return new DefaultOidcUser(authorities, userRequest.getIdToken(), user.getUserInfo(), "preferred_username");
		}

		/**
		 * Extracts {@link GrantedAuthority GrantedAuthorities} from the AccessToken in
		 * the {@link OidcUserRequest}.
		 * 
		 * @param userRequest
		 * @return
		 */
		private Collection<? extends GrantedAuthority> extractKeycloakAuthorities(OidcUserRequest userRequest) {

			Jwt token = parseJwt(userRequest.getAccessToken().getTokenValue());

			// Would be great if Spring Security would provide something like a plugable
			// OidcUserRequestAuthoritiesExtractor interface to hide the junk below...

			@SuppressWarnings("unchecked")
			Map<String, Object> resourceMap = (Map<String, Object>) token.getClaims().get("resource_access");
			String clientId = userRequest.getClientRegistration().getClientId();

			@SuppressWarnings("unchecked")
			Map<String, Map<String, Object>> clientResource = (Map<String, Map<String, Object>>) resourceMap.get(clientId);
			if (CollectionUtils.isEmpty(clientResource)) {
				return Collections.emptyList();
			}

			@SuppressWarnings("unchecked")
			List<String> clientRoles = (List<String>) clientResource.get("roles");
			if (CollectionUtils.isEmpty(clientRoles)) {
				return Collections.emptyList();
			}

			Collection<? extends GrantedAuthority> authorities = AuthorityUtils
					.createAuthorityList(clientRoles.toArray(new String[0]));
			if (authoritiesMapper == null) {
				return authorities;
			}

			return authoritiesMapper.mapAuthorities(authorities);
		}

		private Jwt parseJwt(String accessTokenValue) {
			try {
				// Token is already verified by spring security infrastructure
				return jwtDecoder.decode(accessTokenValue);
			} catch (JwtException e) {
				throw new OAuth2AuthenticationException(INVALID_REQUEST, e);
			}
		}
	}

	/**
	 * Propagates logouts to Keycloak.
	 * 
	 * Necessary because Spring Security 5 (currently) doesn't support
	 * end-session-endpoints.
	 */
	@Slf4j
	@RequiredArgsConstructor
	class KeycloakLogoutHandler extends SecurityContextLogoutHandler {

		private final  RestTemplate restTemplate;

		public KeycloakLogoutHandler(RestTemplate restTemplate2) {
			this.restTemplate = restTemplate2;
		}

		@Override
		public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
			super.logout(request, response, authentication);

			propagateLogoutToKeycloak((OidcUser) authentication.getPrincipal());
		}

		private void propagateLogoutToKeycloak(OidcUser user) {

			String endSessionEndpoint = user.getIssuer() + "/protocol/openid-connect/logout";

			UriComponentsBuilder builder = UriComponentsBuilder //
					.fromUriString(endSessionEndpoint) //
					.queryParam("id_token_hint", user.getIdToken().getTokenValue());

			ResponseEntity<String> logoutResponse = restTemplate.getForEntity(builder.toUriString(), String.class);
			if (logoutResponse.getStatusCode().is2xxSuccessful()) {
				System.out.println("Successfulley logged out in Keycloak");
			} else {
				System.out.println("Could not propagate logout to Keycloak");
			}
		}
	}
	
	@Slf4j
	class KeycloakAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
		
		@Override
	    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
	            AuthenticationException exception) throws IOException, ServletException {
	        System.out.println("Login failed");
	        System.out.println(exception);
	         
	        response.sendRedirect("/");
	    }
	}

	
