package com.meetidentity.gateway.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String OPERATOR = "ROLE_OPERATOR";

    public static final String OPERATOR_ISSUER = "ROLE_OPERATOR_ISSUER";

    public static final String OPERATOR_REGISTRAR = "ROLE_OPERATOR_REGISTRAR";

    public static final String OPERATOR_SPONSOR = "ROLE_OPERATOR_SPONSOR";

    public static final String OPERATOR_DIGITAL_SIGNATORY = "ROLE_OPERATOR_DIGITAL_SIGNATORY";

    public static final String OPERATOR_APPROVAL_AUTHORITY = "ROLE_OPERATOR_APPROVAL_AUTHORITY";

    public static final String ADJUDICATOR = "ROLE_ADJUDICATOR";
    
    public static final String USER = "ROLE_USER";
    
    public static final String MOBILEID_USER = "ROLE_MOBILEID_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {
    }
}
