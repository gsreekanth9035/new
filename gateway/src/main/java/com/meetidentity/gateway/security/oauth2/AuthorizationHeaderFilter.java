package com.meetidentity.gateway.security.oauth2;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

import java.io.IOException;
import java.util.Optional;

import org.springframework.core.Ordered;

import com.meetidentity.gateway.client.TokenRelayRequestInterceptor;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class AuthorizationHeaderFilter extends ZuulFilter {

    private final AuthorizationHeaderUtil headerUtil;

    public AuthorizationHeaderFilter(AuthorizationHeaderUtil headerUtil) {
        this.headerUtil = headerUtil;
    }

    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        // added by chetan ..can be deleted
        // get the white list for allowed entries
		//        Set<String> whiteList = new HashSet<>(Arrays.asList(filterConfigurationBean.getWhiteList().split(",")));
		//        RequestContext ctx = RequestContext.getCurrentContext();
		//        // if request url is part of white list then allow
		//        String url = ctx.getRequest().getRequestURL().toString();
		//        if (checkWhiteList(url, whiteList)) {
		//            return null;
		//        }
        // get headers
        // check if an authorization header is present
        try {
	        Optional<String> authorizationHeader = headerUtil.getAuthorizationHeader();
	        authorizationHeader.ifPresent(s -> ctx.addZuulRequestHeader(TokenRelayRequestInterceptor.AUTHORIZATION, s));
        }catch (Exception e) {
			System.out.println("authorization header is not present");
			try {
				ctx.getResponse().sendRedirect("/");
			} catch (IOException e1) {
				System.out.println("unable to send a redirect to the login page");
			}
		}
        return null;
    }
}
