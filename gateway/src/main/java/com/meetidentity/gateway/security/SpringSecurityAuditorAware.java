package com.meetidentity.gateway.security;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import com.meetidentity.gateway.config.Constants;

/**
 * Implementation of {@link AuditorAware} based on Spring Security.
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

	@Autowired
	private HttpServletRequest httpServletRequest;
	
    @Override
    public Optional<String> getCurrentAuditor() {
//        return SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT);
//        return Optional.of(SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
    	
    	return Optional.of(httpServletRequest.getHeader("loggedInUser")!=null ? httpServletRequest.getHeader("loggedInUser") : Constants.SYSTEM_ACCOUNT);


    }
}
