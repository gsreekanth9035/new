import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormGroup, FormBuilder, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { AbstractControl } from '@angular/forms/src/model';
import { DeviceProfile, DeviceProfileKeyManager, DeviceProfileHsmInfo } from '../device-profile.model';
import { DeviceProfileConfig, DeviceProfileServcie } from '../device-profile.service';
import { DataService } from '../data.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'jhi-ocsv8-device-profile',
    templateUrl: './add-device-profile.component.html',
    styleUrls: ['add-device-profile.scss']
})
export class AddDeviceProfileComponent implements OnInit, OnDestroy {
    id: number;
    deviceProfileForm: FormGroup;
    editMode = false;
    private deviceProfile = new DeviceProfile();
    display = true;
    message: string;
    failure = false;
    isLoadingResults = true;
    // db key manager
    disableKeyCeremony = true;
    keyCeremony = false;
    disableAdminKey = false;
    algorithmTypes: string[];
    // hsm
    hsmTypes: Array<string> = [];

    validateKeyLength = 128;
    deviceProfileConfig: DeviceProfileConfig;
    constructor(
        private formBuilder: FormBuilder,
        private route: Router,
        private ar: ActivatedRoute,
        private router: Router,
        private devProfService: DeviceProfileServcie,
        private dataService: DataService,
        private translateService: TranslateService
    ) {}
    ngOnInit() {
        this.devProfService.getAlgorithmList().subscribe(result => {
            this.algorithmTypes = result;
        }, err => {
            this.isLoadingResults = false;
        });
        this.devProfService.getHsmTypes().subscribe(result => {
            this.hsmTypes = result;
        }, err => {
            this.isLoadingResults = false;
        });
        this.dataService.cuurentObj.subscribe(data => {
            if (data === null) {
                this.cancel();
            } else {
                const productNameId = data.productName.id;
                this.devProfService.getDevProfConfig(productNameId).subscribe(res => {
                    this.deviceProfileConfig = res;
                    if (this.deviceProfileConfig === null) {
                        this.translateService.get('').subscribe(msg => {
                            this.isLoadingResults = false;
                            this.failure = true;
                            this.message = msg;
                        });
                        return;
                    }
                    this.validateKeyLength = this.deviceProfileConfig.keyLength;
                    const category = data.category.value;
                    const supplier = data.supplier.value;
                    const productName = data.productName.value;
                    this.deviceProfile.configValueCategory.value = category;
                    this.deviceProfile.configValueSupplier.value = supplier;
                    this.deviceProfile.configValueProductName.value = productName;
                    this.id = data.id;
                    if (this.id !== undefined) {
                        this.editMode = true;
                        this.devProfService.getDeviceProfileById(this.id).subscribe(result => {
                            this.deviceProfile = result;
                            if (this.deviceProfile.devProfKeyMgr !== null) {
                                if (this.deviceProfile.devProfKeyMgr.diversify === 'MasterKeyCeremony') {
                                    this.keyCeremony = true;
                                }
                                if (this.deviceProfile.devProfKeyMgr.isAdminKeySameAsMasterKey) {
                                    this.disableAdminKey = true;
                                }
                            }
                            this.buildDeviceProfileForm();
                            if (this.deviceProfile.devProfKeyMgr !== null) {
                                if (this.deviceProfile.devProfKeyMgr.diversify === 'MasterKey') {
                                    this.deviceProfileForm.controls['masterKey'].enable();
                                }
                            }
                        }, err => {
                            this.isLoadingResults = false;
                        });
                    } else {
                        this.deviceProfile.keyStorageType = 'Database';
                        this.buildDeviceProfileForm();
                        this.isLoadingResults = false;
                    }
                }, err => {
                    this.isLoadingResults = false;
                });
            }
        }, err => {
            this.isLoadingResults = false;
        });
    }
    setDeviceProfile(deviceProfile: DeviceProfile) {
        this.deviceProfileForm.get('name').setValue(deviceProfile.name);
        this.deviceProfileForm.get('description').setValue(deviceProfile.description);
        this.deviceProfileForm.get('category').setValue(deviceProfile.configValueCategory.value); this.deviceProfileForm.get('category').disable();
        this.deviceProfileForm.get('supplier').setValue(deviceProfile.configValueSupplier.value);
        this.deviceProfileForm.get('productName').setValue(deviceProfile.configValueProductName.value);
        this.deviceProfileForm.get('keyStorageType').setValue(deviceProfile.keyStorageType);

        if (deviceProfile.devProfKeyMgr !== null) {
            this.deviceProfileForm.get('diversify').setValue(deviceProfile.devProfKeyMgr.diversify);
            this.deviceProfileForm.get('masterKey').setValue(deviceProfile.devProfKeyMgr.masterKey);
            this.deviceProfileForm.get('isAdminkeySameAsMasterKey').setValue(deviceProfile.devProfKeyMgr.isAdminKeySameAsMasterKey);
            if (this.deviceProfileForm.get('isAdminkeySameAsMasterKey').value) {
                this.deviceProfileForm.controls['adminKey'].disable();
            }
            this.deviceProfileForm.get('adminKey').setValue(deviceProfile.devProfKeyMgr.adminKey);
            this.deviceProfileForm.get('customerMasterKey').setValue(deviceProfile.devProfKeyMgr.customerMasterKey);
            this.deviceProfileForm.get('customerAdminKey').setValue(deviceProfile.devProfKeyMgr.customerAdminKey);
            this.deviceProfileForm.get('factoryManagementKey').setValue(deviceProfile.devProfKeyMgr.factoryManagementKey);
            this.deviceProfileForm.get('customerManagementKey').setValue(deviceProfile.devProfKeyMgr.customerManagementKey);
        }
        if (deviceProfile.devProfHsmInfo !== null) {
            this.deviceProfileForm.get('hsmType').setValue(deviceProfile.devProfHsmInfo.hsmTypeId);
            this.deviceProfileForm.get('hsm').get('partitionName').setValue(deviceProfile.devProfHsmInfo.partitionName);
            this.deviceProfileForm.get('hsm').get('partitionSerialNumber').setValue(deviceProfile.devProfHsmInfo.partitionSerialNumber);
            this.deviceProfileForm.get('hsm').get('partitionPwd').setValue(deviceProfile.devProfHsmInfo.partitionPassword);
        }
        this.isLoadingResults = false;
    }
    buildDeviceProfileForm() {
        this.deviceProfileForm = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
            description: [''],
            category: [{ value: this.deviceProfile.configValueCategory.value, disabled: true }, Validators.required],
            supplier: [{ value: this.deviceProfile.configValueSupplier.value, disabled: true }, Validators.required],
            productName: [{ value: this.deviceProfile.configValueProductName.value, disabled: true }, Validators.required],
            keyStorageType: [this.deviceProfile.keyStorageType, [Validators.required]],
            diversify: [''],
            masterKey: [''],
            isAdminkeySameAsMasterKey: [''],
            adminKey: [''],
            customerMasterKey: [''],
            customerAdminKey: [''],
            factoryManagementKey: [''],
            customerManagementKey: [''],
            hsmType: [''],
            hsm: this.formBuilder.group({
                partitionName: [''],
                partitionSerialNumber: [''],
                partitionPwd: ['']
            })
        });
        this.applyValidationsToKeyFields();
        this.formControlValueChanged();
        if (this.editMode) {
            this.setDeviceProfile(this.deviceProfile);
        }
    }
    applyValidationsToKeyFields() {
        const keyStorageType = this.deviceProfileForm.get('keyStorageType').value;
        const productName = this.deviceProfile.configValueProductName.value;
        const diversify = this.deviceProfileForm.get('diversify');
        const masterKey = this.deviceProfileForm.get('masterKey');
        const adminKey = this.deviceProfileForm.get('adminKey');
        const customerMasterKey = this.deviceProfileForm.get('customerMasterKey');
        const customerAdminKey = this.deviceProfileForm.get('customerAdminKey');
        const factoryManagementKey = this.deviceProfileForm.get('factoryManagementKey');
        const customerManagementKey = this.deviceProfileForm.get('customerManagementKey');    
        const hsmType = this.deviceProfileForm.get('hsmType');
        const hsm = this.deviceProfileForm.get('hsm') as FormGroup;
        hsmType.clearValidators();
        hsm.clearValidators();
        if (this.deviceProfileConfig.enableDiversify) {
            diversify.setValidators([Validators.required]);
        }
        if (this.deviceProfileConfig.enableMasterKey) {
            if (this.deviceProfileConfig.masterKeyLength !== 0) {
                masterKey.setValidators([Validators.required, Validators.minLength(this.deviceProfileConfig.masterKeyLength), Validators.maxLength(this.deviceProfileConfig.masterKeyLength)]);
            } else {
                masterKey.setValidators([Validators.required, Validators.minLength(this.validateKeyLength), Validators.maxLength(this.validateKeyLength)]);
            }
        }
        if (this.deviceProfileConfig.enableAdminKey) {
            adminKey.setValidators([Validators.required, Validators.minLength(this.validateKeyLength), Validators.maxLength(this.validateKeyLength)]);
        }
        if (this.deviceProfileConfig.enableCustomerMasterKey) {
            if (this.deviceProfileConfig.masterKeyLength !== 0) {
                customerMasterKey.setValidators([Validators.required, Validators.minLength(this.deviceProfileConfig.masterKeyLength),
                    Validators.maxLength(this.deviceProfileConfig.masterKeyLength)]);
            } else {
                customerMasterKey.setValidators([Validators.required, Validators.minLength(this.validateKeyLength), Validators.maxLength(this.validateKeyLength)]);
            }
        }
        if (this.deviceProfileConfig.enableCustomerAdminKey) {
            customerAdminKey.setValidators([Validators.required, Validators.minLength(this.validateKeyLength), Validators.maxLength(this.validateKeyLength)]);
        }
        if (this.deviceProfileConfig.enableFactoryManagementKey) {
            factoryManagementKey.setValidators([Validators.required, Validators.minLength(this.validateKeyLength), Validators.maxLength(this.validateKeyLength)]);
        }
        if (this.deviceProfileConfig.enableCustomerManagementKey) {
            customerManagementKey.setValidators([Validators.required, Validators.minLength(this.validateKeyLength), Validators.maxLength(this.validateKeyLength)]);
        }
        if (keyStorageType === 'HSM') {
            hsmType.setValidators([Validators.required]);
        }
        diversify.updateValueAndValidity();
        masterKey.updateValueAndValidity();
        adminKey.updateValueAndValidity();
        customerMasterKey.updateValueAndValidity();
        customerAdminKey.updateValueAndValidity();
        hsmType.updateValueAndValidity();
        factoryManagementKey.updateValueAndValidity();
        customerManagementKey.updateValueAndValidity();
    }
    formControlValueChanged() {
        this.deviceProfileForm.get('keyStorageType').valueChanges.subscribe(mode => {
            this.applyValidationsToKeyFields();
        });
    }

    cancel() {
        this.router.navigate(['/deviceProfile']);
    }
    saveDeviceProfile() {
        this.isLoadingResults = true;
        this.failure = false;
        const devProfile = this.deviceProfile;
        devProfile.name = this.deviceProfileForm.controls.name.value;
        devProfile.description = this.deviceProfileForm.controls.description.value;
        devProfile.configValueCategory.value = this.deviceProfileForm.controls.category.value;
        devProfile.configValueSupplier.value = this.deviceProfileForm.controls.supplier.value;
        devProfile.configValueProductName.value = this.deviceProfileForm.controls.productName.value;
        devProfile.keyStorageType = this.deviceProfileForm.controls.keyStorageType.value;
        if (this.deviceProfileConfig.enableKeyManager) {
            const devProfKeyMgr = new DeviceProfileKeyManager();
            devProfKeyMgr.diversify = this.deviceProfileForm.controls.diversify.value;
            devProfKeyMgr.masterKey = this.deviceProfileForm.controls.masterKey.value;
            devProfKeyMgr.isAdminKeySameAsMasterKey = this.deviceProfileForm.controls.isAdminkeySameAsMasterKey.value;
            devProfKeyMgr.adminKey = this.deviceProfileForm.controls.adminKey.value;
            devProfKeyMgr.customerMasterKey = this.deviceProfileForm.controls.customerMasterKey.value;
            devProfKeyMgr.customerAdminKey = this.deviceProfileForm.controls.customerAdminKey.value;
            devProfKeyMgr.factoryManagementKey = this.deviceProfileForm.controls.factoryManagementKey.value;
            devProfKeyMgr.customerManagementKey = this.deviceProfileForm.controls.customerManagementKey.value;
            devProfile.devProfKeyMgr = devProfKeyMgr;
            if (this.deviceProfileForm.controls.keyStorageType.value === 'HSM') {
                const devProfHsmInfo = new DeviceProfileHsmInfo();
                devProfHsmInfo.hsmTypeId = this.deviceProfileForm.controls.hsmType.value;
                devProfHsmInfo.partitionName = this.deviceProfileForm.get('hsm.partitionName').value;
                devProfHsmInfo.partitionPassword = this.deviceProfileForm.get('hsm.partitionPwd').value;
                devProfHsmInfo.partitionSerialNumber = this.deviceProfileForm.get('hsm.partitionSerialNumber').value;
                devProfile.devProfHsmInfo = devProfHsmInfo;
            }
        } else {
            devProfile.devProfKeyMgr = null;
            devProfile.devProfHsmInfo = null;
        }
        if (this.editMode) {
            this.devProfService.updateDeviceProfile(devProfile, this.id).subscribe(
                result => {
                    if (result.status === 200) {
                        window.scrollTo(0, 0);
                        this.isLoadingResults = false;
                        this.translateService.get('deviceProfile.updateSuccessMsg', {devProfName: this.deviceProfile.name}).subscribe(message => {
                            this.message = message;
                            this.display = false;
                            this.failure = false;
                        });
                    }
                },
                error => {
                    this.isLoadingResults = false;
                    this.failure = true;
                    this.handleError(error);
                    window.scrollTo(0, 0);
                  //  this.message = error.error.detail;
                }
            );
        } else {
            this.devProfService.saveDevcieProfile(devProfile).subscribe(
                result => {
                    if (result.status === 201) {
                        window.scrollTo(0, 0);
                        this.isLoadingResults = false;
                        this.translateService.get('deviceProfile.createSuccessMsg').subscribe(message => {
                            this.message = message;
                            this.display = false;
                            this.failure = false;
                        });
                    }
                },
                error => {
                    this.isLoadingResults = false;
                    this.failure = true;
                  //  this.message = error.error.detail;
                    this.handleError(error);
                    window.scrollTo(0, 0);
                }
            );
        }
    }
    handleError(error) {
        window.scrollTo(0, 0);
        if (error.status === 409) {
            this.translateService.get('deviceProfile.devProfExistsWithName', {devProfName: this.deviceProfileForm.controls.name.value}).subscribe(message => {
                this.message = message;
            });
        } else if (error.status === 406) {
            this.translateService.get('deviceProfile.failure').subscribe(message => {
                this.message = message;
            });
        } else if (error.error.status === 500) {
            this.message = error.error.detail;
        } else {
            this.translateService.get('deviceProfile.failure').subscribe(message => {
                this.message = message;
            });
        }
    }
    changeAdminKey(event) {
        if (event.target.checked) {
            this.deviceProfileForm.controls['adminKey'].setValue(this.deviceProfileForm.controls['masterKey'].value);
            this.deviceProfileForm.controls['adminKey'].disable();
        } else {
            this.deviceProfileForm.controls['adminKey'].setValue('');
            this.deviceProfileForm.controls['adminKey'].enable();
        }
    }
    enableKeyCeremony(event) {
        this.disableKeyCeremony = true;
        this.deviceProfileForm.controls['masterKey'].disable();
        this.keyCeremony = false;
        if (event.value === 'MasterKey') {
            this.deviceProfileForm.controls['masterKey'].enable();
        }
        if (event.value === 'MasterKeyCeremony') {
            this.disableKeyCeremony = false;
            this.keyCeremony = true;
        }
    }
    changeAdminKeyValue(value) {
        if (this.deviceProfileForm.controls['isAdminkeySameAsMasterKey'].value) {
            this.deviceProfileForm.controls['adminKey'].setValue(value);
        }
    }
    ngOnDestroy() {
        this.dataService.changeObj(null);
    }
}
// export function lengthValidator(val: number): ValidatorFn {
//     return (control: AbstractControl): { [key: string]: boolean } | null => {
//         if (control.value !== undefined && control.value !== null) {
//             const value = control.value;
//             if (value.length > 0 && value.length !== val) {
//                 console.log(control.errors);
//                 return { lengthValidator: true };
//             }
//         }
//         return null;
//     };
// }
