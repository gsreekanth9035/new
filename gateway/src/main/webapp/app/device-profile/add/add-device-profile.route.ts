import { Route } from '@angular/router';
import { AddDeviceProfileComponent } from './add-device-profile.component';

export const ADD_DEVICE_ROUTE: Route = {
    path: 'createDeviceProfile',
    component: AddDeviceProfileComponent,
    data: {
        authorities: [],
        // pageTitle: 'enroll.title',
        // permissions:['ENROLLMENT']
    }
};
