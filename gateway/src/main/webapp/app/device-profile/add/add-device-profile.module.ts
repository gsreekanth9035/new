import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from 'app/material/material.module';
import { ADD_DEVICE_ROUTE } from './add-device-profile.route';
import { AddDeviceProfileComponent } from './add-device-profile.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    GatewaySharedModule,
    RouterModule.forChild([ADD_DEVICE_ROUTE]),
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [AddDeviceProfileComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AddDeviceProfileModule { }
