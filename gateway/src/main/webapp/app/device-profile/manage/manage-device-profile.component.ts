import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DeviceProfileServcie } from '../device-profile.service';
import { DeviceProfile } from '../device-profile.model';
import { DataService } from '../data.service';
import { merge, of as observableOf } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from 'app/shared/util/shared-service';

@Component({
    selector: 'jhi-credential-profile',
    templateUrl: './manage-device-profile.component.html',
    styleUrls: ['manage-device-profile.scss']
})
export class ManageDeviceProfileComponent implements OnInit {
    displayedColumns: string[] = ['name', 'category', 'supplier', 'productName', 'description', 'actions'];

    category: ConfigValue;
    supplier: ConfigValue;
    productName: ConfigValue;
    dialogForm: FormGroup;
    editCase = false;
    dataSource: DeviceProfile[] = [];
    data: any;
    deviceProfileTBD: any;
    deleteMsgDisplay = false;
    delFailure = false;
    message: string;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    pageSize;
    resultsLength = 0;
    pageSizeOptions = [5, 10, 15, 20];
    parentPageSizeOptions =  [5, 10, 15, 20];

    constructor(
        public dialog: MatDialog,
        private formBuilder: FormBuilder,
        private router: Router,
        private devProfService: DeviceProfileServcie,
        private dataService: DataService,
        private translateService: TranslateService,
        private sharedService: SharedService
    ) {}
    ngOnInit() {
        this.pageSize = this.pageSizeOptions[0];
        this.getDeviceProfileList();
    }
    getDeviceProfileList() {
        this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    if (this.paginator.pageSize === undefined) {
                        this.paginator.pageSize = this.pageSize;
                    }
                    return this.devProfService.getDeviceProfileList(
                        this.sort.active,
                        this.sort.direction,
                        this.paginator.pageIndex,
                        this.paginator.pageSize
                    );
                }),
                map(data => {
                    this.resultsLength = data.totalElements;
                    if (data.totalElements > 0) {
                        this.pageSizeOptions = this.sharedService.getPageSizeOptionsBasedOnTotalElements(data.totalElements, this.parentPageSizeOptions);
                    }
                    return data.content;
                }),
                catchError(() => {
                    return observableOf([]);
                })
            )
            .subscribe(data => (this.dataSource = data));
    }

    openDialog(): void {
        this.dialogForm = this.formBuilder.group({
            category: ['', Validators.required],
            supplier: ['', Validators.required],
            productName: ['', Validators.required]
        });
        const dialogRef = this.dialog.open(DeviceProfileDialogComponent, {
            autoFocus: false,
            width: '400px',
            data: { dialogForm: this.dialogForm }
        });

        dialogRef.afterClosed().subscribe(result => {
            // redirect only if selected type are Oberthur, ocsv8
            if (result != null) {
                // if (result.deviceManufacturerType.value === 'Oberthur' && result.appletType.value === 'ocsv8') {
                this.dataService.changeObj(result);
                this.router.navigate(['/createDeviceProfile']);
                // this.router.navigate(['/ocsv8DeviceProfile', {p1: result.category.value, p2: result.supplier.value, p3: result.productName.value}] );
                // }
            }
        });
    }
    editDeviceProfile(deviceProfile) {
        console.log(deviceProfile);
        this.editCase = true;
        const dialogData: DialogData = {
            category: deviceProfile.configValueCategory,
            supplier: deviceProfile.configValueSupplier,
            productName: deviceProfile.configValueProductName,
            id: deviceProfile.id,
            dialogForm: null
        };
        this.dataService.changeObj(dialogData);
        //  if (deviceProfile.configValueDevManufacturer.value === 'Oberthur' && deviceProfile.configValueAppletType.value === 'ocsv8') {
        this.router.navigate(['/createDeviceProfile']);
        //  this.router.navigate(['/ocsv8DeviceProfile', {p1: deviceProfile.configValueCategory.value,
        //    p2: deviceProfile.configValueSupplier.value, p3: deviceProfile.configValueProductName.value, id: deviceProfile.id}] );
        // }
    }

    setDeviceProfileTBD(deviceProfile) {
        this.deleteMsgDisplay = false;
        this.delFailure = false;
        this.deviceProfileTBD = deviceProfile;
    }
    deleteDeviceProfile() {
        this.devProfService.deleteDeviceProfile(this.deviceProfileTBD.id).subscribe(
            result => {
                if (result.status === 204) {
                    this.getDeviceProfileList();
                    this.translateService.get('deviceProfile.delSuccessMsg', { deviceProfileName: `${this.deviceProfileTBD.name}` }).subscribe(msg => {
                        window.scrollTo(0, 0);
                        this.deleteMsgDisplay = true;
                        this.message = msg;
                    });
                }
            },
            error => {
                if (error.status === 409) {
                    this.translateService.get('deviceProfile.delFailureMsg', { deviceProfileName: `${this.deviceProfileTBD.name}` }).subscribe(msg => {
                        window.scrollTo(0, 0);
                        this.delFailure = true;
                        this.message = msg;
                    });
                }
            }
        );
    }
}

@Component({
    templateUrl: 'manage-deviceProfile-dialog.html'
})
export class DeviceProfileDialogComponent implements OnInit {
    categories: ConfigValue[];
    suppliers: ConfigValue[];
    productNames: ConfigValue[];
    devProfileDialogForm: FormGroup;

    constructor(
        private service: DeviceProfileServcie,
        public dialogRef: MatDialogRef<DeviceProfileDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        this.devProfileDialogForm = data.dialogForm;
    }

    changeSupplier(event) {
        this.devProfileDialogForm.controls['supplier'].reset();
        this.devProfileDialogForm.controls['productName'].reset();
        this.service.getDeviceManufactureTypes('SUPPLIER', event.value.id).subscribe(result => {
            this.suppliers = result;
        });
        this.productNames = null;
    }
    changeAppleType(event) {
        this.devProfileDialogForm.controls['productName'].reset();
        this.service.getDeviceManufactureTypes('PRODUCT_NAME', event.value.id).subscribe(result => {
            this.productNames = result;
        });
    }
    ngOnInit() {
        this.service.getDeviceManufactureTypes('CATEGORY', '').subscribe(result => {
            this.categories = result;
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
export interface DialogData {
    category: ConfigValue;
    supplier: ConfigValue;
    productName: ConfigValue;
    id: number;
    dialogForm: FormGroup;
}
export class ConfigValue {
    id: number;
    value: string;
    constructor() {}
}
