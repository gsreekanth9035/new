import { Route } from '@angular/router';
import { ManageDeviceProfileComponent } from './manage-device-profile.component';

export const DEVICE_PROFILE_ROUTE: Route = {
    path: 'deviceProfile',
    component: ManageDeviceProfileComponent,
    data: {
        authorities: [],
        pageTitle: 'deviceProfile.title',
        // permissions:['ENROLLMENT']
    }
};
