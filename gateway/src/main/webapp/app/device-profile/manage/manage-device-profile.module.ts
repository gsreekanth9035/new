import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { GatewaySharedModule } from 'app/shared';
import { DeviceProfileServcie } from 'app/device-profile/device-profile.service';
import { MaterialModule } from 'app/material/material.module';
import { DEVICE_PROFILE_ROUTE } from './manage-device-profile.route';
import { DeviceProfileDialogComponent, ManageDeviceProfileComponent } from './manage-device-profile.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataService } from '../data.service';
import { AngularSvgIconModule } from 'angular-svg-icon';

@NgModule({
  imports: [
    CommonModule,
    GatewaySharedModule,
    RouterModule.forChild([DEVICE_PROFILE_ROUTE]),
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularSvgIconModule
  ],
  entryComponents: [ManageDeviceProfileComponent, DeviceProfileDialogComponent],
  declarations: [ManageDeviceProfileComponent, DeviceProfileDialogComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [DeviceProfileServcie, DataService]
})
export class ManageDeviceProfileModule { }
