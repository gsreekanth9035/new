import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Principal } from 'app/core';
import { Page } from 'app/visual-design/page.model';

@Injectable({
    providedIn: 'root'
})
export class DeviceProfileServcie {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name
        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });
    url: string;
    constructor(private httpClient: HttpClient, private principal: Principal) {
        const organizationName = this.principal.getLoggedInUser().organization;
        this.url = window.location.origin + '/usermanagement/api/v1/organizations/' + organizationName + '/device-profiles';
    }
    getAlgorithmList(): Observable<any> {
        const list: AlgorithmType[] = [{ name: 'AES-128 ECB', value: 'AES-128 ECB' }, { name: 'AES-256 ECB', value: 'AES-256 ECB' }];
        return Observable.create(observer => {
            observer.next(list);
            observer.complete();
        });
    }
    getHsmTypes(): Observable<any> {
        const list: HsmTypes[] = [{ name: 'SafeNet Luna SA', value: 1 }, { name: 'Utimaco', value: 2 }, { name: 'Thales', value: 3 }];
        return Observable.create(observer => {
            observer.next(list);
            observer.complete();
        });
    }
    getDeviceManufactureTypes(configName, parentConfigValueId): Observable<any> {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/config/${configName}/values?parentConfigValueId=${parentConfigValueId}`,
            { headers: this.httpHeaders }
        );
    }
    getDeviceProfileList(sort: string, order: string, page: number, size: number): Observable<Page> {
        return this.httpClient.get<Page>(`${this.url}?sort=${sort}&order=${order}&page=${page}&size=${size}`, {
            headers: this.httpHeaders
        });
    }
    getDeviceProfileById(id: number): Observable<any> {
        return this.httpClient.get(this.url + '/' + id, { headers: this.httpHeaders });
    }
    saveDevcieProfile(deviceProfile): Observable<HttpResponse<any>> {
        return this.httpClient.post(this.url, deviceProfile, { headers: this.httpHeaders, observe: 'response' });
    }
    updateDeviceProfile(deviceProfile, id): Observable<HttpResponse<any>> {
        return this.httpClient.put(this.url + '/' + id, deviceProfile, { headers: this.httpHeaders, observe: 'response' });
    }
    deleteDeviceProfile(id: number): Observable<HttpResponse<any>> {
        return this.httpClient.delete(this.url + '/' + id, { headers: this.httpHeaders, observe: 'response' });
    }
    getDevProfConfig(productNameId): Observable<DeviceProfileConfig> {
        return this.httpClient.get<DeviceProfileConfig>(`${this.url}/${productNameId}/config`);
    }
}
export class AlgorithmType {
    name: string;
    value: string;
    constructor() {}
}
export class HsmTypes {
    name: string;
    value: number;
    constructor() {}
}
export class DeviceProfileConfig {
    id;
    configValueId;
    enableKeyManager: boolean;
    enableDiversify: boolean;
    enableMasterKey: boolean;
    enableAkIsIdenticalToMkCheck: boolean;
    enableAdminKey: boolean;
    enableCustomerMasterKey: boolean;
    enableCustomerAdminKey: boolean;
    enableFactoryManagementKey: boolean;
    enableCustomerManagementKey: boolean;
    keyLength: number;
    masterKeyLength: number;
    isMobileId: boolean;
    isPlasticId: boolean;
    isRfidCard: boolean;
    isMobileService: boolean;
    isAppletLoadingRequired: boolean;
}
