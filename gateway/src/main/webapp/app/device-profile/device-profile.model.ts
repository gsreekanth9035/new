import { DeviceProfileConfig } from './device-profile.service';
import { ConfigValue } from './manage/manage-device-profile.component';

export class DeviceProfile {
    id: number;
    name: string;
    description: string;
    configValueCategory = new ConfigValue();
    configValueSupplier = new ConfigValue();
    configValueProductName = new ConfigValue();
    keyStorageType: string;
    devProfKeyMgr = new DeviceProfileKeyManager();
    devProfHsmInfo = new DeviceProfileHsmInfo();
    deviceProfileConfig: DeviceProfileConfig;
    constructor() {}
}
export class DeviceProfileKeyManager {
    id: number;
    diversify: string;
    masterKey: string;
    isAdminKeySameAsMasterKey: boolean;
    adminKey: string;
    customerMasterKey: string;
    customerAdminKey: string;
    factoryManagementKey: string;
    customerManagementKey: string;
    constructor() {}
}
export class DeviceProfileHsmInfo {
    id: number;
    hsmTypeId: number;
    partitionName: string;
    partitionSerialNumber: string;
    partitionPassword: string;
    constructor() {}
}
