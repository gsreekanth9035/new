import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Device } from 'app/manage-identity-devices/manage-identity-devices.model';
import { Workflow } from 'app/workflow/workflow.model';

@Injectable({
    providedIn: 'root'
})
export class DataService {
    private initialObj = new BehaviorSubject<any>(null);
    private MIDObj = new BehaviorSubject<any>(null);
    private addIdentityDevice = new BehaviorSubject<any>(null);

    cuurentObj = this.initialObj.asObservable();
    CurrentMIDObj = this.MIDObj.asObservable();
    currentIdentityDevice = this.addIdentityDevice.asObservable();
    constructor() {}
    changeObj(obj: any) {
        this.initialObj.next(obj);
    }
    getCurrentObj() {
        return this.initialObj.getValue();
    }

    changeMIDObj(obj: any) {
        this.MIDObj.next(obj);
    }
    getCurrentMIdObj() {
        return this.MIDObj.getValue();
    }

    changeIdentityDevice(obj: string) {
        this.addIdentityDevice.next(obj);
    }
    getCurrentIdentityDevice() {
        return this.addIdentityDevice.getValue();
    }
}
export class Data {
    device: Device;
    userName: string;
    userFirstName: string;
    userLastName: string;
    userId: any;
    userEmail: string;
    isSelfService: boolean;
    onboardSelfService: string;
    isEnrolledUser = false;
    isEnrollmentInProgress = false;
    groupId: string;
    groupName: string;
    addIdentityDeviceType: string;
    workflow: Workflow;
    canEditUserDetails: boolean;
    routeFrom: string;
    isFederated: boolean;
    identityType: string;
    userKcId: string;
    constructor() {}
}
