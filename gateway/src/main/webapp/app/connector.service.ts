import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
@Injectable()
export class ConnectorService {
    errorEvent: Subject<boolean> = new Subject<boolean>();
    reLoadEvent: Subject<boolean> = new Subject<boolean>();
    localeEvent: Subject<string> = new Subject<string>();
    showError(event) {
        this.errorEvent.next(event);
    }
    reLoadUserToken(event) {
        this.reLoadEvent.next(event);
    }
    setLocale(event) {
        this.localeEvent.next(event);
    }
}
