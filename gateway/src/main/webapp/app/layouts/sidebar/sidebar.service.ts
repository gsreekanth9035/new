import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Principal } from 'app/core';

@Injectable({
    providedIn: 'root'
})
export class SidebarService {
    constructor(private httpClient: HttpClient, private principal: Principal) { }
    getDerviceServiceVersion_server() {
        const organization = this.principal.getLoggedInUser().organization;
        return this.httpClient.get(`${window.location.origin}/apdu/api/v1/${organization}/deviceService/version`, { responseType: 'text' });
    }
}
