import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './sidebar.component';
import { GatewaySharedModule } from 'app/shared';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    imports: [ RouterModule, CommonModule, GatewaySharedModule, AngularSvgIconModule, HttpClientModule, FormsModule, ReactiveFormsModule, BrowserAnimationsModule],
    declarations: [ SidebarComponent ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports: [ SidebarComponent ]
})

export class SidebarModule {}
