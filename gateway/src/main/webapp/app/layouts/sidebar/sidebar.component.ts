import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import { Principal, LoginService } from 'app/core';
import { Router } from '@angular/router';
import { DataService, Data } from 'app/device-profile/data.service';
import { SidebarService } from './sidebar.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { JhiMainComponent } from '..';
import { UserInfo } from '../../core/auth/user-info.model';
import { OrganizationBrandingInfo } from '../login/login.component';
import { UserBiometric } from 'app/enroll/userbiometric.model';
import * as compareVersions from 'compare-versions';
import { DeviceClientService } from 'app/shared/util/device-client.service';
declare const $: any;

// Metadata
export interface MenuItem {
    path: string;
    title: string;
    type: string;
    hide: boolean;
    visibleTo: string;
    icontype?: string;
    icon?: string;
    permission: string | string[];
    collapse?: string;
    children?: ChildMenuItem[];
    svgIconPath?: string;
}

export interface ChildMenuItem {
    path: string;
    title: string;
    hide?: boolean;
    icontype?: string;
    icon?: string;
    type?: string;
    permission: string | string[];
    svgIconPath?: string;
}

// Menu Items
export const MENU_ITEMS: MenuItem[] = [
    {
        path: '/dashboard',
        title: 'Dashboard',
        type: 'link',
        hide: false,
        visibleTo: 'all',
        icontype: 'svg',
        icon: 'dashboard',
        permission: ['Menu_Dashboard'],
        svgIconPath: '../../content/images/icons/dashboard_ic.svg'
    },
    // {
    //     path: '/searchUser',
    //     title: 'Search',
    //     type: 'link',
    //     hide: false,
    //     icontype: 'svg',
    //     icon: 'dvr',
    //     permission: ['Menu_Search'],
    //     svgIconPath: '../../content/images/icons/searchIcon.svg'
    // },
    {
        path: '/onboard',
        title: 'Onboard',
        type: 'link',
        hide: true,
        visibleTo: 'privileged',
        icontype: 'svg',
        icon: 'dvr',
        permission: ['Menu_OnboardUsers'],
        svgIconPath: '../../content/images/icons/listIcon.svg'
    },
    {
        path: '/expressenroll',
        title: 'Express_Enroll',
        type: 'link',
        hide: false,
        visibleTo: 'privileged',
        icontype: 'svg',
        icon: 'person_add',
        permission: ['Menu_ExpressEnrollment'],
        svgIconPath: '../../content/images/icons/express_enrollment_ic.svg'
    },
    {
        path: '/register',
        title: 'Enrollment',
        type: 'link',
        hide: true,
        visibleTo: 'privileged',
        icontype: 'svg',
        icon: 'person_add',
        permission: ['Menu_Enrollment'],
        svgIconPath: '../../content/images/icons/circlePlusIcon.svg'
    },
    {
        path: '/enrollmentDetails',
        title: 'Enrollment_Details',
        type: 'link',
        hide: true,
        visibleTo: 'privileged',
        icontype: 'svg',
        icon: 'person_add',
        permission: ['Menu_EnrollmentDetails'],
        svgIconPath: '../../content/images/icons/express_enrollment_ic.svg'
    },
    {
        path: '/approve-requests',
        title: 'Approve_Requests',
        type: 'link',
        hide: true,
        visibleTo: 'privileged',
        icontype: 'svg',
        icon: 'check',
        permission: ['Menu_Approve_Requests'],
        svgIconPath: '../../content/images/icons/circleTickIcon.svg'
    },
    {
        path: '/issuance',
        title: 'Issuance',
        type: 'link',
        hide: false,
        visibleTo: 'selfService',
        icontype: 'svg',
        icon: 'how_to_reg',
        permission: ['Menu_Issuance'],
        svgIconPath: '../../content/images/icons/addUserIcon.svg'
    },
    {
        path: '/pairmobile',
        title: 'Pair_Mobile_Device',
        type: 'link',
        hide: false,
        visibleTo: 'selfService',
        icontype: 'svg',
        icon: 'phone_android',
        permission: ['Menu_Pair_Mobile_Device'],
        svgIconPath: '../../content/images/icons/phoneIcon.svg'
    },
    {
        path: '/appointment-scheduler',
        title: 'Appointment Scheduler',
        type: 'link',
        hide: false,
        visibleTo: 'all',
        icontype: 'svg',
        icon: 'how_to_reg',
        permission: ['Schedule_Appointment'],
        svgIconPath: '../../content/images/icons/date_range-24px.svg'
    },
    {
        path: '/management',
        title: 'Management',
        type: 'sub',
        hide: false,
        visibleTo: 'all',
        icontype: 'svg',
        icon: 'fa-user-cog',
        collapse: 'management',
        permission: ['Menu_Manage'],
        svgIconPath: '../../content/images/icons/management_ic.svg',
        children: [
            {
                path: '/identityDevices',
                // path: '/identityDevices/manageIdentityDevices',
                // recent update by me: lal
                title: 'Identity_Devices',
                type: 'link',
                hide: true,
                icontype: 'svg',
                svgIconPath: '../../content/images/icons/visual_ic_edited.svg',
                permission: ['Menu_Identity_Devices']
            },

            // {
            //     path: '/manageIdentityDevices',
            //     title: 'Identity Devices',
            //     icontype: 'svg',
            //     svgIconPath: '../../content/images/icons/administrationIcon.svg',
            //     permission: 'Menu_Identity_Devices'
            // },

            {
                path: '/manage-vehicles',
                title: 'Vehicles',
                icontype: 'svg',
                svgIconPath: '../../content/images/icons/administrationIcon.svg',
                permission: ['Menu_Manage_Vehicles']
            },
            // {
            //     path: '/users',
            //     title: 'Users',
            //     icontype: 'svg',
            //     svgIconPath: '../../content/images/icons/administrationIcon.svg',
            //     permission: ['Menu_Users']
            // },
            {
                path: '/groups',
                title: 'Groups',
                type: 'link',
                icontype: 'svg',
                icon: 'dvr',
                permission: ['Menu_Groups'],
                svgIconPath: '../../content/images/icons/group_ic.svg'
            },
            {
                path: '/searchUser',
                title: 'Users',
                type: 'link',
                icontype: 'svg',
                icon: 'dvr',
                permission: ['Menu_Search'],
                svgIconPath: '../../content/images/icons/users_ic.svg'
            },
            {
                path: '/idreader',
                title: 'ID Reader',
                type: 'link',
                icontype: 'svg',
                icon: 'dvr',
                permission: ['Menu_ID_Reader'],
                svgIconPath: '../../content/images/icons/smart_card_reader_ic.svg'
            },
            {
                path: '/factoryreset',
                title: 'Factory Reset',
                type: 'link',
                icontype: 'svg',
                icon: 'dvr',
                permission: ['Menu_Factory_Reset'],
                svgIconPath: '../../content/images/icons/factory_reset.svg'
            }
        ]
    },
    {
        path: '/configuration',
        title: 'Configuration',
        type: 'sub',
        hide: false,
        visibleTo: 'privileged',
        icontype: 'svg',
        icon: 'build',
        collapse: 'configuration',
        permission: ['Menu_Device_Profile'],
        svgIconPath: '../../content/images/icons/configuration_ic.svg',
        children: [
            {
                path: '/deviceProfile',
                title: 'Device_Profile',
                icontype: 'svg',
                svgIconPath: '../../content/images/icons/device_profile_ic.svg',
                permission: ['Menu_Device_Profile']
            },
            {
                path: '/visual-design',
                title: 'Visual_Design',
                icontype: 'svg',
                svgIconPath: '../../content/images/icons/visual_ic_edited.svg',
                permission: ['Menu_Visual_Design']
            },
            {
                path: '/vehicle-registration',
                title: 'Vehicle_Registration',
                icontype: 'svg',
                svgIconPath: '../../content/images/icons/administrationIcon.svg',
                permission: ['Menu_Vehicle_Registration']
            },
            {
                path: '/card-printers',
                title: 'Card_Printers',
                icontype: 'svg',
                svgIconPath: '../../content/images/icons/administrationIcon.svg',
                permission: ['Menu_Card_Printers']
            },
            {
                path: '/workflows',
                title: 'Workflows',
                icontype: 'svg',
                svgIconPath: '../../content/images/icons/workflow_ic.svg',
                permission: ['Menu_Workflows']
            }
        ]
    },
    {
        path: '/administration',
        title: 'Administration',
        type: 'sub',
        hide: false,
        visibleTo: 'privileged',
        icontype: 'svg',
        icon: 'fa-user-clock',
        collapse: 'administration',
        permission: ['Menu_Administrator'],
        svgIconPath: '../../content/images/icons/administration_ic.svg',
        children: [
            // {
            //     path: '/organization',
            //     title: 'Organization',
            //     icontype: 'svg',
            //     svgIconPath: '../../content/images/icons/administrationIcon.svg',
            //     permission: ['Menu_Organization']
            // },
            {
                path: '/pairmobileservice',
                title: 'Pair_Mobile_Services',
                icontype: 'svg',
                svgIconPath: '../../content/images/icons/phoneIcon.svg',
                permission: ['Menu_MobileServiceBranding']
            },
            {
                path: '/relying-party',
                title: 'relytingParty',
                icontype: 'svg',
                svgIconPath: '../../content/images/icons/help_ic.svg',
                permission: ['Menu_Oauth_Client_RedirectURI']
            }
        ]
    },
    {
        path: '/monitoring',
        title: 'Monitoring',
        type: 'sub',
        hide: false,
        visibleTo: 'privileged',
        icontype: 'svg',
        icon: 'fa-user-clock',
        collapse: 'monitoring',
        permission: ['Menu_Monitor'],
        svgIconPath: '../../content/images/icons/monitoring_ic.svg',
        children: [
            {
                path: '/activity',
                title: 'Activity',
                icontype: 'svg',
                svgIconPath: '../../content/images/icons/activity_ic.svg',
                permission: ['Menu_Activity']
            },
            {
                path: '/health',
                title: 'Health',
                icontype: 'svg',
                svgIconPath: '../../content/images/icons/health_ic.svg',
                permission: ['Menu_Health']
            },
            {
                path: '/reports',
                title: 'Reports',
                icontype: 'svg',
                svgIconPath: '../../content/images/icons/report_ic.svg',
                permission: ['Menu_Reports']
            }
        ]
    },
    {
        path: '/help',
        title: 'Help',
        type: 'link',
        hide: false,
        visibleTo: 'all',
        icontype: 'svg',
        icon: 'help_outline',
        permission: ['Menu_Dashboard'],
        svgIconPath: '../../content/images/icons/help_ic.svg'
    },
    {
        path: '/logout',
        title: 'Log_out',
        type: 'link',
        hide: false,
        visibleTo: 'all',
        icontype: 'svg',
        icon: 'logout',
        permission: ['Menu_Dashboard'],
        svgIconPath: '../../content/images/icons/logout_2_2.svg'
    },

    {
        path: '/organization/create-organization',
        title: 'Organization',
        type: 'link',
        hide: true,
        visibleTo: 'privileged',
        permission: ['Menu_Enrollment']
    },

    {
        path: '/workflows/manage-workflows',
        title: 'Workflows',
        type: 'link',
        hide: true,
        visibleTo: 'privileged',
        permission: ['Menu_Enrollment']
    },

    {
        path: '/workflows/create-workflow',
        title: 'Workflows',
        type: 'link',
        hide: true,
        visibleTo: 'privileged',
        permission: ['Menu_Enrollment']
    },

    {
        path: '/createDeviceProfile',
        title: 'Create_Device_Profile',
        type: 'link',
        hide: true,
        visibleTo: 'privileged',
        permission: ['Menu_Enrollment']
    },
    {
        path: '/visual-design/create-visual-design',
        title: 'manageVisualDesign.addVisualDesign',
        type: 'link',
        hide: true,
        visibleTo: 'privileged',
        permission: ['Menu_Enrollment']
    },
    {
        path: '/identityDevices/Activate',
        title: 'Identity Devices',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Identity_Devices']
    },
    {
        path: '/identityDevices/Reissue',
        title: 'Identity Devices',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Identity_Devices']
    },
    {
        path: '/identityDevices/ChangeDevicePIN',
        title: 'Identity Devices',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Identity_Devices']
    },
    {
        path: '/identityDevices/ResetDevicePIN',
        title: 'Identity Devices',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Identity_Devices']
    },
    {
        path: '/identityDevices/Suspend',
        title: 'Identity Devices',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Identity_Devices']
    },
    {
        path: '/identityDevices/ResetOTP',
        title: 'Identity Devices',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Identity_Devices']
    },
    {
        path: '/identityDevices/ReportIncident',
        title: 'Identity Devices',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Identity_Devices']
    },
    {
        path: '/identityDevices/Revoke',
        title: 'Identity Devices',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Identity_Devices']
    },
    {
        path: '/identityDevices/Renew',
        title: 'Identity Devices',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Identity_Devices']
    },
    {
        path: '/ocsv8DeviceProfile;p1=Oberthur;p2=ocsv8',
        title: 'Device_Profile',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Identity_Devices']
    },
    {
        path: '/enrollmentReport',
        title: 'Reports',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Enrollment']
    },
    {
        path: '/issuanceReport',
        title: 'Reports',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Enrollment']
    },
    {
        path: '/issuedDevicesReport',
        title: 'Reports',
        type: 'link',
        hide: true,
        visibleTo: 'all',
        permission: ['Menu_Enrollment']
    }
];
@Component({
    selector: 'jhi-sidebar',
    templateUrl: 'sidebar.component.html',
    styleUrls: ['sidebar.scss']
})
export class SidebarComponent implements OnInit {
    public menuItems: any[];
    public deviceServiceInfo: string[] = [];
    public deviceServiceStatus: any;
    public deviceServiceVersion: any;
    public deviceServiceVersion_server: any;
    public compareVersionState: number;
    public loggedInUserFirstName: string;
    public loggedInUserLastName: string;
    public loggedInUser: string;
    public selfServiceForm: FormGroup;
    public isOperator = false;
    public isAdmin = false;
    public hasApplicantRole = false;
    public primaryRole: string;
    public secondaryRole: string;
    // public userAuthoritiesServicemode: number[];
    public userAuthorities: string[] = [];
    public map = new Map([['A', 1], ['B', 2], ['C', 3]]);
    public ecounter = 0;
    public dcounter = 0;
    public bcounter = 0;
    public eflag = false;
    public dflag = false;
    public stopflag = false;
    public searchUserForm: FormGroup;
    organizationBrandingInfo = new OrganizationBrandingInfo();
    logoStyle = {
        width: '',
        height: ''
    };
    isNoAuthorities = false;
    logoutMenuItem: any;
    userImage: any;
    deviceClientName;
    roleBasedColors: any;
    color = 'black';

    constructor(
        private loginService: LoginService,
        private sidebarService: SidebarService,
        private principal: Principal,
        private router: Router,
        private formBuilder: FormBuilder,
        private dataService: DataService,
        private deviceClientService: DeviceClientService
    ) {
        this.userAuthorities = this.principal.getLoggedInUser().authorities;
        if (this.userAuthorities.length === 0) {
            this.isNoAuthorities = true;
        }
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    }

    ngOnInit() {
        this.changeSidebarColor();
        // sidebar toggle start
        $('.leftMenu').on('click', 'li a', function() {
            $(this)
                .parent('li')
                .find('ul')
                .slideToggle();
            $(this).toggleClass('arrow');
        });
        // sidebar toggle end Menu_Administrator

        this.menuItems = MENU_ITEMS.filter(menuItem => menuItem);
        this.organizationBrandingInfo = this.loginService.getBrandingInfo();
        if (this.organizationBrandingInfo === null) {
            this.loginService.loadBrandingInformation().subscribe(brandingInformation => {
                this.organizationBrandingInfo = brandingInformation;
                this.updateBrnadingInfo();
            });
        } else {
            this.updateBrnadingInfo();
        }
        if (this.isNoAuthorities) {
            this.logoutMenuItem = this.menuItems.find(menuitem => menuitem.title === 'Log_out');
        }
        this.updatePS();
        this.loggedInUser = this.principal.getLoggedInUser().username;
        this.loggedInUserFirstName = this.principal.getLoggedInUser().firstName;
        this.loggedInUserLastName = this.principal.getLoggedInUser().lastName;
        this.setBiometrics(this.principal.getLoggedInUser().userBiometrics);
        this.principal.getLoggedInUser().roles.forEach(role => {
            if (role.includes('role_operator')) {
                this.isOperator = true;
                this.primaryRole = role;
            }
            if (role.includes('role_admin')) {
                this.isAdmin = true;
                this.primaryRole = role;
            }
            if (role.includes('role_user')) {
                this.hasApplicantRole = true;
                this.secondaryRole = role;
            }
        });
        if ((this.isOperator || this.isAdmin) && this.hasApplicantRole) {
            // then set users hasUserRoleWithOtherRoles value to true
            this.principal.getLoggedInUser().hasUserRoleWithOtherRoles = true;
        }
        this.selfServiceForm = this.formBuilder.group({
            enabledStatus: [true]
        });
        // this.userAuthoritiesServicemode = this.principal.getLoggedInUser().authoritiesServicemode;
        if (this.isOperator === true || this.isAdmin === true) {
            this.principal.getLoggedInUser().authorities = this.principal.getLoggedInUser().roleBasedAuthorities[this.primaryRole];
            this.selfServiceForm.setValue({ enabledStatus: false });
            this.principal.getLoggedInUser().isApplicantNow = false;
            this.eflag = false;
        } else {
            this.principal.getLoggedInUser().authorities = this.principal.getLoggedInUser().roleBasedAuthorities[this.secondaryRole];
            this.selfServiceForm.setValue({ enabledStatus: true });
            this.principal.getLoggedInUser().isApplicantNow = true;
            this.eflag = true;
        }
        this.deviceServiceStatus = '--';

        this.deviceClientService.getDeviceClientName().subscribe(res => {
            this.deviceClientName = res;
        });

        this.checkForDeviceService();
    }
    changeSidebarColor() {
        this.roleBasedColors = this.principal.getLoggedInUser().roleWithColor;
        this.roleBasedColors.forEach(res => {
            if (this.roleBasedColors.length === 1) {
                if (res.color !== null) {
                    this.color = res.color;
                }
            } else if (this.roleBasedColors.length > 1) {
                if (!res.name.toLowerCase().includes('role_user')) {
                    if (res.color !== null) {
                        this.color = res.color;
                    }
                }
            }
        });
    }
    updateBrnadingInfo() {
        this.logoStyle = {
            width: this.organizationBrandingInfo.sidebarLogoWidth,
            height: this.organizationBrandingInfo.sidebarLogoHeight
        };
        const pairMobileDeviceMenu = this.menuItems.find(element => element.permission.includes('Menu_Pair_Mobile_Device'));
        if (pairMobileDeviceMenu !== undefined) {
            pairMobileDeviceMenu.title = this.organizationBrandingInfo.mobileAppName;
        }
        const administrationMenu = this.menuItems.find(element => element.permission.includes('Menu_Administrator'));
        if (administrationMenu !== undefined) {
            const pairMobileSericeMenu = administrationMenu.children.find(element =>
                element.permission.includes('Menu_MobileServiceBranding')
            );
            if (pairMobileSericeMenu !== undefined) {
                pairMobileSericeMenu.title = this.organizationBrandingInfo.mobileServicesAppName;
            }
        }
    }
    checkForDeviceService() {
        // this.sidebarService.getDeviceSeviceStatus().subscribe(
        // deviceServiceStatus => {
        // this means the response code is 200 : the service is up and running, now check for version
        this.deviceClientService.getDeviceSeviceVersion().subscribe(
            deviceServiceVersion => {
                this.deviceServiceStatus = 'active';
                this.deviceServiceVersion = deviceServiceVersion;
                if (this.deviceServiceVersion_server === undefined) {
                    this.sidebarService.getDerviceServiceVersion_server().subscribe(
                        deviceServiceVersion_server => {
                            this.deviceServiceVersion_server = deviceServiceVersion_server;
                            this.compareVersionState = compareVersions(deviceServiceVersion_server, deviceServiceVersion);
                            this.deviceServiceInfo.push(this.deviceServiceStatus);
                            this.deviceServiceInfo.push(this.deviceServiceVersion);
                            this.deviceServiceInfo.push(this.deviceServiceVersion_server);
                        },
                        err => {
                            this.deviceServiceVersion_server = '--';
                            this.deviceServiceInfo.push(this.deviceServiceStatus);
                            this.deviceServiceInfo.push(this.deviceServiceVersion);
                            this.deviceServiceInfo.push(this.deviceServiceVersion_server);
                        }
                    );
                }
            },
            err => {
                this.deviceServiceStatus = '--';
                this.deviceServiceVersion = '--';
                this.deviceServiceInfo.push(this.deviceServiceStatus);
                this.deviceServiceInfo.push(this.deviceServiceVersion);
                this.deviceServiceInfo.push(this.deviceServiceVersion_server);
            }
        );
        // Retrying
        setTimeout(() => {
            this.checkForDeviceService();
        }, 5000);
    }

    navigate(menuitem) {
        // alert(this.selfServiceForm.get('enabledStatus').value);
        if (this.selfServiceForm.get('enabledStatus').value === true) {
            this.dataService.changeObj(null);
            this.dataService.changeMIDObj(null);
            this.dataService.changeIdentityDevice(null);
            this.router.navigate([`${menuitem.path}`]);
        } else if (this.selfServiceForm.get('enabledStatus').value === false) {
            // alert('inside');
            this.dataService.changeObj(null);
            this.dataService.changeMIDObj(null);
            this.dataService.changeIdentityDevice(null);
            // this.router.navigate(['/searchUser']);
            let url = `${menuitem.path}`;
            const criteria = `${menuitem.title}`;
            url = '/searchUser?searchCriteria=' + criteria;
            // alert(url);
            if (
                'Enrollment' === criteria ||
                'Issuance' === criteria ||
                'Identity_Devices' === criteria ||
                'Search_User' === criteria ||
                'Enrollment_Details' === criteria
            ) {
                this.router.navigate(['/searchUser', { searchCriteria: criteria }]);
            } else {
                this.router.navigate([`${menuitem.path}`]);
            }
        }
    }
    searchuser() {
        this.dataService.changeObj(null);
        this.dataService.changeMIDObj(null);
        this.router.navigate(['/searchUser']);
    }

    searchUser2() {
        this.router.navigate(['/searchUser', { searchCriteria: this.searchUserForm.get('searchCriteria').value }]);
        this.searchUserForm = this.formBuilder.group({
            searchCriteria: ['']
        });
    }
    searchUser(url: any) {
        // alert(url);
        this.dataService.changeObj(null);
        this.dataService.changeMIDObj(null);
        this.router.navigate(['/searchUser']);
    }
    togglefunction(event: any) {
        const isChecked = event.target.checked;
        // alert(isChecked);
        if (isChecked) {
            this.principal.getLoggedInUser().authorities = this.principal.getLoggedInUser().roleBasedAuthorities[this.secondaryRole];
            this.principal.getLoggedInUser().isApplicantNow = true;
            this.eflag = true;
            const role = this.roleBasedColors.find(e => e.name.toLowerCase() === this.secondaryRole);
            if (role !== undefined) {
                if (role.color !== null) {
                    this.color = role.color;
                } else {
                    this.color = 'black';
                }
            }
        } else {
            this.principal.getLoggedInUser().authorities = this.principal.getLoggedInUser().roleBasedAuthorities[this.primaryRole];
            this.principal.getLoggedInUser().isApplicantNow = false;
            this.eflag = false;
            const role = this.roleBasedColors.find(e => e.name.toLowerCase() === this.primaryRole);
            if (role !== undefined) {
                if (role.color !== null) {
                    this.color = role.color;
                } else {
                    this.color = 'black';
                }
            }
            // this.searchuser();
        }

        this.dataService.changeObj(null);
        this.dataService.changeMIDObj(null);
        this.router.navigate(['/dashboard']);
        // alert(this.principal.getLoggedInUser().authorities);
    }
    updatePS(): void {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            const ps = new PerfectScrollbar(elemSidebar, { wheelSpeed: 2, suppressScrollX: true });
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    mapToJson(map) {
        return JSON.stringify([...map]);
    }
    jsonToMap(jsonStr) {
        return new Map(JSON.parse(jsonStr));
    }

    logout() {
        this.loginService.logout();
    }

    setBiometrics(biometrics: UserBiometric[]) {
        if (biometrics !== null) {
            biometrics.forEach(userBiometric => {
                switch (userBiometric.type) {
                    case 'FACE': {
                        this.userImage = 'data:image/png;base64,' + userBiometric.data;
                        break;
                    }
                }
            });
        }
    }
}
