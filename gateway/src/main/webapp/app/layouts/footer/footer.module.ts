import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './footer.component';
import { GatewaySharedModule } from 'app/shared';

@NgModule({
    imports: [ RouterModule, CommonModule,
        GatewaySharedModule ],
    declarations: [ FooterComponent ],
    exports: [ FooterComponent ]
})

export class FooterModule {}
