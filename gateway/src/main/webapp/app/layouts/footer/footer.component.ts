import { Component, OnInit } from '@angular/core';
import { JhiLanguageHelper, LoginService, Principal } from 'app/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { DataService } from 'app/device-profile/data.service';
import { BuildInfo, OrganizationBrandingInfo } from '../login/login.component';
import { HttpResponse } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
    selector: 'jhi-footer',
    templateUrl: 'footer.component.html',
    styleUrls: ['footer.scss']
})
export class FooterComponent implements OnInit {
    organizationBrandingInfo: OrganizationBrandingInfo = null;
    test: Date = new Date();
    sidebarLogoText: any;
    version: string;
    poweredBy: string;
    constructor(
        private loginService: LoginService,
        private principal: Principal,
        private translateService: TranslateService,
        private jhiLanguageHelper: JhiLanguageHelper,
        private router: Router,
        private dataService: DataService
    ) {}

    ngOnInit() {
        this.loginService.loadBrandingInformation().subscribe(
            brandingInformation => {
            	this.organizationBrandingInfo = brandingInformation;
                this.poweredBy = brandingInformation.poweredBy.trim();
            },
            err => {
                // alert('error');
            }
        );
        this.loginService.getVersionAndBuildInfo().subscribe(
            (res: HttpResponse<BuildInfo>) => {
                console.log(res);
                const buildInfo = res.body;
                this.version = buildInfo.version.split('-')[0] + '.' + (buildInfo.buildNumber != null ? buildInfo.buildNumber : '' + 0);
                console.log(buildInfo.version);
                console.log(buildInfo.buildNumber);
                console.log(buildInfo.commitId);
                console.log(this.version);
                if (this.version === undefined || this.version === null) {
                    this.version = '2.0.10';
                }
                console.log(this.version);
            },
            err => {
                console.log(err);
            }
        );
    }
}
