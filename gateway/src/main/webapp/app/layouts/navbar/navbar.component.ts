import { Component, OnInit, Renderer, ViewChild, ElementRef, Directive, Inject, LOCALE_ID, HostListener, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Location, LocationStrategy, PathLocationStrategy, formatDate, DOCUMENT } from '@angular/common';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MENU_ITEMS } from '../sidebar/sidebar.component';
import { LoginService, Principal, UserInfo } from 'app/core';
import { NotificationService, NotificationModel, NotificationRequestBody } from '../notification.service';
import { PagingSpec } from 'app/monitor/monitor.service';
import { switchMap } from 'rxjs/operators';
import { timer } from 'rxjs';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { NOTIFICATION_METHOD_PORTAL, NOTIFICATION_DATE_FORMAT, CERT_RENEW } from 'app/shared/constants/configValues.constants';
import { DataService } from 'app/device-profile/data.service';
import { Device } from 'app/manage-identity-devices/manage-identity-devices.model';
import { DeviceTypeEnum } from 'app/manage-identity-devices/enum/device-types.enum';
import { JhiLanguageHelper } from '../../core/language/language.helper';
import { Keepalive } from '@ng-idle/keepalive';
import {
    EventTargetInterruptSource,
    Idle,
    DEFAULT_INTERRUPTSOURCES,
    EventTargetInterruptOptions,
    DocumentInterruptSource,
    StorageInterruptSource
} from '@ng-idle/core';
import { ConnectorService } from 'app/connector.service';
import { ThrowStmt } from '@angular/compiler';
import { ValidationByLanguageService } from 'app/shared/util/validation-by-language.service';

const misc: any = {
    navbar_menu_visible: 0,
    active_collapse: true,
    disabled_collapse_init: 0
};

declare var $: any;
@Component({
    selector: 'jhi-navbar',
    templateUrl: 'navbar.component.html'
})
export class NavbarComponent implements OnInit, OnDestroy {
    private listTitles: any[];
    location: Location;
    mobile_menu_visible: any = 0;
    private nativeElement: Node;
    private toggleButton: any;
    private sidebarVisible: boolean;
    private _router: Subscription;
    searchUserForm: FormGroup;
    noOfNotifications = 0;
    notifications: NotificationModel[] = [];
    showNotification: boolean;
    notificationWindowHeight;
    organizationId;
    notificationReqBody = new NotificationRequestBody();
    user = new UserInfo();
    subscription = new Subscription();

    pageSize = 15;
    totalPages;
    isFullListDisplayed = false;

    idleState = '';
    timedOut = false;
    idleEnd = false;
    timeInMinOrSecondsKey = 'global.minutes';
    lastPing?: Date = null;
    idleState_inact = 'Not started.';
    timedOut_inact = false;
    lastPing_inact?: number = null;
    selectedLanguage = 'English';
    expiresInSecs: number;
    displayExpTimeInMilliSecs: number;
    newTokenFetched = false;
    millisecsToExp: number;

    @ViewChild('app-navbar-cmp') button: any;
    @HostListener('document:click', [])
    onDocumentClick() {
        this.showNotification = false;
        this.isFullListDisplayed = false;
    }
    constructor(
        location: Location,
        private renderer: Renderer,
        private element: ElementRef,
        private router: Router,
        private formBuilder: FormBuilder,
        private principal: Principal,
        private translateService: TranslateService,
        private notificationService: NotificationService,
        private jhiLanguageHelper: JhiLanguageHelper,
        @Inject(LOCALE_ID) private locale: string,
        @Inject(DOCUMENT) document,
        private dataService: DataService,
        private idle: Idle,
        private keepalive: Keepalive,
        private idle_inact: Idle,
        private keepalive_inact: Keepalive,
        private loginService: LoginService,
        private connectorService: ConnectorService,
        private validationByLanguageService: ValidationByLanguageService
    ) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
        this.location = location;
        this.notificationWindowHeight = document.documentElement.clientHeight;
        this.displayExpTimeInMilliSecs = new Date(this.principal.getLoggedInUser().exp).valueOf();
        this.expiresInSecs = Math.floor((new Date(this.principal.getLoggedInUser().exp).valueOf() - new Date().valueOf()) / 1000);
        this.millisecsToExp = new Date(this.principal.getLoggedInUser().exp).valueOf();

        // sets an idle timeout of 5 seconds, for testing purposes.
        idle.setIdle(27 * 60);
        // sets a timeout period of 5 seconds. after idle+timeout seconds of inactivity, the user will be considered timed out.
        idle.setTimeout(3 * 60);
        // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
        // idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
        //idle.setInterrupts(this.createApplicationInterruptSources());

        idle.onIdleEnd.subscribe(() => {
            console.log('idle end');
            this.idleState = '3 mins';
            this.idleEnd = true;
        });
        idle.onTimeout.subscribe(() => {
            console.log('timeout');
            this.idleState = 'Timed out!';
            this.timedOut = true;
            this.logout();
        });
        idle.onIdleStart.subscribe(() => {
            this.idleState = "You've gone idle!";
            this.idleState = '3 mins';
            this.idleEnd = true;
            if (this.idleEnd) {
                $('#timeOutModal').modal('show');
            }
        });
        idle.onTimeoutWarning.subscribe(countdown => {
            const timercountmins = Math.floor(countdown / 60);
            if (timercountmins === 0) {
                this.timeInMinOrSecondsKey = 'global.seconds';
            }
            if (timercountmins >= 1) {
                const timercountsecs = Math.floor(countdown % 60);
                if (timercountsecs < 10) {
                    this.idleState = timercountmins + '.0' + timercountsecs;
                } else {
                    this.idleState = timercountmins + '.' + timercountsecs;
                }
            } else {
                this.idleState = countdown;
            }
        });
        // alert(this.expiresInSecs);
        // alert(new Date(this.principal.getLoggedInUser().exp).valueOf() - new Date().valueOf());
        // alert(this.lastPing);
        // sets an idle timeout of 5 seconds, for testing purposes.
        // idle.setIdle(this.expiresInSecs);
        // // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
        // idle.setTimeout(this.expiresInSecs);
        // // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
        // // idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
        // idle.onIdleEnd.subscribe();
        // idle.onTimeout.subscribe(() => {
        //     this.idleState = 'Timed Out!';
        //     this.timedOut = true;
        //     // const sessionExpired: HTMLElement = document.getElementById('reLogin') as HTMLElement;
        //     // sessionExpired.click();
        //     // this.logout();
        //     this.connectorService.showError(true);
        // });
        // idle.onIdleStart.subscribe(() => (this.idleState = "You've gone idle!"));
        // idle.onTimeoutWarning.subscribe(countdown => {
        //     const timercountmins = Math.floor(countdown / 60);
        //     if (timercountmins >= 1) {
        //         const timercountsecs = Math.floor(countdown % 60);
        //         if (timercountsecs < 10) {
        //             this.idleState = timercountmins + '.0' + timercountsecs + ' mins!';
        //         } else {
        //             this.idleState = timercountmins + '.' + timercountsecs + ' mins!';
        //         }
        //     } else {
        //         this.idleState = countdown + ' secs!';
        //     }
        // });
        // // sets the ping interval to 15 seconds
        // keepalive.interval(15);
        // keepalive.onPing.subscribe(() => (this.lastPing = new Date()));
        this.reset();

        this.connectorService.reLoadEvent.subscribe(result => {
            if (result === true) {
                this.expiresInSecs = Math.floor((new Date(this.principal.getLoggedInUser().exp).valueOf() - new Date().valueOf()) / 1000);
                this.displayExpTimeInMilliSecs = new Date(this.principal.getLoggedInUser().exp).valueOf();
                this.newTokenFetched = false;
            } else if (result === false) {
            }
        });

        // sets the ping interval to 60 seconds
        // keepalive_inact.interval(60);
        // keepalive_inact.onPing.subscribe(() => {
        //     // this.lastPing_inact = new Date();
        //     this.lastPing_inact = 1;
        //     keepalive.interval(30);
        //     // if ( this.timedOut_inact === false &&
        //     //     this.newTokenFetched === false) {
        //     //     this.millisecsToExp = new Date(this.principal.getLoggedInUser().exp).valueOf();
        //     //    if( Math.floor((new Date(this.principal.getLoggedInUser().exp).valueOf() - new Date().valueOf()) / 1000) <= 60 ) {
        //     //     this.connectorService.showError(true);
        //     //     this.newTokenFetched = true;
        //     //    };
        //     // } else {
        //     //     this.connectorService.showError(false);
        //     //     this.displayExpTimeInMilliSecs = new Date(this.principal.getLoggedInUser().exp).valueOf();
        //     //     this.expiresInSecs = Math.floor((new Date(this.principal.getLoggedInUser().exp).valueOf() - new Date().valueOf()) / 1000);
        //     // }
        // });
        // keepalive.onPing.subscribe(() => {
        //     this.lastPing_inact = 2;
        //     // this.principal.reIdentity(true).then(user => {
        //     //     this.user = user;
        //     //     // this.connectorService.reLoadUserToken(true);
        //     //     this.connectorService.showError(false);
        //     // });
        //     this.connectorService.showError(false);
        // });
    }
    createApplicationInterruptSources(options?: EventTargetInterruptOptions) {
        return [
            new DocumentInterruptSource(
                //'mousemove keydown DOMMouseScroll mousewheel mousedown touchstart touchmove scroll',
                'keydown mousedown touchstart',
                options
            ),
            new StorageInterruptSource()
        ];
    }

    refreshAccessToken() {
        // Retrying
        // alert('alert refresh');
        this.connectorService.showError(true);
        setTimeout(() => {
            this.refreshAccessToken();
        }, 1000 * 60 * 2.5);
    }
    reset() {
        console.log('reset');
        this.idle.watch();
        // this.idleState = 30 + ' mins!';
        this.timedOut = false;
        this.idleEnd = false;
    }
    reset_inact() {
        this.idle_inact.watch();
        this.idleState_inact = 'Started.';
        this.timedOut_inact = false;
    }

    logout() {
        this.loginService.logout();
    }
    // location: Location, private jhiLanguageHelper: JhiLanguageHelper, private jhiLanguageService: JhiLanguageService,
    //     private router: Router, private principal: Principal

    searchUser() {
        this.router.navigate(['/searchUser', { searchCriteria: this.searchUserForm.get('searchCriteria').value }]);
        this.searchUserForm = this.formBuilder.group({
            searchCriteria: ['']
        });
    }

    minimizeSidebar() {
        const body = document.getElementsByTagName('body')[0];

        if (misc.sidebar_mini_active === true) {
            body.classList.remove('sidebar-mini');
            misc.sidebar_mini_active = false;
        } else {
            setTimeout(function() {
                body.classList.add('sidebar-mini');

                misc.sidebar_mini_active = true;
            }, 300);
        }

        // we simulate the window Resize so the charts will get updated in realtime.
        const simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
        }, 180);

        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function() {
            clearInterval(simulateWindowResize);
        }, 1000);
    }
    hideSidebar() {
        const body = document.getElementsByTagName('body')[0];
        const sidebar = document.getElementsByClassName('sidebar')[0];

        if (misc.hide_sidebar_active === true) {
            setTimeout(function() {
                body.classList.remove('hide-sidebar');
                misc.hide_sidebar_active = false;
            }, 300);
            setTimeout(function() {
                sidebar.classList.remove('animation');
            }, 600);
            sidebar.classList.add('animation');
        } else {
            setTimeout(function() {
                body.classList.add('hide-sidebar');
                // $('.sidebar').addClass('animation');
                misc.hide_sidebar_active = true;
            }, 300);
        }

        // we simulate the window Resize so the charts will get updated in realtime.
        const simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
        }, 180);

        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function() {
            clearInterval(simulateWindowResize);
        }, 1000);
    }

    ngOnInit() {
        this.selectedLanguage = localStorage.getItem('selectedLanguage');
        if (this.selectedLanguage === 'es') {
            this.selectedLanguage = 'Spanish';
        }
        if (this.selectedLanguage === 'en') {
            this.selectedLanguage = 'English';
        }

        if (this.isAuthenticated()) {
            this.user = this.principal.getLoggedInUser();
            const notificationReqBody1 = new NotificationRequestBody();
            notificationReqBody1.userId = this.user.userId;
            notificationReqBody1.orgId = this.user.orgId;
            // notificationReqBody1.userGroupId = this.user.groups[0].id;
            notificationReqBody1.method = NOTIFICATION_METHOD_PORTAL;
            this.notificationService.getNotificationCount(notificationReqBody1).subscribe(res => {
                this.noOfNotifications = res.body;
            });
            setTimeout(() => {
                this.getNotificationCount(notificationReqBody1);
            }, 1000 * 60 * 15);
            setTimeout(() => {
                this.refreshAccessToken();
            }, 1000 * 60 * 2.5);
        }
        // this.notifications = [
        //     {
        //      message: 'Dear admin1, Your device: Employee ID(Unique Identifier: 47906A15660000001243),' +
        //     ' certificate of kind : authentication certifiation will expire in 10days i.e., on 5th October, 2020.' +
        //     'Workflow is updated, please re enroll',
        //      isRead: false },
        //     {
        //         message: 'Workflow is updated',
        //         isRead: false
        //     }
        // ];
        this.searchUserForm = this.formBuilder.group({
            searchCriteria: ['']
        });

        // notification menu show in navbar start
        $('.btnNotific').on('click', function(e) {
            e.stopPropagation();
            $('.notificOpen').toggleClass('show');
        });
        $(document).on('click', function() {
            $('.notificOpen').removeClass('show');
        });

        // notification menu show in navbar end

        const navbar: HTMLElement = this.element.nativeElement;
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        if (body.classList.contains('sidebar-mini')) {
            misc.sidebar_mini_active = true;
        }
        if (body.classList.contains('hide-sidebar')) {
            misc.hide_sidebar_active = true;
        }
        this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
            const $layer = document.getElementsByClassName('close-layer')[0];
            if ($layer) {
                $layer.remove();
            }
        });

        this.listTitles = MENU_ITEMS.filter(listTitle => listTitle);
    }
    getNotificationCount(notificationReqBody1: NotificationRequestBody) {
        this.subscription = timer(0, 1000 * 60 * 150)
            .pipe(switchMap(() => this.notificationService.getNotificationCount(notificationReqBody1)))
            .subscribe(res => {
                this.noOfNotifications = res.body;
            });
    }
    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
        let title: string = routeSnapshot.data && routeSnapshot.data['pageTitle'] ? routeSnapshot.data['pageTitle'] : 'gatewayApp';
        if (routeSnapshot.firstChild) {
            title = this.getPageTitle(routeSnapshot.firstChild) || title;
        }
        return title;
    }

    onResize(event) {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        setTimeout(function() {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');

        this.sidebarVisible = true;
    }

    sidebarClose() {
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    }

    sidebarToggle() {
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
        const body = document.getElementsByTagName('body')[0];

        const $toggle = document.getElementsByClassName('navbar-toggler')[0];
        if (this.mobile_menu_visible === 1) {
            // $('html').removeClass('nav-open');
            body.classList.remove('nav-open');
            // if ($layer) {
            //     $layer.remove();
            // }

            setTimeout(function() {
                $toggle.classList.remove('toggled');
            }, 400);

            this.mobile_menu_visible = 0;
        } else {
            setTimeout(function() {
                $toggle.classList.add('toggled');
            }, 430);

            const $layer = document.createElement('div');
            $layer.setAttribute('class', 'close-layer');

            if (body.querySelectorAll('.main-panel')) {
                document.getElementsByClassName('main-panel')[0].appendChild($layer);
            } else if (body.classList.contains('off-canvas-sidebar')) {
                document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
            }

            setTimeout(function() {
                $layer.classList.add('visible');
            }, 100);

            $layer.onclick = function() {
                // asign a function
                body.classList.remove('nav-open');
                this.mobile_menu_visible = 0;
                $layer.classList.remove('visible');
                setTimeout(function() {
                    $layer.remove();
                    $toggle.classList.remove('toggled');
                }, 400);
            }.bind(this);

            body.classList.add('nav-open');
            this.mobile_menu_visible = 1;
        }
    }

    getTitle() {
        let titlee: string = this.location.prepareExternalUrl(this.location.path());
        titlee = titlee.slice(1);
        for (let i = 0; i < this.listTitles.length; i++) {
            if (this.listTitles[i].type === 'link' && this.listTitles[i].path === titlee) {
                return this.listTitles[i].title;
            } else if (this.listTitles[i].type === 'sub') {
                for (let j = 0; j < this.listTitles[i].children.length; j++) {
                    const subtitle = this.listTitles[i].children[j].path;
                    if (subtitle === titlee) {
                        return this.listTitles[i].children[j].title;
                    }
                }
            }
        }
        return '';
    }

    getPath() {
        return this.location.prepareExternalUrl(this.location.path());
    }

    openNotification(state: boolean, $event) {
        $event.stopPropagation();
        if (state) {
            this.notificationReqBody = new NotificationRequestBody();
            const pagingSpec = new PagingSpec('id', 'asc', 0, this.pageSize);
            this.notificationReqBody.pagingSpec = pagingSpec;
            this.notificationReqBody.active = true;
            this.notificationReqBody.userId = this.user.userId;
            this.notificationReqBody.orgId = this.user.orgId;
            // this.notificationReqBody.userGroupId = +this.user.groups[0].id;
            this.notificationReqBody.method = NOTIFICATION_METHOD_PORTAL;
            const endDate = formatDate(new Date(new Date().getTime()), NOTIFICATION_DATE_FORMAT, this.locale);
            // start date is 365 (1year) back date
            const strtDate = formatDate(new Date(new Date().getTime() - 365 * 24 * 60 * 60 * 1000), NOTIFICATION_DATE_FORMAT, this.locale);
            this.notificationReqBody.startTime = strtDate;
            this.notificationReqBody.endTime = endDate;
            this.notificationService.getNotifications(this.notificationReqBody).subscribe(res => {
                this.notifications = res.body.content;
                if (this.notifications.length > 0) {
                    this.showNotification = state;
                }
                this.totalPages = res.body.totalPages;
                this.noOfNotifications = 0;
            });
        } else {
            this.showNotification = state;
            this.isFullListDisplayed = false;
        }
    }
    onScroll() {
        if (!this.isFullListDisplayed) {
            this.notificationReqBody.pagingSpec.page = this.notificationReqBody.pagingSpec.page + 1;
            this.notificationService.getNotifications(this.notificationReqBody).subscribe(res => {
                if (this.notifications.length > 0) {
                    this.appendItems(res.body.content);
                    if (this.notificationReqBody.pagingSpec.page + 1 === this.totalPages) {
                        this.isFullListDisplayed = true;
                    }
                } else {
                    this.notifications = res.body.content;
                }
                this.noOfNotifications = 0;
            });
        }
    }
    appendItems(content) {
        for (let i = 0; i < content.length; ++i) {
            this.notifications.push(content[i]);
        }
    }
    onClickOfNotification(notification: NotificationModel) {
        this.notificationService.markAsRead(notification.id).subscribe(res => {
            if (res.status === 200) {
                notification.markRead = true;
                // then redirect to respective action page depends on notification category
                if (notification.action === CERT_RENEW) {
                    const additionalInfo = JSON.parse(notification.additionalInfo);
                    const deviceUniqueID = additionalInfo.uniqueIdentifier;
                    this.notificationService.getDeviceByUniqueId(this.user.userId, deviceUniqueID).subscribe(deviceObj => {
                        if (deviceObj !== null) {
                            const device = deviceObj;
                            device.userName = this.user.username;
                            if (deviceObj.deviceType === DeviceTypeEnum.MOBILE) {
                                device.isMobileSelected = true;
                            } else {
                                device.isMobileSelected = false;
                            }
                            device.status = deviceObj.userDevices[0].status;
                            this.dataService.changeMIDObj(device);
                            this.router.navigate(['/identityDevices/Renew']);
                            this.showNotification = false;
                        }
                    });
                }
            }
        });
    }
    deleteNotification(notification: NotificationModel) {
        this.notificationService.makeInActive(notification.id).subscribe(res => {
            if (res.status === 200) {
                notification.active = false;
            }
        });
    }
    showSupportedi18NLanguages() {
        // alert("inside showSupportedi18NLanguages: "+this.translateService.currentLang);
        // this.translateService.addLangs(['en', 'fr']);
        // this.translateService.use('es');
        // const browserLang = translate.getBrowserLang();
        // translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }
    changeLanguage($event) {
        // this.translateService.addLangs(['en', 'fr']);
        this.selectedLanguage = $event.target.innerText;
        localStorage.setItem('selectedLanguage', $event.target.value);
        this.connectorService.setLocale($event.target.value);
        this.translateService.use($event.target.value);
        // alert('inside changeLanguage: ' + this.translateService.currentLang);
        this.validationByLanguageService.changeValidationObj($event.target.value);
    }

    ngOnDestroy() {
        if (!this.subscription.closed) {
            this.subscription.unsubscribe();
        }
    }
}
