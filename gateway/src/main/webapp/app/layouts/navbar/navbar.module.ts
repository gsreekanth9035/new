import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material/material.module';
import { GatewaySharedModule } from 'app/shared';
import { NavbarComponent } from './navbar.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [RouterModule, CommonModule, ReactiveFormsModule, MaterialModule, GatewaySharedModule, HttpClientModule, AngularSvgIconModule ],
    declarations: [NavbarComponent],
    exports: [NavbarComponent]
})
export class NavbarModule {}
