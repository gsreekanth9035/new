import { GatewaySharedModule } from 'app/shared';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { CommonModule, registerLocaleData } from '@angular/common';
import { MaterialModule } from '../../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { FlatpickrModule } from 'angularx-flatpickr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import localeEs from '@angular/common/locales/es';
import localeEn from '@angular/common/locales/en';
// import { DateAdapter } from '@angular/material';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { AgmCoreModule } from '@agm/core';

registerLocaleData(localeEs);
// registerLocaleData(localeFr);
registerLocaleData(localeEn);
@NgModule({
    imports: [
        CommonModule,
        GatewaySharedModule,
        MaterialModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        AngularSvgIconModule,
        HttpClientModule,
        FlatpickrModule.forRoot(),
        AgmCoreModule
    ],

    declarations: [LoginComponent],
    // schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports: [LoginComponent]
})
export class LoginModule {}
