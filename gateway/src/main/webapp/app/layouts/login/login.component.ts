import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { JhiLanguageHelper } from '../../core/language/language.helper';
import { HttpResponse } from '@angular/common/http';
import { PairMobileDeviceConfig } from 'app/pair-mobile-device/pair-mobile-device.service';
import { FormControl } from '@angular/forms';
import { ServerMockService } from 'app/services/server-mock.service';
import { WebAuthnService } from 'app/services/web-authn.service';
import { User } from 'app/interfaces/user';
import base64url from 'base64url';
import { ClientDataObj } from 'app/interfaces/client-data-obj';
import { DecodedAttestionObj } from 'app/interfaces/decoded-attestion-obj';
import * as CBOR from '../../utils/cbor';

@Component({
    selector: 'jhi-login',
    templateUrl: './login.component.html',
    styleUrls: ['login.scss']
})
export class LoginComponent implements OnInit {
    organizationBrandingInfo = new OrganizationBrandingInfo();
    pairMobileDeviceConfig = new PairMobileDeviceConfig();

    loginlogo: string;
    logoName: any;
    logoStyle = {
        width: '',
        height: ''
    };
    qrcodedisplay = false;
    emessage = '';
    version: string;

    selectedLanguage = 'English';
    showCalendar = false;
    orgId;
    //
    title = 'angular-web-authn';
    users: User[];
    email = 'a@a.com';
    password = 'aaa';
    useFingerprint = true;
    webAuthnAvailable = !!navigator.credentials && !!navigator.credentials.create;
    // webAuthnAvailable = true;
    webAuthnModaldisplay = false;
    // webAuthnPolicy = {
    //     val1: 'asdf',
    //     value2: 2
    // };
    webAuthnAuthenticatorRegistration: WebAuthnAuthenticatorRegistration;
    webAuthnPolicyDetails: WebAuthnPolicyDetails;
    showScheduler = false;
    emailInput = new FormControl();
    welcomeNote2_key: string;
    poweredBy: string;
    constructor(
        private loginService: LoginService,
        private translateService: TranslateService,
        private jhiLanguageHelper: JhiLanguageHelper,
        private serverMockService: ServerMockService,
        private webAuthnService: WebAuthnService
    ) {
        this.users = serverMockService.getUsers();
    }

    ngOnInit() {
        this.selectedLanguage = localStorage.getItem('selectedLanguage');
        if (this.selectedLanguage === 'es') {
            this.selectedLanguage = 'Spanish';
        }
        if (this.selectedLanguage === 'en') {
            this.selectedLanguage = 'English';
        }
        this.loginService.loadBrandingInformation().subscribe(
            brandingInformation => {
                this.loginService.changeBrandingInfo(brandingInformation);
                this.organizationBrandingInfo = brandingInformation;
                this.logoStyle = {
                    width: this.organizationBrandingInfo.loginLogoWidth,
                    height: this.organizationBrandingInfo.loginLogoHeight
                };
                this.welcomeNote2_key = brandingInformation.loginPageTitle.trim();
                this.poweredBy = brandingInformation.poweredBy.trim();
                this.orgId = brandingInformation.orgId;
                this.showScheduler = brandingInformation.enableSchedulerInLoginScreen;

                // alert(this.organizationBrandingInfo.loginLogo);
            },
            err => {
                // alert('error');
            }
        );
        this.loginService.getPairMobileServiceQRCode().subscribe(
            pairMSConfig => {
                this.pairMobileDeviceConfig = pairMSConfig;
                if (
                    this.pairMobileDeviceConfig.qrCode === undefined ||
                    this.pairMobileDeviceConfig.qrCode === null ||
                    this.pairMobileDeviceConfig.qrCode === ''
                ) {
                    this.pairMobileDeviceConfig.qrCode = null;
                } else {
                    this.pairMobileDeviceConfig.qrCode = 'data:image/png;base64,' + this.pairMobileDeviceConfig.qrCode;
                }
            },
            err => {
                // alert('error');
            }
        );

        this.loginService.getVersionAndBuildInfo().subscribe(
            (res: HttpResponse<BuildInfo>) => {
                console.log(res);
                const buildInfo = res.body;
                this.version = buildInfo.version.split('-')[0] + '.' + (buildInfo.buildNumber != null ? buildInfo.buildNumber : '' + 0);
                console.log(buildInfo.version);
                console.log(buildInfo.buildNumber);
                console.log(buildInfo.commitId);
                console.log(this.version);
                if (this.version === undefined || this.version === null) {
                    this.version = '2.0.10';
                }
                console.log(this.version);
            },
            err => {
                console.log(err);
            }
        );
    }
    cancel() {
        this.emailInput.reset();
        this.emessage = '';
    }
    changeLanguage($event) {
        // alert('inside changeLanguage: ' + $event.target.value);
        // this.translateService.addLangs(['en', 'fr']);
        // this.jhiLanguageHelper.updateLanguage();
        console.log($event);
        this.selectedLanguage = $event.target.innerText;
        localStorage.setItem('selectedLanguage', $event.target.value);
        this.translateService.use($event.target.value);
        // alert ( this.translateService.currentLang );
    }
    getEnrollmentQRCode() {
        this.loginService.getEnrollmentQRCode((<HTMLInputElement>document.getElementById('emailid')).value).subscribe(
            pariConfigForEnrollment => {
                if (pariConfigForEnrollment === null || pariConfigForEnrollment === undefined) {
                    this.emessage = 'userNotFound';
                    this.qrcodedisplay = false;
                } else {
                    this.pairMobileDeviceConfig = pariConfigForEnrollment;

                    if (
                        this.pairMobileDeviceConfig.qrCode === undefined ||
                        this.pairMobileDeviceConfig.qrCode === null ||
                        this.pairMobileDeviceConfig.qrCode === ''
                    ) {
                        if (this.pairMobileDeviceConfig.status === false) {
                            this.pairMobileDeviceConfig.qrCode = null;
                            this.emessage = 'userEnrollmentNotAllowed';
                            this.qrcodedisplay = false;
                        }
                    } else {
                        this.pairMobileDeviceConfig.qrCode = 'data:image/png;base64,' + this.pairMobileDeviceConfig.qrCode;
                        this.emessage = 'scanToBeginEnrollment';
                        this.qrcodedisplay = true;
                    }
                }
            },
            err => {
                this.pairMobileDeviceConfig.qrCode = null;
                this.emessage = 'somethingWentWrong';
                this.qrcodedisplay = false;
            }
        );
    }

    loginWithPWD() {
        this.loginService.login();
    }

    loginWithPKI() {}

    showQRCode() {
        this.qrcodedisplay = true;
    }
    hideQRCode() {
        this.qrcodedisplay = false;
    }
    onClickOfSchedule() {
        this.showCalendar = true;
        this.webAuthnModaldisplay = false;
    }

    goBack($event) {
        this.showCalendar = false;
    }

    showWebAuthn() {
        // this.hello();
        if (this.webAuthnModaldisplay) {
            this.webAuthnModaldisplay = false;
        } else {
            this.webAuthnModaldisplay = true;
        }
    }

    removeUser(email: string) {
        this.serverMockService.removeUser(email);
        this.users = this.serverMockService.getUsers();
    }

    signup() {
        console.log('SIGNUP');
        // Save into the 'DB'
        const prevUser = this.serverMockService.getUser(this.email);
        if (prevUser) {
            alert('🚫 User already exists with this email address');
            return;
        }
        const user: User = this.serverMockService.addUser({ email: this.email, password: this.password, credentials: [] });
        this.users = this.serverMockService.getUsers();

        this.webAuthnService.getWebAuthnPolicyDetails(this.email).subscribe(
            response => {
                this.webAuthnPolicyDetails = response;
                if (this.webAuthnPolicyDetails) {
                    // Ask for WebAuthn Auth method
                    if (this.webAuthnAvailable && this.useFingerprint) {
                        this.webAuthnService
                            .webAuthnSignup(user, this.webAuthnPolicyDetails)
                            .then((credential: PublicKeyCredential) => {
                                console.log('credentials.create RESPONSE', credential.response);
                                let webAuthnAuthenticatorRegistration: WebAuthnAuthenticatorRegistration;
                                webAuthnAuthenticatorRegistration.clientDataJSON = base64url.encode(
                                    this.extractClientDataJSONString(credential)
                                );
                                console.log(webAuthnAuthenticatorRegistration.clientDataJSON);
                                webAuthnAuthenticatorRegistration.attestationObject = base64url.encode(
                                    Buffer.from(this.extractAuthData(credential).buffer)
                                );
                                console.log(webAuthnAuthenticatorRegistration.attestationObject);
                                webAuthnAuthenticatorRegistration.publicKeyCredentialId = base64url.encode(Buffer.from(credential.rawId));

                                console.log(webAuthnAuthenticatorRegistration.publicKeyCredentialId);
                                let initLabel = 'WebAuthn Authenticator (Default Label)';
                                // let labelResult = window.prompt("Please input your registered authenticator's label", initLabel);
                                // if (labelResult === null) labelResult = initLabel;
                                webAuthnAuthenticatorRegistration.authenticatorLabel = initLabel;
                                webAuthnAuthenticatorRegistration.uuid = this.webAuthnPolicyDetails.uuid;
                                console.log(webAuthnAuthenticatorRegistration.uuid);
                                // console.log('credentialId', webAuthnAuthenticatorRegistration_.);
                                const authData = this.extractAuthData(credential);
                                const credentialIdLength = this.getCredentialIdLength(authData);
                                const credentialId: Uint8Array = authData.slice(55, 55 + credentialIdLength);
                                console.log('credentialIdLength', credentialIdLength);
                                console.log('credentialId', credentialId);
                                const publicKeyBytes: Uint8Array = authData.slice(55 + credentialIdLength);
                                const publicKeyObject = CBOR.decode(publicKeyBytes.buffer);
                                console.log('publicKeyObject', publicKeyObject);
                                this.webAuthnService.registerWebAuthnAuthenticator(this.email, webAuthnAuthenticatorRegistration).subscribe(
                                    response1 => {
                                        const webAuthnAuthenticatorRegistration__ = response1;
                                        if (webAuthnAuthenticatorRegistration__) {
                                            console.log('webAuthnAuthenticatorRegistration_ id:', webAuthnAuthenticatorRegistration__.id);
                                            return true;
                                        } else {
                                            console.log('credentials.create ERRORRRRRR 1');
                                            return null;
                                        }
                                    },
                                    err => {
                                        console.log('credentials.create ERRORRRRRR 2', err);
                                        return null;
                                    }
                                );
                                // this.users = this.serverMockService.getUsers();
                            })
                            .catch(error => {
                                console.log('credentials.create ERRORRRRRR 3', error);
                            });
                    }
                } else {
                    return null;
                }
            },
            err => {
                console.log('credentials.create ERRORRRRRR 4', err);
                return null;
            }
        );
    }

    getCredentialIdLength(authData: Uint8Array): number {
        // get the length of the credential ID
        const dataView = new DataView(new ArrayBuffer(2));
        const idLenBytes = authData.slice(53, 55);
        idLenBytes.forEach((value, index) => dataView.setUint8(index, value));
        return dataView.getUint16(0);
    }
    extractClientDataJSONString(credential: PublicKeyCredential): string {
        // decode the clientDataJSON into a utf-8 string
        const utf8Decoder = new (window as any).TextDecoder('utf-8');
        const decodedClientData = utf8Decoder.decode(credential.response.clientDataJSON); // String
        return decodedClientData;
    }
    extractClientData(credential: PublicKeyCredential): ClientDataObj {
        // decode the clientDataJSON into a utf-8 string
        const utf8Decoder = new (window as any).TextDecoder('utf-8');
        const decodedClientData = utf8Decoder.decode(credential.response.clientDataJSON); // String
        const clientDataObj: ClientDataObj = JSON.parse(decodedClientData);
        console.log('clientDataObj', clientDataObj);
        return clientDataObj;
    }
    extractAuthData(credential: PublicKeyCredential): Uint8Array {
        // decode the clientDataJSON into a utf-8 string
        const utf8Decoder = new (window as any).TextDecoder('utf-8');
        const decodedClientData = utf8Decoder.decode(credential.response.clientDataJSON);

        const clientDataObj: ClientDataObj = JSON.parse(decodedClientData);
        console.log('clientDataObj', clientDataObj);

        const decodedAttestationObj: DecodedAttestionObj = CBOR.decode((credential.response as any).attestationObject);
        console.log('decodedAttestationObj', decodedAttestationObj);

        const { authData } = decodedAttestationObj;
        console.log('authData', authData);

        return authData;
    }

    signin() {
        console.log('[signin]');
        const user = this.serverMockService.getUsers().find(u => u.email === this.email && u.password === this.password);
        if (user) {
            alert('✅ Congrats! Authentication went fine!');
        } else {
            alert('🚫 Sorry :( Invalid credentials!');
        }
    }

    webAuthSignin() {
        const user = this.serverMockService.getUser(this.email);
        this.webAuthnService
            .webAuthnSignin(user)
            .then(response => {
                // TODO: validate attestion
                alert('✅ Congrats! Authentication went fine!');
                console.log('SUCCESSFULLY GOT AN ASSERTION!', response);
            })
            .catch(error => {
                alert('🚫 Sorry :( Invalid credentials!');
                console.log('FAIL', error);
            });
    }

    hello() {
        alert('Hello!!!');
        alert(navigator.credentials);
        alert(navigator.credentials.create);
        alert('-----' + this.webAuthnAvailable);
        alert(base64url.encode('ladies and gentlemen, we are floating in space', 'utf8'));
        alert(base64url.decode(base64url.encode('ladies and gentlemen, we are floating in space', 'utf8')));
    }
}

export class OrganizationBrandingInfo {
    name: string;
    description: string;
    displayName: string;
    altName: string;
    loginLogo: string;
    loginLogoWidth: string;
    loginLogoHeight: string;
    sidebarLogo: string;
    sidebarLogoWidth: string;
    sidebarLogoHeight: string;
    primaryColorCode: string;
    secondaryColorCode: string;
    tertiaryColorCode: string;
    quaternaryColorCode: string;
    quinaryColorCode: string;
    privacyPolicy: string;
    hideServiceName: boolean;
    favIcon: string;
    poweredBy = '';
    loginPageTitle = '';
    mobileAppName: string;
    mobileServicesAppName: string;
    orgId: number;
    enableSchedulerInLoginScreen: boolean;
    contactUsSupportEmail: string;
    constructor() {}
}

export class BuildInfo {
    artifactId: string;
    version: string;
    buildNumber: string;
    commitId: string;
    branch: string;
    prId: string;
    prDestinationBranch: string;
}

export class WebAuthnPolicyDetails {
    challengeValue: string;
    userId: string;
    username: string;
    rpEntityName: string;
    signatureAlgorithms: string;
    rpId: string;
    attestationConveyancePreference: string;
    authenticatorAttachment: string;
    requireResidentKey: string;
    userVerificationRequirement: string;
    createTimeout: number;
    excludeCredentialIds: string;
    avoidSameAuthenticatorRegister: boolean;
    isSetRetry: string;
    uuid: string;
}

export class WebAuthnAuthenticatorRegistration {
    id: string;
    clientDataJSON: string;
    attestationObject: string;
    publicKeyCredentialId: string;
    authenticatorLabel: string;
    error: string;
    uuid: string;
}
