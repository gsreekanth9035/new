import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { Router, ActivatedRouteSnapshot, NavigationEnd } from '@angular/router';

import { JhiLanguageService } from 'ng-jhipster';
import { AccountService, JhiLanguageHelper, LoginService, Principal, UserInfo } from 'app/core';
import { LANGUAGES } from 'app/core/language/language.constants';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { MENU_ITEMS } from '../sidebar/sidebar.component';
import { AuthExpiredInterceptor } from 'app/blocks/interceptor/auth-expired.interceptor';
import { ConnectorService } from 'app/connector.service';

@Component({
    selector: 'jhi-main',
    templateUrl: './main.component.html'
})
export class JhiMainComponent implements OnInit {
    user: UserInfo;
    location: Location;
    private listTitles: any[];
    timeStart = false;
    // seconds = 15;
    // clientX = 0;
    // clientY = 0;

    refreshTokenNow = '<span></span>';
    favIcon: HTMLLinkElement = document.querySelector('#favIcon');

    constructor(
        location: Location,
        private jhiLanguageHelper: JhiLanguageHelper,
        private jhiLanguageService: JhiLanguageService,
        private router: Router,
        private principal: Principal,
        private loginService: LoginService,
        private accountService: AccountService,
        private connectorService: ConnectorService
    ) {
        this.location = location;
        this.loginService.loadBrandingInformation().subscribe(res => {
            this.loginService.changeBrandingInfo(res);
            this.favIcon.setAttribute('href', res.favIcon);
        });
    }

    ngOnInit() {
        const browserLanguage = navigator.language;
        if (LANGUAGES.includes(browserLanguage)) {
            this.jhiLanguageService.changeLanguage(browserLanguage);
        }

        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.jhiLanguageHelper.updateTitle(this.getPageTitle(this.router.routerState.snapshot.root));
            }
        });

        this.principal.identity().then(user => {
            this.user = user;
        });

        this.listTitles = MENU_ITEMS.filter(listTitle => listTitle);

        // Start watching for user inactivity.
        // this.userIdle.startWatching();
        // // Start watching when user idle is starting.
        // this.userIdle.onTimerStart().subscribe(count => { this.seconds = this.seconds - 1; this.timeStart = true; /* console.log(count) */ });
        // // Start watch when time is up.
        // this.userIdle.onTimeout().subscribe(() => alert('Time is up!'));

        this.connectorService.errorEvent.subscribe(result => {
            if (result === true) {
                // alert('get result of event as boolean' + result);
                const logingURI = this.loginService.getLoginURI();
                this.refreshTokenNow = '<iframe src="' + logingURI + '" frameborder="0"></iframe>';
                setTimeout(() => {
                    this.connectorService.showError(false);
                }, 1000 * 60 * 0.3);
            } else if (result === false) {
                // this.connectorService.reLoadUserToken(false);
                // this.principal.reIdentity(true).then(user => {
                //     this.user = user;
                // });
                // console.log('show error false');
                this.refreshTokenNow = '<span></span>';
                this.accountService.refreshTokenNow();
            }
        });

        // this.authExpiredInterceptor.dataStream$().subscribe(data => {
        //     // This will be triggered every time data is added to the stream in your HttpInterceptorService class.
        //     // Call your custom function here...
        //     alert(data);
        //     const logingURI = this.loginService.getLoginURI();
        //     this.refreshTokenNow = '<iframe src="' + logingURI + '" frameborder="0"></iframe>';
        // });
    }

    // stop() {
    //     alert('timer is stopped');
    //     this.userIdle.stopTimer();
    //     this.seconds = 15;
    //     this.timeStart = false;
    // }
    // stopWatching() {
    //     this.userIdle.stopWatching();
    // }

    // startWatching() {
    //     this.userIdle.startWatching();
    // }

    // restart() {
    //     this.userIdle.resetTimer();
    // }

    // coordinates(event: MouseEvent): void {
    //     this.clientX = event.clientX;
    //     this.clientY = event.clientY;
    //     console.log(this.clientX, this.clientY);
    // }

    // @HostListener('mousemove') onMove() {
    // window.alert("mouse is moved");
    // }

    // @HostListener('keypress') onKeyPress() {
    //     window.alert("key is pressed");
    //     this.stop();
    // }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
        let title: string = routeSnapshot.data && routeSnapshot.data['pageTitle'] ? routeSnapshot.data['pageTitle'] : 'gatewayApp';
        if (routeSnapshot.firstChild) {
            title = this.getPageTitle(routeSnapshot.firstChild) || title;
        }
        return title;
    }

    getTitle() {
        let titlee: string = this.location.prepareExternalUrl(this.location.path());
        titlee = titlee.slice(1);
        for (let i = 0; i < this.listTitles.length; i++) {
            if (this.listTitles[i].type === 'link' && this.listTitles[i].path === titlee) {
                return this.listTitles[i].title;
            } else if (this.listTitles[i].type === 'sub') {
                for (let j = 0; j < this.listTitles[i].children.length; j++) {
                    const subtitle = this.listTitles[i].children[j].path;
                    if (subtitle === titlee) {
                        return this.listTitles[i].children[j].title;
                    }
                }
            }
        }
        return '';
    }
    getPath() {
        return this.location.prepareExternalUrl(this.location.path());
    }
}
