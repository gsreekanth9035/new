import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Principal } from 'app/core';
import { PagingSpec } from 'app/monitor/monitor.service';
import { Page } from 'app/visual-design/page.model';
import { Device } from 'app/manage-identity-devices/manage-identity-devices.model';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {
    url: string;
    loggedInUser;
    loggedInUserId;
    constructor(private httpClient: HttpClient, private principal: Principal) {
        this.url = `${window.location.origin}/notificationservice`;
        this.loggedInUser = this.principal.getLoggedInUser().username;
        this.loggedInUserId = this.principal.getLoggedInUser().userId.toString();
    }
    getNotificationCount(notificationReq: NotificationRequestBody): Observable<any> {
        if (this.principal.getLoggedInUser() !== null) {
            const httpHeaders = new HttpHeaders({
                'Content-type': 'application/json',
                organizationId: this.principal.getLoggedInUser().orgId.toString(),
                user: this.loggedInUser,
                userId: this.loggedInUserId,
                version: '1'
                // 'Authorization': auth,
            });
            return this.httpClient.post(`${this.url}/notification/audit/count`, notificationReq, {
                headers: httpHeaders,
                observe: 'response'
            });
        } else {
            return new Observable();
        }
    }
    getNotifications(notificationReq: NotificationRequestBody): Observable<HttpResponse<Page>> {
        const httpHeaders = new HttpHeaders({
            'Content-type': 'application/json',
            organizationId: this.principal.getLoggedInUser().orgId.toString(),
            user: this.loggedInUser,
            userId: this.loggedInUserId,
            version: '1'
            // 'Authorization': auth,
        });
        return this.httpClient.post<Page>(`${this.url}/notification/audit`, notificationReq, { headers: httpHeaders, observe: 'response' });
    }
    markAsRead(notificationId: number) {
        const httpHeaders = new HttpHeaders({
            'Content-type': 'application/json',
            organizationId: this.principal.getLoggedInUser().orgId.toString(),
            user: this.loggedInUser,
            userId: this.loggedInUserId,
            version: '1'
            // 'Authorization': auth,
        });
        return this.httpClient.put(`${this.url}/notification/audit/markRead/${notificationId}`, '', {
            headers: httpHeaders,
            observe: 'response'
        });
    }
    makeInActive(notificationId: number) {
        const httpHeaders = new HttpHeaders({
            'Content-type': 'application/json',
            organizationId: this.principal.getLoggedInUser().orgId.toString(),
            user: this.loggedInUser,
            userId: this.loggedInUserId,
            version: '1'
            // 'Authorization': auth,
        });
        return this.httpClient.delete(`${this.url}/notification/audit/delete/${notificationId}`, {
            headers: httpHeaders,
            observe: 'response'
        });
    }
    getDeviceByUniqueId(userID, uniqueIdentifier): Observable<Device> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<Device>(
            `${window.location.origin}/apdu/api/v1/users/${userID}/devices/${uniqueIdentifier}/deviceByUniqueId?organizationName=${orgName}`
        );
    }
}
export class NotificationRequestBody {
    orgId: number;
    userId: number;
    userGroupId: number;
    active: boolean;
    method: string;
    startTime;
    endTime;
    pagingSpec: PagingSpec;
}
export class NotificationModel {
    id: number;
    action: string;
    active: boolean;
    additionalInfo: any;
    createdBy: string;
    createdDate;
    description: string;
    name: string;
    sentStatus: string;
    sentStatusMessage: string;
    markRead: boolean;
}
