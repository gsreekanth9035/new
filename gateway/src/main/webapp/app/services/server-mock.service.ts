import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';
import { UserService } from './user.service';

@Injectable({
    providedIn: 'root'
})
export class ServerMockService {
    constructor(private userService: UserService) {}

    // Validate and Store credential
    // registerCredential(user: User, credential: PublicKeyCredential): boolean {
    //     const authData = this.extractAuthData(credential);
    //     const credentialIdLength = this.getCredentialIdLength(authData);
    //     const credentialId: Uint8Array = authData.slice(55, 55 + credentialIdLength);
    //     console.log('credentialIdLength', credentialIdLength);
    //     console.log('credentialId', credentialId);
    //     const publicKeyBytes: Uint8Array = authData.slice(55 + credentialIdLength);
    //     const publicKeyObject = CBOR.decode(publicKeyBytes.buffer);
    //     console.log('publicKeyObject', publicKeyObject);

    //     const valid = true;

    //     if (valid) {
    //         user.credentials.push({ credentialId, publicKey: publicKeyBytes });
    //         this.updateUser(user);
    //     }

    //     return valid;
    // }

    getUsers() {
        return this.userService.getUsers();
    }

    updateUser(user: User) {
        this.removeUser(user.email);
        this.addUser(user);
    }

    addUser(user: User) {
        user.id = '' + Math.floor(Math.random() * 10000000);
        this.userService.addUser(user);
        return user;
    }

    getUser(email: string) {
        return this.userService.getUser(email);
    }

    removeUser(email: string) {
        return this.userService.removeUser(email);
    }

    getChallenge() {
        // return Uint8Array.from('someChallengeIsHereComOn', c => c.charCodeAt(0));
        return new (window as any).TextEncoder().encode('someChallengeIsHereComOn');
    }

    // encodeChallenge(challengeValue: string) {
    //     // return Uint8Array.from('someChallengeIsHereComOn', c => c.charCodeAt(0));
    //     return new (window as any).TextEncoder().encode(challengeValue);
    // }
}
