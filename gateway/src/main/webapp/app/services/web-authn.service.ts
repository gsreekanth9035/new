import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';
import { ServerMockService } from './server-mock.service';
import { WebAuthnAuthenticatorRegistration, WebAuthnPolicyDetails } from 'app/layouts/login/login.component';
import base64url from 'base64url';

@Injectable({
    providedIn: 'root'
})
export class WebAuthnService {
    domain: string[];
    baseUrl: string;
    headeroption = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    constructor(private http: HttpClient, private serverMockService: ServerMockService) {
        this.domain = location.hostname.split('.', 1);
        this.baseUrl = window.location.origin + '/usermanagement/api/v1/organizations/' + this.domain[0] + '/idp-users/';
    }

    getWebAuthnPolicyDetails(username: string): Observable<WebAuthnPolicyDetails> {
        // alert('get');
        // /api/v1/organizations/{organizationName}/webauthn/policy-details/users/{userName}
        // return this.http.get<WebAuthnPolicyDetails>(
        //     SERVER_API_URL + 'api/v1/organizations/' + this.domain[0] + '/webauthn/policy-details/users/' + username
        return this.http.get<WebAuthnPolicyDetails>(this.baseUrl + username + '/webauthn-policy-details');
    }

    registerWebAuthnAuthenticator(
        username: string,
        webAuthnAuthenticatorRegistration: WebAuthnAuthenticatorRegistration
    ): Observable<WebAuthnAuthenticatorRegistration> {
        // alert('get');
        // /api/v1/organizations/{organizationName}/webauthn/policy-details/users/{userName}
        // return this.http.post<WebAuthnAuthenticatorRegistration>(
        //     SERVER_API_URL + 'api/v1/organizations/' + this.domain[0] + '/webauthn/register-authenticator/users/' + username,
        //     webAuthnAuthenticatorRegistration,
        //     this.headeroption
        // );
        return this.http.post<WebAuthnAuthenticatorRegistration>(
            this.baseUrl + username + '/webauthn-register-authenticator',
            webAuthnAuthenticatorRegistration,
            this.headeroption
        );
    }

    webAuthnSignup(user: User, webAuthnPolicyDetails: WebAuthnPolicyDetails): Promise<Credential> {
        // var uint8array = new TextEncoder().encode(string);
        // var string = new TextDecoder(encoding).decode(uint8array);

        console.log('[webAuthnSignup]');
        console.log(webAuthnPolicyDetails);
        const pubKeyCredParams = this.getPubKeyCredParams(webAuthnPolicyDetails.signatureAlgorithms);
        console.log(pubKeyCredParams);
        console.log(webAuthnPolicyDetails.userId);
        console.log(new (window as any).TextEncoder().encode(webAuthnPolicyDetails.challengeValue));
        console.log(base64url.toBuffer(new (window as any).TextEncoder().encode(webAuthnPolicyDetails.challengeValue)));
        console.log(new (window as any).TextEncoder().encode(webAuthnPolicyDetails.userId));
        let authenticatorSelection: AuthenticatorSelectionCriteria;
        let isAuthenticatorSelectionSpecified = false;

        let publicKeyCredentialCreationOptions: PublicKeyCredentialCreationOptions = {
            // Challenge shoulda come from the server
            // challenge: base64url.toBuffer(this.serverMockService.encodeChallenge(webAuthnPolicyDetails.challengeValue)),
            challenge: base64url.toBuffer(new (window as any).TextEncoder().encode(webAuthnPolicyDetails.challengeValue)),
            rp: {
                name: webAuthnPolicyDetails.rpEntityName
            },
            user: {
                // Some user id coming from the server
                // id: Uint8Array.from(user.id, c => c.charCodeAt(0)),
                id: new (window as any).TextEncoder().encode(webAuthnPolicyDetails.userId),
                displayName: webAuthnPolicyDetails.username,
                name: webAuthnPolicyDetails.username
            },
            // pubKeyCredParams: [{ alg: -7, type: 'public-key' }],
            pubKeyCredParams: pubKeyCredParams
            // authenticatorSelection: {
            //     authenticatorAttachment: 'platform'
            //     // requireResidentKey: true,
            // },
            // timeout: 60000,
            // attestation: 'none',
        };

        if (webAuthnPolicyDetails.rpId !== '' && webAuthnPolicyDetails.rpId !== null && webAuthnPolicyDetails.rpId !== 'not specified') {
            publicKeyCredentialCreationOptions.rp.id = webAuthnPolicyDetails.rpId;
        }

        // attestation
        if (webAuthnPolicyDetails.attestationConveyancePreference !== 'not specified') {
            switch (webAuthnPolicyDetails.attestationConveyancePreference) {
                case 'none':
                    publicKeyCredentialCreationOptions.attestation = 'none';
                    break;
                case 'indirect':
                    publicKeyCredentialCreationOptions.attestation = 'indirect';
                    break;
                case 'direct':
                    publicKeyCredentialCreationOptions.attestation = 'direct';
                    break;
            }
        }

        // authenticatorSelection
        if (webAuthnPolicyDetails.authenticatorAttachment !== 'not specified') {
            switch (webAuthnPolicyDetails.authenticatorAttachment) {
                case 'none':
                    authenticatorSelection.authenticatorAttachment = 'platform';
                    break;
                case 'indirect':
                    authenticatorSelection.authenticatorAttachment = 'cross-platform';
                    break;
            }
            isAuthenticatorSelectionSpecified = true;
        }
        if (webAuthnPolicyDetails.requireResidentKey !== 'not specified') {
            if (webAuthnPolicyDetails.requireResidentKey === 'Yes') {
                authenticatorSelection.requireResidentKey = true;
            } else {
                authenticatorSelection.requireResidentKey = false;
            }
            isAuthenticatorSelectionSpecified = true;
        }
        if (webAuthnPolicyDetails.userVerificationRequirement !== 'not specified') {
            switch (webAuthnPolicyDetails.attestationConveyancePreference) {
                case 'none':
                    authenticatorSelection.userVerification = 'discouraged';
                    break;
                case 'indirect':
                    authenticatorSelection.userVerification = 'preferred';
                    break;
                case 'direct':
                    authenticatorSelection.userVerification = 'required';
                    break;
            }
            isAuthenticatorSelectionSpecified = true;
        }
        if (isAuthenticatorSelectionSpecified) {
            publicKeyCredentialCreationOptions.authenticatorSelection = authenticatorSelection;
        }

        // timeout
        if (webAuthnPolicyDetails.createTimeout !== 0) {
            publicKeyCredentialCreationOptions.timeout = webAuthnPolicyDetails.createTimeout * 1000;
        }

        // excludeCredentialIds
        let excludeCredentials: PublicKeyCredentialDescriptor[] = this.getExcludeCredentials(webAuthnPolicyDetails.excludeCredentialIds);
        console.log(excludeCredentials);
        if (excludeCredentials.length > 0) {
            publicKeyCredentialCreationOptions.excludeCredentials = excludeCredentials;
        }
        console.log(publicKeyCredentialCreationOptions);

        return navigator.credentials.create({ publicKey: publicKeyCredentialCreationOptions });
    }

    getPubKeyCredParams(signatureAlgorithms: string) {
        let pubKeyCredParams: PublicKeyCredentialParameters[] = [];
        if (signatureAlgorithms === '') {
            pubKeyCredParams.push({ type: 'public-key', alg: -7 });
            return pubKeyCredParams;
        }
        let signatureAlgorithmsList = signatureAlgorithms.split(',');

        for (let i = 0; i < signatureAlgorithmsList.length; i++) {
            pubKeyCredParams.push({
                type: 'public-key',
                alg: Number(signatureAlgorithmsList[i])
            });
        }
        return pubKeyCredParams;
    }
    getExcludeCredentials(excludeCredentialIds: string) {
        let excludeCredentials: PublicKeyCredentialDescriptor[] = [];
        if (excludeCredentialIds === '') {
            return excludeCredentials;
        }

        let excludeCredentialIdsList = excludeCredentialIds.split(',');

        for (let i = 0; i < excludeCredentialIdsList.length; i++) {
            excludeCredentials.push({
                type: 'public-key',
                id: base64url.toBuffer(excludeCredentialIdsList[i])
            });
        }
        return excludeCredentials;
    }

    webAuthnSignin(user: User): Promise<Credential> {
        const allowCredentials: Array<PublicKeyCredentialDescriptor> = [];
        user.credentials.forEach(credential => {
            allowCredentials.push({
                type: 'public-key',
                id: credential.credentialId
            });
        });

        // const allowCredentials: PublicKeyCredentialDescriptor[] = user.credentials.map(c => {
        //   console.log(c.credentialId);
        //   return { type: 'public-key', id: c.credentialId, };
        // });

        console.log('allowCredentials', allowCredentials);

        const credentialRequestOptions: PublicKeyCredentialRequestOptions = {
            challenge: this.serverMockService.getChallenge(),
            allowCredentials
        };

        return navigator.credentials.get({
            publicKey: credentialRequestOptions
        });
    }
}
