import { Principal } from 'app/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Page } from 'app/visual-design/page.model';

@Injectable({
    providedIn: 'root'
})
export class PendingApprovalStatService {
    uri: string;
    constructor(private httpClient: HttpClient, private principal: Principal) {
        const orgName = this.principal.getLoggedInUser().organization;
        this.uri = `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users`;
    }

    // Call REST APIs
    pendingApprovalStats(groupID: string, sort: string, order: string, page: number, size: number, startDate, endDate): Observable<Page> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<Page>(
            `${
                this.uri
            }/groups/${groupID}/pendingApprovalStats?sort=${sort}&order=${order}&page=${page}&size=${size}&startDate=${startDate}&endDate=${endDate}`
        );
    }
}

export class UserEntity {
    firstName: string;
    createdDate: string;
    createdBy: string;
    username: string;
    isFederated: boolean;
    groupId: string;
    groupName: string;
    constructor() {}
}
