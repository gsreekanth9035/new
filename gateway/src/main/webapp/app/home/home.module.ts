import { MaterialModule } from './../material/material.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GatewaySharedModule } from 'app/shared';
import {  HomeComponent, HOME_ROUTES } from './';
// import { HOME_ROUTE, HomeComponent, HOME_ROUTES } from './';
import { IssuedDevicesComponent } from './issued-devices/issued-devices.component';
import { PendingEnrollmentComponent } from './pending-enrollment/pending-enrollment.component';
import { PendingIssuanceComponent } from './pending-issuance/pending-issuance.component';
import { PendingRequestComponent } from './pending-request/pending-request.component';
import { StatsComponent } from './stats/stats.component';
import { SubscriptionComponent } from './subscription/subscription.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    // imports: [GatewaySharedModule, RouterModule.forChild([HOME_ROUTE]), MaterialModule, MatProgressSpinnerModule, AngularSvgIconModule, HttpClientModule],
    imports: [GatewaySharedModule, RouterModule.forChild([...HOME_ROUTES]), MaterialModule, MatProgressSpinnerModule, AngularSvgIconModule, HttpClientModule, FormsModule,
     ReactiveFormsModule],
    declarations: [HomeComponent, IssuedDevicesComponent, PendingEnrollmentComponent, PendingIssuanceComponent, PendingRequestComponent, StatsComponent, SubscriptionComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayHomeModule {}
