import { Route, Routes } from '@angular/router';

import { HomeComponent } from './';

export const HOME_ROUTES: Routes = [
    {
        path: 'dashboard',
        component: HomeComponent,
        data: {
            authorities: []
            // ,
            // pageTitle: 'home.title'
        },
        runGuardsAndResolvers: 'always'
    }
];
