import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { LoginService, Principal } from 'app/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd } from '@angular/router';
import { OrganizationBrandingInfo } from 'app/layouts/login/login.component';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
    navigationSubscription;
    isOperator: boolean;
    isAdmin: boolean;
    hasApplicantRole: boolean;
    primaryRole: string;
    secondaryRole: string;
    isApplicantNow: boolean;
    noRoleAssigned: boolean;
    initialiseInvitesIsCalled = false;
    organizationBrandingInfo: OrganizationBrandingInfo;

    constructor(
        private translate: TranslateService,
        private loginService: LoginService,
        private principal: Principal,
        private router: Router,
        private titleService: Title
    ) {
        this.loginService.loadBrandingInformation().subscribe(
            brandingInformation => {
                this.organizationBrandingInfo = brandingInformation;
                translate.get('home.title').subscribe((text: string) => {
                    this.titleService.setTitle(this.organizationBrandingInfo.displayName);
                    // alert(text + this.logoName);
                });
            },
            err => {
                // alert('error');
            }
        );

        // subscribe to the router events - storing the subscription so
        // we can unsubscribe later.
        this.navigationSubscription = this.router.events.subscribe((e: any) => {
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof NavigationEnd) {
                this.initialiseInvites();
            }
        });

        if (this.isOperator === true || this.isAdmin === true) {
            this.principal.getLoggedInUser().authorities = this.principal.getLoggedInUser().roleBasedAuthorities[this.primaryRole];
        } else {
            this.principal.getLoggedInUser().authorities = this.principal.getLoggedInUser().roleBasedAuthorities[this.secondaryRole];
        }
    }

    initialiseInvites() {
        // Set default values and re-fetch any data you need.
        this.isApplicantNow = this.principal.getLoggedInUser().isApplicantNow;
        // alert( 'initialiseInvites ' + this.isApplicantNow );
        this.initialiseInvitesIsCalled = true;
    }

    ngOnInit() {
        // alert(this.principal.getLoggedInUser().authorities);
        // alert(this.principal.getLoggedInUser().roles);
        this.principal.getLoggedInUser().roles.forEach(role => {
            if (role.includes('role_operator')) {
                this.isOperator = true;
                this.primaryRole = role;
            }
            if (role.includes('role_admin')) {
                this.isAdmin = true;
                this.primaryRole = role;
            }
            if (role.includes('role_user')) {
                this.hasApplicantRole = true;
                this.secondaryRole = role;
            }
        });
        if (this.initialiseInvitesIsCalled === false) {
            // alert(this.isOperator);
            // alert(this.isAdmin);
            // alert(this.hasApplicantRole);
            if (this.isOperator === true || this.isAdmin === true) {
                this.principal.getLoggedInUser().authorities = this.principal.getLoggedInUser().roleBasedAuthorities[this.primaryRole];
                this.principal.getLoggedInUser().isApplicantNow = this.isApplicantNow = false;
            } else if (this.hasApplicantRole === true) {
                this.principal.getLoggedInUser().authorities = this.principal.getLoggedInUser().roleBasedAuthorities[this.secondaryRole];
                this.principal.getLoggedInUser().isApplicantNow = this.isApplicantNow = true;
            } else {
                this.noRoleAssigned = true;
            }
        } else if (this.initialiseInvitesIsCalled === true) {
            if (this.isApplicantNow === true) {
                this.principal.getLoggedInUser().authorities = this.principal.getLoggedInUser().roleBasedAuthorities[this.secondaryRole];
            } else {
                this.principal.getLoggedInUser().authorities = this.principal.getLoggedInUser().roleBasedAuthorities[this.primaryRole];
            }
        }
        // alert( 'ngoninit: initialiseInvites ' + this.isApplicantNow );
        // alert(this.principal.getLoggedInUser().authorities);
    }

    ngOnDestroy() {
        // avoid memory leaks here by cleaning up after ourselves. If we
        // don't then we will continue to run our initialiseInvites()
        // method on every navigationEnd event
        if (this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    }
}
