import { Principal } from 'app/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Page } from 'app/visual-design/page.model';

@Injectable({
    providedIn: 'root'
})
export class PendingEnrollmentStatsService {
    uri: string;
    constructor(private httpClient: HttpClient, private principal: Principal) {
        const orgName = this.principal.getLoggedInUser().organization;
        this.uri = `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users`;
    }

    // Call REST APIs
    pendingEnrollmentStats(
        groupID: string,
        sort: string,
        order: string,
        page: number,
        size: number,
        startDate: Date,
        endDate: Date
    ): Observable<Page> {
        return this.httpClient.get<Page>(
            `${
                this.uri
            }/groups/${groupID}/pendingEnrollmentStats?sort=${sort}&order=${order}&page=${page}&size=${size}&startDate=${startDate}&endDate=${endDate}`
        );
    }
}

export class UserEntity {
    id: any;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    createdDate: string;
    createdBy: string;
    enabled: boolean;
    isFederated: boolean;
    groupId: string;
    groupName: string;
    identityTypeName: string;
}
