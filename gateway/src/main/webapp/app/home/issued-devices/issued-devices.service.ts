import { Principal } from 'app/core';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class IssuedDevicesForUserStats {
    issuedDevicesForUser: IssuedDevicesForUser[] = [];

    constructor(private httpClient: HttpClient, private principal: Principal) {}

    // Call REST APIs
    issuedDevicesForUserStats(): Observable<any> {
        const orgName = this.principal.getLoggedInUser().organization;
        const username = this.principal.getLoggedInUser().username;
        return this.httpClient.get<IssuedDevicesForUser[]>(
            `${window.location.origin}/apdu/api/v1/devices/organizations/${orgName}/issuedDevicesForUserStats/${username}`
        );
    }
}

export class IssuedDevicesForUser {
    type: string;
    name: string;
    connection: string;
    serialnumber: string;
    status: string;
}
