import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IssuedDevicesForUserStats, IssuedDevicesForUser } from './issued-devices.service';
import { DataService, Data } from 'app/device-profile/data.service';
import { Principal } from 'app/core';
import { ManageIdentityDevicesService } from 'app/manage-identity-devices/manage-identity-devices.service';
import { MatDialog } from '@angular/material';
import { AddIdentityDeviceDialogComponent } from 'app/manage-identity-devices/manage-identity-devices.component';
import { TranslateService } from '@ngx-translate/core';
import { IssuanceService } from 'app/issuance/issuance.service';
import { AddIdentityDeviceDialogService } from 'app/manage-identity-devices/add-identity-device-dialog.service';

@Component({
    selector: 'jhi-issued-devices',
    templateUrl: './issued-devices.component.html',
    styles: []
})
export class IssuedDevicesComponent implements OnInit {
    role: string;
    isPairMobileDevicePermissionEnabled = false;
    isIssuanceEnabled = false;
    isEnrollmentEnabled = false;
    issuedDevicesForUser: IssuedDevicesForUser[] = [];
    index = 0;
    userEnrolled = false;
    isPageLoaded = false;
    isPendingApproval = false;
    user;
    constructor(
        private router: Router,
        private principal: Principal,
        public dialog: MatDialog,
        private dataService: DataService,
        private manageIdentityDevicesService: ManageIdentityDevicesService,
        private issuedDevicesForUserStats: IssuedDevicesForUserStats,
        private translateService: TranslateService,
        private issuanceService: IssuanceService,
        private identityDeviceDialogService: AddIdentityDeviceDialogService
    ) {}

    ngOnInit() {
        if (this.principal.getLoggedInUser().roles.includes('role_user')) {
            if (this.principal.getLoggedInUser().authorities.includes('Menu_Pair_Mobile_Device')) {
                this.isPairMobileDevicePermissionEnabled = true;
            }
            if (this.principal.getLoggedInUser().authorities.includes('Menu_Issuance')) {
                this.isIssuanceEnabled = true;
            }
            if (this.principal.getLoggedInUser().authorities.includes('Menu_Enrollment')) {
                this.isEnrollmentEnabled = true;
            }
            this.manageIdentityDevicesService.getUserByName(this.principal.getLoggedInUser().username).subscribe(user => {
                if (user !== null && user.id !== null) {
                    if (user.status === 'PENDING_ENROLLMENT' || user.status === 'ENROLLMENT_IN_PROGRESS') {
                        this.isPageLoaded = true;
                        this.userEnrolled = false;
                        return;
                    } else {
                        this.userEnrolled = true;
                        if (user.status === 'PENDING_APPROVAL') {
                            this.isPendingApproval = true;
                        }
                        this.issuedDevicesForUserStats.issuedDevicesForUserStats().subscribe(issuedDevicesForUser => {
                            if (issuedDevicesForUser.length > 0) {
                                this.issuedDevicesForUser = issuedDevicesForUser;
                            }
                            this.isPageLoaded = true;
                        });
                        this.isPageLoaded = true;
                    }
                }
            });
        } else {
            this.userEnrolled = false;
            this.isPageLoaded = true;
        }
    }
    issueUser() {
        this.manageIdentityDevicesService.getUserByName(this.principal.getLoggedInUser().username).subscribe(user => {
            if (user !== null) {
                user.identityType = user.workflow.organizationIdentities.identityTypeName;
                this.identityDeviceDialogService.openAddIdentityDeviceDialog(user);
            }
        });
    }
}
