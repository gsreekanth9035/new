import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jhi-subscription',
  templateUrl: './subscription.component.html',
  styles: []
})
export class SubscriptionComponent implements OnInit {
  color = 'primary';
  diameter = 200;
  mode = 'determinate';
  value = 100;

  constructor() { }

  ngOnInit() {
  }

}
