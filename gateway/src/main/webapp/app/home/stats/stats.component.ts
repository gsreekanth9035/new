import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Stats, UserStats, DeviceStats } from './stats.service';
import { DataService, Data } from 'app/device-profile/data.service';

@Component({
    selector: 'jhi-stats',
    templateUrl: './stats.component.html',
    styles: []
})
export class StatsComponent implements OnInit {
    userStats: UserStats[] = [];
    deviceStats: DeviceStats[] = [];
    index = 0;
    constructor(private router: Router, private route: ActivatedRoute, private stats: Stats, private dataService: DataService) {
        this.stats.userStats().subscribe(userStats => {
            this.userStats = userStats;
        });

        this.stats.deviceStats().subscribe(deviceStats => {
            this.deviceStats = deviceStats;
        });
    }

    ngOnInit() {}
}
