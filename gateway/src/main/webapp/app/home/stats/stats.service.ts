import { Principal } from 'app/core';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class Stats {
    constructor(private httpClient: HttpClient, private principal: Principal) {}

    // Call REST APIs
    userStats(): Observable<UserStats[]> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<UserStats[]>(`${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/userStats`);
    }
    deviceStats(): Observable<DeviceStats[]> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<DeviceStats[]>(`${window.location.origin}/apdu/api/v1/devices/organizations/${orgName}/deviceStats`);
    }
}

export class UserStats {
    type: string;
    totalUsers: number;
    totalEnrolled: number;
    totalActive: number;
    totalSuspended: number;
    totalRevoked: number;
    totalExpired: number;
    constructor() {}
}

export class DeviceStats {
    type: string;
    totalAvailable: number;
    totalEnrolled: number;
    totalActive: number;
    totalSuspended: number;
    totalRevoked: number;
    totalExpired: number;
    constructor() {}
}
