import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PendingIssuanceStatsService, UserEntity } from './pending-issuance.service';
import { DataService, Data } from 'app/device-profile/data.service';
import { MatPaginator, MatSort, DateAdapter, MAT_DATE_FORMATS, MatDatepickerInputEvent, MatDialog } from '@angular/material';
import { merge, of as observableOf } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { AppDateAdapter, APP_DATE_FORMATS } from 'app/shared/util/date.adapter';
import { FormControl, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { SearchUserService } from 'app/search-user/search-user.service';
import { TranslateService } from '@ngx-translate/core';
import { Principal } from 'app/core';
import { IssuanceService } from 'app/issuance/issuance.service';
import { AddIdentityDeviceDialogService } from 'app/manage-identity-devices/add-identity-device-dialog.service';
import { SharedService } from 'app/shared/util/shared-service';

@Component({
    selector: 'jhi-pending-issuance',
    templateUrl: './pending-issuance.component.html',
    styleUrls: ['pending-issuance.scss'],
    providers: [{ provide: DateAdapter, useClass: AppDateAdapter }, { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }]
})
export class PendingIssuanceComponent implements OnInit {
    displayedColumns: string[] = ['requestType', 'requestedBy', 'requestedFor', 'requestedDate', 'currentStatus', 'actions'];
    data: UserEntity[] = [];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    pageSizeOptions = [5, 10, 15, 20];
    parentPageSizeOptions = [5, 10, 15, 20];
    pageSize;
    resultsLength = 0;
    group = this.principal.getLoggedInUser().groups;

    createdByStartDate = null;
    createdByEndDate = null;
    startDate = new FormControl();
    endDate = new FormControl();
    index = 0;
    username_tbd: string;
    userFirstName_tbd: string;
    isLoadingResults: boolean;
    success: boolean;
    message: any;
    userToBeDel: UserEntity;
    maxDate = new Date();
    dateValidationError = false;
    todayDateValidation = false;
    startDatevalidationPattern = false;
    endDateValidationPattern = false;
    endDateValue = new Date();
    startDateValue = new Date();

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private principal: Principal,
        public dialog: MatDialog,
        private pendingIssuanceStatsService: PendingIssuanceStatsService,
        private dataService: DataService,
        private searchUserService: SearchUserService,
        private translateService: TranslateService,
        private datePipe: DatePipe,
        private issuanceService: IssuanceService,
        private identityDeviceDialogService: AddIdentityDeviceDialogService,
        private sharedService: SharedService
    ) {}

    ngOnInit() {
        this.pageSize = this.pageSizeOptions[0];
        this.getPendingIssuanceUsers();
    }
    getPendingIssuanceUsers() {
        this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    if (this.paginator.pageSize === undefined) {
                        this.paginator.pageSize = this.pageSizeOptions[0];
                    }
                    if (this.startDate.value !== null) {
                        this.createdByStartDate = this.datePipe.transform(this.startDate.value, 'dd/MM/yyyy');
                    }
                    if (this.endDate.value !== null) {
                        this.createdByEndDate = this.datePipe.transform(this.endDate.value, 'dd/MM/yyyy');
                    }
                    return this.pendingIssuanceStatsService.pendingIssuanceStats(
                        this.group[0].id,
                        this.sort.active,
                        this.sort.direction,
                        this.paginator.pageIndex,
                        this.paginator.pageSize,
                        this.createdByStartDate,
                        this.createdByEndDate
                    );
                }),
                map(data => {
                    this.resultsLength = data.totalElements;
                    if (data.totalElements > 0) {
                        this.pageSizeOptions = this.sharedService.getPageSizeOptionsBasedOnTotalElements(
                            data.totalElements,
                            this.parentPageSizeOptions
                        );
                    }
                    return data.content;
                }),
                catchError(() => {
                    return observableOf([]);
                })
            )
            .subscribe(data => (this.data = data));
    }

    issueUser(userEntity) {
        this.issuanceService.getUserByName(userEntity.username).subscribe(user => {
            if (user !== null) {
                user.identityType = userEntity.identityType;
                this.identityDeviceDialogService.openAddIdentityDeviceDialog(user);
            }
        });
    }
    onKeyUp(id: any, event: any) {
        if (this.endDateValue !== null) {
            this.endDate.setErrors(null);
        }
        if (this.startDateValue !== null) {
            this.startDate.setErrors(null);
        }
        if (id === 1) {
            this.startDatevalidationPattern = false;
            if (event.target.value === '') {
                this.startDate.setErrors(null);
            } else {
                if (event.target.value.match(".*[A-Za-z$&+,:;=?@#|'<>.^*()%!]+.*")) {
                    this.startDate.setErrors(Validators.pattern);
                    this.startDatevalidationPattern = true;
                }
            }
        } else if (id === 2) {
            this.endDateValidationPattern = false;
            if (event.target.value === '') {
                this.endDate.setErrors(null);
            } else {
                if (event.target.value.match(".*[A-Za-z$&+,:;=?@#|'<>.^*()%!]+.*")) {
                    this.endDate.setErrors(Validators.pattern);
                    this.endDateValidationPattern = true;
                }
            }
        }
    }
    onChangeDate(field, event: MatDatepickerInputEvent<Date>) {
        if (field === 'startDate') {
            this.createdByStartDate = this.datePipe.transform(event.value, 'dd/MM/yyyy');
            this.dateValidationError = false;
            this.todayDateValidation = false;
            this.startDateValue = event.value;
            const startDateObj = new Date(this.startDateValue);
            const todayDateObj = new Date(this.maxDate);
            const endDateObj = new Date(this.endDateValue);
            if (startDateObj > todayDateObj) {
                this.todayDateValidation = true;
                this.startDate.setErrors(Validators.pattern);
            } else {
                if (this.createdByStartDate != null && this.createdByEndDate != null) {
                    this.startDate.setErrors(null);
                    this.endDate.setErrors(null);
                    if (startDateObj > endDateObj) {
                        this.startDate.setErrors(Validators.pattern);
                        this.dateValidationError = true;
                    }
                }
            }
        }
        if (field === 'endDate') {
            this.createdByEndDate = this.datePipe.transform(event.value, 'dd/MM/yyyy');
            this.endDateValue = event.value;
            this.dateValidationError = false;
            this.todayDateValidation = false;
            const startDateObj = new Date(this.startDateValue);
            const todayDateObj = new Date(this.maxDate);
            const endDateObj = new Date(this.endDateValue);
            if (endDateObj > todayDateObj) {
                this.todayDateValidation = true;
                this.endDate.setErrors(Validators.pattern);
            } else if (this.createdByStartDate != null && this.createdByEndDate != null) {
                this.startDate.setErrors(null);
                this.endDate.setErrors(null);
                if (startDateObj > endDateObj) {
                    this.endDate.setErrors(Validators.pattern);
                    this.dateValidationError = true;
                }
            }
        }
        this.pendingIssuanceStatsService
            .pendingIssuanceStats(
                this.group[0].id,
                this.sort.active,
                this.sort.direction,
                this.paginator.pageIndex,
                this.paginator.pageSize,
                this.createdByStartDate,
                this.createdByEndDate
            )
            .subscribe(result => {
                this.resultsLength = result.totalElements;
                this.data = result.content;
            });
    }

    editUser(userEntity: UserEntity) {
        const data = new Data();
        data.userFirstName = userEntity.firstName;
        data.userLastName = userEntity.lastName;
        data.userName = userEntity.username;
        data.isEnrolledUser = true;
        data.groupId = userEntity.groupId;
        data.groupName = userEntity.groupName;
        data.identityType = userEntity.identityTypeName;
        this.dataService.changeObj(data);
        this.router.navigate(['/register']);
    }

    displayEnrollmentDetails(userEntity: UserEntity) {
        const data = new Data();
        data.userFirstName = userEntity.firstName;
        data.userLastName = userEntity.lastName;
        data.userName = userEntity.username;
        data.isEnrolledUser = true;
        data.routeFrom = this.router.url;
        data.isFederated = userEntity.isFederated;
        data.groupId = userEntity.groupId;
        data.groupName = userEntity.groupName;
        data.identityType = userEntity.identityTypeName;
        this.dataService.changeObj(data);
        this.router.navigate(['/enrollmentDetails']);
    }

    setDeleteEnrolledUser(user: UserEntity) {
        this.userToBeDel = user;
        this.username_tbd = user.username;
        this.userFirstName_tbd = user.firstName;
    }

    deleteEnrolledUser() {
        this.isLoadingResults = true;
        this.searchUserService.deleteUser(this.username_tbd, this.userToBeDel.isFederated).subscribe(result => {
            if (result.status === 200) {
                this.translateService.get('searchUser.deletedSuccessMsg').subscribe(message => {
                    this.success = true;
                    this.message = message;
                    this.isLoadingResults = false;
                    this.getPendingIssuanceUsers();
                });
            }
        });
    }
}
