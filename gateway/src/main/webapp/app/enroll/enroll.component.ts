import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit, Inject, LOCALE_ID } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserAttribute } from './userattribute.model';
import { UserAppearance } from './userappearance.model';
import {
    EnrollService,
    WFStepVisualTemplateConfig,
    VisualCredential,
    FingerModel,
    IrisModel,
    FingerCaptureResponse,
    FingerpintDeviceConfig
} from './enroll.service';
import { UserBiometric, UserBiometricAttribute } from './userbiometric.model';
import { UserBiometricSkip } from './userbiometric-skip.model';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Principal, UserInfo } from 'app/core';
import { User } from './user.model';
import * as $ from 'jquery';
import 'bootstrap';
import 'jquery-cropper';
import 'cropperjs';
import { Subscription, timer } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { DataService, Data } from 'app/device-profile/data.service';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MatTableDataSource,
    MatSort,
    MatDatepickerInputEvent,
    MAT_DATE_LOCALE,
    MatDatepicker,
    MatDialog
} from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from 'app/shared/util/date.adapter';
import { formatDate } from '@angular/common';
import { IDProofType } from './idProofType.model';
import { UserIDProof, IDProofIssuingAuthority } from './userIdProof.model';
import { SidebarService } from 'app/layouts/sidebar/sidebar.service';
import { VehicleClassifications } from './vehicleClassifications.model';
import { DrivingLicenseRestrictions } from './drivingLicenseRestrictions.model';
import { UserDrivingLicense } from './userdrivinglicense.model';
import { UserEmployeeID } from './userEmployeeID.model';
import { UserNationalID } from './userNationalID.model';
import { UserVoterID } from './userVoterID.model';
import { UserPermanentResidentID } from './userPermanentResidentID.model';
import { UserStudentID } from './userStudentID.model';
import { UserHealthID } from './userHealthID.model';
import { UserHealthService } from './userHealthService.model';
import { UserHealthServiceVaccineInfo } from './userHealthServiceVaccineInfo.model';
import { UserEnrollmentHistory } from './userenrollmenthistory.model ';
import * as _moment from 'moment';
//  import {  default as _rollupMoment} from 'moment';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { Moment } from 'moment';
import { RegistrationConfigDetails } from './registrationconfigdetails.model';
import { Workflow } from 'app/workflow/workflow.model';
import { UserPIV } from './userPIV.model';
import { IdentityTypeEnum } from './identityType.enum';
import { WorkflowStep } from 'app/workflow/workflow-step.model';
import { Group } from 'app/workflow/group.model';
import { SharedService } from 'app/shared/util/shared-service';
import { AddIdentityDeviceDialogService } from 'app/manage-identity-devices/add-identity-device-dialog.service';
import { SearchUserService } from 'app/search-user/search-user.service';
import { Country } from 'app/onboarding-user/onboarding-user.component';
import { DeviceClientService } from 'app/shared/util/device-client.service';
import { switchMap } from 'rxjs/operators';
import { EnrollmentStepsStatus } from './enrollmentStepsStatus.model';
import { SigantureListResponse, SigantureModel, SigantureCaptureResponse, SignatureResponse } from './model/signature.model';
import { UserAddress } from './user-address.model';
import { DialogDataResponse, ReActivateDialogComponent } from 'app/manage-identity-devices/manage-identity-devices.component';
import { WFStepRegistrationConfig } from 'app/workflow/wf-step-registration-config.model';
import { OrganizationIdentity } from 'app/workflow/model/org-identity-model';
import { WorkflowService } from 'app/workflow/workflow.service';
import { ValidationByLanguageService, ValidatorsComponent } from 'app/shared/util/validation-by-language.service';
import { WFStepGenericConfig } from 'app/workflow/wf-step-generic-config.model';
declare var $: any;
const moment = _moment;

@Component({
    selector: 'jhi-enroll',
    templateUrl: './enroll.component.html',
    styleUrls: ['enroll.scss'],
    providers: [{ provide: DateAdapter, useClass: AppDateAdapter }, { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }]
})
export class EnrollComponent implements OnInit, OnDestroy, AfterViewInit {
    // TODO Refactor
    donors = [];
    x = 'save1';
    y;
    createdDate = Date.now();
    enrollmentStartDate;
    enrollmentEndDate;
    tdc;
    hasIssuancePermission = false;
    // Common
    workflow: Workflow = null;
    userFirstAndLastName: string;
    userToBeEnrolled: UserInfo = new UserInfo();

    isIDProofDisabled = true;
    isRegFormDisabled = true;
    isFaceDisabled = true;
    isIrisDisabled = true;
    isFingerprintDisabled = true;
    isRollFingerprintDisabled = true;
    isSignatureDisabled = true;
    isEnrolledWizardTab: boolean;

    userToEnroll: string;
    userId: number;
    statusMessage: string;
    error = false;
    errormessage = '';
    errormessage1 = 'Unable To Save User Information, Retry After Some Time';
    errormessage2 = 'Duplicate Document Type';
    isClickBackToFailedWizardEnabled = false;
    saveFailureMessage: string;
    isEnrolledUser = false;
    isEnrollmentInProgress = false;
    isPendingApproval = false;
    isIssuedOrReadyForIssuance = false;
    isUserConsentToEditEnrollment = false;
    groupId: string;
    groupName: string;
    identityType: string;
    isDocumentsLoaded: boolean;
    userAddress: UserAddress = null;
    userAttribute: UserAttribute = null; // = new UserAttribute();
    userHealthID: UserHealthID = null;
    userEmployeeID: UserEmployeeID = null;
    userStudentID: UserStudentID = null;
    userAppearance: UserAppearance = null;
    userPIV: UserPIV = null;
    userDrivingLicense: UserDrivingLicense = null; // = new UserDrivingLicense();
    userNationalID: UserNationalID = null; // = new UserNationalID();
    userVoterID: UserVoterID = null;
    userPermanentResidentID: UserPermanentResidentID = null;
    userHealthService: UserHealthService = null;
    userHealthServiceVaccineInfo: UserHealthServiceVaccineInfo = null;
    userHealthServiceVaccineInfos: UserHealthServiceVaccineInfo[] = [];
    userEnrollmentHistory: UserEnrollmentHistory = null;
    data: Data;
    // workflow
    previousStep: string;
    nextStep: string;
    firstStep: string;
    enrollmentSteps: string[] = [
        'ID_PROOFING',
        'REGISTRATION',
        'FACE_CAPTURE',
        'IRIS_CAPTURE',
        'FINGERPRINT_CAPTURE',
        'FINGERPRINT_ROLL_CAPTURE',
        'SIGNATURE_CAPTURE',
        'ENROLL_SUMMARY'
    ];

    workflowStep: WorkflowStep = new WorkflowStep();

    workflowSteps: string[];
    // is Face step added in workflow edit
    isFaceAddedNewlyInWf = false;
    // is Finger print step added in workflow edit
    isFPAddedNewlyInWf = false;
    // is Iris Step added in workflow edit
    isIrisAddedNewlyInWf = false;
    // if Fingerpring configuration changed(min required fingers, thershold value) in edit workflow
    isFPConfigEditedInWf = false;

    // Wizard
    previousSelectedWizardTab: string;
    selectedWizardTab: string;
    a: number;
    b: number;
    c: number;
    d: number;
    e: number;
    f: number;
    g: number;
    h: number;

    // Info
    enrollForm: FormGroup = this.formBuilder.group({});
    isFormCreated: boolean;
    isStateDisabled = true;
    registrationStepDetail: WorkflowStep;
    captureDocument = false;
    infoNextVisibility = true;
    regFormFieldsValidations: any = [];

    // Id proofing starts
    openFrontImageBlock = false;
    openBackImageBlock = false;
    openFileImage = false;
    isAnimate = false;
    // Id proofing ends
    hideIDproofCapture = false;
    isCapturedId = false;
    faceCaptureError = false;
    cameraAccessError = false;
    faceCaptureErrorMsg = '';
    uploadedFaceImage = null;
    isCapturedIdBack = false;
    isCroppedId = false;
    isCroppedIdBack = false;
    capturedImgId: any = [];
    capturedImgIdBack: any = [];
    recaptureIdProofDoc = false;
    croppedImgId: any;
    croppedImgIdBack: any;
    allowCameraSwitchIDProof = false;
    multipleWebcamsAvailable = false;
    // deviceId: string;
    errorsId: WebcamInitError[] = [];
    //   // latest snapshot
    webcamImageId: WebcamImage = null;
    webcamImageIdBack: WebcamImage = null;
    //   // webcam snapshot trigger
    private triggerId: Subject<void> = new Subject<void>();
    //   // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
    private nextWebcamId: Subject<boolean | string> = new Subject<boolean | string>();
    availableMediaDevicesId: MediaDeviceInfo[];
    public videoOptionsId: MediaTrackConstraints = {
        width: { min: 1050, max: 1200 },
        height: { ideal: 576 }
    };
    FaceCropperOptions: any;
    frontCapture = true;
    backCapture = false;
    idProofDocSaveError = false;
    idProofCamAccessError = false;
    idProofDocFileError = false;
    @ViewChild('idProof') idProofElement: ElementRef;
    idProofDocSaveErrorMsg = '';
    displayedColumns: string[] = ['Name', 'Type', 'Issuing Authority', 'Front Image', 'Back Iamge', 'Actions'];
    summaryDisplayedColumns: string[] = ['Type', 'Issuing Authority', 'Front Image', 'Back Iamge'];
    faceWebCamDevice = new FormControl('');
    idProofs: OrganizationIdentity[];
    vechileClassifications: VehicleClassifications[];
    drivingLicenseRestrictions: DrivingLicenseRestrictions[];
    dataSource: MatTableDataSource<UserIDProof>;
    @ViewChild(MatSort) sort: MatSort;
    userIDProofList: UserIDProof[] = [];
    docId = 0;
    IDProofDocForm: FormGroup;
    preview = false;
    isPdfChoosen = false;
    // fpDevices = [{ value: 'secugen', viewValue: 'Secugen' }, { value: 'greenbit', viewValue: 'Greenbit' }];
    issueAuthorities: IDProofIssuingAuthority[] = [];

    sources = [{ value: 'capture', viewValue: 'Capture' }, { value: 'upload', viewValue: 'Upload' }];

    // Iris

    irisDevice = new FormControl('');
    availableIrisDevices: IrisModel[] = [];
    irisPositions: string[] = ['LEFT_EYE,RIGHT_EYE'];
    irisStepDetail: any;
    wfIrisQuality: number;
    isIrisTabSelected = false;
    selectedIrisReader: string;
    irisModelObj = new IrisModel();
    irisModelObjInEdit = new IrisModel();
    leftEye = null;
    rightEye = null;
    irisSubscriber: Subscription;
    irisErrorMsg = '';
    irisWarningMsg = '';
    irisInfoMsg = '';
    irisSuccessMsg = '';
    irisRecapture = false;
    enableIrisRecaptureButton = false;

    // user
    user: User = new User();

    // load registration dropdownlist
    registrationConfigDetails: RegistrationConfigDetails = new RegistrationConfigDetails();

    leftRollFingerprintImages: UserBiometric[] = [];
    rightRollFingerprintImages: UserBiometric[] = [];

    // Fingerprint
    isRollsupport: boolean = true;
    FingerpintDeviceConfig: FingerpintDeviceConfig;
    fingerprintDevice = new FormControl('');
    skipType = new FormControl('');
    skipReason = new FormControl('');
    availableFingerprintDevices: FingerModel[] = [];
    // selectedFingerprintReader: string;
    leftFourFingers: UserBiometric;
    rightFourFingers: UserBiometric;
    thumbFingers: UserBiometric;
    ///// code for dual fingerprints
    indexMiddleLeft: UserBiometric;
    ringLittleLeft: UserBiometric;
    indexMiddleRight: UserBiometric;
    ringLittleRight: UserBiometric;
    thumbsTwo: UserBiometric;
    ////
    leftFingerprintImages: UserBiometric[] = [];
    rightFingerprintImages: UserBiometric[] = [];
    fingerPositions: string[] = [
        'LEFT_THUMB',
        'LEFT_INDEX',
        'LEFT_MIDDLE',
        'LEFT_RING',
        'LEFT_LITTLE',
        'RIGHT_THUMB',
        'RIGHT_INDEX',
        'RIGHT_MIDDLE',
        'RIGHT_RING',
        'RIGHT_LITTLE'
    ];
    dualFingerPositions: string[] = [
        'INDEX_MIDDLE_LEFT_2',
        'RING_LITTLE_LEFT_2',
        'INDEX_MIDDLE_RIGHT_2',
        'RING_LITTLE_RIGHT_2',
        'THUMBS_2'
    ];
    slapFingerPositions: string[] = ['FLAT_SLAP_4_LEFT', 'FLAT_SLAP_4_RIGHT', 'FLAT_SLAP_2_THUMBS'];
    // TODO load from workflow
    minRequiredFingerPositions: string[] = [];
    fingerpositionsFromWorkFlow: string[] = [];
    fingerprintCapturedPositions: string[] = [];
    fingerprintCapturedPositionsInRollFinger: string[] = [];
    index = 0;
    currentFingerPosition: string;
    skippedFingers: UserBiometricSkip[] = [];
    fingerprintSubscriber: Subscription;
    fpContextErrorMsg = '';
    fpContextWarningMsg = '';
    fpContextInfoMsg = '';
    fpContextSuccessMsg = '';
    isFingerprintTabSelected = false;
    fingerprintStepDetail: any;
    workflowImageQuality: number;
    minRequiredFingers: number;
    liveSwap = false;
    fingerModelObj = new FingerModel();
    fingerModelInEdit = new FingerModel();
    fpCaptureCalledCount = 0;
    isSingleFPCaptured = true;
    isSlapFPCaptured = false;
    isTwoFPCaptured = false;
    selectedFingerPositionForRecapture;
    enableFPRecaptureButton = false;
    // enableFPRecaptureButtonInROllFinger = false;
    isFPRecaptureSelected = false;
    newlyAddedFpsCapturePositions = [];
    isFpAutoCaptureCompleted = false;
    isRollFpAutoCaptureCompleted = false;
    isClearFpEnabled = false;
    isClearRollFpEnabled = false;
    isClearFpCalled = false;
    isClearFpCalledInRollFinger = false;
    disableFpDeviceSelection = false;
    disableRollFpDeviceSelection = false;
    // Face
    isTransparentPhotoAllowed = false;
    cropFaceWidth = null; // crop width from workflow
    cropFaceHeight = null; // crop height from workflow
    isCaptured = false;
    isCropped = false;
    captureImage = true;
    capturedImg: any = [];
    croppedImg: any;
    transparentImg: any;
    transparentImgSelected = new FormControl('yes');
    allowCameraSwitch = true;
    deviceId: string;
    errors: WebcamInitError[] = [];
    showInitialFaceCaptureMsg = true;
    // latest snapshot
    webcamImage: WebcamImage = null;
    // webcam snapshot trigger
    private trigger: Subject<void> = new Subject<void>();
    // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
    private nextWebcam: Subject<boolean | string> = new Subject<boolean | string>();
    public videoOptions: MediaTrackConstraints = {
        width: { min: 1050, max: 1200 },
        height: { ideal: 576 }
    };
    faceCaptureCalledInEdit = false;
    camSubscription: Subscription;
    // Signature
    @ViewChild(SignaturePad) signaturePad: SignaturePad;
    public signaturePadOptions: Object = {
        canvasWidth: 535,
        canvasHeight: 94
    };
    public _signature: any = null;
    signatureCaptureTxt: string;
    signatureDevice = new FormControl('');
    signErrorMsg = '';
    signSuccessMsg = '';
    signInfoMsg = '';
    reCaptureSignature = false;
    capturedSignModel = new SignatureResponse();

    // Summary
    credentialTemplates: WFStepVisualTemplateConfig[] = [];
    selectedCredentialTemplate: FormControl = new FormControl('');
    visualCredential: VisualCredential;

    // Save Success / Failure toggles
    saveSuccess = false;
    saveFailure = false;
    approvalRequired = false;
    hasApprovalPermission = false;

    public deviceServiceStatus: any;
    noWorkflowAssigned = false;
    isLoadingResults: boolean;
    isSummaryDiasbled = true;
    maxDate = new Date();
    minYear = this.maxDate.getFullYear() - 100;
    minDate = new Date(this.minYear, this.maxDate.getMonth(), this.maxDate.getDate());
    placeholder = 'mm/dd/yyyy';
    editIdProofDocument: UserIDProof;
    isEditIdProofDocSelected = false;
    enrolledBy: string;
    approvedBy: string;
    fpReaderSubscription: Subscription;
    irisReaderSubscription: Subscription;
    signatureDeviceSubscription: Subscription;
    sigantureCaptureSubscription: Subscription;
    availableSignDevices: SigantureModel[] = [];
    selfService = false;
    loggedInUserGroups: Group[] = [];
    setLdapUserFace = false;
    isFederatedUser = false;
    countryCodes: Country[] = [];
    userEnrollmentDetails = new User();
    enrollmentStepsStatus: EnrollmentStepsStatus;
    showToolTip = false;
    isRollFpCaptureSelected = false;

    subscription: Subscription;
    validations: ValidatorsComponent;

    isWsqFormatEnabled = false;
    hiddingNextButton = false;
    constructor(
        private formBuilder: FormBuilder,
        private translateService: TranslateService,
        private router: Router,
        private enrollService: EnrollService,
        private principal: Principal,
        private dataService: DataService,
        private sidebarService: SidebarService,
        @Inject(LOCALE_ID) private locale: string,
        private sharedService: SharedService,
        public dialog: MatDialog,
        private identityDeviceDialogService: AddIdentityDeviceDialogService,
        private searchUserService: SearchUserService,
        private deviceClientService: DeviceClientService,
        private workflowService: WorkflowService,
        private validationByLanguageService: ValidationByLanguageService
    ) {
        this.enrollmentStartDate = new Date();
        this.countryCodes = this.sharedService.getCountryCodes();
        this.subscription = this.validationByLanguageService.cuurentValidationsObj.subscribe(res => {
            this.validations = res;
            this.changeValidations();
        });
    }

    ngOnInit(): void {
        this.principal.hasAuthority('Menu_Issuance').then(res => {
            this.hasIssuancePermission = res;
        });
        this.loggedInUserGroups = this.principal.getLoggedInUser().groups;
        this.donors = [{ id: 'Y', value: 'SI' }, { id: 'N', value: 'NO' }];
        // this.loadUserDetails();
        this.enrolledBy = this.principal.getLoggedInUser().username;
        this.approvedBy = this.principal.getLoggedInUser().username;
        this.data = this.dataService.getCurrentObj();
        this.isEnrolledWizardTab = false;
        if (this.data !== null) {
            this.userToEnroll = this.data.userName;
            this.userId = this.data.userId;
            this.userFirstAndLastName = this.data.userFirstName + ' ' + this.data.userLastName;
            this.isEnrolledUser = this.data.isEnrolledUser;
            this.isEnrollmentInProgress = this.data.isEnrollmentInProgress;
            this.user.groupId = this.data.groupId;
            this.user.groupName = this.data.groupName;
            this.user.identityType = this.data.identityType;
            this.isFederatedUser = this.data.isFederated;
        } else {
            this.userToBeEnrolled = this.principal.getLoggedInUser();
            this.selfService = true;
            this.userToEnroll = this.userToBeEnrolled.username;
            this.userFirstAndLastName = this.userToBeEnrolled.firstName + ' ' + this.userToBeEnrolled.lastName;
            this.user.userAttribute = new UserAttribute();
            this.user.userAttribute.firstName = this.userToBeEnrolled.firstName;
            this.user.userAttribute.lastName = this.userToBeEnrolled.lastName;
            this.user.userAttribute.email = this.userToBeEnrolled.email;
            this.user.groupId = this.userToBeEnrolled.groups[0].id;
            this.user.groupName = this.userToBeEnrolled.groups[0].name;
            this.userId = this.userToBeEnrolled.userId;
            this.user.identityType = this.userToBeEnrolled.identityType;
        }
        // alert(this.userToEnroll);

        this.enrollService.getEnrollmentStepsStatus(this.userToEnroll).subscribe(
            response => {
                if (response !== null) {
                    this.enrollmentStepsStatus = response;
                    // alert(response);
                    // alert(this.enrollmentStepsStatus);
                }
            },
            error => {}
        );
        this.groupId = this.user.groupId;
        this.groupName = this.user.groupName;
        this.identityType = this.user.identityType;
        const userInfo: UserInfo = new UserInfo();
        let workflowSteps: any;
        userInfo.username = this.userToEnroll;
        const userFirstAndLastName = this.userFirstAndLastName.split(' ');
        userInfo.firstName = userFirstAndLastName[0];
        userInfo.lastName = userFirstAndLastName[1];
        this.enrollService.getWorkflowGeneralDetailsByUserName(this.userToEnroll).subscribe(
            response => {
                if (response !== null) {
                    if (this.userId === null) {
                        // if the searched user is federated user, then register that user in our db and then start enrollment, inrder to avoid null in nserver db
                        this.registerLoginUser(this.data);
                    }
                    this.workflow = response;
                    this.user.identityType = this.workflow.organizationIdentities.identityTypeName;
                    this.identityType = this.user.identityType;
                    userInfo.workflow = this.workflow.name;
                    userInfo.workflowSteps = [];
                    workflowSteps = this.enrollService.getWorkflowStepsByWorkflow(userInfo.workflow).subscribe(response_steps => {
                        response_steps.forEach(step => {
                            userInfo.workflowSteps.push(step.name);
                            if (step.name === 'FINGERPRINT_CAPTURE') {
                                if (step.wfStepGenericConfigs !== null) {
                                    let config: WFStepGenericConfig = step.wfStepGenericConfigs.find(e => e.name === 'FINGERPRINT_WSQ');
                                    if (config !== undefined) {
                                        if (config.value === 'Y') {
                                            this.isWsqFormatEnabled = true;
                                        }
                                    }
                                }
                            }
                        });
                        this.userToBeEnrolled = userInfo;
                        this.enrollService.getUserDetailsByName(this.userToEnroll).subscribe(
                            userdetails => {
                                this.user = userdetails;
                                this.userId = userdetails.id;
                                if (this.selfService) {
                                    if (this.principal.getLoggedInUser().roles.includes('role_user')) {
                                        if (this.user.status === 'PENDING_ENROLLMENT' || this.user.status === 'ENROLLMENT_IN_PROGRESS') {
                                            this.isEnrolledUser = false;
                                            this.isIssuedOrReadyForIssuance = false;
                                            if (this.user.status === 'ENROLLMENT_IN_PROGRESS') {
                                                this.isEnrollmentInProgress = true;
                                            }
                                        } else {
                                            this.isEnrolledUser = true;
                                            if (this.user.status === 'PENDING_APPROVAL') {
                                                this.isIssuedOrReadyForIssuance = false;
                                                this.isPendingApproval = true;
                                            } else {
                                                this.isPendingApproval = false;
                                                this.isIssuedOrReadyForIssuance = true;
                                            }
                                        }
                                    }
                                }
                                if (this.isEnrolledUser || this.isEnrollmentInProgress) {
                                    // this.isDocumentsLoaded = true;
                                    this.setIdProofDocuments(userdetails.userIDProofDocuments);
                                    // set biometrics incase of edit/ enrollment in progress
                                    this.setBiometrics(userdetails.userBiometrics);
                                } else {
                                    this.user.userAttribute.userId = this.user.id;
                                    if (this.user.isNewLdapUser) {
                                        // set face/ signature.. if it has
                                        if (this.user.userBiometrics !== null) {
                                            if (this.user.userBiometrics.length > 0) {
                                                this.user.userBiometrics.forEach(userBiometric => {
                                                    if (
                                                        this.getUserToBeEnrolled().workflowSteps.find(e => e === 'FACE_CAPTURE') !==
                                                        undefined
                                                    ) {
                                                        this.setLdapUserFace = true;
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                                this.enrollService
                                    .getUserRegistrationConfigDetailsByOrgName(this.workflow.organizationIdentities.id)
                                    .subscribe(
                                        registrationConfigDetails => {
                                            this.registrationConfigDetails = registrationConfigDetails;
                                        },
                                        error => {}
                                    );
                                this.loadWorkflowSteps();
                            },
                            error => {}
                        );
                    });
                } else {
                    this.noWorkflowAssigned = true;
                    // return;
                    console.log('User is not assigned to any workflow.');
                }
            },
            error => {}
        );
        this.workflowService.getOrgTrustedExistingIdentityModels().subscribe((idModels: OrganizationIdentity[]) => {
            this.idProofs = idModels;
            this.idProofs.forEach(e => {
                if (e.identityTypeDetails.includes('front') && !e.identityTypeDetails.includes('back')) {
                    e.identityTypeDetails = e.identityTypeDetails + ',upload';
                }
            });
        });
        this.enrollService.getVehicleClassifications().subscribe(result => {
            this.vechileClassifications = result;
        });
        this.enrollService.getDrivingLicenseRestrictions().subscribe(result => {
            this.drivingLicenseRestrictions = result;
        });
        this.getAvailableMediaDevices();
        this.buidIdProofForm();
        this.enrollmentInProgressWizard();
    }
    getAvailableMediaDevices() {
        WebcamUtil.getAvailableVideoInputs().then((mediaDevicesId: MediaDeviceInfo[]) => {
            this.multipleWebcamsAvailable = mediaDevicesId && mediaDevicesId.length > 1;
            this.availableMediaDevicesId = mediaDevicesId;
            if (this.multipleWebcamsAvailable) {
                this.allowCameraSwitchIDProof = true;
            }
        });
    }

    setUserConsentToEditEnrollment() {
        this.isUserConsentToEditEnrollment = true;
    }
    registerLoginUser(data: Data) {
        const userObj = new User();
        userObj.firstName = data.userFirstName;
        userObj.lastName = data.userLastName;
        userObj.name = data.userName;
        userObj.email = data.userEmail;
        userObj.groupId = data.groupId;
        userObj.groupName = data.groupName;
        userObj.isFederated = data.isFederated;
        this.enrollService.registerLoginUser(userObj).subscribe(
            res => {
                let useruser = new User();
                useruser = res.body;
                this.userId = useruser.id;
                if (this.userId !== null) {
                    this.principal.getLoggedInUser().userId = this.userId;
                }
            },
            err => {
                console.log('error occurs');
            }
        );
    }
    setWizardColorForEnrolled() {
        $('.wT .wizardtd .documentbtn').attr('disabled', false);
        $('.wT .wizardtd .infobtn').attr('disabled', false);
        $('.wT .wizardtd .facebtn').attr('disabled', false);
        $('.wT .wizardtd .irisbtn').attr('disabled', false);
        $('.wT .wizardtd .fpRollbtn').attr('disabled', false);
        $('.wT .wizardtd .fpbtn').attr('disabled', false);
        $('.wT .wizardtd .signbtn').attr('disabled', false);
        $('.wT .wizardtd .summarybtn').attr('disabled', false);

        $('.wT .wizardtd .documentbtn').css('border', '4px solid #00bcd4');
        $('.wT .wizardtd .infobtn').css('border', '4px solid #00bcd4');
        $('.wT .wizardtd .facebtn').css('border', '4px solid #00bcd4');
        $('.wT .wizardtd .irisbtn').css('border', '4px solid #00bcd4');
        $('.wT .wizardtd .fpRollbtn').css('border', '4px solid #00bcd4');
        $('.wT .wizardtd .fpbtn').css('border', '4px solid #00bcd4');
        $('.wT .wizardtd .signbtn').css('border', '4px solid #00bcd4');
        $('.wT .wizardtd .summarybtn').css('border', '4px solid #00bcd4');

        $('.wT .wizardtd .spandocumentbtn').css('color', '#00bcd4');
        $('.wT .wizardtd .spaninfobtn').css('color', '#00bcd4');
        $('.wT .wizardtd .spanfacebtn').css('color', '#00bcd4');
        $('.wT .wizardtd .spanirisbtn').css('color', '#00bcd4');
        $('.wT .wizardtd .spanfpRollbtn').css('color', '#00bcd4');
        $('.wT .wizardtd .spanfpbtn').css('color', '#00bcd4');
        $('.wT .wizardtd .spansignbtn').css('color', '#00bcd4');
        $('.wT .wizardtd .spansummarybtn').css('color', '#00bcd4');
        this.isEnrolledWizardTab = true;
    }

    setWizardColorForEnrollmentInProgress() {
        if (this.enrollmentStepsStatus !== undefined) {
            if (this.enrollmentStepsStatus.idproofStatus === 1) {
                $('.wT .wizardtd .documentbtn').attr('disabled', false);
                $('.wT .wizardtd .documentbtn').css('border', '4px solid #00bcd4');
                $('.wT .wizardtd .spandocumentbtn').css('color', '#00bcd4');
            }
            if (this.enrollmentStepsStatus.registrationFormStatus === 1) {
                $('.wT .wizardtd .infobtn').attr('disabled', false);
                $('.wT .wizardtd .infobtn').css('border', '4px solid #00bcd4');
                $('.wT .wizardtd .spaninfobtn').css('color', '#00bcd4');
            }
            if (this.enrollmentStepsStatus.faceStatus === 1) {
                $('.wT .wizardtd .facebtn').attr('disabled', false);
                $('.wT .wizardtd .facebtn').css('border', '4px solid #00bcd4');
                $('.wT .wizardtd .spanfacebtn').css('color', '#00bcd4');
            }
            if (this.enrollmentStepsStatus.irisStatus === 1) {
                $('.wT .wizardtd .irisbtn').attr('disabled', false);
                $('.wT .wizardtd .irisbtn').css('border', '4px solid #00bcd4');
                $('.wT .wizardtd .spanirisbtn').css('color', '#00bcd4');
            }
            if (this.enrollmentStepsStatus.rollFingerprintStatus === 1) {
                $('.wT .wizardtd .fpRollbtn').attr('disabled', false);
                $('.wT .wizardtd .fpRollbtn').css('border', '4px solid #00bcd4');
                $('.wT .wizardtd .spanfpRollbtn').css('color', '#00bcd4');
            }
            if (this.enrollmentStepsStatus.fingerprintStatus === 1) {
                $('.wT .wizardtd .fpbtn').attr('disabled', false);
                $('.wT .wizardtd .fpbtn').css('border', '4px solid #00bcd4');
                $('.wT .wizardtd .spanfpbtn').css('color', '#00bcd4');
            }
            if (this.enrollmentStepsStatus.signatureStatus === 1) {
                $('.wT .wizardtd .signbtn').attr('disabled', false);
                $('.wT .wizardtd .signbtn').css('border', '4px solid #00bcd4');
                $('.wT .wizardtd .spansignbtn').css('color', '#00bcd4');
            }
        }
    }

    getIsEnrolledWizardTab() {
        return this.isEnrolledWizardTab;
    }
    setFaceImageForLdapUser(userBiometric: UserBiometric) {
        this.faceCaptureCalledInEdit = true;
        this.faceCaptureError = false;
        this.cameraAccessError = false;
        this.showInitialFaceCaptureMsg = true;
        this.capturedImg = 'data:image/png;base64,' + atob(userBiometric.data);
        this.uploadedFaceImage = this.capturedImg;
        this.previewUploadedFaceImage();
    }
    onFocus() {
        this.placeholder = '';
    }
    onBlur(event) {
        this.placeholder = 'mm/dd/yyyy';
        if (event.target.value === '') {
            this.placeholder = 'mm/dd/yyyy';
        } else {
            this.placeholder = '';
        }
    }
    onDateInput(event: MatDatepickerInputEvent<Date>) {
        if (event.value === null) {
            this.placeholder = 'mm/dd/yyyy';
        } else {
            this.placeholder = '';
        }
    }
    getConnectedCameraDevices() {
        this.faceCaptureError = false;
        this.cameraAccessError = false;
        this.isCaptured = false;
        WebcamUtil.getAvailableVideoInputs().then(
            (mediaDevicesId: MediaDeviceInfo[]) => {
                this.multipleWebcamsAvailable = mediaDevicesId && mediaDevicesId.length > 1;
                this.availableMediaDevicesId = mediaDevicesId;
                if (this.multipleWebcamsAvailable) {
                    this.allowCameraSwitch = true;
                }
                this.deviceId = mediaDevicesId[0].deviceId;
                console.log(this.deviceId, this.cameraAccessError);
            },
            err => {
                this.isCaptured = false;
            }
        );
        setTimeout(() => {
            if (!this.cameraAccessError && !this.cameraAccessError) {
                this.showInitialFaceCaptureMsg = true;
            }
        }, 2000);
    }
    getConnectedCameraDevicesForIdProof() {
        this.idProofCamAccessError = false;
        WebcamUtil.getAvailableVideoInputs().then(
            (mediaDevicesId: MediaDeviceInfo[]) => {
                this.multipleWebcamsAvailable = mediaDevicesId && mediaDevicesId.length > 1;
                this.availableMediaDevicesId = mediaDevicesId;
                if (this.multipleWebcamsAvailable) {
                    this.allowCameraSwitchIDProof = true;
                }
                this.deviceId = mediaDevicesId[0].deviceId;
                console.log(this.deviceId, this.cameraAccessError);
            },
            err => {}
        );
        setTimeout(() => {
            if (!this.idProofCamAccessError) {
                console.log('insdie id proof cam issue = false');
            }
        }, 2000);
    }
    setFaceCropOptions() {
        if (+this.cropFaceWidth === +this.cropFaceHeight) {
            this.FaceCropperOptions = {
                aspectRatio: 1 / 1,
                dragMode: 'move',
                cropBoxResizable: false,
                toggleDragModeOnDblclick: false,
                minCropBoxWidth: 200,
                minCropBoxHeight: 200,
                zoomOnWheel: true,
                preview: '.img-preview'
            };
        } else if (+this.cropFaceWidth === 250 && +this.cropFaceHeight === 300) {
            this.FaceCropperOptions = {
                aspectRatio: 2 / 3,
                dragMode: 'move',
                cropBoxResizable: false,
                toggleDragModeOnDblclick: false,
                minCropBoxWidth: 200,
                minCropBoxHeight: 200,
                zoomOnWheel: true,
                preview: '.img-preview'
            };
        } else if (+this.cropFaceWidth === 300 && +this.cropFaceHeight === 600) {
            this.FaceCropperOptions = {
                aspectRatio: 1 / 2,
                dragMode: 'move',
                cropBoxResizable: false,
                toggleDragModeOnDblclick: false,
                minCropBoxWidth: 200,
                minCropBoxHeight: 200,
                zoomOnWheel: true,
                preview: '.img-preview'
            };
        }
    }
    buidIdProofForm() {
        this.IDProofDocForm = this.formBuilder.group({
            idProofDocType: ['', Validators.required],
            issuingAuthority: ['', Validators.required],
            source: ['', Validators.required],
            selectedWebCamDevice: [''],
            frontImg: [],
            backImg: [],
            file: [],
            pdfFile: [],
            // fileName: [],
            frontFileName: [],
            backFileName: [],
            frontFileType: [],
            backFileType: [],
            imgURL: []
        });
    }
    ngAfterViewInit() {
        if (this.signaturePad !== undefined) {
            this.signaturePad.clear();
        }
    }

    async loadUserDetails() {
        this.data = this.dataService.getCurrentObj();
        if (this.data !== null) {
            this.userToEnroll = this.data.userName;
            const userInfo: UserInfo = new UserInfo();
            userInfo.username = this.userToEnroll;
            userInfo.workflow = await this.enrollService.getWorkflowByUsername(this.userToEnroll).toPromise();
            userInfo.workflowSteps = [];
            const workflowSteps: any[] = await this.enrollService.getWorkflowStepsByWorkflow(userInfo.workflow).toPromise();
            workflowSteps.forEach(step => {
                userInfo.workflowSteps.push(step.name);
            });
            this.userToBeEnrolled = userInfo;
        } else {
            this.userToBeEnrolled = this.principal.getLoggedInUser();
        }
    }
    changeValidations() {
        Object.keys(this.enrollForm.controls).forEach(fieldName => {
            const field = this.enrollForm.controls[fieldName];
            const regField = this.registrationStepDetail.wfStepRegistrationConfigs.find(e => e.id === +fieldName);
            if (regField !== undefined) {
                const regFieldName = regField.fieldName;
                const required = regField.required;
                if (
                    regFieldName === 'Given Name (First & Last Name)' ||
                    regFieldName === 'Primary Doctor' ||
                    regFieldName === 'Primary Subscriber'
                ) {
                    field.clearValidators();
                    if (required) {
                        field.setValidators([Validators.required, this.validations.specialCharsNotAllowedValidator]);
                    } else {
                        field.setValidators([this.validations.specialCharsNotAllowedValidator]);
                    }
                } else if (
                    regFieldName === 'Policy Number' ||
                    regFieldName === 'Health Plan Number' ||
                    regFieldName === 'Subscriber ID' ||
                    regFieldName === 'Personal Code'
                ) {
                    field.clearValidators();
                    //  'Special characters are not allowed.' (alphanumeric with space)
                    if (required) {
                        field.setValidators([Validators.required, this.validations.specialCharsNotAllowedValidator]);
                    } else {
                        field.setValidators([this.validations.specialCharsNotAllowedValidator]);
                    }
                } else if (
                    regFieldName === 'Middle Name' ||
                    regFieldName === 'Surname/Family Name' ||
                    regFieldName === 'Nationality' ||
                    regFieldName === 'Place of Birth'
                ) {
                    field.clearValidators();
                    if (required) {
                        field.setValidators([Validators.required, this.validations.specialCharsAndNumbersNotAllowedValidator]);
                    } else {
                        field.setValidators([this.validations.specialCharsAndNumbersNotAllowedValidator]);
                    }
                } else if (regFieldName === 'City' || regFieldName === 'State' || regFieldName === 'Country') {
                    field.clearValidators();
                    if (required) {
                        field.setValidators([
                            Validators.required,
                            Validators.maxLength(30),
                            this.validations.specialCharsAndNumbersNotAllowedValidator
                        ]);
                    } else {
                        field.setValidators([Validators.maxLength(30), this.validations.specialCharsAndNumbersNotAllowedValidator]);
                    }
                }
                field.updateValueAndValidity();
            }
        });
    }

    addFormControl(registrationField: WFStepRegistrationConfig): FormControl {
        const specialCharsNotAllowedValidator = this.validations.specialCharsNotAllowedValidator;
        const specialCharsAllowedValidator = this.validations.specialCharsAllowedValidator;
        const specialCharsAndNumbersNotAllowedValidator = this.validations.specialCharsAndNumbersNotAllowedValidator;
        const fildId = registrationField.id;
        const fieldName = registrationField.fieldName;
        const required = registrationField.required;
        if (fieldName === 'Given Name (First & Last Name)' || fieldName === 'Primary Doctor' || fieldName === 'Primary Subscriber') {
            this.setPatternValidationErrosMsgs(fildId, 'enroll.messages.validations.specialCharsNotAllowed');
            // Special characters are not allowed.
            if (required) {
                return new FormControl('', [Validators.required, specialCharsNotAllowedValidator]);
            } else {
                return new FormControl('', [specialCharsNotAllowedValidator]);
            }
        } else if (fieldName === 'Contact Number' || fieldName === 'Primary Doctor Phone') {
            this.setPatternValidationErrosMsgs(fildId, 'enroll.messages.validations.numbersAllowed');
            // Only numbers are allowed.
            if (required) {
                return new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]);
            } else {
                return new FormControl('', [Validators.pattern('^[0-9]*$')]);
            }
        } else if (
            fieldName === 'Policy Number' ||
            fieldName === 'Health Plan Number' ||
            fieldName === 'Subscriber ID' ||
            fieldName === 'Personal Code'
        ) {
            this.setPatternValidationErrosMsgs(fildId, 'enroll.messages.validations.specialCharsNotAllowed');
            //  'Special characters are not allowed.' (alphanumeric with space)
            if (required) {
                return new FormControl('', [Validators.required, specialCharsNotAllowedValidator]);
            } else {
                return new FormControl('', [specialCharsNotAllowedValidator]);
            }
        } else if (fieldName === 'Email') {
            // 'Email not valid'
            this.setPatternValidationErrosMsgs(fildId, 'onboarding.errors.emailNotValid');
            if (required) {
                return new FormControl('', [
                    Validators.required,
                    Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')
                ]);
            } else {
                return new FormControl('', [
                    Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')
                ]);
            }
        } else if (
            fieldName === 'Middle Name' ||
            fieldName === 'Surname/Family Name' ||
            fieldName === 'Nationality' ||
            fieldName === 'Place of Birth'
        ) {
            this.setPatternValidationErrosMsgs(fildId, 'enroll.messages.validations.specialCharsAndNumbrsNotAllowed');
            // 'Special characters and numbers are not allowed'
            if (required) {
                return new FormControl('', [Validators.required, specialCharsAndNumbersNotAllowedValidator]);
            } else {
                return new FormControl('', [specialCharsAndNumbersNotAllowedValidator]);
            }
        } else if (
            fieldName === 'Address Line1' ||
            fieldName === 'Address Line2' ||
            fieldName === 'City' ||
            fieldName === 'State' ||
            fieldName === 'Country' ||
            fieldName === 'ZIP/Postal Code'
        ) {
            registrationField.fieldMaxSize = 30;
            if (fieldName === 'City' || fieldName === 'State' || fieldName === 'Country') {
                this.setPatternValidationErrosMsgs(fildId, 'enroll.messages.validations.specialCharsAndNumbrsNotAllowed');
                if (required) {
                    return new FormControl('', [Validators.required, Validators.maxLength(30), specialCharsAndNumbersNotAllowedValidator]);
                } else {
                    return new FormControl('', [Validators.maxLength(30), specialCharsAndNumbersNotAllowedValidator]);
                }
            }
            if (fieldName === 'ZIP/Postal Code') {
                this.setPatternValidationErrosMsgs(fildId, 'enroll.messages.validations.specialCharsNotAllowed');
                // Special characters are not allowed.
                if (required) {
                    return new FormControl('', [Validators.required, Validators.maxLength(30), Validators.pattern('^[a-zA-Z0-9]+$')]);
                } else {
                    return new FormControl('', [Validators.maxLength(30), Validators.pattern('^[a-zA-Z0-9]+$')]);
                }
            }
            if (fieldName === 'Address Line1' || fieldName === 'Address Line2') {
                this.setPatternValidationErrosMsgs(fildId, 'enroll.messages.validations.allowedCharacters');
                //Allowed special characters are hash(#), hyphen(-), dot, comma, space, braces and forward slash.
                if (required) {
                    return new FormControl('', [Validators.required, Validators.maxLength(50), specialCharsAllowedValidator]);
                } else {
                    return new FormControl('', [Validators.maxLength(50), specialCharsAllowedValidator]);
                }
            } else {
                if (required) {
                    return new FormControl('', [Validators.required, Validators.maxLength(30)]);
                } else {
                    return new FormControl('', [Validators.maxLength(30)]);
                }
            }
        } else if (required) {
            return new FormControl('', Validators.required);
        } else {
            return new FormControl('');
        }
    }

    setPatternValidationErrosMsgs(regFieldId, msg) {
        this.regFormFieldsValidations.push({ id: regFieldId, message: msg });
    }
    getValidationsMsgByFieldId(regFieldId) {
        let msg = '';
        const field = this.regFormFieldsValidations.find(e => e.id === regFieldId);
        if (field !== undefined) {
            msg = field.message;
        }
        return msg;
    }
    createForm() {
        if (this.registrationStepDetail === undefined) {
            this.enrollService
                .getWorkflowStepDetail('REGISTRATION', this.getUserToBeEnrolled().workflow)
                .subscribe((registrationStepDetail: WorkflowStep) => {
                    this.registrationStepDetail = registrationStepDetail;
                    // alert(registrationStepDetail); alert(this.registrationStepDetail.wfStepRegistrationConfigs);
                    if (this.registrationStepDetail.wfStepRegistrationConfigs) {
                        this.registrationStepDetail.wfStepRegistrationConfigs.forEach(registrationField => {
                            this.enrollForm.addControl('' + registrationField.id, this.addFormControl(registrationField));
                            if (registrationField.fieldName === 'Contact Number') {
                                this.enrollForm.addControl(
                                    'countryCode',
                                    new FormControl('', [Validators.required, Validators.pattern('^[0-9 +]+$'), Validators.maxLength(4)])
                                );
                            }
                        });
                    }
                    if (this.isEnrolledUser) {
                        this.setUserDetails(
                            this.user,
                            this.user.userAttribute,
                            this.user.userAppearance,
                            this.user.userPIV,
                            this.user.userDrivingLicense,
                            this.user.userHealthService,
                            // this.user.userHealthService.userHealthServiceVaccineInfos,
                            null,
                            this.user.userNationalID,
                            this.user.userPermanentResidentID,
                            this.user.userEmployeeID,
                            this.user.userStudentID,
                            this.user.userHealthID,
                            this.user.userAddress
                        );
                    } else {
                        this.setUserDetails(
                            this.user,
                            this.user.userAttribute,
                            this.user.userAppearance,
                            this.user.userPIV,
                            this.user.userDrivingLicense,
                            this.user.userHealthService,
                            null,
                            this.user.userNationalID,
                            this.user.userPermanentResidentID,
                            this.user.userEmployeeID,
                            this.user.userStudentID,
                            this.user.userHealthID,
                            this.user.userAddress
                        );
                    }
                });
            this.isFormCreated = true;
        }
    }

    setUserDetails(
        user: User,
        userAttribute: UserAttribute,
        userAppearance: UserAppearance,
        userPIV: UserPIV,
        userDrivingLicense: UserDrivingLicense,
        userHealthService: UserHealthService,
        userHealthServiceVaccineInfos: UserHealthServiceVaccineInfo[],
        userNationalID: UserNationalID,
        userPermanentResidentID: UserPermanentResidentID,
        userEmployeeID: UserEmployeeID,
        userStudentID: UserStudentID,
        userHealthID: UserHealthID,
        userAddress: UserAddress
    ) {
        // add more user details from Ldap
        this.registrationStepDetail.wfStepRegistrationConfigs.forEach(registrationField => {
            switch (registrationField.fieldName) {
                case 'Given Name (First & Last Name)': {
                    this.enrollForm.controls[registrationField.id].setValue(userAttribute.firstName);
                    break;
                }
                case 'Middle Name': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.middleName);
                    }
                    break;
                }
                case 'Surname/Family Name': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.lastName);
                    }
                    break;
                }
                case 'Mother’s Maiden Name': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.mothersMaidenName);
                    }
                    break;
                }
                case 'Date of Birth': {
                    if (userAttribute !== null) {
                        if (this.isEnrolledUser || this.isEnrollmentInProgress) {
                            this.placeholder = '';
                        }
                        if (userAttribute.dateOfBirth !== null && userAttribute.dateOfBirth !== undefined) {
                            this.enrollForm.controls[registrationField.id].setValue(new Date(userAttribute.dateOfBirth));
                        }
                    }
                    break;
                }
                case 'Birth Place/ Nationality': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.nationality);
                    }
                    break;
                }
                case 'Nationality': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.nationality);
                    }
                    break;
                }
                case 'Place of Birth': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.placeOfBirth);
                    }
                    break;
                }
                case 'Personal Code': {
                    if (userNationalID !== null && this.workflow.organizationIdentities.identityTypeName === IdentityTypeEnum.NATIONAL_ID) {
                        this.enrollForm.controls[registrationField.id].setValue(userNationalID.personalCode);
                    }
                    if (
                        userPermanentResidentID !== null &&
                        this.workflow.organizationIdentities.identityTypeName === IdentityTypeEnum.PERMANENT_RESIDENT_ID
                    ) {
                        this.enrollForm.controls[registrationField.id].setValue(userPermanentResidentID.personalCode);
                    }
                    break;
                }
                case 'Passport Number': {
                    if (userPermanentResidentID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userPermanentResidentID.passportNumber);
                    }
                    break;
                }
                case 'Resident Card Type Number': {
                    if (userPermanentResidentID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userPermanentResidentID.residentcardTypeNumber);
                    }
                    break;
                }
                case 'Resident Card Type Code': {
                    if (userPermanentResidentID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userPermanentResidentID.residentcardTypeCode);
                    }
                    break;
                }
                case 'Vaccine Name': {
                    if (userNationalID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userNationalID.vaccineName);
                    }
                    break;
                }
                case 'Vaccination Date': {
                    if (userNationalID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userNationalID.vaccinationDate);
                    }
                    break;
                }
                case 'Vaccination Location Name': {
                    if (userNationalID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userNationalID.vaccinationLocationName);
                    }
                    break;
                }
                case 'Gender': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.gender);
                    }
                    break;
                }
                case 'Blood Type': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.bloodType);
                    }
                    break;
                }
                case 'Address': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.address);
                    }
                    break;
                }
                case 'Email': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.email);
                    }
                    break;
                }
                case 'Contact Number': {
                    if (userAttribute !== null) {
                        if (parseInt(userAttribute.contact, 10) !== 0) {
                            this.enrollForm.controls[registrationField.id].setValue(userAttribute.contact);
                        }
                        if (userAttribute.countryCode !== null) {
                            this.enrollForm.controls.countryCode.setValue(userAttribute.countryCode);
                        }
                    }
                    break;
                }
                case 'Organ Donor': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.organDonor);
                    }
                    break;
                }
                case 'Veteran': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.veteran);
                    }
                    break;
                }
                case 'Hair Color': {
                    if (userAppearance !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAppearance.hairColor);
                    }
                    break;
                }
                case 'Eye Color': {
                    if (userAppearance !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAppearance.eyeColor);
                    }
                    break;
                }
                case 'Height': {
                    if (userAppearance !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAppearance.height);
                    }
                    break;
                }
                case 'Weight': {
                    if (userAppearance !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAppearance.weight);
                    }
                    break;
                }
                case 'Address Line1': {
                    if (userAddress !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAddress.addressLine1);
                    }
                    break;
                }
                case 'Address Line2': {
                    if (userAddress !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAddress.addressLine2);
                    }
                    break;
                }
                case 'City': {
                    if (userAddress !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAddress.city);
                    }
                    break;
                }
                case 'State': {
                    if (userAddress !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAddress.state);
                    }
                    break;
                }
                case 'Country': {
                    if (userAddress !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAddress.country);
                    }
                    break;
                }
                case 'ZIP/Postal Code': {
                    if (userAddress !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAddress.zipCode);
                    }
                    break;
                }
                case 'Rank/Grade/Employee Status': {
                    if (userPIV !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userPIV.rank);
                    }
                    break;
                }
                case 'Employee Affiliation': {
                    if (userPIV !== null && this.workflow.organizationIdentities.identityTypeName === IdentityTypeEnum.PIV) {
                        this.enrollForm.controls[registrationField.id].setValue(userPIV.employeeAffiliation);
                    } else if (
                        userEmployeeID !== null &&
                        this.workflow.organizationIdentities.identityTypeName === IdentityTypeEnum.EMPLOYEE_ID
                    ) {
                        this.enrollForm.controls[registrationField.id].setValue(userEmployeeID.affiliation);
                    }
                    break;
                }
                case 'Employee Affiliation Color Code': {
                    if (userPIV !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userPIV.employeeAffiliationColorCode);
                    }
                    break;
                }
                case 'Restrictions': {
                    if (userDrivingLicense !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userDrivingLicense.restrictions);
                    }
                    break;
                }
                case 'Vehicle Classification': {
                    if (userDrivingLicense !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userDrivingLicense.vehicleClass);
                    }
                    break;
                }
                case 'Endorsements': {
                    if (userDrivingLicense !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userDrivingLicense.endorsements);
                    }
                    break;
                }
                case 'Donor': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.organDonor);
                    }
                    break;
                }
                case 'Veteran': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.veteran);
                    }
                    break;
                }
                case 'Department': {
                    if (userStudentID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userStudentID.department);
                    } else if (
                        userEmployeeID !== null &&
                        this.workflow.organizationIdentities.identityTypeName === IdentityTypeEnum.EMPLOYEE_ID
                    ) {
                        this.enrollForm.controls[registrationField.id].setValue(userEmployeeID.department);
                    }
                    break;
                }
                case 'Primary Subscriber': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.primarySubscriber);
                    }
                    break;
                }
                case 'Subscriber ID': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.primarySubscriberID);
                    }
                    break;
                }
                case 'Primary Doctor': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.pcp);
                    }
                    break;
                }
                case 'Primary Doctor Phone': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.pcpPhone);
                    }
                    break;
                }
                case 'Policy Number': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.policyNumber);
                    }
                    break;
                }
                case 'Group Plan': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.groupPlan);
                    }
                    break;
                }
                case 'Health Plan Number': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.healthPlanNumber);
                    }
                    break;
                }
                case 'Vaccine': {
                    if (userHealthServiceVaccineInfos !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthServiceVaccineInfos[0].vaccine);
                    }
                    break;
                }
                case 'Route': {
                    if (userHealthServiceVaccineInfos !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthServiceVaccineInfos[0].route);
                    }
                    break;
                }
                case 'Site': {
                    if (userHealthServiceVaccineInfos !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthServiceVaccineInfos[0].site);
                    }
                    break;
                }
                case 'Date': {
                    if (userHealthServiceVaccineInfos !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(new Date(userHealthServiceVaccineInfos[0].date));
                    }
                    break;
                }
                case 'Administered By': {
                    if (userHealthServiceVaccineInfos !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthServiceVaccineInfos[0].administeredBy);
                    }
                    break;
                }
            }
        });
    }
    setIdProofDocuments(userIDProofArray: UserIDProof[]) {
        // if (this.isEnrolledUser && this.isDocumentsLoaded) {
        if ((this.isEnrolledUser || this.isEnrollmentInProgress) && userIDProofArray.length > 0) {
            this.user.userIDProofDocuments = userIDProofArray;
            userIDProofArray.forEach(userDocument => {
                // userDocument.fileType = '';
                if (userDocument.frontFileName !== null || userDocument.backFileName !== null) {
                    userDocument.source = 'upload';
                    if (userDocument.frontImg !== null) {
                        userDocument.frontImg = `data:${userDocument.frontFileType};base64,` + userDocument.frontImg;
                    }
                    if (userDocument.backImg !== null) {
                        userDocument.backImg = `data:${userDocument.backFileType};base64,` + userDocument.backImg;
                    }
                } else {
                    userDocument.source = 'capture';
                    if (userDocument.frontImg !== null) {
                        userDocument.frontImg = `data:${userDocument.frontFileType};base64,` + userDocument.frontImg;
                    }
                    if (userDocument.backImg !== null) {
                        userDocument.backImg = `data:${userDocument.backFileType};base64,` + userDocument.backImg;
                    }
                }
                if (userDocument.file !== null) {
                    userDocument.source = 'upload';
                    const file = `data:${userDocument.frontFileType};base64,` + userDocument.file;
                    // userDocument.fileName = userDocument.name;
                    userDocument.file = file;
                }
                const userIDProof = new UserIDProof();
                userIDProof.docId = ++this.docId;
                userIDProof.id = userDocument.id;
                userIDProof.idProofTypeName = userDocument.idProofType.identityTypeName;
                userIDProof.idProofTypeId = userDocument.idProofType.id;
                userIDProof.idProofType = userDocument.idProofType;
                userIDProof.issuingAuthorityId = userDocument.idProofIssuingAuthority.id;
                userIDProof.issuingAuthority = userDocument.idProofIssuingAuthority.issuingAuthority;
                userIDProof.idProofIssuingAuthority = userDocument.idProofIssuingAuthority;
                userIDProof.name = userDocument.name;
                userIDProof.frontImg = userDocument.frontImg;
                userIDProof.backImg = userDocument.backImg;
                userIDProof.source = userDocument.source;
                userIDProof.file = userDocument.file;
                userIDProof.pdfFile = userDocument.file;
                userIDProof.frontFileType = userDocument.frontFileType;
                userIDProof.backFileType = userDocument.backFileType;
                //  userIDProof.fileName = userDocument.fileName;
                userIDProof.frontFileName = userDocument.frontFileName;
                userIDProof.backFileName = userDocument.backFileName;
                this.userIDProofList.push(userIDProof);
                this.dataSource = new MatTableDataSource<UserIDProof>(this.userIDProofList);
                this.dataSource.sort = this.sort;
            });
            this.captureDocument = false;
        }
    }
    async setBiometrics(biometrics: UserBiometric[]) {
        if (biometrics !== null) {
            let setFPdetails = false;
            let setRollFpDetails = false;

            if (
                this.getUserToBeEnrolled().workflowSteps.find(e => e === 'FINGERPRINT_CAPTURE') !== undefined &&
                biometrics.filter(e => e.type === 'FINGERPRINT').length !== 0
            ) {
                // it means FP step is there and set the captured fps
                setFPdetails = true;
                this.isClearFpEnabled = true;
                this.disableFpDeviceSelection = false;
                await this.getFPStepDetails();
            }
            if (
                this.getUserToBeEnrolled().workflowSteps.find(e => e === 'FINGERPRINT_ROLL_CAPTURE') !== undefined &&
                biometrics.filter(e => e.type === 'ROLL_FINGERPRINT').length !== 0
            ) {
                // it means FP step is there and set the captured fps
                setRollFpDetails = true;
                this.isClearRollFpEnabled = true;
                this.disableRollFpDeviceSelection = false;
                await this.getFPStepDetails();
            }
            biometrics.forEach(userBiometric => {
                switch (userBiometric.type) {
                    case 'FACE': {
                        if (this.getUserToBeEnrolled().workflowSteps.find(e => e === 'FACE_CAPTURE') !== undefined) {
                            this.getFaceCaptureStepDetails(true, userBiometric);
                            this.isCaptured = true;
                            this.isCropped = true;
                            this.croppedImg = `data:image/${userBiometric.format};base64,` + userBiometric.data;
                            this.capturedImg = `data:image/${userBiometric.format};base64,` + userBiometric.data;
                            this.webcamImage = new WebcamImage(this.croppedImg, `image/${userBiometric.format}`);
                        }
                        break;
                    }
                    case 'IRIS': {
                        if (this.getUserToBeEnrolled().workflowSteps.find(e => e === 'IRIS_CAPTURE') !== undefined) {
                            if (userBiometric.position === 'LEFT_EYE') {
                                this.leftEye = userBiometric.data;
                            } else if (userBiometric.position === 'RIGHT_EYE') {
                                this.rightEye = userBiometric.data;
                            }
                        }
                        break;
                    }
                    case 'SIGNATURE': {
                        if (this.getUserToBeEnrolled().workflowSteps.find(e => e === 'SIGNATURE_CAPTURE') !== undefined) {
                            // this.signature = atob(userBiometric.data);
                            this.signature = `data:image/${userBiometric.format};base64,` + userBiometric.data;
                        }
                        break;
                    }
                    case 'ROLL_FINGERPRINT': {
                        if (setRollFpDetails) {
                            this.minRequiredFingerPositions = this.fingerPositions;
                            this.setRollFingerPrints(userBiometric);
                        }
                        break;
                    }
                    case 'FINGERPRINT': {
                        if (setFPdetails) {
                            this.setFingerPrints(userBiometric);
                        }
                        break;
                    }
                }
            });
            if (setFPdetails) {
                // it means min required fingers was changed in workflow
                if (this.fingerprintCapturedPositions.length < this.minRequiredFingerPositions.length) {
                    this.newlyAddedFpsCapturePositions = this.minRequiredFingerPositions.filter(
                        e1 => !this.fingerprintCapturedPositions.find(e2 => e2 === e1)
                    );
                } else {
                    this.isFpAutoCaptureCompleted = true;
                }
            }
        }
    }
    setSiganture() {
        const userBiometricSignature: UserBiometric = this.user.userBiometrics.find(e => e.type === 'SIGNATURE');
        if (userBiometricSignature !== undefined) {
            if (this.getUserToBeEnrolled().workflowSteps.find(e => e === 'SIGNATURE_CAPTURE') !== undefined) {
                this.signature = 'data:image/png;base64,' + userBiometricSignature.data;
            }
        }
    }
    setFaceImage() {
        const userBiometricFace: UserBiometric = this.user.userBiometrics.find(e => e.type === 'FACE');
        if (this.getUserToBeEnrolled().workflowSteps.find(e => e === 'FACE_CAPTURE') !== undefined && userBiometricFace !== undefined) {
            this.getFaceCaptureStepDetails(this.isTransparentPhotoAllowed, userBiometricFace);
            this.isCaptured = true;
            this.isCropped = true;
            this.croppedImg = 'data:image/png;base64,' + userBiometricFace.data;
            this.capturedImg = 'data:image/png;base64,' + userBiometricFace.data;
        }
    }
    setRollFingerPrints(userBiometric: UserBiometric) {
        if (this.minRequiredFingerPositions.find(e => e === userBiometric.position) !== undefined) {
            if (userBiometric.position === 'LEFT_THUMB') {
                this.leftRollFingerprintImages[0] = userBiometric;
                this.leftRollFingerprintImages[0].jpegData = userBiometric.data;
                this.fingerprintCapturedPositionsInRollFinger.push(userBiometric.position);
            }
            if (userBiometric.position === 'LEFT_INDEX') {
                this.leftRollFingerprintImages[1] = userBiometric;
                this.leftRollFingerprintImages[1].jpegData = userBiometric.data;
                this.fingerprintCapturedPositionsInRollFinger.push(userBiometric.position);
            }
            if (userBiometric.position === 'LEFT_MIDDLE') {
                this.leftRollFingerprintImages[2] = userBiometric;
                this.leftRollFingerprintImages[2].jpegData = userBiometric.data;
                this.fingerprintCapturedPositionsInRollFinger.push(userBiometric.position);
            }
            if (userBiometric.position === 'LEFT_RING') {
                this.leftRollFingerprintImages[3] = userBiometric;
                this.leftRollFingerprintImages[3].jpegData = userBiometric.data;
                this.fingerprintCapturedPositionsInRollFinger.push(userBiometric.position);
            }
            if (userBiometric.position === 'LEFT_LITTLE') {
                this.leftRollFingerprintImages[4] = userBiometric;
                this.leftRollFingerprintImages[4].jpegData = userBiometric.data;
                this.fingerprintCapturedPositionsInRollFinger.push(userBiometric.position);
            }
            if (userBiometric.position === 'RIGHT_THUMB') {
                this.rightRollFingerprintImages[0] = userBiometric;
                this.rightRollFingerprintImages[0].jpegData = userBiometric.data;
                this.fingerprintCapturedPositionsInRollFinger.push(userBiometric.position);
            }
            if (userBiometric.position === 'RIGHT_INDEX') {
                this.rightRollFingerprintImages[1] = userBiometric;
                this.rightRollFingerprintImages[1].jpegData = userBiometric.data;
                this.fingerprintCapturedPositionsInRollFinger.push(userBiometric.position);
            }
            if (userBiometric.position === 'RIGHT_MIDDLE') {
                this.rightRollFingerprintImages[2] = userBiometric;
                this.rightRollFingerprintImages[2].jpegData = userBiometric.data;
                this.fingerprintCapturedPositionsInRollFinger.push(userBiometric.position);
            }
            if (userBiometric.position === 'RIGHT_RING') {
                this.rightRollFingerprintImages[3] = userBiometric;
                this.rightRollFingerprintImages[3].jpegData = userBiometric.data;
                this.fingerprintCapturedPositionsInRollFinger.push(userBiometric.position);
            }
            if (userBiometric.position === 'RIGHT_LITTLE') {
                this.rightRollFingerprintImages[4] = userBiometric;
                this.rightRollFingerprintImages[4].jpegData = userBiometric.data;
                this.fingerprintCapturedPositionsInRollFinger.push(userBiometric.position);
            }
        }
        this.isRollFpAutoCaptureCompleted = true;
    }
    setFingerPrints(userBiometric: UserBiometric) {
        if (this.minRequiredFingerPositions.find(e => e === userBiometric.position) !== undefined) {
            if (userBiometric.position === 'LEFT_THUMB') {
                this.leftFingerprintImages[0] = userBiometric;
                this.leftFingerprintImages[0].jpegData = userBiometric.data;
                this.leftFingerprintImages[0].wsqData = userBiometric.wsqData;
                this.fingerprintCapturedPositions.push(userBiometric.position);
            }
            if (userBiometric.position === 'LEFT_INDEX') {
                this.leftFingerprintImages[1] = userBiometric;
                this.leftFingerprintImages[1].jpegData = userBiometric.data;
                this.leftFingerprintImages[1].wsqData = userBiometric.wsqData;
                this.fingerprintCapturedPositions.push(userBiometric.position);
            }
            if (userBiometric.position === 'LEFT_MIDDLE') {
                this.leftFingerprintImages[2] = userBiometric;
                this.leftFingerprintImages[2].jpegData = userBiometric.data;
                this.leftFingerprintImages[2].wsqData = userBiometric.wsqData;
                this.fingerprintCapturedPositions.push(userBiometric.position);
            }
            if (userBiometric.position === 'LEFT_RING') {
                this.leftFingerprintImages[3] = userBiometric;
                this.leftFingerprintImages[3].jpegData = userBiometric.data;
                this.leftFingerprintImages[3].wsqData = userBiometric.wsqData;
                this.fingerprintCapturedPositions.push(userBiometric.position);
            }
            if (userBiometric.position === 'LEFT_LITTLE') {
                this.leftFingerprintImages[4] = userBiometric;
                this.leftFingerprintImages[4].jpegData = userBiometric.data;
                this.leftFingerprintImages[4].wsqData = userBiometric.wsqData;
                this.fingerprintCapturedPositions.push(userBiometric.position);
            }
            if (userBiometric.position === 'RIGHT_THUMB') {
                this.rightFingerprintImages[0] = userBiometric;
                this.rightFingerprintImages[0].jpegData = userBiometric.data;
                this.rightFingerprintImages[0].wsqData = userBiometric.wsqData;
                this.fingerprintCapturedPositions.push(userBiometric.position);
            }
            if (userBiometric.position === 'RIGHT_INDEX') {
                this.rightFingerprintImages[1] = userBiometric;
                this.rightFingerprintImages[1].jpegData = userBiometric.data;
                this.rightFingerprintImages[1].wsqData = userBiometric.wsqData;
                this.fingerprintCapturedPositions.push(userBiometric.position);
            }
            if (userBiometric.position === 'RIGHT_MIDDLE') {
                this.rightFingerprintImages[2] = userBiometric;
                this.rightFingerprintImages[2].jpegData = userBiometric.data;
                this.rightFingerprintImages[2].wsqData = userBiometric.wsqData;
                this.fingerprintCapturedPositions.push(userBiometric.position);
            }
            if (userBiometric.position === 'RIGHT_RING') {
                this.rightFingerprintImages[3] = userBiometric;
                this.rightFingerprintImages[3].jpegData = userBiometric.data;
                this.rightFingerprintImages[3].wsqData = userBiometric.wsqData;
                this.fingerprintCapturedPositions.push(userBiometric.position);
            }
            if (userBiometric.position === 'RIGHT_LITTLE') {
                this.rightFingerprintImages[4] = userBiometric;
                this.rightFingerprintImages[4].jpegData = userBiometric.data;
                this.rightFingerprintImages[4].wsqData = userBiometric.wsqData;
                this.fingerprintCapturedPositions.push(userBiometric.position);
            }
            if (userBiometric.position === 'FLAT_SLAP_4_LEFT') {
                this.isSlapFPCaptured = true;
                this.isSingleFPCaptured = false;
                this.leftFourFingers = userBiometric;
                this.leftFourFingers.jpegData = userBiometric.data;
            }
            if (userBiometric.position === 'FLAT_SLAP_4_RIGHT') {
                this.isSlapFPCaptured = true;
                this.isSingleFPCaptured = false;
                this.rightFourFingers = userBiometric;
                this.rightFourFingers.jpegData = userBiometric.data;
            }
            if (userBiometric.position === 'FLAT_SLAP_2_THUMBS') {
                this.isSlapFPCaptured = true;
                this.isSingleFPCaptured = false;
                this.thumbFingers = userBiometric;
                this.thumbFingers.jpegData = userBiometric.data;
            }
            /////// code for dual Fingerprints
            if (userBiometric.position === 'INDEX_MIDDLE_LEFT_2') {
                this.isTwoFPCaptured = true;
                this.isSlapFPCaptured = false;
                this.isSingleFPCaptured = false;
                this.indexMiddleLeft = userBiometric;
                this.indexMiddleLeft.jpegData = userBiometric.data;
            }
            if (userBiometric.position === 'RING_LITTLE_LEFT_2') {
                this.isTwoFPCaptured = true;
                this.isSlapFPCaptured = false;
                this.isSingleFPCaptured = false;
                this.ringLittleLeft = userBiometric;
                this.ringLittleLeft.jpegData = userBiometric.data;
            }
            if (userBiometric.position === 'INDEX_MIDDLE_RIGHT_2') {
                this.isTwoFPCaptured = true;
                this.isSlapFPCaptured = false;
                this.isSingleFPCaptured = false;
                this.indexMiddleRight = userBiometric;
                this.indexMiddleRight.jpegData = userBiometric.data;
            }
            if (userBiometric.position === 'RING_LITTLE_RIGHT_2') {
                this.isTwoFPCaptured = true;
                this.isSlapFPCaptured = false;
                this.isSingleFPCaptured = false;
                this.ringLittleRight = userBiometric;
                this.ringLittleRight.jpegData = userBiometric.data;
            }
            if (userBiometric.position === 'THUMBS_2') {
                this.isTwoFPCaptured = true;
                this.isSlapFPCaptured = false;
                this.isSingleFPCaptured = false;
                this.thumbsTwo = userBiometric;
                this.thumbsTwo.jpegData = userBiometric.data;
            }
        }
    }
    private loadWorkflowSteps() {
        this.workflowSteps = this.getUserToBeEnrolled().workflowSteps;
        // this.workflowSteps.splice(3, 1);
        this.a = this.workflowSteps.indexOf('ID_PROOFING');
        this.b = this.workflowSteps.indexOf('REGISTRATION');
        this.c = this.workflowSteps.indexOf('FACE_CAPTURE');
        this.d = this.workflowSteps.indexOf('IRIS_CAPTURE');
        this.e = this.workflowSteps.indexOf('FINGERPRINT_CAPTURE');
        this.f = this.workflowSteps.indexOf('FINGERPRINT_ROLL_CAPTURE');
        this.g = this.workflowSteps.indexOf('SIGNATURE_CAPTURE');
        this.h = this.workflowSteps.indexOf('ENROLL_SUMMARY');

        if (this.enrollmentSteps.includes(this.workflowSteps[0])) {
            this.firstStep = this.workflowSteps[0];
            if (this.enrollmentSteps.includes(this.workflowSteps[1])) {
                this.nextStep = this.workflowSteps[1];
            } else {
                this.nextStep = 'ENROLL_SUMMARY';
            }
        }

        // TODO Retrieve Min Fingers required from WorkflowStep
        this.previousSelectedWizardTab = this.firstStep;
        this.selectedWizardTab = this.firstStep;
        if (this.selectedWizardTab === 'ID_PROOFING') {
            // Pre-loading form if next step is registration, inoder to make enrolment for invalid
            // (since creating form with form controlnames and required fields will make form an invalid, else
            // a smple without controlnames and required fields is always valid)
            if (this.nextStep === 'REGISTRATION') {
                this.createForm();
            }
        }
        if (this.selectedWizardTab === 'REGISTRATION' || (this.b !== -1 && (this.isEnrolledUser || this.isEnrollmentInProgress))) {
            if (!this.isFormCreated) {
                this.createForm();
            }
        }
        if (this.isEnrolledUser) {
            this.setWizardColorForEnrolled();
        } else if (this.isEnrollmentInProgress) {
            this.setWizardColorForEnrollmentInProgress();
        } else {
            this.isEnrolledWizardTab = false;
        }
    }

    enrollmentInProgressWizard() {}

    async nextWizardTab(wizardTab: string, a: any, btnNext: any) {
        this.error = false;
        const returnval = await this.saveStepDetails(this.selectedWizardTab);
        // alert('nextWizardTab' + returnval);
        if (returnval === true) {
            if (this.isEnrollmentInProgress === false) {
                this.isEnrollmentInProgress = true;
            }
            this.previousSelectedWizardTab = this.selectedWizardTab;
            switch (btnNext) {
                case 'documentnext':
                    $('.wT .wizardtd .documentbtn').css('border', '4px solid #00bcd4');
                    // $('.wT .documentbtn .fas').css('color', '#00bcd4');
                    $('.wT .wizardtd .spandocumentbtn').css('color', '#00bcd4');
                    break;
                case 'infonext':
                    $('.wT .wizardtd .infobtn').css('border', '4px solid #00bcd4');
                    // $('.wT .infobtn .fas').css('color', '#00bcd4');
                    $('.wT .wizardtd .spaninfobtn').css('color', '#00bcd4');
                    break;
                case 'facenext':
                    $('.wT .wizardtd .facebtn').css('border', '4px solid #00bcd4');
                    // $('.wT .facebtn .material-icons').css('color', '#00bcd4');
                    $('.wT .wizardtd .spanfacebtn').css('color', '#00bcd4');
                    break;
                case 'irisnext':
                    $('.wT .wizardtd .irisbtn').css('border', '4px solid #00bcd4');
                    // $('.wT .irisbtn .material-icons').css('color', '#00bcd4');
                    $('.wT .wizardtd .spanirisbtn').css('color', '#00bcd4');
                    break;
                case 'rollfpnext':
                    $('.wT .wizardtd .fpRollbtn').css('border', '4px solid #00bcd4');
                    $('.wT .wizardtd .spanfpRollbtn').css('color', '#00bcd4');
                    break;
                case 'fpnext':
                    $('.wT .wizardtd .fpbtn').css('border', '4px solid #00bcd4');
                    // $('.wT .fpbtn .material-icons').css('color', '#00bcd4');
                    $('.wT .wizardtd .spanfpbtn').css('color', '#00bcd4');
                    break;
                case 'signnext':
                    $('.wT .wizardtd .signbtn').css('border', '4px solid #00bcd4');
                    // $('.wT .signbtn .fas').css('color', '#00bcd4');
                    $('.wT .wizardtd .spansignbtn').css('color', '#00bcd4');
                    break;
            }
            this.selectedWizardTab = wizardTab;
            this.resolvePrevAndNextWorkflowSteps();
            $('.navline').css('width', a);
        } else {
            this.error = true;
            this.errormessage = this.errormessage1;
            this.clickBackToFailedWizard();
        }
    }

    async previousWizardTab(wizardTab: string, a: any, btnBack: any) {
        this.error = false;
        const returnval = await this.saveStepDetails(this.selectedWizardTab);
        // alert('previousWizardTab' + returnval);
        if (returnval === true) {
            if (this.isEnrollmentInProgress === false) {
                this.isEnrollmentInProgress = true;
            }
            this.previousSelectedWizardTab = this.selectedWizardTab;
            switch (btnBack) {
                case 'summaryback':
                    $('.wT .wizardtd .summarybtn').css('border', '4px solid #00bcd4');
                    $('.wT .wizardtd .spansummarybtn').css('color', '#00bcd4');
                    // $('.wT .signbtn .far').css('color', '#00bcd4');
                    break;
                //     case 'signback':
                //         $('.summarybtn, .signbtn').css('border', '4px solid #e3e1e2');
                //         break;
                //     case 'irisback':
                //         $('.summarybtn, .signbtn, .irisbtn').css('border', '4px solid #e3e1e2');
                //         break;
                //     case 'fpback':
                //         $('.summarybtn, .signbtn, .irisbtn, .fpbtn').css('border', '4px solid #e3e1e2');
                //         break;
                //     case 'faceback':
                //         $('.summarybtn, .signbtn, .fpbtn, .facebtn').css('border', '4px solid #e3e1e2');
                //         break;
                //         case 'infoback':
                //         $('.summarybtn, .signbtn, .fpbtn, .facebtn, .infobtn').css('border', '4px solid #e3e1e2');
                //         break;
            }
            this.selectedWizardTab = wizardTab;
            this.resolvePrevAndNextWorkflowSteps();
            $('.navline').css('width', a);
        } else {
            this.error = true;
            this.errormessage = this.errormessage1;
            this.clickBackToFailedWizard();
        }
        this.resolvePrevAndNextWorkflowSteps();
    }

    async wizardTabClicked(wizardTab: string) {
        this.error = false;
        if (this.isClickBackToFailedWizardEnabled === false) {
            const returnval = await this.saveStepDetails(this.selectedWizardTab);
            // alert('nextWizardTab' + returnval);
            if (returnval === true) {
                if (this.isEnrollmentInProgress === false) {
                    this.isEnrollmentInProgress = true;
                }
                this.previousSelectedWizardTab = this.selectedWizardTab;
                this.selectedWizardTab = wizardTab;
                // alert(this.selectedWizardTab);
                this.resolvePrevAndNextWorkflowSteps();
                this.error = false;
            } else {
                this.error = true;
                this.errormessage = this.errormessage1;
                this.clickBackToFailedWizard();
            }
        } else {
            this.isClickBackToFailedWizardEnabled = false;
        }
    }

    async saveStepDetails(wizardTab: string): Promise<boolean> {
        if (this.selectedWizardTab === 'ID_PROOFING') {
            return await this.buildAndSaveIDProofDocumentsStepToDB(true);
        }
        if (this.selectedWizardTab === 'REGISTRATION') {
            return this.buildAndSaveRegistrationFieldsStepToDB(true);
        }
        if (this.selectedWizardTab === 'FACE_CAPTURE') {
            return this.buildAndSaveFaceStepToDB(true);
        }
        if (this.selectedWizardTab === 'IRIS_CAPTURE') {
            return this.buildAndSaveIrisStepToDB(true);
        }
        if (this.selectedWizardTab === 'FINGERPRINT_CAPTURE') {
            return this.buildAndSaveFingerprintsStepToDB(true);
        }
        if (this.selectedWizardTab === 'FINGERPRINT_ROLL_CAPTURE') {
            return this.buildAndSaveFingerprintsStepToDB(true);
        }
        if (this.selectedWizardTab === 'SIGNATURE_CAPTURE') {
            return this.buildAndSaveSignatureStepToDB(true);
        }
        if (this.selectedWizardTab === 'ENROLL_SUMMARY') {
            $('.wT .wizardtd .summarybtn').css('border', '4px solid #00bcd4');
            $('.wT .wizardtd .spansummarybtn').css('color', '#00bcd4');
            return true;
        }
    }

    clickBackToFailedWizard() {
        if (this.selectedWizardTab === 'ID_PROOFING') {
            $('.wT .wizardtd .documentbtn').click();
        }
        if (this.selectedWizardTab === 'REGISTRATION') {
            $('.wT .wizardtd .infobtn').click();
        }
        if (this.selectedWizardTab === 'FACE_CAPTURE') {
            $('.wT .wizardtd .facebtn').click();
        }
        if (this.selectedWizardTab === 'IRIS_CAPTURE') {
            $('.wT .wizardtd .irisbtn').click();
        }
        if (this.selectedWizardTab === 'FINGERPRINT_ROLL_CAPTURE') {
            $('.wT .wizardtd .fpRollbtn').click();
        }
        if (this.selectedWizardTab === 'FINGERPRINT_CAPTURE') {
            $('.wT .wizardtd .fpbtn').click();
        }
        if (this.selectedWizardTab === 'SIGNATURE_CAPTURE') {
            $('.wT .wizardtd .signbtn').click();
        }
        if (this.selectedWizardTab === 'ENROLL_SUMMARY') {
            $('.wT .wizardtd .summarybtn').click();
        }
        this.isClickBackToFailedWizardEnabled = true;
    }

    hasWorkflowStep(workflowStep: string): boolean {
        console.log(workflowStep);
        if (!this.getUserToBeEnrolled() || !this.getUserToBeEnrolled().workflowSteps) {
            console.log('false::');
            return false;
        }

        if (this.getUserToBeEnrolled().workflowSteps.includes(workflowStep)) {
            console.log('true::');
            return true;
        }
        console.log('false::');
        return false;
    }

    livePreviewSwap() {
        this.liveSwap = !this.liveSwap;
    }

    onPreviewChange() {
        this.liveSwap = !this.liveSwap;
    }

    stopDeviceClientCalls() {
        if (this.fpReaderSubscription) {
            this.fpReaderSubscription.unsubscribe();
        }
        if (this.irisReaderSubscription) {
            this.irisReaderSubscription.unsubscribe();
        }
        if (this.fingerprintSubscriber) {
            this.fingerprintSubscriber.unsubscribe();
        }
        if (this.signatureDeviceSubscription) {
            this.signatureDeviceSubscription.unsubscribe();
        }
        if (this.sigantureCaptureSubscription) {
            this.sigantureCaptureSubscription.unsubscribe();
        }
    }

    private resolvePrevAndNextWorkflowSteps() {
        this.isRollFpCaptureSelected = false;
        // alert(this.selectedWizardTab);
        if (this.camSubscription) {
            this.camSubscription.unsubscribe();
        }
        if (this.selectedWizardTab === 'ENROLL_SUMMARY') {
            this.enrollmentStartDate = new Date();
            for (let i = this.workflowSteps.length - 1; i >= 0; i--) {
                const beforeStep: string = this.workflowSteps[i];
                if (this.enrollmentSteps.includes(beforeStep)) {
                    this.previousStep = beforeStep;
                    this.nextStep = 'ENROLL_SUMMARY';
                    break;
                }
            }
            $('.wT .wizardtd .spansummarybtn').css('color', '#00bcd4');
            // Load Available Visual Templates
            this.enrollService.getWorkflowCredentialTemplates(this.getUserToBeEnrolled().username).subscribe(response => {
                this.credentialTemplates = response;
                if (this.credentialTemplates && this.credentialTemplates.length === 1) {
                    this.selectedCredentialTemplate.setValue(this.credentialTemplates[0].id);
                    this.getVisualCredential();
                }
            });
            this.stopDeviceClientCalls();
        } else {
            // alert('firststep :' + this.firstStep);
            // alert('selectedWizardTab :' + this.selectedWizardTab);
            if (this.firstStep !== this.selectedWizardTab) {
                const beforeStep: string = this.workflowSteps[this.workflowSteps.indexOf(this.selectedWizardTab) - 1];
                if (this.enrollmentSteps.includes(beforeStep)) {
                    this.previousStep = beforeStep;
                }
            }

            const afterStep: string = this.workflowSteps[this.workflowSteps.indexOf(this.selectedWizardTab) + 1];
            // alert('afterStep :' + afterStep);
            if (this.enrollmentSteps.includes(afterStep)) {
                this.nextStep = afterStep;
            } else {
                this.nextStep = 'ENROLL_SUMMARY';
            }
            this.stopDeviceClientCalls();
        }
        // alert('nextStep :' + this.nextStep);
        // alert('previousStep :' + this.previousStep);
        if (this.selectedWizardTab === 'REGISTRATION') {
            this.enrollmentStartDate = new Date();
            this.createForm();
            $('.wT .wizardtd .spaninfobtn').css('color', '#00bcd4');
            this.stopDeviceClientCalls();
        }
        if (this.selectedWizardTab === 'ID_PROOFING') {
            this.enrollmentStartDate = new Date();
        }
        if (this.selectedWizardTab === 'FACE_CAPTURE') {
            this.enrollmentStartDate = new Date();
            this.isRollFpCaptureSelected = false;
            if (this.capturedImg.length === 0) {
                this.setFaceImage();
            }
            this.faceCaptureError = false;
            // get wf Face _capture step details
            this.getFaceCaptureStepDetails(false, null);
            if (this.workflowSteps.indexOf('REGISTRATION') === -1) {
                this.b = this.a;
            } else {
                // b remains same
            }
            $('.wT .wizardtd .spanfacebtn').css('color', '#00bcd4');
            this.stopDeviceClientCalls();
            this.checkCamPermission();
        }
        // Logic to trigger IRIS auto capture if Selected tab is Iris
        if (this.selectedWizardTab === 'IRIS_CAPTURE') {
            this.enrollmentStartDate = new Date();
            this.deviceClientService.getDeviceSeviceVersion().subscribe(
                deviceServiceStatus => {
                    this.deviceServiceStatus = '';
                    this.triggerIrisAutoCapture();
                },
                err => {
                    this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                        this.deviceServiceStatus = serviceErrMessage;
                        if (this.irisReaderSubscription) {
                            this.irisReaderSubscription.unsubscribe();
                        }
                        // setTimeout(() => {
                        this.triggerIrisAutoCapture();
                        // }, 5000);
                    });
                }
            );
            this.stopDeviceClientCalls();
            if (this.workflowSteps.indexOf('FACE_CAPTURE') === -1) {
                if (this.workflowSteps.indexOf('REGISTRATION') === -1) {
                    this.c = this.a;
                } else {
                    this.c = this.b;
                }
            } else {
                // c remains same
            }
            $('.wT .wizardtd .spanirisbtn').css('color', '#00bcd4');
        }
        if (this.selectedWizardTab === 'FINGERPRINT_ROLL_CAPTURE') {
            this.enrollmentStartDate = new Date();
            this.isRollFpCaptureSelected = true;
            this.hideCapturePosition();
            this.enableFPRecaptureButton = false;
            this.currentFingerPosition = undefined;
            this.fpContextInfoMsg = '';
            this.fpContextWarningMsg = '';
            this.fpContextErrorMsg = '';
            this.fpContextSuccessMsg = '';
            if (this.isClearFpCalledInRollFinger) {
                // set biometrics
                // this.user.userBiometrics.forEach(userBiometric => {
                //     this.setFingerPrints(userBiometric);
                // });
                this.fpContextInfoMsg = '';
                this.fpContextWarningMsg = '';
                this.fpContextErrorMsg = '';
                this.fpContextSuccessMsg = '';
            }
            this.deviceClientService.getDeviceSeviceVersion().subscribe(
                deviceServiceStatus => {
                    this.deviceServiceStatus = '';
                    this.triggerFingerprintAutoCapture();
                },
                err => {
                    this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                        this.deviceServiceStatus = serviceErrMessage;
                        if (this.fpReaderSubscription) {
                            this.fpReaderSubscription.unsubscribe();
                        }
                        if (this.fingerprintSubscriber) {
                            this.fingerprintSubscriber.unsubscribe();
                        }
                        // setTimeout(() => {
                        this.triggerFingerprintAutoCapture();
                        // }, 5000);
                        //  }
                    });
                }
            );
            // this.stopDeviceClientCalls();
            if (this.workflowSteps.indexOf('IRIS_CAPTURE') === -1) {
                if (this.workflowSteps.indexOf('FACE_CAPTURE') === -1) {
                    if (this.workflowSteps.indexOf('REGISTRATION') === -1) {
                        this.d = this.a;
                    } else {
                        this.d = this.b;
                    }
                } else {
                    this.d = this.c;
                }
            } else {
                // d remains same
            }
            $('.wT .wizardtd .spanfpRollbtn').css('color', '#00bcd4');
        }
        // Logic to trigger Fingerprint Auto Capture if Selected tab is Fingerprint
        if (this.selectedWizardTab === 'FINGERPRINT_CAPTURE') {
            this.enrollmentStartDate = new Date();
            this.fpContextInfoMsg = '';
            this.fpContextWarningMsg = '';
            this.fpContextErrorMsg = '';
            this.fpContextSuccessMsg = '';
            this.currentFingerPosition = undefined;

            this.hideCapturePosition();
            this.enableFPRecaptureButton = false;
            if (this.isClearFpCalled) {
                // set biometrics
                this.user.userBiometrics.forEach(userBiometric => {
                    this.setFingerPrints(userBiometric);
                });
                this.fpContextInfoMsg = '';
                this.fpContextWarningMsg = '';
                this.fpContextErrorMsg = '';
                this.fpContextSuccessMsg = '';
            }
            this.deviceClientService.getDeviceSeviceVersion().subscribe(
                deviceServiceStatus => {
                    this.deviceServiceStatus = '';
                    this.triggerFingerprintAutoCapture();
                },
                err => {
                    this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                        this.deviceServiceStatus = serviceErrMessage;
                        if (this.fpReaderSubscription) {
                            this.fpReaderSubscription.unsubscribe();
                        }
                        if (this.fingerprintSubscriber) {
                            this.fingerprintSubscriber.unsubscribe();
                        }
                        // setTimeout(() => {
                        this.triggerFingerprintAutoCapture();
                        // }, 5000);
                        //  }
                    });
                }
            );
            this.stopDeviceClientCalls();
            if (this.workflowSteps.indexOf('FINGERPRINT_ROLL_CAPTURE') === -1) {
                if (this.workflowSteps.indexOf('IRIS_CAPTURE') === -1) {
                    if (this.workflowSteps.indexOf('FACE_CAPTURE') === -1) {
                        if (this.workflowSteps.indexOf('REGISTRATION') === -1) {
                            this.e = this.a;
                        } else {
                            this.e = this.b;
                        }
                    } else {
                        this.e = this.c;
                    }
                } else {
                    this.e = this.d;
                }
            } else {
                // e remains same
            }
            $('.wT .wizardtd .spanfpbtn').css('color', '#00bcd4');
        }
        if (this.selectedWizardTab === 'SIGNATURE_CAPTURE') {
            this.enrollmentStartDate = new Date();
            this.setSiganture();
            // reset all the values
            this.availableSignDevices = [];
            this.deviceServiceStatus = '';
            this.signatureDevice.setValue('');
            this.signErrorMsg = '';
            this.signSuccessMsg = '';
            this.signInfoMsg = '';
            this.reCaptureSignature = false;
            this.deviceClientService.getDeviceSeviceVersion().subscribe(
                deviceServiceStatus => {
                    this.deviceServiceStatus = '';
                    this.getConnectedSignDevices();
                },
                err => {
                    this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                        this.deviceServiceStatus = serviceErrMessage;
                        if (this.signatureDeviceSubscription) {
                            this.signatureDeviceSubscription.unsubscribe();
                        }
                        // setTimeout(() => {
                        this.getConnectedSignDevices();
                        // }, 5000);
                    });
                }
            );
            if (this.workflowSteps.indexOf('FINGERPRINT_CAPTURE') === -1) {
                if (this.workflowSteps.indexOf('FINGERPRINT_ROLL_CAPTURE') === -1) {
                    if (this.workflowSteps.indexOf('IRIS_CAPTURE') === -1) {
                        if (this.workflowSteps.indexOf('FACE_CAPTURE') === -1) {
                            if (this.workflowSteps.indexOf('REGISTRATION') === -1) {
                                this.f = this.a;
                            } else {
                                this.f = this.b;
                            }
                        } else {
                            this.f = this.c;
                        }
                    } else {
                        this.f = this.d;
                    }
                } else {
                    this.f = this.e;
                }
            } else {
                // f remains same
            }
            $('.wT .wizardtd .spansignbtn').css('color', '#00bcd4');
            this.stopDeviceClientCalls();
        }
    }
    getConnectedSignDevices(): void {
        const signModel = new SigantureModel();
        signModel.deviceName = 'Integrated Touchpad';
        this.signatureDeviceSubscription = this.deviceClientService.getConnectedSignatureDevicesList().subscribe(
            (res: SigantureListResponse) => {
                this.deviceServiceStatus = '';
                this.availableSignDevices = res.signatureResponse;
                // if (res.signatureResponse !== null && res.signatureResponse !== undefined && (res.responseCode === 'DS0000' || res.responseCode === 'DS0067')) {
                //     this.availableSignDevices = res.signatureResponse.filter(e => e.deviceId !== '-1');
                //     console.log('available signature pad list', this.availableSignDevices);
                // }
                if (this.availableSignDevices.length !== 0) {
                    if (this.availableSignDevices.filter(e => e.deviceName === 'Integrated Touchpad').length === 0) {
                        this.availableSignDevices.push(signModel);
                    }
                } else {
                    this.availableSignDevices.push(signModel);
                    this.signatureDevice.setValue(signModel);
                }
            },
            err => {
                if (this.availableSignDevices.filter(e => e.deviceName === 'Integrated Touchpad').length === 0) {
                    this.availableSignDevices = [];
                    this.availableSignDevices.push(signModel);
                    this.signatureDevice.setValue(signModel);
                }
                if (this.signatureDeviceSubscription) {
                    this.signatureDeviceSubscription.unsubscribe();
                }
                this.getConnectedSignDevices();
            }
        );
    }
    onSignatureDeviceChange(eventValue: SigantureModel) {
        if (eventValue !== undefined) {
            this.deviceServiceStatus = '';
            this.signErrorMsg = '';
            this.signSuccessMsg = '';
            this.signInfoMsg = '';
            this.reCaptureSignature = false;
            if (eventValue.deviceName !== 'Integrated Touchpad') {
                // this.signInfoMsg = 'Signature device activated, please sign on the signature pad';
                this.captureSignature();
            } else {
                if (this.signatureDeviceSubscription) {
                    this.signatureDeviceSubscription.unsubscribe();
                }
                if (this.sigantureCaptureSubscription) {
                    this.sigantureCaptureSubscription.unsubscribe();
                }
            }
        }
    }
    captureSignature() {
        this.deviceServiceStatus = '';
        this.signErrorMsg = '';
        this.signSuccessMsg = '';
        this.translateService.get('enroll.messages.info.signature.infoMsg').subscribe(message => {
            this.signInfoMsg = message;
        });
        // get the conected sign device
        console.log(this.signatureDevice.value);
        if (this.signatureDevice.value.deviceName !== 'Integrated Touchpad') {
            // this.signInfoMsg = 'Signature device activated, please sign on the signature pad';
            this.sigantureCaptureSubscription = this.deviceClientService.captureSignature(this.signatureDevice.value).subscribe(
                (res: SigantureCaptureResponse) => {
                    console.log(res);
                    this.deviceServiceStatus = '';
                    this.signInfoMsg = '';
                    this.signErrorMsg = '';
                    this.signSuccessMsg = '';
                    if (res.responseCode === 'DS0000') {
                        this.translateService.get('enroll.messages.success.signatureCaptureSuccess').subscribe(message => {
                            this.signSuccessMsg = message;
                            this.signature = `data:image/png;base64,${res.signatureResponse.imageData}`;
                            this.capturedSignModel = res.signatureResponse;
                        });
                        // this.signSuccessMsg = 'Captured successfully';
                    } else if (res.responseCode === 'DS0076') {
                        this.translateService.get('enroll.messages.error.signature.captureTimedOut').subscribe(message => {
                            this.signErrorMsg = message;
                        });
                        // this.signErrorMsg = 'Signature Capture Timeout, to recapture signature press capture';
                        this.reCaptureSignature = true;
                    } else if (res.responseCode === 'DS0305') {
                        // user clicked on cancel
                        this.translateService.get('enroll.messages.error.signature.rejectedToSignMsg').subscribe(message => {
                            this.signErrorMsg = message;
                        });
                        // this.signErrorMsg = 'user rejected to sign, to recapture signature press capture';
                        this.reCaptureSignature = true;
                    } else if (res.responseCode === 'DS0306') {
                        // user clicked on retry
                        this.signErrorMsg = 'Press capture to restart';
                        this.reCaptureSignature = true;
                    } else {
                        this.translateService.get('enroll.messages.error.signature.errorMsg').subscribe(message => {
                            this.signErrorMsg = message;
                        });
                        // this.signErrorMsg = 'Error occured while capturing, press capture to restart';
                        this.reCaptureSignature = true;
                    }
                },
                err => {}
            );
        } else {
            this.signInfoMsg = '';
            this.signSuccessMsg = '';
            // this.reCaptureSign = false;
            this.translateService.get('enroll.messages.error.signature.noDeviceConnected').subscribe(message => {
                this.signErrorMsg = message;
            });
            // this.signErrorMsg = 'No Signature pad connected';
        }
    }
    getFaceCaptureStepDetails(setTransparentImage: boolean, userBiometric: UserBiometric) {
        this.enrollService.getWorkflowStepDetail('FACE_CAPTURE', this.getUserToBeEnrolled().workflow).subscribe(faceCaptureConfig => {
            if (faceCaptureConfig.wfStepGenericConfigs) {
                // Set Config
                let icaoComplianceConfig = false;
                let cropSizeConfig = '';
                faceCaptureConfig.wfStepGenericConfigs.forEach(genericConfig => {
                    if (genericConfig.name === 'ICAO_COMPLIANCE') {
                        if (genericConfig.value === 'Y') {
                            icaoComplianceConfig = true;
                        }
                    } else if (genericConfig.name === 'CROP_SIZE') {
                        cropSizeConfig = genericConfig.value;
                        this.cropFaceWidth = cropSizeConfig.split('X')[0].trim();
                        this.cropFaceHeight = cropSizeConfig.split('X')[1].trim();
                    } else if (genericConfig.name === 'TRANSPARENT_PHOTO_ALLOWED') {
                        if (genericConfig.value === 'Y') {
                            this.isTransparentPhotoAllowed = true;
                            if (setTransparentImage) {
                                userBiometric.userBiometricAttributes.forEach(e => {
                                    if (e.name === 'TRANSPARENT_PHOTO_ALLOWED') {
                                        if (e.value === 'true') {
                                            this.isTransparentPhotoAllowed = true;
                                            this.transparentImgSelected.setValue('yes');
                                        } else {
                                            this.transparentImgSelected.setValue('no');
                                        }
                                    }
                                });
                                if (this.isTransparentPhotoAllowed) {
                                    const user = new User();
                                    const userbiometrics: UserBiometric[] = [];
                                    const userBiometricFace: UserBiometric = new UserBiometric();
                                    userBiometricFace.type = 'FACE';
                                    userBiometricFace.data = this.croppedImg.replace('data:image/png;base64,', '');
                                    userbiometrics.push(userBiometricFace);
                                    user.userBiometrics = userbiometrics;
                                    this.enrollService
                                        .getTransparentPhoto(user, this.cropFaceWidth, this.cropFaceHeight)
                                        .subscribe((res: User) => {
                                            const userBiometricFace1 = res.userBiometrics.find(e => e.type === 'FACE');
                                            this.transparentImg =
                                                'data:image/png;base64,' + userBiometricFace1.base64OfTransparentImageData;
                                        });
                                }
                            }
                        }
                    }
                });
                this.setFaceCropOptions();
                if (this.setLdapUserFace) {
                    this.setFaceImageForLdapUser(this.user.userBiometrics.find(e => e.type === 'FACE'));
                }
            }
        });
    }
    onCredentialTemplateChanged(event: any) {
        this.getVisualCredential();
    }

    getVisualCredential(): void {
        this.isSummaryDiasbled = true;
        this.isLoadingResults = true;
        let isTransparentPhotoSelected = false;
        if (this.transparentImgSelected.value === 'yes' && this.isTransparentPhotoAllowed) {
            isTransparentPhotoSelected = true;
        }
        this.enrollService
            .getVisualCredential(this.selectedCredentialTemplate.value, this.constructVisualCredential(false), isTransparentPhotoSelected)
            .subscribe(
                response => {
                    this.visualCredential = response;
                    this.isLoadingResults = false;
                    this.isSummaryDiasbled = false;
                },
                error => {
                    this.isSummaryDiasbled = false;
                    this.isLoadingResults = false;
                }
            );
    }

    // ---------------------------------------Information starts-----------------------------------

    enableStates(event: any) {
        // update the ui( enable state field)
        this.isStateDisabled = false;
    }

    captureInformation() {}
    onCancelCaptureDocument() {
        if (this.camSubscription) {
            this.camSubscription.unsubscribe();
        }
        this.captureDocument = false;
        this.isEditIdProofDocSelected = false;
        this.hiddingNextButton = false;
        this.hideIDproofCapture = true;
    }
    captureDocumentVisibility() {
        this.idProofDocSaveError = false;
        this.idProofCamAccessError = false;
        this.idProofDocFileError = false;
        this.captureDocument = true;
        this.IDProofDocForm.reset();
        this.isCapturedId = false;
        this.isCapturedIdBack = false;
        this.isCroppedId = false;
        this.isCroppedIdBack = false;
        this.capturedImgId = [];
        this.capturedImgIdBack = [];
        this.croppedImgId = undefined;
        this.croppedImgIdBack = undefined;
        this.frontCapture = true;
        this.backCapture = false;
        this.recaptureIdProofDoc = false;
        this.issueAuthorities = [];
        this.hideIDproofCapture = false;
        this.hiddingNextButton = true;
    }

    // ----------------------------------IRIS starts-------------------------------------------------
    RecapturIris(): void {
        this.captureIris(this.irisModelObjInEdit);
    }
    async triggerIrisAutoCapture() {
        if (this.selectedWizardTab === 'IRIS_CAPTURE') {
            if (this.user.userBiometrics.find(e => e.type === 'IRIS') === undefined) {
                this.isIrisAddedNewlyInWf = true;
            }
            // If Iris captured , return
            if (!(this.isEnrolledUser || this.isEnrollmentInProgress) || this.isIrisAddedNewlyInWf) {
                if (this.leftEye != null && this.rightEye != null) {
                    return;
                }
            }
            if (!this.irisStepDetail) {
                this.irisStepDetail = await this.enrollService
                    .getWorkflowStepDetail('IRIS_CAPTURE', this.getUserToBeEnrolled().workflow)
                    .toPromise();
                if (this.irisStepDetail.wfStepGenericConfigs) {
                    for (let i = 0; i < this.irisStepDetail.wfStepGenericConfigs.length; i++) {
                        if (this.irisStepDetail.wfStepGenericConfigs[i].name === 'IRIS_MIN_THRESHOLD') {
                            this.wfIrisQuality = this.irisStepDetail.wfStepGenericConfigs[i].value;
                        }
                    }
                }
            }
            this.isIrisTabSelected = true;
            // Check for connected Iris devices and Start Iris capture process
            if (this.irisReaderSubscription) {
                this.irisReaderSubscription.unsubscribe();
            }
            this.getConnectedIrisDevices();
        } else {
            this.isIrisTabSelected = false;
        }
    }

    getConnectedIrisDevices(): void {
        this.irisReaderSubscription = this.deviceClientService.getConnectedIrisReaders().subscribe(
            response => {
                this.irisErrorMsg = '';
                this.deviceServiceStatus = '';
                if (response.responseCode !== 'DS0000') {
                    this.enableIrisRecaptureButton = false;
                    this.translateService.get('enroll.messages.error.iris.noIrisDeviceConnected').subscribe(irisErrMessage => {
                        this.irisErrorMsg = irisErrMessage;
                    });
                    if (this.irisReaderSubscription) {
                        this.irisReaderSubscription.unsubscribe();
                    }
                    // setTimeout(() => {
                    this.getConnectedIrisDevices();
                    // }, 5000);
                } else if (response.irisDetails.length === 0) {
                    this.enableIrisRecaptureButton = false;
                    this.translateService.get('enroll.messages.error.iris.noIrisDeviceConnected').subscribe(message => {
                        this.irisErrorMsg = message;
                    });
                    if (this.irisReaderSubscription) {
                        this.irisReaderSubscription.unsubscribe();
                    }
                    // setTimeout(() => {
                    this.getConnectedIrisDevices();
                    // }, 5000);
                } else {
                    // Available Devices;
                    this.availableIrisDevices = [];
                    for (let i = 0; i < response.irisDetails.length; i++) {
                        const irisModel = new IrisModel();
                        irisModel.deviceName = response.irisDetails[i].deviceName;
                        irisModel.deviceSerialNo = response.irisDetails[i].deviceSerialNo;
                        irisModel.deviceId = response.irisDetails[i].deviceId;
                        this.availableIrisDevices.push(irisModel);
                    }
                    if (response.irisDetails.length === 1) {
                        this.irisDevice.setValue(this.availableIrisDevices[0]);
                        // also check for Iris details there for edit user
                        // || (this.user.userBiometrics.find(e => e.type === 'IRIS') !== undefined)
                        // if (!(this.isEnrolledUser || this.isEnrollmentInProgress) || this.isIrisAddedNewlyInWf) {
                        if (this.leftEye == null && this.rightEye == null) {
                            this.captureIris(this.availableIrisDevices[0]);
                        } else {
                            this.enableIrisRecaptureButton = true;
                            this.irisModelObjInEdit = this.availableIrisDevices[0];
                        }
                    }
                }
            },
            err => {
                this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                    this.deviceServiceStatus = '';
                    this.deviceServiceStatus = serviceErrMessage;
                    this.enableIrisRecaptureButton = false;
                    if (this.irisReaderSubscription) {
                        this.irisReaderSubscription.unsubscribe();
                    }
                    // setTimeout(() => {
                    this.getConnectedIrisDevices();
                    // }, 5000);
                });
            }
        );
    }
    captureIris(irisModel: IrisModel): void {
        if (!this.isIrisTabSelected) {
            return;
        }
        this.translateService.get('enroll.messages.info.iris.irisPosition').subscribe(
            irisInfoMessage => {
                this.irisInfoMsg = irisInfoMessage;
                this.irisModelObj = irisModel;
                this.irisSubscriber = this.deviceClientService.captureIris(irisModel).subscribe(irisResponse => {
                    if (irisResponse.responseCode === 'DS0000') {
                        this.leftEye = irisResponse.irisDetails.iris.leftIris;
                        this.rightEye = irisResponse.irisDetails.iris.rightIris;
                        if (this.leftEye !== '' && this.rightEye !== '') {
                            this.irisInfoMsg = '';
                            this.translateService.get('enroll.messages.success.iris.irisCaptureCompleted').subscribe(irisSuccMsg => {
                                this.irisSuccessMsg = irisSuccMsg;
                                this.deviceServiceStatus = '';
                                this.irisErrorMsg = '';
                            });
                        }
                    } else {
                        this.translateService.get('enroll.messages.error.iris.unknownErrorIris').subscribe(irisErrMsg => {
                            this.irisErrorMsg = irisErrMsg;
                            this.irisRecapture = true;
                        });
                    }
                });
            },
            err => {
                this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                    this.irisErrorMsg = serviceErrMessage;
                    if (this.irisReaderSubscription) {
                        this.irisReaderSubscription.unsubscribe();
                    }
                    // setTimeout(() => {
                    this.getConnectedIrisDevices();
                    // }, 5000);
                });
            }
        );
    }
    changeIrisDevice(event: any) {
        if (this.irisSubscriber) {
            this.irisSubscriber.unsubscribe();
        }
        this.irisModelObjInEdit = event.value;
        // also check for Iris details there for edit user
        if (!(this.isEnrolledUser || this.isEnrollmentInProgress) || this.isIrisAddedNewlyInWf) {
            this.captureIris(event.value);
        }
    }
    clearIris() {
        this.translateService.get('enroll.messages.info.iris.delIrisConfirmMsg').subscribe(msg => {
            const dialogRef = this.dialog.open(ReActivateDialogComponent, {
                autoFocus: false,
                width: '500px',
                data: {
                    isShowPUK: false,
                    dialogMsg: msg
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                console.log(result);
                if (result === 'OK') {
                    this.clearIrisAndStartCapture();
                }
            });
        });
    }
    clearIrisAndStartCapture() {
        console.log('clear iris called:::');
        this.irisErrorMsg = '';
        this.irisSuccessMsg = '';
        this.irisInfoMsg = '';
        this.leftEye = null;
        this.rightEye = null;
        this.getConnectedIrisDevices();
    }
    // ----------------------------------IRIS Ends-------------------------------------------------

    // ----------------------------------FingerPrint starts-------------------------------------------------

    async RecaptureFP(fingerPosition: string) {
        this.isFPRecaptureSelected = true;
        this.currentFingerPosition = this.minRequiredFingerPositions.find(e => e === fingerPosition);
        if (this.currentFingerPosition !== undefined) {
            this.selectedFingerPositionForRecapture = this.currentFingerPosition;
            this.fingerModelInEdit.fPosition = this.currentFingerPosition;
            this.captureFP(this.fingerModelInEdit);
        }
    }

    async triggerFingerprintAutoCapture() {
        if (this.selectedWizardTab === 'FINGERPRINT_CAPTURE') {
            this.hideCapturePosition();
            // check Fp step added newly in workflow edit (this is only for edit enrollment of user)
            if (this.user.userBiometrics.find(e => e.type === 'FINGERPRINT') === undefined || this.isClearFpCalled) {
                this.isFPAddedNewlyInWf = true;
            }
            // Get Step Detail only if not retrieved before
            await this.getFPStepDetails();
            if (this.currentFingerPosition === undefined) {
                if (this.minRequiredFingerPositions.length === 0) {
                    this.minRequiredFingerPositions = this.fingerPositions;
                }
                this.currentFingerPosition = this.minRequiredFingerPositions[0];
            }
            // If all fingers captured , hide blink animation
            console.log(this.fingerprintCapturedPositions.length, this.minRequiredFingerPositions.length);
            if (!(this.isEnrolledUser || this.isEnrollmentInProgress) || this.isFPAddedNewlyInWf) {
                console.log('inside if:::');
                if (this.fingerprintCapturedPositions.length === this.minRequiredFingerPositions.length) {
                    this.hideCapturePosition();
                    return;
                }
            } else {
                // this.hideCapturePosition();
                // && this.fingerprintCapturedPositions.length !==10
                if (this.fingerprintCapturedPositions.length !== this.minRequiredFingerPositions.length) {
                    this.isFPConfigEditedInWf = true;
                }
                if (!this.isFPConfigEditedInWf) {
                    this.hideCapturePosition();
                    // return;
                } else {
                    this.currentFingerPosition = this.newlyAddedFpsCapturePositions[0];
                }
            }
            // Set blink animation on current finger capture position
            // this.setCurrentCapturePosition(this.currentFingerPosition);

            this.isFingerprintTabSelected = true;

            if (this.fpReaderSubscription) {
                this.fpReaderSubscription.unsubscribe();
            }
            // Check for connected Fingerprint devices and Start fingerprint capture process
            this.getConnectedFingerprintReaders();
        } else if (this.isRollFpCaptureSelected) {
            this.isFingerprintTabSelected = true;
            this.hideCapturePosition();
            if (this.user.userBiometrics.find(e => e.type === 'FINGERPRINT') === undefined || this.isClearFpCalledInRollFinger) {
                this.isFPAddedNewlyInWf = true;
            } else {
                console.log('in edit::::');
            }
            // Get Step Detail only if not retrieved before
            await this.getFPStepDetails();
            console.log(this.currentFingerPosition);
            this.minRequiredFingerPositions = this.fingerPositions;
            if (this.currentFingerPosition === undefined) {
                // if (this.minRequiredFingerPositions.length === 0) {
                // }
                this.currentFingerPosition = this.fingerPositions[0];
            }
            // If all fingers captured , hide blink animation

            if (this.fingerprintCapturedPositionsInRollFinger.length === this.minRequiredFingerPositions.length) {
                this.hideCapturePosition();
                return;
            }
            // if (!(this.isEnrolledUser || this.isEnrollmentInProgress) || this.isFPAddedNewlyInWf) {
            //     if ((this.fingerprintCapturedPositionsInRollFinger.length === this.minRequiredFingerPositions.length)
            //     || (this.fingerprintCapturedPositionsInRollFinger.length > this.minRequiredFingerPositions.length)
            //     ) {
            //         this.hideCapturePosition();
            //         return;
            //     } else {
            //         console.log('in edit roll fp:::');
            //     }
            // }
            if (this.fpReaderSubscription) {
                this.fpReaderSubscription.unsubscribe();
            }
            // Check for connected Fingerprint devices and Start fingerprint capture process
            this.getConnectedFingerprintReaders();
        }
    }
    // get the Finger print step details from workflow
    async getFPStepDetails() {
        console.log(this.fingerpositionsFromWorkFlow);
        this.minRequiredFingerPositions = this.fingerpositionsFromWorkFlow;
        if (!this.fingerprintStepDetail) {
            this.fingerprintStepDetail = await this.enrollService
                .getWorkflowStepDetail('FINGERPRINT_CAPTURE', this.getUserToBeEnrolled().workflow)
                .toPromise();
            if (this.fingerprintStepDetail.wfStepGenericConfigs) {
                for (let i = 0; i < this.fingerprintStepDetail.wfStepGenericConfigs.length; i++) {
                    if (this.fingerprintStepDetail.wfStepGenericConfigs[i].name === 'FINGERPRINT_MIN_THRESHOLD') {
                        this.workflowImageQuality = this.fingerprintStepDetail.wfStepGenericConfigs[i].value;
                    }
                    if (this.fingerprintStepDetail.wfStepGenericConfigs[i].name === 'MINIMUM_FINGERS_REQUIRED') {
                        this.minRequiredFingers = this.fingerprintStepDetail.wfStepGenericConfigs[i].value;
                    }
                    if (this.fingerprintStepDetail.wfStepGenericConfigs[i].name === 'FINGER_POSITION') {
                        this.minRequiredFingerPositions.push(this.fingerprintStepDetail.wfStepGenericConfigs[i].value);
                        this.fingerpositionsFromWorkFlow = this.minRequiredFingerPositions;
                    }
                }
            }
        }
    }
    changeFingerprintDevice(event: any) {
        this.isFPRecaptureSelected = false;
        if (this.availableFingerprintDevices.length === 1) {
            return;
        }
        this.isLoadingResults = true;
        if (this.fingerprintSubscriber !== undefined) {
            if (!this.fingerprintSubscriber.closed) {
                this.fingerprintSubscriber.unsubscribe();
            } else {
                this.isLoadingResults = false;
            }
        }
        if (this.isEnrolledUser || this.isEnrollmentInProgress) {
            this.fpContextInfoMsg = '';
            this.fpContextSuccessMsg = '';
            this.enableFPRecaptureButton = true;
        } else {
            this.resetValues();
        }
        this.enableSingleOrSlapFp(event.value);
    }

    changeFingerprintDeviceForRollFp(event: any) {
        // this.isFPRecaptureSelected = false;
        if (this.availableFingerprintDevices.length === 1) {
            return;
        }
        this.isLoadingResults = true;
        if (this.fingerprintSubscriber !== undefined) {
            if (!this.fingerprintSubscriber.closed) {
                this.fingerprintSubscriber.unsubscribe();
            } else {
                this.isLoadingResults = false;
            }
        }
        if (this.isEnrolledUser || this.isEnrollmentInProgress) {
            this.fpContextInfoMsg = '';
            this.fpContextSuccessMsg = '';
            // this.enableFPRecaptureButton = true;
        } else {
            this.resetValuesForRollFp();
        }
        this.enableSingleOrSlapFp(event.value);
    }

    // Get connected fingerprint readers and start capture process
    getConnectedFingerprintReaders(): void {
        this.fpReaderSubscription = this.deviceClientService.getConnectedFingerprintReaders().subscribe(
            response => {
                this.isLoadingResults = false;
                this.deviceServiceStatus = '';
                this.fpContextErrorMsg = '';
                if (response.responseCode !== 'DS0000') {
                    this.enableFPRecaptureButton = false;
                    this.translateService.get('enroll.messages.error.fingerprint.noDeviceConnected').subscribe(message => {
                        this.fpContextErrorMsg = message;
                    });
                    // Retrying
                    if (this.fpReaderSubscription) {
                        this.fpReaderSubscription.unsubscribe();
                    }
                    // setTimeout(() => {
                    this.getConnectedFingerprintReaders();
                    // }, 5000);
                } else if (response.fingerPrintReaders.length === 0) {
                    this.translateService.get('enroll.messages.error.fingerprint.noDeviceConnected').subscribe(message => {
                        this.fpContextErrorMsg = message;
                    });
                } else {
                    if (response.fingerPrintReaders[0].deviceName === undefined) {
                        this.translateService.get('enroll.messages.error.fingerprint.noDeviceConnected').subscribe(message => {
                            this.fpContextErrorMsg = message;
                        });
                        // Retrying
                        if (this.fpReaderSubscription) {
                            this.fpReaderSubscription.unsubscribe();
                        }
                        // setTimeout(() => {
                        this.getConnectedFingerprintReaders();
                        // }, 5000);
                    } else {
                        // Available Devices;
                        // this.fingerprintDevice.reset();
                        this.availableFingerprintDevices = [];
                        for (let i = 0; i < response.fingerPrintReaders.length; i++) {
                            const fingerModel = new FingerModel();
                            fingerModel.deviceId = response.fingerPrintReaders[i].deviceId;
                            fingerModel.deviceSerialNo = response.fingerPrintReaders[i].deviceSerialNo;
                            fingerModel.deviceName = response.fingerPrintReaders[i].deviceName;
                            fingerModel.manufacturar = response.fingerPrintReaders[i].manufacturar;
                            this.availableFingerprintDevices.push(fingerModel);
                        }
                        if (response.fingerPrintReaders.length === 1) {
                            if (this.isEnrolledUser || this.isEnrollmentInProgress) {
                                this.enableFPRecaptureButton = true;
                            }
                            this.fingerprintDevice.setValue(this.availableFingerprintDevices[0]);
                            this.enableSingleOrSlapFp(this.availableFingerprintDevices[0]);
                        } else {
                            this.translateService.get('enroll.messages.info.fingerprint.selectFPReaderMsg').subscribe(message => {
                                this.fpContextInfoMsg = message;
                            });
                        }
                    }
                }
            },
            err => {
                this.fingerprintDevice.reset();
                this.isLoadingResults = false;
                this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                    this.deviceServiceStatus = '';
                    this.enableFPRecaptureButton = false;
                    this.isFPRecaptureSelected = false;
                    this.deviceServiceStatus = serviceErrMessage;
                    // Retrying
                    if (this.fpReaderSubscription) {
                        this.fpReaderSubscription.unsubscribe();
                    }
                    if (this.fingerprintSubscriber) {
                        this.fingerprintSubscriber.unsubscribe();
                    }
                    // setTimeout(() => {
                    this.getConnectedFingerprintReaders();
                    // }, 5000);
                });
            }
        );
    }
    // Based on Fingerprint reader name, Single/slap UI will be triggered
    enableSingleOrSlapFp(fingerModel: FingerModel): void {
        this.hideCapturePosition();
        let fingerpintDeviceConfig: FingerpintDeviceConfig;
        this.isLoadingResults = false;
        console.log('fingerprint device connfiguration');
        this.enrollService.getFingerprintDeviceConfig(fingerModel.manufacturar, fingerModel.deviceName).subscribe(
            res => {
                if (res.status === 200) {
                    fingerpintDeviceConfig = res.body;
                    this.isRollsupport = fingerpintDeviceConfig.rollSupport;

                    if (!this.isRollFpCaptureSelected) {
                        if (fingerpintDeviceConfig.captureType == 'Single') {
                            this.isSingleFPCaptured = true;
                            this.isSlapFPCaptured = false;
                            this.isTwoFPCaptured = false;
                        } else if (fingerpintDeviceConfig.captureType == 'Slap') {
                            this.isSingleFPCaptured = false;
                            this.isTwoFPCaptured = false;
                            this.isSlapFPCaptured = true;
                        } else {
                            this.isTwoFPCaptured = true;
                            this.isSingleFPCaptured = false;
                            this.isSlapFPCaptured = false;
                        }

                        if (!this.isFPConfigEditedInWf) {
                            if (this.isSlapFPCaptured) {
                                this.minRequiredFingerPositions = this.slapFingerPositions;
                                this.currentFingerPosition = this.minRequiredFingerPositions[0];
                            } else if (this.isSingleFPCaptured) {
                                this.minRequiredFingerPositions = this.fingerpositionsFromWorkFlow;
                                this.currentFingerPosition = this.minRequiredFingerPositions[0];
                            } else if (this.isTwoFPCaptured) {
                                ///
                                this.minRequiredFingerPositions = this.dualFingerPositions;
                                this.currentFingerPosition = this.minRequiredFingerPositions[0];
                            } ///
                        }
                        fingerModel.fPosition = this.currentFingerPosition;
                        if (!(this.isEnrollmentInProgress || this.isEnrolledUser) || this.isFPAddedNewlyInWf || this.isFPConfigEditedInWf) {
                            this.fingerModelInEdit = fingerModel;
                            this.captureFP(fingerModel);
                        } else {
                            this.fingerModelInEdit = fingerModel;
                            // enable recapture button in fp page....
                            this.enableFPRecaptureButton = true;
                        }
                    } else {
                        console.log('roll capture selected:::');
                        if (this.isRollsupport) {
                            this.isSingleFPCaptured = true;
                            this.minRequiredFingerPositions = this.fingerPositions;
                            fingerModel.fPosition = this.currentFingerPosition;
                            console.log(fingerModel.fPosition);
                            // if (!(this.isEnrollmentInProgress || this.isEnrolledUser) || this.isFPAddedNewlyInWf || this.isFPConfigEditedInWf) {
                            this.fingerModelInEdit = fingerModel;
                            this.captureFP(fingerModel);
                            // } else {
                            //     this.fingerModelInEdit = fingerModel;
                            //     this.enableFPRecaptureButton = true;
                            // }
                        }
                    }
                }
            },
            err => {
                console.log(err);
                this.translateService.get('enroll.wizard.tab.fingerprints.deviceSupport').subscribe(msg => {
                    this.fpContextErrorMsg = msg;
                });
            }
        );
    }
    clearRollFingerPrints() {
        this.translateService.get('enroll.messages.info.fingerprint.delFpConfirmMsg').subscribe(msg => {
            const dialogRef = this.dialog.open(ReActivateDialogComponent, {
                autoFocus: false,
                width: '500px',
                data: {
                    isShowPUK: false,
                    dialogMsg: msg
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result === 'OK') {
                    this.clerRollFps();
                }
            });
        });
    }
    clearFingerPrints() {
        this.translateService.get('enroll.messages.info.fingerprint.delFpConfirmMsg').subscribe(msg => {
            const dialogRef = this.dialog.open(ReActivateDialogComponent, {
                autoFocus: false,
                width: '500px',
                data: {
                    isShowPUK: false,
                    dialogMsg: msg
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result === 'OK') {
                    this.clearFps();
                }
            });
        });
    }
    clerRollFps() {
        this.isClearFpCalledInRollFinger = true;
        this.isLoadingResults = true;
        // unsubscribe capture subscription
        if (this.fingerprintSubscriber !== undefined) {
            if (!this.fingerprintSubscriber.closed) {
                this.fingerprintSubscriber.unsubscribe();
            } else {
                this.isLoadingResults = false;
            }
        }
        this.isRollFpAutoCaptureCompleted = false;
        // clear captured fps
        this.leftRollFingerprintImages = [];
        this.rightRollFingerprintImages = [];

        this.currentFingerPosition = undefined;
        this.fingerprintCapturedPositionsInRollFinger = [];
        // clear all the messages
        this.fpContextInfoMsg = '';
        this.fpContextWarningMsg = '';
        this.fpContextErrorMsg = '';
        this.fpContextSuccessMsg = '';

        this.isSingleFPCaptured = true;
        this.isSlapFPCaptured = false;

        this.isClearRollFpEnabled = false;
        this.disableRollFpDeviceSelection = false;
        this.triggerFingerprintAutoCapture();
    }
    clearFps() {
        this.isClearFpCalled = true;
        this.isLoadingResults = true;
        // unsubscribe capture subscription
        if (this.fingerprintSubscriber !== undefined) {
            if (!this.fingerprintSubscriber.closed) {
                this.fingerprintSubscriber.unsubscribe();
            } else {
                this.isLoadingResults = false;
            }
        }
        this.isFpAutoCaptureCompleted = false;
        // remove all the messages and call the fp reader
        // clear captured fps
        this.leftFourFingers = undefined;
        this.rightFourFingers = undefined;
        this.thumbFingers = undefined;
        this.leftFingerprintImages = [];
        this.rightFingerprintImages = [];

        this.currentFingerPosition = undefined;
        this.fingerprintCapturedPositions = [];
        this.fpContextInfoMsg = '';
        this.fpContextWarningMsg = '';
        this.fpContextErrorMsg = '';
        this.fpContextSuccessMsg = '';
        this.isSingleFPCaptured = true;
        this.isSlapFPCaptured = false;
        this.fingerModelInEdit = new FingerModel();

        this.isClearFpEnabled = false;
        this.disableFpDeviceSelection = false;

        this.triggerFingerprintAutoCapture();
    }

    captureFP(fingerModel: FingerModel): void {
        // If Fingerprint tab not selected / present , stop capture process to avoid memory leaks
        console.log('capture FP called');
        this.fingerModelObj = fingerModel;
        if (this.fpCaptureCalledCount === 3) {
            this.fpCaptureCalledCount = 0;
            this.fpContextWarningMsg = '';
        }
        if (!this.isFingerprintTabSelected || $('.fpbtn').length === 0) {
            return;
        }
        this.fpContextSuccessMsg = '';
        this.translateService
            .get('enroll.messages.info.fingerprint.fingerPosition', { fingerPosition: `${fingerModel.fPosition}` })
            .subscribe(message => {
                this.fpContextInfoMsg = message;
            });

        // Set blink animation on current finger capture position
        this.setCurrentCapturePosition(fingerModel.fPosition);
        // Device Service REST API Call
        this.fingerprintSubscriber = this.deviceClientService
            .captureFP(fingerModel, this.isRollFpCaptureSelected, this.isWsqFormatEnabled)
            .subscribe(
                (response: FingerCaptureResponse) => {
                    if (this.isSlapFPCaptured) {
                        this.index = this.slapFingerPositions.indexOf(fingerModel.fPosition);
                    } else if (this.isSingleFPCaptured) {
                        this.index = this.fingerPositions.indexOf(fingerModel.fPosition);
                    } else {
                        this.index = this.dualFingerPositions.indexOf(fingerModel.fPosition);
                    }
                    const fingerPrintDetails = response;
                    if (fingerPrintDetails.responseCode === 'DS0076') {
                        this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
                        this.translateService.get('enroll.messages.error.fingerprint.captureTimedOut').subscribe(message => {
                            this.fpContextErrorMsg = message;
                        });
                        // Retrying
                        if (this.fpCaptureCalledCount < 3) {
                            this.captureFP(fingerModel);
                        } else {
                            this.fingerModelObj = fingerModel;
                            this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(message => {
                                this.fpContextWarningMsg = message;
                            });
                        }
                    } else if (fingerPrintDetails.responseCode !== 'DS0000') {
                        this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
                        this.translateService.get('enroll.messages.error.fingerprint.captureTimedOut').subscribe(message => {
                            this.fpContextErrorMsg = message;
                        });
                        // Retrying
                        if (this.fpCaptureCalledCount <= 3) {
                            this.captureFP(fingerModel);
                        } else {
                            this.fingerModelObj = fingerModel;
                            this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(message => {
                                this.fpContextWarningMsg = message;
                            });
                        }
                    } else if (fingerPrintDetails.responseCode === 'DS0000') {
                        if (this.isSlapFPCaptured) {
                            if (fingerModel.fPosition === 'FLAT_SLAP_4_RIGHT') {
                                const rightFingers = response.fingerPrintDetails.RightFingers;
                                if (rightFingers.noOfFingers !== '4') {
                                    this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
                                    this.translateService.get('enroll.messages.error.fingerprint.rightFourFingers').subscribe(message => {
                                        this.fpContextErrorMsg = message;
                                    });
                                    // Retrying
                                    if (this.fpCaptureCalledCount < 3) {
                                        this.captureFP(fingerModel);
                                    } else {
                                        this.fingerModelObj = fingerModel;
                                        this.fpContextErrorMsg = '';
                                        this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(message => {
                                            this.fpContextWarningMsg = message;
                                        });
                                    }
                                    return;
                                } else {
                                    if (rightFingers.image !== null) {
                                        if (
                                            !this.checkImageQualityMatchedOrNot(rightFingers.RightIndex.quality, fingerModel, 'RIGHT_INDEX')
                                        ) {
                                            return;
                                        }
                                        if (
                                            !this.checkImageQualityMatchedOrNot(
                                                rightFingers.RightMiddle.quality,
                                                fingerModel,
                                                'RIGHT_MIDDLE'
                                            )
                                        ) {
                                            return;
                                        }
                                        if (
                                            !this.checkImageQualityMatchedOrNot(rightFingers.RightRing.quality, fingerModel, 'RIGHT_RING')
                                        ) {
                                            return;
                                        }
                                        if (
                                            !this.checkImageQualityMatchedOrNot(
                                                rightFingers.RightLittle.quality,
                                                fingerModel,
                                                'RIGHT_LITTLE'
                                            )
                                        ) {
                                            return;
                                        }
                                        const rightFourFp: UserBiometric = new UserBiometric();
                                        rightFourFp.jpegData = rightFingers.image;
                                        rightFourFp.data = rightFingers.image;
                                        rightFourFp.imageQuality = rightFingers.quality;
                                        rightFourFp.position = 'FLAT_SLAP_4_RIGHT';
                                        this.rightFourFingers = rightFourFp;
                                    } else {
                                        return;
                                    }
                                    if (rightFingers.RightIndex.image !== null) {
                                        const biometricRightIndex: UserBiometric = new UserBiometric();
                                        biometricRightIndex.jpegData = rightFingers.RightIndex.image;
                                        biometricRightIndex.data = rightFingers.RightIndex.image;
                                        biometricRightIndex.imageQuality = rightFingers.RightIndex.quality;
                                        biometricRightIndex.position = 'RIGHT_INDEX';
                                        biometricRightIndex.ansi = rightFingers.RightIndex.ansi;
                                        biometricRightIndex.format = rightFingers.imageType;
                                        this.rightFingerprintImages[1] = biometricRightIndex;
                                    } else {
                                        return;
                                    }
                                    if (rightFingers.RightMiddle.image !== null) {
                                        const biometricRightMiddle: UserBiometric = new UserBiometric();
                                        biometricRightMiddle.jpegData = rightFingers.RightMiddle.image;
                                        biometricRightMiddle.data = rightFingers.RightMiddle.image;
                                        biometricRightMiddle.imageQuality = rightFingers.RightMiddle.quality;
                                        biometricRightMiddle.position = 'RIGHT_MIDDLE';
                                        biometricRightMiddle.ansi = rightFingers.RightMiddle.ansi;
                                        biometricRightMiddle.format = rightFingers.imageType;
                                        this.rightFingerprintImages[2] = biometricRightMiddle;
                                    } else {
                                        return;
                                    }
                                    if (rightFingers.RightRing.image !== null) {
                                        const biometricRightRing: UserBiometric = new UserBiometric();
                                        biometricRightRing.jpegData = rightFingers.RightRing.image;
                                        biometricRightRing.data = rightFingers.RightRing.image;
                                        biometricRightRing.imageQuality = rightFingers.RightRing.quality;
                                        biometricRightRing.position = 'RIGHT_RING';
                                        biometricRightRing.ansi = rightFingers.RightRing.ansi;
                                        biometricRightRing.format = rightFingers.imageType;
                                        this.rightFingerprintImages[3] = biometricRightRing;
                                    } else {
                                        return;
                                    }
                                    if (rightFingers.RightLittle.image !== null) {
                                        const biometricRightLittle: UserBiometric = new UserBiometric();
                                        biometricRightLittle.jpegData = rightFingers.RightLittle.image;
                                        biometricRightLittle.data = rightFingers.RightLittle.image;
                                        biometricRightLittle.imageQuality = rightFingers.RightLittle.quality;
                                        biometricRightLittle.position = 'RIGHT_LITTLE';
                                        biometricRightLittle.ansi = rightFingers.RightLittle.ansi;
                                        biometricRightLittle.format = rightFingers.imageType;
                                        this.rightFingerprintImages[4] = biometricRightLittle;
                                    } else {
                                        return;
                                    }
                                }
                            } else if (fingerModel.fPosition === 'FLAT_SLAP_4_LEFT') {
                                const leftFingers = response.fingerPrintDetails.LeftFingers;
                                if (leftFingers.noOfFingers !== '4') {
                                    this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
                                    this.translateService.get('enroll.messages.error.fingerprint.leftFourFingers').subscribe(message => {
                                        this.fpContextErrorMsg = message;
                                    });
                                    // Retrying
                                    if (this.fpCaptureCalledCount < 3) {
                                        this.captureFP(fingerModel);
                                    } else {
                                        this.fpContextErrorMsg = '';
                                        this.fingerModelObj = fingerModel;
                                        this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(message => {
                                            this.fpContextWarningMsg = message;
                                        });
                                    }
                                    return;
                                } else {
                                    if (leftFingers.image !== null) {
                                        if (!this.checkImageQualityMatchedOrNot(leftFingers.LeftIndex.quality, fingerModel, 'LEFT_INDEX')) {
                                            return;
                                        }
                                        if (
                                            !this.checkImageQualityMatchedOrNot(leftFingers.LeftMiddle.quality, fingerModel, 'LEFT_MIDDLE')
                                        ) {
                                            return;
                                        }
                                        if (!this.checkImageQualityMatchedOrNot(leftFingers.LeftRing.quality, fingerModel, 'LEFT_RING')) {
                                            return;
                                        }
                                        if (
                                            !this.checkImageQualityMatchedOrNot(leftFingers.LeftLittle.quality, fingerModel, 'LEFT_LITTLE')
                                        ) {
                                            return;
                                        }
                                        const leftFourFp: UserBiometric = new UserBiometric();
                                        leftFourFp.jpegData = leftFingers.image;
                                        leftFourFp.data = leftFingers.image;
                                        leftFourFp.imageQuality = leftFingers.quality;
                                        leftFourFp.position = 'FLAT_SLAP_4_LEFT';
                                        this.leftFourFingers = leftFourFp;
                                    } else {
                                        return;
                                    }
                                    if (leftFingers.LeftIndex.image !== null) {
                                        const biometricLeftIndex: UserBiometric = new UserBiometric();
                                        biometricLeftIndex.jpegData = leftFingers.LeftIndex.image;
                                        biometricLeftIndex.data = leftFingers.LeftIndex.image;
                                        biometricLeftIndex.imageQuality = leftFingers.LeftIndex.quality;
                                        biometricLeftIndex.position = 'LEFT_INDEX';
                                        biometricLeftIndex.ansi = leftFingers.LeftIndex.ansi;
                                        biometricLeftIndex.format = leftFingers.imageType;
                                        this.leftFingerprintImages[1] = biometricLeftIndex;
                                    } else {
                                        return;
                                    }
                                    if (leftFingers.LeftMiddle.image !== null) {
                                        const biometricLeftMiddle: UserBiometric = new UserBiometric();
                                        biometricLeftMiddle.jpegData = leftFingers.LeftMiddle.image;
                                        biometricLeftMiddle.data = leftFingers.LeftMiddle.image;
                                        biometricLeftMiddle.imageQuality = leftFingers.LeftMiddle.quality;
                                        biometricLeftMiddle.position = 'LEFT_MIDDLE';
                                        biometricLeftMiddle.ansi = leftFingers.LeftMiddle.ansi;
                                        biometricLeftMiddle.format = leftFingers.imageType;
                                        this.leftFingerprintImages[2] = biometricLeftMiddle;
                                    } else {
                                        return;
                                    }
                                    if (leftFingers.LeftRing.image !== null) {
                                        const biometricLeftRing: UserBiometric = new UserBiometric();
                                        biometricLeftRing.jpegData = leftFingers.LeftRing.image;
                                        biometricLeftRing.data = leftFingers.LeftRing.image;
                                        biometricLeftRing.imageQuality = leftFingers.LeftRing.quality;
                                        biometricLeftRing.position = 'LEFT_RING';
                                        biometricLeftRing.ansi = leftFingers.LeftRing.ansi;
                                        biometricLeftRing.format = leftFingers.imageType;
                                        this.leftFingerprintImages[3] = biometricLeftRing;
                                    } else {
                                        return;
                                    }
                                    if (leftFingers.LeftLittle.image !== null) {
                                        const biometricLeftLittle: UserBiometric = new UserBiometric();
                                        biometricLeftLittle.jpegData = leftFingers.LeftLittle.image;
                                        biometricLeftLittle.data = leftFingers.LeftLittle.image;
                                        biometricLeftLittle.imageQuality = leftFingers.LeftLittle.quality;
                                        biometricLeftLittle.position = 'LEFT_LITTLE';
                                        biometricLeftLittle.ansi = leftFingers.LeftLittle.ansi;
                                        biometricLeftLittle.format = leftFingers.imageType;
                                        this.leftFingerprintImages[4] = biometricLeftLittle;
                                    } else {
                                        return;
                                    }
                                }
                            } else if (fingerModel.fPosition === 'FLAT_SLAP_2_THUMBS') {
                                const thumbFingers = response.fingerPrintDetails.ThumbFingers;
                                if (thumbFingers.noOfFingers !== '2') {
                                    this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
                                    this.translateService.get('enroll.messages.error.fingerprint.thumbFingers').subscribe(message => {
                                        this.fpContextErrorMsg = message;
                                    });
                                    // Retrying
                                    if (this.fpCaptureCalledCount < 3) {
                                        this.captureFP(fingerModel);
                                    } else {
                                        this.fingerModelObj = fingerModel;
                                        this.fpContextErrorMsg = '';
                                        this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(message => {
                                            this.fpContextWarningMsg = message;
                                        });
                                    }
                                    return;
                                } else {
                                    if (thumbFingers.image !== null) {
                                        if (
                                            !this.checkImageQualityMatchedOrNot(thumbFingers.LeftThumb.quality, fingerModel, 'LEFT_THUMB')
                                        ) {
                                            return;
                                        }
                                        if (
                                            !this.checkImageQualityMatchedOrNot(thumbFingers.RightThumb.quality, fingerModel, 'RIGHT_THUMB')
                                        ) {
                                            return;
                                        }
                                        const thumbFp: UserBiometric = new UserBiometric();
                                        thumbFp.jpegData = thumbFingers.image;
                                        thumbFp.data = thumbFingers.image;
                                        thumbFp.imageQuality = thumbFingers.quality;
                                        thumbFp.position = 'FLAT_SLAP_2_THUMBS';
                                        this.thumbFingers = thumbFp;
                                    } else {
                                        return;
                                    }
                                    if (thumbFingers.LeftThumb.image !== null) {
                                        const biometricLeftThumb: UserBiometric = new UserBiometric();
                                        biometricLeftThumb.jpegData = thumbFingers.LeftThumb.image;
                                        biometricLeftThumb.data = thumbFingers.LeftThumb.image;
                                        biometricLeftThumb.imageQuality = thumbFingers.LeftThumb.quality;
                                        biometricLeftThumb.position = 'LEFT_THUMB';
                                        biometricLeftThumb.ansi = thumbFingers.LeftThumb.ansi;
                                        biometricLeftThumb.format = thumbFingers.imageType;
                                        this.leftFingerprintImages[0] = biometricLeftThumb;
                                    } else {
                                        return;
                                    }
                                    if (thumbFingers.RightThumb.image !== null) {
                                        const biometricRightThumb: UserBiometric = new UserBiometric();
                                        biometricRightThumb.jpegData = thumbFingers.RightThumb.image;
                                        biometricRightThumb.data = thumbFingers.RightThumb.image;
                                        biometricRightThumb.imageQuality = thumbFingers.RightThumb.quality;
                                        biometricRightThumb.position = 'RIGHT_THUMB';
                                        biometricRightThumb.ansi = thumbFingers.RightThumb.ansi;
                                        biometricRightThumb.format = thumbFingers.imageType;
                                        this.rightFingerprintImages[0] = biometricRightThumb;
                                    } else {
                                        return;
                                    }
                                }
                            }
                        } else if (this.isTwoFPCaptured) {
                            ////////////// code for dual fingerprint
                            if (fingerModel.fPosition === 'INDEX_MIDDLE_LEFT_2') {
                                const leftIndex = response.fingerPrintDetails.Fingers;
                                if (leftIndex.noOfFingers !== '2') {
                                    this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
                                    this.translateService.get('enroll.messages.error.fingerprint.indexMiddleLeft').subscribe(message => {
                                        this.fpContextErrorMsg = message;
                                    });
                                    // Retrying
                                    if (this.fpCaptureCalledCount < 3) {
                                        this.captureFP(fingerModel);
                                    } else {
                                        this.fingerModelObj = fingerModel;
                                        this.fpContextErrorMsg = '';
                                        this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(message => {
                                            this.fpContextWarningMsg = message;
                                        });
                                    }
                                    return;
                                } else {
                                    if (leftIndex.image !== null) {
                                        if (!this.checkImageQualityMatchedOrNot(leftIndex.FirstFinger.quality, fingerModel, 'LEFT_INDEX')) {
                                            return;
                                        }
                                        if (
                                            !this.checkImageQualityMatchedOrNot(leftIndex.SecondFinger.quality, fingerModel, 'LEFT_MIDDLE')
                                        ) {
                                            return;
                                        }
                                        const leftIndexFp: UserBiometric = new UserBiometric();
                                        leftIndexFp.jpegData = leftIndex.image;
                                        leftIndexFp.data = leftIndex.image;
                                        leftIndexFp.imageQuality = leftIndex.quality;
                                        leftIndexFp.position = 'INDEX_MIDDLE_LEFT_2';
                                        this.indexMiddleLeft = leftIndexFp;
                                    } else {
                                        return;
                                    }
                                    if (leftIndex.FirstFinger.image !== null) {
                                        const biometricLeftIndex: UserBiometric = new UserBiometric();
                                        biometricLeftIndex.jpegData = leftIndex.FirstFinger.image;
                                        biometricLeftIndex.data = leftIndex.FirstFinger.image;
                                        biometricLeftIndex.imageQuality = leftIndex.FirstFinger.quality;
                                        biometricLeftIndex.position = 'LEFT_INDEX';
                                        biometricLeftIndex.ansi = leftIndex.FirstFinger.ansi;
                                        biometricLeftIndex.format = leftIndex.imageType;
                                        this.rightFingerprintImages[1] = biometricLeftIndex;
                                    } else {
                                        return;
                                    }
                                    if (leftIndex.SecondFinger.image !== null) {
                                        const biometricLeftMiddle: UserBiometric = new UserBiometric();
                                        biometricLeftMiddle.jpegData = leftIndex.SecondFinger.image;
                                        biometricLeftMiddle.data = leftIndex.SecondFinger.image;
                                        biometricLeftMiddle.imageQuality = leftIndex.SecondFinger.quality;
                                        biometricLeftMiddle.position = 'LEFT_MIDDLE';
                                        biometricLeftMiddle.ansi = leftIndex.SecondFinger.ansi;
                                        biometricLeftMiddle.format = leftIndex.imageType;
                                        this.rightFingerprintImages[2] = biometricLeftMiddle;
                                    } else {
                                        return;
                                    }
                                }
                            } else if (fingerModel.fPosition === 'RING_LITTLE_LEFT_2') {
                                const leftRing = response.fingerPrintDetails.Fingers;
                                if (leftRing.noOfFingers !== '2') {
                                    this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
                                    this.translateService.get('enroll.messages.error.fingerprint.ringLittleLeft').subscribe(message => {
                                        this.fpContextErrorMsg = message;
                                    });
                                    // Retrying
                                    if (this.fpCaptureCalledCount < 3) {
                                        this.captureFP(fingerModel);
                                    } else {
                                        this.fingerModelObj = fingerModel;
                                        this.fpContextErrorMsg = '';
                                        this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(message => {
                                            this.fpContextWarningMsg = message;
                                        });
                                    }
                                    return;
                                } else {
                                    if (leftRing.image !== null) {
                                        if (!this.checkImageQualityMatchedOrNot(leftRing.FirstFinger.quality, fingerModel, 'LEFT_RING')) {
                                            return;
                                        }
                                        if (
                                            !this.checkImageQualityMatchedOrNot(leftRing.SecondFinger.quality, fingerModel, 'LEFT_LITTLE')
                                        ) {
                                            return;
                                        }
                                        const leftRingFp: UserBiometric = new UserBiometric();
                                        leftRingFp.jpegData = leftRing.image;
                                        leftRingFp.data = leftRing.image;
                                        leftRingFp.imageQuality = leftRing.quality;
                                        leftRingFp.position = 'RING_LITTLE_LEFT_2';
                                        this.indexMiddleLeft = leftRingFp;
                                    } else {
                                        return;
                                    }
                                    if (leftRing.FirstFinger.image !== null) {
                                        const biometricLeftIndex: UserBiometric = new UserBiometric();
                                        biometricLeftIndex.jpegData = leftRing.FirstFinger.image;
                                        biometricLeftIndex.data = leftRing.FirstFinger.image;
                                        biometricLeftIndex.imageQuality = leftRing.FirstFinger.quality;
                                        biometricLeftIndex.position = 'LEFT_RING';
                                        biometricLeftIndex.ansi = leftRing.FirstFinger.ansi;
                                        biometricLeftIndex.format = leftRing.imageType;
                                        this.rightFingerprintImages[1] = biometricLeftIndex;
                                    } else {
                                        return;
                                    }
                                    if (leftRing.SecondFinger.image !== null) {
                                        const biometricLeftMiddle: UserBiometric = new UserBiometric();
                                        biometricLeftMiddle.jpegData = leftRing.SecondFinger.image;
                                        biometricLeftMiddle.data = leftRing.SecondFinger.image;
                                        biometricLeftMiddle.imageQuality = leftRing.SecondFinger.quality;
                                        biometricLeftMiddle.position = 'LEFT_LITTLE';
                                        biometricLeftMiddle.ansi = leftRing.SecondFinger.ansi;
                                        biometricLeftMiddle.format = leftRing.imageType;
                                        this.rightFingerprintImages[2] = biometricLeftMiddle;
                                    } else {
                                        return;
                                    }
                                }
                            } else if (fingerModel.fPosition === 'INDEX_MIDDLE_RIGHT_2') {
                                const rightIndex = response.fingerPrintDetails.Fingers;
                                if (rightIndex.noOfFingers !== '2') {
                                    this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
                                    this.translateService.get('enroll.messages.error.fingerprint.indexMiddleRight').subscribe(message => {
                                        this.fpContextErrorMsg = message;
                                    });
                                    // Retrying
                                    if (this.fpCaptureCalledCount < 3) {
                                        this.captureFP(fingerModel);
                                    } else {
                                        this.fingerModelObj = fingerModel;
                                        this.fpContextErrorMsg = '';
                                        this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(message => {
                                            this.fpContextWarningMsg = message;
                                        });
                                    }
                                    return;
                                } else {
                                    if (rightIndex.image !== null) {
                                        if (
                                            !this.checkImageQualityMatchedOrNot(rightIndex.FirstFinger.quality, fingerModel, 'RIGHT_INDEX')
                                        ) {
                                            return;
                                        }
                                        if (
                                            !this.checkImageQualityMatchedOrNot(
                                                rightIndex.SecondFinger.quality,
                                                fingerModel,
                                                'RIGHT_MIDDLE'
                                            )
                                        ) {
                                            return;
                                        }
                                        const rightIndexFp: UserBiometric = new UserBiometric();
                                        rightIndexFp.jpegData = rightIndex.image;
                                        rightIndexFp.data = rightIndex.image;
                                        rightIndexFp.imageQuality = rightIndex.quality;
                                        rightIndexFp.position = 'INDEX_MIDDLE_RIGHT_2';
                                        this.indexMiddleLeft = rightIndexFp;
                                    } else {
                                        return;
                                    }
                                    if (rightIndex.FirstFinger.image !== null) {
                                        const biometricightIndex: UserBiometric = new UserBiometric();
                                        biometricightIndex.jpegData = rightIndex.FirstFinger.image;
                                        biometricightIndex.data = rightIndex.FirstFinger.image;
                                        biometricightIndex.imageQuality = rightIndex.FirstFinger.quality;
                                        biometricightIndex.position = 'RIGHT_INDEX';
                                        biometricightIndex.ansi = rightIndex.FirstFinger.ansi;
                                        biometricightIndex.format = rightIndex.imageType;
                                        this.rightFingerprintImages[1] = biometricightIndex;
                                    } else {
                                        return;
                                    }
                                    if (rightIndex.SecondFinger.image !== null) {
                                        const biometricRightMiddle: UserBiometric = new UserBiometric();
                                        biometricRightMiddle.jpegData = rightIndex.SecondFinger.image;
                                        biometricRightMiddle.data = rightIndex.SecondFinger.image;
                                        biometricRightMiddle.imageQuality = rightIndex.SecondFinger.quality;
                                        biometricRightMiddle.position = 'RIGHT_MIDDLE';
                                        biometricRightMiddle.ansi = rightIndex.SecondFinger.ansi;
                                        biometricRightMiddle.format = rightIndex.imageType;
                                        this.rightFingerprintImages[2] = biometricRightMiddle;
                                    } else {
                                        return;
                                    }
                                }
                            } else if (fingerModel.fPosition === 'RING_LITTLE_RIGHT_2') {
                                const rightRing = response.fingerPrintDetails.Fingers;
                                if (rightRing.noOfFingers !== '2') {
                                    this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
                                    this.translateService.get('enroll.messages.error.fingerprint.ringLittleRight').subscribe(message => {
                                        this.fpContextErrorMsg = message;
                                    });
                                    // Retrying
                                    if (this.fpCaptureCalledCount < 3) {
                                        this.captureFP(fingerModel);
                                    } else {
                                        this.fingerModelObj = fingerModel;
                                        this.fpContextErrorMsg = '';
                                        this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(message => {
                                            this.fpContextWarningMsg = message;
                                        });
                                    }
                                    return;
                                } else {
                                    if (rightRing.image !== null) {
                                        if (!this.checkImageQualityMatchedOrNot(rightRing.FirstFinger.quality, fingerModel, 'RIGHT_RING')) {
                                            return;
                                        }
                                        if (
                                            !this.checkImageQualityMatchedOrNot(rightRing.SecondFinger.quality, fingerModel, 'RIGHT_LITTLE')
                                        ) {
                                            return;
                                        }
                                        const rightRingFp: UserBiometric = new UserBiometric();
                                        rightRingFp.jpegData = rightRing.image;
                                        rightRingFp.data = rightRing.image;
                                        rightRingFp.imageQuality = rightRing.quality;
                                        rightRingFp.position = 'RING_LITTLE_RIGHT_2';
                                        this.indexMiddleLeft = rightRingFp;
                                    } else {
                                        return;
                                    }
                                    if (rightRing.FirstFinger.image !== null) {
                                        const biometricRightIndex: UserBiometric = new UserBiometric();
                                        biometricRightIndex.jpegData = rightRing.FirstFinger.image;
                                        biometricRightIndex.data = rightRing.FirstFinger.image;
                                        biometricRightIndex.imageQuality = rightRing.FirstFinger.quality;
                                        biometricRightIndex.position = 'RIGHT_RING';
                                        biometricRightIndex.ansi = rightRing.FirstFinger.ansi;
                                        biometricRightIndex.format = rightRing.imageType;
                                        this.rightFingerprintImages[1] = biometricRightIndex;
                                    } else {
                                        return;
                                    }
                                    if (rightRing.SecondFinger.image !== null) {
                                        const biometricRightMiddle: UserBiometric = new UserBiometric();
                                        biometricRightMiddle.jpegData = rightRing.SecondFinger.image;
                                        biometricRightMiddle.data = rightRing.SecondFinger.image;
                                        biometricRightMiddle.imageQuality = rightRing.SecondFinger.quality;
                                        biometricRightMiddle.position = 'RIGHT_LITTLE';
                                        biometricRightMiddle.ansi = rightRing.SecondFinger.ansi;
                                        biometricRightMiddle.format = rightRing.imageType;
                                        this.rightFingerprintImages[2] = biometricRightMiddle;
                                    } else {
                                        return;
                                    }
                                }
                            } else if (fingerModel.fPosition === 'THUMBS_2') {
                                const thumbs = response.fingerPrintDetails.Fingers;
                                if (thumbs.noOfFingers !== '2') {
                                    this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
                                    this.translateService.get('enroll.messages.error.fingerprint.thumbsTwo').subscribe(message => {
                                        this.fpContextErrorMsg = message;
                                    });
                                    // Retrying
                                    if (this.fpCaptureCalledCount < 3) {
                                        this.captureFP(fingerModel);
                                    } else {
                                        this.fingerModelObj = fingerModel;
                                        this.fpContextErrorMsg = '';
                                        this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(message => {
                                            this.fpContextWarningMsg = message;
                                        });
                                    }
                                    return;
                                } else {
                                    if (thumbs.image !== null) {
                                        if (!this.checkImageQualityMatchedOrNot(thumbs.FirstFinger.quality, fingerModel, 'RIGHT_THUMB')) {
                                            return;
                                        }
                                        if (!this.checkImageQualityMatchedOrNot(thumbs.SecondFinger.quality, fingerModel, 'RIGHT_THUMB')) {
                                            return;
                                        }
                                        const thumbFp: UserBiometric = new UserBiometric();
                                        thumbFp.jpegData = thumbs.image;
                                        thumbFp.data = thumbs.image;
                                        thumbFp.imageQuality = thumbs.quality;
                                        thumbFp.position = 'THUMBS_2';
                                        this.indexMiddleLeft = thumbFp;
                                    } else {
                                        return;
                                    }
                                    if (thumbs.FirstFinger.image !== null) {
                                        const biometricLeftThumbs: UserBiometric = new UserBiometric();
                                        biometricLeftThumbs.jpegData = thumbs.FirstFinger.image;
                                        biometricLeftThumbs.data = thumbs.FirstFinger.image;
                                        biometricLeftThumbs.imageQuality = thumbs.FirstFinger.quality;
                                        biometricLeftThumbs.position = 'LEFT_THUMB';
                                        biometricLeftThumbs.ansi = thumbs.FirstFinger.ansi;
                                        biometricLeftThumbs.format = thumbs.imageType;
                                        this.rightFingerprintImages[1] = biometricLeftThumbs;
                                    } else {
                                        return;
                                    }
                                    if (thumbs.SecondFinger.image !== null) {
                                        const biometricRightThumbs: UserBiometric = new UserBiometric();
                                        biometricRightThumbs.jpegData = thumbs.SecondFinger.image;
                                        biometricRightThumbs.data = thumbs.SecondFinger.image;
                                        biometricRightThumbs.imageQuality = thumbs.SecondFinger.quality;
                                        biometricRightThumbs.position = 'RIGHT_THUMB';
                                        biometricRightThumbs.ansi = thumbs.SecondFinger.ansi;
                                        biometricRightThumbs.format = thumbs.imageType;
                                        this.rightFingerprintImages[2] = biometricRightThumbs;
                                    } else {
                                        return;
                                    }
                                }
                            }
                            ///////
                        } else if (this.isSingleFPCaptured) {
                            const singleFinger = response.fingerPrintDetails.SingleFinger;
                            if (Object.keys(singleFinger).length === 0) {
                                this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
                                this.translateService.get('enroll.messages.error.fingerprint.captureTimedOut').subscribe(message => {
                                    this.fpContextErrorMsg = message;
                                });
                                // Retrying
                                if (this.fpCaptureCalledCount <= 3) {
                                    this.captureFP(fingerModel);
                                } else {
                                    this.fingerModelObj = fingerModel;
                                    this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(message => {
                                        this.fpContextWarningMsg = message;
                                    });
                                }
                            }
                            if (singleFinger.image != null) {
                                if (!this.checkImageQualityMatchedOrNot(singleFinger.quality, fingerModel, fingerModel.fPosition)) {
                                    return;
                                }
                                const biometric: UserBiometric = new UserBiometric();
                                biometric.position = fingerModel.fPosition;
                                biometric.data = singleFinger.image;
                                biometric.jpegData = singleFinger.image;
                                biometric.imageQuality = singleFinger.quality;
                                biometric.ansi = singleFinger.ansi;
                                biometric.format = singleFinger.imageType;
                                if (this.isWsqFormatEnabled) {
                                    biometric.wsqData = singleFinger.wsqImage;
                                }
                                if (!this.isRollFpCaptureSelected) {
                                    if (this.index < 5) {
                                        this.leftFingerprintImages[this.index] = biometric;
                                    } else {
                                        this.rightFingerprintImages[this.index - 5] = biometric;
                                    }
                                } else {
                                    console.log(this.index);
                                    if (this.index < 5) {
                                        this.leftRollFingerprintImages[this.index] = biometric;
                                    } else {
                                        this.rightRollFingerprintImages[this.index - 5] = biometric;
                                    }
                                }
                            }
                        }
                        const currentCaptureFP = fingerModel.fPosition;
                        // If another finger is captured and actual finger is skipped
                        if (this.isFPRecaptureSelected) {
                            this.hideCapturePosition();
                            this.fpContextInfoMsg = '';
                            this.fpContextWarningMsg = '';
                            this.fpContextErrorMsg = '';
                            this.deviceServiceStatus = '';
                            this.translateService.get('enroll.messages.success.fingerprint.reCaptureSucces').subscribe(message => {
                                this.fpContextSuccessMsg = message;
                            });
                            fingerModel = new FingerModel();
                            this.currentFingerPosition = '';
                            this.isFPRecaptureSelected = false;
                            return;
                        } else {
                            if (this.minRequiredFingerPositions.find(e => e === fingerModel.fPosition) !== undefined) {
                                if (this.isRollFpCaptureSelected) {
                                    this.fingerprintCapturedPositionsInRollFinger.push(fingerModel.fPosition);
                                } else {
                                    this.fingerprintCapturedPositions.push(fingerModel.fPosition);
                                }
                                this.newlyAddedFpsCapturePositions.push(fingerModel.fPosition);
                            }
                        }
                        let isCapturenextFinger = true;
                        if (this.isRollFpCaptureSelected) {
                            console.log(
                                this.minRequiredFingerPositions.length,
                                this.fingerprintCapturedPositionsInRollFinger.length >= this.minRequiredFingerPositions.length
                            );
                            if (this.fingerprintCapturedPositionsInRollFinger.length >= this.minRequiredFingerPositions.length) {
                                isCapturenextFinger = false;
                            }
                        } else {
                            if (this.fingerprintCapturedPositions.length >= this.minRequiredFingerPositions.length) {
                                isCapturenextFinger = false;
                            }
                        }
                        // Capture Next Finger
                        if (!isCapturenextFinger) {
                            // If Last finger captured , hide blink animation
                            if ((this.isEnrolledUser || this.isEnrollmentInProgress) && this.isFPConfigEditedInWf) {
                                this.isFPConfigEditedInWf = false;
                                this.enableFPRecaptureButton = true;
                            }
                            this.hideCapturePosition();
                            this.fpContextInfoMsg = '';
                            this.fpContextWarningMsg = '';
                            this.fpContextErrorMsg = '';
                            this.deviceServiceStatus = '';
                            this.translateService.get('enroll.messages.success.fingerprint.captureCompleted').subscribe(message => {
                                this.fpContextSuccessMsg = message;
                            });
                            fingerModel = new FingerModel();
                            this.enableFPRecaptureButton = true;
                            this.isFPAddedNewlyInWf = false;
                            if (!this.isRollFpCaptureSelected) {
                                this.isFpAutoCaptureCompleted = true;
                                this.isClearFpCalled = false;
                            } else {
                                this.isClearFpCalledInRollFinger = false;
                                this.isRollFpAutoCaptureCompleted = true;
                            }
                            return;
                        } else {
                            console.log('capture next finger::::', 'is fp eonfig edited inwf::', this.isFPConfigEditedInWf);
                            this.fpContextErrorMsg = '';
                            this.fpContextWarningMsg = '';
                            if (this.isFPConfigEditedInWf) {
                                const list = this.newlyAddedFpsCapturePositions.find(
                                    e1 => !this.fingerprintCapturedPositions.find(e2 => e2 === e1)
                                );
                                if (list !== undefined) {
                                    this.currentFingerPosition = this.minRequiredFingerPositions[
                                        this.minRequiredFingerPositions.indexOf(list)
                                    ];
                                    console.log(this.currentFingerPosition);
                                } else {
                                    this.hideCapturePosition();
                                }
                            } else {
                                console.log(this.isRollFpCaptureSelected);
                                if (!this.isRollFpCaptureSelected) {
                                    this.currentFingerPosition = this.minRequiredFingerPositions[this.fingerprintCapturedPositions.length];
                                } else {
                                    console.log(this.fingerprintCapturedPositionsInRollFinger);
                                    this.currentFingerPosition = this.minRequiredFingerPositions[
                                        this.fingerprintCapturedPositionsInRollFinger.length
                                    ];
                                    console.log(this.currentFingerPosition);
                                }
                            }
                            fingerModel.fPosition = this.currentFingerPosition;
                            this.fpCaptureCalledCount = 0;
                            this.translateService
                                .get('enroll.messages.success.fingerprint.captureSuccess', { fingerPosition: currentCaptureFP })
                                .subscribe(message => {
                                    this.fpContextInfoMsg = message; // `${currentCaptureFP} captured successfully;`;
                                });
                            if (!this.isRollFpCaptureSelected) {
                                this.isClearFpEnabled = true;
                                this.disableRollFpDeviceSelection = true;
                            } else {
                                this.isClearRollFpEnabled = true;
                                this.disableFpDeviceSelection = true;
                            }
                            this.hideCapturePosition();
                            setTimeout(() => {
                                this.captureFP(fingerModel);
                            }, 2000);
                        }
                    } else {
                        // this.translateService.get('enroll.messages.error.fingerprint.unknownErrorFromDeviceService').subscribe(message => {
                        //     this.fpContextInfoMsg = '';
                        //     this.fpContextErrorMsg = message;
                        // });
                        // if (this.fpReaderSubscription) {
                        //     this.fpReaderSubscription.unsubscribe();
                        // }
                        // this.getConnectedFingerprintReaders();

                        this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
                        this.translateService.get('enroll.messages.error.fingerprint.captureTimedOut').subscribe(message => {
                            this.fpContextErrorMsg = message;
                        });
                        // Retrying
                        if (this.fpCaptureCalledCount <= 3) {
                            this.captureFP(fingerModel);
                        } else {
                            this.fingerModelObj = fingerModel;
                            this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(message => {
                                this.fpContextWarningMsg = message;
                            });
                        }
                    }
                },
                err => {
                    this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                        this.deviceServiceStatus = '';
                        this.deviceServiceStatus = serviceErrMessage;
                        this.fpContextInfoMsg = '';
                        this.hideCapturePosition();
                        // Retrying
                        if (this.fpReaderSubscription) {
                            this.fpReaderSubscription.unsubscribe();
                        }
                        // setTimeout(() => {
                        this.getConnectedFingerprintReaders();
                        // }, 5000);
                    });
                }
            );
    }

    checkImageQualityMatchedOrNot(imageQuality, fingerModel: FingerModel, fingerName: string): boolean {
        if (this.workflowImageQuality <= parseInt(imageQuality, 10)) {
            this.fpContextErrorMsg = '';
            this.fpContextWarningMsg = '';
            console.log('check image quality callled::::', true);
            return true;
        } else {
            this.fpCaptureCalledCount = this.fpCaptureCalledCount + 1;
            this.fpContextErrorMsg = '';
            this.translateService
                .get('enroll.messages.warn.fingerprint.invalidImageQuality', {
                    fingerPosition: `${fingerName}`,
                    imageQuality: `${imageQuality}`
                })
                .subscribe(message => {
                    this.fpContextWarningMsg = message + this.workflowImageQuality;
                    if (this.fpCaptureCalledCount < 3) {
                        this.captureFP(fingerModel);
                    } else {
                        this.fingerModelObj = fingerModel;
                        this.fpContextErrorMsg = '';
                        this.fpContextWarningMsg = '';
                        this.translateService.get('enroll.messages.warn.fingerprint.retriesExceeded').subscribe(warnMessage => {
                            this.fpContextWarningMsg = warnMessage;
                        });
                    }
                });
            console.log('check image quality callled::::', false);
            return false;
        }
    }
    resetValuesForRollFp(): void {
        this.currentFingerPosition = '';
        this.fingerprintCapturedPositionsInRollFinger = [];
        this.leftRollFingerprintImages = [];
        this.rightRollFingerprintImages = [];
        this.fpContextInfoMsg = '';
        this.fpContextWarningMsg = '';
        this.fpContextErrorMsg = '';
        this.fpContextSuccessMsg = '';
        this.isSingleFPCaptured = true;
        this.isSlapFPCaptured = false;
        // this.fingerModelInEdit = new FingerModel();
    }
    resetValues(): void {
        this.currentFingerPosition = '';
        this.fingerprintCapturedPositions = [];
        this.leftFingerprintImages = [];
        this.rightFingerprintImages = [];
        this.fpContextInfoMsg = '';
        this.fpContextWarningMsg = '';
        this.fpContextErrorMsg = '';
        this.fpContextSuccessMsg = '';
        this.isSingleFPCaptured = true;
        this.isSlapFPCaptured = false;
        this.fingerModelInEdit = new FingerModel();
    }
    skipFinger(): void {
        // Unsubscribe current capture request.
        if (this.fingerprintSubscriber) {
            this.fingerprintSubscriber.unsubscribe();
        }
    }

    skipFingerCancel(): void {
        // Start again
        // this.captureFP(this.currentFingerPosition);
    }

    skipFingerSave(): void {
        const currIndex = this.fingerPositions.indexOf(this.currentFingerPosition);

        // Record Skipped Finger position
        if (this.minRequiredFingerPositions.includes(this.currentFingerPosition)) {
            this.fingerprintCapturedPositions.push(this.currentFingerPosition);
        }

        // Capture Skip Reason Details
        const userBiometricSkip: UserBiometricSkip = new UserBiometricSkip();
        userBiometricSkip.type = 'FINGERPRINT';
        userBiometricSkip.position = this.currentFingerPosition;
        userBiometricSkip.skipType = this.skipType.value;
        userBiometricSkip.skipReason = this.skipReason.value;

        this.skippedFingers[currIndex] = userBiometricSkip;

        // Clear Values
        this.skipType.setValue('');
        this.skipReason.setValue('');

        // If Last Finger skipped, no need to start the capture loop
        if (currIndex + 1 === this.fingerPositions.length) {
            return;
        }

        // Skip Current and continue to capture next
        // this.captureFP(this.fingerPositions[this.fingerPositions.indexOf(this.currentFingerPosition) + 1]);
    }

    setCurrentCapturePosition(fingerPosition: string): void {
        if (fingerPosition === this.fingerPositions[0]) {
            $(document).ready(function() {
                $('.blink').css({ top: '39%', left: '43%' });
            });
        } else if (fingerPosition === this.fingerPositions[1]) {
            $(document).ready(function() {
                $('.blink').css({ top: '8%', left: '36%' });
            });
        } else if (fingerPosition === this.fingerPositions[2]) {
            $(document).ready(function() {
                $('.blink').css({ top: '1%', left: '25%' });
            });
        } else if (fingerPosition === this.fingerPositions[3]) {
            $(document).ready(function() {
                $('.blink').css({ top: '7%', left: '15%' });
            });
        } else if (fingerPosition === this.fingerPositions[4]) {
            $(document).ready(function() {
                $('.blink').css({ top: '22%', left: '7%' });
            });
        } else if (fingerPosition === this.fingerPositions[5]) {
            $(document).ready(function() {
                $('.blink').css({ top: '39%', left: '49%' });
            });
        } else if (fingerPosition === this.fingerPositions[6]) {
            $(document).ready(function() {
                $('.blink').css({ top: '8%', left: '56%' });
            });
        } else if (fingerPosition === this.fingerPositions[7]) {
            $(document).ready(function() {
                $('.blink').css({ top: '1%', left: '67%' });
            });
        } else if (fingerPosition === this.fingerPositions[8]) {
            $(document).ready(function() {
                $('.blink').css({ top: '7%', left: '79%' });
            });
        } else if (fingerPosition === this.fingerPositions[9]) {
            $(document).ready(function() {
                $('.blink').css({ top: '22%', left: '88%' });
            });
        } else if (fingerPosition === this.slapFingerPositions[0]) {
            $(document).ready(function() {
                $('.rectangle').css({ top: '20px', left: '8%' });
                $('.redDot').css({ top: '2%', left: '4%' });
            });
        } else if (fingerPosition === this.slapFingerPositions[1]) {
            $(document).ready(function() {
                $('.rectangle').css({ top: '20px', left: '56%' });
                $('.redDot').css({ top: '8px', left: '91%' });
            });
        } else if (fingerPosition === this.slapFingerPositions[2]) {
            $(document).ready(function() {
                $('.rectangle').css({ top: '135px', left: '39%', width: '100px', height: '50px' });
                $('.redDot').css({ top: '110px', left: '46%' });
            });
        } else if (fingerPosition === this.dualFingerPositions[0]) {
            //////// for dual Fingerprints
            $(document).ready(function() {
                $('.rectangle').css({ top: '23px', left: '24%', width: '100px', height: '58px' });
                $('.redDot').css({ top: '18px', left: '41.5%' });
            });
        } else if (fingerPosition === this.dualFingerPositions[1]) {
            $(document).ready(function() {
                $('.rectangle').css({ top: '47px', left: '5%', width: '100px', height: '58px' });
                $('.redDot').css({ top: '23px', left: '9%' });
            });
        } else if (fingerPosition === this.dualFingerPositions[2]) {
            $(document).ready(function() {
                $('.rectangle').css({ top: '26px', left: '56%', width: '100px', height: '50px' });
                $('.redDot').css({ top: '10px', left: '52%' });
            });
        } else if (fingerPosition === this.dualFingerPositions[3]) {
            $(document).ready(function() {
                $('.rectangle').css({ top: '49px', left: '75%', width: '100px', height: '50px' });
                $('.redDot').css({ top: '23px', left: '87%' });
            });
        } else if (fingerPosition === this.dualFingerPositions[4]) {
            $(document).ready(function() {
                $('.rectangle').css({ top: '135px', left: '39%', width: '100px', height: '50px' });
                $('.redDot').css({ top: '110px', left: '46%' });
            });
        } ////
    }

    hideCapturePosition(): void {
        $(document).ready(function() {
            $('.blink').css({ top: '-300%', left: '92%' });
            $('.rectangle').css({ top: '-300%', left: '92%' });
            $('.redDot').css({ top: '-300%', left: '92%' });
        });
    }

    moveRectangleRight() {
        $(document).ready(function() {
            $('.rectangle').css({ top: '20px', left: '56%' });
            $('.redDot').css({ top: '8px', left: '91%' });
        });
    }

    moveRectangleMiddle() {
        $(document).ready(function() {
            $('.rectangle').css({ top: '135px', left: '39%', width: '100px', height: '50px' });
            $('.redDot').css({ top: '110px', left: '46%' });
        });
    }

    // ----------------------------------------Capture Id Proofing Starts--------------------------------
    public handleImageId(webcamImageId: WebcamImage): void {
        this.webcamImageId = webcamImageId;
    }

    editIdProofDoc(userDocument1: UserIDProof) {
        const userDocument = userDocument1;
        this.isEditIdProofDocSelected = true;
        this.editIdProofDocument = userDocument;
        this.captureDocumentVisibility();
        if (userDocument.source === 'capture') {
            this.isCroppedId = true;
            // this.isCroppedIdBack = true;
            this.isCroppedIdBack = true;
            this.croppedImgId = userDocument.frontImg;
            this.croppedImgIdBack = userDocument.backImg;
            userDocument.frontFileType = '';
            userDocument.backFileType = '';
            userDocument.frontFileName = '';
            userDocument.backFileName = '';
            userDocument.file = '';
        } else if (userDocument.source === 'upload') {
            userDocument.frontFileType = userDocument.frontFileType;
            userDocument.backFileType = userDocument.backFileType;
            userDocument.frontFileName = userDocument.frontFileName;
            userDocument.backFileName = userDocument.backFileName;
        }
        userDocument.idProofType = this.idProofs.find(e => e.id === userDocument.idProofType.id);
        this.enrollService.getIssuingAuthorities(userDocument.idProofType.id).subscribe(result => {
            this.issueAuthorities = result;
            userDocument.idProofIssuingAuthority = this.issueAuthorities.find(e => e.id === userDocument.idProofIssuingAuthority.id);
            this.IDProofDocForm.setValue({
                idProofDocType: userDocument.idProofType,
                issuingAuthority: userDocument.idProofIssuingAuthority,
                source: userDocument.source,
                selectedWebCamDevice: '',
                frontImg: userDocument.frontImg,
                backImg: userDocument.backImg,
                file: userDocument.file,
                pdfFile: userDocument.file,
                //  fileName: userDocument.fileName,
                frontFileName: userDocument.frontFileName,
                backFileName: userDocument.backFileName,
                frontFileType: userDocument.frontFileType,
                backFileType: userDocument.backFileType,
                imgURL: ''
            });
        });
    }
    onIDProofSourceChange(event) {
        if (event.value === 'capture') {
            WebcamUtil.getAvailableVideoInputs().then((mediaDevicesId: MediaDeviceInfo[]) => {
                this.multipleWebcamsAvailable = mediaDevicesId && mediaDevicesId.length > 1;
                this.availableMediaDevicesId = mediaDevicesId;
                if (this.multipleWebcamsAvailable) {
                    this.allowCameraSwitchIDProof = true;
                }
                this.deviceId = mediaDevicesId[0].deviceId;
            });
            this.IDProofDocForm.controls['file'].setValue(null);
            this.preview = false;
            this.isPdfChoosen = false;
            this.checkCamPermission();
        } else if (event.value === 'upload') {
            this.idProofDocSaveError = false;
            this.idProofCamAccessError = false;
            this.IDProofDocForm.controls['frontImg'].reset();
            this.IDProofDocForm.controls['backImg'].reset();
            this.isCapturedId = false;
            this.isCapturedIdBack = false;
            this.isCroppedId = false;
            this.isCroppedIdBack = false;
            this.capturedImgId = [];
            this.capturedImgIdBack = [];
            this.croppedImgId = undefined;
            this.croppedImgIdBack = undefined;
            this.frontCapture = true;
            this.backCapture = false;
            if (this.camSubscription) {
                this.camSubscription.unsubscribe();
            }
        }
    }
    checkCamPermission() {
        this.camSubscription = timer(0, 2000)
            .pipe(switchMap(() => (navigator as any).permissions.query({ name: 'camera' })))
            .subscribe(
                (permissionObj: any) => {
                    if (permissionObj.state === 'granted') {
                        WebcamUtil.getAvailableVideoInputs().then((mediaDevicesId: MediaDeviceInfo[]) => {
                            this.multipleWebcamsAvailable = mediaDevicesId && mediaDevicesId.length > 1;
                            this.availableMediaDevicesId = mediaDevicesId;
                            if (this.multipleWebcamsAvailable) {
                                this.allowCameraSwitchIDProof = true;
                            }
                            this.deviceId = mediaDevicesId[0].deviceId;
                        });
                        this.camSubscription.unsubscribe();
                    } else if (permissionObj.state === 'denied') {
                        this.camSubscription.unsubscribe();
                    }
                },
                error => {
                    console.log(error);
                    if (error.name === 'NotAllowedError') {
                        console.log('user denied access to camera');
                    }
                    this.camSubscription.unsubscribe();
                }
            );
    }
    onIdProofDocTypeChange(event) {
        this.idProofDocFileError = false;
        this.idProofDocSaveError = false;
        this.idProofCamAccessError = false;
        this.IDProofDocForm.controls['issuingAuthority'].reset();
        this.enrollService.getIssuingAuthorities(event.value.id).subscribe(result => {
            this.issueAuthorities = result;
        });
    }
    deleteIdProof(IDProofDoc) {
        this.translateService.get('enroll.messages.info.delIdProofDocConfirmMsg').subscribe(msg => {
            const dialogRef = this.dialog.open(ReActivateDialogComponent, {
                autoFocus: false,
                width: '500px',
                data: {
                    isShowPUK: false,
                    dialogMsg: msg
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result === 'OK') {
                    this.userIDProofList = this.userIDProofList.filter(e => e.docId !== IDProofDoc.docId);
                    this.dataSource = new MatTableDataSource<UserIDProof>(this.userIDProofList);
                }
            });
        });
    }
    saveIDProofDoc() {
        this.error = false;
        if (this.camSubscription) {
            this.camSubscription.unsubscribe();
        }
        if (this.idProofDocFileError) {
            this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
            return;
        }
        this.idProofDocSaveError = false;
        this.idProofCamAccessError = false;
        let frontImg;
        let backImg;
        let file;
        // const fileType = this.IDProofDocForm.controls['fileType'].value;
        const frontFileType = this.IDProofDocForm.controls['frontFileType'].value;
        const backFileType = this.IDProofDocForm.controls['backFileType'].value;
        const selectedIdProofType = this.IDProofDocForm.controls['idProofDocType'].value.identityTypeName;

        if (this.userIDProofList.filter(e => e.idProofType.identityTypeName === selectedIdProofType).length > 0) {
            this.idProofDocSaveErrorMsg = 'enroll.messages.error.duplicateDoc';
            this.idProofDocSaveError = true;

            // Duplicate Document Type
            this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
            return;
        }
        if (this.IDProofDocForm.invalid) {
            this.idProofDocSaveErrorMsg = 'enroll.messages.error.idProofDocSelectFields';
            this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
            this.idProofDocSaveError = true;
            return;
        }
        frontImg = this.IDProofDocForm.value.frontImg;
        backImg = this.IDProofDocForm.value.backImg;
        if (this.IDProofDocForm.value.source === 'capture') {
            if (frontImg === null) {
                this.idProofDocSaveErrorMsg = 'enroll.messages.error.idProofDocCaptureCropFrontImage';
                this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
                this.idProofDocSaveError = true;
                return;
            }
            // if (backImg === null && selectedIdProofType === 'Driving License') {
            //     this.idProofDocSaveErrorMsg = 'enroll.messages.error.idProofDocCaptureCropBackImage';
            //     this.idProofDocSaveError = true;
            //     this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
            //     return;
            // }
        } else if (this.IDProofDocForm.value.source === 'upload') {
            file = this.IDProofDocForm.value.file;
            if (this.IDProofDocForm.controls['idProofDocType'].value.identityTypeDetails.includes('pdf')) {
                // if (selectedIdProofType === 'Birth Certificate') {
                if (file === null && frontImg === null) {
                    this.idProofDocSaveErrorMsg = 'enroll.messages.error.idProofDocUploadIdProofdoc';
                    this.idProofDocSaveError = true;
                    this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
                    return;
                }
            } else {
                if (frontImg === null) {
                    this.idProofDocSaveErrorMsg = 'enroll.messages.error.idProofDocUploadIdProofdoc';
                    this.idProofDocSaveError = true;
                    this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
                    return;
                }
            }
            // else {
            //     if (frontImg === null) {
            //         this.idProofDocSaveErrorMsg = 'enroll.messages.error.idProofDocCaptureCropFrontImage';
            //         this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
            //         this.idProofDocSaveError = true;
            //         return;
            //     }
            // if (backImg === null) {
            //     this.idProofDocSaveErrorMsg = 'enroll.messages.error.idProofDocCaptureCropBackImage';
            //     this.idProofDocSaveError = true;
            //     this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
            //     return;
            // }
            //  }
        }
        const userIDProof = new UserIDProof();
        if (this.isEditIdProofDocSelected) {
            this.userIDProofList = this.userIDProofList.filter(e => e.docId !== this.editIdProofDocument.docId);
            userIDProof.id = this.editIdProofDocument.id;
            userIDProof.docId = this.editIdProofDocument.docId;
        } else {
            userIDProof.docId = ++this.docId;
        }
        userIDProof.idProofType = this.IDProofDocForm.value.idProofDocType;
        userIDProof.idProofTypeName = this.IDProofDocForm.value.idProofDocType.identityTypeName;
        userIDProof.idProofTypeId = this.IDProofDocForm.value.idProofDocType.id;
        userIDProof.idProofIssuingAuthority = this.IDProofDocForm.value.issuingAuthority;
        userIDProof.issuingAuthority = this.IDProofDocForm.value.issuingAuthority.issuingAuthority;
        userIDProof.issuingAuthorityId = this.IDProofDocForm.value.issuingAuthority.id;
        userIDProof.name = `${this.userToBeEnrolled.username}_${userIDProof.idProofTypeName}`;
        userIDProof.frontImg = this.IDProofDocForm.value.frontImg;
        userIDProof.backImg = this.IDProofDocForm.value.backImg;
        userIDProof.source = this.IDProofDocForm.value.source;
        userIDProof.file = this.IDProofDocForm.value.file;
        userIDProof.pdfFile = this.IDProofDocForm.value.pdfFile;
        // userIDProof.fileType = this.IDProofDocForm.value.fileType;
        userIDProof.frontFileType = this.IDProofDocForm.value.frontFileType;
        userIDProof.backFileType = this.IDProofDocForm.value.backFileType;
        //  userIDProof.fileName = this.IDProofDocForm.value.fileName;
        userIDProof.frontFileName = this.IDProofDocForm.value.frontFileName;
        userIDProof.backFileName = this.IDProofDocForm.value.backFileName;
        this.userIDProofList.push(userIDProof);
        this.dataSource = new MatTableDataSource<UserIDProof>(this.userIDProofList);
        this.dataSource.sort = this.sort;
        this.captureDocument = false;

        this.hideIDproofCapture = true;
        const $cam = $('#idProofCam');
        $cam.hide = true;
        this.hiddingNextButton = false;
    }
    onUploadFile(event, captureType) {
        this.idProofDocSaveError = false;
        this.idProofCamAccessError = false;
        this.idProofDocFileError = false;
        if (event.target.files.length === 0) {
            return;
        } else {
            const reader = new FileReader();
            const [file] = event.target.files;
            const fileName = event.target.files[0].name;
            const fileType: string = event.target.files[0].type;
            // fileType.match('/image\/.*/')
            if (fileType === 'application/pdf' || fileType === 'image/png' || fileType === 'image/jpg' || fileType === 'image/jpeg') {
                if (fileType === 'application/pdf') {
                    // if (this.IDProofDocForm.controls['idProofDocType'].value.identityTypeName === 'Birth Certificate') {
                    if (this.IDProofDocForm.controls['idProofDocType'].value.identityTypeDetails.includes('pdf')) {
                        //  checking the size of the uploaded file, 1mb = 1048576 bytes
                        if (event.target.files[0].size > 1048576 * 5) {
                            this.idProofDocSaveErrorMsg = 'enroll.messages.error.idProofDocFileSizeExceed';
                            this.idProofDocSaveError = true;
                            this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
                            return;
                        }
                    } else {
                        this.idProofDocSaveErrorMsg = 'enroll.messages.error.notAcceptedFile';
                        this.idProofDocFileError = true;
                        this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
                        return;
                    }
                }
                reader.readAsDataURL(file);
                reader.onload = () => {
                    if (captureType === 'front') {
                        if (fileType === 'application/pdf') {
                            this.IDProofDocForm.controls['file'].setValue(reader.result.toString());
                            this.IDProofDocForm.controls['pdfFile'].setValue(file);
                        } else {
                            this.IDProofDocForm.controls['frontImg'].setValue(reader.result.toString());
                        }
                        this.IDProofDocForm.controls['frontFileName'].setValue(fileName);
                        this.IDProofDocForm.controls['frontFileType'].setValue(fileType);
                    } else if (captureType === 'back') {
                        this.IDProofDocForm.controls['backImg'].setValue(reader.result.toString());
                        this.IDProofDocForm.controls['backFileName'].setValue(fileName);
                        this.IDProofDocForm.controls['backFileType'].setValue(fileType);
                    }
                    //   this.IDProofDocForm.controls['fileName'].setValue(fileName);
                };
            } else {
                this.idProofDocSaveErrorMsg = 'enroll.messages.error.notAcceptedFile';
                this.idProofDocFileError = true;
                this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
                return;
            }
        }
    }

    downloadPdfFile(file, name) {
        if (typeof file === 'object') {
            const downloadLink = document.createElement('a');
            downloadLink.style.display = 'none';
            document.body.appendChild(downloadLink);
            downloadLink.setAttribute('href', window.URL.createObjectURL(file));
            downloadLink.setAttribute('download', name);
            downloadLink.click();
            document.body.removeChild(downloadLink);
            return;
        }
        const downloadLink1 = document.createElement('a');
        const fileName = 'sample.pdf';
        downloadLink1.href = file;
        downloadLink1.download = name;
        downloadLink1.click();
    }
    previewDesign(files) {
        this.isPdfChoosen = false;
        if (files.length === 0) {
            return;
        }
        const mimeType = files[0].type;
        // if (mimeType.match(/image\/.*/) == null) {
        //   console.log('returned::::');
        // return;
        // }
        if (mimeType === 'application/pdf') {
            this.isPdfChoosen = true;
        } else if (mimeType.match(/image\/.*/) == null) {
        }
        const reader = new FileReader();
        reader.readAsDataURL(files[0]);
        reader.onload = _event => {
            this.IDProofDocForm.controls['imgURL'].setValue(reader.result);
            this.preview = true;
            //  this.imgURL = reader.result;
        };
    }

    public triggerSnapshotId(imgType): void {
        // Cropper
        const options = {
            // viewMode: 2,
            initialAspectRatio: 2 / 2,
            dragMode: 'move',
            toggleDragModeOnDblclick: false,
            preview: '.img-preview',
            zoomOnWheel: true
        };
        if (imgType === 'front') {
            this.isCapturedId = true;
            this.triggerId.next();
            this.capturedImgId = this.webcamImageId.imageAsDataUrl;

            const $imageId = $('#imageId');
            $imageId.cropper(options);
            $imageId.cropper('replace', this.capturedImgId);
            this.isCroppedId = false;
        }
        if (imgType === 'back') {
            this.isCapturedId = true;
            this.isCapturedIdBack = true;
            this.triggerId.next();
            this.capturedImgIdBack = this.webcamImageId.imageAsDataUrl;

            const $imageIdBack = $('#imageIdBack');
            $imageIdBack.cropper(options);
            $imageIdBack.cropper('replace', this.capturedImgIdBack);
            this.isCroppedIdBack = false;
        }
    }
    zoomin(id) {
        $(`#${id}`).cropper('zoom', 0.1);
    }
    zoomout(id) {
        $(`#${id}`).cropper('zoom', -0.1);
    }
    public cropSnapshotId(imgType): void {
        if (imgType === 'front') {
            this.isCapturedIdBack = false;
            this.isCroppedId = true;
            this.croppedImgId = $('#imageId')
                .cropper('getCroppedCanvas', { width: 600, height: 600 })
                .toDataURL('image/png');
            this.isCapturedId = false;
            this.capturedImgId = null;
            $('#imageId')
                .siblings()
                .remove();
            this.IDProofDocForm.controls['frontFileType'].setValue('image/png');
            this.IDProofDocForm.controls['frontImg'].setValue(this.croppedImgId);
            this.frontCapture = false;
            if (!this.recaptureIdProofDoc || this.croppedImgIdBack === undefined) {
                this.backCapture = true;
                //  this.isCroppedIdBack = false;
            } else {
                this.hideIDproofCapture = true;
            }
        }
        if (imgType === 'back') {
            this.isCapturedId = false;
            this.isCroppedIdBack = true;
            this.croppedImgIdBack = $('#imageIdBack')
                .cropper('getCroppedCanvas', { width: 600, height: 600 })
                .toDataURL('image/png');
            this.isCapturedIdBack = false;
            this.capturedImgIdBack = null;
            $('#imageIdBack')
                .siblings()
                .remove();
            this.backCapture = false;
            this.IDProofDocForm.controls['backFileType'].setValue('image/png');
            this.IDProofDocForm.controls['backImg'].setValue(this.croppedImgIdBack);
            this.hideIDproofCapture = true;
            if (this.recaptureIdProofDoc) {
                this.hideIDproofCapture = true;
            }
        }
        this.nextWebcamId.next(this.IDProofDocForm.controls['selectedWebCamDevice'].value);
    }
    recapture(direction: string) {
        this.hideIDproofCapture = false;
        this.recaptureIdProofDoc = true;
        if (direction === 'front') {
            if (this.croppedImgId === undefined || !this.isCapturedId) {
                this.isCapturedId = false;
                this.isCapturedIdBack = false;
                $('#imageId').cropper('destroy');
            } else if (this.croppedImgIdBack === undefined || !this.isCroppedIdBack) {
                this.isCapturedId = false;
                this.isCapturedIdBack = false;
                $('#imageIdBack').cropper('destroy');
            }
            this.backCapture = false;
            this.frontCapture = true;
            this.capturedImgId = [];
            // if(this.croppedImgIdBack === undefined ) {
            this.capturedImgIdBack = [];
            this.backCapture = false;
            // }

            $('#imageIdBack').cropper('destroy');
        }
        if (direction === 'back') {
            if (this.croppedImgIdBack === undefined || !this.isCapturedIdBack) {
                this.isCapturedId = false;
                this.isCapturedIdBack = false;
                $('#imageIdBack').cropper('destroy');
            } else if (this.croppedImgId === undefined || !this.isCroppedId) {
                this.isCapturedId = false;
                $('#imageId').cropper('destroy');
            }
            this.frontCapture = false;
            this.backCapture = true;
            this.capturedImgIdBack = [];
            // if(this.croppedImgId === undefined ) {
            this.capturedImgId = [];
            this.frontCapture = false;
            //  }
            $('#imageId').cropper('destroy');
        }
    }
    public handleInitErrorId(error: WebcamInitError): void {
        if (this.IDProofDocForm.controls['selectedWebCamDevice'].value !== null) {
            this.deviceId = this.IDProofDocForm.controls['selectedWebCamDevice'].value;
            this.showNextWebcamId(this.IDProofDocForm.controls['selectedWebCamDevice'].value);
            //  return;
        }
        if (error.mediaStreamError && error.mediaStreamError.name === 'NotAllowedError') {
            this.idProofDocSaveErrorMsg = 'enroll.messages.error.cameraAccessError'; // 'Camera access was not allowed by user';
            this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
            this.idProofDocSaveError = true;
            return;
        } else if (error.mediaStreamError && error.mediaStreamError.name === 'NotReadableError') {
            this.idProofDocSaveErrorMsg = 'enroll.messages.error.notReadableError'; // 'It looks another app is using Camera already!';
            this.idProofElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
            this.idProofCamAccessError = true;
            return;
        }
        console.log(error);
        this.errorsId.push(error);
    }

    public cameraWasSwitchedId(deviceId: string): void {
        if (this.IDProofDocForm.controls['selectedWebCamDevice'].value !== null) {
            this.deviceId = this.IDProofDocForm.controls['selectedWebCamDevice'].value;
        } else {
            this.IDProofDocForm.controls['selectedWebCamDevice'].setValue(deviceId);
            this.deviceId = deviceId;
        }
    }

    public showNextWebcamId(directionOrDeviceId: boolean | string): void {
        // true => move forward through devices
        // false => move backwards through devices
        // string => move to device with given deviceId
        this.idProofDocSaveError = false;
        this.idProofCamAccessError = false;
        this.nextWebcamId.next(directionOrDeviceId);
        this.deviceId = directionOrDeviceId.toString();
        this.IDProofDocForm.controls['selectedWebCamDevice'].setValue(directionOrDeviceId);
    }

    public trimWebcamName(name: string) {
        return name.replace(/\([^()]*\)/g, '');
    }

    public get triggerObservableId(): Observable<void> {
        return this.triggerId.asObservable();
    }

    public get nextWebcamObservableId(): Observable<boolean | string> {
        return this.nextWebcamId.asObservable();
    }

    // ----------------------------------------Capture Face Starts--------------------------------

    public handleImage(webcamImage: WebcamImage): void {
        this.webcamImage = webcamImage;
    }

    public triggerSnapshot(): void {
        this.faceCaptureCalledInEdit = true;
        this.faceCaptureError = false;
        this.cameraAccessError = false;
        this.isCaptured = true;
        this.trigger.next();
        this.capturedImg = this.webcamImage.imageAsDataUrl;
        const $image = $('#image');
        $image.cropper(this.FaceCropperOptions);
        $image.cropper('replace', this.capturedImg);
    }

    uploadFaceImage(event) {
        if (this.camSubscription) {
            this.camSubscription.unsubscribe();
        }
        this.faceCaptureCalledInEdit = true;
        this.faceCaptureError = false;
        this.cameraAccessError = false;
        if (event.target.files.length > 0) {
        } else {
            return;
        }
        const mimeType = event.target.files[0].type;
        //  checking the size of the uploaded file, 1mb = 1048576 bytes
        if (event.target.files[0].size > 1048576) {
            this.faceCaptureError = true;
            this.translateService.get(`enroll.messages.error.face.fileSizeError`).subscribe(message => {
                this.faceCaptureErrorMsg = message;
                return;
            });
        } else {
            if (mimeType.match(/image\/.*/) !== null) {
                const reader = new FileReader();
                reader.readAsDataURL(event.target.files[0]);
                reader.onload = _event => {
                    const base64String = reader.result.toString();
                    this.uploadedFaceImage = base64String;
                    this.previewUploadedFaceImage();
                };
            } else {
                this.faceCaptureError = true;
                this.faceCaptureErrorMsg = 'Please select the correct file';
                return;
            }
        }
    }
    previewUploadedFaceImage() {
        this.isCaptured = true;
        const $image = $('#image');
        $image.cropper(this.FaceCropperOptions);
        $image.cropper('replace', this.uploadedFaceImage);
    }
    public cropSnapshot(): void {
        if (this.cropFaceWidth === null && this.cropFaceHeight === null) {
            this.cropFaceWidth = 300;
            this.cropFaceHeight = 300;
        }
        this.croppedImg = $('#image')
            .cropper('getCroppedCanvas', { width: +this.cropFaceWidth, height: +this.cropFaceHeight })
            .toDataURL('image/png');
        this.isCropped = true;
        // check if workflow has transparent allowed or not if allowed get transparent image from serverand show in ui
        if (this.isTransparentPhotoAllowed) {
            this.isLoadingResults = true;
            const user = new User();
            const userbiometrics: UserBiometric[] = [];
            const userBiometricFace: UserBiometric = new UserBiometric();
            userBiometricFace.type = 'FACE';
            userBiometricFace.format = 'png';
            userBiometricFace.data = this.croppedImg.replace('data:image/png;base64,', '');
            userbiometrics.push(userBiometricFace);
            user.userBiometrics = userbiometrics;
            this.enrollService.getTransparentPhoto(user, this.cropFaceWidth, this.cropFaceHeight).subscribe(
                (res: User) => {
                    const userBiometricFace1 = res.userBiometrics.find(e => e.type === 'FACE');
                    this.transparentImg = 'data:image/png;base64,' + userBiometricFace1.base64OfTransparentImageData;
                    this.isLoadingResults = false;
                },
                err => {
                    this.isLoadingResults = false;
                }
            );
        }
    }
    public deleteFaceImage(): void {
        if (this.isEnrolledUser || this.isEnrollmentInProgress) {
            this.faceCaptureCalledInEdit = true;
        }
        this.isCaptured = false;
        this.isCropped = false;
        this.capturedImg = '';
        $('#image').cropper('destroy');
    }

    public handleInitError(error: WebcamInitError): void {
        if (error.mediaStreamError && error.mediaStreamError.name === 'NotAllowedError') {
            this.faceCaptureError = true;
            // this.cameraAccessError = true;
            this.showInitialFaceCaptureMsg = false;
            // console.warn('Camera access was not allowed by user!'); enroll.messages.error.face.fileSizeError
            this.translateService.get(`enroll.messages.error.cameraAccessError`).subscribe(message => {
                this.faceCaptureErrorMsg = message;
                return;
            });
        } else if (error.mediaStreamError && error.mediaStreamError.name === 'NotReadableError') {
            // this.faceCaptureError = true;
            this.cameraAccessError = true;
            this.showInitialFaceCaptureMsg = false;
            // console.warn('It looks another app is using Camera already!'); enroll.messages.error.notReadableError
            this.translateService.get(`enroll.messages.error.notReadableError`).subscribe(message => {
                this.faceCaptureErrorMsg = message; // 'It looks another app is using Camera already!';
                return;
            });
        }
    }

    public cameraWasSwitched(deviceId: string): void {
        this.faceCaptureError = false;
        this.cameraAccessError = false;
        this.faceWebCamDevice.setValue(deviceId);
        this.deviceId = deviceId;
    }

    public showNextWebcam(directionOrDeviceId: boolean | string): void {
        // true => move forward through devices
        // false => move backwards through devices
        // string => move to device with given deviceId
        this.nextWebcam.next(directionOrDeviceId);
    }

    public get triggerObservable(): Observable<void> {
        return this.trigger.asObservable();
    }

    public get nextWebcamObservable(): Observable<boolean | string> {
        return this.nextWebcam.asObservable();
    }

    // --------------------------------Signature starts--------------------------------------------------------
    public drawComplete(): void {
        this.signature = this.signaturePad.toDataURL('image/png', 0.5);
    }

    drawStart(): void {
        console.log('Begin Drawing');
    }

    get signature(): any {
        return this._signature;
    }

    set signature(value: any) {
        this._signature = value;
    }

    public clear(): void {
        if (this.signaturePad !== undefined) {
            this.signaturePad.clear();
        }
        this.signature = null;
        this.signErrorMsg = '';
        this.signSuccessMsg = '';
        this.signInfoMsg = '';
        this.reCaptureSignature = false;
    }

    // registerUser
    registerUser() {
        this.isLoadingResults = true;
        this.enrollService.registerUser(this.constructUser(true)).subscribe(
            response => {
                this.saveSuccess = true;
                this.saveFailure = false;
                this.statusMessage = 'User ' + this.getUserToBeEnrolled().username + ' enrolled successfully!.';
                this.translateService.get('enroll.messages.success.userEnrolled').subscribe(
                    message => {
                        this.statusMessage = message;
                    },
                    error => {}
                );
                // this.statusMessage = 'User ' + this.getUserToBeEnrolled().username + ' enrolled successfully!.';
                this.checkForApprovalRequired();
            },
            error => {
                this.isLoadingResults = false;
                // if (error.status === 409) {
                //     this.saveFailure = true;
                //     this.saveSuccess = false;
                //     this.translateService.get('enroll.messages.error.duplicateBiometricInfo').subscribe(message => {
                //         this.statusMessage = message;
                //     });
                // } else {
                //     this.saveFailure = true;
                //     this.saveSuccess = false;
                //     this.translateService.get('enroll.messages.error.internalerror').subscribe(message => {
                //         this.statusMessage = message;
                //     });
                // }
                this.saveFailure = true;
                this.saveSuccess = false;
                this.statusMessage = error.error.detail;
            }
        );
    }

    checkForApprovalRequired() {
        this.enrollService.getWorkflowStepDetail('APPROVAL_BEFORE_ISSUANCE', this.getUserToBeEnrolled().workflow).subscribe(
            (stepDetail: WorkflowStep) => {
                if (stepDetail.wfStepApprovalGroupConfigs !== undefined) {
                    this.loggedInUserGroups.forEach(userGroup => {
                        stepDetail.wfStepApprovalGroupConfigs.forEach(approvalGroup => {
                            if (userGroup.id === approvalGroup.groupId) {
                                this.hasApprovalPermission = true;
                            }
                        });
                    });
                    this.approvalRequired = true;
                } else {
                    this.approvalRequired = false;
                }
                this.isLoadingResults = false;
            },
            error => {
                this.isLoadingResults = false;
            }
        );
    }

    // Update User
    updateUser() {
        // Update User
        this.isLoadingResults = true;
        this.enrollService.updateUser(this.constructUser(true)).subscribe(
            response => {
                this.saveSuccess = true;
                this.saveFailure = false;
                this.statusMessage = 'User ' + this.getUserToBeEnrolled().username + ' updated successfully!.';
                this.translateService.get('enroll.messages.success.userEnrolled').subscribe(
                    message => {
                        this.statusMessage = message;
                    },
                    error => {}
                );
                // this.statusMessage = 'User ' + this.getUserToBeEnrolled().username + ' enrolled successfully!.';
                this.checkForApprovalRequired();
            },
            error => {
                this.isLoadingResults = false;
                if (error.status === 409) {
                    this.saveFailure = true;
                    this.saveSuccess = false;
                    this.translateService.get('enroll.messages.error.duplicateBiometricInfo').subscribe(message => {
                        this.statusMessage = message;
                    });
                } else {
                    this.saveFailure = true;
                    this.saveSuccess = false;
                    this.translateService.get('enroll.messages.error.internalerror').subscribe(message => {
                        this.statusMessage = message;
                    });
                }
            }
        );
    }

    // Save User
    completeEnrollmentInProgress() {
        this.isLoadingResults = true;
        this.buildUserEnrollmentHistoryObj();
        console.log(this.signature);
        if (this.signature === undefined) {
            this.user.userBiometrics.forEach((item, index) => {
                if (item.type === 'SIGNATURE') {
                    this.user.userBiometrics.splice(index, 1);
                }
            });
        }
        // if (this.croppedImg === undefined || this.croppedImg === null || this.croppedImg === '') {
        //     this.user.userBiometrics.forEach((item, index) => {
        //         if (item.type === 'FACE') {
        //             this.user.userBiometrics.splice(index, 1);
        //         }
        //     });
        // }
        this.enrollService.completeEnrollmentInProgress(this.user).subscribe(
            response => {
                this.saveSuccess = true;
                this.saveFailure = false;
                if (this.isEnrolledUser) {
                    // this.statusMessage = 'User ' + this.getUserToBeEnrolled().username + ' updated successfully!.';
                    this.translateService.get('enroll.messages.success.userUpdated').subscribe(
                        message => {
                            this.statusMessage = message;
                        },
                        error => {}
                    );
                } else {
                    // this.statusMessage = 'User ' + this.getUserToBeEnrolled().username + ' enrolled successfully!.';
                    this.translateService.get('enroll.messages.success.userEnrolled').subscribe(
                        message => {
                            this.statusMessage = message;
                        },
                        error => {}
                    );
                }

                this.checkForApprovalRequired();
            },
            error => {
                this.isLoadingResults = false;
                this.saveFailure = true;
                this.saveSuccess = false;
                this.statusMessage = error.error.detail;
            }
        );
    }
    constructVisualCredential(saveToDB: boolean): User {
        // Registration Fileds
        this.buildAndSaveRegistrationFieldsStepToDB(saveToDB);
        // IDProof
        this.buildAndSaveIDProofDocumentsStepToDB(saveToDB);
        // Face
        this.buildAndSaveFaceStepToDB(saveToDB);
        // Signature
        this.buildAndSaveSignatureStepToDB(saveToDB);
        return this.user;
    }

    constructUser(saveToDB: boolean): User {
        // IDProof
        this.buildAndSaveIDProofDocumentsStepToDB(saveToDB);
        // Registration Fileds
        this.buildAndSaveRegistrationFieldsStepToDB(saveToDB);
        // Face
        this.buildAndSaveFaceStepToDB(saveToDB);
        // IRIS
        this.buildAndSaveIrisStepToDB(saveToDB);
        // FP
        this.buildAndSaveFingerprintsStepToDB(saveToDB);
        // Signature
        this.buildAndSaveSignatureStepToDB(saveToDB);
        // Skip Fingers
        // this.buildAndSaveSkipFingerPrintsStepToDB(saveToDB);
        // Enrollment History
        // this.buildAndSaveEnrollmentHistoryToDB(saveToDB);
        return this.user;
    }

    setBasicUserDetails() {
        this.user.name = this.getUserToBeEnrolled().username;
        this.user.id = this.userId;
        this.user.groupId = this.groupId;
        this.user.groupName = this.groupName;
        this.user.workflowName = this.getUserToBeEnrolled().workflow;
        this.user.isEnrollmentInProgress = this.isEnrollmentInProgress;
    }
    // Face
    async buildAndSaveFaceStepToDB(saveToDB: boolean): Promise<boolean> {
        this.setBasicUserDetails();
        // IDProof: check with chetan on why here
        await this.buildAndSaveIDProofDocumentsStepToDB(false);
        if (this.croppedImg != null) {
            let found = false;
            let userBioAttrFound = false;
            const userBiometricFace: UserBiometric = new UserBiometric();
            const userBiometricAttribute: UserBiometricAttribute = new UserBiometricAttribute();
            userBiometricFace.type = 'FACE';
            userBiometricFace.format = 'png';
            userBiometricFace.data = this.croppedImg.replace('data:image/png;base64,', '');
            userBiometricAttribute.name = 'TRANSPARENT_PHOTO_ALLOWED';
            if (this.isTransparentPhotoAllowed && this.transparentImgSelected.value === 'yes') {
                userBiometricAttribute.value = 'true';
            } else {
                userBiometricAttribute.value = 'false';
            }
            this.user.userBiometrics.forEach(userBiometricFaceElement => {
                if (userBiometricFaceElement.type === userBiometricFace.type) {
                    userBiometricFaceElement.format = userBiometricFace.format;
                    userBiometricFaceElement.data = userBiometricFace.data;
                    found = true;
                    if (userBiometricFaceElement.userBiometricAttributes.length > 0) {
                        userBiometricFaceElement.userBiometricAttributes.forEach(e => {
                            if (e.name === 'TRANSPARENT_PHOTO_ALLOWED') {
                                e.value = userBiometricAttribute.value;
                                userBioAttrFound = true;
                            }
                        });
                    } else {
                        userBiometricFaceElement.userBiometricAttributes.push(userBiometricAttribute);
                    }
                }
            });
            if (!userBioAttrFound) {
                userBiometricFace.userBiometricAttributes.push(userBiometricAttribute);
            }
            if (!found) {
                userBiometricFace.userBiometricAttributes.push(userBiometricAttribute);
                this.user.userBiometrics.push(userBiometricFace);
            }
            this.buildUserEnrollmentHistoryObj();
            if (saveToDB) {
                this.isLoadingResults = true;
                const promise = await this.enrollService.saveFaceStepToDB(this.user, 'FACE');
                this.isLoadingResults = false;
                return promise;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    // IRIS
    async buildAndSaveIrisStepToDB(saveToDB: boolean): Promise<boolean> {
        this.setBasicUserDetails();
        // IDProof: check with chetan on why here
        await this.buildAndSaveIDProofDocumentsStepToDB(false);
        if (this.leftEye != null && this.rightEye != null) {
            let found = false;
            const userBiometricLeftIris: UserBiometric = new UserBiometric();
            userBiometricLeftIris.type = 'IRIS';
            userBiometricLeftIris.format = 'BASE64';
            userBiometricLeftIris.position = 'LEFT_EYE';
            userBiometricLeftIris.data = this.leftEye;
            const userBiometricRightIris: UserBiometric = new UserBiometric();
            userBiometricRightIris.type = 'IRIS';
            userBiometricRightIris.format = 'BASE64';
            userBiometricRightIris.position = 'RIGHT_EYE';
            userBiometricRightIris.data = this.rightEye;

            this.user.userBiometrics.forEach(userBiometricIRISElement => {
                if (
                    userBiometricIRISElement.type === userBiometricLeftIris.type &&
                    userBiometricIRISElement.position === userBiometricLeftIris.position
                ) {
                    userBiometricIRISElement.data = userBiometricLeftIris.data;
                    found = true;
                }
            });
            if (!found) {
                this.user.userBiometrics.push(userBiometricLeftIris);
            }
            this.user.userBiometrics.forEach(userBiometricIRISElement => {
                if (
                    userBiometricIRISElement.type === userBiometricRightIris.type &&
                    userBiometricIRISElement.position === userBiometricRightIris.position
                ) {
                    userBiometricIRISElement.data = userBiometricRightIris.data;
                    found = true;
                }
            });
            if (!found) {
                this.user.userBiometrics.push(userBiometricRightIris);
            }
            this.buildUserEnrollmentHistoryObj();
            if (saveToDB) {
                this.isLoadingResults = true;
                const promise = await this.enrollService.saveIRISStepToDB(this.user, 'IRIS');
                this.isLoadingResults = false;
                return promise;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    // FP
    async buildAndSaveFingerprintsStepToDB(saveToDB: boolean): Promise<boolean> {
        this.setBasicUserDetails();
        // IDProof: check with chetan on why here
        const userBiometrics: UserBiometric[] = [];
        await this.buildAndSaveIDProofDocumentsStepToDB(false);
        console.log(this.selectedWizardTab);
        if (this.selectedWizardTab === 'FINGERPRINT_CAPTURE') {
            for (let i = 0; i < 5; i++) {
                if (this.leftFingerprintImages[i] !== undefined) {
                    this.leftFingerprintImages[i].type = 'FINGERPRINT';
                    const resBiometric: UserBiometric = this.user.userBiometrics.find(
                        biometric =>
                            biometric.type === this.leftFingerprintImages[i].type &&
                            biometric.position === this.leftFingerprintImages[i].position
                    );
                    if (resBiometric === undefined) {
                        this.user.userBiometrics.push(this.leftFingerprintImages[i]);
                        userBiometrics.push(this.leftFingerprintImages[i]);
                    } else {
                        resBiometric.data = this.leftFingerprintImages[i].data;
                        resBiometric.ansi = this.leftFingerprintImages[i].ansi;
                        resBiometric.imageQuality = this.leftFingerprintImages[i].imageQuality;
                        if (this.isWsqFormatEnabled) {
                            resBiometric.wsqData = this.leftFingerprintImages[i].wsqData;
                        } else {
                            resBiometric.wsqData = '';
                        }
                        userBiometrics.push(resBiometric);
                    }
                }
            }
            for (let i = 0; i < 5; i++) {
                if (this.rightFingerprintImages[i] !== undefined) {
                    this.rightFingerprintImages[i].type = 'FINGERPRINT';
                    const resBiometric: UserBiometric = this.user.userBiometrics.find(
                        biometric =>
                            biometric.type === this.rightFingerprintImages[i].type &&
                            biometric.position === this.rightFingerprintImages[i].position
                    );
                    if (resBiometric === undefined) {
                        this.user.userBiometrics.push(this.rightFingerprintImages[i]);
                        userBiometrics.push(this.rightFingerprintImages[i]);
                    } else {
                        resBiometric.data = this.rightFingerprintImages[i].data;
                        resBiometric.ansi = this.rightFingerprintImages[i].ansi;
                        resBiometric.imageQuality = this.rightFingerprintImages[i].imageQuality;
                        if (this.isWsqFormatEnabled) {
                            resBiometric.wsqData = this.rightFingerprintImages[i].wsqData;
                        } else {
                            resBiometric.wsqData = '';
                        }
                        userBiometrics.push(resBiometric);
                    }
                }
            }

            const indexes = [];
            this.user.userBiometrics.forEach((e1, index) => {
                if (e1.type === 'FINGERPRINT') {
                    if (userBiometrics.find(e => e.position === e1.position) === undefined) {
                        console.log(e1.position, index);
                        indexes.push(e1.position);
                    }
                }
            });
            indexes.forEach(e => {
                console.log(this.user.userBiometrics[e]);
                this.removeElement(e);
            });
            this.buildUserEnrollmentHistoryObj();
            if (saveToDB) {
                this.isLoadingResults = true;
                const promise = await this.enrollService.saveFingerPrintsStepToDB(this.user, 'FINGERPRINT');
                this.isLoadingResults = false;
                return promise;
            } else {
                return true;
            }
        } else if (this.selectedWizardTab === 'FINGERPRINT_ROLL_CAPTURE') {
            for (let i = 0; i < 5; i++) {
                if (this.leftRollFingerprintImages[i] !== undefined) {
                    this.leftRollFingerprintImages[i].type = 'ROLL_FINGERPRINT';
                    const resBiometric: UserBiometric = this.user.userBiometrics.find(
                        biometric =>
                            biometric.type === this.leftRollFingerprintImages[i].type &&
                            biometric.position === this.leftRollFingerprintImages[i].position
                    );
                    if (resBiometric === undefined) {
                        this.user.userBiometrics.push(this.leftRollFingerprintImages[i]);
                        userBiometrics.push(this.leftRollFingerprintImages[i]);
                    } else {
                        resBiometric.data = this.leftRollFingerprintImages[i].data;
                        resBiometric.ansi = this.leftRollFingerprintImages[i].ansi;
                        resBiometric.imageQuality = this.leftRollFingerprintImages[i].imageQuality;
                        userBiometrics.push(resBiometric);
                    }
                }
            }
            for (let i = 0; i < 5; i++) {
                console.log(this.rightRollFingerprintImages);
                if (this.rightRollFingerprintImages[i] !== undefined) {
                    this.rightRollFingerprintImages[i].type = 'ROLL_FINGERPRINT';
                    const resBiometric: UserBiometric = this.user.userBiometrics.find(
                        biometric =>
                            biometric.type === this.rightRollFingerprintImages[i].type &&
                            biometric.position === this.rightRollFingerprintImages[i].position
                    );
                    if (resBiometric === undefined) {
                        this.user.userBiometrics.push(this.rightRollFingerprintImages[i]);
                        userBiometrics.push(this.rightRollFingerprintImages[i]);
                    } else {
                        resBiometric.data = this.rightRollFingerprintImages[i].data;
                        resBiometric.ansi = this.rightRollFingerprintImages[i].ansi;
                        resBiometric.imageQuality = this.rightRollFingerprintImages[i].imageQuality;
                        userBiometrics.push(resBiometric);
                    }
                }
            }
            const indexes = [];
            this.user.userBiometrics.forEach((e1, index) => {
                if (e1.type === 'ROLL_FINGERPRINT') {
                    if (userBiometrics.find(e => e.position === e1.position) === undefined) {
                        console.log(e1.position, index);
                        indexes.push(e1.position);
                    }
                }
            });
            indexes.forEach(e => {
                console.log(this.user.userBiometrics[e]);
                // this.removeElement(e);
            });
            this.buildUserEnrollmentHistoryObj();
            if (saveToDB) {
                this.isLoadingResults = true;
                const promise = await this.enrollService.saveFingerPrintsStepToDB(this.user, 'ROLL_FINGERPRINT');
                this.isLoadingResults = false;
                return promise;
            } else {
                return true;
            }
        }
    }
    removeElement(position) {
        this.user.userBiometrics.forEach((item, index) => {
            if (item.position === position) {
                this.user.userBiometrics.splice(index, 1);
            }
        });
    }
    // Skip FP
    async buildAndSaveSkipFingerPrintsStepToDB(saveToDB: boolean): Promise<boolean> {
        this.setBasicUserDetails();
        // IDProof: check with chetan on why here
        await this.buildAndSaveIDProofDocumentsStepToDB(false);
        let notEmpty = false;
        for (let i = 0; i < 10; i++) {
            if (this.skippedFingers[i] !== undefined) {
                this.user.userBiometricSkips.push(this.skippedFingers[i]);
                notEmpty = true;
            }
        }
        if (saveToDB) {
            this.isLoadingResults = true;
            const promise = await this.enrollService.saveSkipFingerPrintsStepToDB(this.user, 'FINGERPRINT');
            this.isLoadingResults = false;
            return promise;
        } else {
            return true;
        }
    }
    // Signature
    async buildAndSaveSignatureStepToDB(saveToDB: boolean): Promise<boolean> {
        this.setBasicUserDetails();
        // IDProof: check with chetan on why here
        await this.buildAndSaveIDProofDocumentsStepToDB(false);
        if (this._signature != null) {
            let found = false;
            const userBiometricSignature: UserBiometric = new UserBiometric();
            userBiometricSignature.type = 'SIGNATURE';
            if (this.capturedSignModel.imageIsoData !== undefined) {
                userBiometricSignature.ansi = this.capturedSignModel.imageIsoData;
                userBiometricSignature.format = this.capturedSignModel.imageType.toLowerCase();
            } else {
                userBiometricSignature.format = 'BASE64';
                userBiometricSignature.format = 'png';
            }
            userBiometricSignature.data = this.signature.replace(`data:image/${userBiometricSignature.format};base64,`, '');
            this.user.userBiometrics.forEach(userBiometricSignatureElement => {
                if (userBiometricSignatureElement.type === userBiometricSignature.type) {
                    userBiometricSignatureElement.data = userBiometricSignature.data;
                    userBiometricSignatureElement.format = userBiometricSignature.format;
                    found = true;
                }
            });
            if (!found) {
                this.user.userBiometrics.push(userBiometricSignature);
            }
            this.buildUserEnrollmentHistoryObj();
            if (saveToDB) {
                this.isLoadingResults = true;
                const promise = await this.enrollService.saveSignatureStepToDB(this.user, 'SIGNATURE');
                this.isLoadingResults = false;
                return promise;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    // Enrollment History
    async buildAndSaveEnrollmentHistoryToDB(saveToDB: boolean): Promise<boolean> {
        this.setBasicUserDetails();
        // IDProof: check with chetan on why here
        await this.buildAndSaveIDProofDocumentsStepToDB(false);
        this.userEnrollmentHistory = new UserEnrollmentHistory();
        this.userEnrollmentHistory.enrolledBy = this.enrolledBy;
        this.userEnrollmentHistory.enrollmentStartDate = this.enrollmentStartDate;
        this.userEnrollmentHistory.enrollmentEndDate = new Date();
        this.user.userEnrollmentHistory = this.userEnrollmentHistory;

        if (saveToDB) {
            this.isLoadingResults = true;
            const promise = await this.enrollService.saveEnrollmentHistoryStepToDB(this.user);
            this.isLoadingResults = false;
            return promise;
        } else {
            return true;
        }
    }
    // IDProof
    async buildAndSaveIDProofDocumentsStepToDB(saveToDB: boolean): Promise<boolean> {
        this.setBasicUserDetails();
        if (this.userIDProofList.length !== 0) {
            const userIDProofDocuments: Array<UserIDProof> = [];
            this.userIDProofList.forEach(element => {
                const userIDProof = new UserIDProof();
                userIDProof.name = element.name;
                userIDProof.idProofTypeId = element.idProofTypeId;
                userIDProof.issuingAuthorityId = element.issuingAuthorityId;
                if (element.source === 'capture') {
                    if (element.frontImg !== null) {
                        userIDProof.frontImg = element.frontImg.replace(`data:${element.frontFileType};base64,`, '');
                        userIDProof.frontFileType = element.frontFileType;
                    }
                    if (element.backImg !== null) {
                        userIDProof.backImg = element.backImg.replace(`data:${element.backFileType};base64,`, '');
                        userIDProof.backFileType = element.backFileType;
                    }
                } else if (element.source === 'upload') {
                    if (element.frontFileType === 'application/pdf') {
                        userIDProof.file = element.file.replace(`data:${element.frontFileType};base64,`, '');
                        userIDProof.frontFileName = element.frontFileName;
                        userIDProof.frontFileType = element.frontFileType;
                    }
                    if (element.frontImg !== null) {
                        userIDProof.frontImg = element.frontImg.replace(`data:${element.frontFileType};base64,`, '');
                        userIDProof.frontFileName = element.frontFileName;
                        userIDProof.frontFileType = element.frontFileType;
                    }
                    if (element.backImg !== null) {
                        userIDProof.backImg = element.backImg.replace(`data:${element.backFileType};base64,`, '');
                        userIDProof.backFileName = element.backFileName;
                        userIDProof.backFileType = element.backFileType;
                    }
                    // if (element.backFileType === 'application/pdf') {
                    //     userIDProof.file = element.file.replace(`data:${element.backFileType};base64,`, '');
                    //     userIDProof.frontFileName = element.backFileName;
                    // }
                }
                userIDProofDocuments.push(userIDProof);
            });
            this.user.userIDProofDocuments = userIDProofDocuments;
            this.buildUserEnrollmentHistoryObj();
            if (saveToDB) {
                this.isLoadingResults = true;
                const promise = await this.enrollService.saveIDProofDocumentsStepToDB(this.user);
                this.isLoadingResults = false;
                return promise;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    buildUserEnrollmentHistoryObj() {
        this.userEnrollmentHistory = new UserEnrollmentHistory();
        this.userEnrollmentHistory.enrolledBy = this.enrolledBy;
        this.userEnrollmentHistory.enrollmentStartDate = this.enrollmentStartDate;
        this.userEnrollmentHistory.enrollmentEndDate = new Date();
        this.user.userEnrollmentHistory = this.userEnrollmentHistory;
    }

    // Registration Fields
    async buildAndSaveRegistrationFieldsStepToDB(saveToDB: boolean): Promise<boolean> {
        this.setBasicUserDetails();
        // IDProof: check with chetan on why here
        await this.buildAndSaveIDProofDocumentsStepToDB(false);
        if (this.registrationStepDetail.wfStepRegistrationConfigs) {
            this.registrationStepDetail.wfStepRegistrationConfigs.forEach(registrationField => {
                switch (registrationField.fieldName) {
                    case 'Given Name (First & Last Name)': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.firstName = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Middle Name': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.middleName = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Surname/Family Name': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.lastName = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Mother’s Maiden Name': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.mothersMaidenName = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Date of Birth': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.dateOfBirth = this.sharedService.getDateWithoutTimeZoneOffset(
                            this.enrollForm.controls[registrationField.id].value
                        );
                        break;
                    }
                    case 'Email': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.email = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Birth Place/ Nationality': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.nationality = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Nationality': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.nationality = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Place of Birth': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.placeOfBirth = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Personal Code': {
                        if (this.workflow.organizationIdentities.identityTypeName === IdentityTypeEnum.NATIONAL_ID) {
                            if (this.userNationalID === null) {
                                this.userNationalID = new UserNationalID();
                            }
                            this.userNationalID.personalCode = this.enrollForm.controls[registrationField.id].value;
                        }
                        if (this.workflow.organizationIdentities.identityTypeName === IdentityTypeEnum.PERMANENT_RESIDENT_ID) {
                            if (this.userPermanentResidentID === null) {
                                this.userPermanentResidentID = new UserPermanentResidentID();
                            }
                            this.userPermanentResidentID.personalCode = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Passport Number': {
                        if (this.userPermanentResidentID === null) {
                            this.userPermanentResidentID = new UserPermanentResidentID();
                        }
                        this.userPermanentResidentID.passportNumber = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Resident Card Type Number': {
                        if (this.userPermanentResidentID === null) {
                            this.userPermanentResidentID = new UserPermanentResidentID();
                        }
                        this.userPermanentResidentID.residentcardTypeNumber = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Resident Card Type Code': {
                        if (this.userPermanentResidentID === null) {
                            this.userPermanentResidentID = new UserPermanentResidentID();
                        }
                        this.userPermanentResidentID.residentcardTypeCode = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Vaccine Name': {
                        if (this.userNationalID === null) {
                            this.userNationalID = new UserNationalID();
                        }
                        this.userNationalID.vaccineName = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Vaccination Date': {
                        if (this.userNationalID === null) {
                            this.userNationalID = new UserNationalID();
                        }
                        this.userNationalID.vaccinationDate = this.sharedService.getDateWithoutTimeZoneOffset(
                            this.enrollForm.controls[registrationField.id].value
                        );
                        break;
                    }
                    case 'Vaccination Location Name': {
                        if (this.userNationalID === null) {
                            this.userNationalID = new UserNationalID();
                        }
                        this.userNationalID.vaccinationLocationName = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Gender': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.gender = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Blood Type': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.bloodType = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Address': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.address = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Email': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.email = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Contact Number': {
                        this.userAttribute.contact = this.enrollForm.controls[registrationField.id].value;
                        this.userAttribute.countryCode = this.enrollForm.controls.countryCode.value;
                        break;
                    }
                    case 'Organ Donor': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.organDonor = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Veteran': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        this.userAttribute.veteran = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Hair Color': {
                        if (this.userAppearance === null) {
                            this.userAppearance = new UserAppearance();
                        }
                        this.userAppearance.hairColor = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Eye Color': {
                        if (this.userAppearance === null) {
                            this.userAppearance = new UserAppearance();
                        }
                        this.userAppearance.eyeColor = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Height': {
                        if (this.userAppearance === null) {
                            this.userAppearance = new UserAppearance();
                        }
                        this.userAppearance.height = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Weight': {
                        if (this.userAppearance === null) {
                            this.userAppearance = new UserAppearance();
                        }
                        this.userAppearance.weight = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Address Line1': {
                        const addressLine1 = this.enrollForm.controls[registrationField.id].value;
                        if (this.userAddress === null && addressLine1 !== '') {
                            this.userAddress = new UserAddress();
                        }
                        if (addressLine1 !== '') {
                            this.userAddress.addressLine1 = addressLine1;
                        }
                        break;
                    }
                    case 'Address Line2': {
                        const addressLine2 = this.enrollForm.controls[registrationField.id].value;
                        if (this.userAddress === null && addressLine2 !== '') {
                            this.userAddress = new UserAddress();
                        }
                        if (addressLine2 !== '') {
                            this.userAddress.addressLine2 = addressLine2;
                        }
                        break;
                    }
                    case 'City': {
                        const city = this.enrollForm.controls[registrationField.id].value;
                        if (this.userAddress === null && city !== '') {
                            this.userAddress = new UserAddress();
                        }
                        if (city !== '') {
                            this.userAddress.city = city;
                        }
                        break;
                    }
                    case 'State': {
                        const state = this.enrollForm.controls[registrationField.id].value;
                        if (this.userAddress === null && state !== '') {
                            this.userAddress = new UserAddress();
                        }
                        if (state !== '') {
                            this.userAddress.state = state;
                        }
                        break;
                    }
                    case 'Country': {
                        const country = this.enrollForm.controls[registrationField.id].value;
                        if (this.userAddress === null && country !== '') {
                            this.userAddress = new UserAddress();
                        }
                        if (country !== '') {
                            this.userAddress.country = country;
                        }
                        break;
                    }
                    case 'ZIP/Postal Code': {
                        const zipCode = this.enrollForm.controls[registrationField.id].value;
                        if (this.userAddress === null && zipCode !== '') {
                            this.userAddress = new UserAddress();
                        }
                        if (zipCode !== '') {
                            this.userAddress.zipCode = zipCode;
                        }
                        break;
                    }
                    case 'Rank/Grade/Employee Status': {
                        if (this.userPIV === null) {
                            this.userPIV = new UserPIV();
                        }
                        this.userPIV.rank = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Employee Affiliation': {
                        if (this.workflow.organizationIdentities.identityTypeName === IdentityTypeEnum.PIV) {
                            if (this.userPIV === null) {
                                this.userPIV = new UserPIV();
                            }
                            this.userPIV.employeeAffiliation = this.enrollForm.controls[registrationField.id].value;
                        } else if (this.workflow.organizationIdentities.identityTypeName === IdentityTypeEnum.EMPLOYEE_ID) {
                            if (this.userEmployeeID === null) {
                                this.userEmployeeID = new UserEmployeeID();
                            }
                            this.userEmployeeID.affiliation = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Employee Affiliation Color Code': {
                        if (this.userPIV === null) {
                            this.userPIV = new UserPIV();
                        }
                        this.userPIV.employeeAffiliationColorCode = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Restrictions': {
                        if (this.userDrivingLicense === null) {
                            this.userDrivingLicense = new UserDrivingLicense();
                        }
                        this.userDrivingLicense.restrictions = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Vehicle Classification': {
                        if (this.userDrivingLicense === null) {
                            this.userDrivingLicense = new UserDrivingLicense();
                        }
                        this.userDrivingLicense.vehicleClass = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Endorsements': {
                        if (this.userDrivingLicense === null) {
                            this.userDrivingLicense = new UserDrivingLicense();
                        }
                        this.userDrivingLicense.endorsements = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Donor': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        //  alert(this.enrollForm.controls[registrationField.id].value);
                        this.userAttribute.organDonor = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Veteran': {
                        if (this.userAttribute === null) {
                            this.userAttribute = new UserAttribute();
                        }
                        //  alert(this.enrollForm.controls[registrationField.id].value);
                        this.userAttribute.veteran = this.enrollForm.controls[registrationField.id].value;
                        break;
                    }
                    case 'Department': {
                        if (this.workflow.organizationIdentities.identityTypeName === IdentityTypeEnum.STUDENT_ID) {
                            if (this.userStudentID === null) {
                                this.userStudentID = new UserStudentID();
                            }
                            this.userStudentID.department = this.enrollForm.controls[registrationField.id].value;
                        } else if (this.workflow.organizationIdentities.identityTypeName === IdentityTypeEnum.EMPLOYEE_ID) {
                            if (this.userEmployeeID === null) {
                                this.userEmployeeID = new UserEmployeeID();
                            }
                            this.userEmployeeID.department = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Primary Subscriber': {
                        if (this.userHealthID === null) {
                            this.userHealthID = new UserHealthID();
                        }
                        if (this.userHealthID !== null) {
                            this.userHealthID.primarySubscriber = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Subscriber ID': {
                        if (this.userHealthID === null) {
                            this.userHealthID = new UserHealthID();
                        }
                        if (this.userHealthID !== null) {
                            this.userHealthID.primarySubscriberID = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Primary Doctor': {
                        if (this.userHealthID === null) {
                            this.userHealthID = new UserHealthID();
                        }
                        if (this.userHealthID !== null) {
                            this.userHealthID.pcp = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Primary Doctor Phone': {
                        if (this.userHealthID === null) {
                            this.userHealthID = new UserHealthID();
                        }
                        if (this.userHealthID !== null) {
                            this.userHealthID.pcpPhone = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Policy Number': {
                        if (this.userHealthID === null) {
                            this.userHealthID = new UserHealthID();
                        }
                        if (this.userHealthID !== null) {
                            this.userHealthID.policyNumber = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Group Plan': {
                        if (this.userHealthID === null) {
                            this.userHealthID = new UserHealthID();
                        }
                        if (this.userHealthID !== null) {
                            this.userHealthID.groupPlan = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Health Plan Number': {
                        if (this.userHealthID === null) {
                            this.userHealthID = new UserHealthID();
                        }
                        if (this.userHealthID !== null) {
                            this.userHealthID.healthPlanNumber = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Vaccine': {
                        if (this.userHealthServiceVaccineInfo === null) {
                            this.userHealthServiceVaccineInfo = new UserHealthServiceVaccineInfo();
                        }
                        if (this.userHealthServiceVaccineInfo !== null) {
                            this.userHealthServiceVaccineInfo.vaccine = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Route': {
                        if (this.userHealthServiceVaccineInfo === null) {
                            this.userHealthServiceVaccineInfo = new UserHealthServiceVaccineInfo();
                        }
                        if (this.userHealthServiceVaccineInfo !== null) {
                            this.userHealthServiceVaccineInfo.route = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Site': {
                        if (this.userHealthServiceVaccineInfo === null) {
                            this.userHealthServiceVaccineInfo = new UserHealthServiceVaccineInfo();
                        }
                        if (this.userHealthServiceVaccineInfo !== null) {
                            this.userHealthServiceVaccineInfo.site = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Date': {
                        if (this.userHealthServiceVaccineInfo === null) {
                            this.userHealthServiceVaccineInfo = new UserHealthServiceVaccineInfo();
                        }
                        if (this.userHealthServiceVaccineInfo !== null) {
                            this.userHealthServiceVaccineInfo.date = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                    case 'Administered By': {
                        if (this.userHealthServiceVaccineInfo === null) {
                            this.userHealthServiceVaccineInfo = new UserHealthServiceVaccineInfo();
                        }
                        if (this.userHealthServiceVaccineInfo !== null) {
                            this.userHealthServiceVaccineInfo.administeredBy = this.enrollForm.controls[registrationField.id].value;
                        }
                        break;
                    }
                }
                // if (preview) {
                //     userAttributes.push(new UserAttribute(registrationField.fieldName, this.enrollForm.controls[registrationField.id].value));
                // } else {
                //     userAttributes.push(new UserAttribute(registrationField.id, this.enrollForm.controls[registrationField.id].value));
                // }
            });
        }

        if (this.userAttribute !== null) {
            this.user.userAttribute = this.userAttribute;
            this.userFirstAndLastName = this.userAttribute.firstName + ' ' + this.userAttribute.lastName;
        }
        if (this.userAppearance !== null) {
            this.user.userAppearance = this.userAppearance;
        }
        if (this.userPIV !== null) {
            this.user.userPIV = this.userPIV;
        }
        if (this.userDrivingLicense !== null) {
            this.user.userDrivingLicense = this.userDrivingLicense;
        }
        if (this.userHealthServiceVaccineInfo !== null) {
            if (this.userHealthService === null) {
                this.userHealthService = new UserHealthService();
            }
            if (this.userHealthServiceVaccineInfos.length === 0) {
                this.userHealthServiceVaccineInfos.push(this.userHealthServiceVaccineInfo);
                this.userHealthService.userHealthServiceVaccineInfos = this.userHealthServiceVaccineInfos;
            }
        }
        if (this.userHealthService !== null) {
            this.user.userHealthService = this.userHealthService;
        }
        if (this.userHealthID !== null) {
            this.user.userHealthID = this.userHealthID;
        }
        if (this.userStudentID !== null) {
            this.user.userStudentID = this.userStudentID;
        }
        if (this.userEmployeeID !== null) {
            this.user.userEmployeeID = this.userEmployeeID;
        }
        if (this.userNationalID !== null) {
            this.user.userNationalID = this.userNationalID;
        }
        if (this.userPermanentResidentID !== null) {
            this.user.userPermanentResidentID = this.userPermanentResidentID;
        }

        if (this.userAddress !== null) {
            this.user.userAddress = this.userAddress;
        }
        this.user.userEnrollmentHistory = this.userEnrollmentHistory;
        this.buildUserEnrollmentHistoryObj();

        if (saveToDB) {
            this.isLoadingResults = true;
            const promise = await this.enrollService.saveRegistrationFieldsStepToDB(this.user);
            this.isLoadingResults = false;
            return promise;
        } else {
            return true;
        }
    }

    formatmeetidentityDate(date: string) {
        if (date !== null && date !== '' && date !== undefined) {
            return formatDate(date, 'MM/dd/yyyy', this.locale);
        }
    }
    getUserToBeEnrolled() {
        return this.userToBeEnrolled;
    }

    getSelectedWizardTab() {
        return this.selectedWizardTab;
    }

    signatureCaptureText() {
        this.signatureCaptureTxt = 'SignatureCaptured';
    }

    navigateToDashboard() {
        this.router.navigate(['/dashboard']);
    }

    navigateToApprovalPage() {
        const data = new Data();
        data.userFirstName = this.userToBeEnrolled.firstName;
        data.userLastName = this.userToBeEnrolled.lastName;
        data.groupName = this.groupName;
        data.identityType = this.identityType;
        data.userName = this.userToBeEnrolled.username;
        this.dataService.changeObj(data);
        this.router.navigate(['/approve-requests']);
    }

    issueUser() {
        this.searchUserService.getUserByName(this.userToEnroll).subscribe(user => {
            if (user !== null) {
                user.identityType = this.identityType;
                this.identityDeviceDialogService.openAddIdentityDeviceDialog(user);
            }
        });
    }

    ngOnDestroy() {
        this.subscription && this.subscription.unsubscribe();
        if (this.fpReaderSubscription) {
            this.fpReaderSubscription.unsubscribe();
        }
        if (this.irisReaderSubscription) {
            this.irisReaderSubscription.unsubscribe();
        }
        if (this.fingerprintSubscriber) {
            this.fingerprintSubscriber.unsubscribe();
        }
        if (this.camSubscription) {
            this.camSubscription.unsubscribe();
        }
        if (this.signatureDeviceSubscription) {
            this.signatureDeviceSubscription.unsubscribe();
        }
        if (this.sigantureCaptureSubscription) {
            this.sigantureCaptureSubscription.unsubscribe();
        }
    }

    showTdsCount(event) {
        this.tdc = event;
    }

    openImage(imageType) {
        if (imageType === 'front') {
            this.openFrontImageBlock = true;
        } else if (imageType === 'back') {
            this.openBackImageBlock = true;
        } else if (imageType === 'fileImage') {
            this.openFileImage = true;
        }
        this.isAnimate = true;
    }

    closeImage() {
        this.openFrontImageBlock = false;
        this.openBackImageBlock = false;
        this.openFileImage = false;
    }
    check(attr, toolTipId): void {
        const val = document.getElementById(toolTipId);
        this.showToolTip = val.offsetWidth < val.scrollWidth ? true : false;
    }
}
