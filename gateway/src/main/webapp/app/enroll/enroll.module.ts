import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'app/material/material.module';
import { WebcamModule } from 'ngx-webcam';
import { SignaturePadModule } from 'angular2-signaturepad';
import { GatewaySharedModule } from '../shared';
import { EnrollService } from './enroll.service';
import { EnrollWizardComponent } from './enroll-wizard/enroll-wizard.component';
import { EnrollComponent } from './enroll.component';
import { ENROLL_ROUTE } from './enroll.route';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { EnrollmentDetailsComponent } from './enrollment-details/enrollment-details.component';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MyMissingTranslationHandler } from '../shared/util/translation-handler.service';
import { MissingTranslationHandler } from '@ngx-translate/core';

export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY'
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    }
};
@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MaterialModule,
        GatewaySharedModule,
        WebcamModule,
        SignaturePadModule,
        RouterModule.forChild(ENROLL_ROUTE),
        AngularSvgIconModule
    ],
    declarations: [EnrollWizardComponent, EnrollComponent, EnrollmentDetailsComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [
        EnrollService,
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
        { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true }},
        { provide: MissingTranslationHandler, useClass: MyMissingTranslationHandler }
    ]
})
export class EnrollModule {}
