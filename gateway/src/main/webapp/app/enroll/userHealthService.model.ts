import { UserHealthServiceVaccineInfo } from './userHealthServiceVaccineInfo.model';

export class UserHealthService {
    public  uimsId: string;
    public idNumber: number;
    public issuingAuthority: string;
    public primarySubscriber: string;
    public primarySubscriberID: number;
    public primaryDoctor: string;
    public primaryDoctorPhone: number;
    public dateOfIssuance: string;
    public dateOfExpiry: string;
    public userHealthServiceVaccineInfos: UserHealthServiceVaccineInfo[];
    constructor() {}
}
