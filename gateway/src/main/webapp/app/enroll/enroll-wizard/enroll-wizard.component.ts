import { Component, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { Principal } from 'app/core';
import { UserInfo } from '../../core/auth/user-info.model';
import { TimeoutError } from 'rxjs';
declare var $: any;

@Component({
    selector: 'jhi-enroll-wizard',
    templateUrl: './enroll-wizard.component.html',
    styleUrls: ['./enroll-wizard.component.scss']
})
export class EnrollWizardComponent implements OnInit {
    integerval = 1;
    @Input() isIDProofDisabled = true;
    @Input() isRegFormDisabled = true;
    @Input() isFaceDisabled = true;
    @Input() isIrisDisabled = true;
    @Input() isFingerprintDisabled = true;
    @Input() isRollFingerprintDisabled = true;
    @Input() isSignatureDisabled = true;
    @Input() isSummaryDisabled = true;
    @Input('selectedWizardTab') selectedWizardTab: string;
    @Input('isEnrolledWizardTab') isEnrolledWizardTab: boolean;
    @Input('userToBeEnrolled') userToBeEnrolled: UserInfo;
    @Output() tabClick = new EventEmitter<string>();
    @Output() wizardTdsCount = new EventEmitter<number>();

    constructor(private principal: Principal) {}

    ngOnInit() {
        $('.closebtn').click(function() {
            $('.fig').css('visibility', 'hidden');
        });
        $(document).ready(() => {
            if ($('.wT td:nth-child(1) span').css('color') === undefined) {
                return;
            }

            let x;
            x = $('.wT').find('td').length;
            const per = 100 / x;
            this.wizardTdsCount.emit(x);

            $('.navline').css('width', per / 2 + '%');
            $('.wT td:nth-child(1) span').css('color', '#00bcd4');
            $('.mainbbtn').click(function() {
                $(this).addClass('active');
            });

            $('.wT td:nth-child(1) .wBtn,.infoback').click(function() {
                $('.navline').css('width', per / 2 + '%');
                $('.wT td:nth-child(1) span').css('color', '#00bcd4');
            });

            $('.wT td:nth-child(2) .wBtn, .infonext, .fpback').click(function() {
                $('.navline').css('width', per * 2 - per / 2 + '%');
                $('td:nth-child(1) .wBtn,td:nth-child(2) .wBtn').css('border', '4px solid #00bcd4');
                $('.wT td:nth-child(1) span,.wT td:nth-child(2) span').css('color', '#00bcd4');
            });

            $('.wT td:nth-child(3) .wBtn, .facenext, .irisback').click(function() {
                $('.navline').css('width', per * 3 - per / 2 + '%');
                $('td:nth-child(1) .wBtn,td:nth-child(2) .wBtn,td:nth-child(3) .wBtn').css('border', '4px solid #00bcd4');
                $('.wT td:nth-child(1) span,.wT td:nth-child(2) span,.wT td:nth-child(3) span').css('color', '#00bcd4');
            });

            $('.wT td:nth-child(4) .wBtn, .fpnext, .signback').click(function() {
                $('.navline').css('width', per * 4 - per / 2 + '%');
                $('td:nth-child(1) .wBtn,td:nth-child(2) .wBtn,td:nth-child(3) .wBtn,td:nth-child(4) .wBtn').css(
                    'border',
                    '4px solid #00bcd4'
                );
                $('.wT td:nth-child(1) span,.wT td:nth-child(2) span,.wT td:nth-child(3) span,.wT td:nth-child(4) span').css(
                    'color',
                    '#00bcd4'
                );
            });

            $('.wT td:nth-child(5) .wBtn, .irisnext, .summaryback').click(function() {
                $('.navline').css('width', per * 5 - per / 2 + '%');
                $('td:nth-child(1) .wBtn,td:nth-child(2) .wBtn,td:nth-child(3) .wBtn,td:nth-child(4) .wBtn, td:nth-child(5) .wBtn').css(
                    'border',
                    '4px solid #00bcd4'
                );
                $(
                    '.wT td:nth-child(1) span,.wT td:nth-child(2) span,.wT td:nth-child(3) span,.wT td:nth-child(4) span, .wT td:nth-child(5) span'
                ).css('color', '#00bcd4');
            });

            $('.wT td:nth-child(6) .wBtn, .signnext').click(function() {
                $('.navline').css('width', per * 6 - per / 2 + '%');
                $(
                    'td:nth-child(1) .wBtn,td:nth-child(2) .wBtn,td:nth-child(3) .wBtn,td:nth-child(4) .wBtn, td:nth-child(5) .wBtn, td:nth-child(6) .wBtn'
                ).css('border', '4px solid #00bcd4');
                $(
                    '.wT td:nth-child(1) span,.wT td:nth-child(2) span,.wT td:nth-child(3) span,.wT td:nth-child(4) span, .wT td:nth-child(5) span, .wT td:nth-child(6) span'
                ).css('color', '#00bcd4');
            });

            $('.wT td:last-child() .wBtn, .signnext').click(function() {
                $('.navline').css('width', '100%');
                $(
                    'td:nth-child(1) .wBtn,td:nth-child(2) .wBtn,td:nth-child(3) .wBtn,td:nth-child(4) .wBtn, td:nth-child(5) .wBtn, td:nth-child(6) .wBtn, td:nth-child(7) .wBtn'
                ).css('border', '4px solid #00bcd4');
                $('.wT td span').css('color', '#00bcd4');
            });
        });
        // console.log('isEnrolledWizardTab1 ' + (this.isEnrolledWizardTab === true));
    }

    changecolor(): boolean {
        // console.log('isEnrolledWizardTab2 ' + (this.isEnrolledWizardTab === true));

        {
            if (this.integerval === 2) {
                return false;
            }
            if (this.userToBeEnrolled.username === this.principal.getLoggedInUser().username) {
                return false;
            }
            this.integerval = 2;
            console.log('called 22 changecolor' + this.integerval);

            let x;
            x = $('.wT').find('td').length;
            const per = 100 / x;
            this.wizardTdsCount.emit(x);
            console.log($('.wT td:nth-child(1) span').css('color'));
            $('.navline').css('width', per / 2 + '%');
            $('.wT td:nth-child(1) span').css('color', '#00bcd4');
            $('.mainbbtn').click(function() {
                $(this).addClass('active');
            });
            $('.wT td:nth-child(1) .wBtn,.infoback').click(function() {
                $('.navline').css('width', per / 2 + '%');
                $('.wT td:nth-child(1) span').css('color', '#00bcd4');
            });

            $('.wT td:nth-child(2) .wBtn, .infonext, .fpback').click(function() {
                $('.navline').css('width', per * 2 - per / 2 + '%');
                $('td:nth-child(1) .wBtn,td:nth-child(2) .wBtn').css('border', '4px solid #00bcd4');
                $('.wT td:nth-child(1) span,.wT td:nth-child(2) span').css('color', '#00bcd4');
            });

            $('.wT td:nth-child(3) .wBtn, .facenext, .irisback').click(function() {
                $('.navline').css('width', per * 3 - per / 2 + '%');
                $('td:nth-child(1) .wBtn,td:nth-child(2) .wBtn,td:nth-child(3) .wBtn').css('border', '4px solid #00bcd4');
                $('.wT td:nth-child(1) span,.wT td:nth-child(2) span,.wT td:nth-child(3) span').css('color', '#00bcd4');
            });

            $('.wT td:nth-child(4) .wBtn, .fpnext, .signback').click(function() {
                $('.navline').css('width', per * 4 - per / 2 + '%');
                $('td:nth-child(1) .wBtn,td:nth-child(2) .wBtn,td:nth-child(3) .wBtn,td:nth-child(4) .wBtn').css(
                    'border',
                    '4px solid #00bcd4'
                );
                $('.wT td:nth-child(1) span,.wT td:nth-child(2) span,.wT td:nth-child(3) span,.wT td:nth-child(4) span').css(
                    'color',
                    '#00bcd4'
                );
            });

            $('.wT td:nth-child(5) .wBtn, .irisnext, .summaryback').click(function() {
                $('.navline').css('width', per * 5 - per / 2 + '%');
                $('td:nth-child(1) .wBtn,td:nth-child(2) .wBtn,td:nth-child(3) .wBtn,td:nth-child(4) .wBtn, td:nth-child(5) .wBtn').css(
                    'border',
                    '4px solid #00bcd4'
                );
                $(
                    '.wT td:nth-child(1) span,.wT td:nth-child(2) span,.wT td:nth-child(3) span,.wT td:nth-child(4) span, .wT td:nth-child(5) span'
                ).css('color', '#00bcd4');
            });

            $('.wT td:nth-child(6) .wBtn, .signnext').click(function() {
                $('.navline').css('width', per * 6 - per / 2 + '%');
                $(
                    'td:nth-child(1) .wBtn,td:nth-child(2) .wBtn,td:nth-child(3) .wBtn,td:nth-child(4) .wBtn, td:nth-child(5) .wBtn, td:nth-child(6) .wBtn'
                ).css('border', '4px solid #00bcd4');
                $(
                    '.wT td:nth-child(1) span,.wT td:nth-child(2) span,.wT td:nth-child(3) span,.wT td:nth-child(4) span, .wT td:nth-child(5) span, .wT td:nth-child(6) span'
                ).css('color', '#00bcd4');
            });

            $('.wT td:last-child() .wBtn, .signnext').click(function() {
                $('.navline').css('width', '100%');
                $(
                    'td:nth-child(1) .wBtn,td:nth-child(2) .wBtn,td:nth-child(3) .wBtn,td:nth-child(4) .wBtn, td:nth-child(5) .wBtn, td:nth-child(6) .wBtn, td:nth-child(7) .wBtn'
                ).css('border', '4px solid #00bcd4');
                $('.wT td span').css('color', '#00bcd4');
            });
        }
        return true;
    }

    changeChildColor(e) {
        alert(e.srcElement.attributes.style.color);
        e.srcElement.child[0].attributes.style.color = 'red';
        alert(e.srcElement.child[0].attributes.style.color);
    }

    onWizardTabClick(selectedWizardTab: string) {
        this.tabClick.emit(selectedWizardTab);
    }

    hasWorkflowStep(workflowStep: string): boolean {
        if (!this.userToBeEnrolled || !this.userToBeEnrolled.workflowSteps) {
            return false;
        }

        if (this.userToBeEnrolled.workflowSteps.includes(workflowStep)) {
            return true;
        }
        return false;
    }
}
