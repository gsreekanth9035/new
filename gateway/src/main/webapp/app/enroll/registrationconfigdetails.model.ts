export class RegistrationConfigDetails {
    ranks: Array<TypeDescription>;
    department: Array<TypeDescription>;
    employeeAffiliation: Array<TypeDescription>;
    employeeColorCode: Array<TypeDescription>;
    physicalccEye: Array<TypeDescription>;
    physicalccHair: Array<TypeDescription>;
    physicalccHeight: Array<TypeDescription>;
    gender: Array<TypeDescription>;
    bloodType: Array<TypeDescription>;
    healthGroupPlan: Array<TypeDescription>;
    drivingLicenseEndorsements: Array<TypeDescription>;
    constructor() {}
}

export class TypeDescription {
    type: string;
    description: string;
    constructor() {}
}
