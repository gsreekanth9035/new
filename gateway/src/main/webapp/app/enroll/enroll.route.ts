import { Routes } from '@angular/router';

import { EnrollComponent } from './enroll.component';
import { EnrollmentDetailsComponent } from './enrollment-details/enrollment-details.component';

export const ENROLL_ROUTE: Routes = [
    {
        path: 'register',
        component: EnrollComponent,
        data: {
            authorities: [],
            pageTitle: 'enroll.pageTitle',
            permissions: ['ENROLLMENT'],
        }
    },
    {
        path: 'enrollmentDetails',
        component: EnrollmentDetailsComponent,
        data: {
            authorities: [],
            pageTitle: 'enroll.title',
            permissions: ['ENROLLMENT'],
        }
    },
];
