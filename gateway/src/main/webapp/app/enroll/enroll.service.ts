import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';

import { User } from './user.model';
import { Principal } from 'app/core';
import { IDProofType } from './idProofType.model';
import { IDProofIssuingAuthority } from './userIdProof.model';
import { VehicleClassifications } from './vehicleClassifications.model';
import { DrivingLicenseRestrictions } from './drivingLicenseRestrictions.model';
import { WorkflowStep } from 'app/workflow/workflow-step.model';
import { RegistrationConfigDetails } from './registrationconfigdetails.model';
import { Workflow } from 'app/workflow/workflow.model';
import { UserBioInfo } from 'app/approve-requests/model/user-bio-info.model';
import { EnrollmentStepsStatus } from './enrollmentStepsStatus.model';

@Injectable({
    providedIn: 'root'
})
export class EnrollService {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name,
        loggedInUserId: '' + this.principal.getLoggedInUser().userId
        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });
    constructor(private httpClient: HttpClient, private principal: Principal) {}

    saveIDProofDocumentsStepToDB(user: User): Promise<boolean> {
        const promise = new Promise<boolean>((resolve, reject) => {
            this.httpHeaders.append('Content-type', 'application/json');
            this.httpClient
                .post(
                    `${window.location.origin}/usermanagement/api/v1/organizations/${
                        this.principal.getLoggedInUser().organization
                    }/users/saveIDProofDocs`,
                    user,
                    { headers: this.httpHeaders, observe: 'response' }
                )
                .toPromise()
                .then(response => {
                    // Success
                    resolve(true);
                })
                .catch(response => {
                    // failure
                    resolve(false);
                });
        });
        return promise;
    }
    saveFaceStepToDB(user: User, type: String): Promise<boolean> {
        const promise = new Promise<boolean>((resolve, reject) => {
            this.httpHeaders.append('Content-type', 'application/json');
            this.httpClient
                .post(
                    `${window.location.origin}/usermanagement/api/v1/organizations/${
                        this.principal.getLoggedInUser().organization
                    }/users/saveUserBiometrics/${type}`,
                    user,
                    { headers: this.httpHeaders, observe: 'response' }
                )
                .toPromise()
                .then(response => {
                    // Success
                    resolve(true);
                })
                .catch(response => {
                    // failure
                    resolve(false);
                });
        });
        return promise;
    }
    saveIRISStepToDB(user: User, type: String): Promise<boolean> {
        const promise = new Promise<boolean>((resolve, reject) => {
            this.httpHeaders.append('Content-type', 'application/json');
            this.httpClient
                .post(
                    `${window.location.origin}/usermanagement/api/v1/organizations/${
                        this.principal.getLoggedInUser().organization
                    }/users/saveUserBiometrics/${type}`,
                    user,
                    { headers: this.httpHeaders, observe: 'response' }
                )
                .toPromise()
                .then(response => {
                    // Success
                    resolve(true);
                })
                .catch(response => {
                    // failure
                    resolve(false);
                });
        });
        return promise;
    }
    saveFingerPrintsStepToDB(user: User, type: String): Promise<boolean> {
        const promise = new Promise<boolean>((resolve, reject) => {
            this.httpHeaders.append('Content-type', 'application/json');
            this.httpClient
                .post(
                    `${window.location.origin}/usermanagement/api/v1/organizations/${
                        this.principal.getLoggedInUser().organization
                    }/users/saveUserBiometrics/${type}`,
                    user,
                    { headers: this.httpHeaders, observe: 'response' }
                )
                .toPromise()
                .then(response => {
                    // Success
                    resolve(true);
                })
                .catch(response => {
                    // failure
                    resolve(false);
                });
        });
        return promise;
    }
    saveSkipFingerPrintsStepToDB(user: User, type: String): Promise<boolean> {
        const promise = new Promise<boolean>((resolve, reject) => {
            this.httpHeaders.append('Content-type', 'application/json');
            this.httpClient
                .post(
                    `${window.location.origin}/usermanagement/api/v1/organizations/${
                        this.principal.getLoggedInUser().organization
                    }/users/saveUserBiometrics/${type}`,
                    user,
                    { headers: this.httpHeaders, observe: 'response' }
                )
                .toPromise()
                .then(response => {
                    // Success
                    resolve(true);
                })
                .catch(response => {
                    // failure
                    resolve(false);
                });
        });
        return promise;
    }
    saveSignatureStepToDB(user: User, type: String): Promise<boolean> {
        const promise = new Promise<boolean>((resolve, reject) => {
            this.httpHeaders.append('Content-type', 'application/json');
            this.httpClient
                .post(
                    `${window.location.origin}/usermanagement/api/v1/organizations/${
                        this.principal.getLoggedInUser().organization
                    }/users/saveUserBiometrics/${type}`,
                    user,
                    { headers: this.httpHeaders, observe: 'response' }
                )
                .toPromise()
                .then(response => {
                    // Success
                    resolve(true);
                })
                .catch(response => {
                    // failure
                    resolve(false);
                });
        });
        return promise;
    }
    saveRegistrationFieldsStepToDB(user: User): Promise<boolean> {
        const promise = new Promise<boolean>((resolve, reject) => {
            this.httpHeaders.append('Content-type', 'application/json');
            this.httpClient
                .post(
                    `${window.location.origin}/usermanagement/api/v1/organizations/${
                        this.principal.getLoggedInUser().organization
                    }/users/saveRegistration`,
                    user,
                    { headers: this.httpHeaders, observe: 'response' }
                )
                .toPromise()
                .then(response => {
                    // Success
                    resolve(true);
                })
                .catch(response => {
                    // failure
                    resolve(false);
                });
        });
        return promise;
    }
    saveEnrollmentHistoryStepToDB(user: User): Promise<boolean> {
        const promise = new Promise<boolean>((resolve, reject) => {
            this.httpHeaders.append('Content-type', 'application/json');
            this.httpClient
                .post(
                    `${window.location.origin}/usermanagement/api/v1/organizations/${
                        this.principal.getLoggedInUser().organization
                    }/users/saveEnrollmentHistory`,
                    user,
                    { headers: this.httpHeaders, observe: 'response' }
                )
                .toPromise()
                .then(response => {
                    // Success
                    resolve(true);
                })
                .catch(response => {
                    // failure
                    resolve(false);
                });
        });
        return promise;
    }
    completeEnrollmentInProgress(user: User): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.httpClient.post(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/users/completeEnrollmentInProgress`,
            user,
            { headers: this.httpHeaders, observe: 'response' }
        );
    }

    getEnrollmentStepsStatus(userToEnroll: String): Observable<EnrollmentStepsStatus> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<EnrollmentStepsStatus>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/users/${userToEnroll}/enrollmentStepsStatus`,
            { headers: this.httpHeaders }
        );
    }

    registerLoginUser(user: User): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.httpClient.post(
            `${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/users/register`,
            user,
            { headers: this.httpHeaders, observe: 'response' }
        );
    }

    registerUser(user: User): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.httpClient.post(
            `${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/users`,
            user,
            { headers: this.httpHeaders, observe: 'response' }
        );
    }

    updateUser(user: User): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.httpClient.put(
            `${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/users`,
            user,
            { headers: this.httpHeaders, observe: 'response' }
        );
    }

    getWorkflowByUsername(username: string): Observable<string> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.httpClient.get(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/users/${username}/workflowGeneralDetails`,
            { headers: this.httpHeaders, responseType: 'text' }
        );
    }

    getWorkflowGeneralDetailsByUserName(username: string): Observable<Workflow> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<Workflow>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/users/${username}/workflowGeneralDetails`,
            { headers: this.httpHeaders }
        );
    }

    getFingerprintDeviceConfig(fingerprintDeviceManfacture: string, fingerprintDeviceName: string): Observable<any> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<any>(
            `${
                window.location.origin
            }/usermanagement/api/v1/organizations/${organization}/deviceManufacturer/${fingerprintDeviceManfacture}/deviceName/${fingerprintDeviceName}`,
            {
                headers: this.httpHeaders,
                observe: 'response'
            }
        );
    }

    getWorkflowStepsByWorkflow(workflowName: string): Observable<WorkflowStep[]> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<WorkflowStep[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/workflows/${workflowName}/steps`,
            { headers: this.httpHeaders }
        );
    }

    getWorkflowStepDetail(stepName: string, workflowName: string): Observable<WorkflowStep> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<WorkflowStep>(
            `${
                window.location.origin
            }/usermanagement/api/v1/organizations/${organization}/workflows/${workflowName}/steps/${stepName}/details`,
            { headers: this.httpHeaders }
        );
    }

    getWorkflowCredentialTemplates(username: string): Observable<WFStepVisualTemplateConfig[]> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<WFStepVisualTemplateConfig[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/users/${username}/workflow-credential-templates`,
            { headers: this.httpHeaders }
        );
    }

    getVisualCredential(credentialTemplateID: string, user: User, isTransparentPhotoSelected): Observable<VisualCredential> {
        this.httpHeaders.append('Content-type', 'application/json');
        const organization = this.principal.getLoggedInUser().organization;
        const userName = user.name;

        return this.httpClient.post<VisualCredential>(
            `${
                window.location.origin
            }/usermanagement/api/v1/organizations/${organization}/users/${userName}/visual-credentials/${credentialTemplateID}?transparentPhoto=${isTransparentPhotoSelected}`,
            user,
            { headers: this.httpHeaders }
        );
    }

    getVisualCredentialInSummary(
        credentialTemplateID: string,
        user: User,
        isTransparentPhotoSelected: boolean
    ): Observable<VisualCredential> {
        this.httpHeaders.append('Content-type', 'application/json');
        const organization = this.principal.getLoggedInUser().organization;
        const userName = user.name;
        user.userBiometrics = null;
        return this.httpClient.post<VisualCredential>(
            `${
                window.location.origin
            }/usermanagement/api/v1/organizations/${organization}/users/${userName}/visual-credentials-on-summary/${credentialTemplateID}?transparentPhoto=${isTransparentPhotoSelected}`,
            user,
            { headers: this.httpHeaders }
        );
    }

    convertRawImageToPngImage(rawDataString: string, imgWidth: number, imgHeight: number): Observable<any> {
        const data = {
            rawData: rawDataString,
            imageType: 'png',
            imageWidth: imgWidth,
            imageHeight: imgHeight
        };
        return this.httpClient.post(`${window.location.origin}/usermanagement/api/v1/fingerprint/extractFingerImage`, data, {
            headers: this.httpHeaders,
            responseType: 'text'
        });
    }

    getIdProofType(): Observable<IDProofType[]> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<IDProofType[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/idProof/idProofTypes`,
            {
                headers: this.httpHeaders
            }
        );
    }
    getVehicleClassifications(): Observable<VehicleClassifications[]> {
        const organization = this.principal.getLoggedInUser().organization;
        // const userName = user.name;
        return this.httpClient.get<VehicleClassifications[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/vehicle/vehicleClassifications`,
            {
                headers: this.httpHeaders
            }
        );
    }
    getDrivingLicenseRestrictions(): Observable<DrivingLicenseRestrictions[]> {
        const organization = this.principal.getLoggedInUser().organization;
        // const userName = user.name;
        return this.httpClient.get<DrivingLicenseRestrictions[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/drivingLicense/restrictions`,
            {
                headers: this.httpHeaders
            }
        );
    }

    getIssuingAuthorities(idProofTypeId: number): Observable<IDProofIssuingAuthority[]> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<IDProofIssuingAuthority[]>(
            `${
                window.location.origin
            }/usermanagement/api/v1/organizations/${organization}/idProof/idProofTypes/${idProofTypeId}/issuingAuthorities`,
            {
                headers: this.httpHeaders
            }
        );
    }

    getUserDetailsByName(userName: string): Observable<User> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<User>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/${userName}/details`,
            {
                headers: this.httpHeaders
            }
        );
    }

    getUserRegistrationConfigDetailsByOrgName(id: number): Observable<RegistrationConfigDetails> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<RegistrationConfigDetails>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/registrationConfigDetails/identitymodel/${id}`,
            {
                headers: this.httpHeaders
            }
        );
    }

    getUserBiometricsByUserId(userId: number): Observable<UserBioInfo> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<UserBioInfo>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/${userId}/biometrics`,
            {
                headers: this.httpHeaders
            }
        );
    }
    getTransparentPhoto(user: User, width1, height1): Observable<User> {
        const httpHeaders = new HttpHeaders({
            width: width1,
            height: height1
        });
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.post<User>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/biometrics/face/transparent`,
            user,
            { headers: httpHeaders }
        );
    }
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    }
}

export class WFStepVisualTemplateConfig {
    id: number;
    visualTemplateName: string;
}

export class VisualCredential {
    front: string;
    back: string;
}

export class FingerModel {
    deviceName: string;
    deviceId: any;
    deviceSerialNo: string;
    fPosition: string;
    viewFPDeviceName: string;
    manufacturar: string;
    constructor() {}
}

export class FingerpintDeviceConfig {
    ansiTemplateSupport: boolean;
    captureType: string;
    deviceManufacturer: string;
    deviceName: string;
    flatSupport: boolean;
    id: number;
    isoTemplateSupport: boolean;
    organization: any;
    rollSupport: boolean;
    version: any;
    constructor() {}
}

export class IrisModel {
    deviceId: any;
    deviceName: string;
    deviceSerialNo: string;
    viewIrisDeviceName: string;
    constructor() {}
}

export class IrisCaptureResponse {
    irisDetails: IrisDetails;
    responseCode: string;
}

export class IrisDetails {
    iris: Iris;
}

export class Iris {
    leftIris: string;
    rightIris: string;
    constructor() {}
}

export class FingerCaptureResponse {
    fingerPrintDetails: FingerPrintDetails;
    responseCode: string;
}

export class FingerPrintDetails {
    LeftFingers: LeftFingers;
    RightFingers: RightFingers;
    ThumbFingers: ThumbFingers;
    SingleFinger: SingleFinger;
    Fingers: Fingers;
    constructor() {}
}
export class Fingers {
    FirstFinger: FingerImageDetails;
    SecondFinger: FingerImageDetails;
    image: string;
    imageType: string;
    noOfFingers: any;
    quality: any;
}

export class LeftFingers {
    LeftIndex: FingerImageDetails;
    LeftLittle: FingerImageDetails;
    LeftMiddle: FingerImageDetails;
    LeftRing: FingerImageDetails;
    height: any;
    image: string;
    noOfFingers: any;
    quality: number;
    width: any;
    imageType: string;
}

export class RightFingers {
    RightIndex: FingerImageDetails;
    RightLittle: FingerImageDetails;
    RightMiddle: FingerImageDetails;
    RightRing: FingerImageDetails;
    height: any;
    image: string;
    noOfFingers: any;
    quality: number;
    width: any;
    imageType: string;
}

export class ThumbFingers {
    LeftThumb: FingerImageDetails;
    RightThumb: FingerImageDetails;
    height: any;
    image: string;
    noOfFingers: any;
    quality: number;
    width: any;
    imageType: string;
}

export class FingerImageDetails {
    image: string;
    quality: number;
    ansi: string;
    iso: string;
}

export class SingleFinger {
    ansi: string;
    height: any;
    image: string;
    imageType: string;
    iso: string;
    quality: number;
    width: any;
    wsqImage: string;
}
