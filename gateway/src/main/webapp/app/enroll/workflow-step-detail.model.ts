export class WorkflowStepDetail {

    wfStepGenericConfigs: WfStepGenericConfig[];
    wfStepRegistrationConfigs: WfStepRegistrationConfig[];
    constructor() {}

}

export class WfStepGenericConfig {
    name: string;
    value: string;
}

export class WfStepRegistrationConfig {
    id: number;
    fieldName: string;
    fieldLabel: string;
    required: boolean;
}
