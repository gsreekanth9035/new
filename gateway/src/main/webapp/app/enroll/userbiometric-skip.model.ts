export class UserBiometricSkip {
    type: string;
    position: string;
    skipType: string;
    skipReason: string;

    constructor() {}
}
