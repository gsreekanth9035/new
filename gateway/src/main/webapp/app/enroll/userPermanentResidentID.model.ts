export class UserPermanentResidentID {
    public docNumber: string;

    public dateOfIssuance: string;

    public dateOfExpiry: string;

    public personalCode: string;

    public userId: number;

    public uimsID: string;

    public passportNumber: string;
    public residentcardTypeNumber: string;
    public residentcardTypeCode: string;

    constructor() {}
}
