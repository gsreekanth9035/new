export class UserHealthServiceVaccineInfo {
    public uimsId: string;
    public vaccine: string;
    public route: string;
    public site: string;
    public date: string;
    public administeredBy: string;
    constructor() {}
}
