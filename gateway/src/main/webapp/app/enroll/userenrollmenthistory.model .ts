export class UserEnrollmentHistory {
    id: number;
    modifiedsteps: string;
    status: string;
    approvalStatus: string;
    approvedBy: string;
    approvedate: string;
    reason: string;
    rejectedBy: Date;
    rejectedDate: string;
    enrolledBy: string;
    enrollmentStartDate: Date;
    enrollmentEndDate: Date;

    constructor() {}
}
