export class SigantureListResponse {
    responseCode: string;
    signatureResponse: SigantureModel[] = [];
}
export class SigantureModel {
    deviceId: any;
    deviceName: string;
    deviceSerialNo: string;
    firmWare: string;
}
export class SigantureCaptureResponse {
    responseCode: string;
    signatureResponse: SignatureResponse;
}
export class SignatureResponse {
    imageIsoData: string;
    imageType: string;
    imageData: string;
}
