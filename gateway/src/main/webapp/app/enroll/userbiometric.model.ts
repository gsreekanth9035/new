export class UserBiometric {
    type: string;
    position: string;
    format: string;
    data: any;
    wsqData: any;
    pngData: any;
    jpegData: any;
    imageQuality: number;
    ansi: any;
    base64OfTransparentImageData: any;
    userBiometricAttributes: UserBiometricAttribute[] = [];

    constructor() {}
}
export class UserBiometricAttribute {
	name: string;
	value: string;
}
