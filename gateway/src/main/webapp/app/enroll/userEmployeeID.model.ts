export class UserEmployeeID {
    public employeeIDNumber: string;

    public dateOfIssuance: string;

    public dateOfExpiry: string;

    public affiliation: string;

    public department: string;

    public issuerIN: string;

    public agencyCardSn: string;

    public userId: number;

    public uimsId: string;

    constructor() {}
}
