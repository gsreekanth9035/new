export class UserDrivingLicense {
    public licenseNumber: string;

    public documentDescriminator: string;

    public issuingAuthority: string;

    public licenseCategory: string;

    public dateOfIssuance: string;

    public dateOfExpiry: string;

    public vehicleClass: string;

    public restrictions: string;

    public endorsements: string;

    public vehicleClassification: string;

    public restriction_id: number;

    public vehicle_classification_id: number;

    public under18Until: string;

    public under19Until: string;

    public under21Until: string;

    public auditInformation: string;
    public uimsID: string;
    constructor() {}
}
