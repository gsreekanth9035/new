import { OrganizationIdentity } from 'app/workflow/model/org-identity-model';

export class UserIDProof {
    id: number;
    name: string;
    idProofTypeName: string;
    idProofTypeId: number;
    issuingAuthority: string;
    issuingAuthorityId: number;
    idProofType = new OrganizationIdentity();
    idProofIssuingAuthority = new IDProofIssuingAuthority();
    frontImg;
    backImg;
    source: string;
    file: any;
    pdfFile: any;
    // fileType: string;
    frontFileType: string;
    backFileType: string;
    docId: number;
    frontFileName;
    backFileName;
    constructor() {}
}
export class IDProofIssuingAuthority {
    id: number;
    issuingAuthority: string;
    constructor() {}
}
