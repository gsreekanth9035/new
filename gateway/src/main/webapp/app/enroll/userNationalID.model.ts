export class UserNationalID {
    public docNumber: string;

    public dateOfIssuance: string;

    public dateOfExpiry: string;

    public personalCode: string;

    public userId: number;

    public uimsID: string;

    public vaccineName: string;
    public vaccinationDate: Date;
    public vaccinationLocationName: string;
    public vaccinationLocation: string;

    constructor() {}
}
