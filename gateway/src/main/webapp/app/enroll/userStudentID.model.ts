export class UserStudentID {
    public studentIDNumber: string;

    public dateOfIssuance: string;

    public dateOfExpiry: string;

    public institution: string;

    public department: string;

    public issuerIN: string;

    public agencyCardSNS: string;

    public userId: number;

    public uimsId: string;

    constructor() {}
}
