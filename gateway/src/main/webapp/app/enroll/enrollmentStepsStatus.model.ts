export class EnrollmentStepsStatus {
    id: number;
    userId: number;
    idproofStatus: number;
    registrationFormStatus: number;
    faceStatus: number;
    irisStatus: number;
    fingerprintStatus: number;
    rollFingerprintStatus: number;
    signatureStatus: number;
    summaryStatus: number;
    comments: string;
    version: number;

    constructor() {}
}
