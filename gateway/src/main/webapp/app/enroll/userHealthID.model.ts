export class UserHealthID {
    public idNumber: string;

    public dateOfIssuance: string;

    public dateOfExpiry: string;

    public primarySubscriber: string;

    public primarySubscriberID: string;

    public pcp: string;

    public pcpPhone: string;

    public issuerIN: string;

    public agencyCardSNS: string;

    public policyNumber: string;

    public groupPlan: string;

    public healthPlanNumber: string;

    public userId: number;

    public uimsId: string;

    constructor() {}
}
