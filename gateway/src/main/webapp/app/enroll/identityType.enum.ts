export const enum IdentityTypeEnum {
    PIV = 'Government',
    EMPLOYEE_ID = 'Workforce',
    NATIONAL_ID = 'National ID',
    PERMANENT_RESIDENT_ID = 'Permanent Resident ID',
    DRIVINGLICENSE = 'Driving License',
    VEHICLE_ID = 'Vehicle Identification ID',
    HEALTH_ID = 'Health ID',
    VOTER_ID = 'Voter ID',
    STUDENT_ID = 'Student ID',
    BIRTH_CERTIFICATE = 'Birth Certificate',
    MARRIAGE_CERTIFICATE = 'Marriage Certificate',
    DIVORCE_CERTIFICATE = 'Divorce Certificate',
    DEATH_CERTIFICATE = 'Death Certificate',
    PASSPORT = 'Passport',
    HEALTH_SERVICE_ID = 'Health Service ID'
}
