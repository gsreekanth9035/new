export class UserAttribute {
    firstName: string;
    middleName: string;
    lastName: string;
    mothersMaidenName: string;
    email: string;
    userId: any;
    public dateOfBirth: Date;
    public nationality: string;
    public placeOfBirth: string;
    public gender: string;
    public countryCode: string;
    public contact: string;
    public address: string;
    public veteran: boolean;
    public organDonor: boolean;
    public bloodType: string;
    constructor() {}
}
