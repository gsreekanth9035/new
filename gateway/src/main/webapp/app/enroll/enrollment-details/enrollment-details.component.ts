import { OnInit, Component, ViewChild, Inject, LOCALE_ID } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MatTableDataSource, MatSort } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from 'app/shared/util/date.adapter';
import { DataService, Data } from 'app/device-profile/data.service';
import { Router } from '@angular/router';
import { EnrollService, VisualCredential, WFStepVisualTemplateConfig } from '../enroll.service';
import { UserIDProof } from '../userIdProof.model';
import { UserInfo } from 'app/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../user.model';
import { UserAppearance } from '../userappearance.model';
import { UserAttribute } from '../userattribute.model';
import { UserDrivingLicense } from '../userdrivinglicense.model';
import { formatDate } from '@angular/common';
import { UserBiometric } from '../userbiometric.model';
import { UserBiometricSkip } from '../userbiometric-skip.model';
import { SearchUserService } from 'app/search-user/search-user.service';
import { TranslateService } from '@ngx-translate/core';
import { UserHealthService } from '../userHealthService.model';
import { UserHealthID } from '../userHealthID.model';
import { UserEmployeeID } from '../userEmployeeID.model';
import { UserStudentID } from '../userStudentID.model';
import { UserPIV } from '../userPIV.model';
import { UserNationalID } from '../userNationalID.model';
import { UserVoterID } from '../userVoterID.model';
import { UserPermanentResidentID } from '../userPermanentResidentID.model';
import { UserHealthServiceVaccineInfo } from '../userHealthServiceVaccineInfo.model';
import { UserAddress } from '../user-address.model';
import { MatPaginator, MatDatepickerInputEvent } from '@angular/material';
import { UserEnrollmentHistory } from '../userenrollmenthistory.model ';
@Component({
    selector: 'jhi-enroll-summary-details',
    templateUrl: './enrollment-details.component.html',
    styleUrls: ['enrollment-details.scss', '../enroll.scss'],
    providers: [{ provide: DateAdapter, useClass: AppDateAdapter }, { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }]
})
export class EnrollmentDetailsComponent implements OnInit {
    // Id proofing starts
    isAnimate = false;
    openFrontImageBlock = false;
    openBackImageBlock = false;
    // Id proofing ends

    message: string;
    display = true;
    // face
    isCropped = false;
    croppedImg: any;

    // Iris
    leftEye: string;
    rightEye: string;
    // fp
    leftFingerprintImages: UserBiometric[] = [];
    rightFingerprintImages: UserBiometric[] = [];
    skippedFingers: UserBiometricSkip[] = [];

    leftRollFingerprintImages: UserBiometric[] = [];
    rightRollFingerprintImages: UserBiometric[] = [];
    skippedRollFingers: UserBiometricSkip[] = [];

    isUserDetialsLoaded = false;
    data: Data;
    userIdProofDocuments: UserIDProof[] = [];
    userName: string;
    enrolledUser: UserInfo = new UserInfo();
    user = new User();
    registrationStepDetail: any;
    enrollForm: FormGroup;
    // visual design
    credentialTemplates: WFStepVisualTemplateConfig[] = [];
    selectedCredentialTemplate: FormControl = new FormControl('');
    visualCredential: VisualCredential;

    dataSource: MatTableDataSource<UserIDProof>;
    public signature: any = null;
    @ViewChild(MatSort) sort: MatSort;

    idProofDisplayedColumns: string[] = ['Type', 'Issuing Authority', 'Front Image', 'Back Iamge'];

    isLoadingResults: boolean;
    isFederatedUser = false;
    userFirstAndLastName;
    groupName: string;
    identityType: string;
    showToolTip = false;
    isTransparentPhotoAllowed = false;
    //enrollment history
    historydata: UserEnrollmentHistory[] = [];
    displayedColumns: string[] = [
        'modifiedsteps',
        'status',
        'enrolledBy',
        'approvalStatus',
        'approvedBy',
        'approvedate',
        'reason',
        'rejectedBy',
        'rejectedDate',
        'enrollmentStartDate',
        'enrollmentEndDate'
    ];
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sorts: MatSort;

    constructor(
        private dataService: DataService,
        private enrollService: EnrollService,
        private formBuilder: FormBuilder,
        @Inject(LOCALE_ID) private locale: string,
        private router: Router,
        private searchUserService: SearchUserService,
        private translateService: TranslateService
    ) {
        this.isLoadingResults = true;
        this.buildForm();
        this.data = this.dataService.getCurrentObj();
        if (this.data != null) {
            this.userFirstAndLastName = this.data.userFirstName + ' ' + this.data.userLastName;
            this.groupName = this.data.groupName;
            this.identityType = this.data.identityType;
            this.isFederatedUser = this.data.isFederated;
            this.userName = this.data.userName;
            this.enrolledUser.username = this.userName;
            this.enrollService.getWorkflowGeneralDetailsByUserName(this.userName).subscribe(
                wfName => {
                    let workflowSteps: any;
                    this.enrolledUser.workflow = wfName.name;
                    this.enrolledUser.workflowSteps = [];
                    workflowSteps = this.enrollService.getWorkflowStepsByWorkflow(this.enrolledUser.workflow).subscribe(
                        response_steps => {
                            response_steps.forEach(step => {
                                this.enrolledUser.workflowSteps.push(step.name);
                            });
                            this.enrollService.getWorkflowStepDetail('REGISTRATION', this.enrolledUser.workflow).subscribe(
                                registrationStepDetail => {
                                    this.registrationStepDetail = registrationStepDetail;
                                    const configs = registrationStepDetail.wfStepRegistrationConfigs;
                                    this.enrollService.getUserDetailsByName(this.userName).subscribe(userDetails => {
                                        let userHealthServiceVaccineInfos: UserHealthServiceVaccineInfo[] = [];
                                        if (userDetails.userHealthService !== null) {
                                            userHealthServiceVaccineInfos = userDetails.userHealthService.userHealthServiceVaccineInfos;
                                        }
                                        this.user = userDetails;
                                        this.userIdProofDocuments = userDetails.userIDProofDocuments;
                                        this.historydata = userDetails.userEnrollmentHistoryList;
                                        this.setIdProofDoc(this.userIdProofDocuments);
                                        if (configs) {
                                            if (this.registrationStepDetail.wfStepRegistrationConfigs) {
                                                this.registrationStepDetail.wfStepRegistrationConfigs.forEach(registrationField => {
                                                    this.enrollForm.addControl(
                                                        registrationField.id,
                                                        this.addFormControl(registrationField.required)
                                                    );
                                                    if (registrationField.fieldName === 'Contact Number') {
                                                        this.enrollForm.addControl(
                                                            'countryCode',
                                                            new FormControl('', [
                                                                Validators.required,
                                                                Validators.pattern('^[0-9 +]+$'),
                                                                Validators.maxLength(4)
                                                            ])
                                                        );
                                                    }
                                                });
                                            }
                                            this.setUserDetails(
                                                userDetails,
                                                userDetails.userAttribute,
                                                userDetails.userAppearance,
                                                userDetails.userPIV,
                                                userDetails.userDrivingLicense,
                                                userDetails.userHealthService,
                                                userHealthServiceVaccineInfos,
                                                userDetails.userNationalID,
                                                userDetails.userVoterID,
                                                userDetails.userPermanentResidentID,
                                                userDetails.userEmployeeID,
                                                userDetails.userStudentID,
                                                userDetails.userHealthID,
                                                userDetails.userAddress
                                            );
                                        }
                                        // set biometric data
                                        this.setBiometrics(userDetails.userBiometrics);
                                        this.enrollService.getWorkflowCredentialTemplates(this.userName).subscribe(
                                            async visualTemplates => {
                                                // Load Available Visual Templates
                                                this.credentialTemplates = visualTemplates;
                                                if (this.credentialTemplates && this.credentialTemplates.length === 1) {
                                                    this.selectedCredentialTemplate.setValue(this.credentialTemplates[0].id);
                                                    await this.getVisualCredential(userDetails);
                                                } else {
                                                    this.isLoadingResults = false;
                                                }
                                            },
                                            error => {
                                                this.isLoadingResults = false;
                                            }
                                        );
                                        this.isUserDetialsLoaded = true;
                                    });
                                },
                                err => {
                                    this.isLoadingResults = false;
                                }
                            );
                        },
                        err => {
                            this.isLoadingResults = false;
                        }
                    );
                },
                err => {
                    this.isLoadingResults = false;
                }
            );
        }
        //  this.isLoadingResults = false;
    }
    ngOnInit() {}
    buildForm() {
        this.enrollForm = this.formBuilder.group({});
    }
    formatmeetidentityDate(date: string) {
        if (date !== null && date !== '' && date !== undefined) {
            return formatDate(date, 'MM/dd/yyyy', this.locale);
        }
    }
    addFormControl(required: boolean): FormControl {
        if (required) {
            return new FormControl('', Validators.required);
        }
        return new FormControl('');
    }
    setIdProofDoc(docs: UserIDProof[]) {
        docs.forEach(userDocument => {
            if (userDocument.frontFileName !== null || userDocument.backFileName !== null) {
                userDocument.source = 'upload';
                if (userDocument.frontImg !== null) {
                    userDocument.frontImg = `data:${userDocument.frontFileType};base64,` + userDocument.frontImg;
                }
                if (userDocument.backImg !== null) {
                    userDocument.backImg = `data:${userDocument.backFileType};base64,` + userDocument.backImg;
                }
            } else {
                userDocument.source = 'capture';
                if (userDocument.frontImg !== null) {
                    userDocument.frontImg = `data:${userDocument.frontFileType};base64,` + userDocument.frontImg;
                }
                if (userDocument.backImg !== null) {
                    userDocument.backImg = `data:${userDocument.backFileType};base64,` + userDocument.backImg;
                }
            }
            if (userDocument.file !== null) {
                userDocument.source = 'upload';
                const file = `data:${userDocument.frontFileType};base64,` + userDocument.file;
                //   userDocument.fileName = userDocument.name;
                userDocument.file = file;
            }
        });
        this.dataSource = new MatTableDataSource<UserIDProof>(docs);
    }
    downloadPdfFile(base64File, name) {
        const linkSource = base64File;
        const downloadLink = document.createElement('a');
        downloadLink.style.display = 'none';
        document.body.appendChild(downloadLink);
        const fileName = name;

        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        document.body.removeChild(downloadLink);
    }

    setUserDetails(
        userDetails: User,
        userAttribute: UserAttribute,
        userAppearance: UserAppearance,
        userPIV: UserPIV,
        userDrivingLicense: UserDrivingLicense,
        userHealthService: UserHealthService,
        userHealthServiceVaccineInfos: UserHealthServiceVaccineInfo[],
        userNationalID: UserNationalID,
        userVoterID: UserVoterID,
        userPermanentResidentID: UserPermanentResidentID,
        userEmployeeID: UserEmployeeID,
        userStudentID: UserStudentID,
        userHealthID: UserHealthID,
        userAddress: UserAddress
    ) {
        this.registrationStepDetail.wfStepRegistrationConfigs.forEach(registrationField => {
            switch (registrationField.fieldName) {
                case 'Given Name (First & Last Name)': {
                    if (userAttribute != null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.firstName);
                    }
                    break;
                }
                case 'Middle Name': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.middleName);
                    }
                    break;
                }
                case 'Surname/Family Name': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.lastName);
                    }
                    break;
                }
                case 'Mother’s Maiden Name': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.mothersMaidenName);
                    }
                    break;
                }
                case 'Date of Birth': {
                    if (userAttribute !== null) {
                        if (userAttribute.dateOfBirth !== null) {
                            this.enrollForm.controls[registrationField.id].setValue(new Date(userAttribute.dateOfBirth));
                        }
                    }
                    break;
                }
                case 'Birth Place/ Nationality': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.nationality);
                    }
                    break;
                }
                case 'Nationality': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.nationality);
                    }
                    break;
                }
                case 'Place of Birth': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.placeOfBirth);
                    }
                    break;
                }
                case 'Personal Code': {
                    if (userNationalID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userNationalID.personalCode);
                    }
                    if (userPermanentResidentID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userPermanentResidentID.personalCode);
                    }
                    break;
                }
                case 'Passport Number': {
                    if (userPermanentResidentID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userPermanentResidentID.passportNumber);
                    }
                    break;
                }
                case 'Resident Card Type Number': {
                    if (userPermanentResidentID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userPermanentResidentID.residentcardTypeNumber);
                    }
                    break;
                }
                case 'Resident Card Type Code': {
                    if (userPermanentResidentID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userPermanentResidentID.residentcardTypeCode);
                    }
                    break;
                }
                case 'Vaccine Name': {
                    if (userNationalID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userNationalID.vaccineName);
                    }
                    break;
                }
                case 'Vaccination Date': {
                    if (userNationalID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(new Date(userNationalID.vaccinationDate));
                    }
                    break;
                }
                case 'Vaccination Location Name': {
                    if (userNationalID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userNationalID.vaccinationLocationName);
                    }
                    break;
                }
                case 'Gender': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.gender);
                    }
                    break;
                }
                case 'Blood Type': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.bloodType);
                    }
                    break;
                }
                case 'Address': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.address);
                    }
                    break;
                }
                case 'Email': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.email);
                    }
                    break;
                }
                case 'Contact': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.contact);
                    }
                    break;
                }
                case 'Contact Number': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.contact);
                        if (userAttribute.countryCode !== null) {
                            this.enrollForm.controls.countryCode.setValue(userAttribute.countryCode);
                        }
                    }
                    break;
                }
                case 'Organ Donor': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.organDonor);
                    }
                    break;
                }
                case 'Veteran': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.veteran);
                    }
                    break;
                }
                case 'Hair Color': {
                    if (userAppearance !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAppearance.hairColor);
                    }
                    break;
                }
                case 'Eye Color': {
                    if (userAppearance !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAppearance.eyeColor);
                    }
                    break;
                }
                case 'Height': {
                    if (userAppearance !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAppearance.height);
                    }
                    break;
                }
                case 'Weight': {
                    if (userAppearance !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAppearance.weight);
                    }
                    break;
                }
                case 'Address Line1': {
                    if (userAddress !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAddress.addressLine1);
                    }
                    break;
                }
                case 'Address Line2': {
                    if (userAddress !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAddress.addressLine2);
                    }
                    break;
                }
                case 'City': {
                    if (userAddress !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAddress.city);
                    }
                    break;
                }
                case 'State': {
                    if (userAddress !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAddress.state);
                    }
                    break;
                }
                case 'Country': {
                    if (userAddress !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAddress.country);
                    }
                    break;
                }
                case 'ZIP/Postal Code': {
                    if (userAddress !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAddress.zipCode);
                    }
                    break;
                }
                case 'Rank/Grade/Employee Status': {
                    if (userPIV !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userPIV.rank);
                    }
                    break;
                }
                case 'Employee Affiliation': {
                    if (userPIV !== null) {
                        if (userPIV.employeeAffiliation !== null) {
                            this.enrollForm.controls[registrationField.id].setValue(userPIV.employeeAffiliation);
                        }
                    }
                    if (userEmployeeID !== null) {
                        if (userEmployeeID.affiliation !== null) {
                            this.enrollForm.controls[registrationField.id].setValue(userEmployeeID.affiliation);
                        }
                    }
                    break;
                }
                case 'Employee Affiliation Color Code': {
                    if (userPIV !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userPIV.employeeAffiliationColorCode);
                    }
                    break;
                }
                case 'Restrictions': {
                    if (userDrivingLicense !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userDrivingLicense.restrictions);
                    }
                    break;
                }
                case 'Vehicle Classification': {
                    if (userDrivingLicense !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userDrivingLicense.vehicleClass);
                    }
                    break;
                }
                case 'Endorsements': {
                    if (userDrivingLicense !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userDrivingLicense.endorsements);
                    }
                    break;
                }
                case 'Donor': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.organDonor);
                    }
                    break;
                }
                case 'Veteran': {
                    if (userAttribute !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userAttribute.veteran);
                    }
                    break;
                }
                case 'Department': {
                    if (userStudentID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userStudentID.department);
                    }
                    if (userEmployeeID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userEmployeeID.department);
                    }
                    break;
                }
                case 'Primary Subscriber': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.primarySubscriber);
                    }
                    break;
                }
                case 'Subscriber ID': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.primarySubscriberID);
                    }
                    break;
                }
                case 'Primary Doctor': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.pcp);
                    }
                    break;
                }
                case 'Primary Doctor Phone': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.pcpPhone);
                    }
                    break;
                }
                case 'Policy Number': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.policyNumber);
                    }
                    break;
                }
                case 'Group Plan': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.groupPlan);
                    }
                    break;
                }
                case 'Health Plan Number': {
                    if (userHealthID !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthID.healthPlanNumber);
                    }
                    break;
                }
                case 'Vaccine': {
                    if (userHealthServiceVaccineInfos !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthServiceVaccineInfos[0].vaccine);
                    }
                    break;
                }
                case 'Route': {
                    if (userHealthServiceVaccineInfos !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthServiceVaccineInfos[0].route);
                    }
                    break;
                }
                case 'Site': {
                    if (userHealthServiceVaccineInfos !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthServiceVaccineInfos[0].site);
                    }
                    break;
                }
                case 'Date': {
                    if (userHealthServiceVaccineInfos !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(new Date(userHealthServiceVaccineInfos[0].date));
                    }
                    break;
                }
                case 'Administered By': {
                    if (userHealthServiceVaccineInfos !== null) {
                        this.enrollForm.controls[registrationField.id].setValue(userHealthServiceVaccineInfos[0].administeredBy);
                    }
                    break;
                }
            }
        });
    }
    setBiometrics(biometrics: UserBiometric[]) {
        if (biometrics !== null) {
            biometrics.forEach(userBiometric => {
                switch (userBiometric.type) {
                    case 'FACE': {
                        this.isCropped = true;
                        this.croppedImg = `data:image/${userBiometric.format};base64,` + userBiometric.data;
                        if (userBiometric.userBiometricAttributes.length > 0) {
                            userBiometric.userBiometricAttributes.forEach(e => {
                                if (e.name === 'TRANSPARENT_PHOTO_ALLOWED') {
                                    if (e.value === 'true') {
                                        this.isTransparentPhotoAllowed = true;
                                    }
                                }
                            });
                        }
                        break;
                    }
                    case 'IRIS': {
                        if (userBiometric.position === 'LEFT_EYE') {
                            this.leftEye = userBiometric.data;
                        } else if (userBiometric.position === 'RIGHT_EYE') {
                            this.rightEye = userBiometric.data;
                        }
                        break;
                    }
                    case 'SIGNATURE': {
                        this.signature = `data:image/${userBiometric.format};base64,` + userBiometric.data;
                        break;
                    }
                    case 'FINGERPRINT': {
                        if (userBiometric.position === 'LEFT_THUMB') {
                            this.leftFingerprintImages[0] = userBiometric;
                        }
                        if (userBiometric.position === 'LEFT_INDEX') {
                            this.leftFingerprintImages[1] = userBiometric;
                        }
                        if (userBiometric.position === 'LEFT_MIDDLE') {
                            this.leftFingerprintImages[2] = userBiometric;
                        }
                        if (userBiometric.position === 'LEFT_RING') {
                            this.leftFingerprintImages[3] = userBiometric;
                        }
                        if (userBiometric.position === 'LEFT_LITTLE') {
                            this.leftFingerprintImages[4] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_THUMB') {
                            this.rightFingerprintImages[0] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_INDEX') {
                            this.rightFingerprintImages[1] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_MIDDLE') {
                            this.rightFingerprintImages[2] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_RING') {
                            this.rightFingerprintImages[3] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_LITTLE') {
                            this.rightFingerprintImages[4] = userBiometric;
                        }
                        break;
                    }
                    case 'ROLL_FINGERPRINT': {
                        if (userBiometric.position === 'LEFT_THUMB') {
                            this.leftRollFingerprintImages[0] = userBiometric;
                        }
                        if (userBiometric.position === 'LEFT_INDEX') {
                            this.leftRollFingerprintImages[1] = userBiometric;
                        }
                        if (userBiometric.position === 'LEFT_MIDDLE') {
                            this.leftRollFingerprintImages[2] = userBiometric;
                        }
                        if (userBiometric.position === 'LEFT_RING') {
                            this.leftRollFingerprintImages[3] = userBiometric;
                        }
                        if (userBiometric.position === 'LEFT_LITTLE') {
                            this.leftRollFingerprintImages[4] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_THUMB') {
                            this.rightRollFingerprintImages[0] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_INDEX') {
                            this.rightRollFingerprintImages[1] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_MIDDLE') {
                            this.rightRollFingerprintImages[2] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_RING') {
                            this.rightRollFingerprintImages[3] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_LITTLE') {
                            this.rightRollFingerprintImages[4] = userBiometric;
                        }
                        break;
                    }
                }
            });
        }
    }
    onCredentialTemplateChanged(event: any) {
        const user: User = this.user;
        // user.id = null;
        user.userIDProofDocuments = null;
        user.workflowName = this.enrolledUser.workflow;
        this.getVisualCredential(user);
    }
    getVisualCredential(userDetails: User): void {
        const user: User = userDetails;
        // user.id = null;
        user.userIDProofDocuments = null;
        user.workflowName = this.enrolledUser.workflow;
        this.enrollService
            .getVisualCredentialInSummary(this.selectedCredentialTemplate.value, user, this.isTransparentPhotoAllowed)
            .subscribe(
                response => {
                    this.visualCredential = response;
                    this.isLoadingResults = false;
                },
                error => {
                    this.isLoadingResults = false;
                }
            );
    }
    hasWorkflowStep(workflowStep: string): boolean {
        if (!this.enrolledUser || !this.enrolledUser.workflowSteps) {
            return false;
        }

        if (this.enrolledUser.workflowSteps.includes(workflowStep)) {
            return true;
        }

        return false;
    }

    navigateToDashboard() {
        this.router.navigate(['/dashboard']);
    }
    navigateToSearch() {
        this.router.navigate(['/searchUser']);
    }
    goBack() {
        this.router.navigate([this.data.routeFrom]);
    }

    deleteUser() {
        this.searchUserService.deleteUser(this.userName, this.isFederatedUser).subscribe(result => {
            if (result.status === 200) {
                this.translateService.get('searchUser.deletedSuccessMsg').subscribe(message => {
                    this.display = false;
                    window.scrollTo(0, 0);
                    this.message = message;
                });
            }
        });
    }
    editEnrollment() {
        this.data = this.dataService.getCurrentObj();
        this.dataService.changeObj(this.data);
        this.router.navigate(['/register']);
    }

    openImage(imageType) {
        if (imageType === 'front') {
            this.openFrontImageBlock = true;
        } else if (imageType === 'back') {
            this.openBackImageBlock = true;
        }
        this.isAnimate = true;
    }

    closeImage() {
        this.openFrontImageBlock = false;
        this.openBackImageBlock = false;
    }

    check(attr, toolTipId): void {
        const val = document.getElementById(toolTipId);
        this.showToolTip = val.offsetWidth < val.scrollWidth ? true : false;
    }
}
