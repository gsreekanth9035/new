export class UserPIV {
    public id: number;

    public uimsID: string;

    public employeeAffiliation: string;

    public affiliationOptionalLine: string;

    public employeeAffiliationColorCode: string;

    public employeePhotoBorderColor: string;

    public rank: string;

    public agency: string;

    public agencyAbbreviation: string;

    public agencyCardSerialNumber: string;

    public issuerIdentificationNumber: string;

    public agencySpecificData: string;

    public agencySpecificText: string;

    public dateOfIssuance: string;

    public dateOfExpiry: string;

    public cardExpiry: string;

    public pdf417BarCode: string;

    public linearBarCode: string;

    constructor() {}
}
