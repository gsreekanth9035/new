export class UserVoterID {
    public curp: string;

    public dateOfIssuance: string;

    public dateOfExpiry: string;

    public userId: number;

    public uimsID: string;

    public registrationYear: string;
    public electorKey: string;

    constructor() {}
}
