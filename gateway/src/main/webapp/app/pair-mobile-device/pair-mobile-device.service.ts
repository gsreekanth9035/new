import { ErrorHandlerInterceptor } from './../blocks/interceptor/errorhandler.interceptor';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Principal } from 'app/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable({
    providedIn: 'root'
})
export class PairMobileDeviceService {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name
        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });

    constructor(private httpClient: HttpClient, private principal: Principal) {}

    getUserQRCode(userName): Observable<string> {
        const organizationName = this.principal.getLoggedInUser().organization;
        return this.httpClient
            .get(`${window.location.origin}/usermanagement/api/v1/organizations/${organizationName}/users/${userName}/pairDevice`, {
                responseType: 'text'
            })
            .catch(this.errorHandler);
    }
    errorHandler(error: HttpErrorResponse) {
        return Observable.throw('Unable to generate QR code. Please try again or contact Admin.');
    }

    getWebauthnMobileDeviceConfig(userName): Observable<WebauthnRegisterationConfig> {
        const organizationName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<WebauthnRegisterationConfig>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organizationName}/users/${userName}/execute-webauthn`
        );
    }

    getPairMobileDeviceConfig(userName): Observable<PairMobileDeviceConfig> {
        const organizationName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<PairMobileDeviceConfig>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organizationName}/users/${userName}/pairDevice`
        );
    }

    getUserStatus(userName: string): Observable<any> {
        const organizationName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organizationName}/users/${userName}/user-status`
        );
    }
}

export class PairMobileDeviceConfig {
    googlePlayStoreAppUrl: string;
    appleAppStoreAppUrl: string;
    qrCodeExpiryInMins: number;
    qrCode: string;
    qrCodeWidth: number;
    qrCodeHeight: number;
    secretKey: String;
    pairMobileDeviceSecretKeyLength: number;
    pairMobileDeviceUrl: String;
    enableDemoVideo: boolean;
    disableQRCodeExpiry: boolean;
    status: boolean;
}

export class WebauthnRegisterationConfig {
    base64url: string;
    qrcode: string;
}
