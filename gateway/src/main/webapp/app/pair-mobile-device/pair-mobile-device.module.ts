import { GatewaySharedModule } from './../shared/shared.module';
import { PAIRMOBILEDEVICE_ROUTES } from './pair-mobile-device.route';
import { NgModule } from '@angular/core';
import { PairMobileDeviceComponent, FormatTimePipe, WebAuthnDialogContentComponent } from './pair-mobile-device.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'app/material/material.module';
import { MatTooltipModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        GatewaySharedModule,
        RouterModule.forChild(PAIRMOBILEDEVICE_ROUTES),
        MatTooltipModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MaterialModule
    ],
    entryComponents: [WebAuthnDialogContentComponent],
    declarations: [PairMobileDeviceComponent, FormatTimePipe, WebAuthnDialogContentComponent]
})
export class PairMobileDeviceModule {}
