import { Route, Routes } from '@angular/router';
import { PairMobileDeviceComponent } from './pair-mobile-device.component';

export const PAIRMOBILEDEVICE_ROUTES: Routes = [
    {
        path: 'pairmobile',
        component: PairMobileDeviceComponent,
        data: {
            authorities: [],
            pageTitle: 'pairMobileDevice.title',
        }
    }
];
