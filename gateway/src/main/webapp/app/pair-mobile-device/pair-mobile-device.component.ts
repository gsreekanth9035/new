import { Component, Inject, inject, OnInit } from '@angular/core';
import { PairMobileDeviceService, PairMobileDeviceConfig, WebauthnRegisterationConfig } from './pair-mobile-device.service';
import { Data, DataService } from 'app/device-profile/data.service';
import { LoginService, Principal } from 'app/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { Pipe, PipeTransform } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IssuanceService } from 'app/issuance/issuance.service';
import { TranslateService } from '@ngx-translate/core';
import { Workflow } from 'app/workflow/workflow.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import base64url from 'base64url';
import { Title } from '@angular/platform-browser';
import { OrganizationBrandingInfo } from 'app/layouts/login/login.component';
import { WorkflowService } from 'app/workflow/workflow.service';
import { WorkflowStep } from 'app/workflow/workflow-step.model';
import { WFMobileIDStepIdentityConfig } from 'app/workflow/model/mobile-id-issuance.model';

@Component({
    selector: 'jhi-pair-mobile-device',
    templateUrl: './pair-mobile-device.component.html',
    styleUrls: ['pair-mobile-device.scss']
})
export class PairMobileDeviceComponent implements OnInit {
    errorMsg: string;
    base64QRCodeData: string;
    pairMobileDeviceConfig: PairMobileDeviceConfig;
    workflowStep: WorkflowStep;
    wfMobileIDStepIdentityConfig: WFMobileIDStepIdentityConfig;
    userFirstAndLastName;
    userName;
    data: Data;
    countDown;
    counter = 120;
    tick = 1000;
    showDisableQrCode;
    expiryQrcode: boolean;
    isSelfService = false;
    issuedDevices = 0;
    isAllowedDevicesApplicable = false;
    allowedDevice;
    issuanceAllowed = false;
    isPendingApproval = false;
    message = '';
    onboardSelfService: string;
    userEnrolled: boolean;
    groupName: string;
    workflowName: string;
    identityType: string;
    showToolTip = false;
    organizationName: string;
    isEnrollmentEnabled: boolean = false;
    isFIDO2MobileRegistrationEnabled: boolean = false;
    webauthnRegisterationConfig: WebauthnRegisterationConfig;
    walletApp: string;
    constructor(
        private pairMobileDeviceService: PairMobileDeviceService,
        private workflowservice: WorkflowService,
        private principal: Principal,
        private router: Router,
        private titleService: Title,
        private dataService: DataService,
        private issuanceService: IssuanceService,
        private translateService: TranslateService,
        public dialog: MatDialog,
        private loginService: LoginService,
        private activatedRoute: ActivatedRoute
    ) {
        this.organizationName = this.principal.getLoggedInUser().organization;
    }

    ngOnInit() {
        const brnadingInformation: OrganizationBrandingInfo = this.loginService.getBrandingInfo();
        if (brnadingInformation !== null) {
            this.walletApp = brnadingInformation.mobileAppName;
            this.activatedRoute.snapshot.data['pageTitle'] = `pairMobileDevice.${this.walletApp}`;
            this.translateService.get(`pairMobileDevice.${this.walletApp}`).subscribe(msg => {
                this.titleService.setTitle(msg);
            });
        }
        if (this.principal.getLoggedInUser().roles.includes('role_user')) {
            if (this.principal.getLoggedInUser().authorities.includes('Menu_Enrollment')) {
                this.isEnrollmentEnabled = true;
            }
        }
        this.expiryQrcode = false;
        this.data = this.dataService.getCurrentObj();
        if (this.data !== null) {
            this.userFirstAndLastName = this.data.userFirstName + ' ' + this.data.userLastName;
            this.groupName = this.data.groupName;
            this.identityType = this.data.identityType;
            this.userName = this.data.userName;
            this.isSelfService = false;
            this.onboardSelfService = this.data.onboardSelfService;
            if (this.principal.getLoggedInUser().username === this.userName) {
                this.isSelfService = true;
            }
            this.userEnrolled = true;
            this.loadQrCodeDetails();
        } else {
            // this.userFirstAndLastName = this.principal.getLoggedInUser().firstName + ' ' + this.principal.getLoggedInUser().lastName;
            this.groupName = this.principal.getLoggedInUser().groups[0].name;
            if (this.principal.getLoggedInUser().canIssue !== null) {
                this.userEnrolled = true;
            } else {
                this.userEnrolled = false;
            }
            this.isSelfService = true;
            this.userName = this.principal.getLoggedInUser().username;
            if (this.principal.getLoggedInUser().onboardSelfService === 'true') {
                this.onboardSelfService = 'true';
            } else {
                this.onboardSelfService = 'false';
            }
            // get user status from API
            this.pairMobileDeviceService.getUserStatus(this.userName).subscribe(res => {
                if (res.status === 'PENDING_ENROLLMENT' || res.status === 'ENROLLMENT_IN_PROGRESS') {
                    this.userEnrolled = false;
                    this.issuanceAllowed = false;
                    return;
                } else {
                    this.userEnrolled = true;
                    if (res.status === 'PENDING_APPROVAL') {
                        this.issuanceAllowed = false;
                        this.isPendingApproval = true;
                        return;
                    } else {
                        this.issuanceAllowed = true;
                    }
                }
                this.loadQrCodeDetails();
            });
        }

        // load webauthn fido2
    } // ngOninit ends

    openDialog() {
        this.pairMobileDeviceService.getWebauthnMobileDeviceConfig(this.userName).subscribe(res => {
            this.webauthnRegisterationConfig = res;
            console.log(this.webauthnRegisterationConfig);

            const dialogRef = this.dialog.open(WebAuthnDialogContentComponent, {
                autoFocus: false,
                width: '1090px',
                data: {
                    webauthnRegisterationConfig: this.webauthnRegisterationConfig
                }
            });

            dialogRef.afterClosed().subscribe(result => {
                console.log(`Dialog result: ${result}`);
            });
        });
    }

    loadQrCodeDetails() {
        if (this.userEnrolled === true && this.isPendingApproval === false) {
            if (!(!this.userEnrolled && this.isSelfService && this.onboardSelfService === 'false')) {
                this.issuanceService.getWorkflowGeneralInfoByName(this.userName).subscribe((workflow: Workflow) => {
                    this.identityType = workflow.organizationIdentities.identityTypeName;
                    this.workflowName = workflow.name;
                    this.isAllowedDevicesApplicable = workflow.allowedDevicesApplicable;
                    this.allowedDevice = workflow.allowedDevices;
                    if (this.isAllowedDevicesApplicable) {
                        this.issuanceService.getIssuedDevicesByUserName(this.userName).subscribe(res => {
                            this.issuedDevices = res;
                            if (this.issuedDevices >= this.allowedDevice) {
                                this.translateService
                                    .get('issuance.validations.maxAllowedDeviceError', { user: `${this.userName}` })
                                    .subscribe(
                                        msg => {
                                            this.message = msg;
                                        },
                                        err => {
                                            this.message = `Issuance for user: '${
                                                this.userName
                                            }' is not allowed as total issued devices will exceed maximum allowed devices`;
                                        }
                                    );
                                return;
                            }
                            this.issuanceAllowed = true;
                            this.refreshQrCode();
                            this.getMobileIssuanceDetails();
                        });
                    } else {
                        this.issuanceAllowed = true;
                        this.refreshQrCode();
                        this.getMobileIssuanceDetails();
                    }
                });
            }
        }
        // this.countDown = Observable.timer(0, this.tick)
        // .take(this.counter)
        // .map(() => --this.counter);

        // setTimeout(() => {
        //     this.expiryQrcode = true;
        // }, 120000);
    }

    refreshQrCode() {
        this.pairMobileDeviceService.getPairMobileDeviceConfig(this.userName).subscribe(response => {
            this.pairMobileDeviceConfig = response;
            this.base64QRCodeData = this.pairMobileDeviceConfig.qrCode;
            this.errorMsg = undefined;
            // alert( this.pairMobileDeviceConfig.disableQRCodeExpiry);
            /// alert(this.pairMobileDeviceConfig.enableDemoVideo);
            this.expiryQrcode = false;
            if (this.pairMobileDeviceConfig.disableQRCodeExpiry !== true) {
                this.counter = this.pairMobileDeviceConfig.qrCodeExpiryInMins * 60;
                this.countDown = Observable.timer(0, this.tick)
                    .take(this.counter)
                    .map(() => --this.counter);
                setTimeout(() => {
                    this.expiryQrcode = true;
                }, this.pairMobileDeviceConfig.qrCodeExpiryInMins * 60 * 1000);
            }
        }, error => (this.errorMsg = error));
    }

    getMobileIssuanceDetails() {
        this.workflowservice.getWFMobileIDStepIdentityConfig(this.workflowName).subscribe(response => {
            this.workflowStep = response;
            if (
                this.workflowStep !== null &&
                this.workflowStep.wfMobileIDStepIdentityConfigs != null &&
                this.workflowStep.wfMobileIDStepIdentityConfigs.length > 0
            ) {
                this.wfMobileIDStepIdentityConfig = this.workflowStep.wfMobileIDStepIdentityConfigs[0];
                this.isFIDO2MobileRegistrationEnabled = this.wfMobileIDStepIdentityConfig.fido2;
                //
            }
        }, error => (this.errorMsg = error));
    }

    goSearchPage() {
        this.router.navigate(['/searchUser']);
    }
    check(attr, toolTipId): void {
        const val = document.getElementById(toolTipId);
        this.showToolTip = val.offsetWidth < val.scrollWidth ? true : false;
    }
}

@Pipe({
    name: 'formatTime'
})
export class FormatTimePipe implements PipeTransform {
    transform(value: number): string {
        const minutes: number = Math.floor(value / 60);
        return ('00' + minutes).slice(-2) + ':' + ('00' + Math.floor(value - minutes * 60)).slice(-2);
    }
}

export class DialogData {
    webauthnRegisterationConfig: WebauthnRegisterationConfig;
    user;
    dialogMsg: string;
    constructor() {}
}

@Component({
    templateUrl: 'dialog-content-example-dialog.html'
})
export class WebAuthnDialogContentComponent {
    dialogMsg;
    user;
    configLink;
    configQrcode;
    constructor(public dialogRef: MatDialogRef<WebAuthnDialogContentComponent>, @Inject(MAT_DIALOG_DATA) public dialogData: DialogData) {
        console.log(dialogData.webauthnRegisterationConfig);
        this.configLink = base64url.decode(dialogData.webauthnRegisterationConfig.base64url);
        this.configQrcode = dialogData.webauthnRegisterationConfig.qrcode;
        console.log(this.configLink);
        console.log(this.configQrcode);
    }
    onCancel(): void {
        this.dialogRef.close();
    }
}
