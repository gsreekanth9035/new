import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService, Principal } from 'app/core';
export interface PeriodicElement {
    orgName: string;
    category: string;
    actions: string;
}

const ELEMENT_DATA: PeriodicElement[] = [{ orgName: 'meetidentity', category: 'meetidentity', actions: 'H' }];

@Component({
    selector: 'jhi-manage-organization',
    templateUrl: './manage-organization.component.html',
    styleUrls: ['manage-organization.scss']
})
export class ManageOrganizationComponent implements OnInit {
    displayedColumns: string[] = ['orgName', 'category', 'actions'];
    dataSource = ELEMENT_DATA;
    loginlogo: any;
    logoName: any;

    constructor(
        private loginService: LoginService,
        private router: Router,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute
    ) {}

    ngOnInit() {
        this.loginService.loadBrandingInformation().subscribe(brandingInformation => {
            this.loginlogo = brandingInformation[0];
            this.logoName = brandingInformation[2];
        });
    }

    createOrganization() {
        this.router.navigate(['/organization/create-organization']);
    }
}
