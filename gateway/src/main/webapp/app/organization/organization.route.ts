import { Route, Routes } from '@angular/router';
import { ManageOrganizationComponent } from './manage-organization/manage-organization.component';
import { CreateOrganizationComponent } from './create-organization/create-organization.component';

export const ORGANIZATION_ROUTES: Routes = [
    {
        path: 'organization',
        component: ManageOrganizationComponent,
        data: {
            authorities: [],
            pageTitle: 'manageOrganization.title',
        }
    },
    {
        path: 'organization/create-organization',
        component: CreateOrganizationComponent,
        data: {
            authorities: [],
            pageTitle: 'createOrganization.title',
        }
    }
];
