import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from '../material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ORGANIZATION_ROUTES } from './organization.route';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { ManageOrganizationComponent } from './manage-organization/manage-organization.component';
import { CreateOrganizationComponent } from './create-organization/create-organization.component';

@NgModule({
  imports: [
      RouterModule.forChild(ORGANIZATION_ROUTES),
      CommonModule,
      BrowserAnimationsModule,
      ReactiveFormsModule,
      HttpClientModule,
      MaterialModule,
      GatewaySharedModule,
      AngularSvgIconModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ ManageOrganizationComponent, CreateOrganizationComponent ]
})

export class OrganizationModule { }
