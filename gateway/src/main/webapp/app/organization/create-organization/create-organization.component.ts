import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService, Principal } from 'app/core';

@Component({
    selector: 'jhi-organization',
    templateUrl: './create-organization.component.html',
    styleUrls: ['create-organization.scss']
})

export class CreateOrganizationComponent implements OnInit {
    successMsg = false;
    failureMsg = false;

    organizationForm = new FormGroup({
        category: new FormControl('', Validators.required),
        government: new FormControl('', Validators.required),
        federal: new FormControl('', Validators.required),
        enterprise: new FormControl('', Validators.required),
        health: new FormControl('', Validators.required),
        education: new FormControl('', Validators.required)
    });

    categories = [
        { value: '', viewValue: 'Select' },
        { value: 'government', viewValue: 'Government' },
        { value: 'federal', viewValue: 'Federal' },
        { value: 'enterprise', viewValue: 'Enterprise' },
        { value: 'health', viewValue: 'Health' },
        { value: 'education', viewValue: 'Education' }
    ];

    governmentDeviceTypes = [
        { value: '', viewValue: 'Select', },
        { value: 'national', viewValue: 'National ID' },
        { value: 'driver', viewValue: 'Driver License' },
        { value: 'piv', viewValue: 'PIV ID' },
        { value: 'virtual', viewValue: 'Virtual ID' }
    ];

    federalDeviceTypes = [
        { value: '', viewValue: 'Select' },
        { value: 'piv', viewValue: 'PIV ID' },
    ];

    enterpriseDeviceTypes = [
        { value: '', viewValue: 'Select', },
        { value: 'employee', viewValue: 'Employee ID' },
        { value: 'civ', viewValue: 'CIV' },
    ];

    healthDeviceTypes = [
        { value: '', viewValue: 'Select' },
        { value: 'health', viewValue: ' Health ID' },
    ];

    educationDeviceTypes = [
        { value: '', viewValue: 'Select', },
        { value: 'student', viewValue: 'Student ID' },
        { value: 'employee', viewValue: 'Employee ID' },
    ];

    orgFields = [
        { name: 'Issuer Identification Number', isChecked: true, isDisabled: true, canDelete: false },
        { name: 'Agency Code', isChecked: true, isDisabled: true, canDelete: false },
        { name: 'Employee Afiliation', isChecked: true, isDisabled: true, canDelete: false },
        { name: 'Employee Color code', isChecked: true, isDisabled: true, canDelete: false },
        { name: 'Agency', isChecked: true, isDisabled: true, canDelete: false },
        { name: 'Seal', isChecked: true, isDisabled: true, canDelete: false }
    ];

    orgFieldsOptional = [
        { name: 'Rank', isChecked: false, isDisabled: false, canDelete: true },
        { name: 'Issuing Authority', isChecked: false, isDisabled: false, canDelete: true },
        { name: 'Organization Abbreviation', isChecked: false, isDisabled: false, canDelete: true },
        { name: 'Header', isChecked: false, isDisabled: false, canDelete: true },
        { name: 'Emergency Text', isChecked: false, isDisabled: false, canDelete: true },
        { name: 'Return Address', isChecked: false, isDisabled: false, canDelete: true },
        { name: 'Standard Text', isChecked: false, isDisabled: false, canDelete: true },
        { name: 'DUNS', isChecked: false, isDisabled: false, canDelete: true },
        { name: 'Optional Information', isChecked: false, isDisabled: false, canDelete: true }
    ];
    loginlogo: any;
    logoName: any;
    OrganizationName: any;

    constructor(private loginService: LoginService, private router: Router, private formBuilder: FormBuilder, private route: ActivatedRoute) { }

    ngOnInit() { this.loginService.loadBrandingInformation().subscribe(brandingInformation => {
        this.loginlogo = brandingInformation[0];
        this.OrganizationName = brandingInformation[2];
      });
    }

    onAddField() {
        // for (let i = 0; i < this.orgFieldsOptional.length; i++) {
        //     if (this.orgFields.indexOf(this.orgFieldsOptional[i]) < 0) {
        //         this.orgFields.push(this.orgFieldsOptional[i]);
        //         break;
        //     }
        // }
    }

    saveOrg() {
        this.successMsg = this.organizationForm.valid;
        this.failureMsg = this.organizationForm.invalid;
    }

    goHome() {
        this.router.navigate(['']);
    }

    onAddToOrgFields(i) {
        if (!this.orgFields.includes(this.orgFieldsOptional[i])) {
            console.log(this.orgFieldsOptional[i]);
            this.orgFields.push(this.orgFieldsOptional[i]);
        } else {
            const orgIndex = this.orgFields.findIndex(x => {
                return (x === this.orgFieldsOptional[i]);

            });

            this.orgFields.splice(orgIndex, 1);

            console.log(orgIndex);
        }

    }

    onOptionDelete(i) {
        this.orgFields.splice(i, 1);
    }

}
