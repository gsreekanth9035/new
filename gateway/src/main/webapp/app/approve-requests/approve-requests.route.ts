import { Routes } from '@angular/router';
import { ApproveRequestsComponent } from './approve-requests.component';

export const APPROVEREQUESTS_ROUTE: Routes = [
    {
        path: 'approve-requests',
        component: ApproveRequestsComponent,
        data: {
            authorities: [],
            pageTitle: 'approveRequests.pageTitle'
        }
    }
];
