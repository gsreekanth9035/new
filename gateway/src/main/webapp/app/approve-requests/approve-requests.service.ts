import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Principal } from 'app/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { User } from 'app/enroll/user.model';
import { BiometricVerificationResponse } from './model/biometric-verification-response.model';
import { UserBioInfo } from './model/user-bio-info.model';

@Injectable({
    providedIn: 'root'
})
export class ApproveRequestsService {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name,
        loggedInUserId: '' + this.principal.getLoggedInUser().userId
        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });
    constructor(private httpClient: HttpClient, private principal: Principal) {}

    approveEnrollment(username: string, approvalReason: string) {
        const orgName = this.principal.getLoggedInUser().organization;
        const loggedInUser = this.principal.getLoggedInUser().username;
        const data = {
            approvedBy: loggedInUser,
            reason: approvalReason
        };
        return this.httpClient.put(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/${username}/approveEnrollment`,
            data,
            { headers: this.httpHeaders, observe: 'response' }
        );
    }
    rejectEnrollment(username: string, rejectedReason: string) {
        const orgName = this.principal.getLoggedInUser().organization;
        const loggedInUser = this.principal.getLoggedInUser().username;
        const data = {
            rejectedBy: loggedInUser,
            reason: rejectedReason
        };
        return this.httpClient.put(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/${username}/rejectEnrollment`,
            data,
            { headers: this.httpHeaders, observe: 'response' }
        );
    }

    enroll(userBioInfo: UserBioInfo): Observable<BiometricVerificationResponse> {
        this.httpHeaders.append('Content-type', 'application/json');
        const organization = this.principal.getLoggedInUser().organization;

        return this.httpClient.post<BiometricVerificationResponse>(
            `${window.location.origin}/biometricverification/api/v1/organizations/${organization}/biometricIdentity/enroll`,
            userBioInfo,
            { headers: this.httpHeaders }
        );
    }
    updateUserInNServer(userBioInfo: UserBioInfo): Observable<BiometricVerificationResponse> {
        this.httpHeaders.append('Content-type', 'application/json');
        const organization = this.principal.getLoggedInUser().organization;

        return this.httpClient.post<BiometricVerificationResponse>(
            `${window.location.origin}/biometricverification/api/v1/organizations/${organization}/biometricIdentity/update`,
            userBioInfo,
            { headers: this.httpHeaders }
        );
    }
    fetchDuplicates(userBioInfo): Observable<BiometricVerificationResponse> {
        this.httpHeaders.append('Content-type', 'application/json');
        const organization = this.principal.getLoggedInUser().organization;
        return this.httpClient.post<BiometricVerificationResponse>(
            `${window.location.origin}/biometricverification/api/v1/organizations/${organization}/biometricIdentity/fetchDuplicates`,
            userBioInfo,
            { headers: this.httpHeaders }
        );
    }
    getUserDetailsByUserId(userId: number): Observable<User> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<User>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/getByID/${userId}/details`
        );
    }
}
