import { OnInit, Component, ViewChild, Inject, LOCALE_ID } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MatTableDataSource, MatSort, MatDialog } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from 'app/shared/util/date.adapter';
import { DataService, Data } from 'app/device-profile/data.service';
import { Router } from '@angular/router';
import { UserInfo, Principal } from 'app/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UserBiometric } from 'app/enroll/userbiometric.model';
import { UserBiometricSkip } from 'app/enroll/userbiometric-skip.model';
import { User } from 'app/enroll/user.model';
import { VisualCredential, EnrollService, WFStepVisualTemplateConfig } from 'app/enroll/enroll.service';
import { UserIDProof } from 'app/enroll/userIdProof.model';
import { UserDrivingLicense } from 'app/enroll/userdrivinglicense.model';
import { UserAppearance } from 'app/enroll/userappearance.model';
import { UserStudentID } from 'app/enroll/userStudentID.model';
import { UserEmployeeID } from 'app/enroll/userEmployeeID.model';
import { UserHealthID } from 'app/enroll/userHealthID.model';
import { UserPIV } from 'app/enroll/userPIV.model';
import { UserNationalID } from 'app/enroll/userNationalID.model';
import { UserPermanentResidentID } from 'app/enroll/userPermanentResidentID.model';
import { UserHealthService } from 'app/enroll/userHealthService.model';
import { UserHealthServiceVaccineInfo } from 'app/enroll/userHealthServiceVaccineInfo.model';
import { UserEnrollmentHistory } from 'app/enroll/userenrollmenthistory.model ';
import { SearchUserService } from 'app/search-user/search-user.service';
import { UserAttribute } from 'app/enroll/userattribute.model';
import { formatDate } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { ApproveRequestsService } from './approve-requests.service';
import { Workflow } from 'app/workflow/workflow.model';
import { WFStepRegistrationConfig } from 'app/workflow/wf-step-registration-config.model';
import { WorkflowStep } from 'app/workflow/workflow-step.model';
import { UserBioInfo } from './model/user-bio-info.model';
import { BiometricDuplicateInformation, BiometricVerificationResponse } from './model/biometric-verification-response.model';
import {
    APPROVE_REASONS_IF_DUPLICATES_FOUND,
    APPROVE_REASONS_IF_NO_DUPLICATES,
    DUPLICATE_FOUND,
    NO_DUPLICATE_DATA_FOUND,
    REJECT_REASONS
} from 'app/shared/constants/configValues.constants';
import { ConfigIdValuePair, WorkflowService } from 'app/workflow/workflow.service';
import { AddIdentityDeviceDialogService } from 'app/manage-identity-devices/add-identity-device-dialog.service';
import { UserAddress } from 'app/enroll/user-address.model';

@Component({
    selector: 'jhi-approve-requests',
    templateUrl: './approve-requests.component.html',
    styleUrls: ['approve-requests.scss', '../enroll/enroll.scss'],
    providers: [{ provide: DateAdapter, useClass: AppDateAdapter }, { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }]
})
export class ApproveRequestsComponent implements OnInit {
    // Id proofing starts
    openFrontImageBlock = false;
    openBackImageBlock = false;
    isAnimate = false;
    // Id proofing ends

    message: string;
    display = true;
    displaySuccessMsg = false;
    isDuplicateBiometricChecked = false;

    isUserDetialsLoaded = false;
    data: Data;
    userIdProofDocuments: UserIDProof[] = [];
    userName: string;
    enrolledUser: UserInfo = new UserInfo();
    user = new User();
    registrationStepDetail: any;
    enrollForm: FormGroup;
    // visual design
    credentialTemplates: WFStepVisualTemplateConfig[] = [];
    selectedCredentialTemplate: FormControl = new FormControl('');
    visualCredential: VisualCredential;

    dataSource: MatTableDataSource<UserIDProof>;
    public signature: any = null;
    @ViewChild(MatSort) sort: MatSort;

    idProofDisplayedColumns: string[] = ['Type', 'Issuing Authority', 'Front Image', 'Back Iamge'];

    isLoadingResults: boolean;
    approvalReasons: ConfigIdValuePair[] = [];
    rejectionReasons: ConfigIdValuePair[] = [];
    approveSuccess = false;
    rejectSuccess = false;
    workflow: Workflow;
    hasIssuancePermission = false;

    approvalReason = new FormControl();
    rejectionReason = new FormControl();

    // Adjudication related variables
    isDuplicateBiometricsFound = false;
    matchedUsersCount: number;
    adjudicationUserInfoFields: WFStepRegistrationConfig[] = [];
    duplicatesFoundMsg: string;
    bioDuplicatesList: BiometricDuplicateInformation[] = [];
    sourceUser = new UserEnrollmentDetails(this.formBuilder);
    duplicateUsers: UserEnrollmentDetails[] = [];
    currentDuplicateUser = new UserEnrollmentDetails(this.formBuilder);
    userBioInfo = new UserBioInfo();
    isBiometricsValid = true;
    isFederatedUser = false;
    nserverStatus: string;
    userFirstAndLastName: string;
    groupName: string;
    identityType: string;
    showToolTip = false;
    isTransparentPhotoAllowed = false;
    constructor(
        private approveRequestsService: ApproveRequestsService,
        private dataService: DataService,
        private enrollService: EnrollService,
        private formBuilder: FormBuilder,
        @Inject(LOCALE_ID) private locale: string,
        private router: Router,
        private searchUserService: SearchUserService,
        private translateService: TranslateService,
        private principal: Principal,
        private workflowService: WorkflowService,
        public dialog: MatDialog,
        private identityDeviceDialogService: AddIdentityDeviceDialogService
    ) {
        // this.adjudicationUserInfoFields = ['Family Name', 'Given Name', 'Date of Birth', 'Gender']
        this.principal.hasAuthority('Menu_Issuance').then(res => {
            this.hasIssuancePermission = res;
        });
        this.isLoadingResults = true;
        this.enrollForm = this.formBuilder.group({});
        this.data = this.dataService.getCurrentObj();
        if (this.data != null) {
            this.userFirstAndLastName = this.data.userFirstName + ' ' + this.data.userLastName;
            this.groupName = this.data.groupName;
            this.identityType = this.data.identityType;
            this.isFederatedUser = this.data.isFederated;
            this.userName = this.data.userName;
            this.enrolledUser.username = this.userName;
            this.enrollService.getWorkflowGeneralDetailsByUserName(this.userName).subscribe(
                wf => {
                    this.workflow = wf;
                    this.enrolledUser.workflow = wf.name;
                    this.enrolledUser.workflowSteps = [];
                    this.enrollService.getWorkflowStepsByWorkflow(this.enrolledUser.workflow).subscribe(
                        response_steps => {
                            response_steps.forEach(step => {
                                this.enrolledUser.workflowSteps.push(step.name);
                            });
                            this.sourceUser.workflowSteps = this.enrolledUser.workflowSteps;
                            this.enrollService.getWorkflowStepDetail('REGISTRATION', this.enrolledUser.workflow).subscribe(
                                registrationStepDetail => {
                                    this.registrationStepDetail = registrationStepDetail;
                                    const configs = registrationStepDetail.wfStepRegistrationConfigs;
                                    this.enrollService.getUserDetailsByName(this.userName).subscribe(
                                        userDetails => {
                                            this.user = userDetails;
                                            this.userIdProofDocuments = userDetails.userIDProofDocuments;
                                            if (userDetails.userBiometrics.length !== 0) {
                                                this.enrollService.getUserBiometricsByUserId(this.user.id).subscribe(
                                                    userBioInfo => {
                                                        this.userBioInfo = userBioInfo;
                                                        this.approveRequestsService.fetchDuplicates(userBioInfo).subscribe(
                                                            (dupliateUserRes: BiometricVerificationResponse) => {
                                                                if (dupliateUserRes.statusMessage === DUPLICATE_FOUND) {
                                                                    this.workflowService
                                                                        .getConfigIdValues(APPROVE_REASONS_IF_DUPLICATES_FOUND)
                                                                        .subscribe(
                                                                            configIdValues => {
                                                                                this.approvalReasons = configIdValues;
                                                                                this.bioDuplicatesList = dupliateUserRes.bioDuplicatesList;
                                                                                this.matchedUsersCount = this.bioDuplicatesList.length;
                                                                                this.translateService
                                                                                    .get(
                                                                                        'approveRequests.adjudication.duplicatesFoundMsg',
                                                                                        {
                                                                                            matchedCount: this.matchedUsersCount
                                                                                        }
                                                                                    )
                                                                                    .subscribe(
                                                                                        msg => {
                                                                                            this.duplicatesFoundMsg = msg;
                                                                                            this.isDuplicateBiometricChecked = true;
                                                                                            this.isDuplicateBiometricsFound = true;
                                                                                            this.setSourceUserRegAndBioDetails(
                                                                                                configs,
                                                                                                userDetails
                                                                                            );
                                                                                            this.setUserEnrollmentDetails(
                                                                                                this.currentDuplicateUser,
                                                                                                this.bioDuplicatesList[0]
                                                                                            );
                                                                                        },
                                                                                        err => {
                                                                                            this.duplicatesFoundMsg = `Adjudication: Duplicate found, ${
                                                                                                this.matchedUsersCount
                                                                                            } matches`;
                                                                                        }
                                                                                    );
                                                                            },
                                                                            err => {
                                                                                this.isLoadingResults = false;
                                                                            }
                                                                        );
                                                                } else if (dupliateUserRes.statusMessage === NO_DUPLICATE_DATA_FOUND) {
                                                                    userBioInfo.deDuplicationCheck = false;
                                                                    this.approveRequestsService.enroll(userBioInfo).subscribe(
                                                                        bioVerificationRes => {
                                                                            if (bioVerificationRes.statusMessage === 'DUPLICATE_ID') {
                                                                                this.approveRequestsService
                                                                                    .updateUserInNServer(userBioInfo)
                                                                                    .subscribe(
                                                                                        updateBioRes => {
                                                                                            if (
                                                                                                updateBioRes.statusMessage ===
                                                                                                    'BAD_OBJECT' ||
                                                                                                updateBioRes.statusMessage ===
                                                                                                    'OBJECT_NOT_FOUND'
                                                                                            ) {
                                                                                                // it means user not captured crct facial r fp r iris(biometrics),
                                                                                                // handle this case.. show an error msg
                                                                                                this.isBiometricsValid = false;
                                                                                                this.setSourceUserDetailsIfNoDuplicates(
                                                                                                    configs,
                                                                                                    userDetails
                                                                                                );
                                                                                            } else {
                                                                                                this.setSourceUserDetailsIfNoDuplicates(
                                                                                                    configs,
                                                                                                    userDetails
                                                                                                );
                                                                                            }
                                                                                        },
                                                                                        err => {
                                                                                            this.isLoadingResults = false;
                                                                                        }
                                                                                    );
                                                                            } else if (
                                                                                bioVerificationRes.statusMessage === 'BAD_OBJECT' ||
                                                                                bioVerificationRes.statusMessage === 'OBJECT_NOT_FOUND'
                                                                            ) {
                                                                                // it means user not captured crct facial r fp r iris(biometrics), handle this case.. show an error msg
                                                                                this.isBiometricsValid = false;
                                                                                this.setSourceUserDetailsIfNoDuplicates(
                                                                                    configs,
                                                                                    userDetails
                                                                                );
                                                                            } else {
                                                                                this.setSourceUserDetailsIfNoDuplicates(
                                                                                    configs,
                                                                                    userDetails
                                                                                );
                                                                            }
                                                                        },
                                                                        err => {
                                                                            this.isLoadingResults = false;
                                                                        }
                                                                    );
                                                                } else {
                                                                    this.setSourceUserDetailsIfNoDuplicates(configs, userDetails);
                                                                }
                                                            },
                                                            err => {
                                                                this.isLoadingResults = false;
                                                            }
                                                        );
                                                    },
                                                    err => {
                                                        this.isLoadingResults = false;
                                                    }
                                                );
                                            } else {
                                                // no biometrics for user, so no need to call nserver
                                                this.setSourceUserDetailsIfNoDuplicates(configs, userDetails);
                                            }
                                        },
                                        err => {
                                            this.isLoadingResults = false;
                                        }
                                    );
                                },
                                err => {
                                    this.isLoadingResults = false;
                                }
                            );
                        },
                        err => {
                            this.isLoadingResults = false;
                        }
                    );
                },
                err => {
                    this.isLoadingResults = false;
                }
            );
        }
    }
    setSourceUserDetailsIfNoDuplicates(configs: WFStepRegistrationConfig[], userDetails: User) {
        this.isDuplicateBiometricChecked = true;
        this.setIdProofDoc(this.userIdProofDocuments);
        this.setVisualTemplates(userDetails);
        this.setSourceUserRegAndBioDetails(configs, userDetails);
    }
    setSourceUserRegAndBioDetails(configs: WFStepRegistrationConfig[], userDetails: User) {
        if (configs) {
            let userHealthServiceVaccineInfos: UserHealthServiceVaccineInfo[] = [];
            if (userDetails.userHealthService !== null) {
                userHealthServiceVaccineInfos = userDetails.userHealthService.userHealthServiceVaccineInfos;
            }
            this.setUserRegistrationInfo(this.registrationStepDetail.wfStepRegistrationConfigs, this.sourceUser, this.enrollForm);
            this.setUserDetails(
                this.sourceUser,
                configs,
                userDetails.userAttribute,
                userDetails.userAppearance,
                userDetails.userPIV,
                userDetails.userDrivingLicense,
                userDetails.userHealthService,
                userHealthServiceVaccineInfos,
                userDetails.userNationalID,
                userDetails.userPermanentResidentID,
                userDetails.userEmployeeID,
                userDetails.userStudentID,
                userDetails.userHealthID,
                userDetails.userAddress
            );
            // set biometric data
            this.setBiometrics(this.sourceUser, userDetails.userBiometrics);
            this.isUserDetialsLoaded = true;
        }
    }
    setUserRegistrationInfo(wfStepRegistrationConfigs, user: UserEnrollmentDetails, enrollForm) {
        user.adjudicationUserInfoFields = [];
        if (wfStepRegistrationConfigs) {
            wfStepRegistrationConfigs.forEach(registrationField => {
                if (this.isDuplicateBiometricsFound) {
                    let isFound = false;
                    if (registrationField.fieldName === 'Surname/Family Name') {
                        isFound = true;
                    } else if (registrationField.fieldName === 'Given Name (First & Last Name)') {
                        isFound = true;
                    } else if (registrationField.fieldName === 'Date of Birth') {
                        isFound = true;
                    } else if (registrationField.fieldName === 'Gender') {
                        isFound = true;
                    }
                    if (isFound) {
                        user.adjudicationUserInfoFields.push(registrationField);
                    }
                }
                enrollForm.addControl(registrationField.id, this.addFormControl(registrationField.required));
                if (registrationField.fieldName === 'Contact Number') {
                    enrollForm.addControl(
                        'countryCode',
                        new FormControl('', [Validators.required, Validators.pattern('^[0-9 +]+$'), Validators.maxLength(4)])
                    );
                }
            });
            user.enrollForm = enrollForm;
        }
    }
    setUserEnrollmentDetails(user: UserEnrollmentDetails, bioDuplicateUser: BiometricDuplicateInformation) {
        user.faceMatchScore = bioDuplicateUser.faceInfo.faceMatchedScore;
        user.fingersMatchScore = bioDuplicateUser.fingersMatchingScore;
        user.irisMatchScore = bioDuplicateUser.irisMatchingScore;
        user.id = user.id + 1;
        user.workflowSteps = [];
        this.approveRequestsService.getUserDetailsByUserId(bioDuplicateUser.userID).subscribe(
            userResp => {
                user.userId = bioDuplicateUser.userID;
                user.username = userResp.name;
                user.workflow = userResp.workflow.name;
                this.enrollService.getWorkflowStepsByWorkflow(user.workflow).subscribe(
                    response_steps => {
                        response_steps.forEach(step => {
                            user.workflowSteps.push(step.name);
                        });
                        this.enrollService.getWorkflowStepDetail('REGISTRATION', this.enrolledUser.workflow).subscribe(
                            registrationStepDetail => {
                                const configs = registrationStepDetail.wfStepRegistrationConfigs;
                                if (configs) {
                                    let userHealthServiceVaccineInfos: UserHealthServiceVaccineInfo[] = [];
                                    if (userResp.userHealthService !== null) {
                                        userHealthServiceVaccineInfos = userResp.userHealthService.userHealthServiceVaccineInfos;
                                    }
                                    this.setUserRegistrationInfo(configs, user, user.enrollForm);
                                    this.setUserDetails(
                                        user,
                                        configs,
                                        userResp.userAttribute,
                                        userResp.userAppearance,
                                        userResp.userPIV,
                                        userResp.userDrivingLicense,
                                        userResp.userHealthService,
                                        userHealthServiceVaccineInfos,
                                        userResp.userNationalID,
                                        userResp.userPermanentResidentID,
                                        userResp.userEmployeeID,
                                        userResp.userStudentID,
                                        userResp.userHealthID,
                                        userResp.userAddress
                                    );
                                }
                                this.setBiometrics(user, userResp.userBiometrics);
                                const user1 = user;
                                this.duplicateUsers.push(user1);
                                this.isLoadingResults = false;
                            },
                            err => {
                                this.isLoadingResults = false;
                            }
                        );
                    },
                    err => {
                        this.isLoadingResults = false;
                    }
                );
            },
            err => {
                this.isLoadingResults = false;
            }
        );
    }
    onClickOfLeftArrow(duplicateUser: UserEnrollmentDetails) {
        this.isLoadingResults = true;
        const id = duplicateUser.id;
        if (id !== 1) {
            if (this.duplicateUsers[id - 2] === undefined) {
                this.setUserEnrollmentDetails(this.currentDuplicateUser, this.bioDuplicatesList[id - 2]);
            } else {
                this.currentDuplicateUser = this.duplicateUsers[id - 2];
                this.isLoadingResults = false;
            }
        }
    }
    onClickOfRightArrow(duplicateUser: UserEnrollmentDetails) {
        this.isLoadingResults = true;
        const id: number = duplicateUser.id;
        if (id !== this.matchedUsersCount) {
            if (this.duplicateUsers[id] === undefined) {
                this.currentDuplicateUser = new UserEnrollmentDetails(this.formBuilder);
                this.currentDuplicateUser.id = id;
                this.setUserEnrollmentDetails(this.currentDuplicateUser, this.bioDuplicatesList[id]);
            } else {
                this.currentDuplicateUser = this.duplicateUsers[id];
                this.isLoadingResults = false;
            }
        }
    }
    OnClickOfApprove() {
        this.approvalReason.reset();

        this.approvalReason.setValidators([Validators.required]);
        this.approvalReason.updateValueAndValidity();

        this.rejectionReason.clearValidators();
        this.rejectionReason.markAsUntouched();
        this.rejectionReason.updateValueAndValidity();
    }
    OnClickOfReject() {
        this.rejectionReason.reset();

        this.rejectionReason.setValidators([Validators.required]);
        this.rejectionReason.updateValueAndValidity();

        this.approvalReason.clearValidators();
        this.approvalReason.markAsUntouched();
        this.approvalReason.updateValueAndValidity();
    }
    approveUser() {
        this.isLoadingResults = true;
        this.approveSuccess = false;
        const reason1 = this.approvalReason.value;
        if (this.isBiometricsValid) {
            if (this.isDuplicateBiometricsFound) {
                this.userBioInfo.deDuplicationCheck = false;
                this.approveRequestsService.updateUserInNServer(this.userBioInfo).subscribe(res => {this.isLoadingResults = false;
                }, err => {
                    this.isLoadingResults = false;
                });
            }
            this.approveRequestsService.approveEnrollment(this.enrolledUser.username, reason1).subscribe(
                response => {
                    const statuscode = response.status;
                    if (statuscode === 200) {
                        this.display = false;
                        this.isDuplicateBiometricChecked = false;
                        this.displaySuccessMsg = true;
                        this.approveSuccess = true;
                        window.scrollTo(0, 0);
                        this.translateService
                            .get('enroll.messages.success.approvalSuccess', { user: this.enrolledUser.username, reason: reason1 })
                            .subscribe(
                                msg => {
                                    this.message = msg;
                                },
                                err => {
                                    this.message = `Enrollment is approved of the User: '${
                                        this.enrolledUser.username
                                    }' with Reason: ${reason1}`;
                                }
                            );
                    }
                    this.isLoadingResults = false;
                }, error => {
                    this.isLoadingResults = false;
                }
            );
        } else {
            this.isLoadingResults = false;
            window.scrollTo(0, 0);
        }
    }
    rejectUser() {
        this.isLoadingResults = false;
        this.rejectSuccess = false;
        const reason1 = this.rejectionReason.value;
        this.approveRequestsService.rejectEnrollment(this.enrolledUser.username, reason1).subscribe(
            response => {
                const statuscode = response.status;
                if (statuscode === 200) {
                    this.rejectSuccess = true;
                    this.display = false;
                    this.isDuplicateBiometricChecked = false;
                    this.displaySuccessMsg = true;
                    window.scrollTo(0, 0);
                    this.translateService
                        .get('enroll.messages.success.rejectionSuccess', { user: this.enrolledUser.username, reason: reason1 })
                        .subscribe(
                            msg => {
                                this.message = msg;
                            },
                            err => {
                                this.message = `Enrollment is rejected of the User: '${
                                    this.enrolledUser.username
                                }' with Reason: ${reason1}`;
                            }
                        );
                }
                this.isLoadingResults = false;
            }, error => {
                this.isLoadingResults = false;
            }
        );
    }
    
    formatmeetidentityDate(date: string) {
        if (date !== null && date !== '' && date !== undefined) {
            return formatDate(date, 'dd/MM/yyyy', this.locale);
        }
    }
    addFormControl(required: boolean): FormControl {
        if (required) {
            return new FormControl('', Validators.required);
        }
        return new FormControl('');
    }
    ngOnInit() {
        this.workflowService.getConfigIdValues(APPROVE_REASONS_IF_NO_DUPLICATES).subscribe(configIdValues => {
            this.approvalReasons = configIdValues;
        });
        this.workflowService.getConfigIdValues(REJECT_REASONS).subscribe(configIdValues => {
            this.rejectionReasons = configIdValues;
        });
    }
    setIdProofDoc(docs: UserIDProof[]) {
        docs.forEach(userDocument => {
            if (userDocument.frontFileName !== null || userDocument.backFileName !== null) {
                userDocument.source = 'upload';
                if (userDocument.frontImg !== null) {
                    userDocument.frontImg = `data:${userDocument.frontFileType};base64,` + userDocument.frontImg;
                }
                if (userDocument.backImg !== null) {
                    userDocument.backImg = `data:${userDocument.backFileType};base64,` + userDocument.backImg;
                }
            } else {
                userDocument.source = 'capture';
                if (userDocument.frontImg !== null) {
                    userDocument.frontImg = `data:${userDocument.frontFileType};base64,` + userDocument.frontImg;
                }
                if (userDocument.backImg !== null) {
                    userDocument.backImg = `data:${userDocument.backFileType};base64,` + userDocument.backImg;
                }
            }
            if (userDocument.file !== null) {
                userDocument.source = 'upload';
                const file = `data:${userDocument.frontFileType};base64,` + userDocument.file;
                //   userDocument.fileName = userDocument.name;
                userDocument.file = file;
            }
        });
        this.dataSource = new MatTableDataSource<UserIDProof>(docs);
    }
    downloadPdfFile(base64File, name) {
        const linkSource = base64File;
        const downloadLink = document.createElement('a');
        downloadLink.style.display = 'none';
        document.body.appendChild(downloadLink);
        const fileName = name;

        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        document.body.removeChild(downloadLink);
    }
    setUserDetails(
        user: UserEnrollmentDetails,
        wfStepRegistrationConfigs,
        userAttribute: UserAttribute,
        userAppearance: UserAppearance,
        userPIV: UserPIV,
        userDrivingLicense: UserDrivingLicense,
        userHealthService: UserHealthService,
        userHealthServiceVaccineInfos: UserHealthServiceVaccineInfo[],
        userNationalID: UserNationalID,
        userPermanentResidentID: UserPermanentResidentID,
        userEmployeeID: UserEmployeeID,
        userStudentID: UserStudentID,
        userHealthID: UserHealthID,
        userAddress: UserAddress
    ) {
        wfStepRegistrationConfigs.forEach(registrationField => {
            switch (registrationField.fieldName) {
                case 'Given Name (First & Last Name)': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.firstName);
                    }
                    break;
                }
                case 'Middle Name': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.middleName);
                    }
                    break;
                }
                case 'Surname/Family Name': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.lastName);
                    }
                    break;
                }
                case 'Mother’s Maiden Name': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.mothersMaidenName);
                    }
                    break;
                }
                case 'Date of Birth': {
                    if (userAttribute !== null) {
                        if (userAttribute.dateOfBirth !== null) {
                            user.enrollForm.controls[registrationField.id].setValue(new Date(userAttribute.dateOfBirth));
                        }
                    }
                    break;
                }
                case 'Birth Place/ Nationality': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.nationality);
                    }
                    break;
                }
                case 'Nationality': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.nationality);
                    }
                    break;
                }
                case 'Place of Birth': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.placeOfBirth);
                    }
                    break;
                }
                case 'Personal Code': {
                    if (userNationalID !== null) {
                        if (userNationalID.personalCode !== null) {
                            user.enrollForm.controls[registrationField.id].setValue(userNationalID.personalCode);
                        }
                    }
                    if (userPermanentResidentID !== null) {
                        if (userPermanentResidentID.personalCode !== null) {
                            user.enrollForm.controls[registrationField.id].setValue(userPermanentResidentID.personalCode);
                        }
                    }
                    break;
                }
                case 'Passport Number': {
                    if (userPermanentResidentID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userPermanentResidentID.passportNumber);
                    }
                    break;
                }
                case 'Resident Card Type Number': {
                    if (userPermanentResidentID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userPermanentResidentID.residentcardTypeNumber);
                    }
                    break;
                }
                case 'Resident Card Type Code': {
                    if (userPermanentResidentID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userPermanentResidentID.residentcardTypeCode);
                    }
                    break;
                }
                case 'Vaccine Name': {
                    if (userNationalID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userNationalID.vaccineName);
                    }
                    break;
                }
                case 'Vaccination Date': {
                    if (userNationalID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(new Date(userNationalID.vaccinationDate));
                    }
                    break;
                }
                case 'Vaccination Location Name': {
                    if (userNationalID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userNationalID.vaccinationLocationName);
                    }
                    break;
                }
                case 'Gender': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.gender);
                    }
                    break;
                }
                case 'Blood Type': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.bloodType);
                    }
                    break;
                }
                case 'Address': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.address);
                    }
                    break;
                }
                case 'Email': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.email);
                    }
                    break;
                }
                case 'Contact':
                case 'Contact Number': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.contact);
                        if (userAttribute.countryCode !== null) {
                            user.enrollForm.controls.countryCode.setValue(userAttribute.countryCode);
                        }
                    }
                    break;
                }
                case 'Organ Donor': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.organDonor);
                    }
                    break;
                }
                case 'Veteran': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.veteran);
                    }
                    break;
                }
                case 'Hair Color': {
                    if (userAppearance !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAppearance.hairColor);
                    }
                    break;
                }
                case 'Eye Color': {
                    if (userAppearance !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAppearance.eyeColor);
                    }
                    break;
                }
                case 'Height': {
                    if (userAppearance !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAppearance.height);
                    }
                    break;
                }
                case 'Weight': {
                    if (userAppearance !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAppearance.weight);
                    }
                    break;
                }
                case 'Address Line1': {
                    if (userAddress !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAddress.addressLine1);
                    }
                    break;
                }
                case 'Address Line2': {
                    if (userAddress !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAddress.addressLine2);
                    }
                    break;
                }
                case 'City': {
                    if (userAddress !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAddress.city);
                    }
                    break;
                }
                case 'State': {
                    if (userAddress !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAddress.state);
                    }
                    break;
                }
                case 'Country': {
                    if (userAddress !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAddress.country);
                    }
                    break;
                }
                case 'ZIP/Postal Code': {
                    if (userAddress !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAddress.zipCode);
                    }
                    break;
                }
                case 'Rank/Grade/Employee Status': {
                    if (userPIV !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userPIV.rank);
                    }
                    break;
                }
                case 'Employee Affiliation': {
                    if (userPIV !== null) {
                        if (userPIV.employeeAffiliation !== null) {
                            user.enrollForm.controls[registrationField.id].setValue(userPIV.employeeAffiliation);
                        }
                    }
                    if (userEmployeeID !== null) {
                        if (userEmployeeID.affiliation !== null) {
                            user.enrollForm.controls[registrationField.id].setValue(userEmployeeID.affiliation);
                        }
                    }
                    break;
                }
                case 'Employee Affiliation Color Code': {
                    if (userPIV !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userPIV.employeeAffiliationColorCode);
                    }
                    break;
                }
                case 'Restrictions': {
                    if (userDrivingLicense !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userDrivingLicense.restrictions);
                    }
                    break;
                }
                case 'Vehicle Classification': {
                    if (userDrivingLicense !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userDrivingLicense.vehicleClass);
                    }
                    break;
                }
                case 'Endorsements': {
                    if (userDrivingLicense !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userDrivingLicense.endorsements);
                    }
                    break;
                }
                case 'Donor': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.organDonor);
                    }
                    break;
                }
                case 'Veteran': {
                    if (userAttribute !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userAttribute.veteran);
                    }
                    break;
                }
                case 'Department': {
                    if (userStudentID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userStudentID.department);
                    }
                    break;
                }
                case 'Primary Subscriber': {
                    if (userHealthID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userHealthID.primarySubscriber);
                    }
                    break;
                }
                case 'Subscriber ID': {
                    if (userHealthID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userHealthID.primarySubscriberID);
                    }
                    break;
                }
                case 'Primary Doctor': {
                    if (userHealthID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userHealthID.pcp);
                    }
                    break;
                }
                case 'Primary Doctor Phone': {
                    if (userHealthID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userHealthID.pcpPhone);
                    }
                    break;
                }
                case 'Policy Number': {
                    if (userHealthID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userHealthID.policyNumber);
                    }
                    break;
                }
                case 'Group Plan': {
                    if (userHealthID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userHealthID.groupPlan);
                    }
                    break;
                }
                case 'Health Plan Number': {
                    if (userHealthID !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userHealthID.healthPlanNumber);
                    }
                    break;
                }
                case 'Vaccine': {
                    if (userHealthServiceVaccineInfos !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userHealthServiceVaccineInfos[0].vaccine);
                    }
                    break;
                }
                case 'Route': {
                    if (userHealthServiceVaccineInfos !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userHealthServiceVaccineInfos[0].route);
                    }
                    break;
                }
                case 'Site': {
                    if (userHealthServiceVaccineInfos !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userHealthServiceVaccineInfos[0].site);
                    }
                    break;
                }
                case 'Date': {
                    if (userHealthServiceVaccineInfos !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(new Date(userHealthServiceVaccineInfos[0].date));
                    }
                    break;
                }
                case 'Administered By': {
                    if (userHealthServiceVaccineInfos !== null) {
                        user.enrollForm.controls[registrationField.id].setValue(userHealthServiceVaccineInfos[0].administeredBy);
                    }
                    break;
                }
            }
        });
    }

    setVisualTemplates(userDetails: User) {
        this.enrollService.getWorkflowCredentialTemplates(this.userName).subscribe(
            async visualTemplates => {
                // Load Available Visual Templates
                this.credentialTemplates = visualTemplates;
                if (this.credentialTemplates && this.credentialTemplates.length === 1) {
                    this.selectedCredentialTemplate.setValue(this.credentialTemplates[0].id);
                    await this.getVisualCredential(userDetails);
                } else {
                    this.isLoadingResults = false;
                }
            },
            error => {
                this.isLoadingResults = false;
            }
        );
    }
    setBiometrics(user: UserEnrollmentDetails, biometrics: UserBiometric[]) {
        user.rightFingerprintImages = [];
        user.rightFingerprintImages = [];
        if (biometrics !== null) {
            biometrics.forEach(userBiometric => {
                switch (userBiometric.type) {
                    case 'FACE': {
                        user.isCropped = true;
                        user.croppedImg = `data:image/${userBiometric.format};base64,` + userBiometric.data;
                        if (userBiometric.userBiometricAttributes.length > 0) {
                            userBiometric.userBiometricAttributes.forEach(e => {
                                if (e.name === 'TRANSPARENT_PHOTO_ALLOWED') {
                                    if (e.value === 'true') {
                                        this.isTransparentPhotoAllowed = true;
                                    }
                                }
                            });
                        }
                        break;
                    }
                    case 'IRIS': {
                        if (userBiometric.position === 'LEFT_EYE') {
                            user.leftEye = userBiometric.data;
                        } else if (userBiometric.position === 'RIGHT_EYE') {
                            user.rightEye = userBiometric.data;
                        }
                        break;
                    }
                    case 'SIGNATURE': {
                        this.signature = `data:image/${userBiometric.format};base64,` + userBiometric.data;
                        break;
                    }
                    case 'FINGERPRINT': {
                        if (userBiometric.position === 'LEFT_THUMB') {
                            user.leftFingerprintImages[0] = userBiometric;
                        }
                        if (userBiometric.position === 'LEFT_INDEX') {
                            user.leftFingerprintImages[1] = userBiometric;
                        }
                        if (userBiometric.position === 'LEFT_MIDDLE') {
                            user.leftFingerprintImages[2] = userBiometric;
                        }
                        if (userBiometric.position === 'LEFT_RING') {
                            user.leftFingerprintImages[3] = userBiometric;
                        }
                        if (userBiometric.position === 'LEFT_LITTLE') {
                            user.leftFingerprintImages[4] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_THUMB') {
                            user.rightFingerprintImages[0] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_INDEX') {
                            user.rightFingerprintImages[1] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_MIDDLE') {
                            user.rightFingerprintImages[2] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_RING') {
                            user.rightFingerprintImages[3] = userBiometric;
                        }
                        if (userBiometric.position === 'RIGHT_LITTLE') {
                            user.rightFingerprintImages[4] = userBiometric;
                        }
                        break;
                    }
                }
            });
        }
    }
    onCredentialTemplateChanged(event: any) {
        const user: User = this.user;
        // user.id = null;
        user.userIDProofDocuments = null;
        user.workflowName = this.enrolledUser.workflow;
        this.getVisualCredential(user);
    }
    getVisualCredential(userDetails: User): void {
        const user: User = userDetails;
        // user.id = null;
        user.userIDProofDocuments = null;
        user.workflowName = this.enrolledUser.workflow;
        this.enrollService
            .getVisualCredentialInSummary(this.selectedCredentialTemplate.value, user, this.isTransparentPhotoAllowed)
            .subscribe(
                response => {
                    this.visualCredential = response;
                    this.isLoadingResults = false;
                },
                error => {
                    this.isLoadingResults = false;
                }
            );
    }
    hasWorkflowStep(user: UserEnrollmentDetails, workflowStep: string): boolean {
        if (!user || !user.workflowSteps) {
            return false;
        }

        if (user.workflowSteps.includes(workflowStep)) {
            return true;
        }
        return false;
    }

    editEnrollment() {
        console.log('edit enrollment called');
    }
    navigateToDashboard() {
        this.router.navigate(['/dashboard']);
    }
    navigateToSearch() {
        this.router.navigate(['/searchUser']);
    }
    goBack() {
        this.router.navigate([this.data.routeFrom]);
    }
    issueUser() {
        this.searchUserService.getUserByName(this.user.name).subscribe(user => {
            if (user !== null) {
                user.identityType = this.identityType;
                this.identityDeviceDialogService.openAddIdentityDeviceDialog(user);
            }
        });
    }
    deleteUser() {
        this.searchUserService.deleteUser(this.userName, this.isFederatedUser).subscribe(result => {
            if (result.status === 200) {
                this.translateService.get('searchUser.deletedSuccessMsg').subscribe(message => {
                    this.display = false;
                    this.isDuplicateBiometricChecked = false;
                    this.displaySuccessMsg = true;
                    window.scrollTo(0, 0);
                    this.message = message;
                });
            }
        });
    }

    openImage(imageType) {
        if (imageType === 'front') {
            this.openFrontImageBlock = true;
        } else if (imageType === 'back') {
            this.openBackImageBlock = true;
        }
        this.isAnimate = true;
    }

    closeImage() {
        this.openFrontImageBlock = false;
        this.openBackImageBlock = false;
    }
    check(attr, toolTipId): void {
        const val = document.getElementById(toolTipId);
        this.showToolTip = val.offsetWidth < val.scrollWidth ? true : false;
    }
}
export class UserEnrollmentDetails {
    id = 0;
    userId: number;
    username: string;
    faceMatchScore = 0;
    irisMatchScore = 0;
    fingersMatchScore = 0;
    workflow: string;
    enrollForm: FormGroup;
    adjudicationUserInfoFields: WFStepRegistrationConfig[] = [];
    workflowSteps: string[] = [];
    // face
    isCropped = false;
    croppedImg: any;

    // Iris
    leftEye: string;
    rightEye: string;

    // fp
    leftFingerprintImages: UserBiometric[] = [];
    rightFingerprintImages: UserBiometric[] = [];
    skippedFingers: UserBiometricSkip[] = [];
    constructor(private formBuilder: FormBuilder) {
        this.enrollForm = this.formBuilder.group({});
    }
}
