export class UserBioInfo {
    faceModality: FaceModality;
    fingerprintModalities: FingerprintModality[] = [];
    irisModalities: IRISModality[] = [];
    fingerImage: string;
    remoteServerDetails;
    deDuplicationCheck: boolean;
}
export class FingerprintModality {
    fingerInputData: string;
    fingerPosition: string;
    imageType: string;
}
export class FaceModality {
    faceImg: string;
    faceInputData: string;
}
export class IRISModality {
    irisData: string;
    irisPosition: string;
}
