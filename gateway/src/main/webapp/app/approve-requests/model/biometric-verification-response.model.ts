export class BiometricVerificationResponse {
    statusMessage: string;
    sessionId: string;
    bioDuplicatesList: BiometricDuplicateInformation[] = [];
}
export class BiometricDuplicateInformation {
    userID: number;
    bioScore: number;
    faceInfo: FaceInfo;
    fingerprintsInfo: FingerInfo[] = [];
    fingersMatchingScore: number;
    irisInfo: IrisInfo[] = [];
    irisMatchingScore: number;
}
export class FaceInfo {
    isFaceMatched: boolean;
    faceMatchedScore: number;
}
export class FingerInfo {
    isFingerMatched: boolean;
    fingerPosition: string;
    fingerMatchedScore: number;
}
export class IrisInfo {
    isIrisMatched: boolean;
    irisPosition: string;
    irisMatchedScore: number;
}
