import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Principal } from '../auth/principal.service';
import { AuthServerProvider } from '../auth/auth-session.service';
import { stringify } from '@angular/core/src/util';
import { Location } from '@angular/common';
import { Logout } from './logout.model';
import { OrganizationBrandingInfo, BuildInfo, WebAuthnPolicyDetails } from 'app/layouts/login/login.component';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { PairMobileDeviceConfig } from 'app/pair-mobile-device/pair-mobile-device.service';
import { SERVER_API_URL } from 'app/app.constants';

@Injectable({ providedIn: 'root' })
export class LoginService {
    domain: string[];
    url: string;
    msQRCodeURL: string;
    enrollmentQRCodeURL: string;
    headeroption = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    private brandingInfo = new BehaviorSubject<any>(null);
    brnadingInformation = this.brandingInfo.asObservable();
    constructor(
        private http: HttpClient,
        private locationnn: Location,
        private principal: Principal,
        private translateService: TranslateService,
        private authServerProvider: AuthServerProvider
    ) {
        this.domain = location.hostname.split('.', 1);
        this.url = window.location.origin + '/usermanagement/api/v1/getOrganizationBrandingInfo/' + this.domain;
        this.msQRCodeURL = window.location.origin + '/usermanagement/api/v1/organizations/' + this.domain[0] + '/pair-mobile-service';
    }
    changeBrandingInfo(obj: any) {
        this.brandingInfo.next(obj);
    }

    getBrandingInfo() {
        return this.brandingInfo.getValue();
    }
    loadBrandingInformation(): Observable<OrganizationBrandingInfo> {
        if (this.url.indexOf('9000') > -1) {
            this.url = this.url.replace('9000', '10001');
            // alert(this.url);
        }
        // alert(this.url);
        return this.http.get<OrganizationBrandingInfo>(`${this.url}`);
    }

    getPairMobileServiceQRCode(): Observable<PairMobileDeviceConfig> {
        if (this.msQRCodeURL.indexOf('9000') > -1) {
            this.msQRCodeURL = this.msQRCodeURL.replace('9000', '10001');
        }

        return this.http.get<PairMobileDeviceConfig>(`${this.msQRCodeURL}`);
    }

    getEnrollmentQRCode(userEmail: String): Observable<PairMobileDeviceConfig> {
        this.enrollmentQRCodeURL =
            window.location.origin + '/usermanagement/api/v1/organizations/' + this.domain[0] + '/users/enroll-qrcode';
        if (this.enrollmentQRCodeURL.indexOf('9000') > -1) {
            this.enrollmentQRCodeURL = this.enrollmentQRCodeURL.replace('9000', '10001');
        }
        let emailid = {
            emailid: userEmail
        };
        return this.http.post<PairMobileDeviceConfig>(`${this.enrollmentQRCodeURL}`, emailid, this.headeroption);
    }

    getVersionAndBuildInfo(): Observable<HttpResponse<BuildInfo>> {
        console.log(window.location.origin);
        let url = `${window.location.origin}​​​​​​​​/api/version/getBuildInfo`;
        if (url.indexOf('9000') > -1) {
            url = url.replace('9000', '10001');
        }
        console.log(url);
        return this.http.get<BuildInfo>(url, { observe: 'response' });
    }

    refreshTokenNow() {
        const refreshTokenUrl = window.location.origin + '/api/refreshAccessToken';
        // alert(refreshTokenUrl);
        return this.http.get(`${refreshTokenUrl}`);
    }

    getLoginURI() {
        let origin = location.origin;
        origin = origin.replace(':9000', ':10001');
        // alert(origin);
        // const org = location.hostname.split(".",1);
        // alert(org);
        const externalUrl = 'oauth2/authorization/' + this.domain[0];
        // alert(`${origin}${this.locationnn.prepareExternalUrl(externalUrl)}`);
        let loginUrl = `${origin}${this.locationnn.prepareExternalUrl(externalUrl)}`;
        loginUrl = loginUrl.replace('#', '');
        loginUrl = loginUrl + '?kc_locale=' + this.translateService.currentLang;
        // alert(loginUrl);
        return loginUrl;
    }

    login() {
        let origin = location.origin;
        origin = origin.replace(':9000', ':10001');
        // alert(origin);
        // const org = location.hostname.split(".",1);
        // alert(org);
        const externalUrl = 'oauth2/authorization/' + this.domain[0];
        // alert(`${origin}${this.locationnn.prepareExternalUrl(externalUrl)}`);
        let loginUrl = `${origin}${this.locationnn.prepareExternalUrl(externalUrl)}`;
        loginUrl = loginUrl.replace('#', '');
        loginUrl = loginUrl + '?kc_locale=' + this.translateService.currentLang;
        // alert(loginUrl);
        location.href = loginUrl;
    }

    logout() {
        const orgName = this.principal.getLoggedInUser().organization;
        // alert(orgName);
        // alert(this.domain[0]);
        this.authServerProvider.logout(this.domain[0]).subscribe((logout: Logout) => {
            let logoutUrl = logout.logoutUrl;
            const redirectUri = `${location.origin}${this.locationnn.prepareExternalUrl('/')}`;
            if (logoutUrl !== undefined && logoutUrl !== null) {
                // if Keycloak, uri has protocol/openid-connect/token
                if (logoutUrl.includes('/protocol')) {
                    logoutUrl = logoutUrl + '?redirect_uri=' + redirectUri + '&kc_locale=' + this.translateService.currentLang;
                } else {
                    // Okta
                    logoutUrl = logoutUrl + '?id_token_hint=' + logout.idToken + '&post_logout_redirect_uri=' + redirectUri;
                }
                // window.location.href = logoutUrl;
                // alert(logoutUrl);
            }
        });
        this.principal.authenticate(null);
        // location.href = `${location.origin}`.replace('10001', '9000');
        // this.login();
        // location.href = 'http://localhost:9080/auth/realms/meetidentity/protocol/openid-connect/logout?redirect_uri=http://localhost:10001/';
    }
}
