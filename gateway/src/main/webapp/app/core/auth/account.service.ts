import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { UserInfo } from './user-info.model';

@Injectable({ providedIn: 'root' })
export class AccountService {
    constructor(private http: HttpClient) {}

    get(): Observable<HttpResponse<UserInfo>> {
        // alert('get');
        return this.http.get<UserInfo>(SERVER_API_URL + 'api/account', { observe: 'response' });
    }

    refreshTokenNow() {
        // alert('get');
        return this.http.get(SERVER_API_URL + 'api/refreshAccessToken');
    }

    save(account: any): Observable<HttpResponse<any>> {
        return this.http.post(SERVER_API_URL + 'api/account', account, { observe: 'response' });
    }
}
