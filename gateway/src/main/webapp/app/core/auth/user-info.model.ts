import { UserBiometric } from 'app/enroll/userbiometric.model';
import { Group } from 'app/workflow/group.model';

export class UserInfo {
    sub: string;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    exp: Date;
    organization: string;
    workflow: string;
    authorities: string[];
    roleBasedAuthorities: any;
    workflowSteps: string[];
    roles: string[];
    roleWithColor: string[];
    groups: Group[];
    authoritiesServicemode: number[];
    isApplicantNow = false;
    hasUserRoleWithOtherRoles = false;
    userId: number;
    orgId: number;
    isFederated: boolean;
    status: string;
    userStatus: string;
    canEnroll: boolean;
    canIssue: boolean;
    canLifeCycle: boolean;
    canDisplayEnrollDetails: boolean;
    canApprove: boolean;
    onboardSelfService: String;
    userBiometrics: Array<UserBiometric>;
    isDeviceClientSecurityEnabled: boolean;
    identityType: string;
    constructor() {}
}
