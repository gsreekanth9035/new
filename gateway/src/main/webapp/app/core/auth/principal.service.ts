import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AccountService } from './account.service';
import { UserInfo } from './user-info.model';
import { Group } from 'app/workflow/group.model';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class Principal {
    private userIdentity: UserInfo;
    private authenticated = false;
    private authenticationState = new Subject<any>();

    constructor(private account: AccountService, private router: Router) {}

    authenticate(identity) {
        this.userIdentity = identity;
        this.authenticated = identity !== null;
        this.authenticationState.next(this.userIdentity);
    }

    hasAnyAuthority(authorities: string[]): Promise<boolean> {
        return Promise.resolve(this.hasAnyAuthorityDirect(authorities));
    }

    hasAnyAuthorityDirect(authorities: string[]): boolean {
        if (!this.authenticated || !this.userIdentity || !this.userIdentity.authorities) {
            return false;
        }

        if (authorities[0] === 'DEFAULT') {
            return true;
        }

        for (let i = 0; i < authorities.length; i++) {
            if (this.userIdentity.authorities.includes(authorities[i])) {
                return true;
            }
        }

        return false;
    }

    hasAuthority(authority: string): Promise<boolean> {
        if (!this.authenticated) {
            return Promise.resolve(false);
        }

        return this.identity().then(
            id => {
                return Promise.resolve(id.authorities && id.authorities.includes(authority));
            },
            () => {
                return Promise.resolve(false);
            }
        );
    }

    hasAnyGroup(groups: Group[]): boolean {
        if (!this.authenticated || !this.userIdentity || !this.userIdentity.groups) {
            return false;
        }
        let hasGroup = false;
        groups.forEach(group => {
            this.userIdentity.groups.forEach(loginUserGroup => {
                if (loginUserGroup.id === group.id) {
                    hasGroup = true;
                }
            });
        });
        return hasGroup;
    }

    identity(force?: boolean): Promise<any> {
        if (force === true) {
            this.userIdentity = undefined;
        }

        // check and see if we have retrieved the userIdentity data from the server.
        // if we have, reuse it by immediately resolving
        if (this.userIdentity) {
            return Promise.resolve(this.userIdentity);
        }

        // retrieve the userIdentity data from the server, update the identity object, and then resolve.
        return this.account
            .get()
            .toPromise()
            .then(response => {
                const user: UserInfo = response.body;
                if (user) {
                    this.userIdentity = user;
                    console.log(this.userIdentity.exp);
                    this.authenticated = true;
                } else {
                    this.userIdentity = null;
                    this.authenticated = false;
                }
                const lowerCaseRoles: string[] = [];
                user.roles.forEach(role => {
                    lowerCaseRoles.push(role.toLowerCase());
                });
                user.roles = [];
                user.roles = lowerCaseRoles;
                this.authenticationState.next(this.userIdentity);
                this.router.navigate(['/dashboard']);
                return this.userIdentity;
            })
            .catch(err => {
                this.userIdentity = null;
                this.authenticated = false;
                this.authenticationState.next(this.userIdentity);
                return null;
            });
    }

    reIdentity(force?: boolean): Promise<any> {
        // retrieve the userIdentity data from the server, update the identity object, and then resolve.
        return this.account
            .get()
            .toPromise()
            .then(response => {
                const user: UserInfo = response.body;
                if (user) {
                    this.userIdentity = user;
                    console.log('22222' + this.userIdentity.exp);
                    this.authenticated = true;
                } else {
                    this.userIdentity = null;
                    this.authenticated = false;
                }
                const lowerCaseRoles: string[] = [];
                user.roles.forEach(role => {
                    lowerCaseRoles.push(role.toLowerCase());
                });
                user.roles = [];
                user.roles = lowerCaseRoles;
                this.authenticationState.next(this.userIdentity);
                // this.router.navigate(['/dashboard']);
                return this.userIdentity;
            })
            .catch(err => {
                this.userIdentity = null;
                this.authenticated = false;
                this.authenticationState.next(this.userIdentity);
                return null;
            });
    }

    getLoggedInUser(): UserInfo {
        return this.userIdentity;
    }

    isAuthenticated(): boolean {
        return this.authenticated;
    }

    isIdentityResolved(): boolean {
        return this.userIdentity !== undefined;
    }

    getAuthenticationState(): Observable<any> {
        return this.authenticationState.asObservable();
    }
}
