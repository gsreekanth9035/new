import { Injectable, RendererFactory2, Renderer2 } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { LANGUAGES } from 'app/core/language/language.constants';
import { LoginService } from '../login/login.service';
import { OrganizationBrandingInfo } from 'app/layouts/login/login.component';
import { ConnectorService } from 'app/connector.service';

@Injectable({ providedIn: 'root' })
export class JhiLanguageHelper {
    renderer: Renderer2 = null;
    private _language: BehaviorSubject<string>;
    organizationBrandingInfo: OrganizationBrandingInfo;
    titlestring: string;
    constructor(
        private loginService: LoginService,
        private translateService: TranslateService,
        // tslint:disable-next-line: no-unused-variable
        private rootRenderer: RendererFactory2,
        private titleService: Title,
        private router: Router,
        private connectorService: ConnectorService
    ) {
        // this._language = new BehaviorSubject<string>(this.translateService.currentLang);
        // this.renderer = rootRenderer.createRenderer(document.querySelector('html'), null);
        // this.init();
        if (localStorage.getItem('selectedLanguage') === null) {
            localStorage.setItem('selectedLanguage', 'en');
            this.translateService.use('en');
            this.connectorService.setLocale('en');
        } else {
            this.translateService.use(localStorage.getItem('selectedLanguage'));
            this.connectorService.setLocale(localStorage.getItem('selectedLanguage'));
        }
        // this.translateService.currentLang = localStorage.getItem('selectedLanguage');
        //     this.translateService.use(localStorage.getItem('selectedLanguage'));
        // } else {
        //     this.translateService.currentLang = 'en';
        //     localStorage.setItem('selectedLanguage', this.translateService.currentLang);
        //     this.translateService.use(this.translateService.currentLang);
        // }
        // alert('inside constructor: ' + this.translateService.currentLang);
        // alert('inside localStorage: ' + localStorage.getItem('selectedLanguage'));
        this._language = new BehaviorSubject<string>(this.translateService.currentLang);
        this.renderer = rootRenderer.createRenderer(document.querySelector('html'), null);
        this.init();
    }

    getAll(): Promise<any> {
        return Promise.resolve(LANGUAGES);
    }

    get language(): Observable<string> {
        return this._language.asObservable();
    }

    /**
     * Update the window title using params in the following
     * order:
     * 1. titleKey parameter
     * 2. $state.$current.data.pageTitle (current state page title)
     * 3. 'global.title'
     */
    updateTitle(titleKey?: string) {
        if (!titleKey) {
            titleKey = this.getPageTitle(this.router.routerState.snapshot.root);
        }

        this.translateService.get(titleKey).subscribe(title => {
            this.titleService.setTitle(title);
            this.titlestring = title;
            if (this.titlestring.includes(':')) {
                this.loginService.loadBrandingInformation().subscribe(
                    brandingInformation => {
                        this.organizationBrandingInfo = brandingInformation;
                        // alert(this.organizationBrandingInfo.loginLogo);
                        // this.jhiLanguageHelper.updateTitle(this.getPageTitle(this.router.routerState.snapshot.root));
                        this.titlestring = this.titlestring.replace(':', this.organizationBrandingInfo.loginPageTitle);
                        this.titleService.setTitle(this.titlestring);
                    },
                    err => {
                        // alert('error');
                    }
                );
            } else {
                this.titleService.setTitle(this.titlestring);
            }
        });
    }

    public updateLanguage() {
        this.init();
    }

    private init() {
        this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
            // alert('event' + event.lang);
            // alert('inside initi1: ' + this.translateService.currentLang);
            // alert('inside localStorage: ' + localStorage.getItem('selectedLanguage'));
            this.translateService.currentLang = localStorage.getItem('selectedLanguage');
            // alert('inside initi2: ' + this.translateService.currentLang);
            // localStorage.setItem('selectedLanguage', this.translateService.currentLang);
            // this._language.next(localStorage.getItem('selectedLanguage'));
            // this.translateService.use(localStorage.getItem('selectedLanguage'));
            // this.renderer.setAttribute(document.querySelector('html'), 'lang', localStorage.getItem('selectedLanguage'));
            // this.updateTitle(this.getPageTitle(this.router.routerState.snapshot.root));

            this._language.next(localStorage.getItem('selectedLanguage'));
            this.renderer.setAttribute(document.querySelector('html'), 'lang', localStorage.getItem('selectedLanguage'));
            this.updateTitle(this.getPageTitle(this.router.routerState.snapshot.root));
        });
    }

    private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
        let title: string = routeSnapshot.data && routeSnapshot.data['pageTitle'] ? routeSnapshot.data['pageTitle'] : 'gatewayApp';
        if (routeSnapshot.firstChild) {
            title = this.getPageTitle(routeSnapshot.firstChild) || title;
        }
        return title;
    }
}
