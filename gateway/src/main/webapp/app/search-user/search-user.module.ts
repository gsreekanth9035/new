import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from './../material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SEARCH_USER_ROUTES } from './search-user.route';
import { MobileDevicesDialogComponent, SearchUserComponent } from './search-user.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild(SEARCH_USER_ROUTES),
        CommonModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MaterialModule,
        GatewaySharedModule
    ],
    entryComponents: [MobileDevicesDialogComponent],
    declarations: [SearchUserComponent, MobileDevicesDialogComponent]
})
export class SearchUserModule {}
