import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Principal } from 'app/core';
import { Page } from 'app/visual-design/page.model';
import { Role } from 'app/workflow/role.model';
import { User } from 'app/enroll/user.model';
import { st } from '@angular/core/src/render3';

@Injectable({
    providedIn: 'root'
})
export class SearchUserService {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name,
        loggedInUserId: '' + this.principal.getLoggedInUser().userId

        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });
    updateUserLocationStatus_bulk(markedUsers: Array<string>, selectedValue: number) {
        this.httpHeaders.append('Content-type', 'application/json');
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.post(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/updateUsersLocationStatus/${selectedValue}`,
            markedUsers,
            {
                headers: this.httpHeaders,
                observe: 'response'
            }
        );
    }
    updateUserLocationStatus(selectedValue: number) {
        const orgName = this.principal.getLoggedInUser().organization;
        // return this.httpClient.delete(`${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/${userName}`, {
        //     observe: 'response'
        // });
    }
    constructor(private httpClient: HttpClient, private principal: Principal) {}

    searchUsers(searchCriteria: string, page: number, size: number): Observable<Page> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<Page>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users?
searchCriteria=${searchCriteria}&page=${page}&size=${size}`
        );
    }

    searchUsersByLocation(searchCriteria: string, page: number, size: number): Observable<Page> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<Page>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/searchByLocation?
searchCriteria=${searchCriteria}&page=${page}&size=${size}`
        );
    }

    groupMembers(searchCriteria: string, groupID: string, page: number, size: number): Observable<Page> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<Page>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/groups?groupID=${groupID}&
searchCriteria=${searchCriteria}&page=${page}&size=${size}`
        );
    }

    deleteUser(userName: string, isFederatedUser: boolean): Observable<any> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.delete(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/${userName}?isFederated=${isFederatedUser}`,
            {
                observe: 'response'
            }
        );
    }

    deleteUserAudit(userName: string) {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.delete(`${window.location.origin}/api/organizations/${orgName}/users/${userName}`, {
            observe: 'response'
        });
    }

    changeUserAccess(userName: string, status: string): Observable<any> {
        const orgName = this.principal.getLoggedInUser().organization;
        console.log(`${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/${userName}/access/${status}`);
        return this.httpClient.get<any>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/${userName}/access/${status}`,
            { headers: this.httpHeaders, observe: 'response' }
        );
    }

    getUserByName(userName: string): Observable<User> {
        return this.httpClient.get<User>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/users/${userName}`
        );
    }
    sendNotificationToMIWalletAppForPairMobileDevice(userName: String, appName: string): Observable<any> {
        return this.httpClient.get<User>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/users/${userName}/pairDevice/send-notification?appName=${appName}`,
            { headers: this.httpHeaders, observe: 'response' }
        );
    }
}

export class SearchUserResult {
    checked: boolean;
    userID: number;
    sub: string;
    userName: string;
    firstName: string;
    lastName: string;
    email: string;
    enabled: boolean;
    canEnroll: boolean;
    canIssue: boolean;
    canLifeCycle: boolean;
    canDisplayEnrollDetails: boolean;
    canApprove: boolean;
    mobileIDUser: boolean;
    admin: boolean;
    operator: boolean;
    applicant: boolean;
    adjudicator: boolean;
    groupId: string;
    groupName: string;
    workflowName: string;
    roles: Role[];
    roleIDs: string[];
    identityTypeName: string;
    location: string;
    date: string;
    status: number;
    isEnrollmentInProgress: boolean;
    onboardSelfService: string;
    isFederated: boolean;
}
