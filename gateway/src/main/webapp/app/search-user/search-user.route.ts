import { SearchUserComponent } from './search-user.component';
import { Route, Routes } from '@angular/router';

export const SEARCH_USER_ROUTES: Routes = [
    {
        path: 'searchUser',
        component: SearchUserComponent,
        data: {
            authorities: [],
            pageTitle: 'searchUser.pageTitle'
        }
    }
];
