import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchUserService, SearchUserResult } from './search-user.service';
import { DataService, Data } from 'app/device-profile/data.service';
import { MatSort, MatPaginator, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { merge, of as observableOf } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { Principal } from 'app/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { DialogDataResponse } from 'app/manage-identity-devices/manage-identity-devices.component';
import { AddIdentityDeviceDialogService } from 'app/manage-identity-devices/add-identity-device-dialog.service';
import { SharedService } from 'app/shared/util/shared-service';
import { DeviceProfileConfig } from 'app/device-profile/device-profile.service';
import { DeviceProfile } from 'app/device-profile/device-profile.model';
import { OnboardingUserService } from 'app/onboarding-user/onboarding-user.service';
import { Role } from 'app/workflow/role.model';
import { IdentityProviderUser } from 'app/onboarding-user/onboarding-user.component';
declare var $: any;
@Component({
    selector: 'jhi-search-user',
    templateUrl: './search-user.component.html',
    styleUrls: ['search-user.scss']
})
export class SearchUserComponent implements OnInit {
    searchUserResults: SearchUserResult[] = [];
    displayedColumns: string[] = [
        // 'firstName',
        //  'lastName',
        'name',
        'userName',
        'role',
        //  'email',
        'enabled',
        'status',
        'group',
        // 'workflow',

        'identityType',
        'actions'
    ];
    displayedColumns_location: string[] = [
        'checkbox',
        'userName',
        'firstName',
        'lastName',
        'email',
        'location',
        'date',
        'status',
        'identityType',
        'actions'
    ];
    authorities: string[] = [];
    searchUserForm: FormGroup;
    searchCriteria: any;
    userType_isAdmin: boolean;
    data: SearchUserResult[] = [];
    dataWithLocation: SearchUserResult[] = [];
    userName: string;
    userToBeDeleted = new SearchUserResult();
    success = false;
    message: string;
    selectedUserName: string;
    activationTypes: ConfigValue[] = [
        { id: 0, value: 'Send Alert and Mark as Disease Free' },
        { id: 1, value: 'Send Alert and Mark for Stay Home' },
        { id: 2, value: 'Send Alert and Mark for Quarantine' },
        { id: 3, value: 'Send Alert and Mark for Unrestricted' }
    ];

    statusTypes: ConfigValue[] = [
        { id: 0, value: 'Disease Free' },
        { id: 1, value: 'Staying at Home' },
        { id: 2, value: 'Quarantined' },
        { id: 3, value: 'Unrestricted' }
    ];

    selectedValue = 0;
    resultsLength = 0;
    pageSizeOptions = [5, 10, 15, 20];
    parentPageSizeOptions = [5, 10, 15, 20];
    pageSize = 10;
    isLoadingResults = true;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    userFirstLastName_tbd: any;
    locationmarked = false;
    marked = false;
    selectAllUsers = false;
    markedUsers: Array<string>;
    isDisabled: boolean;
    theCheckbox = false;
    isChecked = new FormControl(false);
    isEnrollmentEnabled = false;
    isIssuanceEnabled = false;
    isLifeCycleEnabled = false;
    isOnboardingEnabled = false;
    isApprovalEnabled = false;
    isPairMobileDevicePermissionEnabled = false;
    isUserDeletePermissionEnabled = false;
    searchBy = 'searchUser.searchByNameOrEmail';
    disableSearchButton = false;
    userRoles: Role[];
    showToolTip = false;
    selectedFoods = ['ROLE_USER', 'ROLE_OPERATOR'];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private searchUserService: SearchUserService,
        private principal: Principal,
        private formBuilder: FormBuilder,
        private dataService: DataService,
        public dialog: MatDialog,
        private translateService: TranslateService,
        private identityDeviceDialogService: AddIdentityDeviceDialogService,
        private sharedService: SharedService,
        private onboardingUserService: OnboardingUserService
    ) {
        // This enables ngOnInit to be called every time, if the route is same, it always creates new component
        this.router.routeReuseStrategy.shouldReuseRoute = function() {
            return false;
        };
    }

    ngOnInit() {
        this.userName = this.principal.getLoggedInUser().username;
        this.searchUserForm = this.formBuilder.group({
            searchCriteria: ['']
        });

        if (this.principal.getLoggedInUser().roles.includes('role_admin')) {
            this.userType_isAdmin = true;
        } else if (this.principal.getLoggedInUser().roles.includes('role_operator')) {
            this.userType_isAdmin = false;
        }
        if (this.principal.getLoggedInUser().authorities.includes('Menu_Enrollment')) {
            this.isEnrollmentEnabled = true;
        }
        if (this.principal.getLoggedInUser().authorities.includes('Menu_Issuance')) {
            this.isIssuanceEnabled = true;
        }
        if (this.principal.getLoggedInUser().authorities.includes('Menu_Identity_Devices')) {
            this.isLifeCycleEnabled = true;
        }
        if (this.principal.getLoggedInUser().authorities.includes('Menu_OnboardUsers')) {
            this.isOnboardingEnabled = true;
        }
        if (this.principal.getLoggedInUser().authorities.includes('Menu_Approve_Requests')) {
            this.isApprovalEnabled = true;
        }
        if (this.principal.getLoggedInUser().authorities.includes('Menu_Pair_Mobile_Device')) {
            this.isPairMobileDevicePermissionEnabled = true;
        }
        if (this.principal.getLoggedInUser().authorities.includes('Menu_Delete_Users')) {
            this.isUserDeletePermissionEnabled = true;
        }
        this.onboardingUserService.getRolesFromAuthService().subscribe(roles => {
            this.userRoles = roles;
        });
        this.getUsers();
        this.isDisabled = false;
    }

    getUsers() {
        if (this.locationmarked) {
            // const searchCriteria = this.searchUserForm.get('searchCriteria').value;
            if (true) {
                merge(this.paginator.page)
                    .pipe(
                        startWith({}),
                        switchMap(() => {
                            this.isLoadingResults = true;
                            if (this.paginator.pageSize === undefined) {
                                this.paginator.pageSize = this.pageSize;
                            }
                            // if (this.userType_isAdmin) {
                            return this.searchUserService.searchUsersByLocation(
                                this.searchUserForm.get('searchCriteria').value,
                                this.paginator.pageIndex,
                                this.paginator.pageSize
                            );
                            // } else if (!this.userType_isAdmin) {
                            //     return this.searchUserService.groupMembers(
                            //         searchCriteria,
                            //         group[0].id,
                            //         this.paginator.pageIndex,
                            //         this.paginator.pageSize
                            //     );
                            // }
                        }),
                        map(dataWithLocation => {
                            this.resultsLength = dataWithLocation.totalElements;
                            if (this.resultsLength > 0) {
                                this.isDisabled = false;
                            } else {
                                this.isDisabled = true;
                            }
                            this.isLoadingResults = false;
                            if (dataWithLocation.totalElements > 0) {
                                this.pageSizeOptions = this.sharedService.getPageSizeOptionsBasedOnTotalElements(
                                    dataWithLocation.totalElements,
                                    this.parentPageSizeOptions
                                );
                            }
                            return dataWithLocation.content;
                        }),
                        catchError(() => {
                            this.isLoadingResults = false;
                            return observableOf([]);
                        })
                    )
                    .subscribe(dataWithLocation => (this.dataWithLocation = dataWithLocation));
            }
        } else {
            const group = this.principal.getLoggedInUser().groups;
            const roles = this.principal.getLoggedInUser().roles;
            merge(this.paginator.page)
                .pipe(
                    startWith({}),
                    switchMap(() => {
                        this.isLoadingResults = true;
                        if (this.paginator.pageSize === undefined) {
                            this.paginator.pageSize = this.pageSize;
                        }
                        if (this.paginator.page.observers.length > 1) {
                            for (let i = 0; i <= this.paginator.page.observers.length - 1; i++) {
                                this.paginator.page.observers[i].complete();
                            }
                        }
                        const searchCriteria = this.searchUserForm.get('searchCriteria').value;
                        // if (this.userType_isAdmin) {
                        return this.searchUserService.searchUsers(searchCriteria, this.paginator.pageIndex, this.paginator.pageSize);
                        // } else if (!this.userType_isAdmin) {
                        //     return this.searchUserService.groupMembers(
                        //         searchCriteria,
                        //         group[0].id,
                        //         this.paginator.pageIndex,
                        //         this.paginator.pageSize
                        //     );
                        // }
                    }),
                    map(data => {
                        this.resultsLength = data.totalElements;
                        this.isLoadingResults = false;
                        if (data.totalElements > 0) {
                            this.pageSizeOptions = this.sharedService.getPageSizeOptionsBasedOnTotalElements(
                                data.totalElements,
                                this.parentPageSizeOptions
                            );
                        }
                        return data.content;
                    }),
                    catchError(() => {
                        this.isLoadingResults = false;
                        return observableOf([]);
                    })
                )
                .subscribe(data => (this.data = data));
        }
    }

    // searchUser() {
    //     this.router.navigate(['/searchUser', { searchCriteria: this.searchUserForm.get('searchCriteria').value }]);
    //     this.searchUserForm = this.formBuilder.group({
    //         searchCriteria: ['']
    //     });
    // }

    // searchUser() {
    //     this.searchCriteria = this.searchUserForm.get('searchCriteria').value;
    //     // this.router.navigate(['/searchUser', { searchCriteria:  this.searchCriteria }]);
    //     this.searchUserService.searchUsers(this.searchCriteria).subscribe(searchResults => {
    //              this.searchUserResults = searchResults;
    //     });
    //     // this.searchUserForm.reset();
    // }

    goHome() {
        this.router.navigate(['']);
    }
    onBoardUsers() {
        this.dataService.changeObj(null);
        this.router.navigate(['/onboard']);
    }

    enrollUser(userResult: SearchUserResult) {
        // $(document.getElementById('usermenu')).css('display', 'block');
        const data = new Data();
        data.userFirstName = userResult.firstName;
        data.userId = userResult.userID;
        data.userName = userResult.userName;
        data.userLastName = userResult.lastName;
        data.userEmail = userResult.email;
        data.groupId = userResult.groupId;
        data.groupName = userResult.groupName;
        data.isFederated = userResult.isFederated;
        data.isEnrollmentInProgress = userResult.isEnrollmentInProgress;
        data.identityType = userResult.identityTypeName;
        this.dataService.changeObj(data);
        this.router.navigate(['/register']);
    }
    editEnrollment(userResult: SearchUserResult) {
        const data = new Data();
        data.userFirstName = userResult.firstName;
        data.userId = userResult.userID;
        data.userName = userResult.userName;
        data.userLastName = userResult.lastName;
        data.userEmail = userResult.email;
        data.groupId = userResult.groupId;
        data.groupName = userResult.groupName;
        data.isFederated = userResult.isFederated;
        data.isEnrollmentInProgress = userResult.isEnrollmentInProgress;
        // data.isEnrolledUser = true;
        data.identityType = userResult.identityTypeName;
        this.dataService.changeObj(data);
        this.router.navigate(['/register']);
    }

    enrollUserWithMobileID(userResult: SearchUserResult) {
        this.isLoadingResults = true;
        this.disableSearchButton = true;
        let isOnboardEnabled = false;
        if (this.isOnboardingEnabled) {
            isOnboardEnabled = true;
            this.isOnboardingEnabled = false;
        }
        this.searchUserForm.disable();

        if (this.dialog.openDialogs.length === 0) {
            this.searchUserService.getUserByName(userResult.userName).subscribe(
                async user => {
                    if (user !== null) {
                        const wfDevProfs: DeviceProfile[] = user.wfDeviceProfiles.filter(e => e.deviceProfileConfig.isMobileId);
                        user.wfDeviceProfiles = wfDevProfs;
                        if (userResult.operator || userResult.admin) {
                            const devProf = new DeviceProfile();
                            const devProfConfig = new DeviceProfileConfig();
                            devProf.name = 'Mobile Service';
                            devProfConfig.isMobileService = true;
                            devProf.deviceProfileConfig = devProfConfig;
                            wfDevProfs.push(devProf);
                        }

                        let msg = await (<any>this.translateService.get('searchUser.pairMobileDeviceInvite').toPromise());
                        // msg = 'Invite to Pair Mobile Device';
                        const dialogRef = this.dialog.open(MobileDevicesDialogComponent, {
                            autoFocus: false,
                            width: '400px',
                            data: {
                                headerText: msg,
                                user: user
                            }
                        });
                        dialogRef.afterClosed().subscribe(
                            result => {
                                console.log(result);
                                if (result !== undefined && result !== null) {
                                    this.isLoadingResults = true;
                                    if (result.deviceProfileConfig.isMobileId) {
                                        this.searchUserService
                                            .sendNotificationToMIWalletAppForPairMobileDevice(userResult.userName, 'mobile_wallet')
                                            .subscribe(
                                                res => {
                                                    console.log('response::::::', res);
                                                    if (res.status === 200) {
                                                        // display a message in ui
                                                        console.log('notification snet successfully');
                                                        this.isLoadingResults = false;

                                                        this.translateService
                                                            .get('searchUser.notificationSentToApp', { appName: 'Mobile Wallet' })
                                                            .subscribe(
                                                                message => {
                                                                    this.success = true;
                                                                    this.message = message;
                                                                    window.scrollTo(0, 0);
                                                                    setTimeout(() => {
                                                                        this.success = false;
                                                                    }, 15000);
                                                                },
                                                                err => {
                                                                    this.isLoadingResults = false;
                                                                }
                                                            );
                                                    } else {
                                                        // handle if any exceptions
                                                        this.isLoadingResults = false;
                                                    }
                                                },
                                                err => {
                                                    this.isLoadingResults = false;
                                                }
                                            );
                                    } else if (result.deviceProfileConfig.isMobileService) {
                                        this.searchUserService
                                            .sendNotificationToMIWalletAppForPairMobileDevice(userResult.userName, 'mobile_service')
                                            .subscribe(
                                                res => {
                                                    console.log('response::::::', res);
                                                    if (res.status === 200) {
                                                        // display a message in ui
                                                        console.log('notification snet successfully');
                                                        this.translateService
                                                            .get('searchUser.notificationSentToApp', { appName: 'Mobile Services' })
                                                            .subscribe(
                                                                message => {
                                                                    this.success = true;
                                                                    this.message = message;
                                                                    window.scrollTo(0, 0);
                                                                    setTimeout(() => {
                                                                        this.success = false;
                                                                    }, 15000);
                                                                },
                                                                err => {
                                                                    this.isLoadingResults = false;
                                                                }
                                                            );
                                                        this.isLoadingResults = false;
                                                    } else {
                                                        // handle if any exceptions
                                                        this.isLoadingResults = false;
                                                    }
                                                },
                                                err => {
                                                    this.isLoadingResults = false;
                                                }
                                            );
                                    }
                                }
                            },
                            err => {}
                        );
                        this.searchUserForm.enable();
                        this.disableSearchButton = false;
                        this.isLoadingResults = false;
                        if (isOnboardEnabled) {
                            this.isOnboardingEnabled = true;
                        }
                    }
                },
                err => {
                    this.disableSearchButton = false;
                    this.searchUserForm.enable();
                    if (isOnboardEnabled) {
                        this.isOnboardingEnabled = true;
                    }
                    this.isLoadingResults = false;
                }
            );
        }
        // const data = new Data();
        // data.userFirstName = userResult.firstName;
        // data.userId = userResult.userID;
        // data.userName = userResult.userName;
        // data.userLastName = userResult.lastName;
        // data.onboardSelfService = 'true';
        // data.userEmail = userResult.email;
        // data.groupId = userResult.groupId;
        // data.groupName = userResult.groupName;
        // data.isFederated = userResult.isFederated;
        // this.dataService.changeObj(data);
        // this.router.navigate(['/pairmobile']);
    }

    displayEnrollmentDetails(userResult: SearchUserResult) {
        const data = new Data();
        data.userFirstName = userResult.firstName;
        data.userName = userResult.userName;
        data.userLastName = userResult.lastName;
        data.groupId = userResult.groupId;
        data.groupName = userResult.groupName;
        data.isEnrolledUser = true;
        data.canEditUserDetails = userResult.canIssue && this.isEnrollmentEnabled;
        data.routeFrom = this.router.url;
        data.isFederated = userResult.isFederated;
        data.identityType = userResult.identityTypeName;
        this.dataService.changeObj(data);
        this.router.navigate(['/enrollmentDetails']);
    }
    approveEnrollmentDetails(userResult: SearchUserResult) {
        const data = new Data();
        data.userFirstName = userResult.firstName;
        data.userName = userResult.userName;
        data.userLastName = userResult.lastName;
        data.groupId = userResult.groupId;
        data.groupName = userResult.groupName;
        data.isEnrolledUser = true;
        data.routeFrom = this.router.url;
        data.isFederated = userResult.isFederated;
        data.identityType = userResult.identityTypeName;
        this.dataService.changeObj(data);
        this.router.navigate(['/approve-requests']);
    }
    issueUser(userResult: SearchUserResult) {
        this.isLoadingResults = true;
        this.disableSearchButton = true;
        let isOnboardEnabled = false;
        if (this.isOnboardingEnabled) {
            isOnboardEnabled = true;
            this.isOnboardingEnabled = false;
        }
        this.searchUserForm.disable();
        this.searchUserService.getUserByName(userResult.userName).subscribe(
            user => {
                if (user !== null) {
                    user.identityType = userResult.identityTypeName;
                    this.identityDeviceDialogService.openAddIdentityDeviceDialog(user);
                }
                this.searchUserForm.enable();
                this.disableSearchButton = false;
                this.isLoadingResults = false;
                if (isOnboardEnabled) {
                    this.isOnboardingEnabled = true;
                }
            },
            err => {
                this.disableSearchButton = false;
                this.searchUserForm.enable();
                if (isOnboardEnabled) {
                    this.isOnboardingEnabled = true;
                }
                this.isLoadingResults = false;
            }
        );
    }

    editOnboardUserInfo(userResult: SearchUserResult) {
        const data = new Data();
        data.userFirstName = userResult.firstName;
        data.userLastName = '';
        data.userName = userResult.userName;
        data.isEnrolledUser = true;
        data.routeFrom = this.router.url;
        data.identityType = userResult.identityTypeName;
        data.groupId = userResult.groupId;
        data.groupName = userResult.groupName;
        this.dataService.changeObj(data);
        this.router.navigate(['/onboard']);
    }

    editUser(userResult: SearchUserResult) {
        const data = new Data();
        data.userFirstName = userResult.firstName;
        data.userLastName = '';
        data.userName = userResult.userName;
        data.isEnrolledUser = true;
        this.dataService.changeObj(data);
        this.router.navigate(['/register']);
    }

    manageDevicesOfUser(userResult: SearchUserResult) {
        const data = new Data();
        data.userFirstName = userResult.firstName;
        data.userName = userResult.userName;
        data.userLastName = userResult.lastName;
        data.groupId = userResult.groupId;
        data.groupName = userResult.groupName;
        data.isEnrolledUser = true;
        data.routeFrom = this.router.url;
        data.identityType = userResult.identityTypeName;
        data.userKcId = userResult.sub;
        this.dataService.changeObj(data);
        this.router.navigate(['/identityDevices/manageIdentityDevices']);
    }
    setTBDeletedUser(userResult: SearchUserResult) {
        this.userToBeDeleted = userResult;
        this.userFirstLastName_tbd = userResult.firstName + ' ' + userResult.lastName;
    }
    deleteUser() {
        this.isLoadingResults = true;
        this.searchUserService.deleteUser(this.userToBeDeleted.userName, this.userToBeDeleted.isFederated).subscribe(
            result => {
                if (result.status === 200) {
                    this.translateService.get('searchUser.deletedSuccessMsg').subscribe(
                        message => {
                            this.getUsers();
                            this.success = true;
                            this.message = message + ' : ' + this.userFirstLastName_tbd;
                            window.scrollTo(0, 0);
                            setTimeout(() => {
                                this.success = false;
                            }, 15000);
                        },
                        err => {
                            this.isLoadingResults = false;
                        }
                    );
                    this.searchUserService.deleteUserAudit(this.userToBeDeleted.userName).subscribe(resp => {
                        if (resp.status === 200) {
                            // audit trail deleted
                            this.userToBeDeleted = null;
                        }
                    });
                } else {
                    // handle error messages
                }
            },
            err => {
                this.isLoadingResults = false;
            }
        );
    }

    toggleVisibility(e) {
        this.locationmarked = e.target.checked;
        this.searchUserForm.reset();
        if (this.locationmarked) {
            this.searchBy = 'searchUser.searchByLocation';
            this.dataWithLocation = [];
        } else {
            this.searchBy = 'searchUser.searchByNameOrEmail';
        }
    }
    setSelectedUser(event) {
        this.marked = event.target.checked;
        // alert(this.marked);
        this.markedUsers = [];
        this.dataWithLocation.forEach(element => {
            // alert(element.checked);
            if (element.checked === true) {
                this.markedUsers.push('' + element.userID);
            }
        });
    }
    selectAllUserIDs(e) {
        this.selectAllUsers = e.target.checked;
        if (this.selectAllUsers) {
            $('#locationSearch').modal('show');
            this.markedUsers = [];
            this.dataWithLocation.forEach(element => {
                element.checked = true;
                // alert(element.userID);
                this.markedUsers.push('' + element.userID);
            });
        } else {
            this.markedUsers = [];
            this.dataWithLocation.forEach(element => {
                element.checked = false;
            });
        }
    }
    updateUserLocationStatus_bulk() {
        this.markedUsers.forEach(element => {
            // alert ( element );
        });

        this.searchUserService.updateUserLocationStatus_bulk(this.markedUsers, this.selectedValue).subscribe(result => {
            if (result.status === 200) {
                this.getUsers();
                this.isChecked.setValue(false);
            }
        });
    }
    changeUserAccess(event, username: string) {
        // console.log(event.target.hasAttribute("checked"));
        this.isLoadingResults = true;
        if (event.target.hasAttribute('checked')) {
            this.searchUserService.changeUserAccess(username, 'disabled').subscribe(
                result => {
                    document.getElementById(username).innerHTML = 'Disabled';
                    event.target.removeAttribute('checked');
                    this.isLoadingResults = false;
                },
                err => {
                    // alert(username);
                    this.isLoadingResults = false;
                    document.getElementById(username + 'span').click;
                }
            );
        } else {
            this.searchUserService.changeUserAccess(username, 'enabled').subscribe(
                result => {
                    document.getElementById(username).innerHTML = 'Enabled';
                    event.target.setAttribute('checked', 'checked');
                    this.isLoadingResults = false;
                },
                err => {
                    // alert(username);
                    this.isLoadingResults = false;
                    document.getElementById(username + 'span').click;
                }
            );
        }
        // console.log(event.target);
    }

    getUserRoles(userDetails: any) {
        this.userRoles.forEach(e => {
            e.disabled = false;
        });
        this.userRoleValidation(userDetails.roleIDs);
    }
    changeRole(event, users) {
        // alert(event.value);
        this.isLoadingResults = true;
        if (event.value.length === 0) {
            this.userRoles.forEach(e => {
                e.disabled = false;
            });
        }
        this.userRoleValidation(event.value);
        const user = new IdentityProviderUser();
        user.authServiceRoleID = event.value;
        user.userName = users.userName;
        this.onboardingUserService.updateUserRoles(user).subscribe(
            result => {
                this.isLoadingResults = false;
            },
            err => {
                this.isLoadingResults = false;
            }
        );
    }
    check($event: any): void {
        this.showToolTip = $event.srcElement.offsetWidth < $event.srcElement.scrollWidth ? true : false;
    }
    userRoleValidation(roleIDs: any) {
        roleIDs.forEach(res => {
            this.userRoles.forEach(element => {
                if (element.authServiceRoleID === res) {
                    if (element.name.toLowerCase() !== 'role_user') {
                        if (element.name.toLowerCase() === 'role_admin') {
                            this.userRoles.forEach(e => {
                                if (e.name.toLowerCase().includes('role_operator') || e.name.toLowerCase().includes('role_adjudicator')) {
                                    e.disabled = true;
                                } else {
                                    e.disabled = false;
                                }
                            });
                        } else if (element.name.toLowerCase().includes('role_operator')) {
                            this.userRoles.forEach(e => {
                                if (
                                    e.name.toLowerCase().includes('role_admin') ||
                                    e.name.toLowerCase() === 'role_adjudicator' ||
                                    (e.name.toLowerCase().includes('role_operator') && e.name !== element.name)
                                ) {
                                    e.disabled = true;
                                } else {
                                    e.disabled = false;
                                }
                            });
                        } else if (element.name.toLowerCase() === 'role_adjudicator') {
                            this.userRoles.forEach(e => {
                                if (e.name.toLowerCase().includes('role_admin') || e.name.toLowerCase().includes('role_operator')) {
                                    e.disabled = true;
                                } else {
                                    e.disabled = false;
                                }
                            });
                        }
                    } else {
                        if (roleIDs.length === 1 && element.name.toLowerCase() === 'role_user') {
                            this.userRoles.forEach(e => {
                                e.disabled = false;
                            });
                        }
                    }
                }
            });
        });
    }
}

interface ConfigValue {
    id: number;
    value: string;
}

@Component({
    templateUrl: 'mobile-devices-dialog.html'
})
export class MobileDevicesDialogComponent {
    canIssueMobile = false;
    canIssueCard = false;
    devProfiles;
    headerText;
    constructor(public dialogRef: MatDialogRef<MobileDevicesDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogDataResponse) {
        this.headerText = data.headerText;
        if (data.user != null) {
            this.devProfiles = data.user.wfDeviceProfiles;
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }
}
