export class VehicleRegistration {
    vin: string;
    make: string;
    year: number;
    model: string;
    color: string;
    licensePlateNumber: string;
    capacity: number;
    dateOfSale: Date;
    dealerName: string;
    dealerLicenseNumber: string;
    dealerInvoiceNumber: string;

    // Owner
    ownerName: string;
    ownerAddress: string;

    // Insurance
    policyNumber: string;
    insuranceCompanyName: string;
    insuranceAgentName: string;
    constructor() { }
}
