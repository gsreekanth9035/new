import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from './../material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VEHICLE_REGISTRATION_ROUTES } from './vehicle-registration.route';
import { VehicleRegistrationComponent } from './create/vehicle-registration.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ManageVehicleRegComponent } from './manage/manage-vehicle-reg.component';
import { VehicleRegistrationService } from './vehicle-registration.service';

@NgModule({
  imports: [
      RouterModule.forChild(VEHICLE_REGISTRATION_ROUTES),
      CommonModule,
      BrowserAnimationsModule,
      ReactiveFormsModule,
      HttpClientModule,
      MaterialModule,
      GatewaySharedModule
  ],
  declarations: [ VehicleRegistrationComponent, ManageVehicleRegComponent ]
})

export class VehicleRegistrationModule { }
