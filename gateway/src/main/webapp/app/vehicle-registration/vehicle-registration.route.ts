import { VehicleRegistrationComponent } from './create/vehicle-registration.component';
import {  Routes } from '@angular/router';
import { ManageVehicleRegComponent } from './manage/manage-vehicle-reg.component';

export const VEHICLE_REGISTRATION_ROUTES: Routes = [
    {
        path: 'vehicle-registration',
        component: ManageVehicleRegComponent,
        data: {
            authorities: [],
            pageTitle: 'vehicleRegistration.pageTitle',
        }
    },
    {
        path: 'vehicle-registration/create-vehicle-registration',
        component: VehicleRegistrationComponent,
        data: {
            authorities: [],
            pageTitle: 'vehicleRegistration.title',
        }
    }
];
