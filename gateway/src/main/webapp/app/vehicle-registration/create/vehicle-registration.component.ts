import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { VehicleRegistrationService } from '../vehicle-registration.service';
import { DataService } from 'app/device-profile/data.service';
import { VehicleRegistration } from '../vehicle-reg.model';
import { TranslateService } from '@ngx-translate/core';
import { DateAdapter, MAT_DATE_FORMATS, NativeDateAdapter } from '@angular/material';
import { APP_DATE_FORMATS, AppDateAdapter } from 'app/shared/util/date.adapter';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'jhi-vehicle-registration',
    templateUrl: './vehicle-registration.component.html',
    styleUrls: ['vehicle-registration.scss'],
    providers: [
        { provide: DateAdapter, useClass: AppDateAdapter },
         { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
    ]
})

export class VehicleRegistrationComponent implements OnInit, OnDestroy {
    regDetailsJsonString: string;
    vehicleRegForm: FormGroup;
    regDetailsError = false;
    licencePlateNo: string;
    editMode = false;
    vehiclRegistrationDetails = new VehicleRegistration();
    saveSuccess = false;
    saveFailure = false;
    regOptions = [{name: 'Upload JSON File', value: 'file' }, {name: 'Vehicle Registration Form', value: 'form' }];
    display = false;
    message: string;
    dateOfSale;

    constructor(private router: Router, private route: ActivatedRoute, private vehicleRegistrationService: VehicleRegistrationService, private formBuilder: FormBuilder,
        private dataService: DataService, private translateService: TranslateService, private datePipe: DatePipe) {
    }

    ngOnInit() {
        this.licencePlateNo = this.route.snapshot.paramMap.get('lpn');
        if (this.licencePlateNo !== null) {
            this.editMode = true;
            this.vehicleRegistrationService.getVehicleRegByLicencePlateNumber(this.licencePlateNo).subscribe(vehicleRegDetails => {
                const myDate = vehicleRegDetails.dateOfSale.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, '$2/$1/$3');
                const newDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
                vehicleRegDetails.dateOfSale = new Date(newDate);
                this.vehiclRegistrationDetails = vehicleRegDetails;
                this.regOptions = [{name: 'Vehicle Registration Form', value: 'form' }];
                this.buildVehicleRegForm();
                this.vehicleRegForm.controls['selectedOption'].setValue('form');
                this.display = true;
            });
        } else {
            this.buildVehicleRegForm();
            this.vehicleRegForm.controls['selectedOption'].setValue('form');
            this.display = true;
        }
    }

    buildVehicleRegForm() {
        this.vehicleRegForm = this.formBuilder.group({
            selectedOption: [''],
            vin: [this.vehiclRegistrationDetails.vin],
            make: [this.vehiclRegistrationDetails.make],
            year: [this.vehiclRegistrationDetails.year],
            model: [this.vehiclRegistrationDetails.model],
            color: [this.vehiclRegistrationDetails.color],
            licensePlateNumber: [this.vehiclRegistrationDetails.licensePlateNumber],
            capacity: [this.vehiclRegistrationDetails.capacity],
            dateOfSale: [this.vehiclRegistrationDetails.dateOfSale],
            dealerName: [this.vehiclRegistrationDetails.dealerName],
            dealerLicenseNumber: [this.vehiclRegistrationDetails.dealerLicenseNumber],
            dealerInvoiceNumber: [this.vehiclRegistrationDetails.dealerInvoiceNumber],
            ownerName: [this.vehiclRegistrationDetails.ownerName],
            ownerAddress: [this.vehiclRegistrationDetails.ownerAddress],
            policyNumber: [this.vehiclRegistrationDetails.policyNumber],
            insuranceCompanyName: [this.vehiclRegistrationDetails.insuranceCompanyName],
            insuranceAgentName: [this.vehiclRegistrationDetails.insuranceAgentName]
        });
        this.formControlValueChanges();
    }

    formControlValueChanges() {
        const vin = this.vehicleRegForm.controls.vin;
        const make = this.vehicleRegForm.controls.make;
        const year = this.vehicleRegForm.controls.year;
        const model = this.vehicleRegForm.controls.model;
        const color = this.vehicleRegForm.controls.color;
        const licensePlateNumber = this.vehicleRegForm.controls.licensePlateNumber;
        const capacity = this.vehicleRegForm.controls.capacity;
        const dateOfSale = this.vehicleRegForm.controls.dateOfSale;
        const dealerName = this.vehicleRegForm.controls.dealerName;
        const dealerLicenseNumber = this.vehicleRegForm.controls.dealerLicenseNumber;
        const dealerInvoiceNumber = this.vehicleRegForm.controls.dealerInvoiceNumber;
        const ownerName = this.vehicleRegForm.controls.ownerName;
        const ownerAddress = this.vehicleRegForm.controls.ownerAddress;
        const policyNumber = this.vehicleRegForm.controls.policyNumber;
        const insuranceCompanyName = this.vehicleRegForm.controls.insuranceCompanyName;
        const insuranceAgentName = this.vehicleRegForm.controls.insuranceAgentName;

        this.vehicleRegForm.get('selectedOption').valueChanges.subscribe(mode => {
            if (mode === 'file') {
                vin.clearValidators();
                make.clearValidators();
                year.clearValidators();
                model.clearValidators();
                color.clearValidators();
                licensePlateNumber.clearValidators();
                capacity.clearValidators();
                dateOfSale.clearValidators();
                dealerName.clearValidators();
                dealerLicenseNumber.clearValidators();
                dealerInvoiceNumber.clearValidators();
                ownerName.clearValidators();
                ownerAddress.clearValidators();
                policyNumber.clearValidators();
                insuranceCompanyName.clearValidators();
                insuranceAgentName.clearValidators();
            } else if (mode === 'form') {
                this.regDetailsError = false;
                vin.setValidators([Validators.required]);
                make.setValidators([Validators.required]);
                year.setValidators([Validators.required]);
                model.setValidators([Validators.required]);
                color.setValidators([Validators.required]);
                licensePlateNumber.setValidators([Validators.required]);
                capacity.setValidators([Validators.required]);
                dateOfSale.setValidators([Validators.required]);
                dealerName.setValidators([Validators.required]);
                dealerLicenseNumber.setValidators([Validators.required]);
                dealerInvoiceNumber.setValidators([Validators.required]);
                ownerName.setValidators([Validators.required]);
                ownerAddress.setValidators([Validators.required]);
                policyNumber.setValidators([Validators.required]);
                insuranceCompanyName.setValidators([Validators.required]);
                insuranceAgentName.setValidators([Validators.required]);
            }
            vin.updateValueAndValidity();
            make.updateValueAndValidity();
            year.updateValueAndValidity();
            model.updateValueAndValidity();
            color.updateValueAndValidity();
            licensePlateNumber.updateValueAndValidity();
            capacity.updateValueAndValidity();
            dateOfSale.updateValueAndValidity();
            dealerName.updateValueAndValidity();
            dealerLicenseNumber.updateValueAndValidity();
            dealerInvoiceNumber.updateValueAndValidity();
            ownerName.updateValueAndValidity();
            ownerAddress.updateValueAndValidity();
            policyNumber.updateValueAndValidity();
            insuranceCompanyName.updateValueAndValidity();
            insuranceAgentName.updateValueAndValidity();
        });
    }
    saveVehicleRegistration() {
        this.saveSuccess = false;
        this.saveFailure = false;
        this.regDetailsError = false;
        if (this.editMode) {
            this.vehicleRegistrationService.updateVehicleRegistration(this.vehiclRegistrationDetails.licensePlateNumber, this.vehicleRegForm.value).subscribe(
                result => {
                    if (result.status === 201) {
                        this.translateService.get('vehicleRegistration.messages.updateSuccess').subscribe(message => {
                            this.message = message;
                            this.saveSuccess = true;
                            this.saveFailure = false;
                        });
                    }
                }, error => {
                    this.handleError(error);
                });
        } else {
            if (this.vehicleRegForm.controls['selectedOption'].value === 'file') {
                if (this.regDetailsJsonString === undefined) {
                    this.regDetailsError = true;
                  return;
                }
            }
            if (this.vehicleRegForm.controls['selectedOption'].value === 'form') {
                this.regDetailsJsonString = null;
                const jsonString = {
                    'vehicleRegistrations': [
                        this.vehicleRegForm.value
                    ]
                };
                this.regDetailsJsonString = JSON.stringify(jsonString);
            }
            this.vehicleRegistrationService.createVehicleRegistration(this.regDetailsJsonString).subscribe(
                response => {
                    if (response.status === 201) {
                        this.translateService.get('vehicleRegistration.messages.success').subscribe(message => {
                            this.message = message;
                            this.saveSuccess = true;
                            this.saveFailure = false;
                        });
                    }
                },
                error => {
                    this.handleError(error);
                }
            );
        }
    }
    handleError(error) {
        if (error.status === 406) {
            this.translateService.get('vehicleRegistration.messages.failure').subscribe(message => {
                this.message = message;
            });
        } else if (error.error.status === 500) {
            this.message = error.error.detail;
            if (this.message === undefined) {
                this.translateService.get('vehicleRegistration.messages.failure').subscribe(message => {
                    this.message = message;
                });
            }
        } else {
            this.translateService.get('vehicleRegistration.messages.failure').subscribe(message => {
                this.message = message;
            });
        }
        this.saveFailure = true;
        this.saveSuccess = false;
        window.scrollTo(0, 0);
    }
    onRegistrationDetailsChange(event) {
        this.regDetailsError = false;
        if (event.target.files.length > 0) {
            this.regDetailsError = false;
        } else {
            this.regDetailsError = true;
            return;
        }
        const reader = new FileReader();
        const [file] = event.target.files;
        reader.readAsText(file);
        reader.onload = () => {
            this.regDetailsJsonString = reader.result.toString();
            const obj =  JSON.parse(this.regDetailsJsonString);
            const myDate = obj.vehicleRegistrations[0].dateOfSale.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, '$2/$1/$3');
            const newDate = this.datePipe.transform(myDate, 'yyyy-MM-dd', 'es-ES');
            obj.vehicleRegistrations[0].dateOfSale = newDate;
            this.regDetailsJsonString = JSON.stringify(obj);
        };
    }

    goHome() {
        this.router.navigate(['/vehicle-registration']);
    }
    ngOnDestroy() {
        this.dataService.changeObj(null);
    }

}
 export class Options {
     name: string;
     value: string;
     constructor() {}
 }
