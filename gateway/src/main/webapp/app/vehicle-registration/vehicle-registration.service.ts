import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, observable } from 'rxjs';
import { Principal } from 'app/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { VehicleRegistration } from './vehicle-reg.model';
import { Page } from 'app/visual-design/page.model';

@Injectable({
    providedIn: 'root'
})
export class VehicleRegistrationService {
    constructor(private httpClient: HttpClient, private principal: Principal) {}

    // get all vehicle reg of user
    getVehicleRegistration(sort: string, order: string, page: number, size: number): Observable<Page> {
        const httpHeaders = new HttpHeaders({
            'Content-type': 'application/json'
        });
        return this.httpClient.get<Page>(`${window.location.origin}/usermanagement/api/v1/organizations/
    ${this.principal.getLoggedInUser().organization}/vehicle-registrations?sort=${sort}&order=${order}&page=${page}&size=${size}`);
    }

    // Call REST APIs
    createVehicleRegistration(jsonString: string): Observable<any> {
        const httpHeaders = new HttpHeaders({
            'Content-type': 'application/json'
        });

        return this.httpClient.post(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/vehicle-registrations`,
            jsonString,
            { headers: httpHeaders, observe: 'response' }
        ); // .catch(this.errorHandler);
    }
    getVehicleRegByLicencePlateNumber(licencePlateNumber): Observable<any> {
        return this.httpClient.get(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/vehicle-registrations/${licencePlateNumber}`
        );
    }

    updateVehicleRegistration(licencePlateNumber, vehicleReg: VehicleRegistration): Observable<any> {
        return this.httpClient.put(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/vehicle-registrations/${licencePlateNumber}`,
            vehicleReg,
            { observe: 'response' }
        );
    }
    deleteVehicleRegistration(vin: string): Observable<any> {
        return this.httpClient.delete(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/vehicle-registrations/${vin}`,
            { observe: 'response' }
        );
    }

    errorHandler(error: HttpErrorResponse) {
        return Observable.throw('Error: Vehicle Registration failed');
    }
}
