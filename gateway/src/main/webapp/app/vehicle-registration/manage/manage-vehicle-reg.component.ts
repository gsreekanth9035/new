import { OnInit, Component, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { DataService } from 'app/device-profile/data.service';
import { VehicleRegistration } from '../vehicle-reg.model';
import { VehicleRegistrationService } from '../vehicle-registration.service';
import { merge, of as observableOf } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';

@Component({
    selector: 'jhi-manage-vehicle-reg',
    templateUrl: './manage-vehicle-reg.component.html',
    styleUrls: ['manage-vehicle-reg.scss']
})
export class ManageVehicleRegComponent implements OnInit {
    displayedColumns: string[] = ['vin', 'make', 'year', 'model', 'licensePlateNumber', 'owner', 'actions'];
    data: VehicleRegistration[] = [];
    message: any;
    succesMsg = false;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    resultsLength = 0;
    pageSize = 5;

    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private vehicleRegService: VehicleRegistrationService,
        private dataService: DataService
    ) {}

    ngOnInit() {
        this.getVehicleRegList();
    }

    getVehicleRegList() {
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    if (this.paginator.pageSize === undefined) {
                        this.paginator.pageSize = this.pageSize;
                    }
                    return this.vehicleRegService.getVehicleRegistration(
                        this.sort.active,
                        this.sort.direction,
                        this.paginator.pageIndex,
                        this.paginator.pageSize
                    );
                }),
                map(data => {
                    this.resultsLength = data.totalElements;
                    return data.content;
                }),
                catchError(() => {
                    return observableOf([]);
                })
            )
            .subscribe(data => (this.data = data));
    }

    addVehicleRegistration() {
        this.router.navigate(['/vehicle-registration/create-vehicle-registration']);
    }
    editVehicleReg(licencePlateNumber: string) {
        this.router.navigate(['/vehicle-registration/create-vehicle-registration', { lpn: licencePlateNumber }]);
    }
    deleteVehicleReg(vin) {
        this.vehicleRegService.deleteVehicleRegistration(vin).subscribe(
            result => {
                if (result.status === 200) {
                    this.getVehicleRegList();
                    this.succesMsg = true;
                    this.message = 'Vehicle Registration deleted successfully';
                    window.scrollTo(0, 0);
                }
            },
            error => {
                console.log(error);
            }
        );
    }
}
