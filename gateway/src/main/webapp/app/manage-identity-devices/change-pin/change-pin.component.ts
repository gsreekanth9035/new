import { Component, OnInit, OnDestroy } from '@angular/core';
import { CardReader } from 'app/issuance/model/cardReader.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IssuanceService } from 'app/issuance/issuance.service';
import { DataService } from 'app/device-profile/data.service';
import { timer, Subscription, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { compareValidator } from 'app/issuance/CompareValidator';
import { Device } from '../manage-identity-devices.model';
import { UserInfo, Principal } from 'app/core';
import { CmsSyncRequest } from 'app/issuance/model/cmsSyncRequest.model';
import { Commands } from 'app/issuance/model/commands.model';
import { TranslateService } from '@ngx-translate/core';
import { DeviceClientService } from 'app/shared/util/device-client.service';

@Component({
    selector: 'jhi-change-pin',
    templateUrl: './change-pin.component.html',
    styleUrls: ['change-pin.scss']
})
export class ChangePinComponent implements OnInit, OnDestroy {
    isLoadingResults = false;
    pinHide = true;
    confirmPinHide = true;
    oldPinHide = true;
    cardReaders: CardReader[] = [];
    changePinForm: FormGroup;
    subscription: Subscription = new Subscription();
    errorDisplay = false;
    display = true;
    device: Device;
    user = new UserInfo();
    workflowName: string;
    orgName: string;
    processName: string;
    message: string;
    flag = false;
    timer: any;
    selectedCardAtr: string;
    selectedCardName: string;
    constructor(
        private formBuilder: FormBuilder,
        private issuanceService: IssuanceService,
        private dataService: DataService,
        private principal: Principal,
        private translateService: TranslateService,
        private deviceClientService: DeviceClientService
    ) {
        this.buildChangePinForm();
    }
    buildChangePinForm() {
        this.changePinForm = this.formBuilder.group({
            identityDevice: ['', Validators.required],
            oldPin: ['', [Validators.required, Validators.pattern('^([0-9]{6,8})$')]],
            pin: ['', [Validators.required, compareValidator('oldPin', 'compareEqual'), Validators.pattern('^([0-9]{6,8})$')]],
            confirmPin: ['', [Validators.required, compareValidator('pin', 'compare'), Validators.pattern('^([0-9]{6,8})$')]]
        });
    }
    ngOnInit() {
        this.device = this.dataService.getCurrentMIdObj();
        if (this.device.userFirstName !== undefined) {
            this.issuanceService.getWorkflowByUsername(this.device.userName).subscribe(workflow => {
                this.workflowName = workflow;
            });
        } else {
            this.workflowName = this.principal.getLoggedInUser().workflow;
        }
        this.orgName = this.principal.getLoggedInUser().organization;
        this.getReadersList();
    }

    getReadersList() {
        this.subscription = timer(0, 2000)
            .pipe(switchMap(() => this.deviceClientService.getCardReaders()))
            .subscribe(
                readersList => {
                    const list = readersList.cardReaderList;
                    this.flag = false;
                    let updateList = false;
                    if (readersList.responseCode === 'DS0000') {
                        //     this.activationForm get('identityDevice')
                        const selectedReader = this.changePinForm.controls['identityDevice'].value;
                        const cr = this.changePinForm.controls['identityDevice'].value;
                        if (cr === null || cr === undefined) {
                            this.cardReaders = list;
                            this.changePinForm.controls['identityDevice'].setValue(this.cardReaders[0]);
                            this.setCardName(this.cardReaders[0]);
                            return;
                        }
                        if (this.cardReaders.length === list.length || this.cardReaders.length > list.length) {
                            let isFound = false;
                            for (const c of this.cardReaders) {
                                const found = list.find(e => e.cardReaderName === c.cardReaderName);
                                if (found === undefined) {
                                    updateList = true;
                                } else {
                                    if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                    }
                                }
                            }
                            const card1 = list.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (card1 !== undefined) {
                                if (!isFound || this.selectedCardName === '' || card1.atr !== this.selectedCardAtr) {
                                    if (card1 !== undefined) {
                                        this.setCardName(card1);
                                    }
                                }
                            }
                        } else if (this.cardReaders.length < list.length) {
                            let isFound = false;
                            for (const c of list) {
                                const found = this.cardReaders.find(e => e.cardReaderName === c.cardReaderName);
                                if (found === undefined) {
                                    updateList = true;
                                } else {
                                    if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                    }
                                }
                            }
                            const card1 = list.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (card1 !== undefined) {
                                if (!isFound || this.selectedCardName === '' || card1.atr !== this.selectedCardAtr) {
                                    if (card1 !== undefined) {
                                        this.setCardName(card1);
                                    }
                                }
                            }
                        }
                        if (updateList) {
                            this.cardReaders = list;
                            const cardReader: CardReader = this.cardReaders.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (cardReader !== undefined) {
                                this.changePinForm.controls['identityDevice'].setValue(cardReader);
                                if (cardReader.atr !== this.selectedCardAtr) {
                                    this.setCardName(cardReader);
                                }
                            } else {
                                this.changePinForm.controls['identityDevice'].setValue(this.cardReaders[0]);
                                if (this.cardReaders[0].atr !== this.selectedCardAtr) {
                                    this.setCardName(this.cardReaders[0]);
                                }
                            }
                        }
                    } else {
                        this.changePinForm.controls['identityDevice'].setValue(null);
                        this.cardReaders = [];
                        this.selectedCardAtr = '';
                        this.selectedCardName = '';
                        //  this.notSupportedCard = false;
                        this.translateService.get('issuance.connectReaderAndCard').subscribe(msg => {
                            this.message = msg;
                            this.errorDisplay = true;
                        });
                        window.scrollTo(0, 0);
                    }
                    this.flag = true;
                },
                error => {
                    this.isLoadingResults = false;
                    if (this.subscription) {
                        this.subscription.unsubscribe();
                    }
                    this.errorDisplay = true;
                    this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                        this.message = serviceErrMessage;
                        // Retrying
                        this.getReadersList();
                    });
                }
            );
    }
    setCardName(cardReader: CardReader) {
        const atr = cardReader.atr;
        if (atr.length !== 0) {
            this.errorDisplay = false;
            const newAtr = atr.replace(/ /g, '');
            this.issuanceService.getCardNameByAtr(newAtr).subscribe(name => {
                if (this.selectedCardName !== name) {
                    this.selectedCardAtr = atr;
                    this.selectedCardName = name;
                }
            });
        } else {
            this.translateService.get('issuance.cardNotFound').subscribe(
                msg => {
                    this.errorDisplay = true;
                    this.message = msg;
                    this.selectedCardAtr = '';
                    this.selectedCardName = '';
                    window.scrollTo(0, 0);
                },
                err => {
                    this.message = 'Card not found, Please insert';
                }
            );
        }
    }

    changePin() {
        let n = 0;
        this.timer = setTimeout(() => {
            console.log(n);
            if (this.flag && n === 0) {
                n++;
                this.checkConnections();
            }
        }, 1000);
    }
    checkConnections() {
        if (this.changePinForm.controls.identityDevice.value === null) {
            this.translateService.get('issuance.connectReaderAndCard').subscribe(msg => {
                this.message = msg;
                this.errorDisplay = true;
                window.scrollTo(0, 0);
                return null;
            });
        } else {
            this.deviceClientService
                .getAtrByReaderName(this.changePinForm.controls.identityDevice.value.urlEncodedcardReaderName)
                .subscribe(response => {
                    if (response.atr === '') {
                        this.translateService.get('issuance.cardNotFound').subscribe(message => {
                            this.errorDisplay = true;
                            this.message = message;
                            window.scrollTo(0, 0);
                            return;
                        });
                    } else {
                        this.startChangePin(response.atr);
                    }
                });
        }
    }
    startChangePin(atr) {
        this.isLoadingResults = true;
        this.errorDisplay = false;
        // removing the white spaces in atr
        const newAtr = atr.replace(/ /g, '');
        const cardReaderName = this.changePinForm.controls.identityDevice.value.cardReaderName;
        this.processName = 'change-pin';
        this.errorDisplay = false;
        // start identify process
        this.issuanceService.start('identify', newAtr).subscribe(
            identifyStartResponse => {
                // execute the commands to card
                identifyStartResponse.body.state = 0;
                this.deviceClientService.execute(identifyStartResponse.body, cardReaderName).subscribe(
                    identifyCommandsRes => {
                        this.issuanceService.next(identifyCommandsRes.body).subscribe(
                            identifyNextRes => {
                                let nextResponse = new Commands();
                                nextResponse = identifyNextRes.body;
                                if (nextResponse.step === 'done') {
                                    const cuid = identifyNextRes.body.entries[0].result.serial;
                                    console.log('cuid::::::', cuid);
                                    if (cuid === this.device.uniqueIdentifier) {
                                        // get uuid
                                        const cmsSyncRequest = this.buildCmsSyncRequest(nextResponse);
                                        this.issuanceService.getUUID(this.processName, cmsSyncRequest).subscribe(
                                            uuid => {
                                                // start change pin process
                                                this.issuanceService.start(this.processName, uuid).subscribe(
                                                    issaunceStartRes => {
                                                        // send the commands to card
                                                        this.deviceClientService.execute(issaunceStartRes.body, cardReaderName).subscribe(
                                                            result => {
                                                                this.issuanceService.next(result.body).subscribe(
                                                                    res => {
                                                                        this.recurse(res.body, cardReaderName);
                                                                    },
                                                                    error => {
                                                                        this.isLoadingResults = false;
                                                                        console.log(error);
                                                                    }
                                                                );
                                                            },
                                                            error => {
                                                                this.isLoadingResults = false;
                                                                console.log(error);
                                                            }
                                                        );
                                                    },
                                                    error => {
                                                        this.isLoadingResults = false;
                                                        console.log(error);
                                                    }
                                                );
                                            },
                                            error => {
                                                this.isLoadingResults = false;
                                                console.log(error);
                                            }
                                        );
                                    } else {
                                        this.translateService.get('manageIdentiyDevices.noMatchCard').subscribe(message => {
                                            this.message = message;
                                            window.scrollTo(0, 0);
                                            this.errorDisplay = true;
                                        });
                                        this.isLoadingResults = false;
                                    }
                                } else {
                                    if (nextResponse.step === 'error' || nextResponse.stage === 'error') {
                                        this.translateService.get('manageIdentiyDevices.changePinFailed').subscribe(message => {
                                            this.message = message;
                                            this.errorDisplay = true;
                                            this.isLoadingResults = false;
                                            window.scrollTo(0, 0);
                                        });
                                    }
                                    return throwError(nextResponse.entries[0].message);
                                }
                            },
                            error => {
                                this.isLoadingResults = false;
                                console.log(error);
                            }
                        );
                    },
                    error => {
                        this.isLoadingResults = false;
                        console.log(error);
                    }
                );
            },
            error => {
                this.isLoadingResults = false;
                console.log(error);
            }
        );
        clearInterval(this.timer);
    }
    recurse(res, cardReaderName) {
        if (res.stage !== 'done' && res.step !== 'error') {
            this.deviceClientService.execute(res, cardReaderName).subscribe(
                result1 => {
                    console.log(result1);
                    this.issuanceService.next(result1.body).subscribe(
                        response => {
                            this.recurse(response.body, cardReaderName);
                        },
                        error => {
                            this.isLoadingResults = false;
                            console.log(error);
                        }
                    );
                },
                error => {
                    this.isLoadingResults = false;
                    console.log(error);
                }
            );
        } else if (res.step === 'error') {
            this.translateService.get('manageIdentiyDevices.changePinFailed').subscribe(message => {
                if (res.entries[0].message.includes('_')) {
                    this.message = `${message}-` + 'Error Code: ' + res.entries[0].message + ')';
                } else {
                    this.message = 'Change Device PIN Failed!';
                }
                this.errorDisplay = true;
                this.isLoadingResults = false;
                window.scrollTo(0, 0);
            });
        } else if (res.step === 'done' && res.stage === 'done') {
            this.translateService.get('manageIdentiyDevices.changePinSuccessMsg').subscribe(message => {
                this.message = message;
            });
            this.isLoadingResults = false;
            this.errorDisplay = false;
            this.display = false;
            this.subscription.unsubscribe();
        }
    }
    buildCmsSyncRequest(nextResponse) {
        const cmsSyncRequest = new CmsSyncRequest();
        cmsSyncRequest.processName = this.processName;
        cmsSyncRequest.deviceType = 'permanent';
        cmsSyncRequest.device = {
            type: nextResponse.entries[0].result.type,
            serial: nextResponse.entries[0].result.serial,
            cardType: nextResponse.entries[0].result.cardType,
            cardProductName: nextResponse.entries[0].result.cardProductName
        };
        cmsSyncRequest.user = this.device.userName;
        cmsSyncRequest.orgName = this.orgName;
        cmsSyncRequest.wfName = this.workflowName;
        cmsSyncRequest.oldPin = this.changePinForm.controls.oldPin.value;
        cmsSyncRequest.newPin = this.changePinForm.controls.pin.value;
        return cmsSyncRequest;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.dataService.changeMIDObj(null);
        clearInterval(this.timer);
    }
}
