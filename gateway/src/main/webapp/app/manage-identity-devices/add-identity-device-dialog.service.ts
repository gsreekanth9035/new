import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Principal } from 'app/core';
import { TranslateService } from '@ngx-translate/core';
import { AddIdentityDeviceDialogComponent } from './manage-identity-devices.component';
import { Data, DataService } from 'app/device-profile/data.service';
import { Router } from '@angular/router';
import { User } from 'app/enroll/user.model';

@Injectable({
    providedIn: 'root'
})
export class AddIdentityDeviceDialogService {
    hasIssuancePermission = false;
    isPairMobileDevicePermissionEnabled = false;
    constructor(
        private principal: Principal,
        private dialog: MatDialog,
        private translateService: TranslateService,
        private dataService: DataService,
        private router: Router
    ) {
        this.principal.hasAuthority('Menu_Issuance').then(res => {
            this.hasIssuancePermission = res;
        });
        this.principal.hasAuthority('Menu_Pair_Mobile_Device').then(res => {
            this.isPairMobileDevicePermissionEnabled = res;
        });
    }
    async openAddIdentityDeviceDialog(user1: User) {
        if (this.dialog.openDialogs.length === 0) {
            const msg = await (<any>this.translateService.get('manageIdentiyDevices.issueIdentityDevices').toPromise());
            const dialogRef = this.dialog.open(AddIdentityDeviceDialogComponent, {
                autoFocus: false,
                width: '900px',
                data: {
                    headerText: msg,
                    user: user1,
                    isIssuancePermissionEnabled: this.hasIssuancePermission,
                    isMobilePermissionEnabled: this.isPairMobileDevicePermissionEnabled
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                const data = new Data();
                data.userFirstName = user1.firstName;
                data.userName = user1.name;
                data.userLastName = user1.lastName;
                data.groupId = user1.groupId;
                data.groupName = user1.groupName;
                data.workflow = user1.workflow;
                data.identityType = user1.identityType;
                this.dataService.changeObj(data);
                if (result !== undefined) {
                    if (result.deviceProfileConfig.isMobileId) {
                        this.router.navigate(['/pairmobile']);
                    } else {
                        result.groupId = user1.groupId;
                        result.groupName = user1.groupName;
                        result.identityType = user1.identityType;
                        console.log(result);
                        this.dataService.changeIdentityDevice(result);
                        this.router.navigate(['/issuance']);
                    }
                }
            });
        }
    }
}
