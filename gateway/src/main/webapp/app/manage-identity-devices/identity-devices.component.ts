import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'jhi-identity-devices',
    templateUrl: './identity-devices.component.html',
    styleUrls: []
})
export class IdentityDevicesComponent implements OnInit {

    constructor(
        private router: Router,
    ) { }

     ngOnInit() {
    }

}
