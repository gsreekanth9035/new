import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Principal } from 'app/core';
import { Observable } from 'rxjs';
import { Device, MobileCredential } from './manage-identity-devices.model';
import { DeviceCertificates } from './renew/deviceCerts.model';
import { User } from 'app/enroll/user.model';
import { WfStepAppletLoadingInfo } from 'app/workflow/model/wf-applet-loading-info.model';

@Injectable({
    providedIn: 'root'
})
export class ManageIdentityDevicesService {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name
        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });
    url: string;
    constructor(private httpClient: HttpClient, private principal: Principal) {
        this.url = window.location.origin + '/apdu/api/v1';
    }
    getDevicesOfUser(userId, role: string): Observable<Device[]> {
        return this.httpClient.get<Array<Device>>(`${this.url}/users/${userId}/devices?role=${role}`);
    }
    getUserMobileCredentials(userId, uniqueIdentifier, organizationName, role): Observable<MobileCredential[]> {
        return this.httpClient.get<MobileCredential[]>(
            `${this.url}/users/${userId}/devices/${uniqueIdentifier}?organizationName=${organizationName}&role=${role}`
        );
    }
    // TODO Find Other Way to Get User ID
    getUserByName(userName: string): Observable<User> {
        return this.httpClient.get<User>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/users/${userName}`
        );
    }

    manageDevice(userId, device: Device): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.put(`${this.url}/users/${userId}/devices/organizations/${orgName}/manageDevice`, device, {
            headers: this.httpHeaders,
            observe: 'response'
        });
    }
    updateMobileCredential(deviceId, device: Device): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.put(`${this.url}/organizations/${orgName}/mobile-devices/${deviceId}/credentials`, device, {
            headers: this.httpHeaders,
            observe: 'response'
        });
    }
    updateMobileOTPCredential(userName: String): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        const orgName = this.principal.getLoggedInUser().organization;
        const lurl = window.location.origin + '/usermanagement/api/v1';
        return this.httpClient.put(`${lurl}/organizations/${orgName}/users/${userName}/resetMobileOTPCredential`, {
            headers: this.httpHeaders,
            observe: 'response'
        });
    }
    renewCerts(userId, deviceId, orgName: string, wfName: string): Observable<Array<DeviceCertificates>> {
        return this.httpClient.get<Array<DeviceCertificates>>(
            `${this.url}/users/${userId}/devices/${deviceId}/certificates?organizationName=${orgName}&workflowName=${wfName}`
        );
    }
    getPUK(deviceId, userId): Observable<any> {
        return this.httpClient.get(`${this.url}/devices/${deviceId}/${userId}/puk`);
    }

    getAppletLoadingInfoForJcop4(orgName: string, workflowName: string): Observable<WfStepAppletLoadingInfo> {
        return this.httpClient.get<WfStepAppletLoadingInfo>(
            `${
                window.location.origin
            }/usermanagement/api/v1/organizations/${orgName}/workflows/${workflowName}/steps/perso-and-issuance-step/applet-loading-info?requestFrom=portal`
        );
    }

    sendPinOverMail(orgName: string, userId: number, uniqueIdentifier: string, userEmail1: string): Observable<any> {
        // this.httpHeaders.append('userEmail', userEmail);
        const httpHeaders = new HttpHeaders({
            userEmail: userEmail1
        });
        return this.httpClient.get(`${this.url}/organizations/${orgName}/users/${userId}/devices/${uniqueIdentifier}/send-pin-mail`, {
            headers: httpHeaders,
            observe: 'response'
        });
    }

    getUserCredentials(userId: string): Observable<CredentialRepresentation[]> {
        return this.httpClient.get<Array<CredentialRepresentation>>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/idp-users/${userId}/user-credentials`
        );
    }
    moveCredentialAfterAnotherCredential(userId: string, credentialId: string, newPreviousCredentialId: string): Observable<any> {
        return this.httpClient.get(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/idp-users/${userId}/user-credentials/${credentialId}/moveAfter/${newPreviousCredentialId}`,
            { observe: 'response' }
        );
    }
    moveCredentialToFirst(userId: string, credentialId: string): Observable<any> {
        return this.httpClient.get(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/idp-users/${userId}/user-credentials/${credentialId}/moveToFirst`,
            { observe: 'response' }
        );
    }
}
export class CredentialRepresentation {
    id: string;
    type: string;
    value: string;
    temporary: boolean;

    period: number;
    digits: number;
    counter: number;
    algorithm: string;
    priority: number;
    userLabel: string;
}
