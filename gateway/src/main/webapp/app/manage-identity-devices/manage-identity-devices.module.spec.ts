import { ManageIdentityDevicesModule } from './manage-identity-devices.module';

describe('ManageIdentityDevicesModule', () => {
  let manageIdentityDevicesModule: ManageIdentityDevicesModule;

  beforeEach(() => {
    manageIdentityDevicesModule = new ManageIdentityDevicesModule();
  });

  it('should create an instance', () => {
    expect(manageIdentityDevicesModule).toBeTruthy();
  });
});
