import { CardReader } from 'app/issuance/model/cardReader.model';

export class Device {
    id: number;
    friendlyName: string;
    type: string;
    uniqueIdentifier: string;
    uimsId: string;
    operatingSystem: string;
    osVersion: string;
    appVersion: string;
    deviceModelName: string;
    deviceManufacturerName: string;
    productName: string;
    idForToolTip: string;
    idForCuid: string;
    status: string;
    devProfName: string;
    userDevices: UserDevice[];
    mobileCredentials: MobileCredential[] = [];
    cardType: string;
    deviceType: string;

    cardReader: CardReader;
    connection: string;
    health: string;
    displayIcon: string;
    tooltipMsg: string;
    actionType: string;
    reason: number;
    reasonComment: string;
    roleDeviceActions: Array<RoleDeviceAction> = [];
    actions: Array<String> = [];
    deviceConnectedActions: Array<String> = [];
    deviceNotConnectedActions: Array<String> = [];
    userName: string;
    userFirstName: string;
    isMobileSelected: boolean;
    userId: number;
    userEmail: string;

    constructor() {}
}

export class UserDevice {
    issuedDate: Date;
    activationDate: Date;
    expirationDate: Date;
    status: string;
    constructor() {}
}
export class MobileCredential {
    mobileCredIdToolTip: string;

    credentialTemplateID: number;
    status: string;
    roleDeviceActions: Array<RoleDeviceAction> = [];
    actions: Array<String> = [];
    deviceConnectedActions: Array<String> = [];
    deviceNotConnectedActions: Array<String> = [];
    credentialTemplateName: string;
    base64FrontCredential: string;
    base64BackCredential: string;
    userDeviceMobileCredentialID: number;
    // List<VehicleRegistration> vehicleRegistrations;

    friendlyName: string;
    visualIdNumber: string;
    softOTP: boolean;
    otpConfigStatus: string;
    OtpCode: string;
    pkiCertificatesEnabled: boolean;
    noOfCertificates: number;
    pushVerify: boolean;
    isContentSigning: boolean;
    pushVerifyNumber: string;
    pushVerifyToolTipId: string;
    softOTPToolTipId: string;
    constructor() {}
}

export class RoleDeviceAction {
    deviceType: string;
    roleName: string;
    action: string;
    isConnected: boolean;
    status: string;
    constructor() {}
}
