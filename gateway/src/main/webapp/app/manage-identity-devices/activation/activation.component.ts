import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { compareValidator } from 'app/issuance/CompareValidator';
import { IssuanceService } from 'app/issuance/issuance.service';
import { Commands } from 'app/issuance/model/commands.model';
import { throwError, Subscription, timer } from 'rxjs';
import { CmsSyncRequest } from 'app/issuance/model/cmsSyncRequest.model';
import { UserInfo, Principal } from 'app/core';
import { CardReader } from 'app/issuance/model/cardReader.model';
import { switchMap } from 'rxjs/operators';
import { DataService } from 'app/device-profile/data.service';
import { Device } from '../manage-identity-devices.model';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { DeviceClientService } from 'app/shared/util/device-client.service';
import { UNIFYIA_JCOP4 } from 'app/shared/constants/configValues.constants';
import { ManageIdentityDevicesService } from '../manage-identity-devices.service';
import { WfStepAppletLoadingInfo } from 'app/workflow/model/wf-applet-loading-info.model';

@Component({
    selector: 'jhi-activation',
    templateUrl: './activation.component.html',
    styleUrls: ['activation.scss']
})
export class ActivationComponent implements OnInit, OnDestroy {
    isLoadingResults = false;
    activationForm: FormGroup;
    subscription: Subscription = new Subscription();
    user = new UserInfo();
    userName: string;
    userFirstName: string;
    message: string;
    workflowName: string;
    orgName: string;
    cardReaders: CardReader[] = [];
    processName: string;
    errorDisplay = false;
    identifyProcessResponse: any;
    mappedDeviceID: any;
    device: Device;
    display = true;
    pinHide = true;
    confirmPinHide = true;
    oldPinHide = true;
    flag = false;
    timer: any;
    selectedCardAtr: string;
    selectedCardName: string;
    isFido2Enabled = false;
    userId: number;
    userEmail: string;
    successMail = false;
    successMailMsg;
    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private issuanceService: IssuanceService,
        private principal: Principal,
        private dataService: DataService,
        private translateService: TranslateService,
        private deviceClientService: DeviceClientService,
        private manageIdentityDevicesService: ManageIdentityDevicesService
    ) {
        this.buildActivationForm();
    }

    ngOnInit() {
        this.device = this.dataService.getCurrentMIdObj();
        if (this.device === null) {
            this.router.navigate(['./']);
            return;
        }
        this.userName = this.device.userName;
        this.userFirstName = this.device.userFirstName;
        this.mappedDeviceID = this.device.id;
        this.userId = this.device.userId;
        this.userEmail = this.device.userEmail;
        // this.cardReaders.push(this.device.cardReader);
        this.activationForm.get('identityDevice').setValue(this.cardReaders[0]);
        // if firstname is not null, it will be self activation...
        if (this.userFirstName === undefined) {
            this.workflowName = this.principal.getLoggedInUser().workflow;
        } else {
            this.issuanceService.getWorkflowByUsername(this.userName).subscribe(workflow => {
                this.workflowName = workflow;
            });
        }
        this.orgName = this.principal.getLoggedInUser().organization;
        if (this.device.productName === UNIFYIA_JCOP4) {
            this.manageIdentityDevicesService
                .getAppletLoadingInfoForJcop4(this.orgName, this.workflowName)
                .subscribe((res: WfStepAppletLoadingInfo) => {
                    // if the selected card is jcop4 and also check for fido2 enabled/ not
                    if (res !== null) {
                        this.isFido2Enabled = res.fido2AppletEnabled;
                    }
                });
        }
        this.getReadersList();
    }

    buildActivationForm() {
        this.activationForm = this.formBuilder.group({
            identityDevice: ['', Validators.required],
            pin: ['', [Validators.required, Validators.pattern('^([0-9]{6,8})$')]],
            newPin: ['', [Validators.required, compareValidator('pin', 'compareEqual'), Validators.pattern('^([0-9]{6,8})$')]],
            confirmPin: ['', [Validators.required, compareValidator('newPin', 'compare'), Validators.pattern('^([0-9]{6,8})$')]]
        });
    }
    getReadersList() {
        this.subscription = timer(0, 2000)
            .pipe(switchMap(() => this.deviceClientService.getCardReaders()))
            .subscribe(
                readersList => {
                    const list = readersList.cardReaderList;
                    this.flag = false;
                    let updateList = false;
                    if (readersList.responseCode === 'DS0000') {
                        //     this.activationForm get('identityDevice')
                        const selectedReader = this.activationForm.controls['identityDevice'].value;
                        const cr = this.activationForm.controls['identityDevice'].value;
                        if (cr === null || cr === undefined) {
                            this.cardReaders = list;
                            this.activationForm.controls['identityDevice'].setValue(this.cardReaders[0]);
                            this.setCardName(this.cardReaders[0]);
                            return;
                        }
                        if (this.cardReaders.length === list.length || this.cardReaders.length > list.length) {
                            let isFound = false;
                            for (const c of this.cardReaders) {
                                const found = list.find(e => e.cardReaderName === c.cardReaderName);
                                if (found === undefined) {
                                    updateList = true;
                                } else {
                                    if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                    }
                                }
                            }
                            const card1 = list.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (card1 !== undefined) {
                                if (!isFound || this.selectedCardName === '' || card1.atr !== this.selectedCardAtr) {
                                    if (card1 !== undefined) {
                                        this.setCardName(card1);
                                    }
                                }
                            }
                        } else if (this.cardReaders.length < list.length) {
                            let isFound = false;
                            for (const c of list) {
                                const found = this.cardReaders.find(e => e.cardReaderName === c.cardReaderName);
                                if (found === undefined) {
                                    updateList = true;
                                } else {
                                    if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                    }
                                }
                            }
                            const card1 = list.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (card1 !== undefined) {
                                if (!isFound || this.selectedCardName === '' || card1.atr !== this.selectedCardAtr) {
                                    if (card1 !== undefined) {
                                        this.setCardName(card1);
                                    }
                                }
                            }
                        }
                        if (updateList) {
                            this.cardReaders = list;
                            const cardReader: CardReader = this.cardReaders.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (cardReader !== undefined) {
                                this.activationForm.controls['identityDevice'].setValue(cardReader);
                                if (cardReader.atr !== this.selectedCardAtr) {
                                    this.setCardName(cardReader);
                                }
                            } else {
                                this.activationForm.controls['identityDevice'].setValue(this.cardReaders[0]);
                                if (this.cardReaders[0].atr !== this.selectedCardAtr) {
                                    this.setCardName(this.cardReaders[0]);
                                }
                            }
                        }
                    } else {
                        this.activationForm.controls['identityDevice'].setValue(null);
                        this.cardReaders = [];
                        this.selectedCardAtr = '';
                        this.selectedCardName = '';
                        //  this.notSupportedCard = false;
                        this.translateService.get('issuance.connectReaderAndCard').subscribe(msg => {
                            this.message = msg;
                            this.errorDisplay = true;
                        });
                        window.scrollTo(0, 0);
                    }
                    this.flag = true;
                },
                error => {
                    this.isLoadingResults = false;
                    if (this.subscription) {
                        this.subscription.unsubscribe();
                    }
                    this.errorDisplay = true;
                    this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                        this.message = serviceErrMessage;
                        // Retrying
                        this.getReadersList();
                    });
                }
            );
    }
    setCardName(cardReader: CardReader) {
        const atr = cardReader.atr;
        if (atr.length !== 0) {
            this.errorDisplay = false;
            const newAtr = atr.replace(/ /g, '');
            this.issuanceService.getCardNameByAtr(newAtr).subscribe(name => {
                if (this.selectedCardName !== name) {
                    this.selectedCardAtr = atr;
                    this.selectedCardName = name;
                }
            });
        } else {
            this.translateService.get('issuance.cardNotFound').subscribe(
                msg => {
                    this.errorDisplay = true;
                    this.message = msg;
                    this.selectedCardAtr = '';
                    this.selectedCardName = '';
                    window.scrollTo(0, 0);
                },
                err => {
                    this.message = 'Card not found, Please insert';
                }
            );
        }
    }

    startProcess(response, processName, cardReaderName) {
        this.processName = processName;
        const cmsSyncRequest = this.buildCmsSyncRequest(response, processName);
        this.issuanceService.getUUID(processName, cmsSyncRequest).subscribe(
            uuid => {
                // start  process
                this.issuanceService.start(processName, uuid).subscribe(
                    issaunceStartRes => {
                        // send the commands to card
                        this.deviceClientService.execute(issaunceStartRes.body, cardReaderName).subscribe(
                            result => {
                                this.issuanceService.next(result.body).subscribe(
                                    res => {
                                        this.recurse(res.body, cardReaderName);
                                    },
                                    error => {
                                        this.isLoadingResults = false;
                                        console.log(error);
                                    }
                                );
                            },
                            error => {
                                this.isLoadingResults = false;
                                console.log(error);
                            }
                        );
                    },
                    error => {
                        this.isLoadingResults = false;
                        console.log(error);
                    }
                );
            },
            error => {
                this.isLoadingResults = false;
                console.log(error);
            }
        );
    }
    activate() {
        let n = 0;
        this.timer = setInterval(() => {
            if (this.flag && n === 0) {
                n++;
                this.checkConnections();
            }
        }, 1000);
    }
    checkConnections() {
        if (this.activationForm.controls.identityDevice.value === null) {
            this.translateService.get('issuance.connectReaderAndCard').subscribe(msg => {
                this.message = msg;
                this.errorDisplay = true;
                window.scrollTo(0, 0);
                return null;
            });
        } else {
            this.deviceClientService
                .getAtrByReaderName(this.activationForm.controls.identityDevice.value.urlEncodedcardReaderName)
                .subscribe(response => {
                    if (response.atr === '') {
                        this.translateService.get('issuance.cardNotFound').subscribe(message => {
                            this.errorDisplay = true;
                            this.message = message;
                            window.scrollTo(0, 0);
                            return;
                        });
                    } else {
                        this.startActivate(response.atr);
                    }
                });
        }
    }

    resendPinThroughMail() {
        this.isLoadingResults = true;
        // call rest API to send email
        this.manageIdentityDevicesService
            .sendPinOverMail(this.orgName, this.userId, this.device.uniqueIdentifier, this.userEmail)
            .subscribe(
                res => {
                    this.isLoadingResults = false;
                    if (res.status === 200) {
                        this.successMail = true;
                        this.translateService.get('manageIdentiyDevices.pinOverMailSentSuccess').subscribe(msg => {
                            this.successMailMsg = msg; //
                            setTimeout(() => {
                                this.successMail = false;
                            }, 15000);
                        });
                    }
                },
                err => {
                    this.isLoadingResults = false;
                }
            );
    }

    startActivate(atr) {
        this.isLoadingResults = true;
        this.errorDisplay = false;
        // removing the white spaces in atr
        const newAtr = atr.replace(/ /g, '');
        const cardReaderName = this.activationForm.controls.identityDevice.value.cardReaderName;
        // start identify process
        this.issuanceService.start('identify', newAtr).subscribe(
            identifyStartResponse => {
                // execute the commands to card
                identifyStartResponse.body.state = 0;
                this.deviceClientService.execute(identifyStartResponse.body, cardReaderName).subscribe(
                    identifyCommandsRes => {
                        this.issuanceService.next(identifyCommandsRes.body).subscribe(
                            identifyNextRes => {
                                let nextResponse = new Commands();
                                nextResponse = identifyNextRes.body;
                                if (nextResponse.step === 'done') {
                                    const cuid = identifyNextRes.body.entries[0].result.serial;
                                    if (cuid === this.device.uniqueIdentifier) {
                                        // start gpunlock process
                                        this.identifyProcessResponse = identifyNextRes;
                                        this.startProcess(identifyNextRes, 'gpunlock', cardReaderName);
                                    } else {
                                        window.scrollTo(0, 0);
                                        this.errorDisplay = true;
                                        this.isLoadingResults = false;
                                        this.message = 'Current device does not match with the selected device';
                                    }
                                } else {
                                    if (nextResponse.step === 'error' || nextResponse.stage === 'error') {
                                        this.translateService.get(`manageIdentiyDevices.activationFailed`).subscribe(message => {
                                            this.isLoadingResults = false;
                                            window.scrollTo(0, 0);
                                            this.message = message;
                                            this.errorDisplay = true;
                                        });
                                    }
                                    // return throwError(nextResponse.entries[0].message);
                                }
                            },
                            error => {
                                this.message = 'error while activation';
                                this.isLoadingResults = false;
                                console.log(error);
                            }
                        );
                    },
                    error => {
                        this.message = 'error while activation';
                        this.isLoadingResults = false;
                        console.log(error);
                    }
                );
            },
            error => {
                this.isLoadingResults = false;
                this.message = 'error while activation';
                console.log(error);
            }
        );
        clearInterval(this.timer);
    }
    recurse(res, cardReaderName) {
        if (res.stage !== 'done' && res.step !== 'error') {
            this.deviceClientService.execute(res, cardReaderName).subscribe(
                result1 => {
                    this.issuanceService.next(result1.body).subscribe(
                        response => {
                            this.recurse(response.body, cardReaderName);
                        },
                        error => {
                            this.isLoadingResults = false;
                            console.log(error);
                        }
                    );
                },
                error => {
                    this.isLoadingResults = false;
                    console.log(error);
                }
            );
        } else if (res.step === 'error' || res.stage === 'error') {
            this.isLoadingResults = false;
            if (this.processName === 'change-pin') {
                this.processName = 'gplock';
                this.startProcess(this.identifyProcessResponse, this.processName, cardReaderName);
                this.translateService.get(`manageIdentiyDevices.activationFailed`).subscribe(message => {
                    window.scrollTo(0, 0);
                    this.message = message;
                    if (res.entries[0].message.includes('_')) {
                        this.message = `${message}-` + '(Error Code: ' + res.entries[0].message + ')';
                    }
                    this.errorDisplay = true;
                });
            }
            this.translateService.get(`manageIdentiyDevices.activationFailed`).subscribe(message => {
                window.scrollTo(0, 0);
                this.message = res.entries[0].message + `. ${message}`;
                this.errorDisplay = true;
            });
        } else if (res.step === 'done' && res.stage === 'done') {
            if (this.processName === 'gpunlock') {
                this.processName = 'change-pin';
                this.startProcess(this.identifyProcessResponse, this.processName, cardReaderName);
            } else if (this.processName === 'change-pin') {
                // if fido2 enabled, then call reset-pin....
                if (this.isFido2Enabled) {
                    this.processName = 'reset-pin';
                    this.startProcess(this.identifyProcessResponse, this.processName, cardReaderName);
                } else {
                    this.translateService.get(`manageIdentiyDevices.activateSuccessMsg`).subscribe(message => {
                        this.subscription.unsubscribe();
                        clearInterval(this.timer);
                        this.message = message;
                        this.errorDisplay = false;
                        this.display = false;
                        this.isLoadingResults = false;
                        window.scrollTo(0, 0);
                    });
                }
            } else if (this.processName === 'reset-pin') {
                this.translateService.get(`manageIdentiyDevices.activateSuccessMsg`).subscribe(message => {
                    this.subscription.unsubscribe();
                    clearInterval(this.timer);
                    this.message = message;
                    this.errorDisplay = false;
                    this.display = false;
                    this.isLoadingResults = false;
                    window.scrollTo(0, 0);
                });
            }
        }
    }
    buildCmsSyncRequest(identifyNextRes, processName) {
        const cmsSyncRequest = new CmsSyncRequest();
        cmsSyncRequest.processName = processName;
        cmsSyncRequest.deviceType = 'permanent';
        cmsSyncRequest.device = {
            type: identifyNextRes.body.entries[0].result.type,
            serial: identifyNextRes.body.entries[0].result.serial,
            cardType: identifyNextRes.body.entries[0].result.cardType,
            cardProductName: identifyNextRes.body.entries[0].result.cardProductName
        };
        cmsSyncRequest.user = this.userName;
        cmsSyncRequest.userGroupXrefId = 1;
        cmsSyncRequest.orgName = this.orgName;
        cmsSyncRequest.wfName = this.workflowName;
        if (this.processName === 'reset-pin') {
            cmsSyncRequest.pin = this.activationForm.controls.newPin.value;
        } else {
            cmsSyncRequest.oldPin = this.activationForm.controls.pin.value;
            cmsSyncRequest.newPin = this.activationForm.controls.newPin.value;
        }
        cmsSyncRequest.deviceId = this.mappedDeviceID;
        return cmsSyncRequest;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.dataService.changeMIDObj(null);
        clearInterval(this.timer);
    }

    goHome() {
        this.router.navigate(['']);
    }
}
