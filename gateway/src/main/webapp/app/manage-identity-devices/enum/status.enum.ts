export const enum StatusEnum {
    PENDING_ACTIVATION = 'Pending Activation',
    ACTIVE = 'Active',
    SUSPENDED = 'Suspended',
    REVOKED = 'Revoked',
    INACTIVE = 'Inactive',
    ON_HOLD = 'OnHold',
    REMOVED = 'Removed'
}
