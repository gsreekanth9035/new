export const enum ActionEnum {
    CHANGE_FRIENDLY_NAME = 'ChangeFriendlyName',
    ACTIVATE = 'Activate',
    REPORT_INCIDENT = 'Report Incident',
    SUSPEND = 'Suspend',
    REVOKE = 'Revoke',
    REACTIVATE = 'Reactivate',
    CHANGE_DEVICE_PIN = 'ChangeDevicePIN',
    RESET_DEVICE_PIN = 'ResetDevicePIN',
    RENEW = 'Renew',
    REISSUE = 'Reissue',
    DELETE = 'Delete',
    INACTIVATE = 'Inactivate',
    REMOVE = 'Remove',
    SHOW_PUK = 'Show PUK',
    RESET_OTP = 'Reset Otp'
}
