export const enum DeviceTypeEnum {
    SMART_CARD = 'Smart Card',
    TOKEn = 'Token',
    MOBILE = 'Mobile',
    PLASTICE_CARD = 'Plastic Card'
}
