
export class DeviceCertificates {
    containerName: string;
    // checkBox: boolean;
    select: boolean;
    certIssuer: string;
    certStatus: string;
    expiryDate: string;
    renewalEligibility: string;
    action: string;
    disableCheckBox = false;
    checkBoxValue = false;
    constructor() { }
}
