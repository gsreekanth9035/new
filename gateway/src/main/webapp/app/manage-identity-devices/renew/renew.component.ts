import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from 'app/device-profile/data.service';
import { Device } from '../manage-identity-devices.model';
import { Principal, UserInfo } from 'app/core';
import { CardReader } from 'app/issuance/model/cardReader.model';
import { throwError, Subscription, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { IssuanceService } from 'app/issuance/issuance.service';
import { MatTableDataSource, MatPaginator, MatSort, MatCheckboxChange } from '@angular/material';
import { DeviceCertificates } from './deviceCerts.model';
import { ManageIdentityDevicesService } from '../manage-identity-devices.service';
import { SelectionModel } from '@angular/cdk/collections';
import { DatePipe } from '@angular/common';
import { Commands } from 'app/issuance/model/commands.model';
import { CmsSyncRequest } from 'app/issuance/model/cmsSyncRequest.model';
import { TranslateService } from '@ngx-translate/core';
import { DeviceClientService } from 'app/shared/util/device-client.service';

@Component({
    selector: 'jhi-renew',
    templateUrl: './renew.component.html',
    styleUrls: ['renew.scss']
})
export class RenewComponent implements OnInit, OnDestroy {
    renewForm: FormGroup;
    pinHide = true;
    device: Device;
    workflowName: string;
    orgName: string;
    errorDisplay = false;
    message: string;
    cardReaders: CardReader[] = [];
    subscription: Subscription = new Subscription();
    selectedCertList: DeviceCertificates[] = [];
    data: DeviceCertificates[] = [];
    flag = false;
    timer: any;
    intermediate = false;
    intermediateStyle = {};
    mappedDeviceID: any;
    identifyProcessResponse: any;
    processName: string;
    success = false;
    failure = false;
    display = true;
    progressClass = 'progress-bar bg-success';
    username: string;
    width = 0;
    errorMsg: string;
    isActionEligible = false;

    displayedColumns: string[] = ['select', 'Name', 'Issuer', 'Status', 'Expires', 'RenewalEligibility', 'Action'];
    dataSource: MatTableDataSource<DeviceCertificates>;
    selection = new SelectionModel<DeviceCertificates>(true, []);
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    selectedCardAtr: string;
    selectedCardName: string;
    constructor(
        private formBuilder: FormBuilder,
        private dataService: DataService,
        private principal: Principal,
        private issuanceService: IssuanceService,
        private manageIdentityDevicesService: ManageIdentityDevicesService,
        private datePipe: DatePipe,
        private translateService: TranslateService,
        private deviceClientService: DeviceClientService
    ) {
        this.buildRenewForm();
    }
    ngOnInit() {
        this.device = this.dataService.getCurrentMIdObj();
        this.username = this.device.userName;
        this.mappedDeviceID = this.device.id;
        this.orgName = this.principal.getLoggedInUser().organization;
        if (this.device.userFirstName !== undefined) {
            this.issuanceService.getWorkflowByUsername(this.device.userName).subscribe(workflow => {
                this.workflowName = workflow;
                console.log(this.workflowName);
                this.getUserCerts();
            });
        } else {
            this.workflowName = this.principal.getLoggedInUser().workflow;
            this.getUserCerts();
        }
        this.getReadersList();
    }
    getUserCerts() {
        this.manageIdentityDevicesService.getUserByName(this.username).subscribe(user => {
            console.log(this.workflowName);
            this.isActionEligible = false;
            this.manageIdentityDevicesService.renewCerts(user.id, this.device.id, this.orgName, this.workflowName).subscribe(certs => {
                certs.forEach(cert => {
                    // cert.expiryDate = this.datePipe.transform(cert.expiryDate, 'MMM d, y');
                    if (cert.action === 'None') {
                        cert.action = '';
                        cert.disableCheckBox = true;
                    } else {
                        this.isActionEligible = true;
                        cert.disableCheckBox = false;
                    }
                });
                this.data = certs;
                this.dataSource = new MatTableDataSource(this.data);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            });
        });
    }

    getReadersList() {
        this.subscription = timer(0, 2000)
            .pipe(switchMap(() => this.deviceClientService.getCardReaders()))
            .subscribe(
                readersList => {
                    const list = readersList.cardReaderList;
                    this.flag = false;
                    let updateList = false;
                    if (readersList.responseCode === 'DS0000') {
                        //     this.activationForm get('identityDevice')
                        const selectedReader = this.renewForm.controls['identityDevice'].value;
                        const cr = this.renewForm.controls['identityDevice'].value;
                        if (cr === null || cr === undefined) {
                            this.cardReaders = list;
                            this.renewForm.controls['identityDevice'].setValue(this.cardReaders[0]);
                            this.setCardName(this.cardReaders[0]);
                            return;
                        }
                        if (this.cardReaders.length === list.length || this.cardReaders.length > list.length) {
                            let isFound = false;
                            for (const c of this.cardReaders) {
                                const found = list.find(e => e.cardReaderName === c.cardReaderName);
                                if (found === undefined) {
                                    updateList = true;
                                } else {
                                    if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                    }
                                }
                            }
                            const card1 = list.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (card1 !== undefined) {
                                if (!isFound || this.selectedCardName === '' || card1.atr !== this.selectedCardAtr) {
                                    if (card1 !== undefined) {
                                        this.setCardName(card1);
                                    }
                                }
                            }
                        } else if (this.cardReaders.length < list.length) {
                            let isFound = false;
                            for (const c of list) {
                                const found = this.cardReaders.find(e => e.cardReaderName === c.cardReaderName);
                                if (found === undefined) {
                                    updateList = true;
                                } else {
                                    if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                    }
                                }
                            }
                            const card1 = list.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (card1 !== undefined) {
                                if (!isFound || this.selectedCardName === '' || card1.atr !== this.selectedCardAtr) {
                                    if (card1 !== undefined) {
                                        this.setCardName(card1);
                                    }
                                }
                            }
                        }
                        if (updateList) {
                            this.cardReaders = list;
                            const cardReader: CardReader = this.cardReaders.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (cardReader !== undefined) {
                                this.renewForm.controls['identityDevice'].setValue(cardReader);
                                if (cardReader.atr !== this.selectedCardAtr) {
                                    this.setCardName(cardReader);
                                }
                            } else {
                                this.renewForm.controls['identityDevice'].setValue(this.cardReaders[0]);
                                if (this.cardReaders[0].atr !== this.selectedCardAtr) {
                                    this.setCardName(this.cardReaders[0]);
                                }
                            }
                        }
                    } else {
                        this.renewForm.controls['identityDevice'].setValue(null);
                        this.cardReaders = [];
                        this.selectedCardAtr = '';
                        this.selectedCardName = '';
                        this.translateService.get('issuance.connectReaderAndCard').subscribe(msg => {
                            this.errorMsg = msg;
                            this.errorDisplay = true;
                        });
                        window.scrollTo(0, 0);
                    }
                    this.flag = true;
                },
                error => {
                    if (this.subscription) {
                        this.subscription.unsubscribe();
                    }
                    this.errorDisplay = true;
                    this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                        this.errorMsg = serviceErrMessage;
                        // Retrying
                        this.getReadersList();
                    });
                }
            );
    }
    setCardName(cardReader: CardReader) {
        const atr = cardReader.atr;
        if (atr.length !== 0) {
            this.errorDisplay = false;
            const newAtr = atr.replace(/ /g, '');
            this.issuanceService.getCardNameByAtr(newAtr).subscribe(name => {
                if (this.selectedCardName !== name) {
                    this.selectedCardAtr = atr;
                    this.selectedCardName = name;
                }
            });
        } else {
            this.translateService.get('issuance.cardNotFound').subscribe(
                msg => {
                    this.errorDisplay = true;
                    this.errorMsg = msg;
                    this.selectedCardAtr = '';
                    this.selectedCardName = '';
                    window.scrollTo(0, 0);
                },
                err => {
                    this.errorMsg = 'Card not found, Please insert';
                }
            );
        }
    }

    buildRenewForm() {
        this.renewForm = this.formBuilder.group({
            identityDevice: ['', Validators.required],
            pin: ['', [Validators.required, Validators.pattern('^([0-9]{6,8})$')]],
            checked: []
        });
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.data.length;
        const numRowsMinusExcluded = this.dataSource.data.filter(row => !row.disableCheckBox).length;

        return numSelected === numRowsMinusExcluded;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected()
            ? this.selection.clear()
            : this.dataSource.data.forEach(row => {
                  if (!row.disableCheckBox) {
                      this.selection.select(row);
                  }
              });
    }
    isIntermediate() {
        if (this.selection.hasValue() && !this.isAllSelected()) {
            this.intermediateStyle = { border: '1px #00bcd4 solid; border-radius: 2px' };
        }
        return this.selection.hasValue() && !this.isAllSelected();
    }
    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: DeviceCertificates): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        // return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
    }
    startProcess(response, processName, cardReaderName) {
        this.processName = processName;
        const cmsSyncRequest = this.buildCmsSyncRequest(response, processName);
        this.issuanceService.getUUID(processName, cmsSyncRequest).subscribe(
            uuid => {
                // start  process
                this.issuanceService.start(processName, uuid).subscribe(
                    issaunceStartRes => {
                        this.width = this.width + 10;
                        // send the commands to card
                        this.deviceClientService.execute(issaunceStartRes.body, cardReaderName).subscribe(
                            result => {
                                this.issuanceService.next(result.body).subscribe(
                                    res => {
                                        this.recurse(res.body, cardReaderName);
                                    },
                                    error => {
                                        this.progressClass = 'progress-bar bg-danger';
                                        this.failure = true;
                                        this.success = false;
                                        this.message = error;
                                        console.log(error);
                                    }
                                );
                            },
                            error => {
                                console.log(error);
                                this.progressClass = 'progress-bar bg-danger';
                                this.failure = true;
                                this.success = false;
                                this.message = error;
                            }
                        );
                    },
                    error => {
                        console.log(error);
                        this.progressClass = 'progress-bar bg-danger';
                        this.failure = true;
                        this.success = false;
                        this.message = error;
                    }
                );
            },
            error => {
                console.log(error);
                this.progressClass = 'progress-bar bg-danger';
                this.failure = true;
                this.success = false;
                this.message = error;
            }
        );
    }

    startRenew() {
        let n = 0;
        this.timer = setTimeout(() => {
            if (this.flag && n === 0) {
                n++;
                this.checkConnections();
            }
        }, 1000);
    }
    checkConnections() {
        if (this.renewForm.controls.identityDevice.value === null) {
            this.translateService.get('issuance.connectReaderAndCard').subscribe(msg => {
                this.message = msg;
                this.errorDisplay = true;
                window.scrollTo(0, 0);
                return null;
            });
        } else {
            this.deviceClientService
                .getAtrByReaderName(this.renewForm.controls.identityDevice.value.urlEncodedcardReaderName)
                .subscribe(response => {
                    if (response.atr === '') {
                        this.translateService.get('issuance.cardNotFound').subscribe(message => {
                            this.errorDisplay = true;
                            this.message = message;
                            window.scrollTo(0, 0);
                            return;
                        });
                    } else {
                        this.renewOrIssueCerts(response.atr);
                    }
                });
        }
    }

    renewOrIssueCerts(atr) {
        // write renew functionality here
        console.log('selected certs list:::', this.selection.selected);
        this.selectedCertList = this.selection.selected;
        this.errorDisplay = false;

        if (this.selectedCertList.length === 0) {
            this.errorDisplay = true;
            this.errorMsg = 'No certificates selected for Renew';
            window.scrollTo(0, 0);
            return;
        }
        this.display = false;
        this.progressClass = 'progress-bar bg-success';
        // removing the white spaces in atr
        const newAtr = atr.replace(/ /g, '');
        const cardReaderName = this.renewForm.controls['identityDevice'].value.cardReaderName;
        this.processName = 'verify-pin';
        // start identify process
        this.issuanceService.start('identify', newAtr).subscribe(
            identifyStartResponse => {
                // execute the commands to card
                const deviceServiceReq1 = identifyStartResponse.body;
                deviceServiceReq1.state = 0;
                this.deviceClientService.execute(deviceServiceReq1, cardReaderName).subscribe(
                    identifyCommandsRes => {
                        this.issuanceService.next(identifyCommandsRes.body).subscribe(
                            identifyNextRes => {
                                let nextResponse = new Commands();
                                nextResponse = identifyNextRes.body;
                                if (nextResponse.step === 'done') {
                                    this.width = 5;
                                    const cuid = identifyNextRes.body.entries[0].result.serial;
                                    if (cuid === this.device.uniqueIdentifier) {
                                        // start Renew process
                                        this.identifyProcessResponse = identifyNextRes;
                                        this.startProcess(identifyNextRes, this.processName, cardReaderName);
                                    } else {
                                        window.scrollTo(0, 0);
                                        this.errorDisplay = true;
                                        this.message = 'Connected device is not matched with the selected device';
                                    }
                                } else {
                                    if (nextResponse.step === 'error' || nextResponse.stage === 'error') {
                                        this.progressClass = 'progress-bar bg-danger';
                                        this.failure = true;
                                        this.success = false;
                                        this.message =
                                            nextResponse.entries[0].message !== null
                                                ? nextResponse.entries[0].message + '. Renewal failed'
                                                : 'Renewal failed';
                                        return;
                                    }
                                    return throwError(nextResponse.entries[0].message);
                                }
                            },
                            error => {
                                console.log(error);
                                this.progressClass = 'progress-bar bg-danger';
                                this.failure = true;
                                this.success = false;
                                this.message = 'Error while Renewal process';
                            }
                        );
                    },
                    error => {
                        console.log(error);
                        this.progressClass = 'progress-bar bg-danger';
                        this.failure = true;
                        this.success = false;
                        this.message = error;
                    }
                );
            },
            error => {
                console.log(error);
                this.progressClass = 'progress-bar bg-danger';
                this.failure = true;
                this.success = false;
                this.message = error;
            }
        );
        clearInterval(this.timer);
    }
    recurse(res, cardReaderName) {
        if (res.stage !== 'done' && res.step !== 'error') {
            this.width = this.width + 5;
            this.deviceClientService.execute(res, cardReaderName).subscribe(
                result1 => {
                    this.message = res.stageStatus;
                    this.issuanceService.next(result1.body).subscribe(
                        response => {
                            if (response.body !== null) {
                                this.recurse(response.body, cardReaderName);
                            } else {
                                this.progressClass = 'progress-bar bg-danger';
                                this.failure = true;
                                this.success = false;
                                this.message = 'Error while renew process';
                            }
                        },
                        error => {
                            console.log(error);
                            this.progressClass = 'progress-bar bg-danger';
                            this.failure = true;
                            this.success = false;
                            this.message = error;
                        }
                    );
                },
                error => {
                    console.log(error);
                    this.progressClass = 'progress-bar bg-danger';
                    this.failure = true;
                    this.success = false;
                    this.message = error;
                }
            );
        } else if (res.step === 'error' || res.stage === 'error') {
            this.progressClass = 'progress-bar bg-danger';
            this.failure = true;
            this.success = false;
            this.translateService.get(`manageIdentiyDevices.renewalFailed`).subscribe(message => {
                if (res.entries[0].message.includes('_')) {
                    this.message = `${message}-` + '(Error Code: ' + res.entries[0].message + ')';
                } else {
                    this.message = message;
                }
            });
            return throwError(res.step);
        } else if (res.step === 'done' && res.stage === 'done') {
            if (this.processName === 'verify-pin') {
                this.processName = 'renewal';
                this.startProcess(this.identifyProcessResponse, this.processName, cardReaderName);
            } else if (this.processName === 'renewal') {
                this.translateService.get('manageIdentiyDevices.renew.renewSuccessMsg').subscribe(
                    message => {
                        this.width = 100;
                        this.message = message;
                        this.success = true;
                        this.subscription.unsubscribe();
                        window.scrollTo(0, 0);
                    },
                    err => {
                        this.width = 100;
                        this.message = 'Renewal process completed successfully';
                        this.success = true;
                        this.subscription.unsubscribe();
                        window.scrollTo(0, 0);
                    }
                );
            }
        }
    }
    buildCmsSyncRequest(identifyNextRes, processName) {
        const cmsSyncRequest = new CmsSyncRequest();
        cmsSyncRequest.processName = processName;
        cmsSyncRequest.deviceType = 'permanent';
        cmsSyncRequest.device = {
            type: identifyNextRes.body.entries[0].result.type,
            serial: identifyNextRes.body.entries[0].result.serial,
            cardType: identifyNextRes.body.entries[0].result.cardType,
            cardProductName: identifyNextRes.body.entries[0].result.cardProductName
        };
        cmsSyncRequest.user = this.username;
        cmsSyncRequest.userGroupXrefId = 1;
        cmsSyncRequest.orgName = this.orgName;
        cmsSyncRequest.wfName = this.workflowName;
        cmsSyncRequest.pin = this.renewForm.controls.pin.value;
        cmsSyncRequest.deviceId = this.mappedDeviceID;
        cmsSyncRequest.deviceCertsForRenew = this.selectedCertList;
        return cmsSyncRequest;
    }

    /**  onRowCheckChange(event: MatCheckboxChange, cert: DeviceCertificates) {
        console.log(event.checked, 'cert::', cert);
        this.selectedCertList = [];
        if (event.checked) {
            this.selectedCertList.push(cert);
            if (this.selectedCertList.length === this.data.length) {
                console.log('full list');
                 if (!cert.disableCheckBox) {
                    this.renewForm.get('checkBox').setValue(true);
                 }
            }
        } else {
            this.selectedCertList = this.selectedCertList.filter(elment => elment.containerName !== cert.containerName);
            console.log('after removed', this.selectedCertList.filter(elment => elment.containerName !== cert.containerName),
            this.selectedCertList.length);
            if (this.selectedCertList.length < this.data.length) {
                console.log('empty list');
                this.renewForm.get('checkBox').setValue(false);
            }
        }
        console.log('list:::', this.selectedCertList);
    }
    onCheckChange(event: MatCheckboxChange) {
       console.log(event.checked);
       if (event.checked) {
           const newData = this.data.filter(cert => cert.disableCheckBox === false);
            console.log('newData::::', newData);
           newData.forEach(element => {
            element.checkBoxValue = true;
           });
           this.selectedCertList = newData;
       } else {
        this.renewForm.get('checked').setValue(false);
        this.selectedCertList = new Array<DeviceCertificates>();
        console.log('in else:::', this.selectedCertList);
       }
       console.log('list:::', this.selectedCertList);
    } */
    refresh() {
        window.location.reload();
    }
    retry(): void {
        this.failure = false;
        this.success = false;
        this.display = true;
        this.message = '';
        this.progressClass = '';
        this.width = 0;
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.dataService.changeMIDObj(null);
        clearInterval(this.timer);
    }
}
