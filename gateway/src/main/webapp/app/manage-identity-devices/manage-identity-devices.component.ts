import { Component, OnInit, OnDestroy, Inject, ElementRef, HostListener, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTable, MAT_DIALOG_DATA } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Principal } from 'app/core';
import { CredentialRepresentation, ManageIdentityDevicesService } from './manage-identity-devices.service';
import { Device, MobileCredential } from './manage-identity-devices.model';
import { CardReader, CardReaderList } from 'app/issuance/model/cardReader.model';
import { Subscription, timer } from 'rxjs';
import { IssuanceService } from 'app/issuance/issuance.service';
import { DataService, Data } from 'app/device-profile/data.service';
import { TranslateService } from '@ngx-translate/core';
import { StatusEnum } from './enum/status.enum';
import { ActionEnum } from './enum/actions.enum';
import { switchMap } from 'rxjs/operators';
import { DeviceTypeEnum } from './enum/device-types.enum';
import { DeviceClientService } from 'app/shared/util/device-client.service';
import { FormControl, Validators } from '@angular/forms';
import { User } from 'app/enroll/user.model';

declare var $: any;
@Component({
    selector: 'jhi-manage-identity-devices',
    templateUrl: './manage-identity-devices.component.html',
    styleUrls: ['manage-identity-devices.scss']
})
export class ManageIdentityDevicesComponent implements OnInit, OnDestroy {
    displayedColumns: string[] = ['position', 'type', 'userLabel'];
    dataSource: CredentialRepresentation[] = [];
    @ViewChild('table') table: MatTable<CredentialRepresentation[]>;

    public isShowPUKSelected = false;
    public dialogMsg = '';
    public actionType = '';
    isLoadingResults = false;
    isMobileSelected = false;
    userMappedDevices: Device[] = [];
    devices: Device[] = [];
    mobileCredentials: MobileCredential[] = [];
    user: User;
    userName = '';
    userFirstName: string;
    editRowId: any = '';
    userId: number;
    currentFriendlyName = new FormControl('');
    subscription: Subscription = new Subscription();
    cardReaders: CardReader[] = [];
    display = true;
    error = false;
    message: string;
    list: CuidWithReader[] = [];
    displayIcon = 'tablet_mac';
    actions: Array<String>;
    cuidMatched = false;
    data: Data;
    mobileId = '';
    role: string;
    selectedDevice = new Device();
    mobileDeviceMsg: string;
    pageLoaded = false;
    hasSmartCardDevices = false;
    showToolTip = false;
    userEnrolled = false;
    isSelfService = true;
    hasUserRoleWithOtherRoles = false;
    organizationName: string;
    isPairMobileDevicePermissionEnabled = false;
    isIssuanceEnabled = false;
    isPendingApproval = false;
    isEnrollmentEnabled = false;
    issuanceAllowed = false;
    maxAllowedDevices;
    issuedDevices;
    userFirstAndLastName: string;
    groupName: string;
    identityType: string;
    userKcId: string;

    showCredentials = true;
    constructor(
        private router: Router,
        private principal: Principal,
        private manageIdentityDevicesService: ManageIdentityDevicesService,
        private issuanceService: IssuanceService,
        private dataService: DataService,
        public dialog: MatDialog,
        private translateService: TranslateService,
        private activatedRoute: ActivatedRoute,
        private el: ElementRef,
        private deviceClientService: DeviceClientService
    ) {
        if (this.principal.getLoggedInUser().authorities.includes('Menu_Pair_Mobile_Device')) {
            this.isPairMobileDevicePermissionEnabled = true;
        }
        if (this.principal.getLoggedInUser().authorities.includes('Menu_Issuance')) {
            this.isIssuanceEnabled = true;
        }
        if (this.principal.getLoggedInUser().authorities.includes('Menu_Enrollment')) {
            this.isEnrollmentEnabled = true;
        }
        this.hasUserRoleWithOtherRoles = this.principal.getLoggedInUser().hasUserRoleWithOtherRoles;
    }
    ngOnInit() {
        this.isLoadingResults = true;
        this.role = 'ROLE_USER';
        this.organizationName = this.principal.getLoggedInUser().organization;
        this.data = this.dataService.getCurrentObj();
        if (this.data !== null) {
            this.userFirstAndLastName = this.data.userFirstName + ' ' + this.data.userLastName;
            this.groupName = this.data.groupName;
            this.identityType = this.data.identityType;
            this.userName = this.data.userName;
            this.userFirstName = this.data.userFirstName;
            this.isSelfService = false;
            this.userKcId = this.data.userKcId;
            if (this.principal.getLoggedInUser().username !== this.userName) {
                // && this.principal.getLoggedInUser().roles.includes('role_operator')
                this.role = this.principal.getLoggedInUser().roles.filter(e => e.toLowerCase() !== 'role_user')[0];
                this.isSelfService = false;
            }
            this.userEnrolled = true;
            this.getMaxAllowedDevices();
        } else {
            this.userFirstAndLastName = this.principal.getLoggedInUser().firstName + ' ' + this.principal.getLoggedInUser().lastName;
            this.groupName = this.principal.getLoggedInUser().groups[0].name;
            this.isSelfService = true;
            this.userName = this.principal.getLoggedInUser().username;
            this.userKcId = this.principal.getLoggedInUser().sub;
            this.getMaxAllowedDevices();
        }
    }

    getMaxAllowedDevices() {
        this.issuanceService.getWorkflowGeneralInfoByName(this.userName).subscribe(
            workflow => {
                if (workflow !== null) {
                    this.identityType = workflow.organizationIdentities.identityTypeName;
                    if (workflow.allowedDevicesApplicable) {
                        this.maxAllowedDevices = workflow.allowedDevices;
                    } else {
                        this.maxAllowedDevices = 'NA';
                    }
                } else {
                    this.pageLoaded = true;
                    this.userEnrolled = false;
                }
                this.getUserDevices();
            },
            err => {
                this.isLoadingResults = false;
            }
        );
    }
    getUserDevices() {
        this.devices = [];
        this.mobileCredentials = [];
        this.manageIdentityDevicesService.getUserByName(this.userName).subscribe(
            user => {
                if (user !== null && user.id !== null) {
                    this.issuedDevices = this.issuanceService.getIssuedDevices(user.id).subscribe(
                        res => {
                            this.issuedDevices = res;
                            if (user.status === 'PENDING_ENROLLMENT' || user.status === 'ENROLLMENT_IN_PROGRESS') {
                                this.pageLoaded = true;
                                this.userEnrolled = false;
                                this.isLoadingResults = false;
                                return;
                            }
                            if (user.status === 'PENDING_APPROVAL') {
                                this.pageLoaded = true;
                                this.userEnrolled = true;
                                this.isPendingApproval = true;
                            }
                            this.userEnrolled = true;
                            this.user = user;
                            this.userId = user.id;
                            let count = 0;
                            this.manageIdentityDevicesService.getDevicesOfUser(user.id, this.role).subscribe(
                                devices => {
                                    if (devices !== null) {
                                        this.getUserCredentialsList();
                                        this.userMappedDevices = devices;
                                        for (const element of this.userMappedDevices) {
                                            this.cuidMatched = false;
                                            const userDevIdentity = new Device();
                                            userDevIdentity.id = element.id;
                                            userDevIdentity.friendlyName = element.friendlyName;
                                            userDevIdentity.type = element.type;
                                            userDevIdentity.devProfName = element.devProfName;
                                            userDevIdentity.uniqueIdentifier = element.uniqueIdentifier;
                                            userDevIdentity.status = element.userDevices[0].status;
                                            userDevIdentity.health = userDevIdentity.status.toLowerCase();
                                            userDevIdentity.displayIcon = '';
                                            userDevIdentity.roleDeviceActions = element.roleDeviceActions;
                                            // element.deviceConnectedActions.push('Read');
                                            userDevIdentity.deviceConnectedActions = element.deviceConnectedActions;
                                            userDevIdentity.deviceNotConnectedActions = element.deviceNotConnectedActions;
                                            userDevIdentity.uimsId = element.uimsId;
                                            userDevIdentity.idForToolTip = 'device' + count++;
                                            userDevIdentity.productName = element.productName;
                                            userDevIdentity.deviceManufacturerName = element.deviceManufacturerName;
                                            if (element.type === DeviceTypeEnum.SMART_CARD) {
                                                userDevIdentity.cardType = element.cardType;
                                                userDevIdentity.idForCuid = 'cuid' + count++;
                                                userDevIdentity.connection = 'Not Connected';
                                                // element.deviceNotConnectedActions.push('Read');
                                                userDevIdentity.actions = element.deviceNotConnectedActions;
                                                this.hasSmartCardDevices = true;
                                            } else if (element.type === DeviceTypeEnum.MOBILE) {
                                                //   userDevIdentity.idForToolTip = 'mobile' + count++;
                                                userDevIdentity.operatingSystem = element.operatingSystem;
                                                userDevIdentity.osVersion = element.osVersion;
                                                userDevIdentity.deviceModelName = element.deviceModelName;
                                                userDevIdentity.appVersion = element.appVersion;
                                                // for mobile there is no status like connected and not connected
                                                userDevIdentity.actions = element.deviceConnectedActions;
                                            } else if (element.type === DeviceTypeEnum.PLASTICE_CARD) {
                                                userDevIdentity.actions = element.deviceNotConnectedActions;
                                            }
                                            this.devices.push(userDevIdentity);
                                        }
                                        if (this.hasSmartCardDevices === true) {
                                            this.subscription = timer(0, 2000)
                                                .pipe(switchMap(() => this.getDeviceConnectionStatus()))
                                                .subscribe(response => {});
                                        }
                                        if (this.data !== null) {
                                            const device = this.data.device;
                                            if (device !== undefined) {
                                                if (device.isMobileSelected) {
                                                    this.isMobileSelected = true;
                                                    const dev = this.devices.find(e => e.id === device.id);
                                                    this.selectedDevice = dev;
                                                    this.refresh();
                                                }
                                            }
                                        }
                                        this.pageLoaded = true;
                                        this.isLoadingResults = false;
                                    } else {
                                        this.isLoadingResults = false;
                                        this.pageLoaded = true;
                                    }
                                },
                                error => {
                                    this.isLoadingResults = false;
                                    console.log(error);
                                }
                            );
                        },
                        err => {
                            this.isLoadingResults = false;
                        }
                    );
                } else {
                    this.pageLoaded = true;
                    this.userEnrolled = false;
                }
            },
            err => {
                this.pageLoaded = true;
                this.isLoadingResults = false;
            }
        );
    }

    getUserCredentialsList() {
        this.manageIdentityDevicesService.getUserCredentials(this.userKcId).subscribe((res: CredentialRepresentation[]) => {
            this.dataSource = res;
        });
    }
    arrowUpEvent(index: number, element: CredentialRepresentation) {
        if (index - 1 === 0) {
            this.manageIdentityDevicesService.moveCredentialToFirst(this.userKcId, element.id).subscribe(res => {
                if (res.status === 204) {
                    this.moveArrayItemToNewIndex(index, index - 1);
                }
            });
        } else {
            const newPreviousCredential = this.dataSource[index - 2];
            if (newPreviousCredential !== null || newPreviousCredential !== undefined) {
                this.manageIdentityDevicesService
                    .moveCredentialAfterAnotherCredential(this.userKcId, element.id, newPreviousCredential.id)
                    .subscribe(res => {
                        if (res.status === 204) {
                            this.moveArrayItemToNewIndex(index, index - 1);
                        }
                    });
            }
        }
    }

    arrowDownEvent(index: number, element: CredentialRepresentation) {
        const newPreviousCredential = this.dataSource[index + 1];
        if (newPreviousCredential !== null || newPreviousCredential !== undefined) {
            this.manageIdentityDevicesService
                .moveCredentialAfterAnotherCredential(this.userKcId, element.id, newPreviousCredential.id)
                .subscribe(res => {
                    if (res.status === 204) {
                        this.moveArrayItemToNewIndex(index, index + 1);
                    }
                });
        }
    }

    moveArrayItemToNewIndex(oldIndex, newIndex) {
        if (newIndex >= this.dataSource.length) {
            var k = newIndex - this.dataSource.length + 1;
            while (k--) {
                this.dataSource.push(undefined);
            }
        }
        this.dataSource.splice(newIndex, 0, this.dataSource.splice(oldIndex, 1)[0]);
        this.table.renderRows();
    }
    async getDeviceConnectionStatus() {
        const cuidWithReaderList = await this.getCuidList();
        const deviceList = this.devices.filter(e => e.type === DeviceTypeEnum.SMART_CARD);
        if (deviceList.length !== 0 && !this.isMobileSelected) {
            deviceList.forEach(element => {
                this.cuidMatched = false;
                const connectionElement = document.getElementById(`${element.id}`);
                if (cuidWithReaderList.length !== 0) {
                    cuidWithReaderList.forEach(cuid => {
                        if (!this.cuidMatched && connectionElement !== null) {
                            if (cuid.serial === element.uniqueIdentifier) {
                                connectionElement.innerHTML = 'Connected';
                                (document.getElementById(`${element.id}`) as HTMLElement).style.color = '#17d6e0';
                                element.deviceType = cuid.deviceType;
                                element.cardReader = cuid.crdReader;
                                this.cuidMatched = true;
                                element.actions = element.deviceConnectedActions;
                            } else {
                                this.changeStatusToNotConnected(connectionElement, element);
                                this.cuidMatched = false;
                            }
                        }
                    });
                } else {
                    this.changeStatusToNotConnected(connectionElement, element);
                    this.cuidMatched = false;
                }
            });
        }
    }

    async getCuidList() {
        this.list = [];
        const readersList = <CardReaderList>await this.deviceClientService.getCardReaders().toPromise();
        if (readersList.responseCode === 'DS0000') {
            for (const reader of readersList.cardReaderList) {
                if (reader.atr !== '') {
                    if (reader.atr.replace(/ /g, '').length !== 0) {
                        const idRes = <any>await this.issuanceService.start('identify', reader.atr.replace(/ /g, '')).toPromise();
                        idRes.body.state = 0;
                        const exRes = <any>await this.deviceClientService.execute(idRes.body, reader.cardReaderName).toPromise();
                        const nxRes = <any>await this.issuanceService.next(exRes.body).toPromise();
                        if (nxRes.body !== null) {
                            if (nxRes.body.step === 'done') {
                                const serial1 = nxRes.body.entries[0].result.serial;
                                const crd = new CardReader();
                                crd.atr = reader.atr.replace(/ /g, '');
                                crd.cardReaderName = reader.cardReaderName;
                                this.list.push({ serial: serial1, crdReader: crd, deviceType: nxRes.body.entries[0].result.type });
                            }
                        }
                    }
                }
            }
        }
        return this.list;
    }
    changeStatusToNotConnected(connectionElement, element: Device) {
        connectionElement.innerHTML = 'Not Connected';
        (document.getElementById(`${element.id}`) as HTMLElement).style.color = 'orange';
        element.actions = element.deviceNotConnectedActions;
    }

    onMobileSelected(device: Device) {
        this.isLoadingResults = true;
        this.subscription.unsubscribe();
        this.selectedDevice = device;
        device.mobileCredentials = [];
        this.mobileCredentials = [];
        this.isMobileSelected = true;
        let count = 0;
        this.manageIdentityDevicesService
            .getUserMobileCredentials(this.userId, device.uniqueIdentifier, this.organizationName, this.role)
            .subscribe(
                mobileCredentials => {
                    const mobCred: MobileCredential[] = [];
                    mobileCredentials.forEach(mobileCred => {
                        mobileCred.status = mobileCred.status.toLowerCase();
                        mobileCred.mobileCredIdToolTip = 'mobileCred' + count++;
                        mobileCred.pushVerifyToolTipId = 'pushVerify' + count++;
                        mobileCred.softOTPToolTipId = 'softOTP' + count++;
                        if (device.status === StatusEnum.SUSPENDED || device.status === StatusEnum.REVOKED) {
                            mobileCred.actions = [];
                        } else {
                            // if (mobileCred.softOTP) {
                            //     // keep reset otp action
                            // } else {
                            //     // remove reset otp action
                            //     const actions: Array<String> = mobileCred.deviceConnectedActions.filter(e => e.toLocaleLowerCase() !== 'reset otp');
                            //     mobileCred.deviceConnectedActions = actions;
                            // }
                            mobileCred.actions = mobileCred.deviceConnectedActions;
                        }
                        mobCred.push(mobileCred);
                    });
                    this.mobileCredentials = mobCred;
                    device.mobileCredentials = this.mobileCredentials;
                    if (this.mobileCredentials.length === 0) {
                        this.mobileDeviceMsg = 'No visual identities are mapped to this device';
                    }
                    this.isLoadingResults = false;
                },
                error => {
                    this.isLoadingResults = false;
                }
            );
    }

    onClick(device: Device, mobileCredential: MobileCredential, action: string) {
        this.isShowPUKSelected = false;
        device.actionType = '';
        device.userName = this.userName;
        device.isMobileSelected = false;
        device.userId = this.user.id;
        device.userEmail = this.user.userAttribute.email;
        device.uniqueIdentifier = device.uniqueIdentifier;
        if (this.data !== null) {
            device.userFirstName = this.data.userFirstName;
        }
        if (device.type === DeviceTypeEnum.MOBILE) {
            if (action !== ActionEnum.REACTIVATE && action !== ActionEnum.REMOVE) {
                if (mobileCredential !== null) {
                    const deviceObj = new Device();
                    deviceObj.isMobileSelected = true;
                    deviceObj.id = device.id;
                    deviceObj.type = device.type;
                    deviceObj.userName = this.userName;
                    deviceObj.mobileCredentials.push(mobileCredential);
                    this.dataService.changeMIDObj(deviceObj);
                    this.router.navigate([`../${action.replace(/ /g, '')}`], { relativeTo: this.activatedRoute });
                } else {
                    this.dataService.changeMIDObj(device);
                    this.router.navigate([`../${action.replace(/ /g, '')}`], { relativeTo: this.activatedRoute });
                }
            } else if (action === ActionEnum.REACTIVATE) {
                this.dataService.changeMIDObj(device);
                this.actionType = ActionEnum.REACTIVATE;
                const deviceObj = new Device();
                deviceObj.type = device.type;
                deviceObj.actionType = ActionEnum.REACTIVATE;
                if (mobileCredential !== null) {
                    deviceObj.mobileCredentials.push(mobileCredential);
                    this.translateService.get('manageIdentiyDevices.reactivateConfirmMsg').subscribe(message => {
                        this.dialogMsg = message;
                        this.openDialog(deviceObj);
                    });
                } else {
                    this.translateService.get('manageIdentiyDevices.reactivateConfirmMsg').subscribe(message => {
                        this.dialogMsg = message;
                        this.openDialog(device);
                    });
                }
            } else if (action === ActionEnum.REMOVE) {
                this.dataService.changeMIDObj(device);
                this.actionType = ActionEnum.REMOVE;
                const deviceObj = new Device();
                deviceObj.type = device.type;
                deviceObj.actionType = ActionEnum.REMOVE;
                deviceObj.uniqueIdentifier = device.uniqueIdentifier;
                deviceObj.userId = this.userId;
                if (mobileCredential !== null) {
                    deviceObj.mobileCredentials.push(mobileCredential);
                    this.translateService.get('manageIdentiyDevices.removeConfirmMsg').subscribe(message => {
                        this.dialogMsg = message;
                        this.openDialog(deviceObj);
                    });
                } else {
                    this.translateService.get('manageIdentiyDevices.removeConfirmMsg').subscribe(message => {
                        this.dialogMsg = message;
                        this.openDialog(device);
                    });
                }
            }
        } else {
            this.dataService.changeMIDObj(device);
            if (action === ActionEnum.REACTIVATE) {
                this.translateService.get('manageIdentiyDevices.reactivateConfirmMsg').subscribe(message => {
                    this.dialogMsg = message;
                    this.actionType = 'Reactivate';
                    this.openDialog(device);
                });
            } else if (action === ActionEnum.REMOVE) {
                this.translateService.get('manageIdentiyDevices.removeConfirmMsg').subscribe(message => {
                    this.dialogMsg = message;
                    this.actionType = 'Remove';
                    this.openDialog(device);
                });
            } else if (action === ActionEnum.SHOW_PUK) {
                this.isShowPUKSelected = true;
                // get puk pin from db to selected device(smart card)
                this.manageIdentityDevicesService.getPUK(device.id, this.userId).subscribe(puk => {
                    this.dialogMsg = `PUK PIN: ${puk}`;
                    this.openDialog(device);
                });
            } else {
                if (action.toLocaleLowerCase() === 'read id') {
                    this.router.navigate([`/idreader`]);
                } else {
                    this.router.navigate([`../${action.replace(/ /g, '')}`], { relativeTo: this.activatedRoute });
                }
            }
        }
    }

    edit(device) {
        this.editRowId = device.id;
        this.currentFriendlyName.setValue(device.friendlyName);
    }
    saveFriendlyName(device: Device) {
        const deviceObj: Device = device;
        deviceObj.friendlyName = this.currentFriendlyName.value;
        if (device.id !== undefined || device.id !== null) {
            device.actionType = ActionEnum.CHANGE_FRIENDLY_NAME;
            this.manageIdentityDevicesService.manageDevice(this.userId, device).subscribe(result => {
                if (result.status === 200) {
                    this.editRowId = '';
                }
            });
        } else {
            // devie is not existed
        }
    }
    cancelEdit(device) {
        this.editRowId = '';
        this.currentFriendlyName.setValue(device.friendlyName);
    }

    getColor(health) {
        switch (health) {
            case 'active':
                return '#5cb85c';
            case 'expiringSoon':
                return 'orange';
            case 'pending activation':
                return 'orange';
            case 'inactive':
                return 'orange';
            case 'suspended':
                return 'orange';
            case 'revoked':
                return 'red';
            case 'expired':
                return 'red';
            case 'onhold':
                return 'orange';
            case 'removed':
                return 'red';
        }
    }
    getStatusColor(connection) {
        switch (connection) {
            case 'Connected':
                return '#17d6e0';
            case 'Online':
                return '#17d6e0';
            case 'Not Connected':
                return 'orange';
            case 'Offline':
                return 'orange';
        }
    }
    getTooltipMsg(health) {
        switch (health) {
            case 'active':
                return 'Device is healthy';
            case 'expiringSoon':
                return 'Device is expiring soon.';
            case 'pending activation':
                return 'Device is not active.';
            case 'inactive':
                return 'Device is not active.';
            case 'suspended':
                return 'Device is suspended.';
            case 'revoked':
                return 'Device is revoked.';
            case 'expired':
                return 'Device is revoked or expired.';
            case 'onhold':
                return 'Reported an incident';
            case 'removed':
                return 'Device is removed';
        }
    }
    async openAddIdentityDeviceDialog() {
        const msg = await (<any>this.translateService.get('manageIdentiyDevices.addIdentityDevices').toPromise());
        this.user.identityType = this.identityType;
        const dialogRef = this.dialog.open(AddIdentityDeviceDialogComponent, {
            autoFocus: false,
            width: '900px',
            data: {
                headerText: msg,
                user: this.user,
                isIssuancePermissionEnabled: this.isIssuanceEnabled,
                isMobilePermissionEnabled: this.isPairMobileDevicePermissionEnabled
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result !== undefined) {
                if (this.isSelfService) {
                    const data = new Data();
                    data.workflow = this.user.workflow;
                } else {
                    this.data.workflow = this.user.workflow;
                }
                if (result.deviceProfileConfig.isMobileId) {
                    this.router.navigate(['/pairmobile']);
                } else {
                    this.dataService.changeIdentityDevice(result);
                    this.router.navigate(['/issuance']);
                }
            } else {
                this.actionType = ' ';
            }
        });
    }
    openDialog(device: Device): void {
        const dialogRef = this.dialog.open(ReActivateDialogComponent, {
            autoFocus: false,
            width: '400px',
            data: {
                isShowPUK: this.isShowPUKSelected,
                dialogMsg: this.dialogMsg
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result === 'OK') {
                if (device.type === DeviceTypeEnum.MOBILE) {
                    if (this.actionType === ActionEnum.REACTIVATE) {
                        this.isLoadingResults = true;
                        device.actionType = ActionEnum.REACTIVATE;
                        if (this.isMobileSelected) {
                            this.updateMobileCredential(device, 'reactivateMobileSuccessMsg', 'reactivateMobileErrorMsg');
                        } else {
                            this.manageDevice(device, 'reactivateSuccessMsg', 'reactivateErrorMsg');
                        }
                    } else if (this.actionType === ActionEnum.REMOVE) {
                        this.isLoadingResults = true;
                        device.actionType = ActionEnum.REMOVE;
                        if (this.isMobileSelected) {
                            this.updateMobileCredential(device, 'removeMobileSuccessMsg', 'removeMobileErrorMsg');
                        } else {
                            this.manageDevice(device, 'removeSuccessMsg', 'removeErrorMsg');
                        }
                    }
                } else if (this.actionType === ActionEnum.REACTIVATE) {
                    this.isLoadingResults = true;
                    device.actionType = ActionEnum.REACTIVATE;
                    this.manageDevice(device, 'reactivateSuccessMsg', 'reactivateErrorMsg');
                } else if (this.actionType === ActionEnum.REMOVE) {
                    this.isLoadingResults = true;
                    device.actionType = ActionEnum.REMOVE;
                    this.manageDevice(device, 'removeSuccessMsg', 'removeErrorMsg');
                }
            } else {
                this.actionType = ' ';
                //  dialogRef.close();
            }
        });
    }
    updateMobileCredential(device: Device, successMsgString: string, failureMsgString: string) {
        this.manageIdentityDevicesService
            .updateMobileCredential(device.mobileCredentials[0].userDeviceMobileCredentialID, device)
            .subscribe(
                response => {
                    if (response.status === 200) {
                        this.translateService.get(`manageIdentiyDevices.${successMsgString}`).subscribe(
                            message => {
                                this.message = message;
                            },
                            error => {
                                this.isLoadingResults = false;
                            }
                        );
                        this.actionType = '';
                        this.display = false;
                        window.scrollTo(0, 0);
                        this.isLoadingResults = false;
                    } else {
                        this.actionType = '';
                        this.translateService.get(`manageIdentiyDevices.${failureMsgString}`).subscribe(
                            message => {
                                this.message = message;
                            },
                            error => {
                                this.isLoadingResults = false;
                            }
                        );
                        this.error = true;
                        this.isLoadingResults = false;
                    }
                },
                error => {
                    this.isLoadingResults = false;
                }
            );
    }
    manageDevice(device, successMsgString, failureMsg) {
        this.manageIdentityDevicesService.manageDevice(this.userId, device).subscribe(
            inactivateResult => {
                if (inactivateResult.status === 200) {
                    this.translateService.get(`manageIdentiyDevices.${successMsgString}`).subscribe(
                        message => {
                            this.message = message;
                        },
                        error => {
                            this.isLoadingResults = false;
                        }
                    );
                    this.actionType = '';
                    this.display = false;
                    window.scrollTo(0, 0);
                    this.isLoadingResults = false;
                } else {
                    this.actionType = '';
                    this.translateService.get(`manageIdentiyDevices.${failureMsg}`, { friendlyName: device.friendlyName }).subscribe(
                        message => {
                            this.message = message;
                        },
                        error => {
                            this.isLoadingResults = false;
                        }
                    );
                    this.message = failureMsg;
                    this.error = true;
                    this.isLoadingResults = false;
                }
            },
            error => {
                this.isLoadingResults = false;
            }
        );
    }
    onBackClick() {
        this.isMobileSelected = false;
        if (this.hasSmartCardDevices === true) {
            this.subscription = timer(0, 2000)
                .pipe(switchMap(() => this.getDeviceConnectionStatus()))
                .subscribe(response => {});
        }
    }
    goSearchPage() {
        this.router.navigate(['/searchUser']);
    }
    refresh(): void {
        if (this.isMobileSelected) {
            this.onMobileSelected(this.selectedDevice);
        } else {
            this.getUserDevices();
        }
        this.display = true;
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    check(attr, toolTipId): void {
        const val = document.getElementById(toolTipId);
        this.showToolTip = val.offsetWidth < val.scrollWidth ? true : false;
    }
}
@Component({
    templateUrl: 'reactivate-dialog.html'
})
export class ReActivateDialogComponent {
    msg: string;
    isShowPUK = false;
    comments = new FormControl(null, Validators.required);
    showComments = false;
    constructor(public dialogRef: MatDialogRef<ReActivateDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogDataResponse) {
        data.element = data.element;
        this.msg = data.dialogMsg;
        this.isShowPUK = data.isShowPUK;
        console.log(data);
        if (data.showComments == null || data.showComments === undefined) {
            this.showComments = false;
        } else {
            this.showComments = data.showComments;
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }
}
export class DialogDataResponse {
    element: string;
    headerText: string;
    user;
    canIssueCard: boolean;
    canIssueMobile: boolean;
    isIssuancePermissionEnabled: boolean;
    isMobilePermissionEnabled: boolean;
    isShowPUK: boolean;
    dialogMsg: string;
    showComments: boolean;
    constructor() {}
}
export class CuidWithReader {
    serial: string;
    crdReader: CardReader;
    deviceType: string;
    constructor() {}
}
@Component({
    templateUrl: 'manage-identity-devices-dialog.html'
})
export class AddIdentityDeviceDialogComponent {
    canIssueMobile = false;
    canIssueCard = false;
    isIssuancePermissionEnabled = false;
    isMobilePermissionEnabled = false;
    devProfiles;
    headerText;
    constructor(
        public dialogRef: MatDialogRef<AddIdentityDeviceDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogDataResponse
    ) {
        this.headerText = data.headerText;
        this.isIssuancePermissionEnabled = data.isIssuancePermissionEnabled;
        this.isMobilePermissionEnabled = data.isMobilePermissionEnabled;
        if (data.user != null) {
            this.devProfiles = data.user.wfDeviceProfiles;
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }
}
