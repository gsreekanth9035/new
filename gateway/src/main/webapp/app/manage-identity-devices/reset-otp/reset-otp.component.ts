import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from 'app/device-profile/data.service';
import { ManageIdentityDevicesService } from '../manage-identity-devices.service';
import { Device } from '../manage-identity-devices.model';
import { WorkflowService, ConfigIdValuePair } from 'app/workflow/workflow.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ActionEnum } from '../enum/actions.enum';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'jhi-reset-otp',
    templateUrl: './reset-otp.component.html',
    styleUrls: ['reset-otp.scss']
})
export class ResetOtpComponent implements OnInit, OnDestroy {
    isLoadingResults = false;
    reasons: ConfigIdValuePair[];
    display = true;
    message: string;
    error = false;
    device: Device;
    enableInputField = false;
    resetOTPForm: FormGroup;
    constructor(
        private manageIdentityDevicesService: ManageIdentityDevicesService,
        private dataService: DataService,
        private workflowService: WorkflowService,
        private formBuilder: FormBuilder,
        private translateService: TranslateService,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.resetOTPForm = this.formBuilder.group({
            reason: ['', [Validators.required]],
            reasonComment: []
        });
        this.workflowService.getConfigIdValues('SUSPEND_DEVICE_REASONS').subscribe(configIdValues => {
            this.reasons = configIdValues;
        });
        this.device = this.dataService.getCurrentMIdObj();
        if (this.device === null) {
            this.router.navigate(['./']);
        }
    }
    checkReason(event) {
        const reason = this.resetOTPForm.get('reason');
        const reasonComment = this.resetOTPForm.get('reasonComment');
        this.enableInputField = false;
        if (event.value.value === 'Other') {
            this.enableInputField = true;
            reason.clearValidators();
            reasonComment.setValidators([Validators.required]);
        } else {
            reasonComment.setValue(null);
            reasonComment.clearValidators();
            reason.setValidators([Validators.required]);
        }
        reason.updateValueAndValidity();
        reasonComment.updateValueAndValidity();
    }
    resetOTPDevice() {
        this.device.actionType = ActionEnum.RESET_OTP;
        this.device.reason = this.resetOTPForm.controls['reason'].value.id;
        this.device.reasonComment = this.resetOTPForm.controls['reasonComment'].value;
        if (this.device.isMobileSelected) {
            this.isLoadingResults = true;
            this.manageIdentityDevicesService.updateMobileOTPCredential(this.device.userName).subscribe(
                response => {
                    // if (response.status === 200) {
                    this.translateService.get('manageIdentiyDevices.resetOTPMobileDeviceSuccessMsg').subscribe(message => {
                        this.message = message;
                        this.display = false;
                        window.scrollTo(0, 0);
                    });
                    this.isLoadingResults = false;
                    // } else {
                    //     this.isLoadingResults = false;
                    //     this.translateService.get('manageIdentiyDevices.resetOTPErrorMsg').subscribe(message => {
                    //         this.message = message;
                    //         window.scrollTo(0, 0);
                    //         this.error = true;
                    //     });
                    // }
                },
                error => {
                    console.log(error);
                    this.isLoadingResults = false;
                }
            );
        } else {
            this.isLoadingResults = true;
            this.manageIdentityDevicesService.getUserByName(this.device.userName).subscribe(user => {
                this.manageIdentityDevicesService.manageDevice(user.id, this.device).subscribe(
                    result => {
                        if (result.status === 200) {
                            this.translateService.get('manageIdentiyDevices.resetOTPSuccessMsg').subscribe(message => {
                                this.message = message;
                            });
                            this.isLoadingResults = false;
                            this.display = false;
                        } else {
                            this.isLoadingResults = false;
                            this.translateService.get('manageIdentiyDevices.resetOTPErrorMsg').subscribe(message => {
                                this.message = message;
                                window.scrollTo(0, 0);
                                this.error = true;
                            });
                        }
                    },
                    error => {
                        console.log(error);
                        this.isLoadingResults = false;
                    }
                );
            });
        }
    }

    goBack() {
        const data = this.dataService.getCurrentObj();
        if (data != null) {
            data.device = this.device;
        }
        this.router.navigate(['../manageIdentityDevices'], { relativeTo: this.activatedRoute });
    }

    ngOnDestroy() {
        this.dataService.changeMIDObj(null);
    }
}
