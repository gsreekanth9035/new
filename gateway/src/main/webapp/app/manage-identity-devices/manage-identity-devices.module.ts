import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MANAGE_IDENTITY_DEVICES } from './manage-identity-devices.route';
import { RouterModule } from '@angular/router';
import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from 'app/material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    ManageIdentityDevicesComponent,
    ReActivateDialogComponent,
    AddIdentityDeviceDialogComponent
} from './manage-identity-devices.component';
import { ReportIncidentComponent } from './report-incident/report-incident.component';
import { ChangePinComponent } from './change-pin/change-pin.component';
import { ResetPinComponent } from './reset-pin/reset-pin.component';
import { ActivationComponent } from './activation/activation.component';
import { SuspendComponent } from './suspend/suspend.component';
import { ReissueComponent } from './reissue/reissue.component';
import { RevokeComponent } from './revoke/revoke.component';
import { RenewComponent } from './renew/renew.component';
import { IdentityDevicesComponent } from './identity-devices.component';
import { ResetOtpComponent } from './reset-otp/reset-otp.component';

@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MaterialModule,
        GatewaySharedModule,
        RouterModule.forChild(MANAGE_IDENTITY_DEVICES)
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    entryComponents: [ReActivateDialogComponent, AddIdentityDeviceDialogComponent],
    declarations: [
        ManageIdentityDevicesComponent,
        IdentityDevicesComponent,
        ActivationComponent,
        ReissueComponent,
        SuspendComponent,
        ResetOtpComponent,
        ReportIncidentComponent,
        ChangePinComponent,
        ResetPinComponent,
        RevokeComponent,
        ReActivateDialogComponent,
        RenewComponent,
        AddIdentityDeviceDialogComponent
    ]
})
export class ManageIdentityDevicesModule {}
