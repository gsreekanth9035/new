import { Component, OnInit, OnDestroy } from '@angular/core';
import { ManageIdentityDevicesService } from '../manage-identity-devices.service';
import { DataService } from 'app/device-profile/data.service';
import { Device } from '../manage-identity-devices.model';
import { WorkflowService, ConfigIdValuePair } from 'app/workflow/workflow.service';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActionEnum } from '../enum/actions.enum';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'jhi-revoke',
    templateUrl: './revoke.component.html',
    styleUrls: ['revoke.scss']
})
export class RevokeComponent implements OnInit, OnDestroy {
    isLoadingResults = false;
    reasons: ConfigIdValuePair[];
    display = true;
    message: string;
    error = false;
    device: Device;
    enableInputField = false;
    revokeForm: FormGroup;
    constructor(
        private manageIdentityDevicesService: ManageIdentityDevicesService,
        private dataService: DataService,
        private workflowService: WorkflowService,
        private formBuilder: FormBuilder,
        private translateService: TranslateService,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.revokeForm = this.formBuilder.group({
            reason: ['', [Validators.required]],
            reasonComment: []
        });
        this.workflowService.getConfigIdValues('REVOKE_DEVICE_REASONS').subscribe(configIdValues => {
            this.reasons = configIdValues;
        });
        this.device = this.dataService.getCurrentMIdObj();
        if (this.device === null) {
            this.router.navigate(['./']);
        }
    }
    checkReason(event) {
        const reason = this.revokeForm.get('reason');
        const reasonComment = this.revokeForm.get('reasonComment');
        this.enableInputField = false;
        if (event.value.value === 'Other') {
            this.enableInputField = true;
            reason.clearValidators();
            reasonComment.setValidators([Validators.required]);
        } else {
            reasonComment.setValue(null);
            reasonComment.clearValidators();
            reason.setValidators([Validators.required]);
        }
        reason.updateValueAndValidity();
        reasonComment.updateValueAndValidity();
    }
    revokeDevice() {
        this.device.actionType = ActionEnum.REVOKE;
        this.device.reason = this.revokeForm.controls['reason'].value.id;
        this.device.reasonComment = this.revokeForm.controls['reasonComment'].value;
        if (this.device.isMobileSelected) {
            this.isLoadingResults = true;
            this.manageIdentityDevicesService
                .updateMobileCredential(this.device.mobileCredentials[0].userDeviceMobileCredentialID, this.device)
                .subscribe(response => {
                    if (response.status === 200) {
                        this.isLoadingResults = false;
                        this.translateService.get('manageIdentiyDevices.revokeMobileDeviceSuccessMsg').subscribe(message => {
                            this.message = message;
                            this.display = false;
                            window.scrollTo(0, 0);
                        });
                    } else {
                        this.isLoadingResults = false;
                        this.translateService.get('manageIdentiyDevices.revokeErrorMsg').subscribe(message => {
                            this.message = message;
                            window.scrollTo(0, 0);
                            this.error = true;
                        });
                    }
                }, err => {
                    this.isLoadingResults = false;
                    this.translateService.get('manageIdentiyDevices.revokeErrorMsg').subscribe(message => {
                        this.message = message;
                        window.scrollTo(0, 0);
                        this.error = true;
                    });
                });
        } else {
            this.isLoadingResults = true;
            this.manageIdentityDevicesService.getUserByName(this.device.userName).subscribe(user => {
                this.manageIdentityDevicesService.manageDevice(user.id, this.device).subscribe(result => {
                    if (result.status === 200) {
                        this.isLoadingResults = false;
                        this.translateService.get('manageIdentiyDevices.revokeSuccessMsg').subscribe(message => {
                            this.message = message;
                        });
                        this.display = false;
                    } else {
                        this.isLoadingResults = false;
                        this.translateService.get('manageIdentiyDevices.revokeDeviceErrorMsg').subscribe(message => {
                            this.message = message;
                            window.scrollTo(0, 0);
                            this.error = true;
                        });
                    }
                }, err => {
                    this.isLoadingResults = false;
                    this.translateService.get('manageIdentiyDevices.revokeDeviceErrorMsg').subscribe(message => {
                        this.message = message;
                        window.scrollTo(0, 0);
                        this.error = true;
                    });
                });
            });
        }
    }

    goBack() {
        const data = this.dataService.getCurrentObj();
        if (data != null) {
            data.device = this.device;
        }
        this.router.navigate(['../manageIdentityDevices'], { relativeTo: this.activatedRoute });
    }

    ngOnDestroy() {
        this.dataService.changeMIDObj(null);
    }
}
