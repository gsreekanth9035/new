import { Route, Routes } from '@angular/router';
import { ManageIdentityDevicesComponent } from './manage-identity-devices.component';
import { ReportIncidentComponent } from './report-incident/report-incident.component';
import { ChangePinComponent } from './change-pin/change-pin.component';
import { ResetPinComponent } from './reset-pin/reset-pin.component';
import { ActivationComponent } from './activation/activation.component';
import { ReissueComponent } from './reissue/reissue.component';
import { SuspendComponent } from './suspend/suspend.component';
import { RevokeComponent } from './revoke/revoke.component';
import { RenewComponent } from './renew/renew.component';
import { IdentityDevicesComponent } from './identity-devices.component';
import { ResetOtpComponent } from './reset-otp/reset-otp.component';

export const MANAGE_IDENTITY_DEVICES: Routes = [
    {
        path: 'identityDevices',
        component: IdentityDevicesComponent,
        children: [
            {
                path: '',
                redirectTo: 'manageIdentityDevices',
                pathMatch: 'full'
            },
            {
                path: 'manageIdentityDevices',
                component: ManageIdentityDevicesComponent,
                data: {
                    authorities: [],
                    pageTitle: 'manageIdentiyDevices.title',
                    permissions: ['']
                }
            },
            {
                path: 'Activate',
                component: ActivationComponent,
                data: {
                    authorities: [],
                    pageTitle: 'manageIdentiyDevices.title',
                    permissions: ['']
                }
            },
            {
                path: 'Reissue',
                component: ReissueComponent,
                data: {
                    authorities: [],
                    pageTitle: 'manageIdentiyDevices.title',
                    permissions: ['']
                }
            },
            {
                path: 'ChangeDevicePIN',
                component: ChangePinComponent,
                data: {
                    authorities: [],
                    pageTitle: 'manageIdentiyDevices.title',
                    permissions: ['']
                }
            },
            {
                path: 'ResetDevicePIN',
                component: ResetPinComponent,
                data: {
                    authorities: [],
                    pageTitle: 'manageIdentiyDevices.title',
                    permissions: ['']
                }
            },
            {
                path: 'Suspend',
                component: SuspendComponent,
                data: {
                    authorities: [],
                    pageTitle: 'manageIdentiyDevices.title',
                    permissions: ['']
                }
            },
            {
                path: 'ReportIncident',
                component: ReportIncidentComponent,
                data: {
                    authorities: [],
                    pageTitle: 'manageIdentiyDevices.title',
                    permissions: ['']
                }
            },
            {
                path: 'Revoke',
                component: RevokeComponent,
                data: {
                    authorities: [],
                    pageTitle: 'manageIdentiyDevices.title',
                    permissions: [' ']
                }
            },
            {
                path: 'Renew',
                component: RenewComponent,
                data: {
                    authorities: [],
                    pageTitle: 'manageIdentiyDevices.title',
                    permissions: [' ']
                }
            },
            {
                path: 'ResetOTP',
                component: ResetOtpComponent,
                data: {
                    authorities: [],
                    pageTitle: 'manageIdentiyDevices.title',
                    permissions: [' ']
                }
            }
        ]
    }

    // {
    //     path: 'manageIdentityDevices',
    //     component: ManageIdentityDevicesComponent,
    //     data: {
    //         authorities: [],
    //         pageTitle: 'manageIdentiyDevices.title',
    //         permissions: ['']
    //     }
    // },
    // {
    //     path: 'Activate',
    //     component: ActivationComponent,
    //     data: {
    //         authorities: [],
    //         pageTitle: 'manageIdentiyDevices.title',
    //         permissions: ['']
    //     }
    // },
    // {
    //     path: 'Reissue',
    //     component: ReissueComponent,
    //     data: {
    //         authorities: [],
    //         pageTitle: 'manageIdentiyDevices.title',
    //         permissions: ['']
    //     }
    // },
    // {
    //     path: 'ChangeDevicePIN',
    //     component: ChangePinComponent,
    //     data: {
    //         authorities: [],
    //         pageTitle: 'manageIdentiyDevices.title',
    //         permissions: ['']
    //     }
    // },
    // {
    //     path: 'ResetDevicePIN',
    //     component: ResetPinComponent,
    //     data: {
    //         authorities: [],
    //         pageTitle: 'manageIdentiyDevices.title',
    //         permissions: ['']
    //     }
    // },
    // {
    //     path: 'Suspend',
    //     component: SuspendComponent,
    //     data: {
    //         authorities: [],
    //         pageTitle: 'manageIdentiyDevices.title',
    //         permissions: ['']
    //     }
    // },
    // {
    //     path: 'ReportIncident',
    //     component: ReportIncidentComponent,
    //     data: {
    //         authorities: [],
    //         pageTitle: 'manageIdentiyDevices.title',
    //         permissions: ['']
    //     }
    // },
    // {
    //     path: 'Revoke',
    //     component: RevokeComponent,
    //     data: {
    //         authorities: [],
    //         pageTitle: 'manageIdentiyDevices.title',
    //         permissions: [' ']
    //     }
    // },
    // {
    //     path: 'Renew',
    //     component: RenewComponent,
    //     data: {
    //         authorities: [],
    //         pageTitle: 'manageIdentiyDevices.title',
    //         permissions: [' ']
    //     }
    // },
];
