import { Component, OnInit, OnDestroy } from '@angular/core';
import { ManageIdentityDevicesService } from '../manage-identity-devices.service';
import { DataService } from 'app/device-profile/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Device, UserDevice } from '../manage-identity-devices.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { WorkflowService, ConfigIdValuePair } from 'app/workflow/workflow.service';
import { ActionEnum } from '../enum/actions.enum';

@Component({
    selector: 'jhi-report-incident',
    templateUrl: './report-incident.component.html',
    styleUrls: ['report-incident.scss']
})
export class ReportIncidentComponent implements OnInit, OnDestroy {
    isLoadingResults = false;
    reasons: ConfigIdValuePair[];
    display = true;
    message: string;
    error = false;
    device: Device;
    enableInputField = false;
    reportIncidentForm: FormGroup;
    constructor(
        private manageIdentityDevicesService: ManageIdentityDevicesService,
        private dataService: DataService,
        private router: Router,
        private formBuilder: FormBuilder,
        private translateService: TranslateService,
        private workflowService: WorkflowService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.reportIncidentForm = this.formBuilder.group({
            reason: ['', [Validators.required]],
            reasonComment: []
        });
        this.workflowService.getConfigIdValues('REPORT_INCIDENT_REASONS').subscribe(configIdValues => {
            this.reasons = configIdValues;
        });
        this.device = this.dataService.getCurrentMIdObj();
        if (this.device === null) {
            this.router.navigate(['./']);
        }
    }

    reportIncident() {
        this.device.actionType = ActionEnum.REPORT_INCIDENT;
        this.device.reason = this.reportIncidentForm.controls['reason'].value.id;
        this.device.reasonComment = this.reportIncidentForm.controls['reasonComment'].value;
        if (this.device.isMobileSelected) {
            this.isLoadingResults = true;
            this.manageIdentityDevicesService
                .updateMobileCredential(this.device.mobileCredentials[0].userDeviceMobileCredentialID, this.device)
                .subscribe(
                    response => {
                        if (response.status === 200) {
                            this.translateService.get('manageIdentiyDevices.reportIncidentSuccessMsg').subscribe(message => {
                                this.message = message;
                                this.display = false;
                                this.isLoadingResults = false;
                                window.scrollTo(0, 0);
                            });
                        } else {
                            this.isLoadingResults = false;
                            this.translateService.get('manageIdentiyDevices.reportIncidentErrorMsg').subscribe(
                                message => {
                                    this.message = message;
                                    window.scrollTo(0, 0);
                                    this.error = true;
                                },
                                error => {
                                    this.message = 'Error while reporting an incident!';
                                    window.scrollTo(0, 0);
                                    this.error = true;
                                }
                            );
                        }
                    },
                    error => {
                        console.log(error);
                        this.isLoadingResults = false;
                    }
                );
        } else {
            this.isLoadingResults = true;
            this.manageIdentityDevicesService.getUserByName(this.device.userName).subscribe(user => {
                this.manageIdentityDevicesService.manageDevice(user.id, this.device).subscribe(
                    result => {
                        if (result.status === 200) {
                            this.translateService.get('manageIdentiyDevices.reportIncidentSuccessMsg').subscribe(message => {
                                this.message = message;
                            });
                            this.isLoadingResults = false;
                            this.display = false;
                        } else {
                            this.isLoadingResults = false;
                            this.translateService.get('manageIdentiyDevices.reportIncidentErrorMsg').subscribe(
                                message => {
                                    this.message = message;
                                    window.scrollTo(0, 0);
                                    this.error = true;
                                },
                                error => {
                                    this.message = 'Error while reporting an incident!';
                                    window.scrollTo(0, 0);
                                    this.error = true;
                                }
                            );
                        }
                    },
                    error => {
                        console.log(error);
                        this.isLoadingResults = false;
                    }
                );
            });
        }
    }

    checkReason(event) {
        const reason = this.reportIncidentForm.get('reason');
        const reasonComment = this.reportIncidentForm.get('reasonComment');
        this.enableInputField = false;
        if (event.value.value === 'Other') {
            this.enableInputField = true;
            reason.clearValidators();
            reasonComment.setValidators([Validators.required]);
        } else {
            reasonComment.setValue(null);
            reasonComment.clearValidators();
            reason.setValidators([Validators.required]);
        }
        reason.updateValueAndValidity();
        reasonComment.updateValueAndValidity();
    }

    goBack() {
        const data = this.dataService.getCurrentObj();
        if (data != null) {
            data.device = this.device;
        }
        this.router.navigate(['../manageIdentityDevices'], { relativeTo: this.activatedRoute });
    }

    ngOnDestroy() {
        this.dataService.changeMIDObj(null);
    }
}
