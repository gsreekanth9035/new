import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoginService } from 'app/core/login/login.service';
import { StateStorageService } from 'app/core/auth/state-storage.service';
import { ConnectorService } from 'app/connector.service';

@Injectable()
export class AuthExpiredInterceptor implements HttpInterceptor {
    cookieValue = 'UNKNOWN';
    dataStream: ReplaySubject<any> = new ReplaySubject();

    dataStream$(): Observable<any> {
        return this.dataStream.asObservable();
    }

    constructor(private stateStorageService: StateStorageService, private injector: Injector, private connectorService: ConnectorService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // we can set a new Header
        // const auth = request.headers.get('Authorization');
        // alert('Authorization' + auth);

        return next.handle(request).pipe(
            tap(
                (event: HttpEvent<any>) => {
                    // alert(event.type);
                    this.connectorService.showError(false);
                },
                (err: any) => {
                    if (err instanceof HttpErrorResponse) {
                        if (err.status === 408) {
                            // alert(err.status);
                            // this.dataStream.next(err.status);
                            // alert( document.cookie );
                            this.connectorService.showError(true);
                        } else if (err.status === 401) {
                            // alert(err.status);
                            const destination = this.stateStorageService.getDestinationState();
                            if (destination !== null) {
                                const to = destination.destination;
                                const toParams = destination.params;
                                if (to.name === 'accessdenied') {
                                    this.stateStorageService.storePreviousState(to.name, toParams);
                                }
                            } else {
                                this.stateStorageService.storeUrl('/');
                            }

                            const loginService: LoginService = this.injector.get(LoginService);
                            loginService.login();
                        } else if (err.url && !err.url.includes('/api/account')) {
                            // alert(err.status);
                            const destination = this.stateStorageService.getDestinationState();
                            if (destination !== null) {
                                const to = destination.destination;
                                const toParams = destination.params;
                                if (to.name === 'accessdenied') {
                                    this.stateStorageService.storePreviousState(to.name, toParams);
                                }
                            } else {
                                this.stateStorageService.storeUrl('/');
                            }

                            const loginService: LoginService = this.injector.get(LoginService);
                            // loginService.login();
                        }
                    }
                }
            )
        );
    }
}
