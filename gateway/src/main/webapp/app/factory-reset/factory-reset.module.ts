import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from '../material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FACTORY_RESET_ROUTE } from './factory-reset.route';
import { FactoryResetComponent } from './factory-reset.component';

@NgModule({
    imports: [
        RouterModule.forChild([FACTORY_RESET_ROUTE]),
        CommonModule,
        GatewaySharedModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        MaterialModule,
        GatewaySharedModule,
        AngularSvgIconModule
    ],
    declarations: [FactoryResetComponent]
})
export class FactoryResetModule {}
