import { Route } from '@angular/router';
import { FactoryResetComponent } from './factory-reset.component';

export const FACTORY_RESET_ROUTE: Route = {
    path: 'factoryreset',
    component: FactoryResetComponent,
    data: {
        authorities: [],
        pageTitle: 'idreader.title'
    }
};
