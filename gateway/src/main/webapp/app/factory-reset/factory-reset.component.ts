import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Principal } from 'app/core';
import { IssuanceService } from 'app/issuance/issuance.service';
import { TranslateService } from '@ngx-translate/core';
import { DeviceClientService } from 'app/shared/util/device-client.service';
import { WorkflowService } from 'app/workflow/workflow.service';
import { CardReader } from 'app/issuance/model/cardReader.model';
import { throwError, Subscription, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { DeviceProfile } from 'app/device-profile/device-profile.model';
import { Commands } from 'app/issuance/model/commands.model';
import { CmsSyncRequest } from 'app/issuance/model/cmsSyncRequest.model';
import { DeviceValidator } from 'app/issuance/model/deviceValidator.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'jhi-factory-reset',
    templateUrl: './factory-reset.component.html',
    styleUrls: ['factory-reset.scss']
})
export class FactoryResetComponent implements OnInit, OnDestroy {
    isLoadingResults = false;
    factoryResetForm: FormGroup;
    orgName: string;
    cardReaders: CardReader[] = [];
    subscription: Subscription = new Subscription();
    flag = false;
    timer: any;

    selectedCardAtr: string;
    selectedCardName: string;

    errorMsg = '';
    errorDisplay = false;

    success = false;
    message = '';
    atr: string;
    deviceProfiles: DeviceProfile[] = [];
    wfDeviceProfilesList: DeviceProfile[] = [];
    processName;
    identifyProcessResponse: any;
    cardType;
    appLock: string;
    display = false;

    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private principal: Principal,
        private issuanceService: IssuanceService,
        private translateService: TranslateService,
        private deviceClientService: DeviceClientService,
        private workflowService: WorkflowService
    ) {
        this.orgName = this.principal.getLoggedInUser().organization;
        this.buildFactoryResetForm();
    }
    ngOnInit() {
        this.display = true;
    }
    buildFactoryResetForm() {
        this.factoryResetForm = this.formBuilder.group({
            identityDevice: ['', Validators.required],
            deviceProfile: [null, Validators.required]
        });
        this.getReadersList();
    }

    getReadersList() {
        this.subscription = timer(0, 5000)
            .pipe(switchMap(() => this.deviceClientService.getCardReaders()))
            .subscribe(
                readersList => {
                    const list = readersList.cardReaderList;
                    this.flag = false;
                    let updateList = false;
                    if (readersList.responseCode === 'DS0000') {
                        const selectedReader = this.factoryResetForm.controls['identityDevice'].value;
                        const cr = this.factoryResetForm.controls['identityDevice'].value;
                        if (cr === null || cr === undefined) {
                            this.cardReaders = list;
                            this.factoryResetForm.controls['identityDevice'].setValue(this.cardReaders[0]);
                            this.setCardName(this.cardReaders[0]);
                            return;
                        }
                        if (this.cardReaders.length === list.length || this.cardReaders.length > list.length) {
                            let isFound = false;
                            for (const c of this.cardReaders) {
                                const found = list.find(e => e.cardReaderName === c.cardReaderName);
                                if (found === undefined) {
                                    updateList = true;
                                } else {
                                    if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                    }
                                }
                            }
                            const card1 = list.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (card1 !== undefined) {
                                if (!isFound || this.selectedCardName === '' || card1.atr !== this.selectedCardAtr) {
                                    if (card1 !== undefined) {
                                        this.setCardName(card1);
                                    }
                                }
                            }
                        } else if (this.cardReaders.length < list.length) {
                            let isFound = false;
                            for (const c of list) {
                                const found = this.cardReaders.find(e => e.cardReaderName === c.cardReaderName);
                                if (found === undefined) {
                                    updateList = true;
                                } else {
                                    if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                    }
                                }
                            }
                            const card1 = list.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (card1 !== undefined) {
                                if (!isFound || this.selectedCardName === '' || card1.atr !== this.selectedCardAtr) {
                                    if (card1 !== undefined) {
                                        this.setCardName(card1);
                                    }
                                }
                            }
                        }
                        if (updateList) {
                            this.cardReaders = list;
                            const cardReader: CardReader = this.cardReaders.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (cardReader !== undefined) {
                                this.factoryResetForm.controls['identityDevice'].setValue(cardReader);
                                if (cardReader.atr !== this.selectedCardAtr) {
                                    this.setCardName(cardReader);
                                }
                            } else {
                                this.factoryResetForm.controls['identityDevice'].setValue(this.cardReaders[0]);
                                if (this.cardReaders[0].atr !== this.selectedCardAtr) {
                                    this.setCardName(this.cardReaders[0]);
                                }
                            }
                        }
                    } else {
                        this.factoryResetForm.controls['identityDevice'].setValue(null);
                        this.cardReaders = [];
                        this.selectedCardAtr = '';
                        this.selectedCardName = '';
                        this.translateService.get('issuance.connectReaderAndCard').subscribe(msg => {
                            this.errorMsg = msg;
                            this.errorDisplay = true;
                        });
                    }
                    this.flag = true;
                },
                error => {
                    if (this.subscription) {
                        this.subscription.unsubscribe();
                    }
                    this.errorDisplay = true;
                    this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                        this.errorMsg = serviceErrMessage;
                        // Retrying
                        this.getReadersList();
                    });
                }
            );
    }
    setCardName(cardReader: CardReader) {
        console.log(cardReader + '::::');
        const atr = cardReader.atr;
        if (atr.length !== 0) {
            // if (!this.isPinValidationFalied) {
            //     this.errorDisplay = false;
            // }
            const newAtr = atr.replace(/ /g, '');
            this.atr = newAtr;
            this.issuanceService.getCardNameByAtr(newAtr).subscribe(
                name => {
                    console.log(name);
                    if (name === '') {
                        this.deviceProfiles = [];
                        this.selectedCardName = '';
                        this.selectedCardAtr = '';
                        this.errorDisplay = true;
                        this.errorMsg = 'Card not supported.';
                        console.log('card name is empty');
                    } else {
                        this.errorDisplay = false;
                        console.log(this.selectedCardName, name);
                        if (this.selectedCardName !== name) {
                            this.selectedCardAtr = atr;
                            this.selectedCardName = name;
                            // call identify process and get the card details
                            // this.processName = 'identify';
                            console.log('call identify process:::::');
                            this.idetifyProcess(cardReader, newAtr);
                        }
                    }
                },
                err => {
                    this.deviceProfiles = [];
                }
            );
        } else {
            this.translateService.get('issuance.cardNotFound').subscribe(
                msg => {
                    this.deviceProfiles = [];
                    this.errorDisplay = true;
                    this.errorMsg = msg;
                    this.selectedCardAtr = '';
                    this.selectedCardName = '';
                },
                err => {
                    this.errorMsg = 'Card not found, Please insert';
                    console.log('insdie card not found:::');
                }
            );
        }
    }
    async idetifyProcess(cardReader: CardReader, atr) {
        const cardReaderName = cardReader.cardReaderName;
        // start identify process
        this.issuanceService.start('identify', atr).subscribe(
            identifyStartResponse => {
                const deviceServiceReq1 = identifyStartResponse.body;
                deviceServiceReq1.state = 0;
                console.log(deviceServiceReq1);
                // execute the commands to card
                this.deviceClientService.execute(deviceServiceReq1, cardReaderName).subscribe(
                    identifyCommandsRes => {
                        this.issuanceService.next(identifyCommandsRes.body).subscribe(
                            identifyNextRes => {
                                let nextResponse = new Commands();
                                nextResponse = identifyNextRes.body;
                                console.log(nextResponse);
                                if (nextResponse.step === 'done') {
                                    // this.cardSerialNumber = identifyNextRes.body.entries[0].result.serial;
                                    this.identifyProcessResponse = identifyNextRes;
                                    this.appLock = identifyNextRes.body.entries[0].result.appLock;
                                    this.cardType = identifyNextRes.body.entries[0].result.type;
                                    // this.showDeviceProfile = true;
                                    this.loadDevicePRofiles(identifyNextRes.body.entries[0].result.cardProductName);
                                    console.log(nextResponse);
                                } else {
                                    this.selectedCardName = '';
                                    return throwError(nextResponse.entries[0].message);
                                }
                            },
                            error => {
                                console.log(error);
                            }
                        );
                    },
                    error => {
                        console.log(error);
                    }
                );
            },
            error => {
                console.log(error);
            }
        );
    }
    loadDevicePRofiles(cardProductName: string) {
        if (this.wfDeviceProfilesList.length === 0) {
            this.workflowService.getDeviceProfiles().subscribe(
                deviceProfiles => {
                    this.wfDeviceProfilesList = deviceProfiles;
                    this.getDeviceProfilesBasedOnDevice(cardProductName);
                },
                err => {
                    this.isLoadingResults = false;
                }
            );
        } else {
            this.getDeviceProfilesBasedOnDevice(cardProductName);
        }
    }
    getDeviceProfilesBasedOnDevice(cardProductName: string) {
        const devProfList: DeviceProfile[] = this.wfDeviceProfilesList.filter(e => e.configValueProductName.value === cardProductName);
        console.log(devProfList);
        if (devProfList.length === 1) {
            this.deviceProfiles = devProfList;
            this.factoryResetForm.controls.deviceProfile.setValue(this.deviceProfiles[0]);
        } else if (devProfList.length === 0) {
            this.translateService.get('idreader.noDeviceProfFound').subscribe(msg => {
                this.errorMsg = msg;
                this.errorDisplay = true;
            });
        } else {
            this.deviceProfiles = devProfList;
            this.factoryResetForm.controls.deviceProfile.setValue(this.deviceProfiles[0]);
        }
    }
    resetDeviceOrToken() {
        console.log('reset device or token called::::');
        this.isLoadingResults = true;
        this.errorDisplay = false;
        if (this.factoryResetForm.controls['identityDevice'].value === null) {
            this.errorDisplay = true;
            this.errorMsg = 'Please connect reader and card!!';
            window.scrollTo(0, 0);
            this.isLoadingResults = false;
            return;
        }
        if (this.appLock === 'true') {
            console.log('inside the applock');
            this.errorDisplay = true;
            this.errorMsg = 'Card/Token is locked.Please Unlock the same to proceed';
            window.scrollTo(0, 0);
            this.isLoadingResults = false;
            return;
        }
        this.deviceClientService
            .getAtrByReaderName(this.factoryResetForm.controls.identityDevice.value.urlEncodedcardReaderName)
            .subscribe(async response => {
                const atr1 = response.atr;
                if (atr1 === '') {
                    this.translateService.get('issuance.identityDeviceNotFound').subscribe(msg => {
                        this.errorDisplay = true;
                        this.isLoadingResults = false;
                        this.errorMsg = msg;
                        window.scrollTo(0, 0);
                        return null;
                    });
                } else {
                    if (this.identifyProcessResponse === undefined) {
                        console.log('identify response is null');
                        // removing the white spaces in atr
                        const newAtr = atr1.replace(/ /g, '');
                        await this.idetifyProcess(this.factoryResetForm.controls.identityDevice.value.cardReaderName, newAtr);
                    }
                    const cardReaderName = this.factoryResetForm.controls['identityDevice'].value.cardReaderName;
                    this.processName = 'factory_reset';
                    this.startProcess(this.identifyProcessResponse, this.processName, cardReaderName);
                    clearInterval(this.timer);
                }
            });
    }

    startProcess(response, processName, cardReaderName) {
        this.issuanceService.validateDeviceBeforeIssuance(response.body.entries[0].result.serial, 'factory-reset', null).subscribe(
            (issaunceValidateResponse: DeviceValidator) => {
                // 204, true card is not present in our system
                // 200, true card is in revoked/ removed status -- and not printed
                // 202, false card is in revoked/ removed status -- and printed
                // 203, false card is other than revoked/ removed status
                // 205 -- device already factory reset..
                // && issaunceValidateResponse.statusCode === 200
                console.log(issaunceValidateResponse);
                if (issaunceValidateResponse.action === true) {
                    this.processName = processName;
                    const cmsSyncRequest = this.buildCmsSyncRequest(response, processName);
                    this.issuanceService.getUUID(processName, cmsSyncRequest).subscribe(
                        uuid => {
                            // start issuance process
                            this.issuanceService.start(processName, uuid).subscribe(
                                issaunceStartRes => {
                                    // this.width = this.width + 4;
                                    // this.width = this.width + this.widthIncrementCountBy;
                                    // send the commands to card
                                    issaunceStartRes.body.state = 1;
                                    this.deviceClientService.execute(issaunceStartRes.body, cardReaderName).subscribe(
                                        result => {
                                            // this.message = issaunceStartRes.body.stageStatus;
                                            this.issuanceService.next(result.body).subscribe(
                                                res => {
                                                    this.recurse(res.body, cardReaderName);
                                                },
                                                error => {
                                                    // for progress bar
                                                    // this.progressClass = 'progress-bar bg-danger';
                                                    // this.failure = true;
                                                    // this.success = false;
                                                    // this.message = error;
                                                    // for loader
                                                    this.isLoadingResults = false;
                                                    this.errorDisplay = true;
                                                    this.errorMsg = error;
                                                }
                                            );
                                        },
                                        error => {
                                            // this.progressClass = 'progress-bar bg-danger';
                                            // this.failure = true;
                                            // this.success = false;
                                            // this.message = error;
                                            this.isLoadingResults = false;
                                            this.errorDisplay = true;
                                            this.errorMsg = error;
                                        }
                                    );
                                },
                                error => {
                                    // this.progressClass = 'progress-bar bg-danger';
                                    // this.failure = true;
                                    // this.success = false;
                                    // this.message = error;
                                    this.isLoadingResults = false;
                                    this.errorDisplay = true;
                                    this.errorMsg = error;
                                }
                            );
                        },
                        error => {
                            // this.progressClass = 'progress-bar bg-danger';
                            // this.failure = true;
                            // this.success = false;
                            // this.message = error;
                            this.isLoadingResults = false;
                            this.errorDisplay = true;
                            this.errorMsg = error;
                        }
                    );
                } else {
                    // this.progressClass = 'progress-bar bg-danger';
                    // this.failure = true;
                    // this.success = false;
                    this.isLoadingResults = false;
                    this.errorDisplay = true;
                    if (issaunceValidateResponse.message === 'Device_Printed') {
                        this.translateService.get('factoryReset.validations.devicePrinted').subscribe(message => (this.errorMsg = message));
                    } else if (issaunceValidateResponse.statusCode === 205) {
                        this.translateService
                            .get('factoryReset.validations.deviceFactoryReset')
                            .subscribe(message => (this.errorMsg = message));
                    } else {
                        this.translateService
                            .get('factoryReset.validations.deviceValidation', { status: `${issaunceValidateResponse.message}` })
                            .subscribe(message => (this.errorMsg = message));
                    }
                }
            },
            err => {
                this.isLoadingResults = false;
                this.translateService.get('factoryReset.errorWhileFactoryReset').subscribe(msg => {
                    this.errorDisplay = true;
                    this.errorMsg = msg;
                });
            }
        );
    }

    recurse(res, cardReaderName) {
        if (res === null) {
            // this.progressClass = 'progress-bar bg-danger';
            // this.failure = true;
            // this.success = false;
            this.translateService.get('factoryReset.errorWhileFactoryReset').subscribe(msg => {
                // this.message = msg;
                this.errorDisplay = true;
                this.errorMsg = msg;
            });
            return throwError(res.step);
        }
        if (res.stage !== 'done' && res.step !== 'error') {
            // if (res.step === 'done') {
            //     this.width = this.width + this.widthIncrementCountBy;
            // }
            // this.message = res.stageStatus;
            res.state = 1;
            this.deviceClientService.execute(res, cardReaderName).subscribe(
                result1 => {
                    this.issuanceService.next(result1.body).subscribe(
                        response => {
                            if (response.body !== null) {
                                this.recurse(response.body, cardReaderName);
                            } else {
                                this.isLoadingResults = false;
                                this.translateService.get('factoryReset.errorWhileFactoryReset').subscribe(msg => {
                                    this.errorDisplay = true;
                                    this.errorMsg = msg;
                                });
                            }
                        },
                        error => {
                            // this.progressClass = 'progress-bar bg-danger';
                            // this.failure = true;
                            // this.success = false;
                            // this.message = error.message;
                            this.isLoadingResults = false;
                            this.errorDisplay = true;
                            this.errorMsg = error.message;
                        }
                    );
                },
                error => {
                    // this.progressClass = 'progress-bar bg-danger';
                    // this.failure = true;
                    // this.success = false;
                    // this.message = error.message;
                    this.isLoadingResults = false;
                    this.errorDisplay = true;
                    this.errorMsg = error.message;
                }
            );
        } else if (res.step === 'error' || res.stage === 'error') {
            // this.progressClass = 'progress-bar bg-danger';
            // this.failure = true;
            // this.success = false;
            // this.message = res.entries[0].message;
            this.isLoadingResults = false;
            this.errorDisplay = true;
            //this.errorMsg = res.entries[0].message;
            this.translateService.get('factoryReset.error.failed').subscribe(msg => {
                if (res.entries[0].message.includes('_')) {
                    this.errorMsg = msg + '- (Error Code: ' + res.entries[0].message + ')';
                } else {
                    this.errorMsg = msg;
                }
            });
            if (res.entries[0].message === null) {
                this.translateService.get('issuance.errorWhileIssuance').subscribe(msg => {
                    // this.message = msg;
                    this.errorMsg = res.entries[0].message;
                });
            }
            return throwError(res.step);
        } else if (res.step === 'done' && res.stage === 'done') {
            this.translateService.get('factoryReset.factoryResetSuccess').subscribe(msg => {
                // this.width = 100;
                // this.message = msg;
                // this.success = true;
                this.isLoadingResults = false;
                this.errorDisplay = false;
                this.success = true;
                this.message = msg;
                clearTimeout(this.timer);
                if (this.subscription) {
                    this.subscription.unsubscribe();
                }
                this.display = false;
                return res.step;
            });
        }
    }

    buildCmsSyncRequest(identifyNextRes, processName) {
        const cmsSyncRequest = new CmsSyncRequest();
        cmsSyncRequest.processName = processName;
        cmsSyncRequest.deviceType = 'permanent';
        cmsSyncRequest.device = {
            type: identifyNextRes.body.entries[0].result.type,
            serial: identifyNextRes.body.entries[0].result.serial,
            cardType: identifyNextRes.body.entries[0].result.cardType,
            cardProductName: identifyNextRes.body.entries[0].result.cardProductName
        };
        // cmsSyncRequest.user = this.user;
        cmsSyncRequest.userGroupXrefId = 1;
        cmsSyncRequest.orgName = this.orgName;
        cmsSyncRequest.deviceProfileId = this.factoryResetForm.controls.deviceProfile.value.id;
        cmsSyncRequest.issuedBy = this.principal.getLoggedInUser().username;
        cmsSyncRequest.loggedInUserId = this.principal.getLoggedInUser().userId;
        return cmsSyncRequest;
    }

    goHome() {
        this.router.navigate(['/dashboard']);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        clearInterval(this.timer);
    }
}
