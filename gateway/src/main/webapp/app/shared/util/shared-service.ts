import { Injectable } from '@angular/core';
import { Country } from 'app/onboarding-user/onboarding-user.component';
import * as _moment from 'moment';

const moment = _moment;
@Injectable({
    providedIn: 'root'
})
export class SharedService {
    countryCodes: Country[] = [];
    constructor() {
        this.countryCodes = [
            { flagName: 'US', countryCode: '+1' },
            { flagName: 'PA', countryCode: '+507' },
            { flagName: 'MX', countryCode: '+52' }
        ];
    }
    getDateWithoutTimeZoneOffset(UTCDate: Date): Date {
        let localeDate;
        const timeZoneOffset = new Date(UTCDate).getTimezoneOffset();
        const offsetHours = Math.ceil(timeZoneOffset / 60);
        const offsetMinutes = timeZoneOffset % 60;
        if (timeZoneOffset < 0) {
            // negative value
            localeDate = moment(UTCDate).add({ hours: -offsetHours, minutes: -offsetMinutes, seconds: 0 });
        } else {
            // positive value
            localeDate = moment(UTCDate).subtract({ hours: offsetHours, minutes: offsetMinutes, seconds: 0 });
        }
        return localeDate.toDate();
    }
    getPageSizeOptionsBasedOnTotalElements(totalElements, fromPageSizeOptions: Array<number>): Array<number> {
        const pageSizeOptions = [];
        for (let i = 0; i < fromPageSizeOptions.length; i++) {
            if (totalElements >= fromPageSizeOptions[i]) {
                pageSizeOptions.push(fromPageSizeOptions[i]);
            }
        }
        const nearestHigervalue = fromPageSizeOptions.find(e => e >= totalElements);
        if (!pageSizeOptions.includes(nearestHigervalue) && nearestHigervalue !== undefined) {
            pageSizeOptions.push(nearestHigervalue);
        }
        return pageSizeOptions;
    }

    getCountryCodes(): Country[] {
        return this.countryCodes;
    }

    getDateFromTImeZoneOffset(date, offset) {
        // get UTC time in msec
        const utc = date.getTime() + date.getTimezoneOffset() * 60000;
        // create new Date object for different city
        // using supplied offset
        const newDate = new Date(utc + 1000 * offset);
        return newDate;
    }
}
export class TimeZoneApiResponse {
    status: string;
    dstOffset: number;
    errorMessage: string;
    rawOffset: number;
    timeZoneId: string;
    timeZoneName: string;
}
