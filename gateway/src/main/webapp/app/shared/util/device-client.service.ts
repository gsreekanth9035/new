import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Principal } from 'app/core';
import { FingerCaptureResponse, FingerModel, IrisCaptureResponse, IrisModel } from 'app/enroll/enroll.service';
import { SigantureListResponse, SigantureCaptureResponse, SigantureModel } from 'app/enroll/model/signature.model';
import { PrintData, PrinterResponse } from 'app/issuance/issuance.service';
import { CardReaderList } from 'app/issuance/model/cardReader.model';
import { Commands } from 'app/issuance/model/commands.model';
import { PrinterListResponse } from 'app/issuance/model/Printer.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DeviceClientService {
    deviceServiceUrl;
    httpHeaders = new HttpHeaders({
        'Authorization': 'Basic RXBpY0lkZW50aXR5OkVwaWNJZGVudGl0eUAxMjM0'
    });

    constructor(private http: HttpClient, private principal: Principal) {
        const isSecurityEnabled = this.principal.getLoggedInUser().isDeviceClientSecurityEnabled;
        if (isSecurityEnabled) {
            this.deviceServiceUrl = 'http://localhost:1443/Device/Client';
        } else {
            this.deviceServiceUrl = 'http://localhost:1443/EpicIdentity/Client';
        }
    }
    getDeviceClientName(): Observable<any> {
        return this.http.get(`${this.deviceServiceUrl}/name`, {responseType: 'text'});
    }
    getDeviceCleintSecurityCheck(): Observable<any> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.http.get(`${window.location.origin}/apdu/api/v1/${organization}/deviceService/securityCheck`, { responseType: 'text' });
    }
    getDeviceSeviceStatus(): Observable<any> {
        return this.http.get(`${this.deviceServiceUrl}/health`);
    }
    getDeviceSeviceVersion(): Observable<any> {
        return this.http.get(`${this.deviceServiceUrl}/version`, { responseType: 'text' });
    }
    // Card Reader APIs
    getCardReaders(): Observable<CardReaderList> {
        //  this.httpHeaders.append('Authorization',  'Basic ' + btoa(`${username}:${password}`));
        // , {headers: this.httpHeaders}
        return this.http.get<CardReaderList>(this.deviceServiceUrl + '/card/list');
    }
    getAtrByReaderName(cardReaderName: string): Observable<any> {
        return this.http.get(`${this.deviceServiceUrl}/card/status?cardReaderName=${cardReaderName}`);
    }
    // execute commands (used in issuance)
    execute(request: Commands, cardReaderName): Observable<any> {
        request.cardReaderName = cardReaderName;
        return this.http.post(`${this.deviceServiceUrl}/card/execute`, request, { observe: 'response' });
    }

    // Printer API
    // get connected printers list  <PrinterListResponse>
    getConnectedPrinters(): Observable<PrinterListResponse> {
        return this.http.get<PrinterListResponse>(`${this.deviceServiceUrl}/printer/list`);
    }
    getPrinterStatus(manufacturer: string, printerName: string): Observable<PrinterResponse> {
        return this.http.get<PrinterResponse>(`${this.deviceServiceUrl}/printer/status?manufacturer=${manufacturer}&name=${printerName}`);
    }
    // move the card to chip encode position
    chipEncodePosition(): Observable<PrinterResponse> {
        return this.http.get<PrinterResponse>(`${this.deviceServiceUrl}/printer/personalize`);
    }

    print(printData: PrintData): Observable<PrinterResponse> {
        return this.http.post<PrinterResponse>(`${this.deviceServiceUrl}/printer/print`, printData);
    }

    // RejectPosition -- to eject the card from printer
    //  ContactEncoderPosition  -- to place the card in chip encode contact reader position
    // ContactlessEncoderPosition -- to place the card in chip encode contactless reader position
    positionCard(printerPosition: string, manufacturer: string, printerName: string): Observable<PrinterResponse> {
        return this.http.get<PrinterResponse>(`${this.deviceServiceUrl}/printer/positionCard?manufacturer=${manufacturer}&name=${printerName}&position=${printerPosition}`);
    }

    // FingerPrint APIs
    getConnectedFingerprintReaders(): Observable<any> {
        return this.http.get(`${this.deviceServiceUrl}/fingerprint/list`);
    }

    captureFP(fingerPrintModel: FingerModel, isRollFinger: boolean, isWSQEnabled: boolean): Observable<FingerCaptureResponse> {
        const httpHeaders = new HttpHeaders({
            'Content-type': 'application/json'
        });
        const capturedData = {
            scanFormat: fingerPrintModel.fPosition,
            deviceName: fingerPrintModel.deviceName,
            deviceSerialNo: fingerPrintModel.deviceSerialNo,
            deviceId: fingerPrintModel.deviceId,
            manufacturar: fingerPrintModel.manufacturar,
            imageType: ''
        };
        if (isRollFinger) {
            capturedData.scanFormat = 'ROLL_FINGER';
        } else if (isWSQEnabled) {
            capturedData.imageType = 'WSQ';
        }
        return this.http.post<FingerCaptureResponse>(
            `${this.deviceServiceUrl}/fingerprint/capture`,
            capturedData,
            {
                headers: httpHeaders
            }
        );
    }

    // Iris APIs
    getConnectedIrisReaders(): Observable<any> {
        return this.http.get(`${this.deviceServiceUrl}/iris/list`);
    }

    captureIris(irisModel: IrisModel): Observable<IrisCaptureResponse> {
        const httpHeaders = new HttpHeaders({
            'Content-type': 'application/json'
        });
        const irisCaptureReqData = {
            deviceId: irisModel.deviceId,
            deviceName: irisModel.deviceName,
            deviceSerialNo: irisModel.deviceSerialNo
        };
        return this.http.post<IrisCaptureResponse>(
            `${this.deviceServiceUrl}/iris/capture`,
            irisCaptureReqData,
            {
                headers: httpHeaders
            }
        );
    }

    // Siganture APIs
    getConnectedSignatureDevicesList(): Observable<SigantureListResponse> {
        return this.http.get<SigantureListResponse>(`${this.deviceServiceUrl}/signpad/list`);
    }

    captureSignature(signatureModel: SigantureModel): Observable<SigantureCaptureResponse> {
        const signReqData = {
            deviceId: signatureModel.deviceId,
            deviceName: signatureModel.deviceName,
            pen: 'blue',
            thickness: '1'
        };
        return this.http.post<SigantureCaptureResponse>(`${this.deviceServiceUrl}/signpad/capture`, signReqData);
    }
}
