import { Injectable } from '@angular/core';
import { ValidatorFn, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ValidationByLanguageService {
    validations = new ValidatorsComponent();

    private initialValidations = new BehaviorSubject<any>(this.validations);
    cuurentValidationsObj = this.initialValidations.asObservable();
    constructor() {
        this.changeValidationObj(localStorage.getItem('selectedLanguage'));
    }

    changeValidationObj(obj: any) {
        let specialCharsNotAllowedValidator;
        let allowedCharactersAndNumbersAndSpace;
        let specialCharsAndNumbersNotAllowedValidator;
        let specialCharsAllowedValidator;
        let regxForAlphabetOnlyDirective;
        let validatorStringForAlphabetOnlyDirective;
        if (obj === 'en') {
            specialCharsNotAllowedValidator = Validators.pattern('^[a-zA-Z0-9_. ]*$');
            allowedCharactersAndNumbersAndSpace = Validators.pattern('^[a-zA-Z0-9 ]*$');
            specialCharsAndNumbersNotAllowedValidator = Validators.pattern('^[a-zA-Z ]*$');
            specialCharsAllowedValidator = Validators.pattern('^[#.0-9a-zA-Z ,-/()]*$');
            regxForAlphabetOnlyDirective = new RegExp(/^[a-zA-Z ]+$/);
            validatorStringForAlphabetOnlyDirective = /[^a-zA-Z ]*/g;
        } else {
            specialCharsNotAllowedValidator = Validators.pattern('^[a-zA-Z0-9_. ñáéíóúüÁÉÍÑÓÚÜ¡¿«»]*$');
            specialCharsAndNumbersNotAllowedValidator = Validators.pattern('^[a-zA-Z ñáéíóúüÁÉÍÑÓÚÜ¡¿«»]*$');
            specialCharsAllowedValidator = Validators.pattern('^[#.0-9a-zA-Z ,-/() ñáéíóúüÁÉÍÑÓÚÜ¡¿«»]*$');
            allowedCharactersAndNumbersAndSpace = Validators.pattern('^[a-zA-Z0-9 ñáéíóúüÁÉÍÑÓÚÜ¡¿«»]*$');
            regxForAlphabetOnlyDirective = new RegExp(/^[a-zA-Z ñáéíóúüÁÉÍÑÓÚÜ¡¿«»]+$/);
            validatorStringForAlphabetOnlyDirective = /[^a-zA-Z ñáéíóúüÁÉÍÑÓÚÜ¡¿«»]*/g;
        }
        const obj1 = new ValidatorsComponent();
        obj1.specialCharsNotAllowedValidator = specialCharsNotAllowedValidator;
        obj1.specialCharsAndNumbersNotAllowedValidator = specialCharsAndNumbersNotAllowedValidator;
        obj1.specialCharsAllowedValidator = specialCharsAllowedValidator;
        obj1.allowedCharactersAndNumbersAndSpace = allowedCharactersAndNumbersAndSpace;
        obj1.regxForAlphabetOnlyDirective = regxForAlphabetOnlyDirective;
        obj1.validatorStringForAlphabetOnlyDirective = validatorStringForAlphabetOnlyDirective;
        this.initialValidations.next(obj1);
    }
}
export class ValidatorsComponent {
    specialCharsNotAllowedValidator: ValidatorFn;
    specialCharsAndNumbersNotAllowedValidator: ValidatorFn;
    specialCharsAllowedValidator: ValidatorFn;
    allowedCharactersAndNumbersAndSpace: ValidatorFn;
    regxForAlphabetOnlyDirective: RegExp;
    validatorStringForAlphabetOnlyDirective;
    constructor() {}
}
