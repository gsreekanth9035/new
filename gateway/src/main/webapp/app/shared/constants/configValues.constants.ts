export const UNIFYIA_JCOP4 = 'NXP JCOP 4 with Unifyia PIV Applet V 4.0';
// Notification constants
export const NOTIFICATION_METHOD_PORTAL = 'PORTAL';
export const NOTIFICATION_DATE_FORMAT = 'dd-MMM-yyyy_HH-mm-ss.SSS';
export const CERT_RENEW = 'CERT_RENEW';
// biometric verification response constants
export const DUPLICATE_FOUND = 'DUPLICATE_FOUND';
export const NO_DUPLICATE_DATA_FOUND = 'NO_DUPLICATE_DATA_FOUND';
export const APPROVE_REASONS_IF_NO_DUPLICATES = 'APPROVE_REASONS_IF_NO_DUPLICATES';
export const APPROVE_REASONS_IF_DUPLICATES_FOUND = 'APPROVE_REASONS_IF_DUPLICATES_FOUND';
export const REJECT_REASONS = 'REJECT_REASONS';
