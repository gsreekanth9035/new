import { ElementRef, Directive, HostListener, Input, OnInit, Renderer } from '@angular/core';

@Directive({
  selector: 'input[jhiNumbersOnly]'
})
export class NumberDirective {
  regex: RegExp = new RegExp(/^-?[0-9]+(\*){0,1}$/g);
  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', 'v', 'c'];
  constructor(private el: ElementRef) {
  }
  @HostListener('keydown', ['$event'])
  onkeydown(event: KeyboardEvent) {
    if (this.specialKeys.indexOf(event.key) !== -1) {
    } else {
      const current: string = this.el.nativeElement.value;
      const next: string = current.concat(event.key);
      if (next && !String(next).match(this.regex)) {
        event.preventDefault();
      }
    }
  }

  @HostListener('input', ['$event']) onInputChange(event) {
    const initalValue = this.el.nativeElement.value;
    this.el.nativeElement.value = initalValue.replace(/[^0-9]*/g, '');
    if (initalValue !== this.el.nativeElement.value) {
      event.stopPropagation();
    }
  }

}
