import { NgForm, FormGroup } from '@angular/forms';
import { EventEmitter, Output, Directive, HostListener, Input } from '@angular/core';

@Directive({
    selector: '[jhiSubmitIfValid]'
})
export class SubmitIfValidDirective {

    @Output() valid = new EventEmitter<void>();
    @Input() jhiSubmitIfValid: FormGroup;
    constructor() {
    }

    @HostListener('click')
    handleClick() {
        this.markFieldAsDirty();
        this.emitEventIfValid();
    }

    private markFieldAsDirty() {
        Object.keys(this.jhiSubmitIfValid.controls).forEach(fieldName => {
            this.jhiSubmitIfValid.controls[fieldName].markAsDirty();
        });
    }

    private emitEventIfValid() {
        if (this.jhiSubmitIfValid.valid) {
            this.valid.emit();
        }
    }
}
