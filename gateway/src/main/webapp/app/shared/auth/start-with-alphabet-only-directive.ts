import { Directive, HostListener, ElementRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ValidationByLanguageService, ValidatorsComponent } from '../util/validation-by-language.service';

@Directive({
    selector: 'input[jhiStartWithAlphabet]'
})
export class StartWithAlphabetOnlyDirective implements OnDestroy{
    subscription: Subscription;
    validations: ValidatorsComponent;
    regex: RegExp = new RegExp(/^[a-zA-Z ñáéíóúüÁÉÍÑÓÚÜ]+$/);
    private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', 'v', 'c'];
    constructor(private el: ElementRef, private validationByLanguageService: ValidationByLanguageService) {
        this.subscription = this.validationByLanguageService.cuurentValidationsObj.subscribe( res => {
            this.validations = res;
            this.regex = this.validations.regxForAlphabetOnlyDirective;
        });
    }
    @HostListener('keydown', ['$event'])
    onkeydown(event: KeyboardEvent) {
        if (this.specialKeys.indexOf(event.key) !== -1) {
        } else {
            const current: string = this.el.nativeElement.value;
            const next: string = current.concat(event.key);
            if (next && !String(next).match(this.regex)) {
                event.preventDefault();
            }
        }
    }

    @HostListener('input', ['$event'])
    onInputChange(event) {
        const initialValue = this.el.nativeElement.value;
        this.el.nativeElement.value = initialValue.replace(this.validations.validatorStringForAlphabetOnlyDirective, '');
        if (initialValue !== this.el.nativeElement.value) {
            event.preventDefault();
        }
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
