import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';
import { Principal } from 'app/core/auth/principal.service';

@Directive ({
    selector: '[jhiHasWorkflowStep]'
})
export class HasWorkflowStepDirective {

    private workflowStep: string;

    constructor(private principal: Principal, private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {}

    @Input()
    set jhiHasWorkflowStep(workflowStep: string) {
        this.workflowStep = workflowStep;
        this.updateView();

        // Get notified each time authentication state changes.
        this.principal.getAuthenticationState().subscribe(identity => this.updateView());
    }

    updateView(): void {
        this.hasWorkflowStep().then(result => {
            this.viewContainerRef.clear();
            if (result) {
                this.viewContainerRef.createEmbeddedView(this.templateRef);
            }
        });
    }

    hasWorkflowStep(): Promise<boolean> {
        if (!this.principal.isAuthenticated()
            || !this.principal.getLoggedInUser()
            || !this.principal.getLoggedInUser().workflowSteps) {
            return Promise.resolve(false);
        }

        if (this.principal.getLoggedInUser().workflowSteps.includes(this.workflowStep)) {
            return Promise.resolve(true);
        }

        return Promise.resolve(false);
    }
}
