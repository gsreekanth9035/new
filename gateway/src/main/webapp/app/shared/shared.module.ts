import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { GatewaySharedLibsModule, GatewaySharedCommonModule, HasAnyAuthorityDirective, HasWorkflowStepDirective } from './';
import { SubmitIfValidDirective } from './auth/submit-if-valid.directive';
import { NumberDirective } from 'app/shared/auth/numberOnly.directive';
import { StartWithAlphabetOnlyDirective } from './auth/start-with-alphabet-only-directive';

@NgModule({
    imports: [GatewaySharedLibsModule, GatewaySharedCommonModule],
    declarations: [
        HasAnyAuthorityDirective,
        HasWorkflowStepDirective,
        SubmitIfValidDirective,
        NumberDirective,
        StartWithAlphabetOnlyDirective
    ],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
    exports: [
        GatewaySharedCommonModule,
        HasAnyAuthorityDirective,
        HasWorkflowStepDirective,
        SubmitIfValidDirective,
        NumberDirective,
        StartWithAlphabetOnlyDirective
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewaySharedModule {}
