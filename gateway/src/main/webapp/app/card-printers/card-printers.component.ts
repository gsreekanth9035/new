import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { APP_DATE_FORMATS, AppDateAdapter } from 'app/shared/util/date.adapter';

@Component({
    selector: 'jhi-card-printers',
    templateUrl: './card-printers.component.html',
    styleUrls: ['card-printers.scss']
})
export class CardPrintersComponent implements OnInit {
    cardPrinterForm: FormGroup;

    constructor(private formBuilder: FormBuilder) {}

    ngOnInit() {
        this.cardPrinterForm = this.formBuilder.group({
            printerListForm: this.formBuilder.array([
                this.formBuilder.group({
                    printerName: ['', [Validators.required]],
                    deviceIdentifier: ['', [Validators.required]],
                    ipAddress: ['', [Validators.required]]
                })
            ])
        });
    }

    addPrinter() {
        (this.cardPrinterForm.get('printerListForm') as FormArray).push(
            this.formBuilder.group({
                printerName: ['', [Validators.required]],
                deviceIdentifier: ['', [Validators.required]],
                ipAddress: ['', [Validators.required]]
            })
        );
    }

    removeEnrollment(index: number) {
        (this.cardPrinterForm.get('printerListForm') as FormArray).removeAt(index);
    }
}
