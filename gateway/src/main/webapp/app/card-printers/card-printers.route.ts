import { CardPrintersComponent } from './card-printers.component';
import { Routes } from '@angular/router';

export const CARD_PRINTERS_ROUTES: Routes = [
    {
        path: 'card-printers',
        component: CardPrintersComponent,
        data: {
            authorities: [],
            pageTitle: 'cardPrinters.pageTitle'
        }
    }
];
