import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from './../material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CARD_PRINTERS_ROUTES } from './card-printers.route';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CardPrintersComponent } from './card-printers.component';

@NgModule({
    imports: [
        RouterModule.forChild(CARD_PRINTERS_ROUTES),
        CommonModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MaterialModule,
        GatewaySharedModule
    ],
    declarations: [CardPrintersComponent]
})
export class CardPrintersModule {}
