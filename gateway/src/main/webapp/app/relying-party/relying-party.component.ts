import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { RelyingPartyService } from './relying-party.service';

@Component({
  selector: 'jhi-relying-party',
  templateUrl: './relying-party.component.html',
  styleUrls: ['relying-party.scss']
})
export class RelyingPartyComponent implements OnInit {

  redirectUris: Array<String> = [];
  redirectUri = new FormControl('', Validators.required);
  relyingPartyFormGroup: FormGroup;

  success = false;
  failure = false;
  message: string;
  
  isLoadingResults = false;
  redirectUriTobeDel: String;

  constructor(
    private translateService: TranslateService,
    private router: Router,
    private formBuilder: FormBuilder,
   private relyingPartyService: RelyingPartyService
  ) { }

  ngOnInit() {
    this.relyingPartyFormGroup = this.formBuilder.group({
      redirectUri: ['', Validators.required]
    });

    this.relyingPartyService.getRelyingParties().subscribe(redirectUris => {
      this.redirectUris = redirectUris;
    });
  }
  addRedirectUri() {
    const redirectUris = [];
    redirectUris.push(this.relyingPartyFormGroup.controls['redirectUri'].value);
    this.isLoadingResults = true;
    this.relyingPartyService.addRelyingParty(redirectUris).subscribe(res => {
      if (res.status === 200) {
        this.success = true;
        this.isLoadingResults = false;
        this.translateService.get('relyingParty.messages.success').subscribe(msg => {
          this.message = msg;
        });
        this.relyingPartyService.getRelyingParties().subscribe(redirectUris => {
          this.redirectUris = redirectUris;
        });
        window.scrollTo(0, 0);
      } else {
        this.handleErrors(res);
      }
    }, err => {
      this.handleErrors(err);
    });
  }
 
  handleErrors(err) {
    this.failure = true;
    if (err.status === 409) {
      this.translateService.get('relyingParty.messages.conflict').subscribe(msg => {
        this.message = msg;
      });
      window.scrollTo(0, 0);
    } 
    // else if (err.status === 500) {
    //   this.translateService.get('manageGroups.messages.invalid').subscribe(msg => {
    //     this.message = msg;
    //   });
    //   window.scrollTo(0, 0);
    // } 
    else if (err.status === 503) {
      this.translateService.get('relyingParty.messages.serverError').subscribe(msg => {
        this.message = msg;
      });
      window.scrollTo(0, 0);
    } else {
      this.translateService.get('relyingParty.messages.failure').subscribe(msg => {
        this.message = msg;
      });
      window.scrollTo(0, 0);
    }
    this.isLoadingResults = false;
  }

  
  delRedirectUri() {
    this.success = false;
    this.relyingPartyService.delRelyingParty(this.redirectUriTobeDel).subscribe(
      res => {
        if (res.status === 200 || res.status === 204) {
          this.success = true;
          this.isLoadingResults = false;
          this.translateService.get('relyingParty.messages.delSuccess').subscribe(msg => {
            this.success = true;
            this.message = msg;
            this.relyingPartyService.getRelyingParties().subscribe(groups => {
              this.redirectUris = groups;
            });
          });
        }
      },
      err => {
        this.isLoadingResults = false;
        // this.handleError(err);
      }
    );
  }

  deleteRedirectUri(redirectUri) {
    this.redirectUriTobeDel = redirectUri;
  }
  goHome() {
    this.router.navigate(['/dashboard']);
  }

}
