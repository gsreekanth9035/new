import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Principal } from "app/core";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class RelyingPartyService {
    orgName;
    constructor(private httpClient: HttpClient, private principal: Principal) {
        this.orgName = this.principal.getLoggedInUser().organization;
    }

    getRelyingParties(): Observable<any> {
        return this.httpClient.get(`${window.location.origin}/usermanagement/api/v1/organizations/${this.orgName}/idp-client/redirect-uri`)
    }

    addRelyingParty(relyingParties: Array<String>): Observable<any> {
        return this.httpClient.put(`${window.location.origin}/usermanagement/api/v1/organizations/${this.orgName}/idp-client/redirect-uri`,
            relyingParties, { observe: 'response' });
    }

    delRelyingParty(redirectURI: String): Observable<any> {
        return this.httpClient.delete(`${window.location.origin}/usermanagement/api/v1/organizations/${this.orgName}/idp-client/redirect-uri?redirectURI=${redirectURI}`, { observe: 'response' });
    }
}