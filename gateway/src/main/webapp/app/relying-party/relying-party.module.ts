import { GatewaySharedModule } from '../shared/shared.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { RelyingPartyComponent } from './relying-party.component';
import { RELYING_PARTY_ROUTES } from './relying-party.route';
import { MaterialModule } from 'app/material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularSvgIconModule } from 'angular-svg-icon';

@NgModule({
    imports: [CommonModule,
        GatewaySharedModule,
        RouterModule.forChild(RELYING_PARTY_ROUTES),
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        AngularSvgIconModule
    ],
    declarations: [RelyingPartyComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class RelyingPartyModule { }
