import { Routes } from '@angular/router';
import { RelyingPartyComponent } from './relying-party.component';

export const RELYING_PARTY_ROUTES: Routes = [
    {
        path: 'relying-party',
        component: RelyingPartyComponent,
        data: {
            authorities: [],
            pageTitle: 'relyingParty.title',
        }
    }
];
