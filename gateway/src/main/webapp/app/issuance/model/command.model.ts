export class Command {
    type: string;
    apdu: string;
    expected: string[];
    id: string;
    message: string;
    result = new  Map<String, String>();
    constructor() { }
  }
