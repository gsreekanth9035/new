export class CmsSyncRequest {
    user: string;
    processName: string;
    process: SyncProcess;
    userGroupXrefId: Number;
    deviceType: string;
    pin: string;
    cuid: string;
    cardprofid: Number;
    device: any;
    orgName: string;
    wfName: string;
    oldPin: string;
    newPin: string;
    deviceId: number;
    isRandomPinRequired: boolean;

    chipEncoded: boolean;
    printed: boolean;
    deviceCertsForRenew;
    isActivationRequired: boolean;
    isSelfService: boolean;
    issuedBy: string;
    loggedInUserId: number;
    deviceProfileId: number;
    constructor() {}
}
export class SyncProcess {}
