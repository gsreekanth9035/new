import { ResponseApdu } from './responseApdu.model';

export class ResponseCommand {
    session: string;
   stage: string;
   step: string;
   cardReaderName: string;
   entries: ResponseApdu[];
   constructor() { }
  }
