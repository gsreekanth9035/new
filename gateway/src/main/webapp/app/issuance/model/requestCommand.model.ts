import { RequestApdu } from './requestApdu.model';

export class RequestCommand {
    session: string;
    stage: string;
    step: string;
    cardReaderName: string;
    entries: RequestApdu[];
    constructor() { }
  }
