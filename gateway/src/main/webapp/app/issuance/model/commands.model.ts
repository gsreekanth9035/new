import { PivMasterBean, RfidMasterBean } from 'app/id-reader/model/id-reader.model';
import { Command } from './command.model';

export class Commands {
  session: string;
  stage: string;
  step: string;
  cardReaderName: string;
  entries: Command[];
  pivMasterBean: PivMasterBean;
  rfidMasterBean: RfidMasterBean;
  constructor() { }
}
