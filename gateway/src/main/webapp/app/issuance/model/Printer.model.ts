export class PrinterListResponse {
    printerResponse: Printer[];
    responseCode: string;
    constructor() {}
}
export class Printer {
    product_name: string;
    product_version: string;
    status: string;
    manufacturer: string;
    printerName: string;
    constructor() {}
}
