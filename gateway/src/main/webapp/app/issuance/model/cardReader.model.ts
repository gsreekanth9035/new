export class CardReader {
  atr: string;
  cardReaderName: string;
  urlEncodedcardReaderName: string;
  constructor() { }
}

export class CardReaderList {
  cardReaderList: CardReader[];
  responseCode: string;
  constructor() { }
}
