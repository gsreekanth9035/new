import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Subscription } from 'rxjs';

export function compareValidator(controlNameToCompare: string, action: string): ValidatorFn {
    return (c: AbstractControl): ValidationErrors | null => {
        if (c.value === null || c.value.length === 0) {
            return null; // dont validate empty values
        }
        const controlToCompare = c.root.get(controlNameToCompare);
        if (controlToCompare) {
            const subscription: Subscription = controlToCompare.valueChanges.subscribe(() => {
                c.updateValueAndValidity();
                subscription.unsubscribe();
            });
        }
        if (action === 'compare') {
            return controlToCompare && controlToCompare.value !== c.value ? { 'compare': true } : null;
        } else if (action === 'compareEqual') {
            return controlToCompare && controlToCompare.value === c.value ? { 'compareEqual': true } : null;
        }
    };
}
