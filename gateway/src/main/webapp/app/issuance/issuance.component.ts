import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IdentityCard, IssuanceService, PrintData } from 'app/issuance/issuance.service';
import { Router, ActivatedRoute } from '@angular/router';
import { timer, Subscription, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { CmsSyncRequest } from './model/cmsSyncRequest.model';
import { Commands } from './model/commands.model';
import { CardReader } from './model/cardReader.model';
import { UserInfo, Principal } from 'app/core';
import { compareValidator } from './CompareValidator';
import { WFStepVisualTemplateConfig } from './issuance.service';
import { DataService, Data } from 'app/device-profile/data.service';
import { Printer } from './model/Printer.model';
import { TranslateService } from '@ngx-translate/core';
import { DeviceProfile } from 'app/device-profile/device-profile.model';
import { Group } from 'app/workflow/group.model';
import { WfStepGroupConfig } from 'app/workflow/model/wf-step-group-config.model';
import { DialogPosition, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { WFStepCertConfig } from 'app/workflow/wf-step-cert-config.model';
import { DeviceClientService } from 'app/shared/util/device-client.service';
import { DeviceValidator } from './model/deviceValidator.model';
import { ReActivateDialogComponent } from 'app/manage-identity-devices/manage-identity-devices.component';

@Component({
    selector: 'jhi-issuance',
    templateUrl: './issuance.component.html',
    styleUrls: ['issuance.scss']
})
export class IssuanceComponent implements OnInit, OnDestroy {
    pinHide = true;
    confirmPinHide = true;
    width = 0;
    remainingWidth: number;
    widthIncrementCountBy: number;
    message = '';
    errorMsg = '';
    error = false;
    success = false;
    failure = false;
    display = true;
    progressClass = 'progress-bar bg-success';
    persoIdentityDeviceForm: FormGroup;
    cardPrinters: Printer[] = [];
    cardReaders: CardReader[] = [];
    subscription: Subscription = new Subscription();
    printerSubscription = new Subscription();
    userToBeIssue = new UserInfo();
    userFirstName: string;
    workflowName: string;
    orgName: string;
    user: string;
    processName;
    identifyProcessResponse;
    disablePINS = false;
    actions: Array<Action> = [];
    credentialTemplates: WFStepVisualTemplateConfig[] = [];
    flag = false;
    timer: any;
    data: Data;
    userID: string;
    isCardReaderSubscribed = false;
    isPrinterListSubscribed = false;
    userEnrolled = true;
    addIdentityDeviceObj;
    addIdentityDeviceTypeName: string;
    isSame = false;
    isSelfService = true;
    isActivationRequired = false;
    selectedCardAtr: string;
    selectedCardName: string;
    supportedDevProfilesFromWorkflow: Array<string> = [];
    notSupportedCard = false;
    hasUserRoleWithOtherRoles = false;
    isLoadingResults = false;
    isGpLockApplicable = true;
    permissionError = false;
    isCardLocked = false;
    isKeyEscrow = false;
    isCertStepExists = false;
    issuedDevices: number;
    isAllowedDevicesApplicable = false;
    allowedDevice;
    issuanceAllowed = false;
    isPendingApproval = false;
    isIssuedOrReadyForIssuance = false;
    isOperator = false;
    printSelected = false;
    persoAndPrintSelected = false;
    isCardinChipEncodePosition = false;
    isPrintingStarted = false;
    persoCallsubscription: Subscription;
    isIssuanceLimitExceeded = false;
    selectedPrinter: Printer;
    actionSelected = '';
    printerStatusCalls = 0;
    cardType: string;
    cardPositionFromPrinter: string;
    groupName: string;
    identityType: string;
    userFirstAndLastName: string;
    showToolTip = false;
    isEnrollmentEnabled: boolean = false;
    stageSpecificError: string;
    errorCode: string;
    constructor(
        private formBuilder: FormBuilder,
        private issuanceService: IssuanceService,
        private ar: ActivatedRoute,
        private principal: Principal,
        private router: Router,
        private dataService: DataService,
        private translateService: TranslateService,
        public dialog: MatDialog,
        private deviceClientService: DeviceClientService
    ) {
        this.buildIssuanceForm();
    }

    ngOnInit() {
        this.isLoadingResults = true;
        if (this.principal.getLoggedInUser().roles.includes('role_user')) {
            if (this.principal.getLoggedInUser().authorities.includes('Menu_Enrollment')) {
                this.isEnrollmentEnabled = true;
            }
        }
        this.data = this.dataService.getCurrentObj();
        if (this.data !== null) {
            this.principal.getLoggedInUser().roles.forEach(role => {
                if (role.includes('role_operator')) {
                    this.isOperator = true;
                }
            });
            this.userFirstName = this.data.userFirstName;
            this.userFirstAndLastName = this.data.userFirstName + ' ' + this.data.userLastName;
            this.groupName = this.data.groupName;
            this.identityType = this.data.identityType;
            this.user = this.data.userName;
            const userInfo: UserInfo = new UserInfo();
            userInfo.username = this.user;
            if (this.data.workflow !== undefined && this.data.workflow !== null) {
                userInfo.workflow = this.data.workflow.name;
            }
            this.isAllowedDevicesApplicable = this.data.workflow.allowedDevicesApplicable;
            this.allowedDevice = this.data.workflow.allowedDevices;
            this.issuanceService.getWorkflowStepsByWorkflow(userInfo.workflow).subscribe(
                workflowSteps => {
                    this.issuanceService
                        .getWorkflowDeviceProfiles(this.principal.getLoggedInUser().organization, userInfo.workflow)
                        .subscribe(
                            (res: DeviceProfile[]) => {
                                res.forEach(e => {
                                    this.supportedDevProfilesFromWorkflow.push(e.configValueProductName.value);
                                });
                                const wfstps = workflowSteps;
                                userInfo.workflowSteps = [];
                                wfstps.forEach(step => {
                                    userInfo.workflowSteps.push(step.name);
                                });
                                this.userToBeIssue = userInfo;
                                this.getWfCertInfo();
                                this.getActions();
                            },
                            error => {
                                this.isLoadingResults = false;
                            }
                        );
                },
                error => {
                    this.isLoadingResults = false;
                }
            );
        } else {
            this.isSelfService = true;
            this.userToBeIssue = this.principal.getLoggedInUser();
            this.userFirstAndLastName = this.principal.getLoggedInUser().firstName + ' ' + this.principal.getLoggedInUser().lastName;
            this.groupName = this.principal.getLoggedInUser().groups[0].name;
            this.identityType = this.principal.getLoggedInUser().identityType;
            this.issuanceService.getUserByName(this.principal.getLoggedInUser().username).subscribe(user => {
                if (user !== null && user.id !== null) {
                    if (user.status === 'PENDING_ENROLLMENT' || user.status === 'ENROLLMENT_IN_PROGRESS') {
                        this.userEnrolled = false;
                        this.issuanceAllowed = false;
                        this.isLoadingResults = false;
                        return;
                    } else {
                        this.userEnrolled = true;
                        if (user.status === 'PENDING_APPROVAL') {
                            this.issuanceAllowed = false;
                            this.isPendingApproval = true;
                            this.isLoadingResults = false;
                        } else {
                            this.userToBeIssue.canIssue = true;
                            if (this.userToBeIssue.canIssue !== null) {
                                this.issuanceAllowed = true;
                                this.issuanceService.getWorkflowGeneralInfoByName(this.userToBeIssue.username).subscribe(workflow => {
                                    this.allowedDevice = workflow.allowedDevices;
                                    this.isAllowedDevicesApplicable = workflow.allowedDevicesApplicable;
                                    this.issuanceService
                                        .getWorkflowDeviceProfiles(
                                            this.principal.getLoggedInUser().organization,
                                            this.principal.getLoggedInUser().workflow
                                        )
                                        .subscribe(res => {
                                            res.forEach(
                                                e => {
                                                    this.supportedDevProfilesFromWorkflow.push(e.configValueProductName.value);
                                                },
                                                error => {
                                                    this.isLoadingResults = false;
                                                }
                                            );
                                            this.getWfCertInfo();
                                            this.getActions();
                                            this.isLoadingResults = false;
                                        });
                                });
                                this.isLoadingResults = false;
                            }
                        }
                    }
                }
            });
        }
        if (this.dataService.getCurrentIdentityDevice() !== null) {
            this.addIdentityDeviceObj = this.dataService.getCurrentIdentityDevice();
            this.addIdentityDeviceTypeName = this.addIdentityDeviceObj.configValueProductName.value;
            console.log(this.dataService.getCurrentIdentityDevice());
        }
        if (this.addIdentityDeviceTypeName !== null && this.addIdentityDeviceTypeName !== undefined) {
            console.log(this.addIdentityDeviceObj.deviceProfileConfig.isRfidCard);
            if (this.addIdentityDeviceObj.deviceProfileConfig.isRfidCard) {
                this.disablePINS = true;
                this.cardType = 'rfid';
                this.clearPinValidators();
            }
            // } else {
            //     if (this.isSelfService) {
            //         this.disablePINS = false;
            //         this.applyPinValidators();
            //     } else {
            //         this.disablePINS = true;
            //         this.clearPinValidators();
            //     }
            // }
        }
        this.hasUserRoleWithOtherRoles = this.principal.getLoggedInUser().hasUserRoleWithOtherRoles;
    }
    getWfCertInfo() {
        this.userToBeIssue.workflowSteps.forEach(step => {
            if (step === 'PKI_CERTIFICATES') {
                this.isCertStepExists = true;
                this.issuanceService
                    .getWfStepDetails(this.userToBeIssue.workflow, 'PKI_CERTIFICATES')
                    .subscribe((res1: WFStepCertConfig[]) => {
                        if (res1.length !== 0) {
                            res1.forEach(e => {
                                this.isKeyEscrow = e.keyEscrow;
                            });
                        }
                    });
            }
        });
    }
    async getActions() {
        this.workflowName = this.userToBeIssue.workflow;
        this.orgName = this.principal.getLoggedInUser().organization;
        this.user = this.userToBeIssue.username;
        const wfStps = this.userToBeIssue.workflowSteps;
        // operator doing for user
        if (this.principal.getLoggedInUser().username !== this.user) {
            this.disablePINS = true;
            this.isSelfService = false;
        }
        const userResponse = await this.issuanceService.getUserByName(this.user).toPromise();
        if (userResponse === null) {
            // set error msg
            this.userEnrolled = false;
            this.isLoadingResults = false;
            return;
        }
        this.userEnrolled = true;
        this.userID = userResponse.id;
        this.issuedDevices = await this.issuanceService.getIssuedDevices(this.userID).toPromise();
        if (this.isAllowedDevicesApplicable) {
            if (this.issuedDevices >= this.allowedDevice) {
                this.isLoadingResults = false;
                // show error msg
                this.translateService.get('issuance.validations.maxAllowedDeviceError', { user: `${this.user}` }).subscribe(
                    msg => {
                        this.issuanceAllowed = false;
                        this.isIssuanceLimitExceeded = true;
                        this.message = msg;
                    },
                    err => {
                        this.message = `Issuance for user: '${
                            this.user
                        }' is not allowed as total issued devices will exceed maximum allowed devices`;
                    }
                );
                return;
            }
        }
        this.issuanceAllowed = true;
        this.issuanceService.getConfiguredVisualCredentialTemplates(this.userID).subscribe(
            response => {
                this.credentialTemplates = response;
                this.persoIdentityDeviceForm.controls['selectedVisualTemplateConfig'].setValue(this.credentialTemplates[0]);
            },
            error => {
                this.isLoadingResults = false;
            }
        );
        const selAction = this.ar.snapshot.paramMap.get('selectedAction');
        this.issuanceService.getWorkflowStepDetail('PERSO_AND_ISSUANCE', this.workflowName).subscribe(
            issuanceStepDetail => {
                if (wfStps.includes('POST_PERSO_AND_ISSUANCE') && !this.isSelfService) {
                    this.isActivationRequired = true;
                }
                if (wfStps.includes('PERSO_AND_ISSUANCE')) {
                    issuanceStepDetail.wfStepGenericConfigs.forEach(stepConfig => {
                        if (stepConfig.name === 'ENABLE_CHIP_PERSO' && stepConfig.value === 'Y') {
                            const allowedGroups: Group[] = this.getAllowedGroup(issuanceStepDetail.wfStepChipEncodeGroupConfigs);
                            if (this.principal.hasAnyGroup(allowedGroups)) {
                                const action = new Action('perso', 'perso', false);
                                this.actions.push(action);
                            }
                        }
                        if (stepConfig.name === 'ENABLE_CHIP_PERSO_PLUS_VISUAL_ID_PRINT' && stepConfig.value === 'Y') {
                            const allowedGroups: Group[] = this.getAllowedGroup(issuanceStepDetail.wfStepChipEncodeVIDPrintGroupConfigs);
                            if (this.principal.hasAnyGroup(allowedGroups)) {
                                const action = new Action('persoAndprint', 'persoAndprint', false);
                                this.actions.push(action);
                            }
                        }
                        if (stepConfig.name === 'ENABLE_VISUAL_ID_PRINT' && stepConfig.value === 'Y') {
                            const allowedGroups: Group[] = this.getAllowedGroup(issuanceStepDetail.wfStepPrintGroupConfigs);
                            if (this.principal.hasAnyGroup(allowedGroups)) {
                                const action = new Action('print', 'print', false);
                                this.actions.push(action);
                            }
                        }
                    });
                    // Logic will trigger after return from Print Visual ID
                    if (selAction !== null) {
                        this.persoIdentityDeviceForm.controls.selectedAction.setValue(selAction);
                    } else {
                        // setting default action radio button
                        if (this.actions.length !== 0) {
                            // if selected dev profile is pvc id(plastic card), then slect print as default option
                            if (this.addIdentityDeviceObj !== undefined) {
                                if (this.addIdentityDeviceObj.deviceProfileConfig.isPlasticId) {
                                    if (this.actions.find(e => e.name === 'print') !== undefined) {
                                        this.actions.forEach(e => {
                                            if (e.name === 'print') {
                                                this.persoIdentityDeviceForm.controls['selectedAction'].setValue('print');
                                            } else {
                                                e.disabled = true;
                                            }
                                        });
                                    } else {
                                        this.persoIdentityDeviceForm.controls['selectedAction'].setValue(this.actions[0].name);
                                    }
                                } else {
                                    this.persoIdentityDeviceForm.controls['selectedAction'].setValue(this.actions[0].name);
                                }
                            } else {
                                this.persoIdentityDeviceForm.controls['selectedAction'].setValue(this.actions[0].name);
                            }
                        }
                    }
                }
                if (this.actions.length === 0) {
                    this.permissionError = true;
                }
                if (this.disablePINS) {
                    this.clearPinValidators();
                }
                if (this.persoIdentityDeviceForm.controls['selectedAction'].value === 'perso') {
                    this.getReadersList();
                    this.isCardReaderSubscribed = true;
                } else if (this.persoIdentityDeviceForm.controls['selectedAction'].value === 'persoAndprint') {
                    this.getPrintersList();
                    this.isPrinterListSubscribed = true;
                } else if (this.persoIdentityDeviceForm.controls['selectedAction'].value === 'print') {
                    this.getPrintersList();
                    this.printSelected = true;
                }
                this.isLoadingResults = false;
            },
            error => {
                this.isLoadingResults = false;
            }
        );
        if (selAction !== null) {
            this.persoIdentityDeviceForm.controls.selectedAction.setValue(selAction);
        }
    }
    getReadersList() {
        this.subscription = timer(0, 2000)
            .pipe(switchMap(() => this.deviceClientService.getCardReaders()))
            .subscribe(
                readersList => {
                    const list = readersList.cardReaderList;
                    this.flag = false;
                    let updateList = false;
                    if (readersList.responseCode === 'DS0000') {
                        const selectedReader = this.persoIdentityDeviceForm.controls['identityDevice'].value;
                        const cr = this.persoIdentityDeviceForm.controls['identityDevice'].value;
                        if (cr === null || cr === undefined) {
                            this.cardReaders = list;
                            this.persoIdentityDeviceForm.controls['identityDevice'].setValue(this.cardReaders[0]);
                            this.setCardName(this.cardReaders[0]);
                            return;
                        }
                        if (this.cardReaders.length === list.length || this.cardReaders.length > list.length) {
                            // if any reader was removed or no reader connected
                            let isFound = false;
                            for (const c of this.cardReaders) {
                                const found = list.find(e => e.cardReaderName === c.cardReaderName);
                                if (found === undefined) {
                                    updateList = true;
                                } else {
                                    if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                    }
                                }
                            }
                            const card1 = list.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (card1 !== undefined) {
                                if (!isFound || this.selectedCardName === '' || card1.atr !== this.selectedCardAtr) {
                                    if (card1 !== undefined) {
                                        console.log('calling card readers');
                                        this.setCardName(card1);
                                    }
                                }
                            }
                        } else if (this.cardReaders.length < list.length) {
                            // if any new reader was connected
                            let isFound = false;
                            for (const c of list) {
                                const found = this.cardReaders.find(e => e.cardReaderName === c.cardReaderName);
                                if (found === undefined) {
                                    updateList = true;
                                } else {
                                    if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                    }
                                }
                            }
                            const card1 = list.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (card1 !== undefined) {
                                if (!isFound || this.selectedCardName === '' || card1.atr !== this.selectedCardAtr) {
                                    if (card1 !== undefined) {
                                        this.setCardName(card1);
                                    }
                                }
                            }
                        }
                        if (updateList) {
                            this.cardReaders = list;
                            const cardReader: CardReader = this.cardReaders.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (cardReader !== undefined) {
                                this.persoIdentityDeviceForm.controls['identityDevice'].setValue(cardReader);
                                if (cardReader.atr !== this.selectedCardAtr) {
                                    this.setCardName(cardReader);
                                }
                            } else {
                                this.persoIdentityDeviceForm.controls['identityDevice'].setValue(this.cardReaders[0]);
                                if (this.cardReaders[0].atr !== this.selectedCardAtr) {
                                    this.setCardName(this.cardReaders[0]);
                                }
                            }
                        }
                    } else {
                        this.persoIdentityDeviceForm.controls['identityDevice'].setValue(null);
                        this.cardReaders = [];
                        this.selectedCardAtr = '';
                        this.selectedCardName = '';
                        this.notSupportedCard = false;
                        this.translateService.get('issuance.connectReaderAndCard').subscribe(msg => {
                            this.errorMsg = msg;
                            this.error = true;
                        });
                        window.scrollTo(0, 0);
                    }
                    this.flag = true;
                },
                error => {
                    this.notSupportedCard = false;
                    this.selectedCardAtr = '';
                    this.selectedCardName = '';
                    this.isLoadingResults = false;
                    if (this.subscription) {
                        this.subscription.unsubscribe();
                    }
                    this.cardReaders = [];
                    this.persoIdentityDeviceForm.controls['identityDevice'].setValue(null);
                    this.error = true;
                    this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                        this.errorMsg = serviceErrMessage;
                        // Retrying
                        this.getReadersList();
                    });
                }
            );
    }
    setCardName(cardReader: CardReader) {
        const atr = cardReader.atr;
        if (atr.length !== 0) {
            console.log('set Card Name');
            this.error = false;
            const newAtr = atr.replace(/ /g, '');
            this.issuanceService.getCardNameByAtr(newAtr).subscribe(name => {
                console.log(this.selectedCardName !== name, name, this.selectedCardName);
                if (this.selectedCardName !== name) {
                    this.selectedCardAtr = atr;
                    console.log(this.persoAndPrintSelected);
                    this.selectedCardName = name;
                    console.log(this.persoIdentityDeviceForm.get('selectedAction').value === 'persoAndprint');
                    if (this.persoIdentityDeviceForm.get('selectedAction').value === 'persoAndprint') {
                        console.log(this.selectedPrinter);
                        if (this.selectedPrinter !== null) {
                            // if (this.selectedPrinter.status === 'Busy') {
                            console.log('inside');
                            this.isCardinChipEncodePosition = true;
                            // }
                        }
                    }
                    if (!this.supportedDevProfilesFromWorkflow.includes(this.selectedCardName)) {
                        // show a warning message.....
                        console.log('inside error msg');
                        this.notSupportedCard = true;
                    } else if (this.addIdentityDeviceTypeName !== null && this.addIdentityDeviceTypeName !== undefined) {
                        if (this.addIdentityDeviceTypeName !== this.selectedCardName) {
                            this.error = true;
                            this.notSupportedCard = false;
                            this.translateService
                                .get('issuance.validations.incorrectCardSelected', {
                                    connectedCard: `${this.selectedCardName}`,
                                    selectedDevice: `${this.addIdentityDeviceTypeName}`
                                })
                                .subscribe(msg => {
                                    this.errorMsg = msg;
                                    window.scrollTo(0, 0);
                                    return null;
                                });
                        } else {
                            this.notSupportedCard = false;
                        }
                        console.log(this.notSupportedCard);
                    }
                    // else {
                    //     this.notSupportedCard = false;
                    // }
                } else {
                    console.log('in else::::');
                    if (!this.supportedDevProfilesFromWorkflow.includes(this.selectedCardName)) {
                        // show a warning message.....
                        console.log('inside error msg:::!');
                        this.notSupportedCard = true;
                    }
                }
            });
        } else {
            console.log(this.persoAndPrintSelected);
            console.log('calling card readers1');
            if (!this.persoAndPrintSelected) {
                console.log('setCard');
                this.translateService.get('issuance.identityDeviceNotFound').subscribe(msg => {
                    this.error = true;
                    this.errorMsg = msg;
                    this.notSupportedCard = false;
                    this.selectedCardAtr = '';
                    this.selectedCardName = '';
                    window.scrollTo(0, 0);
                });
            } else {
                // const selectedPrinter = this.persoIdentityDeviceForm.controls['cardPrinter'].value;
                const selectedPrinter = this.selectedPrinter;
                console.log('undet set card name' + selectedPrinter.status);
                if (selectedPrinter !== null && selectedPrinter !== undefined) {
                    if (selectedPrinter.status === 'Ready') {
                        this.error = false;
                    } else if (selectedPrinter.status === 'Error') {
                        console.log('inside error');
                        this.translateService.get('issuance.noCardInPrinter').subscribe(msg => {
                            this.error = true;
                            this.errorMsg = msg;
                            this.notSupportedCard = false;
                            this.selectedCardAtr = '';
                            this.selectedCardName = '';
                            window.scrollTo(0, 0);
                        });
                    } else {
                        this.error = false;
                    }
                }
            }
        }
    }
    callCardReadersAndPlaceCardInChipEncodePosition(cardPrinter: Printer) {
        if (!this.printSelected) {
            if (!this.isCardReaderSubscribed) {
                this.isCardReaderSubscribed = true;
                this.getReadersList();
            } else if (this.subscription.closed) {
                this.getReadersList();
            }
        }
        //  position the card in chip encode position if its not in chip encode position
        // and check for the card type to place the card in contact r cntcless position
        /** if (
            (cardPrinter.status === 'Ready' || cardPrinter.status === 'Error') &&
            !this.isCardinChipEncodePosition
        ) {
            this.issuanceService.positionCard('ContactEncoderPosition').subscribe(res => {
                console.log(res);
                if (res.printerResponse.accepted === 'true') {
                    this.isCardinChipEncodePosition = true;
                    this.error = false;
                } else {
                    this.error = true;
                    console.log(res.printerResponse.error === this.errorMsg);
                    this.errorMsg = res.printerResponse.error;
                    window.scrollTo(0, 0);
                }
            });
        } else if (cardPrinter.status === 'Busy') {
            this.error = false;
            this.isCardinChipEncodePosition = true;
        } */
    }
    getPrintersList() {
        this.printerSubscription = timer(0, 3000)
            .pipe(switchMap(() => this.deviceClientService.getConnectedPrinters()))
            .subscribe(
                printerList => {
                    const list: Printer[] = printerList.printerResponse.filter(
                        p => p.status !== 'Offline' && p.status !== 'Unkown' && p.status !== undefined
                    );
                    let updateList = false;
                    if (printerList.responseCode === 'DS0000') {
                        if (list.length !== 0) {
                            const selectedPrinter1 = this.persoIdentityDeviceForm.controls['cardPrinter'].value;
                            const cp = this.persoIdentityDeviceForm.controls['cardPrinter'].value;
                            console.log(cp);
                            if (cp === null || cp === undefined) {
                                this.cardPrinters = list;
                                this.persoIdentityDeviceForm.controls['cardPrinter'].setValue(this.cardPrinters[0]);
                                this.callCardReadersAndPlaceCardInChipEncodePosition(this.cardPrinters[0]);
                                this.selectedPrinter = this.persoIdentityDeviceForm.controls['cardPrinter'].value;
                                return;
                            }
                            this.callCardReadersAndPlaceCardInChipEncodePosition(this.cardPrinters[0]);
                            if (this.cardPrinters.length === list.length || this.cardPrinters.length > list.length) {
                                console.log('inside same printers');
                                let isFound = false;
                                for (const c of this.cardPrinters) {
                                    const found = list.find(e => e.manufacturer === c.manufacturer && e.printerName === c.printerName);
                                    if (found === undefined) {
                                        updateList = true;
                                    } else {
                                        // if (found.manufacturer === this.selectedPrinter.manufacturer && found.printerName === this.selectedPrinter.printerName) {
                                        isFound = true;
                                        // }
                                    }
                                }
                                const printer1 = list.find(
                                    e => e.manufacturer === selectedPrinter1.manufacturer && e.printerName === selectedPrinter1.printerName
                                );
                                if (printer1 !== undefined) {
                                    console.log(printer1);
                                    if (!isFound || printer1.printerName !== this.selectedPrinter.printerName) {
                                        if (printer1 !== undefined) {
                                            console.log('inside', printer1);
                                            this.selectedPrinter = printer1;
                                            this.persoIdentityDeviceForm.controls['cardPrinter'].setValue(printer1);
                                        }
                                    } else {
                                        this.selectedPrinter = printer1;
                                        console.log('this.selectedPrinter1', this.selectedPrinter);
                                        // this.persoIdentityDeviceForm.controls['cardPrinter'].setValue(printer1);
                                    }
                                }
                            } else if (this.cardPrinters.length < list.length) {
                                console.log('inside new printers connected');
                                let isFound = false;
                                for (const c of list) {
                                    const found = this.cardPrinters.find(e => e.printerName === c.printerName);
                                    if (found === undefined) {
                                        updateList = true;
                                    } else {
                                        // if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                        //  }
                                    }
                                }
                                const printer1 = list.find(e => e.printerName === selectedPrinter1.printerName);
                                if (printer1 !== undefined) {
                                    if (!isFound || printer1.printerName !== this.selectedPrinter.printerName) {
                                        if (printer1 !== undefined) {
                                            this.selectedPrinter = printer1;
                                            console.log('this.selectedPrinter2', this.selectedPrinter);
                                            this.callCardReadersAndPlaceCardInChipEncodePosition(this.selectedPrinter);
                                        }
                                    } else {
                                        this.selectedPrinter = printer1;
                                        // this.persoIdentityDeviceForm.controls['cardPrinter'].setValue(printer1);
                                    }
                                }
                            }
                            if (updateList) {
                                this.cardPrinters = list;
                                const cardPrinter: Printer = this.cardPrinters.find(e => e.printerName === selectedPrinter1.printerName);
                                if (cardPrinter !== undefined) {
                                    this.persoIdentityDeviceForm.controls['cardPrinter'].setValue(cardPrinter);
                                    if (cardPrinter.printerName !== this.selectedPrinter.printerName) {
                                        this.selectedPrinter = cardPrinter;
                                        this.callCardReadersAndPlaceCardInChipEncodePosition(this.selectedPrinter);
                                    }
                                } else {
                                    this.persoIdentityDeviceForm.controls['cardPrinter'].setValue(this.cardReaders[0]);
                                    if (this.cardPrinters[0].printerName !== this.selectedPrinter.printerName) {
                                        this.selectedPrinter = cardPrinter;
                                        this.callCardReadersAndPlaceCardInChipEncodePosition(this.selectedPrinter);
                                    }
                                }
                            }
                        } else {
                            this.translateService.get('issuance.printerNotFound').subscribe(msg => {
                                this.error = true;
                                this.isLoadingResults = false;
                                this.errorMsg = msg;
                                window.scrollTo(0, 0);
                            });
                            this.persoIdentityDeviceForm.controls['cardPrinter'].setValue(null);
                            this.cardPrinters = [];
                            this.isCardinChipEncodePosition = false;
                            if (this.subscription) {
                                this.subscription.unsubscribe();
                            }
                            this.persoIdentityDeviceForm.controls['identityDevice'].setValue(null);
                            this.cardReaders = [];
                        }
                    } else {
                        this.error = true;
                        this.notSupportedCard = false;
                        this.selectedCardAtr = '';
                        this.selectedCardName = '';
                        this.translateService.get('issuance.printerNotFound').subscribe(msg => {
                            this.isLoadingResults = false;
                            this.errorMsg = msg;
                            window.scrollTo(0, 0);
                        });
                        this.persoIdentityDeviceForm.controls['cardPrinter'].setValue(null);
                        this.cardPrinters = [];
                        this.isCardinChipEncodePosition = false;
                        if (this.subscription) {
                            this.subscription.unsubscribe();
                        }
                        this.persoIdentityDeviceForm.controls['identityDevice'].setValue(null);
                        this.cardReaders = [];
                        return;
                    }
                },
                error => {
                    this.isCardinChipEncodePosition = false;
                    this.isLoadingResults = false;
                    if (this.printerSubscription) {
                        this.printerSubscription.unsubscribe();
                    }
                    this.persoIdentityDeviceForm.controls['cardPrinter'].setValue(null);
                    this.cardPrinters = [];
                    if (this.subscription) {
                        this.subscription.unsubscribe();
                    }
                    this.cardReaders = [];
                    this.persoIdentityDeviceForm.controls['identityDevice'].setValue(null);

                    this.error = true;
                    this.notSupportedCard = false;
                    this.selectedCardAtr = '';
                    this.selectedCardName = '';
                    this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                        // if (this.errorMsg !== serviceErrMessage) {
                        this.errorMsg = serviceErrMessage;
                        // }
                        // Retrying
                        this.getPrintersList();
                    });
                }
            );
    }
    showSupportedDevices() {
        this.dialog.open(SupportedDevicesDialogComponent, {
            autoFocus: false,
            width: '400px',
            data: { devices: this.supportedDevProfilesFromWorkflow }
        });
    }
    buildIssuanceForm() {
        this.persoIdentityDeviceForm = this.formBuilder.group({
            selectedAction: [],
            identityDevice: [], // in perso and persoAndPrint
            cardPrinter: [], // in persoAndPrint
            selectedVisualTemplateConfig: [''],
            pin: ['', [Validators.required, Validators.pattern('^([0-9]{6,8})$')]],
            confirmPin: ['', [Validators.required, Validators.pattern('^([0-9]{6,8})$'), compareValidator('pin', 'compare')]]
        });
        this.formControlValueChanged();
    }

    getAllowedGroup(groups: WfStepGroupConfig[]): Group[] {
        const allowedGroups: Group[] = [];
        groups.forEach(element => {
            const group = new Group();
            group.id = element.groupId;
            group.name = element.groupName;
            allowedGroups.push(group);
        });
        return allowedGroups;
    }
    applyPinValidators() {
        const pin = this.persoIdentityDeviceForm.get('pin');
        const confirmPin = this.persoIdentityDeviceForm.get('confirmPin');
        pin.setValidators([Validators.required, Validators.pattern('^([0-9]{6,8})$')]);
        confirmPin.setValidators([Validators.required, Validators.pattern('^([0-9]{6,8})$'), compareValidator('pin', 'compare')]);
        pin.updateValueAndValidity();
        confirmPin.updateValueAndValidity();
    }
    clearPinValidators() {
        const pin = this.persoIdentityDeviceForm.get('pin');
        const confirmPin = this.persoIdentityDeviceForm.get('confirmPin');
        // get pin randomly
        pin.setValue(null);
        confirmPin.setValue(null);
        pin.clearValidators();
        confirmPin.clearValidators();
        pin.updateValueAndValidity();
        confirmPin.updateValueAndValidity();
    }
    issuanceRadioSelectionChange(event: any) {
        const action = event.value;
        if (action === 'perso') {
            this.printSelected = false;
            this.persoAndPrintSelected = false;
            if (!this.printerSubscription.closed) {
                this.printerSubscription.unsubscribe();
            }
            if (!this.isCardReaderSubscribed) {
                this.isCardReaderSubscribed = true;
                this.getReadersList();
            }
            if (this.subscription.closed) {
                this.getReadersList();
            }
        } else if (action === 'persoAndprint') {
            this.notSupportedCard = false;
            this.selectedCardAtr = '';
            this.selectedCardName = '';
            this.printSelected = false;

            this.persoAndPrintSelected = true;
            if (!this.isPrinterListSubscribed) {
                this.isPrinterListSubscribed = true;
                this.getPrintersList();
            }
            // if (!this.isCardReaderSubscribed) {
            //     this.isCardReaderSubscribed = true;
            //     this.getReadersList();
            // }
            if (this.printerSubscription.closed) {
                this.getPrintersList();
            }
            this.persoIdentityDeviceForm.controls['identityDevice'].setValue(null);
            this.cardReaders = [];
            this.notSupportedCard = false;
            this.selectedCardAtr = '';
            this.selectedCardName = '';
            if (!this.subscription.closed) {
                this.subscription.unsubscribe();
            }
        } else if (action === 'print') {
            this.printSelected = true;
            this.persoAndPrintSelected = false;
            this.notSupportedCard = false;
            this.error = false;
            if (!this.printerSubscription.closed) {
                this.isPrinterListSubscribed = true;
                this.getPrintersList();
            }
            if (this.printerSubscription.closed) {
                this.getPrintersList();
            }
            this.subscription.unsubscribe();
        }
    }
    formControlValueChanged() {
        const identityDevice = this.persoIdentityDeviceForm.controls.identityDevice;
        const printer = this.persoIdentityDeviceForm.controls.cardPrinter;
        const selectedVisualTemplateConfig = this.persoIdentityDeviceForm.controls.selectedVisualTemplateConfig;
        this.persoIdentityDeviceForm.get('selectedAction').valueChanges.subscribe(mode => {
            if (mode === 'perso') {
                this.printSelected = false;
                this.persoAndPrintSelected = false;
                identityDevice.setValidators([Validators.required]);
                printer.clearValidators();
                selectedVisualTemplateConfig.clearValidators();
                if (!this.disablePINS) {
                    this.applyPinValidators();
                }
            }
            if (mode === 'persoAndprint') {
                this.printSelected = false;
                console.log('entered');
                this.persoAndPrintSelected = true;
                identityDevice.setValidators([Validators.required]);
                printer.setValidators([Validators.required]);
                selectedVisualTemplateConfig.setValidators([Validators.required]);
                if (!this.disablePINS) {
                    this.applyPinValidators();
                }
            }
            if (mode === 'print') {
                this.printSelected = true;
                this.persoAndPrintSelected = false;
                this.clearPinValidators();
                printer.clearValidators();
                identityDevice.clearValidators();
                selectedVisualTemplateConfig.setValidators([Validators.required]);
            }
            printer.updateValueAndValidity();
            identityDevice.updateValueAndValidity();
            selectedVisualTemplateConfig.updateValueAndValidity();
        });
    }

    issue(action: string) {
        this.actionSelected = action;
        console.log(this.persoIdentityDeviceForm.controls['selectedVisualTemplateConfig'].value, this.credentialTemplates[0]);
        if (this.notSupportedCard) {
            return;
        } else {
            let n = 0;
            this.timer = setInterval(() => {
                if (this.flag && n === 0) {
                    n++;
                    // this.error = false;
                    if (this.persoIdentityDeviceForm.get('selectedAction').value === 'persoAndprint') {
                        this.isLoadingResults = true;
                        this.persoAndPrintSelected = true;
                        if (this.selectedPrinter === null) {
                            this.translateService.get('issuance.printerNotFound').subscribe(msg => {
                                this.error = true;
                                this.errorMsg = msg;
                                this.isLoadingResults = false;
                                window.scrollTo(0, 0);
                                return;
                            });
                        }
                        // place the card in chip encode position, it should be based on card type
                        if (this.selectedPrinter) {
                            const selectedPrinter: Printer = this.selectedPrinter;
                            console.log(selectedPrinter);
                            console.log('called printer');
                            if (this.cardType === 'rfid') {
                                this.cardPositionFromPrinter = 'ContactLessPosition';
                            } else {
                                this.cardPositionFromPrinter = 'ContactEncoderPosition';
                            }
                            //  this.issuanceService.getPrinterStatus().subscribe(
                            //   statusResponse => {
                            if (selectedPrinter.status === 'Ready') {
                                this.deviceClientService
                                    .positionCard(this.cardPositionFromPrinter, selectedPrinter.manufacturer, selectedPrinter.printerName)
                                    .subscribe(
                                        response => {
                                            console.log(response);
                                            console.log('response:::', response);
                                            console.log(this.persoIdentityDeviceForm.controls['selectedVisualTemplateConfig'].value);
                                            if (response.responseCode === 'DS0000') {
                                                if (response.printerResponse.accepted === 'true') {
                                                    console.log('chip encode position successful');
                                                    this.isCardinChipEncodePosition = true;
                                                    this.error = false;
                                                    setTimeout(() => {
                                                        //  this.isLoadingResults = false;
                                                        const c = this.checkConnections();
                                                        console.log(c);
                                                    }, 8000);
                                                } else {
                                                    this.isLoadingResults = false;
                                                    // this.translateService.get('issuance.identityDeviceNotFound').subscribe(msg => {
                                                    this.error = true;
                                                    this.errorMsg = response.printerResponse.error;
                                                    this.notSupportedCard = false;
                                                    this.selectedCardAtr = '';
                                                    this.selectedCardName = '';
                                                    window.scrollTo(0, 0);
                                                    return;
                                                    // });
                                                }
                                                /** else {
                                                    this.error = true;
                                                    this.errorMsg = response.printerResponse.error;
                                                    window.scrollTo(0, 0);
                                                    this.isLoadingResults = false;
                                                    return;
                                                } */
                                            } else {
                                                this.translateService.get(`deviceService.${response.responseCode}`).subscribe(message => {
                                                    console.log('else case');
                                                    this.error = true;
                                                    this.errorMsg = message;
                                                    window.scrollTo(0, 0);
                                                    this.isLoadingResults = false;
                                                    return;
                                                });
                                                // this.translateService.get(`deviceService.${response.responseCode}`).subscribe()
                                            }
                                            /**  if (response.printerResponse.accepted === 'Error') {
                                            console.log('Please position card!!');
                                            this.error = true;
                                            this.errorMsg = 'Please position card!!';
                                            window.scrollTo(0, 0);
                                            return;
                                        } */
                                        },
                                        err => {
                                            this.isLoadingResults = false;
                                        }
                                    );
                            } else if (selectedPrinter.status === 'Offline') {
                                this.translateService.get('issuance.printerNotFound').subscribe(msg => {
                                    this.error = true;
                                    this.errorMsg = msg;
                                    this.isLoadingResults = false;
                                    window.scrollTo(0, 0);
                                    return;
                                });
                            } else if (selectedPrinter.status === 'Error') {
                                this.translateService.get('issuance.noCardInPrinter').subscribe(msg => {
                                    this.error = true;
                                    this.errorMsg = msg;
                                    this.isLoadingResults = false;
                                    window.scrollTo(0, 0);
                                    return;
                                });
                            } else {
                                setTimeout(() => {
                                    const c = this.checkConnections();
                                    console.log(c);
                                }, 8000);
                            }
                            //  },
                            // err => {
                            //      this.isLoadingResults = false;
                            //  }
                            //  );
                        } else {
                            this.isLoadingResults = false;
                        }
                    } else {
                        this.persoAndPrintSelected = false;
                        if (this.error) {
                            window.scrollTo(0, 0);
                            return;
                        }
                        this.persoAndPrintSelected = false;
                        this.checkConnections();
                    }
                }
            }, 1000);
        }
    }
    checkConnections() {
        console.log(this.persoIdentityDeviceForm.controls.identityDevice.value);
        if (this.persoIdentityDeviceForm.controls.identityDevice.value === null) {
            //  this.error = true;
            this.notSupportedCard = false;
            this.translateService.get('issuance.connectReaderAndCard').subscribe(msg => {
                this.errorMsg = msg;
                this.error = true;
                window.scrollTo(0, 0);
                this.isLoadingResults = false;
                return null;
            });
        }
        console.log(this.persoIdentityDeviceForm.controls.identityDevice.value);
        this.deviceClientService
            .getAtrByReaderName(this.persoIdentityDeviceForm.controls.identityDevice.value.urlEncodedcardReaderName)
            .subscribe(response => {
                const atr = response.atr;
                console.log('atr coming', atr);
                if (atr === '') {
                    this.translateService.get('issuance.identityDeviceNotFound').subscribe(msg => {
                        this.error = true;
                        this.isLoadingResults = false;
                        this.errorMsg = msg;
                        window.scrollTo(0, 0);
                        return null;
                    });
                } else {
                    this.checkForSameCardsConnection(atr);
                }
            });
    }

    checkForSameCardsConnection(atr) {
        // check whether the card connected and add identity device type is same or not
        if (this.addIdentityDeviceTypeName !== null && this.addIdentityDeviceTypeName !== undefined) {
            if (this.addIdentityDeviceTypeName !== this.selectedCardName) {
                this.translateService
                    .get('issuance.validations.incorrectCardSelected', {
                        connectedCard: `${this.selectedCardName}`,
                        selectedDevice: `${this.addIdentityDeviceTypeName}`
                    })
                    .subscribe(msg => {
                        this.error = true;
                        this.errorMsg = msg;
                        this.isLoadingResults = false;
                        window.scrollTo(0, 0);
                        // if (this.persoAndPrintSelected) {
                        //    this.ejectCardFromPrinter();
                        // }
                        return null;
                    });
            } else {
                this.isSame = true;
                this.startIssuance(atr);
            }
        } else {
            // this.isLoadingResults = false;
            this.startIssuance(atr);
        }
    }

    startIssuance(atr) {
        this.printerSubscription.unsubscribe();
        this.subscription.unsubscribe();

        this.isLoadingResults = false;
        this.display = false;
        this.progressClass = 'progress-bar bg-success';
        // removing the white spaces in atr
        const newAtr = atr.replace(/ /g, '');
        const cardReaderName = this.persoIdentityDeviceForm.controls.identityDevice.value.cardReaderName;
        // start identify process
        this.issuanceService.start('identify', newAtr).subscribe(
            identifyStartResponse => {
                // execute the commands to card
                const deviceServiceReq1 = identifyStartResponse.body;
                deviceServiceReq1.state = 0;
                this.deviceClientService.execute(deviceServiceReq1, cardReaderName).subscribe(
                    identifyCommandsRes => {
                        this.issuanceService.next(identifyCommandsRes.body).subscribe(
                            identifyNextRes => {
                                let nextResponse = new Commands();
                                nextResponse = identifyNextRes.body;
                                this.message = nextResponse.entries[0].message;
                                if (nextResponse.step === 'done') {
                                    this.width = 2;
                                    this.message = nextResponse.entries[0].message;
                                    // get UUID
                                    this.identifyProcessResponse = identifyNextRes;
                                    this.issuanceService
                                        .validateDeviceBeforeIssuance(
                                            identifyNextRes.body.entries[0].result.serial,
                                            'issuance',
                                            this.userID
                                        )
                                        .subscribe(
                                            (issaunceValidateResponse: DeviceValidator) => {
                                                if (issaunceValidateResponse.action === true) {
                                                    // 200 - revoked   201--removed
                                                    if (issaunceValidateResponse.statusCode === 200) {
                                                        // show a dialog or warning message
                                                        this.translateService
                                                            .get('issuance.validations.deviceIssuanceWarningMsg')
                                                            .subscribe(msg => {
                                                                this.showWarningDialog(identifyNextRes, cardReaderName, msg);
                                                                return;
                                                            });
                                                    } else {
                                                        this.resumeIssuanceProcess(identifyNextRes, cardReaderName);
                                                    }
                                                } else {
                                                    this.progressClass = 'progress-bar bg-danger';
                                                    this.failure = true;
                                                    this.success = false;
                                                    if (issaunceValidateResponse.message === 'Device_Printed') {
                                                        this.translateService
                                                            .get('issuance.validations.devicePrinted')
                                                            .subscribe(message => (this.message = message));
                                                    } else {
                                                        this.translateService
                                                            .get('issuance.validations.deviceValidation', {
                                                                status: `${issaunceValidateResponse.message}`
                                                            })
                                                            .subscribe(message => (this.message = message));
                                                    }
                                                    // if (this.persoAndPrintSelected) {
                                                    //     this.ejectCardFromPrinter();
                                                    // }
                                                }
                                            },
                                            error => {
                                                console.log(error);
                                                this.progressClass = 'progress-bar bg-danger';
                                                this.failure = true;
                                                this.success = false;
                                                this.message = 'Error while validating device';
                                                // if (this.persoAndPrintSelected) {
                                                //     this.ejectCardFromPrinter();
                                                // }
                                            }
                                        );
                                } else {
                                    this.progressClass = 'progress-bar bg-danger';
                                    this.failure = true;
                                    this.success = false;
                                    this.message = nextResponse.entries[0].message;
                                    // if (this.persoAndPrintSelected) {
                                    //     this.ejectCardFromPrinter();
                                    // }
                                    return throwError(nextResponse.entries[0].message);
                                }
                            },
                            error => {
                                this.progressClass = 'progress-bar bg-danger';
                                this.failure = true;
                                this.success = false;
                                this.message = error;
                                // if (this.persoAndPrintSelected) {
                                //     this.ejectCardFromPrinter();
                                // }
                            }
                        );
                    },
                    error => {
                        this.progressClass = 'progress-bar bg-danger';
                        this.failure = true;
                        this.success = false;
                        this.message = error.message;
                        // if (this.persoAndPrintSelected) {
                        //     this.ejectCardFromPrinter();
                        // }
                    }
                );
            },
            error => {
                this.progressClass = 'progress-bar bg-danger';
                this.failure = true;
                this.success = false;
                this.message = error.message;
                // if (this.persoAndPrintSelected) {
                //     this.ejectCardFromPrinter();
                // }
            }
        );
        this.subscription.unsubscribe();
        this.printerSubscription.unsubscribe();
        clearTimeout(this.timer);
    }
    showWarningDialog(identifyNextRes, cardReaderName, dialogMsg) {
        const dialogPosition: DialogPosition = {
            top: '20px'
        };
        const dialogRef = this.dialog.open(ReActivateDialogComponent, {
            autoFocus: false,
            width: '650px',
            position: dialogPosition,
            data: {
                isShowPUK: false,
                dialogMsg: dialogMsg
            },
            disableClose: true
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result === 'OK') {
                this.resumeIssuanceProcess(identifyNextRes, cardReaderName);
            } else {
                this.refresh();
            }
        });
    }
    resumeIssuanceProcess(identifyNextRes, cardReaderName) {
        const gpLockApplicable = identifyNextRes.body.entries[0].result.GpLockApplicable;
        this.remainingWidth = 100 - this.width;
        let count = 0;
        const issuanceStagesCount = +identifyNextRes.body.entries[0].result.stagesCount;
        if (gpLockApplicable === 'true') {
            this.isGpLockApplicable = true;
        } else {
            this.isGpLockApplicable = false;
        }
        if (!this.isGpLockApplicable) {
            this.isActivationRequired = false;
        }
        if (this.isCertStepExists) {
            count = count + 1;
        }
        if (this.isKeyEscrow) {
            count = count + 1;
        }
        if (this.isGpLockApplicable && this.isActivationRequired) {
            count = count + 2; // +2 is for gp lock
        }
        if (identifyNextRes.body.entries[0].result.appLock === 'true') {
            this.isCardLocked = true;
            count = count + 2; // 2 is for gp unlock
            count = count + issuanceStagesCount;
            // if (this.persoAndPrintSelected) {
            //     // adding 3 steps for printing
            //     this.remainingWidth = this.remainingWidth + 3;
            // }
            this.widthIncrementCountBy = Math.floor(this.remainingWidth / count);
            // If card is locked, issuance proceeded after unlocking the card
            this.startProcess(identifyNextRes, 'gpunlock', cardReaderName);
        } else {
            this.isCardLocked = false;
            count = count + issuanceStagesCount;
            this.widthIncrementCountBy = Math.floor(this.remainingWidth / count);
            if (identifyNextRes.body.entries[0].result.isRfidCard === 'true') {
                this.processName = 'rfidissuance';
            } else {
                this.processName = 'issuance';
            }
            this.startProcess(identifyNextRes, this.processName, cardReaderName);
        }
    }
    startProcess(response, processName, cardReaderName) {
        this.processName = processName;
        const cmsSyncRequest = this.buildCmsSyncRequest(response, processName);
        this.issuanceService.getUUID(processName, cmsSyncRequest).subscribe(
            uuid => {
                // start issuance process
                this.issuanceService.start(processName, uuid).subscribe(
                    issaunceStartRes => {
                        // this.width = this.width + 4;
                        this.width = this.width + this.widthIncrementCountBy;
                        // send the commands to card
                        this.deviceClientService.execute(issaunceStartRes.body, cardReaderName).subscribe(
                            result => {
                                this.message = issaunceStartRes.body.stageStatus;
                                this.issuanceService.next(result.body).subscribe(
                                    res => {
                                        this.recurse(res.body, cardReaderName);
                                    },
                                    error => {
                                        this.progressClass = 'progress-bar bg-danger';
                                        this.failure = true;
                                        this.success = false;
                                        this.message = error;
                                        // if (this.persoAndPrintSelected) {
                                        //     this.ejectCardFromPrinter();
                                        // }
                                    }
                                );
                            },
                            error => {
                                this.progressClass = 'progress-bar bg-danger';
                                this.failure = true;
                                this.success = false;
                                this.message = error;
                                // if (this.persoAndPrintSelected) {
                                //     this.ejectCardFromPrinter();
                                // }
                            }
                        );
                    },
                    error => {
                        this.progressClass = 'progress-bar bg-danger';
                        this.failure = true;
                        this.success = false;
                        this.message = error;
                        // if (this.persoAndPrintSelected) {
                        //     this.ejectCardFromPrinter();
                        // }
                    }
                );
            },
            error => {
                this.progressClass = 'progress-bar bg-danger';
                this.failure = true;
                this.success = false;
                this.message = error;
                // if (this.persoAndPrintSelected) {
                //     this.ejectCardFromPrinter();
                // }
            }
        );
    }

    recurse(res, cardReaderName) {
        if (res === null) {
            this.progressClass = 'progress-bar bg-danger';
            this.failure = true;
            this.success = false;
            this.translateService.get('issuance.errorWhileIssuance').subscribe(msg => {
                this.message = msg;
            });
            return throwError(res.step);
        }
        if (res.stage !== 'done' && res.step !== 'error') {
            if (res.step === 'done') {
                this.width = this.width + this.widthIncrementCountBy;
            }
            this.message = res.stageStatus;
            // if (this.processName === 'gplock' || this.processName === 'gpunlock') {
            //     // this.width = this.width + 2;
            //     this.width = this.width + this.widthIncrementCountBy;
            // } else {
            //     if (this.persoIdentityDeviceForm.get('selectedAction').value === 'persoAndprint') {
            //         // this.width = this.width + 3;
            //         this.width = this.width + this.widthIncrementCountBy;
            //     } else {
            //         console.log(this.width);
            //         // this.width = this.width + 4;
            //         this.width = this.width + this.widthIncrementCountBy;
            //     }
            // }
            res.state = 1;
            this.deviceClientService.execute(res, cardReaderName).subscribe(
                result1 => {
                    this.issuanceService.next(result1.body).subscribe(
                        response => {
                            this.recurse(response.body, cardReaderName);
                        },
                        error => {
                            this.progressClass = 'progress-bar bg-danger';
                            this.failure = true;
                            this.success = false;
                            this.message = error.message;
                            // if (this.persoAndPrintSelected) {
                            //     this.ejectCardFromPrinter();
                            // }
                        }
                    );
                },
                error => {
                    this.progressClass = 'progress-bar bg-danger';
                    this.failure = true;
                    this.success = false;
                    this.message = error.message;
                    // if (this.persoAndPrintSelected) {
                    //     this.ejectCardFromPrinter();
                    // }
                }
            );
        } else if (res.step === 'error' || res.stage === 'error') {
            this.progressClass = 'progress-bar bg-danger';
            this.failure = true;
            this.success = false;
            this.translateService.get('issuance.errorWhileIssuance').subscribe(msg => {
                this.message = msg;
            });
            if (res.entries[0].message !== null) {
                if (res.entries[0].message.includes('_')) {
                    this.errorCodeMsgDisplay(res.entries[0].message);
                }
            }
            // if (this.persoAndPrintSelected) {
            //     this.ejectCardFromPrinter();
            // }

            return throwError(res.step);
        } else if (res.step === 'done' && this.processName === 'gpunlock') {
            // If card is Locked, Unlocking the card first
            // This case will be executed after unlocking the card
            this.processName = 'issuance';
            this.startProcess(this.identifyProcessResponse, this.processName, cardReaderName);
        } else if (res.step === 'done' && res.stage === 'done') {
            if (this.isActivationRequired && this.isGpLockApplicable) {
                if (this.processName === 'issuance') {
                    this.processName = 'gplock';
                    this.startProcess(this.identifyProcessResponse, this.processName, cardReaderName);
                } else if (this.processName === 'gplock' && res.step === 'done') {
                    if (this.persoIdentityDeviceForm.get('selectedAction').value === 'persoAndprint') {
                        this.persoAndPrintBasedOnAction(res);
                    } else {
                        // this.subscription.unsubscribe();
                        // this.printerSubscription.unsubscribe();
                        this.translateService.get('issuance.persoSuccessMsg').subscribe(msg => {
                            this.width = 100;
                            this.message = msg;
                            this.success = true;
                            clearTimeout(this.timer);
                            return res.step;
                        });
                    }
                }
            } else {
                if (this.persoIdentityDeviceForm.get('selectedAction').value === 'persoAndprint') {
                    this.persoAndPrintBasedOnAction(res);
                } else {
                    // this.subscription.unsubscribe();
                    // this.printerSubscription.unsubscribe();
                    this.translateService.get('issuance.persoSuccessMsg').subscribe(msg => {
                        this.width = 100;
                        this.message = msg;
                        this.success = true;
                        clearTimeout(this.timer);
                        return res.step;
                    });
                }
            }
        }
    }
    errorCodeMsgDisplay(errorCode: string) {
        this.errorCode = errorCode;
        errorCode = errorCode.split('_', 2)[0] + '_' + errorCode.split('_', 2)[1];
        this.translateService.get('issuance.codes.' + errorCode).subscribe(msg => {
            this.stageSpecificError = msg;
        });
    }
    persoAndPrintBasedOnAction(res) {
        if (this.actionSelected === 'persoAndprint') {
            this.translateService.get('issuance.printStarted').subscribe(msg => {
                this.width = 100;
                this.message = msg;
                this.success = true;
                clearTimeout(this.timer);
                this.print();
            });
        } else {
            this.translateService.get('issuance.persoSuccessMsg').subscribe(msg => {
                this.width = 100;
                this.message = msg;
                this.success = true;
                clearTimeout(this.timer);
            });
            const selectedPrinter = this.selectedPrinter;
            // this.message = 'Ejecting the card from printer';
            this.deviceClientService.positionCard('RejectPosition', selectedPrinter.manufacturer, selectedPrinter.printerName).subscribe(
                ejectCardRes => {
                    console.log(ejectCardRes);
                    if (ejectCardRes.printerResponse !== null) {
                        if (ejectCardRes.printerResponse.accepted) {
                            console.log('successfuly ejected the card from printer');
                            this.isLoadingResults = false;
                            this.isCardinChipEncodePosition = false;
                        }
                    }
                },
                err => {
                    this.isLoadingResults = false;
                    this.isCardinChipEncodePosition = false;
                }
            );
        }
    }
    print() {
        this.isPrintingStarted = true;
        const visualTemplate: WFStepVisualTemplateConfig = this.persoIdentityDeviceForm.controls['selectedVisualTemplateConfig'].value;
        const printData = new PrintData();
        printData.OrientationFront = visualTemplate.frontOrientation;
        printData.OrientationBack = visualTemplate.backOrientation;
        printData.ImageType = 'png';
        const selectedPrinter: Printer = this.selectedPrinter;
        printData.manufacturer = selectedPrinter.manufacturer;
        printData.printerName = selectedPrinter.printerName;

        // this.translateService.get('issuance.printStarted').subscribe(msg => {
        //     this.message = msg;
        // }, err => {
        //     this.message = 'Printing started';
        // });
        // const user = new User();
        // user.id =  +this.userID;
        // this.searchUserService.getUserByName(this.user).subscribe( user => {
        if (this.persoAndPrintSelected) {
            this.issuanceService.getVisualDesign(this.userID, visualTemplate.id).subscribe(
                (response: IdentityCard) => {
                    this.startPrinting(response, printData, selectedPrinter, visualTemplate.id);
                },
                err => {
                    this.isPrintingStarted = false;
                    this.progressClass = 'progress-bar bg-danger';
                    this.failure = true;
                    this.success = false;
                    this.message = 'Error while getiing Visual design data';
                }
            );
        } else if (this.printSelected) {
            this.issuanceService.getVisualCredentialForPrinting(this.userID, visualTemplate.id).subscribe(
                (response: IdentityCard) => {
                    this.isLoadingResults = false;
                    this.display = false;
                    this.success = true;
                    this.startPrinting(response, printData, selectedPrinter, visualTemplate.id);
                },
                err => {
                    this.isLoadingResults = false;
                    this.isPrintingStarted = false;
                    this.progressClass = 'progress-bar bg-danger';
                    this.failure = true;
                    this.success = false;
                    this.message = 'Error while getiing Visual design data';
                }
            );
        }

        //   }, err => {
        //     this.isPrintingStarted = false;
        //     this.progressClass = 'progress-bar bg-danger';
        //     this.failure = true;
        //     this.success = false;
        //     this.message = 'Error while printing';
        //   });
    }
    startPrinting(identityCard: IdentityCard, printData: PrintData, selectedPrinter: Printer, visualTemplateId: number) {
        printData.FrontCardImg = identityCard.base64FrontCredential;
        printData.BackCardImg = identityCard.base64BackCredential;
        this.deviceClientService.print(printData).subscribe(
            printResponse => {
                if (printResponse.responseCode === 'DS0000') {
                    const printerStatus = printResponse.printerResponse.status;
                    console.log('printer status:', printerStatus);
                    if (printResponse.printerResponse.accepted === 'true') {
                        this.isPrintingStarted = false;
                        // error:  No card present in card
                        if (this.persoAndPrintSelected) {
                            this.translateService.get('issuance.persoAndPPrintSuccessMsg').subscribe(
                                msg => {
                                    this.width = 100;
                                    this.message = msg;
                                    this.success = true;
                                    clearTimeout(this.timer);
                                    return;
                                },
                                err => {
                                    this.width = 100;
                                    this.message = 'Issuance and printing completed successfully.';
                                    this.success = true;
                                    clearTimeout(this.timer);
                                    return;
                                }
                            );
                        } else if (this.printSelected) {
                            this.issuanceService.savePlasticCardCredential(identityCard, this.userID, visualTemplateId).subscribe(
                                res => {
                                    console.log(res);
                                    if (res.statusCode)
                                        this.translateService.get('issuance.printSuccessMsg').subscribe(
                                            msg => {
                                                this.width = 100;
                                                // this.message = msg;
                                                this.message = 'Printing completed successfully.';
                                                this.success = true;
                                                clearTimeout(this.timer);
                                                return;
                                            },
                                            err => {
                                                this.width = 100;
                                                this.message = 'Printing completed successfully.';
                                                this.success = true;
                                                clearTimeout(this.timer);
                                                return;
                                            }
                                        );
                                },
                                err => {
                                    // this.isLoadingResults = false;
                                }
                            );
                        }
                    } else if (printerStatus === 'Busy') {
                        // this.message = printResponse.printerResponse.Status;
                        this.isLoadingResults = false;
                    } else if (printerStatus === 'Error') {
                        this.isPrintingStarted = false;
                        this.progressClass = 'progress-bar bg-danger';
                        this.failure = true;
                        this.success = false;
                        this.message = 'Error while printing';
                        return;
                    } else {
                        this.callPrinterStatus(identityCard, selectedPrinter.manufacturer, selectedPrinter.printerName, visualTemplateId);
                    }
                    // check for printer status and show the message
                } else {
                    // get the respective message for responsecode and display
                }
            },
            err => {
                if (this.printSelected) {
                    this.issuanceService.deletePlasticCardCredential(this.userID, visualTemplateId).subscribe(res => {
                        console.log('del print credential', res);
                    });
                }
                this.isPrintingStarted = false;
                this.progressClass = 'progress-bar bg-danger';
                this.failure = true;
                this.success = false;
                this.message = 'Error while printing';
            }
        );
    }
    callPrinterStatus(identityCard: IdentityCard, manufacturer: string, printerName: string, visualTemplateId) {
        this.printerStatusCalls++;
        this.deviceClientService.getPrinterStatus(manufacturer, printerName).subscribe(
            res => {
                console.log(res);
                if (res.printerResponse.status === 'Busy' && this.printerStatusCalls === 3) {
                    setTimeout(() => {
                        const c = this.callPrinterStatus(identityCard, manufacturer, printerName, visualTemplateId);
                        console.log(c);
                    }, 5000);
                } else {
                    // if (res.printerResponse.status === 'Error')
                    this.isPrintingStarted = false;
                    // error:  No card present in card
                    if (this.persoAndPrintSelected) {
                        this.translateService.get('issuance.persoAndPPrintSuccessMsg').subscribe(
                            msg => {
                                this.width = 100;
                                this.message = msg;
                                this.success = true;
                                clearTimeout(this.timer);
                                return;
                            },
                            err => {
                                this.width = 100;
                                this.message = 'Issuance and printing completed successfully.';
                                this.success = true;
                                clearTimeout(this.timer);
                                return;
                            }
                        );
                    } else if (this.printSelected) {
                        this.issuanceService.savePlasticCardCredential(identityCard, this.userID, visualTemplateId).subscribe(
                            res => {
                                console.log(res);
                                this.translateService.get('issuance.persoAndPPrintSuccessMsg').subscribe(
                                    msg => {
                                        this.width = 100;
                                        // this.message = msg;
                                        this.message = 'Printing completed successfully.';
                                        this.success = true;
                                        clearTimeout(this.timer);
                                        return;
                                    },
                                    err => {
                                        this.width = 100;
                                        this.message = 'Printing completed successfully.';
                                        this.success = true;
                                        clearTimeout(this.timer);
                                        return;
                                    }
                                );
                            },
                            err => {
                                this.isLoadingResults = false;
                            }
                        );
                    }
                }
            },
            err => {
                this.isPrintingStarted = false;
                this.progressClass = 'progress-bar bg-danger';
                this.failure = true;
                this.success = false;
                this.message = 'Error while printing';
            }
        );
    }
    goSearchPage() {
        // this.ejectCardFromPrinter();
        // if printer is there, eject the card
        this.router.navigate(['/searchUser']);
    }

    buildCmsSyncRequest(identifyNextRes, processName) {
        const cmsSyncRequest = new CmsSyncRequest();
        const selectedActionValue = this.persoIdentityDeviceForm.controls['selectedAction'].value;
        if (selectedActionValue === 'perso') {
            cmsSyncRequest.chipEncoded = true;
        } else if (selectedActionValue === 'persoAndprint') {
            cmsSyncRequest.chipEncoded = true;
            if (this.actionSelected === 'persoAndprint') {
                cmsSyncRequest.printed = true;
            }
        } else if (selectedActionValue === 'print') {
            cmsSyncRequest.printed = true;
        }
        cmsSyncRequest.processName = processName;
        cmsSyncRequest.deviceType = 'permanent';
        cmsSyncRequest.device = {
            type: identifyNextRes.body.entries[0].result.type,
            serial: identifyNextRes.body.entries[0].result.serial,
            cardType: identifyNextRes.body.entries[0].result.cardType,
            cardProductName: identifyNextRes.body.entries[0].result.cardProductName
        };
        cmsSyncRequest.user = this.user;
        cmsSyncRequest.userGroupXrefId = 1;
        cmsSyncRequest.orgName = this.orgName;
        cmsSyncRequest.wfName = this.workflowName;
        cmsSyncRequest.pin = this.persoIdentityDeviceForm.controls.pin.value;
        console.log(this.disablePINS);
        cmsSyncRequest.isRandomPinRequired = this.disablePINS;
        if (!this.isGpLockApplicable) {
            this.isActivationRequired = false;
        }
        console.log(this.isActivationRequired);
        cmsSyncRequest.isActivationRequired = this.isActivationRequired;
        cmsSyncRequest.issuedBy = this.principal.getLoggedInUser().username;
        cmsSyncRequest.loggedInUserId = this.principal.getLoggedInUser().userId;
        return cmsSyncRequest;
    }

    printVisualID(visualConfig: WFStepVisualTemplateConfig) {
        this.isLoadingResults = true;
        if (this.credentialTemplates.length > 0) {
            if (this.persoIdentityDeviceForm.get('selectedAction').value === 'print') {
                if (this.addIdentityDeviceObj !== null && this.addIdentityDeviceObj.deviceProfileConfig.isPlasticId) {
                }
                if (this.selectedPrinter === null) {
                    this.translateService.get('issuance.printerNotFound').subscribe(msg => {
                        this.isLoadingResults = false;
                        this.error = true;
                        this.errorMsg = msg;
                        this.isLoadingResults = false;
                        window.scrollTo(0, 0);
                        return;
                    });
                }
                if (this.selectedPrinter) {
                    const selectedPrinter: Printer = this.selectedPrinter;
                    console.log('called printer111');
                    //  this.issuanceService.getPrinterStatus().subscribe(
                    //   statusResponse => {
                    if (selectedPrinter.status === 'Ready') {
                        // this is to show
                        this.isCardinChipEncodePosition = true;
                        // start printing
                        this.print();
                    } else if (selectedPrinter.status === 'Offline') {
                        this.translateService.get('issuance.printerNotFound').subscribe(msg => {
                            this.isLoadingResults = false;
                            this.error = true;
                            this.errorMsg = msg;
                            this.isLoadingResults = false;
                            window.scrollTo(0, 0);
                            return;
                        });
                    } else if (selectedPrinter.status === 'Error') {
                        this.translateService.get('issuance.noCardInPrinter').subscribe(msg => {
                            this.isLoadingResults = false;
                            this.error = true;
                            this.errorMsg = msg;
                            this.isLoadingResults = false;
                            window.scrollTo(0, 0);
                            return;
                        });
                    } else {
                        setTimeout(() => {
                            // this.display = false;
                            this.isCardinChipEncodePosition = true;
                            this.print();
                        }, 1000);
                    }
                }
            } else {
                this.isLoadingResults = false;
                console.log('selected smart card device for issuance and printing the card, need to handle it');
            }
            // this.router.navigate(['/issuance/print', { id: this.credentialTemplates[0].id, uname: this.user }]);
        } else {
            this.isLoadingResults = false;
        }
    }
    ejectCardFromPrinter() {
        const selectedPrinter = this.selectedPrinter;
        this.deviceClientService
            .positionCard('RejectPosition', selectedPrinter.manufacturer, selectedPrinter.printerName)
            .subscribe(res => {
                console.log(res);
                if (res.printerResponse !== null) {
                    if (res.printerResponse.accepted) {
                        console.log('successfuly ejected the card from printer');
                        this.isLoadingResults = false;
                        this.isCardinChipEncodePosition = false;
                        this.refresh();
                    }
                }
            });
    }
    ejectCardFromPrinter1() {
        this.isLoadingResults = true;
        this.ejectCardFromPrinter();
    }
    refresh(): void {
        if (this.printSelected) {
            console.log('print refresh:::::');
            if (this.printerSubscription.closed) {
                this.getPrintersList();
            }
            this.display = true;
            this.message = '';
            this.failure = false;
        } else {
            if (this.persoAndPrintSelected || this.printSelected) {
                if (this.printerSubscription.closed) {
                    this.getPrintersList();
                }
                //   this.ejectCardFromPrinter();
            }
            this.display = true;
            this.message = '';
            this.subscription.add(this.subscription);
            console.log(this.subscription);
            if (this.subscription.closed) {
                this.getReadersList();
            }
            //  this.getReadersList();
            this.progressClass = '';
            this.width = 0;
            this.failure = false;
        }
    }

    check(attr, toolTipId): void {
        const val = document.getElementById(toolTipId);
        this.showToolTip = val.offsetWidth < val.scrollWidth ? true : false;
    }

    ngOnDestroy() {
        // this.dataService.changeObj(null);
        this.subscription.unsubscribe();
        this.printerSubscription.unsubscribe();
        clearTimeout(this.timer);
    }
}
@Component({
    templateUrl: 'show-supoported-devices-dialog.html'
})
export class SupportedDevicesDialogComponent {
    constructor(public dialogRef: MatDialogRef<SupportedDevicesDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {}
    onNoClick(): void {
        this.dialogRef.close();
    }
}
export class DialogData {
    devices: string[];
    constructor() {}
}
export class Action {
    name: string;
    value: string;
    disabled: boolean;
    constructor(name, value, disabled) {
        this.name = name;
        this.value = value;
        this.disabled = disabled;
    }
}
