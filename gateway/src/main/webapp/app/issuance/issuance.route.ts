import { Route , Routes} from '@angular/router';

import { IssuanceComponent } from './issuance.component';
import { PrintVisualIDComponent } from './print-visual-id.component';

export const ISSUANCE_ROUTE: Routes = [
    {
    path: 'issuance',
    component: IssuanceComponent,
    data: {
        authorities: [],
        pageTitle: 'issuance.pageTitle',
        }
    },
    {
        path: 'issuance/print',
        component: PrintVisualIDComponent,
        data: {
            authorities: [],
            pageTitle: 'issuance.pageTitle',
        }
    }
];
