import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RequestCommand } from './model/requestCommand.model';
import { Commands } from './model/commands.model';
import { Principal } from '../core/auth/principal.service';
import { DeviceValidator } from './model/deviceValidator.model';
import { DeviceProfile } from 'app/device-profile/device-profile.model';
import { WorkflowStep } from 'app/workflow/workflow-step.model';
import { Workflow } from 'app/workflow/workflow.model';

@Injectable({
    providedIn: 'root'
})
export class IssuanceService {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name
        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });

    apduServerUrl: string;
    javaClientUrl: string;
    constructor(private http: HttpClient, private principal: Principal) {
        this.javaClientUrl = `${window.location.origin}/apdu/api/v1/card`;
        this.apduServerUrl = `${window.location.origin}/apdu/api/v1/sync`;
    }

    start(process, key: string): Observable<any> {
        return this.http.get(`${this.apduServerUrl}/start/${process}/${key}`, { observe: 'response' });
    }

    next(requestCommand: RequestCommand): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.http.post<Commands>(`${this.apduServerUrl}/next`, requestCommand, { headers: this.httpHeaders, observe: 'response' });
    }

    getUUID(processName, cmsSynRequest): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.http.post(`${this.apduServerUrl}/addRequest?processName=${processName}`, cmsSynRequest, { headers: this.httpHeaders });
    }

    private handleError(err: HttpErrorResponse) {
        console.log(err.message);
        return Observable.throw(err.message);
    }

    // TODO Find Other Way to Get User ID
    getUserByName(userName): Observable<any> {
        return this.http.get<any>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/users/${userName}`
        );
    }

    getConfiguredVisualCredentialTemplates(userID: string): Observable<WFStepVisualTemplateConfig[]> {
        return this.http.get<WFStepVisualTemplateConfig[]>(
            `${window.location.origin}/usermanagement/api/v1/web_portal_print/users/${userID}/credentials/configuredCredentialTemplates`
        );
    }

    getVisualCredentialForPrinting(userID: string, visualTemplateConfigID): Observable<IdentityCard> {
        const orgName = this.principal.getLoggedInUser().organization;
        const loggedInUser = this.principal.getLoggedInUser().username;
        const httpHeaders = new HttpHeaders({
            issuedBy: loggedInUser
        });
        return this.http.get<IdentityCard>(
            `${
                window.location.origin
            }/apdu/api/v1/organizations/${orgName}/web_portal_print/users/${userID}/credentials/${visualTemplateConfigID}`,
            { headers: httpHeaders }
        );
    }

    savePlasticCardCredential(identityCard: IdentityCard, userID: string, visualTemplateConfigID): Observable<any> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.http.post(
            `${
                window.location.origin
            }/apdu/api/v1/organizations/${orgName}/web_portal_print/users/${userID}/credentials/${visualTemplateConfigID}/save-plastic-card`, identityCard, { observe: 'response'}
        );
    }

    deletePlasticCardCredential(userID: string, visualTemplateConfigID): Observable<any> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.http.delete(
            `${
                window.location.origin
            }/apdu/api/v1/organizations/${orgName}/web_portal_print/users/${userID}/credentials/${visualTemplateConfigID}`, { observe: 'response'}
        );
    }
     
    // Here 'web' is "Application Name" to differentiate source of request Ex: If the request is from mobile we will defile mobile app name like : MobileID
    validateDeviceBeforeIssuance(deviceCUID: string, processName: string, userID): Observable<DeviceValidator> {
        const organization = this.principal.getLoggedInUser().organization;
        const httpHeaders = new HttpHeaders({
            userId: ''+userID
        });
        return this.http.get<DeviceValidator>(
            `${window.location.origin}/apdu/api/v1/devices/validate/web/${deviceCUID}/organizations/${organization}?processName=${processName}`,
            {headers: httpHeaders}
        );
    }
    getWorkflowByUsername(username: string): Observable<string> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.http.get(`${window.location.origin}/usermanagement/api/v1/organizations/${organization}/users/${username}/workflow`, {
            responseType: 'text'
        });
    }

    getWorkflowGeneralInfoByName(userName: string): Observable<Workflow> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.http.get<Workflow>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/users/${userName}/workflowGeneralDetails`
        );
    }

    getIssuedDevices(userId): Observable<any> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.http.get(
            `${window.location.origin}/apdu/api/v1/users/${userId}/devices/organizations/${organization}/issuedUserDevices`
        );
    }

    getIssuedDevicesByUserName(userName): Observable<any> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.http.get(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/users/${userName}/issuedUserDevices`
        );
    }

    getWorkflowStepsByWorkflow(workflowName: string): Observable<any> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.http.get(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/workflows/${workflowName}/steps`
        );
    }
    getWorkflowStepDetail(stepName: string, workflowName: string): Observable<WorkflowStep> {
        const organization = this.principal.getLoggedInUser().organization;
        return this.http.get<WorkflowStep>(
            `${
                window.location.origin
            }/usermanagement/api/v1/organizations/${organization}/workflows/${workflowName}/steps/${stepName}/details`
        );
    }
    getCardNameByAtr(atr: string): Observable<string> {
        return this.http.get(`${window.location.origin}/apdu/api/v1/devices/atrs/${atr}/cardName`, { responseType: 'text' });
    }
    getWorkflowDeviceProfiles(organizationName, wfName): Observable<DeviceProfile[]> {
        return this.http.get<DeviceProfile[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organizationName}/workflows/${wfName}/device-profiles-names`
        );
    }
    getWfStepDetails(wfName, stepName): Observable<any> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.http.get(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/workflows/${wfName}/steps/${stepName}`
        );
    }
    getVisualDesign(userID: string, visualTemplateConfigID): Observable<IdentityCard> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.http.get<IdentityCard>(
            `${
                window.location.origin
            }/apdu/api/v1/organizations/${orgName}/web_portal_print/users/${userID}/credentials/${visualTemplateConfigID}/print`);
    }
}

export class IdentityCard {
    base64FrontCredential: string;
    base64BackCredential: string;
    identitySerialNumber: string;
	userData: string;
	identityTypeName: string;
    constructor() {} 
}

export class WFStepVisualTemplateConfig {
    id: number;
    visualTemplateName: string;
    frontOrientation: string;
    backOrientation: string;
    constructor() {}
}
export class PrintData {
    OrientationFront: string;
    OrientationBack: string;
    FrontCardImg: string; // base64 of bmp image
    BackCardImg: string; // base64 of bmp image
    ImageType: string;
    manufacturer: string;
    printerName: string;
    constructor() {}
}

export class PrinterResponse {
    printerResponse: PersonalizePositionResponse;
    responseCode: string;
    constructor() {}
}
export class PersonalizePositionResponse {
    Status: string;
    accepted: string;
    error: string;
    error_alt: string;
    error_code: string;
    translated_error: string;
    status: string;
    responseCode: string;
    constructor() {}
}
