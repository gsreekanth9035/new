import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ISSUANCE_ROUTE } from 'app/issuance/issuance.route';
import { MaterialModule } from 'app/material/material.module';
import { IssuanceComponent, SupportedDevicesDialogComponent } from 'app/issuance/issuance.component';
import { PrintVisualIDComponent } from './print-visual-id.component';
import { GatewaySharedModule } from 'app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { IssuanceService } from 'app/issuance/issuance.service';

@NgModule({
    imports: [
        CommonModule,
        GatewaySharedModule,
        RouterModule.forChild(ISSUANCE_ROUTE),
        BrowserAnimationsModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    entryComponents: [SupportedDevicesDialogComponent],
    declarations: [IssuanceComponent, PrintVisualIDComponent, SupportedDevicesDialogComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [IssuanceService]
})
export class IssuanceModule {}
