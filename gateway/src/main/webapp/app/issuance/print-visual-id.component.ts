import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IssuanceService } from './issuance.service';
import * as print from 'print-js';

@Component({
    selector: 'jhi-issuance-print',
    templateUrl: './print-visual-id.component.html'
})
export class PrintVisualIDComponent implements OnInit {
    frontBase64Str: string;
    backBase64Str: string;
    readyForPrint = false;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private issuanceService: IssuanceService) {}

    ngOnInit() {
        const visualTemplateConfigID = this.activatedRoute.snapshot.paramMap.get('id');
        const userName = this.activatedRoute.snapshot.paramMap.get('uname');
        this.issuanceService.getUserByName(userName).subscribe(response => {
            const userID = response.id;
            this.issuanceService.getVisualCredentialForPrinting(userID, visualTemplateConfigID).subscribe(visualCard => {
                this.frontBase64Str = visualCard.base64FrontCredential;
                this.backBase64Str = visualCard.base64BackCredential;
                this.readyForPrint = true;
                setTimeout(() => {
                    print({
                        printable: ['data:image/jpeg;base64,' + this.frontBase64Str, 'data:image/jpeg;base64,' + this.backBase64Str],
                        type: 'image',
                        header: null,
                        base64: true,
                        imageStyle: 'width:100%; height:100%'
                    });
                    // window.print();
                    this.router.navigate(['issuance', { selectedAction: 'print' }]);
                });
            }, err => {
                console.log(err);
                this.readyForPrint = false;
            });
        });
    }
}
