import { RegOrgIdentityField } from '../workflow.service';

export class OrgRegIdentityField {
    fieldName: string;
    fieldLabel: string;
    required: boolean;
    isMandatory: boolean;
    regOrgIdentityFieldsList: RegOrgIdentityField[] = [];
}
