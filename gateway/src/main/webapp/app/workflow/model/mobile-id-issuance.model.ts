import { WFStepGenericConfig } from '../wf-step-generic-config.model';

export class WFMobileIDStepIdentityConfig {
    wfMobileIDStepIdentityConfigId: number;
    friendlyName: string;
    pushVerify: boolean;
    softOTP: boolean;
    fido2: boolean;
    isContentSigning: boolean;
    consent: boolean;
    wfMobileIDIdentitySoftOTPConfig: WFMobileIDIdentitySoftOTPConfig;
    wfMobileIdStepVisualIDGroupConfigs: WFMobileIdStepVisualIDGroupConfig[] = [];
    wfMobileIdStepCertificates: WFMobileIDStepCertificates[] = [];
    wfMobileIDStepCertConfig: WFMobileIDStepCertConfig;
    constructor() {}
}

export class WFMobileIDIdentitySoftOTPConfig {
    softOTPType: string;
    secretKey: string;
    algorithm: string;
    digits: number;
    interval: number;
    counter: number;
    constructor() {}
}

export class WFMobileIdStepVisualIDGroupConfig {
    referenceGroupId: string;
    referenceGroupName: string;
    visualTemplateId: number;
    constructor() {}
}
export class WFMobileIDStepCertificates {
    wfMobileIdStepCertId: number;
    certType: string;
    caServer: string;
    certTemplate: string;
    keyEscrow: boolean;
    disableRevoke: boolean;
    algorithm: string;
    keysize: number;
    subjectDN: string;
    subjectAN: string;

    constructor() {}
}
export class WFMobileIDStepCertConfig {
    noOfDaysBeforeCertExpiry: WFStepGenericConfig;
    emailNotificationFreq: WFStepGenericConfig;

    constructor() {}
}
