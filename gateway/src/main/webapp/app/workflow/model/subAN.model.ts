export class SubjectAN {
    OtherNameCheck = false;
    rfcNameCheck = false;
    dnsNameCheck = false;
    x400AddressCheck = false;
    directoryNameCheck = false;
    uriCheck = false;
    ediPartyCheck = false;
    ediPartyText = '';
    ipAddressCheck = false;
    regIdCheck = false;
    regIdText = '';
}
