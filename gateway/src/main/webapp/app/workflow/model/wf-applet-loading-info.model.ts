export class WfStepAppletLoadingInfo {
    pivAppletEnabled: boolean;
    fido2AppletEnabled: boolean;
    attestationCert: string;
    attestationCertPrivateKey: string;
    aaguid: string;
    constructor() {
        
    }
}