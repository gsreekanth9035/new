export class Group {
    id: string;
    name: string;
    visualTemplateID: number;
    isDisabled = false;

    constructor() {}
}
