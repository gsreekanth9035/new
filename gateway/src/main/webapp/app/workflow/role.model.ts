export class Role {
    name: string;
    description: string;
    authServiceRoleID: string;

    disabled = false;

    constructor() {}
}
