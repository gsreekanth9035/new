import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { WorkflowService } from '../workflow.service';
import { Workflow } from '../workflow.model';
import { merge, of as observableOf } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { Principal } from 'app/core';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from 'app/shared/util/shared-service';

@Component({
    selector: 'jhi-manage-workflow',
    templateUrl: 'manage-workflow.component.html',
    styleUrls: ['manage-workflow.scss']
})
export class ManageWorkflowComponent implements OnInit {
    displayedColumns: string[] = ['name', 'defaultWorkflow', 'deviceProfiles', 'groups', 'allowedDevices', 'actions'];
    data: Workflow[] = [];
    workflowName: string;
    showMessage = false;
    message = '';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    pageSize;
    resultsLength = 0;
    pageSizeOptions = [5, 10, 15, 20];
    parentPageSizeOptions = [5, 10, 15, 20];

    constructor(
        private router: Router,
        private workflowService: WorkflowService,
        private principal: Principal,
        private translateService: TranslateService,
        private sharedService: SharedService
    ) {}

    ngOnInit() {
        this.pageSize = this.pageSizeOptions[1];
        this.loadWorkflows();
    }

    addWorkflow() {
        this.router.navigate(['workflows/create-workflow']);
    }

    editWorkflow(workflowName: string) {
        this.router.navigate(['/createworkflow', { editMode: true, wfName: workflowName }]);
    }

    delWorkflow(workflowName: string) {
        this.workflowName = workflowName;
    }

    cloneWorkflow(workflowName) {
        this.router.navigate(['/createworkflow', { clone: true, wfName: workflowName }]);
    }

    deleteWorkflow() {
        this.workflowService.deleteWorkflow(this.workflowName).subscribe(response => {
            if (response) {
                this.showMessage = true;
                this.translateService.get('createWorkflow.messages.successfullydeleted', { wfName: this.workflowName }).subscribe(res => {
                    this.message = res;
                    window.scrollTo(0, 0);
                    this.loadWorkflows();
                });
            }
        });
    }
    loadWorkflows() {
        this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    if (this.paginator.pageSize === undefined) {
                        this.paginator.pageSize = this.pageSize;
                    }
                    return this.workflowService.getWorkflows(
                        this.principal.getLoggedInUser().organization,
                        this.sort.active,
                        this.sort.direction,
                        this.paginator.pageIndex,
                        this.paginator.pageSize
                    );
                }),
                map(data => {
                    this.resultsLength = data.totalElements;
                    if (data.totalElements > 0) {
                        this.pageSizeOptions = this.sharedService.getPageSizeOptionsBasedOnTotalElements(
                            data.totalElements,
                            this.parentPageSizeOptions
                        );
                    }
                    return data.content;
                }),
                catchError(() => {
                    return observableOf([]);
                })
            )
            .subscribe(data => (this.data = data));
    }
}
