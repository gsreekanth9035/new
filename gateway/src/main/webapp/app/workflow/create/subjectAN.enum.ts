export const enum SubjectANEnum {
    OTHER_NAME = 'otherName',
    RFC822NAME = 'rfc822Name',
    DNS_NAME = 'dNSName',
    X400ADDRESS = 'x400Address ',
    DIRECTORY_NAME = 'directoryName',
    EDI_PARTY_NAME = 'ediPartyName',
    URI = 'uniformResourceIdentifier',
    IP_ADDRESS = 'iPAddress',
    REG_ID = 'registeredID'
}
