import { FormGroup, FormBuilder, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatCheckboxChange, MatSelectChange, MatOptionSelectionChange } from '@angular/material';
import {
    WorkflowService,
    CAServerConfig,
    CAProvider,
    ConfigIdValuePair,
    RegOrgIdentityField,
    Finger,
    CertTemplate
} from '../workflow.service';
import { Principal, LoginService } from 'app/core';
import { Workflow } from '../workflow.model';
import { WorkflowStep } from '../workflow-step.model';
import { Group } from '../group.model';
import { WFStepRegistrationConfig } from '../wf-step-registration-config.model';
import { WFStepGenericConfig } from '../wf-step-generic-config.model';
import { WFStepCertConfig } from '../wf-step-cert-config.model';
import { WFStepApprovalGroupConfig } from '../wf-step-approval-group-config';
import { VisualTemplate } from '../visual-template.model';
import { SubjectANEnum } from './subjectAN.enum';
import { SubjectAN } from '../model/subAN.model';
import { OrganizationIdentity } from '../model/org-identity-model';
import {
    WFMobileIdStepVisualIDGroupConfig,
    WFMobileIDStepIdentityConfig,
    WFMobileIDIdentitySoftOTPConfig,
    WFMobileIDStepCertificates,
    WFMobileIDStepCertConfig
} from '../model/mobile-id-issuance.model';
import { OrgRegIdentityField } from '../model/org-field-type.model';
import { TranslateService } from '@ngx-translate/core';
import { DeviceTypeEnum } from 'app/manage-identity-devices/enum/device-types.enum';
import { WfStepGroupConfig } from '../model/wf-step-group-config.model';
import { OrganizationBrandingInfo } from 'app/layouts/login/login.component';
import { DeviceProfileConfig } from 'app/device-profile/device-profile.service';
import { Observable } from 'rxjs';
import { WfStepAppletLoadingInfo } from '../model/wf-applet-loading-info.model';
import { DeviceProfile } from 'app/device-profile/device-profile.model';
declare var $: any;

@Component({
    selector: 'jhi-create-workflow',
    templateUrl: 'create-workflow.component.html',
    styleUrls: ['create-workflow.scss']
})
export class CreateWorkflowComponent implements OnInit, AfterViewInit {
    isLoadingResults = true;
    isGroupDisabled = true;
    delAdditionalIdentity: any;
    errorMessage = false;
    message = '';
    successMessage = '';
    organization: string;
    workflowForm: FormGroup;
    requiredEnrollment = false;
    requiredTokenCardIssuance = false;
    requiredMobileIssuance = false;
    selectedSteps: string[] = [];
    editWorkflow = false;
    cloneWorkflow = false;
    editWorkflowObj = new Workflow();
    wfn: any;
    deleteIdentity: boolean = false;
    tokenIssueCertCheckboxEvent: any;

    saveSuccess = false;
    saveFailure = false;
    serverTimeOut = false;
    expirationErrorMsg: string;
    isAllowedDevicesSelected = false;
    hideAddEnrollButton = true;
    deletedFieldList: OrgRegIdentityField[] = [];
    deletedOrgFieldList: RegOrgIdentityField[] = [];
    regOrgIdentityFields: RegOrgIdentityField[] = [];
    identityModels: OrganizationIdentity[] = [];
    trustedExistingIdentityModels: OrganizationIdentity[] = [];
    activationTypes: ConfigIdValuePair[] = [];
    showFormFields = false;
    cardCertificateExpiringDays: ConfigIdValuePair[] = [];
    cardNotificationFrequencies: ConfigIdValuePair[] = [];

    mobileCertificateExpiringDays: ConfigIdValuePair[] = [];
    mobileNotificationFrequencies: ConfigIdValuePair[] = [];
    fingerNums = [{ value: '2', viewValue: '2' }, { value: '4', viewValue: '4' }, { value: '10', viewValue: '10' }];
    fingersByCount: Finger[] = [];

    // TODO: Decide algorithm / keysizes to retrieve from database or not
    algorithms = [{ id: 1, value: 'ECDSA', viewValue: 'ECDSA' }, { id: 2, value: 'RSA', viewValue: 'RSA' }];
    RSAKeySizes = [{ algId: 2, value: '2048', viewValue: '2048' }];
    ECDSAKeySizes = [
        { algId: 1, value: '224', viewValue: '224' },
        { algId: 1, value: '256', viewValue: '256' },
        { algId: 1, value: '384', viewValue: '384' },
        { algId: 1, value: '521', viewValue: '521' }
    ];
    ECCKeySizesOf9A = [{ algId: 1, value: '256', viewValue: '256' }];
    ECCKeySizesOf9E = [{ algId: 1, value: '256', viewValue: '256' }];
    ECCKeySizesOf9C = [{ algId: 1, value: '256', viewValue: '256' }, { algId: 1, value: '384', viewValue: '384' }];
    ECCKeySizesOf9D = [{ algId: 1, value: '256', viewValue: '256' }, { algId: 1, value: '384', viewValue: '384' }];
    certTypes = [];
    mobileCertTypes = [];
    certTypesForRef = [];
    displayedColumns: string[] = ['FieldName', 'FieldLabel', 'Required', 'Actions'];
    wfStepRegistrationConfigs: WFStepRegistrationConfig[];

    // Drop Down Values
    cropSizes: ConfigIdValuePair[];
    fingerprintThresholds: ConfigIdValuePair[];
    deviceProfiles: DeviceProfile[];
    workflowGroups: Group[];
    groupsList: Group[];
    visualTemplates: VisualTemplate[] = [];
    smartCardVisualTemplates: VisualTemplate[] = [];
    mobileVisualTemplates: VisualTemplate[] = [];

    caServers: CAServerConfig[] = [];
    // organizationName: any;
    organizationBrandingInfo: OrganizationBrandingInfo;

    showCardVisualIdSelection = false;
    months: Array<number> = [];
    days: Array<number> = [];
    isPKICertsEnabled = false;
    isTokenCertsEnabled = false;
    hideAddCardCert = false;
    isPrintEnabled = false;
    isTransparentPhotoEnabled$: Observable<boolean>;

    showAppletSelection = false;
    isFidoAppletSelected = false;
    wfNameValidationError = false;
    wfNameValidationNumberError = false;
    wfNameMaxlength = 30;
    wfNameMinlength = 2;
    errorExpiration = false;

    constructor(
        private loginService: LoginService,
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private workflowService: WorkflowService,
        private principal: Principal,
        private translateService: TranslateService
    ) {
        this.isTransparentPhotoEnabled$ = this.workflowService.checkTransparentPhotoEnabledInSystem();
    }

    ngOnInit() {
        for (let i = 1; i <= 36; i++) {
            this.months.push(i);
        }
        for (let i = 1; i <= 31; i++) {
            this.days.push(i);
        }
        this.certTypes = [
            { name: 'Card Authentication', value: '9E' },
            { name: 'PIV Authentication', value: '9A' },
            { name: 'Digital Signature', value: '9C' },
            { name: 'Encryption', value: '9D' }
        ];
        this.mobileCertTypes = [{ name: 'Authentication', value: 'Authentication' }];
        this.certTypesForRef = this.certTypes;
        this.workflowService.getOrgIssuingIdentityModels().subscribe(
            idModels => {
                this.identityModels = idModels;
            },
            err => {
                this.isLoadingResults = false;
            }
        );
        this.createWorkflowForm();

        this.organization = this.principal.getLoggedInUser().organization;

        // Load Device Profiles
        this.workflowService.getDeviceProfiles().subscribe(
            deviceProfiles => {
                this.deviceProfiles = deviceProfiles;
            },
            err => {
                this.isLoadingResults = false;
            }
        );

        // Load Groups
        this.workflowService.getGroups().subscribe(groups => {
            this.workflowGroups = groups;
        });
        this.workflowService.getAllGroups().subscribe(
            groups => {
                this.groupsList = groups;
            },
            err => {
                this.isLoadingResults = false;
            }
        );
        // Logic to handle Edit Workflow
        this.handleEditWorkflowMode();

        this.loginService.loadBrandingInformation().subscribe(
            brandingInformation => {
                this.organizationBrandingInfo = brandingInformation;
            },
            err => {
                // alert('error');
                this.isLoadingResults = false;
            }
        );
    } // ngOnInit ends
    ngAfterViewInit() {
        // To disable groups in OnInit() starts
        this.workflowForm.get('general.maxAllowedDevices.allowedDevices').disable();
        this.workflowForm.get('biometricEnrollment.idProofingChecked').disable();
        this.workflowForm.get('biometricEnrollment.enrollmentFormChecked').disable();
        this.workflowForm.get('biometricEnrollment.enrollmentForm').disable();
        this.workflowForm.get('biometricEnrollment.faceChecked').disable();
        this.workflowForm.get('biometricEnrollment.face').disable();
        this.workflowForm.get('biometricEnrollment.irisChecked').disable();
        this.workflowForm.get('biometricEnrollment.iris').disable();
        this.workflowForm.get('biometricEnrollment.fingerprintChecked').disable();
        this.workflowForm.get('biometricEnrollment.fingerprint').disable();
        this.workflowForm.get('biometricEnrollment.signatureChecked').disable();
        this.workflowForm.get('biometricEnrollment.approvalChecked').disable();
        this.workflowForm.get('biometricEnrollment.approval').disable();
        this.workflowForm.get('tokenIssuanceChecked').disable();
        this.workflowForm.get('certs').disable();
        this.workflowForm.get('appletSelectionFormGroup').disable();
        this.workflowForm.get('fidoe2AppletDetails').disable();
        (this.workflowForm.get('cardVisualDesigns') as FormArray).disable();
        // this.workflowForm.get('allowPrintGroups').disable(); // control
        this.workflowForm.get('issueCertsToCard').disable(); // group
        this.workflowForm.get('chipEncode').disable(); // group
        this.workflowForm.get('chipPlusVisualIDPrint').disable(); // group
        this.workflowForm.get('visualIDPrint').disable(); // group
        this.workflowForm.get('mobileId.isMobileIdReq').disable(); // group
        this.workflowForm.get('mobileId.onBoardingConfig').disable();
        this.workflowForm.get('mobileId.enforceHardwareBackeKeystrore').disable();
        this.workflowForm.get('mobileId.digitalIdentityIssuance').disable();
        this.workflowForm.get('activation').disable();
        this.workflowForm
            .get('certs')
            .get('contentSign')
            .disable();
        this.workflowForm.get('verificationPolices').disable();
        this.workflowForm.get('mobileId.digitalIdentityIssuance.enableMobileID').disable();
        this.workflowForm.get('mobileId.digitalIdentityIssuance.bindExpirationToMobileID').disable();
        this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray').disable();
    }
    manageWorkflow() {
        this.router.navigate(['workflows']);
    }

    /** ========================================================= Enrollment Form Logic ========================================================================== */
    addEnrollment() {
        if ((this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).length !== this.regOrgIdentityFields.length) {
            (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).push(
                this.formBuilder.group({
                    fieldName: ['', [Validators.required]],
                    fieldLabel: [''],
                    required: [false],
                    isMandatory: [''],
                    regOrgIdentityFieldsList: [this.deletedOrgFieldList]
                })
            );
        }
        if ((this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).length === this.regOrgIdentityFields.length) {
            this.hideAddEnrollButton = true;
        }
    }
    addFormGroup(formControlValue, mandatory) {
        const regOrgFielList = this.regOrgIdentityFields.filter(c => c.orgIdentityFieldId === formControlValue);
        (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).push(
            this.formBuilder.group({
                fieldName: [formControlValue, [Validators.required]],
                fieldLabel: [regOrgFielList[0].fieldDisplayName],
                required: [false],
                isMandatory: [mandatory],
                regOrgIdentityFieldsList: [regOrgFielList]
            })
        );
    }
    getRegOrgIdentityFields(index) {
        const field = (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).at(index).value;
        if (field.fieldName !== '') {
            return field.regOrgIdentityFieldsList;
        } else {
            return this.deletedOrgFieldList;
        }
    }
    enableFields(event) {
        this.isGroupDisabled = false;
        this.deletedOrgFieldList = [];
        this.deletedFieldList = [];
        // load visual templates
        this.workflowService.getVisualTemplates(event.value.identityTypeName).subscribe(visualTemplates => {
            this.visualTemplates = visualTemplates;
            this.smartCardVisualTemplates = [];
            this.mobileVisualTemplates = [];
            visualTemplates.forEach(vt => {
                vt.deviceTypes.forEach(device => {
                    if (device.name.includes(DeviceTypeEnum.SMART_CARD)) {
                        this.smartCardVisualTemplates.push(vt);
                    } else if (device.name.includes(DeviceTypeEnum.MOBILE)) {
                        this.mobileVisualTemplates.push(vt);
                    }
                });
            });
            let scVisualTemplateId;
            let mobileVisualTemplateId;
            if (this.smartCardVisualTemplates.length === 0) {
                scVisualTemplateId = '';
            } else {
                scVisualTemplateId = this.smartCardVisualTemplates[0].id;
            }
            if (this.mobileVisualTemplates.length === 0) {
                mobileVisualTemplateId = '';
            } else {
                mobileVisualTemplateId = this.mobileVisualTemplates[0].id;
            }
            // remove vds for card/token and meetidentity wallet
            (this.workflowForm.get('cardVisualDesigns') as FormArray).controls.forEach(e => {
                e.get('visualTemplateID').setValue(scVisualTemplateId);
            });
            (this.workflowForm
                .get('mobileId.digitalIdentityIssuance.additionalIdentity')
                .get('identityCredentialsArray') as FormArray).controls.forEach((control: FormGroup) => {
                (control.get('mobileVisualDesigns') as FormArray).controls.forEach(e => {
                    e.get('visualTemplateID').setValue(mobileVisualTemplateId);
                });
            });
        });
        // load crop sizes
        this.workflowService.getFaceCropSizes(event.value.identityTypeName).subscribe(configIdValues => {
            this.cropSizes = configIdValues;
        });
        const arr = <FormArray>this.workflowForm.get('biometricEnrollment.enrollmentForm');
        arr.controls = [];
        while (arr.length) {
            arr.removeAt(0);
        }
        this.workflowService.getRegOrgIdentityFields(event.value.identityTypeName).subscribe(idenityFields => {
            this.regOrgIdentityFields = idenityFields;
            if (this.regOrgIdentityFields.length > 0) {
                this.regOrgIdentityFields.forEach((registrationField: RegOrgIdentityField) => {
                    this.addFormGroup(registrationField.orgIdentityFieldId, registrationField.mandatory);
                });
                if (this.workflowForm.get('isbiometricsEnabled').value) {
                    if (this.workflowForm.get('biometricEnrollment.enrollmentFormChecked').value) {
                        (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).enable();
                    } else {
                        (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).disable();
                    }
                } else {
                    if (!this.workflowForm.get('biometricEnrollment.enrollmentFormChecked').value) {
                        (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).disable();
                    }
                }
            }
        });
    }
    getEnrollmentFormGroup(index): FormGroup {
        const formArray = this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray;
        return formArray.controls[index] as FormGroup;
    }
    isMandatory(index) {
        const formGroup: FormGroup = this.getEnrollmentFormGroup(index);
        if (formGroup.get('isMandatory') !== null) {
            return !formGroup.get('isMandatory').value;
        }
        return true;
    }
    isRequired(index) {
        const formGroup: FormGroup = this.getEnrollmentFormGroup(index);
        if (formGroup.get('required') !== null) {
            if (this.editWorkflow) {
                if (formGroup.get('isMandatory').value === true) {
                    return false;
                }
                return !formGroup.get('required').value;
            } else {
                return !formGroup.get('isMandatory').value;
            }
        }
        return true;
    }
    removeEnrollmentField(index: number) {
        this.hideAddEnrollButton = false;
        if (this.deletedFieldList.length > 0) {
            const deletedField = (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).at(index).value;
            if (this.deletedFieldList.find(e => e.fieldName === deletedField.fieldName) === undefined) {
                this.deletedFieldList.push(deletedField);
            }
        } else {
            this.deletedFieldList.push((this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).at(index).value);
        }
        const list: RegOrgIdentityField[] = [];
        this.regOrgIdentityFields.forEach(c => {
            this.deletedFieldList.forEach(v => {
                if (c.orgIdentityFieldId === v.fieldName) {
                    list.push(c);
                }
            });
        });
        this.deletedOrgFieldList = list;
        (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).removeAt(index);
    }
    onOrgIdentityFieldSelect(event, i) {
        const regOrgFielList = this.regOrgIdentityFields.filter(c => c.orgIdentityFieldId === event.value);
        const selectedIdentityField = (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).at(i);
        selectedIdentityField.get('regOrgIdentityFieldsList').setValue(regOrgFielList);
        selectedIdentityField.get('fieldLabel').setValue(regOrgFielList[0].fieldDisplayName);
        this.deletedOrgFieldList = this.deletedOrgFieldList.filter(e => e.orgIdentityFieldId !== selectedIdentityField.value.fieldName);
        this.deletedFieldList = this.deletedFieldList.filter(e => e.fieldName !== selectedIdentityField.value.fieldName);
    }
    /** ========================================================= Number Picker Logic ========================================================================== */
    decreaseNum(numPicker: HTMLInputElement, min, frmGroup) {
        let val = Number(numPicker.value);
        if (val > min) {
            val = Math.floor(val - 1);
        }
        numPicker.value = val.toString();
        const controlName = numPicker.getAttribute('formControlName');
        this.workflowForm.get(frmGroup + '.' + controlName).setValue(numPicker.value);
    }

    increaseNum(numPicker: HTMLInputElement, max, frmGroup) {
        let val = Number(numPicker.value);
        if (val < max) {
            val = Math.floor(val + 1);
        }
        numPicker.value = val.toString();

        const controlName = numPicker.getAttribute('formControlName');
        this.workflowForm.get(frmGroup + '.' + controlName).setValue(numPicker.value);
    }

    /** ========================================================= Drop Down Value Change Logic ========================================================================== */
    onCertTypeChange(event, c) {
        if (event.value !== undefined) {
            const certGroup = (this.workflowForm.get('cardCertificates') as FormArray).at(c);
            if (event.value.name === 'Encryption') {
                certGroup.get('hideKeyEscrow').setValue(false);
            } else {
                certGroup.get('hideKeyEscrow').setValue(true);
            }
            if (
                (this.workflowForm.get('cardCertificates') as FormArray).controls.length > c + 1 ||
                (this.workflowForm.get('cardCertificates') as FormArray).controls.length === 4
            ) {
                // it means its not the latest added row
                const certTypeName = event.value.name;
                if (certTypeName !== undefined && certGroup.get('canChangeCertType').value) {
                    certGroup.get('certTypeList').setValue(this.certTypes.filter(cert => cert.name === certTypeName));
                    certGroup.get('canChangeCertType').setValue(false);
                    this.certTypes = this.certTypes.filter(type => type.name !== certTypeName);
                }
            }
            let keySizeList = [];
            const algorithm = certGroup.get('algorithm').value;
            if (algorithm.length > 0) {
                if (algorithm.toLowerCase().includes('rsa')) {
                    keySizeList = this.RSAKeySizes;
                }
                if (algorithm.toLowerCase().includes('ecdsa')) {
                    if (event.value.value.includes('9A')) {
                        keySizeList = this.ECCKeySizesOf9A;
                    } else if (event.value.value.includes('9E')) {
                        keySizeList = this.ECCKeySizesOf9E;
                    } else if (event.value.value.includes('9C')) {
                        keySizeList = this.ECCKeySizesOf9C;
                    } else if (event.value.value.includes('9D')) {
                        keySizeList = this.ECCKeySizesOf9D;
                    }
                }
                certGroup.get('keySizeList').setValue(keySizeList);
                certGroup.get('keySize').setValue(keySizeList[0].value);
            }
        }
    }

    getCertTypes(index) {
        const field = (this.workflowForm.get('cardCertificates') as FormArray).at(index) as FormGroup;
        if (!field.get('canChangeCertType').value) {
            if ((this.workflowForm.get('cardCertificates') as FormArray).value.length === 1) {
                return this.certTypesForRef;
            }
            return field.get('certTypeList').value;
        } else {
            return this.certTypes;
        }
    }
    getMobileCertTypes(event: any, i, c) {
        return this.mobileCertTypes;
    }
    cardCAServerChange(event, c) {
        if (event.value !== undefined) {
            const certGroup = (this.workflowForm.get('cardCertificates') as FormArray).at(c);
            let isCaTemplatesPresent = false;
            if (event.value.certTemplates !== undefined) {
                if (event.value.certTemplates.length !== 0) {
                    certGroup.get('certProfileList').setValue(event.value.certTemplates);
                    isCaTemplatesPresent = true;
                }
            }
            if (!isCaTemplatesPresent) {
                this.workflowService.getCaTemplates(event.value.id, 'card').subscribe(caTemplates => {
                    certGroup.get('certProfileList').setValue(caTemplates);
                    event.value.certTemplates = caTemplates;
                });
            }
        }
    }
    cardCetProfiles(c) {
        let list = [];
        const certProfileListControl = (this.workflowForm.get('cardCertificates') as FormArray).controls[c].get('certProfileList');
        if (certProfileListControl !== null) {
            list = certProfileListControl.value;
        }
        return list;
    }
    onCertAlgorithmChange(event: any, i, c, type: string) {
        let keySizeList = [];
        if (event.value !== undefined) {
            if (event.value.toLowerCase().includes('rsa')) {
                keySizeList = this.RSAKeySizes;
            }
            if (type === 'card') {
                const certGroup = (this.workflowForm.get('cardCertificates') as FormArray).at(c);
                const certType = certGroup.get('certType').value.value;
                if (event.value.toLowerCase().includes('ecdsa')) {
                    if (certType.includes('9A')) {
                        keySizeList = this.ECCKeySizesOf9A;
                    } else if (certType.includes('9E')) {
                        keySizeList = this.ECCKeySizesOf9E;
                    } else if (certType.includes('9C')) {
                        keySizeList = this.ECCKeySizesOf9C;
                    } else if (certType.includes('9D')) {
                        keySizeList = this.ECCKeySizesOf9D;
                    }
                }
                certGroup.get('keySizeList').setValue(keySizeList);
                certGroup.get('keySize').setValue(keySizeList[0].value);
            } else if (type === 'mobile') {
                const identitiesArray = (this.workflowForm.get(
                    'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
                ) as FormArray).at(i);
                const certGroup = (identitiesArray.get('addCertificatesArray') as FormArray).at(c);
                if (event.value.toLowerCase().includes('ecdsa')) {
                    keySizeList = this.ECDSAKeySizes;
                }
                certGroup.get('keySizeList').setValue(keySizeList);
                certGroup.get('keySize').setValue(keySizeList[0].value);
            }
        }
    }
    certKeySize(i, c, type) {
        let list = [];
        if (type === 'card') {
            const certKeySizeListControl = (this.workflowForm.get('cardCertificates') as FormArray).controls[c].get('keySizeList');
            if (certKeySizeListControl !== null) {
                list = certKeySizeListControl.value;
            }
            return list;
        } else if (type === 'mobile') {
            const identitiesArray = (this.workflowForm.get(
                'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
            ) as FormArray).at(i);
            const certGroupKeySizeList = (identitiesArray.get('addCertificatesArray') as FormArray).at(c).get('keySizeList');
            if (certGroupKeySizeList !== null) {
                list = certGroupKeySizeList.value;
            }
            return list;
        }
    }
    showCardKeyEscrow(c) {
        return (this.workflowForm.get('cardCertificates') as FormArray).at(c).get('hideKeyEscrow').value;
    }
    DNcardApproveData(c) {
        const formGroup = (this.workflowForm.get('cardCertificates') as FormArray).at(c);
        formGroup.get('subjectDNDisplayValue').setValue(formGroup.get('subjectDN').value);
    }
    DNcardCancelData(c) {
        const formGroup = (this.workflowForm.get('cardCertificates') as FormArray).at(c);
        formGroup.get('subjectDN').setValue('');
        formGroup.get('subjectDNDisplayValue').setValue('');
    }
    hideAddCardCertButton(lengt: number) {
        const table = document.getElementById('tableBody') as HTMLTableElement;
        for (var k = 0, row; k < lengt - 1; k++) {
            row = table.rows[k];
            for (var l = 0, col; (col = row.cells[l]); l++) {
                if (col.id === 'add') {
                    col.children[0].hidden = true;
                }
            }
        }
    }
    addCardCert(c) {
        const cardCertRow = (this.workflowForm.get('cardCertificates') as FormArray).at(c);
        const certTypeName = cardCertRow.get('certType').value.name;
        if (certTypeName !== undefined && cardCertRow.get('canChangeCertType').value) {
            cardCertRow.get('certTypeList').setValue(this.certTypes.filter(cert => cert.name === certTypeName));
            cardCertRow.get('canChangeCertType').setValue(false);
            this.certTypes = this.certTypes.filter(type => type.name !== certTypeName);
        }
        (this.workflowForm.get('cardCertificates') as FormArray).push(
            this.formBuilder.group({
                id: [''],
                certType: ['', [Validators.required]],
                certTypeList: [''],
                caServer: ['', [Validators.required]],
                certProfile: ['', [Validators.required]],
                certProfileList: [''],
                canChangeCertType: [true],
                hideKeyEscrow: [true],
                escrow: [false],
                revocation: [false],
                algorithm: ['', [Validators.required]],
                keySize: ['', [Validators.required]],
                keySizeList: [''],
                subjectDN: ['', [Validators.required]],
                subjectDNDisplayValue: [''],
                subjectANDisplayValue: [''],
                subjectAN: this.formBuilder.group({
                    otherNameCheck: [false],
                    rfcNameCheck: [false],
                    dnsNameCheck: [false],
                    x400AddressCheck: [false],
                    directoryNameCheck: [false],
                    uriCheck: [false],
                    ediPartyCheck: [false],
                    ediPartyText: [''],
                    ipAddressCheck: [false],
                    regIdCheck: [false],
                    regIdText: ['']
                })
            })
        );
        if (this.cardCertificates.length === 4) {
            this.hideAddCardCert = true;
        }
        this.hideAddCardCertButton(this.cardCertificates.length);
    }
    removeHideCardButton(c: any) {
        const tableBody = document.getElementById('tableBody') as HTMLTableElement;
        for (var i = 0, row; (row = tableBody.rows[i]); i++) {
            for (var j = 0, col; (col = row.cells[j]); j++) {
                if (col.id === 'add') {
                    col.children[0].hidden = false;
                }
            }
        }
        var len = this.cardCertificates.length;
        if (c < len) {
            for (i = 0, row; i < len; i++) {
                row = tableBody.rows[i];
                for (j = 0, col; (col = row.cells[j]); j++) {
                    if (col.id === 'add') {
                        const n = i + 1;
                        if (tableBody.rows[n]) {
                            col.children[0].hidden = true;
                        } else {
                            col.children[0].hidden = false;
                        }
                    }
                }
            }
        } else {
            for (i = 0, row; i < len - 1; i++) {
                row = tableBody.rows[i];
                for (j = 0, col; (col = row.cells[j]); j++) {
                    if (col.id === 'add') {
                        const n = i + 1;
                        if (tableBody.rows[n]) {
                            col.children[0].hidden = true;
                        } else {
                            col.children[0].hidden = false;
                        }
                    }
                }
            }
        }
    }
    removeCardCert(c: number) {
        this.hideAddCardCert = false;
        const cardCertArray = this.workflowForm.get('cardCertificates') as FormArray;
        if (cardCertArray.length > 1) {
            const certType = (cardCertArray.at(c) as FormGroup).get('certType').value;
            if (certType !== '') {
                if (!(cardCertArray.at(c) as FormGroup).get('canChangeCertType').value) {
                    this.certTypes.push(certType);
                }
            }
            cardCertArray.removeAt(c);
            this.removeHideCardButton(c);
        }
    }
    mobileIDCertDNApproveData(i, c) {
        const formGroup = ((this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).controls[i].get('addCertificatesArray') as FormArray).controls[c];
        formGroup.get('subDnDisplayValue').setValue(formGroup.get('subDn').value);
    }
    mobileIDCertDNCancel(i, c) {
        const formGroup = ((this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).controls[i].get('addCertificatesArray') as FormArray).controls[c];
        formGroup.get('subDn').setValue('');
        formGroup.get('subDnDisplayValue').setValue('');
    }
    changeExpiration(event: any) {
        if (event.value !== 0) {
            this.errorExpiration = false;
        }
    }

    /** ========================================================= Save Workfow Logic ========================================================================== */
    saveWorkflow() {
        this.isLoadingResults = true;
        this.serverTimeOut = false;
        if (
            parseInt(this.workflowForm.get('general').get('months').value, 10) === 0 &&
            parseInt(this.workflowForm.get('general').get('days').value, 10) === 0
        ) {
            this.saveFailure = true;
            this.saveSuccess = false;
            window.scrollTo(0, 0);
            this.translateService.get('createWorkflow.errors.selectWfExpiration').subscribe(
                res => {
                    this.errorExpiration = true;
                    this.expirationErrorMsg = res;
                },
                err => {
                    this.expirationErrorMsg = "Please select Expiration in 'General' group to proceed";
                    this.errorExpiration = true;
                }
            );
            this.isLoadingResults = false;
            return;
        }
        this.saveFailure = false;
        this.errorMessage = false;
        // Validation
        if (!this.workflowForm.valid) {
            window.scrollTo(0, 0);
            this.saveFailure = true;
            this.saveSuccess = false;
            this.isLoadingResults = false;
            return;
        }
        let isBiometricEnabled = false;
        if (this.requiredEnrollment) {
            isBiometricEnabled = this.selectedSteps.some(c => {
                let ret = false;
                if (
                    c === 'ID_PROOFING' ||
                    c === 'REGISTRATION' ||
                    c === 'FACE_CAPTURE' ||
                    c === 'IRIS_CAPTURE' ||
                    c === 'FINGERPRINT_CAPTURE' ||
                    c === 'SIGNATURE_CAPTURE' ||
                    c === 'APPROVAL_BEFORE_ISSUANCE'
                ) {
                    ret = true;
                }
                return ret;
            });
        } else {
            if (!isBiometricEnabled) {
                this.saveFailure = true;
                this.saveSuccess = false;
                this.translateService.get('createWorkflow.errors.biometricStepError').subscribe(
                    res => {
                        this.errorMessage = true;
                        this.message = res;
                    },
                    err => {
                        this.message = 'Please select atleast one step under "Data and biometrics enrollment" ';
                        this.errorMessage = true;
                    }
                );
                window.scrollTo(0, 0);
                this.isLoadingResults = false;
                return;
            }
        }
        if (!this.requiredTokenCardIssuance && !this.requiredMobileIssuance) {
            this.saveFailure = true;
            this.saveSuccess = false;
            this.translateService.get('createWorkflow.errors.issuanceSelectionError').subscribe(
                res => {
                    this.errorMessage = true;
                    this.message = res;
                },
                err => {
                    this.message = "Select at least one issuance step, either 'Card / Token Issuance' or 'Mobile Wallet'.";
                    this.errorMessage = true;
                }
            );
            window.scrollTo(0, 0);
            this.isLoadingResults = false;
            return;
        }
        if (this.requiredTokenCardIssuance) {
            if (
                this.workflowForm.get('chipEncode').get('isChipEncode').value ||
                this.workflowForm.get('chipPlusVisualIDPrint').get('isChipPlusVisualIDPrint').value ||
                this.workflowForm.get('visualIDPrint').get('isVisualIDPrint').value
            ) {
            } else {
                this.saveSuccess = false;
                this.saveFailure = true;
                this.errorMessage = true;
                this.translateService.get('createWorkflow.errors.tokenOrSmartCardError').subscribe(
                    res => {
                        this.errorMessage = true;
                        this.message = res;
                    },
                    err => {
                        this.message = 'Please select at least one issuance option under "Card/Token issuance"';
                        this.errorMessage = true;
                    }
                );
                window.scrollTo(0, 0);
                this.isLoadingResults = false;
                return;
            }
            if (this.showAppletSelection) {
                // validate atleast one checkbox should be selected
                if (
                    this.workflowForm.get('appletSelectionFormGroup.enablePivApplet').value ||
                    this.workflowForm.get('appletSelectionFormGroup.enableFido2Applet').value
                ) {
                    // it means anyone of two checkboxes is selected
                } else {
                    //
                    this.saveSuccess = false;
                    this.saveFailure = true;
                    this.errorMessage = true;
                    this.translateService.get('createWorkflow.errors.appletSelectionError').subscribe(
                        res => {
                            this.errorMessage = true;
                            this.message = res;
                        },
                        err => {
                            this.message = 'Please select at least one applet type loading under "Card/Token issuance" ';
                            this.errorMessage = true;
                        }
                    );
                    window.scrollTo(0, 0);
                    this.isLoadingResults = false;
                    return;
                }
            }
        }
        if (this.requiredMobileIssuance) {
            const isMobileStepSelected = this.selectedSteps.some(c => {
                let ret = false;
                if (c === 'MOBILE_ID_ONBOARDING_CONFIG' || c === 'MOBILE_ID_IDENTITY_ISSUANCE') {
                    ret = true;
                }
                return ret;
            });
            if (!isMobileStepSelected) {
                this.saveSuccess = false;
                this.saveFailure = true;
                this.errorMessage = true;
                this.translateService.get('createWorkflow.errors.mobileIDValidationError').subscribe(
                    res => {
                        this.message = res;
                    },
                    err => {
                        this.message = 'Please select atleast one step under Mobile Wallet';
                    }
                );
                window.scrollTo(0, 0);
                this.isLoadingResults = false;
                return;
            } else if (this.selectedSteps.some(c => c === 'MOBILE_ID_ONBOARDING_CONFIG')) {
                let onboardAndEnrollUsingExistingIds = false;
                onboardAndEnrollUsingExistingIds = this.workflowForm
                    .get('mobileId.onBoardingConfig')
                    .get('selfServiceOptions.onboardEnrollUsers').value;
                if (onboardAndEnrollUsingExistingIds) {
                    if (
                        this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions').get('scanQrCode')
                            .value ||
                        // this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions').get('nfc')
                        //     .value ||
                        this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions').get('frontDoc')
                            .value ||
                        this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions').get('backDoc').value
                    ) {
                    } else {
                        window.scrollTo(0, 0);
                        this.errorMessage = true;
                        this.saveFailure = true;
                        this.translateService.get('createWorkflow.errors.mobileIDStepValidationError').subscribe(
                            res => {
                                this.message = res;
                            },
                            err => {
                                this.message =
                                    'Please check atleast one check box under "onboard and enroll users using existing identities" ';
                            }
                        );
                        this.isLoadingResults = false;
                        return;
                    }
                }
                if (this.workflowForm.get('mobileId.onBoardingConfig').get('selfServiceOptions.onboardEnrollUsers').value) {
                }
            }
        }
        // Create Workflow
        const workflow: Workflow = new Workflow();
        workflow.name = this.workflowForm.get('general').get('wfName').value;
        workflow.name = workflow.name.trim();
        workflow.description = this.workflowForm.get('general').get('description').value;
        workflow.defaultWorkflow = this.workflowForm.get('general').get('defaultWorkflow').value;
        // Device Profiles
        const deviceProfiles: DeviceProfile[] = [];
        this.workflowForm
            .get('general')
            .get('selectedDeviceProfiles')
            .value.forEach((selectedDeviceProfile: DeviceProfile) => {
                const deviceProfile: DeviceProfile = new DeviceProfile();
                deviceProfile.name = selectedDeviceProfile.name;

                deviceProfiles.push(deviceProfile);
            });
        workflow.organizationIdentities = this.workflowForm.get('general').get('identityModel').value;
        workflow.deviceProfiles = deviceProfiles;
        // Groups
        const groups: Group[] = [];

        this.workflowForm
            .get('general')
            .get('selectedGroups')
            .value.forEach(selectedGroupId => {
                const group: Group = new Group();
                group.id = selectedGroupId.id;
                group.name = selectedGroupId.name;
                groups.push(group);
            });

        workflow.groups = groups;
        // Allowed Devices
        if (this.workflowForm.get('general').get('maxAllowedDevices.isAllowedDevices').value) {
            workflow.allowedDevices = this.workflowForm.get('general').get('maxAllowedDevices.allowedDevices').value;
        }
        // Expiration
        workflow.expirationInMonths = this.workflowForm.get('general').get('months').value;
        workflow.expirationInDays = this.workflowForm.get('general').get('days').value;

        // disable expiration enforcement
        workflow.disableExpirationEnforcement = this.workflowForm.get('general').get('disableExpiration').value;
        workflow.revokeEncryptionCertificate = this.workflowForm.get('general').get('revokeEncryptionCert').value;
        workflow.requiredEnrollment = this.requiredEnrollment;
        workflow.requiredTokenCardIssuance = this.requiredTokenCardIssuance;
        workflow.requiredMobileIssuance = this.requiredMobileIssuance;

        // Worklfow Steps

        const workflowSteps: WorkflowStep[] = [];
        if (this.selectedSteps.length > 0) {
            if (this.requiredEnrollment) {
                if (this.selectedSteps.some(c => c === 'ID_PROOFING')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'ID_PROOFING';
                    workflowSteps.push(workflowStep);
                }
                if (this.selectedSteps.some(c => c === 'REGISTRATION')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'REGISTRATION';
                    const wfStepRegistrationConfigs: WFStepRegistrationConfig[] = [];
                    (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).controls.forEach(enrollmentForm => {
                        const wfStepRegistrationConfig: WFStepRegistrationConfig = new WFStepRegistrationConfig();
                        wfStepRegistrationConfig.orgFieldId = enrollmentForm.get('fieldName').value;
                        wfStepRegistrationConfig.fieldLabel = enrollmentForm.get('fieldLabel').value;
                        wfStepRegistrationConfig.required = enrollmentForm.get('required').value;

                        wfStepRegistrationConfigs.push(wfStepRegistrationConfig);
                    });
                    workflowStep.wfStepRegistrationConfigs = wfStepRegistrationConfigs;
                    workflowSteps.push(workflowStep);

                    const wfStepGenericConfigs: WFStepGenericConfig[] = [];
                    const wfStepGenericConfigBiometricConfig: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfigBiometricConfig.name = 'BIOMETRIC_VALIDATION_REQUIRED';
                    wfStepGenericConfigBiometricConfig.value = 'N';
                    wfStepGenericConfigs.push(wfStepGenericConfigBiometricConfig);
                    workflowStep.wfStepGenericConfigs = wfStepGenericConfigs;

                    workflowStep.wfStepRegistrationConfigs = wfStepRegistrationConfigs;
                }
                if (this.selectedSteps.some(c => c === 'FACE_CAPTURE')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'FACE_CAPTURE';
                    const wfStepGenericConfigs: WFStepGenericConfig[] = [];

                    const wfStepGenericConfigICAO: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfigICAO.name = 'ICAO_COMPLIANCE';
                    if (this.workflowForm.get('biometricEnrollment.face').get('icaoCompliance').value) {
                        wfStepGenericConfigICAO.value = 'Y';
                    } else {
                        wfStepGenericConfigICAO.value = 'N';
                    }

                    const wfStepGenericConfigCropSize: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfigCropSize.name = 'CROP_SIZE';
                    wfStepGenericConfigCropSize.value = this.workflowForm.get('biometricEnrollment.face').get('cropSize').value;

                    const wfStepGenericConfigAllowedTransparentImg: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfigAllowedTransparentImg.name = 'TRANSPARENT_PHOTO_ALLOWED';
                    if (this.workflowForm.get('biometricEnrollment.face').get('transparentPhotoAllowed').value) {
                        wfStepGenericConfigAllowedTransparentImg.value = 'Y';
                    } else {
                        wfStepGenericConfigAllowedTransparentImg.value = 'N';
                    }

                    wfStepGenericConfigs.push(wfStepGenericConfigICAO);
                    wfStepGenericConfigs.push(wfStepGenericConfigCropSize);
                    wfStepGenericConfigs.push(wfStepGenericConfigAllowedTransparentImg);

                    workflowStep.wfStepGenericConfigs = wfStepGenericConfigs;
                    workflowSteps.push(workflowStep);
                }
                if (this.selectedSteps.some(c => c === 'IRIS_CAPTURE')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'IRIS_CAPTURE';
                    const wfStepGenericConfigs: WFStepGenericConfig[] = [];

                    const wfStepGenericConfigIrisMode: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfigIrisMode.name = 'IRIS_MODE';
                    wfStepGenericConfigIrisMode.value = this.workflowForm.get('biometricEnrollment.iris').get('irisMode').value;

                    wfStepGenericConfigs.push(wfStepGenericConfigIrisMode);

                    workflowStep.wfStepGenericConfigs = wfStepGenericConfigs;
                    workflowSteps.push(workflowStep);
                }
                if (this.selectedSteps.some(c => c === 'FINGERPRINT_CAPTURE')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'FINGERPRINT_CAPTURE';
                    const wfStepGenericConfigs: WFStepGenericConfig[] = [];

                    const wfStepGenericConfig: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfig.name = 'MINIMUM_FINGERS_REQUIRED';
                    wfStepGenericConfig.value = this.workflowForm
                        .get('biometricEnrollment.fingerprint')
                        .get('numberOfRequiredFingers').value;
                    this.fingersByCount.forEach(f => {
                        const wfStepGenericConfigPosition: WFStepGenericConfig = new WFStepGenericConfig();
                        wfStepGenericConfigPosition.name = 'FINGER_POSITION';
                        wfStepGenericConfigPosition.value = f.fingerConfigValueName;
                        wfStepGenericConfigs.push(wfStepGenericConfigPosition);
                    });
                    const wfStepGenericConfigFPThreshold: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfigFPThreshold.name = 'FINGERPRINT_MIN_THRESHOLD';
                    wfStepGenericConfigFPThreshold.value = this.workflowForm
                        .get('biometricEnrollment.fingerprint')
                        .get('minimumFingersThreshhold').value;

                    const wfStepGenericConfigEnableWSQ: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfigEnableWSQ.name = 'FINGERPRINT_WSQ';
                    if (this.workflowForm.get('biometricEnrollment.fingerprint').get('enableWSQ').value) {
                        wfStepGenericConfigEnableWSQ.value = 'Y';
                    } else {
                        wfStepGenericConfigEnableWSQ.value = 'N';
                    }

                    wfStepGenericConfigs.push(wfStepGenericConfig);
                    wfStepGenericConfigs.push(wfStepGenericConfigFPThreshold);
                    wfStepGenericConfigs.push(wfStepGenericConfigEnableWSQ);

                    workflowStep.wfStepGenericConfigs = wfStepGenericConfigs;
                    workflowSteps.push(workflowStep);
                }
                if (this.selectedSteps.some(c => c === 'FINGERPRINT_ROLL_CAPTURE')) {
                    if (this.selectedSteps.some(c => c === 'FINGERPRINT_CAPTURE')) {
                        const workflowStep: WorkflowStep = new WorkflowStep();
                        workflowStep.name = 'FINGERPRINT_ROLL_CAPTURE';
                        workflowSteps.push(workflowStep);
                    }
                }
                if (this.selectedSteps.some(c => c === 'SIGNATURE_CAPTURE')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'SIGNATURE_CAPTURE';
                    workflowSteps.push(workflowStep);
                }
                if (this.selectedSteps.some(c => c === 'APPROVAL_BEFORE_ISSUANCE')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'APPROVAL_BEFORE_ISSUANCE';
                    const wfStepApprovalGroupConfigs: WFStepApprovalGroupConfig[] = [];
                    this.workflowForm
                        .get('biometricEnrollment.approval')
                        .get('approvalGroup')
                        .value.forEach(approvalGroup => {
                            const wfStepApprovalGroupConfig: WFStepApprovalGroupConfig = new WFStepApprovalGroupConfig();
                            wfStepApprovalGroupConfig.groupId = approvalGroup.id;
                            wfStepApprovalGroupConfig.groupName = approvalGroup.name;
                            wfStepApprovalGroupConfigs.push(wfStepApprovalGroupConfig);
                        });

                    workflowStep.wfStepApprovalGroupConfigs = wfStepApprovalGroupConfigs;
                    workflowSteps.push(workflowStep);
                }
            }
            if (this.requiredTokenCardIssuance) {
                if (this.selectedSteps.some(c => c === 'PERSO_AND_ISSUANCE')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'PERSO_AND_ISSUANCE';

                    const wfStepGenericConfigs: WFStepGenericConfig[] = [];
                    const wfStepGenericConfigContentSignCert: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfigContentSignCert.name = 'CONTENT_SIGNING_CERTIFICATE_REQUIRED';
                    if (this.workflowForm.get('certs').get('contentSign').value) {
                        wfStepGenericConfigContentSignCert.value = 'Y';
                    } else {
                        wfStepGenericConfigContentSignCert.value = 'N';
                    }
                    wfStepGenericConfigs.push(wfStepGenericConfigContentSignCert);
                    const wfAppletLoadingInfo = new WfStepAppletLoadingInfo();
                    if (this.showAppletSelection) {
                        // validate atleast one checkbox should be selected
                        if (this.workflowForm.get('appletSelectionFormGroup.enablePivApplet').value) {
                            wfAppletLoadingInfo.pivAppletEnabled = true;
                        }
                        if (this.workflowForm.get('appletSelectionFormGroup.enableFido2Applet').value) {
                            wfAppletLoadingInfo.fido2AppletEnabled = true;

                            wfAppletLoadingInfo.attestationCert = this.workflowForm.get('fidoe2AppletDetails.attestationCert').value;
                            wfAppletLoadingInfo.attestationCertPrivateKey = this.workflowForm.get(
                                'fidoe2AppletDetails.attestationCertPvtKey'
                            ).value;
                            wfAppletLoadingInfo.aaguid = this.workflowForm.get('fidoe2AppletDetails.aaguid').value;
                        }
                        workflowStep.wfStepAppletLoadingInfo = wfAppletLoadingInfo;
                    }

                    const wfStpChipEncode: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStpChipEncode.name = 'ENABLE_CHIP_PERSO';
                    if (this.workflowForm.get('chipEncode').get('isChipEncode').value) {
                        wfStpChipEncode.value = 'Y';
                        // set groups that can do chip perso
                        const WfStepChipeEncodeGroupConfigs: WfStepGroupConfig[] = [];
                        this.workflowForm.get('chipEncode.chipEncodeGroup').value.forEach(printGroup => {
                            const wfStepChipEncodeGroupConfig: WfStepGroupConfig = new WfStepGroupConfig();
                            wfStepChipEncodeGroupConfig.groupId = printGroup.id;
                            wfStepChipEncodeGroupConfig.groupName = printGroup.name;
                            WfStepChipeEncodeGroupConfigs.push(wfStepChipEncodeGroupConfig);
                        });
                        workflowStep.wfStepChipEncodeGroupConfigs = WfStepChipeEncodeGroupConfigs;
                    } else {
                        wfStpChipEncode.value = 'N';
                    }
                    const wfStpChipEncodePlusVIDPrint: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStpChipEncodePlusVIDPrint.name = 'ENABLE_CHIP_PERSO_PLUS_VISUAL_ID_PRINT';
                    if (this.workflowForm.get('chipPlusVisualIDPrint').get('isChipPlusVisualIDPrint').value) {
                        wfStpChipEncodePlusVIDPrint.value = 'Y';
                        // set groups that can do chip perso plus visual ID print
                        const WfStepChipeEncodeVIDPrintGroupConfigs: WfStepGroupConfig[] = [];
                        this.workflowForm.get('chipPlusVisualIDPrint.chipPlusVisualIDGroup').value.forEach(printGroup => {
                            const wfStepChipEncodeVIDGroupConfig: WfStepGroupConfig = new WfStepGroupConfig();
                            wfStepChipEncodeVIDGroupConfig.groupId = printGroup.id;
                            wfStepChipEncodeVIDGroupConfig.groupName = printGroup.name;
                            WfStepChipeEncodeVIDPrintGroupConfigs.push(wfStepChipEncodeVIDGroupConfig);
                        });
                        workflowStep.wfStepChipEncodeVIDPrintGroupConfigs = WfStepChipeEncodeVIDPrintGroupConfigs;
                    } else {
                        wfStpChipEncodePlusVIDPrint.value = 'N';
                    }
                    const wfStpVIDPrint: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStpVIDPrint.name = 'ENABLE_VISUAL_ID_PRINT';
                    if (this.workflowForm.get('visualIDPrint').get('isVisualIDPrint').value) {
                        wfStpVIDPrint.value = 'Y';
                        // set groups that can do visual ID print
                        const WfStepPrintGroupConfigs: WfStepGroupConfig[] = [];
                        this.workflowForm.get('visualIDPrint.visualIDPrintGroup').value.forEach(printGroup => {
                            const wfStepPrintGroupConfig: WfStepGroupConfig = new WfStepGroupConfig();
                            wfStepPrintGroupConfig.groupId = printGroup.id;
                            wfStepPrintGroupConfig.groupName = printGroup.name;
                            WfStepPrintGroupConfigs.push(wfStepPrintGroupConfig);
                        });
                        workflowStep.wfStepPrintGroupConfigs = WfStepPrintGroupConfigs;
                    } else {
                        wfStpVIDPrint.value = 'N';
                    }
                    if (
                        this.workflowForm.get('chipPlusVisualIDPrint').get('isChipPlusVisualIDPrint').value ||
                        this.workflowForm.get('visualIDPrint').get('isVisualIDPrint').value
                    ) {
                        // add groups and visual designs and also activation options
                        // setting card visual designs to group
                        const groupsVisualDesigns: Group[] = [];
                        (this.workflowForm.get('cardVisualDesigns') as FormArray).controls.forEach((element: FormGroup) => {
                            if (element.get('cardVisualTemplateGroup').value.length > 1) {
                                element.get('cardVisualTemplateGroup').value.forEach((groupId: string) => {
                                    const group = new Group();
                                    group.id = groupId;
                                    group.visualTemplateID = element.get('visualTemplateID').value;
                                    groupsVisualDesigns.push(group);
                                });
                            } else {
                                const group = new Group();
                                group.id = element.get('cardVisualTemplateGroup').value[0];
                                group.visualTemplateID = element.get('visualTemplateID').value;
                                groupsVisualDesigns.push(group);
                            }
                        });
                        workflow.groups.forEach(group => {
                            groupsVisualDesigns.forEach(vd => {
                                if (group.id === vd.id) {
                                    group.visualTemplateID = vd.visualTemplateID;
                                }
                            });
                        });
                    }
                    wfStepGenericConfigs.push(wfStpChipEncode);
                    wfStepGenericConfigs.push(wfStpChipEncodePlusVIDPrint);
                    wfStepGenericConfigs.push(wfStpVIDPrint);

                    workflowStep.wfStepGenericConfigs = wfStepGenericConfigs;
                    workflowSteps.push(workflowStep);
                }
                if (this.selectedSteps.some(c => c === 'PKI_CERTIFICATES')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'PKI_CERTIFICATES';
                    const wfStepCertConfigs: WFStepCertConfig[] = [];

                    (this.workflowForm.get('cardCertificates') as FormArray).controls.forEach((element: FormGroup) => {
                        const certTypeName = element.get('certType').value.name;
                        element.get('certTypeList').setValue(this.certTypesForRef.filter(cert => cert.name === certTypeName));

                        const wfStepCertConfig: WFStepCertConfig = new WFStepCertConfig();
                        if (element.get('caServer').value !== undefined) {
                            const certType = this.certTypes.find(e => e.name === element.get('certType').value.name);
                            if (element.get('id').value === '') {
                                wfStepCertConfig.id = 0;
                            } else {
                                wfStepCertConfig.id = element.get('id').value;
                            }
                            wfStepCertConfig.certType = element.get('certTypeList').value[0].value; // certType.value;
                            wfStepCertConfig.caServer = element.get('caServer').value.id;
                            wfStepCertConfig.certTemplate = element.get('certProfile').value;
                            wfStepCertConfig.algorithm = element.get('algorithm').value;
                            wfStepCertConfig.keyEscrow = element.get('escrow').value;
                            wfStepCertConfig.disableRevoke = element.get('revocation').value;
                            wfStepCertConfig.keysize = element.get('keySize').value;
                            wfStepCertConfig.subjectDN = element.get('subjectDN').value;
                            wfStepCertConfig.subjectAN = element.get('subjectANDisplayValue').value;

                            wfStepCertConfigs.push(wfStepCertConfig);
                        }
                    });
                    workflowStep.wfStepCertConfigs = wfStepCertConfigs;
                    const wfStepGenericConfigs: WFStepGenericConfig[] = [];

                    const wfStepGenericConfigCertExpiryNotifyDays: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfigCertExpiryNotifyDays.name = 'NOTIFY_USER_ABOUT_CERT_EXPIRING_IN_DAYS';
                    wfStepGenericConfigCertExpiryNotifyDays.value = this.workflowForm.get('certs').get('noOfDaysBeforeCertExpiry').value;

                    const wfStepGenericConfigEmailNotificationFrequency: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfigEmailNotificationFrequency.name = 'EMAIL_NOTIFICATION_FREQUENCY';
                    wfStepGenericConfigEmailNotificationFrequency.value = this.workflowForm.get('certs.email').value;

                    wfStepGenericConfigs.push(wfStepGenericConfigCertExpiryNotifyDays);
                    wfStepGenericConfigs.push(wfStepGenericConfigEmailNotificationFrequency);
                    workflowStep.wfStepGenericConfigs = wfStepGenericConfigs;
                    workflowSteps.push(workflowStep);
                }
                if (this.selectedSteps.some(c => c === 'POST_PERSO_AND_ISSUANCE')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'POST_PERSO_AND_ISSUANCE';
                    const wfStepGenericConfigs: WFStepGenericConfig[] = [];

                    const wfStepGenericConfigPIN: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfigPIN.name = 'VERIFY_WITH_PIN';
                    if (this.workflowForm.get('verificationPolices').get('verifyWithPIN').value) {
                        wfStepGenericConfigPIN.value = 'Y';
                    } else {
                        wfStepGenericConfigPIN.value = 'N';
                    }

                    const wfStepGenericConfigMOC: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStepGenericConfigMOC.name = 'VERIFY_WITH_MOC_FINGERPRINT';
                    if (this.workflowForm.get('verificationPolices').get('verifyWithMOCFingerprint').value) {
                        wfStepGenericConfigMOC.value = 'Y';
                    } else {
                        wfStepGenericConfigMOC.value = 'N';
                    }

                    wfStepGenericConfigs.push(wfStepGenericConfigPIN);
                    wfStepGenericConfigs.push(wfStepGenericConfigMOC);

                    workflowStep.wfStepGenericConfigs = wfStepGenericConfigs;
                    workflowSteps.push(workflowStep);
                }
            }
            if (this.requiredMobileIssuance) {
                if (this.selectedSteps.some(c => c === 'MOBILE_ID_HARDWARE_BACKED_KEYSTORE')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'MOBILE_ID_HARDWARE_BACKED_KEYSTORE';
                    workflowSteps.push(workflowStep);
                }
                if (this.selectedSteps.some(c => c === 'MOBILE_ID_ONBOARDING_CONFIG')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'MOBILE_ID_ONBOARDING_CONFIG';
                    const wfStepGenericConfigs: WFStepGenericConfig[] = [];
                    // check for self service onboarding check and MeetIdentity Wallet issuance check and set those values

                    const wfStpGCEnrollUsingExistingIds: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStpGCEnrollUsingExistingIds.name = 'ONBOARD_ENROLL_USING_EXISTING_IDENTITY';
                    if (this.workflowForm.get('mobileId.onBoardingConfig').get('selfServiceOptions.onboardEnrollUsers').value) {
                        wfStpGCEnrollUsingExistingIds.value = 'Y';
                    } else {
                        wfStpGCEnrollUsingExistingIds.value = 'N';
                    }
                    wfStepGenericConfigs.push(wfStpGCEnrollUsingExistingIds);
                    if (wfStpGCEnrollUsingExistingIds.value === 'Y') {
                        // set Trusted Identities, scan qr code,.....
                        const trustedExistingIdentities: OrganizationIdentity[] = [];
                        this.workflowForm
                            .get('mobileId.onBoardingConfig.selfServiceOptions')
                            .get('trustedIdentitiesAllowed')
                            .value.forEach(identity => {
                                const organizationIdentity = new OrganizationIdentity();
                                organizationIdentity.id = identity.id;
                                organizationIdentity.identityTypeName = identity.identityTypeName;
                                trustedExistingIdentities.push(organizationIdentity);
                            });
                        workflowStep.wfMobileIDStepTrustedIdentities = trustedExistingIdentities;
                        const wfStpGCScanQRCode: WFStepGenericConfig = new WFStepGenericConfig();
                        wfStpGCScanQRCode.name = 'SCAN_QR_CODE';
                        if (
                            this.workflowForm
                                .get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions')
                                .get('scanQrCode').value
                        ) {
                            wfStpGCScanQRCode.value = 'Y';
                        } else {
                            wfStpGCScanQRCode.value = 'N';
                        }
                        wfStepGenericConfigs.push(wfStpGCScanQRCode);
                        const wfStpGCTapAndReadThroughNFC: WFStepGenericConfig = new WFStepGenericConfig();
                        wfStpGCTapAndReadThroughNFC.name = 'TAP_AND_READ_THROUGH_NFC';
                        if (
                            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions').get('nfc').value
                        ) {
                            wfStpGCTapAndReadThroughNFC.value = 'Y';
                        } else {
                            wfStpGCTapAndReadThroughNFC.value = 'N';
                        }
                        wfStepGenericConfigs.push(wfStpGCTapAndReadThroughNFC);
                        const wfStpGCCaptureFrontDoc: WFStepGenericConfig = new WFStepGenericConfig();
                        wfStpGCCaptureFrontDoc.name = 'CAPTURE_FRONT_OF_DOCUMENT';
                        if (
                            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions').get('frontDoc')
                                .value
                        ) {
                            wfStpGCCaptureFrontDoc.value = 'Y';
                        } else {
                            wfStpGCCaptureFrontDoc.value = 'N';
                        }
                        wfStepGenericConfigs.push(wfStpGCCaptureFrontDoc);
                        const wfStpGCCaptureBackDoc: WFStepGenericConfig = new WFStepGenericConfig();
                        wfStpGCCaptureBackDoc.name = 'CAPTURE_BACK_OF_DOCUMENT';
                        if (
                            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions').get('backDoc')
                                .value
                        ) {
                            wfStpGCCaptureBackDoc.value = 'Y';
                        } else {
                            wfStpGCCaptureBackDoc.value = 'N';
                        }
                        wfStepGenericConfigs.push(wfStpGCCaptureBackDoc);
                    }
                    const wfStpGCFaceVerify: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStpGCFaceVerify.name = 'FACE_VERIFY_DURING_ONBOARDING_REQUIRED';
                    if (this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions').get('faceVerification').value) {
                        wfStpGCFaceVerify.value = 'Y';
                    } else {
                        wfStpGCFaceVerify.value = 'N';
                    }
                    wfStepGenericConfigs.push(wfStpGCFaceVerify);
                    if (wfStpGCFaceVerify.value === 'Y') {
                        const faceVerifyOptions = this.workflowForm.get(
                            'mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions'
                        );
                        const wfStpGCSaveFaceForVD: WFStepGenericConfig = new WFStepGenericConfig();
                        wfStpGCSaveFaceForVD.name = 'SAVE_CAPTURED_FACE_FOR_VISUAL_ID';
                        if (faceVerifyOptions.get('saveTheCapturedFaceForVisualID').value) {
                            wfStpGCSaveFaceForVD.value = 'Y';
                        } else {
                            wfStpGCSaveFaceForVD.value = 'N';
                        }
                        wfStepGenericConfigs.push(wfStpGCSaveFaceForVD);
                        if (wfStpGCSaveFaceForVD.value === 'Y') {
                            const wfStepGenericConfigCropSize: WFStepGenericConfig = new WFStepGenericConfig();
                            wfStepGenericConfigCropSize.name = 'CROP_SIZE';
                            wfStepGenericConfigCropSize.value = faceVerifyOptions.get('facialCropSize').value;
                            if (wfStepGenericConfigCropSize.value !== '') {
                                wfStepGenericConfigs.push(wfStepGenericConfigCropSize);
                            }
                        }
                    }
                    const wfStpGCApprovalReq: WFStepGenericConfig = new WFStepGenericConfig();
                    wfStpGCApprovalReq.name = 'APPROVAL_OR_ADJUDICATION_REQUIRED';
                    if (this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions').get('approvalOrAdjudicationReq').value) {
                        wfStpGCApprovalReq.value = 'Y';
                    } else {
                        wfStpGCApprovalReq.value = 'N';
                    }
                    wfStepGenericConfigs.push(wfStpGCApprovalReq);
                    if (wfStpGCApprovalReq.value === 'Y') {
                        const adjGroups: Group[] = [];
                        this.workflowForm
                            .get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReqOptions')
                            .get('adjudicationGroups')
                            .value.forEach(adjGroup => {
                                const group = new Group();
                                group.id = adjGroup.id;
                                group.name = adjGroup.name;
                                adjGroups.push(group);
                            });
                        workflowStep.wfStepAdjudicationGroupConfigs = adjGroups;
                    }
                    workflowStep.wfStepGenericConfigs = wfStepGenericConfigs;
                    workflowSteps.push(workflowStep);
                }
                if (this.selectedSteps.some(c => c === 'MOBILE_ID_IDENTITY_ISSUANCE')) {
                    const workflowStep: WorkflowStep = new WorkflowStep();
                    workflowStep.name = 'MOBILE_ID_IDENTITY_ISSUANCE';

                    const wfStepGenericConfigs: WFStepGenericConfig[] = [];
                    if (this.workflowForm.get('mobileId.digitalIdentityIssuance.bindExpirationToMobileID').value) {
                        const wfStpGCbindExpirationToMobileID: WFStepGenericConfig = new WFStepGenericConfig();
                        wfStpGCbindExpirationToMobileID.name = 'BIND_EXPIRATION_WITH_EXISTING_IDENTITY_USER';
                        if (this.workflowForm.get('mobileId.digitalIdentityIssuance.bindExpirationToMobileID').value) {
                            wfStpGCbindExpirationToMobileID.value = 'Y';
                        } else {
                            wfStpGCbindExpirationToMobileID.value = 'N';
                        }
                        wfStepGenericConfigs.push(wfStpGCbindExpirationToMobileID);
                        workflowStep.wfStepGenericConfigs = wfStepGenericConfigs;
                    }
                    const mobileIDIdentities: WFMobileIDStepIdentityConfig[] = [];
                    // get friendly name, groups and visual ids
                    (this.workflowForm.get(
                        'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
                    ) as FormArray).controls.forEach((formGroup: FormGroup) => {
                        const mobileIDIdentity: WFMobileIDStepIdentityConfig = new WFMobileIDStepIdentityConfig();
                        mobileIDIdentity.wfMobileIDStepIdentityConfigId = formGroup.get('id').value;
                        mobileIDIdentity.friendlyName = formGroup.get('friendlyName').value;
                        mobileIDIdentity.pushVerify = formGroup.get('pushVerify').value;
                        mobileIDIdentity.softOTP = formGroup.get('softOtp').value;
                        mobileIDIdentity.fido2 = formGroup.get('fido2').value;
                        mobileIDIdentity.consent = formGroup.get('consent').value;
                        mobileIDIdentity.isContentSigning = formGroup.get('isContentSigning').value;
                        if (mobileIDIdentity.softOTP === true) {
                            mobileIDIdentity.wfMobileIDIdentitySoftOTPConfig = new WFMobileIDIdentitySoftOTPConfig();
                            mobileIDIdentity.wfMobileIDIdentitySoftOTPConfig.softOTPType = 'Time Based';
                            mobileIDIdentity.wfMobileIDIdentitySoftOTPConfig.algorithm = 'SHA256';
                            // mobileIDIdentity.wfMobileIDStepIdentitySoftOTPConfig.secretKey = 'xxxxxxxxxxxx';
                            mobileIDIdentity.wfMobileIDIdentitySoftOTPConfig.digits = 6;
                            mobileIDIdentity.wfMobileIDIdentitySoftOTPConfig.interval = 30;
                        }
                        const mobileGroups: WFMobileIdStepVisualIDGroupConfig[] = [];
                        (formGroup.get('mobileVisualDesigns') as FormArray).controls.forEach(visualID => {
                            if (visualID.get('mobileVisualTemplateGroup').value.length > 1) {
                                visualID.get('mobileVisualTemplateGroup').value.forEach((groupId: string) => {
                                    const group2 = new WFMobileIdStepVisualIDGroupConfig();
                                    group2.referenceGroupId = groupId;
                                    const g: Group = this.workflowForm
                                        .get('general')
                                        .get('selectedGroups')
                                        .value.find((c: Group) => c.id === groupId);
                                    group2.referenceGroupName = g.name;
                                    group2.visualTemplateId = visualID.get('visualTemplateID').value;
                                    mobileGroups.push(group2);
                                });
                            } else {
                                const group2 = new WFMobileIdStepVisualIDGroupConfig();
                                group2.referenceGroupId = visualID.get('mobileVisualTemplateGroup').value[0];
                                const g: Group = this.workflowForm
                                    .get('general')
                                    .get('selectedGroups')
                                    .value.find((c: Group) => c.id === visualID.get('mobileVisualTemplateGroup').value[0]);
                                group2.referenceGroupName = g.name;
                                group2.visualTemplateId = visualID.get('visualTemplateID').value;
                                mobileGroups.push(group2);
                            }
                        });
                        mobileIDIdentity.wfMobileIdStepVisualIDGroupConfigs = mobileGroups;
                        const wFMobileIDStepCerts: WFMobileIDStepCertificates[] = [];
                        // check for mobile certs selection and configuration values
                        if (formGroup.get('issueCertificates').value) {
                            // mobile certs are selected
                            (formGroup.get('addCertificatesArray') as FormArray).controls.forEach((mobileCert: FormGroup) => {
                                const wFMobileIDStepCert = new WFMobileIDStepCertificates();
                                wFMobileIDStepCert.wfMobileIdStepCertId = mobileCert.get('id').value;
                                wFMobileIDStepCert.certType = mobileCert.get('certType').value.value;
                                wFMobileIDStepCert.caServer = mobileCert.get('caServer').value.id;
                                wFMobileIDStepCert.certTemplate = mobileCert.get('certProfile').value;
                                wFMobileIDStepCert.keyEscrow = mobileCert.get('escrow').value;
                                wFMobileIDStepCert.disableRevoke = mobileCert.get('revocation').value;
                                wFMobileIDStepCert.algorithm = mobileCert.get('algorithm').value;
                                wFMobileIDStepCert.keysize = mobileCert.get('keySize').value;
                                // need to work on these two fields
                                wFMobileIDStepCert.subjectDN = mobileCert.get('subDn').value;
                                wFMobileIDStepCert.subjectAN = mobileCert.get('subAN').value;

                                wFMobileIDStepCerts.push(wFMobileIDStepCert);
                            });
                            mobileIDIdentity.wfMobileIdStepCertificates = wFMobileIDStepCerts;
                            const wfMobileIDStepCertConfig = new WFMobileIDStepCertConfig();
                            if (formGroup.get('notifyUsers').value !== '') {
                                const notifyUserConfigValue = new WFStepGenericConfig();
                                notifyUserConfigValue.name = 'NOTIFY_USER_ABOUT_CERT_EXPIRING_IN_DAYS';
                                notifyUserConfigValue.value = formGroup.get('notifyUsers').value;
                                wfMobileIDStepCertConfig.noOfDaysBeforeCertExpiry = notifyUserConfigValue;
                            }
                            if (formGroup.get('emailFrequency').value !== '') {
                                const emailFrequencyConfigValue = new WFStepGenericConfig();
                                emailFrequencyConfigValue.name = 'EMAIL_NOTIFICATION_FREQUENCY';
                                emailFrequencyConfigValue.value = formGroup.get('emailFrequency').value;
                                wfMobileIDStepCertConfig.emailNotificationFreq = emailFrequencyConfigValue;
                            }
                            mobileIDIdentity.wfMobileIDStepCertConfig = wfMobileIDStepCertConfig;
                        }
                        mobileIDIdentities.push(mobileIDIdentity);
                    });
                    workflowStep.wfMobileIDStepIdentityConfigs = mobileIDIdentities;
                    workflowSteps.push(workflowStep);
                }
            }
        }

        workflow.workflowSteps = workflowSteps;

        if (this.editWorkflow) {
            this.workflowService.editWorkflow(this.editWorkflowObj.name, workflow).subscribe(
                response => {
                    this.translateService
                        .get('createWorkflow.messages.successfullyUpdated', { wfName: this.editWorkflowObj.name })
                        .subscribe(res => {
                            this.successMessage = res;
                            this.saveSuccess = true;
                            this.saveFailure = false;
                            this.isLoadingResults = false;
                        });
                },
                error => {
                    this.saveFailure = true;
                    this.saveSuccess = false;
                    this.errorMessage = true;
                    this.isLoadingResults = false;
                    if (error.status === 409 || error.status === 400) {
                        this.translateService.get('createWorkflow.errors.friendlyName').subscribe(res => {
                            this.message = res;
                        });
                    } else if (error.status === 408) {
                        this.serverTimeOut = true;
                    } else {
                        this.message = error.error.detail;
                    }
                    window.scrollTo(0, 0);
                }
            );
        } else {
            this.workflowService.createWorkflow(workflow).subscribe(
                response => {
                    if (response.status === 201) {
                        this.translateService.get('createWorkflow.messages.successfullysaved').subscribe(res => {
                            this.successMessage = res;
                            this.saveSuccess = true;
                            this.saveFailure = false;
                            this.isLoadingResults = false;
                        });
                    }
                },
                error => {
                    this.isLoadingResults = false;
                    this.saveFailure = true;
                    this.saveSuccess = false;
                    this.errorMessage = true;
                    this.message = error.error.detail;
                    if (error.status === 409 || error.status === 400) {
                        this.translateService.get('createWorkflow.errors.friendlyName').subscribe(res => {
                            this.message = res;
                        });
                    } else if (error.status === 408) {
                        this.serverTimeOut = true;
                    } else {
                        this.message = error.error.detail;
                    }
                    window.scrollTo(0, 0);
                }
            );
        }
    }
    prepareSubANData(checkName: string, textValue: string, subjectAN: string) {
        if (subjectAN !== '') {
            subjectAN = subjectAN + `,${checkName}`;
        } else {
            subjectAN = subjectAN + checkName;
        }
        if (textValue !== null) {
            if (textValue.length !== 0) {
                subjectAN = subjectAN + `=${textValue}`;
            }
        }
        return subjectAN;
    }
    prepareSubAN(c) {
        const formGroup = ((this.workflowForm.get('cardCertificates') as FormArray).at(c) as FormGroup).get('subjectAN') as FormGroup;
        let subjectAN = '';
        if (formGroup === null) {
            return subjectAN;
        }
        if (formGroup.get('otherNameCheck').value) {
            subjectAN = subjectAN + SubjectANEnum.OTHER_NAME;
        }
        if (formGroup.get('rfcNameCheck').value) {
            subjectAN = this.prepareSubANData(SubjectANEnum.RFC822NAME, '', subjectAN);
        }
        if (formGroup.get('dnsNameCheck').value) {
            subjectAN = this.prepareSubANData(SubjectANEnum.DNS_NAME, '', subjectAN);
        }
        if (formGroup.get('x400AddressCheck').value) {
            subjectAN = this.prepareSubANData(SubjectANEnum.X400ADDRESS, '', subjectAN);
        }
        if (formGroup.get('directoryNameCheck').value) {
            subjectAN = this.prepareSubANData(SubjectANEnum.DIRECTORY_NAME, '', subjectAN);
        }
        if (formGroup.get('uriCheck').value) {
            subjectAN = this.prepareSubANData(SubjectANEnum.URI, '', subjectAN);
        }
        if (formGroup.get('ediPartyCheck').value) {
            const ediPartyText = formGroup.get('ediPartyText').value;
            subjectAN = this.prepareSubANData(SubjectANEnum.EDI_PARTY_NAME, ediPartyText, subjectAN);
        }
        if (formGroup.get('ipAddressCheck').value) {
            subjectAN = this.prepareSubANData(SubjectANEnum.IP_ADDRESS, '', subjectAN);
        }
        if (formGroup.get('regIdCheck').value) {
            const regIdText = formGroup.get('regIdText').value;
            subjectAN = this.prepareSubANData(SubjectANEnum.REG_ID, regIdText, subjectAN);
        }
        ((this.workflowForm.get('cardCertificates') as FormArray).at(c) as FormGroup).get('subjectANDisplayValue').setValue(subjectAN);
        return subjectAN;
    }

    onCancelSubAN(c) {
        ((this.workflowForm.get('cardCertificates') as FormArray).at(c) as FormGroup).get('subjectANDisplayValue').setValue('');
    }
    /** =================================================== Create Form and Form Groups Logic ======================================================================= */
    createWorkflowForm() {
        this.workflowForm = this.formBuilder.group({
            //  general section
            general: this.formBuilder.group({
                wfName: [
                    '',
                    [
                        Validators.required,
                        Validators.pattern('^[a-zA-Z0-9_ ]+$'),
                        Validators.minLength(this.wfNameMinlength),
                        Validators.maxLength(this.wfNameMaxlength)
                    ]
                ],
                description: [''],
                defaultWorkflow: [false],
                selectedDeviceProfiles: ['', [Validators.required]],
                identityModel: ['', [Validators.required]],
                selectedGroups: ['', [Validators.required]],
                maxAllowedDevices: this.formBuilder.group({
                    isAllowedDevices: [false],
                    // Validators.pattern('^(0?[1-9]|[1-9][0-9])$')
                    allowedDevices: ['0', [Validators.required, maxLengthValidator(100), Validators.pattern('^[0-9]+$')]]
                }),
                months: ['0'],
                days: ['0'],
                disableExpiration: [false],
                revokeEncryptionCert: [false]
            }),
            // general ends

            // Step Selection Flags
            isbiometricsEnabled: [false],
            biometricEnrollment: this.formBuilder.group({
                idProofingChecked: [false],
                enrollmentFormChecked: [false],
                enrollmentForm: this.formBuilder.array([
                    this.formBuilder.group({
                        fieldName: ['', [Validators.required]],
                        fieldLabel: [''],
                        required: [false],
                        isMandatory: [''],
                        regOrgIdentityFieldsList: ['']
                    })
                ]),
                faceChecked: [false],
                face: this.formBuilder.group({
                    icaoCompliance: [false],
                    cropSize: ['', [Validators.required]],
                    // transparencyCheckEnabled: [this.isTransparentPhotoEnabled],
                    transparentPhotoAllowed: ['']
                }),
                irisChecked: [false],
                iris: this.formBuilder.group({
                    irisMode: ['']
                }),
                fingerprintChecked: [false],
                fingerprint: this.formBuilder.group({
                    numberOfRequiredFingers: ['', [Validators.required]],
                    minimumFingersThreshhold: ['', [Validators.required]],
                    enableRollFinger: [false],
                    enableWSQ: [false]
                }),
                signatureChecked: [false],
                approvalChecked: [false],
                approval: this.formBuilder.group({
                    approvalGroup: ['', [Validators.required]]
                })
            }),
            tokenIssuanceChecked: [false],
            appletSelectionFormGroup: this.formBuilder.group({
                enablePivApplet: [false],
                enableFido2Applet: [false]
            }),
            fidoe2AppletDetails: this.formBuilder.group({
                attestationCert: ['', Validators.required],
                attestationCertPvtKey: ['', Validators.required],
                aaguid: ['', Validators.required]
            }),
            chipEncode: this.formBuilder.group({
                isChipEncode: [false],
                chipEncodeGroup: ['', Validators.required]
            }),
            chipPlusVisualIDPrint: this.formBuilder.group({
                isChipPlusVisualIDPrint: [false],
                chipPlusVisualIDGroup: ['', Validators.required]
            }),
            visualIDPrint: this.formBuilder.group({
                isVisualIDPrint: [false],
                visualIDPrintGroup: ['', Validators.required]
            }),
            // Issuance
            issueCertsToCard: this.formBuilder.group({
                tokenIssueCerts: [false]
            }),
            cardVisualDesigns: this.formBuilder.array([]),
            // Card Certificates
            cardCertificates: this.formBuilder.array([
                this.formBuilder.group({
                    id: [''],
                    certType: [{ value: '', disabled: true }, [Validators.required]],
                    certTypeList: [''],
                    caServer: [{ value: '', disabled: true }, [Validators.required]],
                    certProfile: [{ value: '', disabled: true }, [Validators.required]],
                    certProfileList: [''],
                    canChangeCertType: [true],
                    hideKeyEscrow: [true],
                    escrow: [false],
                    revocation: [false],
                    algorithm: [{ value: '', disabled: true }, [Validators.required]],
                    keySize: [{ value: '', disabled: true }, [Validators.required]],
                    keySizeList: [''],
                    subjectDN: [{ value: '', disabled: true }, [Validators.required]],
                    subjectDNDisplayValue: [''],
                    subjectANDisplayValue: [''],
                    subjectAN: this.formBuilder.group({
                        otherNameCheck: [false],
                        rfcNameCheck: [false],
                        dnsNameCheck: [false],
                        x400AddressCheck: [false],
                        directoryNameCheck: [false],
                        uriCheck: [false],
                        ediPartyCheck: [false],
                        ediPartyText: [{ value: '', disabled: true }],
                        ipAddressCheck: [false],
                        regIdCheck: [false],
                        regIdText: [{ value: '', disabled: true }]
                    })
                })
            ]),
            certs: this.formBuilder.group({
                contentSign: [false],
                noOfDaysBeforeCertExpiry: ['', [Validators.required]],
                email: ['', [Validators.required]]
            }),
            activation: [],
            // Activation Starts
            verificationPolices: this.formBuilder.group({
                verifyWithPIN: [false],
                verifyWithMOCFingerprint: [false],
                verifyWithPinFace: [false],
                verifyWithPinFp: [false]
            }),
            // verificationPolices: this.buildActivationPolicies()
            //  verificationPolices: this.formBuilder.array([
            // ]),
            mobileId: this.formBuilder.group({
                isMobileIdReq: [false],
                enforceHardwareBackeKeystrore: [false],
                onBoardingConfig: this.formBuilder.group({
                    isSelfServiceReq: [false],
                    selfServiceOptions: this.formBuilder.group({
                        onboardEnrollUsers: [false],
                        trustedIdentitiesAllowed: ['', [Validators.required]],
                        onboardEnrollUsersOptions: this.formBuilder.group({
                            scanQrCode: [false],
                            nfc: [false],
                            frontDoc: [false],
                            backDoc: [false]
                        }),
                        faceVerification: [false],
                        faceVerificationOptions: this.formBuilder.group({
                            saveTheCapturedFaceForVisualID: [false],
                            facialCropSize: ['', [Validators.required]]
                        }),
                        approvalOrAdjudicationReq: [false],
                        approvalOrAdjudicationReqOptions: this.formBuilder.group({
                            adjudicationGroups: ['', [Validators.required]]
                        })
                    })
                }),

                // DIGITAL IDENTITY ISSUANCE Group
                digitalIdentityIssuance: this.formBuilder.group({
                    enableMobileID: [false],
                    bindExpirationToMobileID: [false],
                    additionalIdentity: this.formBuilder.group({
                        // identityCredentialsArray starts
                        identityCredentialsArray: this.formBuilder.array([
                            this.formBuilder.group({
                                id: [''],
                                friendlyName: ['', Validators.required],
                                mobileVisualDesigns: this.formBuilder.array([]),
                                pushVerify: [false],
                                softOtp: [false],
                                fido2: [false],
                                consent: [false],
                                isContentSigning: [false],
                                issueCertificates: [false],
                                addCertificatesArray: this.formBuilder.array([
                                    this.formBuilder.group({
                                        id: [''],
                                        certType: ['', Validators.required],
                                        certTypeList: [''],
                                        caServer: ['', Validators.required],
                                        certProfile: ['', Validators.required],
                                        certProfileList: [''],
                                        hideKeyEscrow: [true],
                                        escrow: [false],
                                        revocation: [false],
                                        algorithm: ['', Validators.required],
                                        keySize: ['', Validators.required],
                                        keySizeList: [''],
                                        subDn: ['', Validators.required],
                                        subDnDisplayValue: [''],
                                        subAN: ['']
                                    })
                                ]),
                                notifyUsers: ['', Validators.required],
                                emailFrequency: ['', Validators.required]
                            })
                        ]) // identityCredentialsArray ends
                    })
                })
            }) // DIGITAL IDENTITY ISSUANCE Group ends

            // Mobile ID ends
        }); // form ends
    }
    onKeyUp(event: KeyboardEvent) {
        this.wfNameValidationError = false;
        this.wfNameValidationNumberError = false;
        if (this.generalForm.controls.wfName.value.charAt(0) === ' ' || this.generalForm.controls.wfName.value.charAt(0) === '_') {
            this.generalForm.controls.wfName.setErrors(Validators.pattern);
            this.wfNameValidationError = true;
        }
        if (this.generalForm.controls.wfName.value.charAt(0) >= '0' && this.generalForm.controls.wfName.value.charAt(0) <= '9') {
            this.wfNameValidationNumberError = true;
            this.generalForm.controls.wfName.setErrors(Validators.pattern);
        }
    }
    buildActivationPolicies() {
        const arr = this.activationTypes.map(activationType => {
            return this.formBuilder.control(false);
        });
        return this.formBuilder.array(arr);
    }
    get verificationPolicies() {
        return this.workflowForm.get('verificationPolices');
    }

    /** ==================================================================== Handle Edit Workflow Mode Logic ======================================================== */
    async handleEditWorkflowMode() {
        // this.isLoadingResults = true;
        const editMode = this.route.snapshot.paramMap.get('editMode');
        const clone = this.route.snapshot.paramMap.get('clone');
        if (clone) {
            this.cloneWorkflow = true;
        } else if (editMode) {
            this.editWorkflow = true;
        }
        if (editMode || clone) {
            this.workflowService.getWorkflowByName(this.route.snapshot.paramMap.get('wfName')).subscribe(async workflow => {
                // Load CA Servers if PKI certs step is there
                const wfMobileIdentityIssuanceStep = workflow.workflowSteps.find(e => e.name === 'MOBILE_ID_IDENTITY_ISSUANCE');
                let isMobileCertsExists = false;
                if (wfMobileIdentityIssuanceStep !== undefined) {
                    if (wfMobileIdentityIssuanceStep.wfMobileIDStepIdentityConfigs.length > 0) {
                        wfMobileIdentityIssuanceStep.wfMobileIDStepIdentityConfigs.forEach(e => {
                            if (e.wfMobileIdStepCertificates.length > 0) {
                                isMobileCertsExists = true;
                            }
                        });
                    }
                }
                if (workflow.workflowSteps.find(e => e.name === 'PKI_CERTIFICATES') !== undefined || isMobileCertsExists) {
                    console.log('inside pki certs');
                    try {
                        const caProviders: CAProvider[] = await this.workflowService.getCaProviders().toPromise();
                        for (let i = 0; i < caProviders.length; i++) {
                            const caServerConfigs: CAServerConfig[] = await this.workflowService
                                .getCaServers(caProviders[i].id)
                                .toPromise();
                            caServerConfigs.forEach(caServerConfig => {
                                caServerConfig.caProviderName = caProviders[i].name;
                                this.caServers.push(caServerConfig);
                            });
                        }
                        if (workflow.workflowSteps.find(e => e.name === 'PKI_CERTIFICATES') !== undefined) {
                            const wfStepCertConfigs = workflow.workflowSteps.find(e => e.name === 'PKI_CERTIFICATES').wfStepCertConfigs;
                            if (wfStepCertConfigs !== undefined) {
                                const caServerIds = [];
                                wfStepCertConfigs.forEach(e => caServerIds.push(+e.caServer));
                                const certTemplatesList = <CertTemplate[]>await this.workflowService
                                    .getCertTemplatesByIds(caServerIds, 'card')
                                    .toPromise();
                                this.caServers.forEach(caServer => {
                                    const certTemplates1 = certTemplatesList.find(e => e.caServerConfigId === caServer.id);
                                    if (certTemplates1 !== undefined) {
                                        caServer.certTemplates = certTemplates1.certTemplates;
                                    }
                                });
                            }
                        }
                    } catch (error) {
                        console.log('in catch');
                        this.isLoadingResults = false;
                    }
                }
                this.editWorkflowObj = workflow;
                if (workflow.organizationIdentities !== null) {
                    // Load Org Reg Identity Fields
                    if (workflow.organizationIdentities.identityTypeName !== null) {
                        this.workflowForm
                            .get('general')
                            .get('identityModel')
                            .setValue(
                                this.identityModels.filter(
                                    idType => idType.identityTypeName === workflow.organizationIdentities.identityTypeName
                                )[0]
                            );
                    }
                    this.workflowService
                        .getFaceCropSizes(this.workflowForm.get('general').get('identityModel').value.identityTypeName)
                        .subscribe(configIdValues => {
                            this.cropSizes = configIdValues;
                        });
                    this.isGroupDisabled = false;
                    this.visualTemplates = await this.workflowService
                        .getVisualTemplates(this.workflowForm.get('general').get('identityModel').value.identityTypeName)
                        .toPromise();
                    if (this.visualTemplates.length > 0) {
                        this.smartCardVisualTemplates = [];
                        this.mobileVisualTemplates = [];
                        this.visualTemplates.forEach(e => {
                            e.deviceTypes.forEach(device => {
                                if (device.name.includes(DeviceTypeEnum.SMART_CARD)) {
                                    this.smartCardVisualTemplates.push(e);
                                } else if (device.name.includes(DeviceTypeEnum.MOBILE)) {
                                    this.mobileVisualTemplates.push(e);
                                }
                            });
                        });
                    }
                }
                // Assign workflow to form fields
                const deviceProfiles: Array<DeviceProfile> = [];
                let isSmartCardDevProfExists = false;
                let isMobileDevProfExists = false;
                workflow.deviceProfiles.forEach(deviceProfile => {
                    if (deviceProfile.deviceProfileConfig.isMobileId) {
                        isMobileDevProfExists = true;
                    } else if (!deviceProfile.deviceProfileConfig.isPlasticId) {
                        this.workflowForm.get('tokenIssuanceChecked').enable();
                        this.isPrintEnabled = true;
                        isSmartCardDevProfExists = true;
                        if (deviceProfile.deviceProfileConfig.isAppletLoadingRequired) {
                            this.showAppletSelection = true;
                        }
                    } else if (deviceProfile.deviceProfileConfig.isPlasticId) {
                        this.isPrintEnabled = true;
                    }
                });
                this.deviceProfiles.forEach(d => {
                    workflow.deviceProfiles.forEach(c1 => {
                        if (c1.name === d.name) {
                            deviceProfiles.push(d);
                        }
                    });
                });
                this.workflowForm
                    .get('general')
                    .get('selectedDeviceProfiles')
                    .setValue(deviceProfiles);

                // if (isMobileDevProfExists) {
                //     this.workflowForm.get('mobileId.isMobileIdReq').enable();
                // }
                if (isSmartCardDevProfExists) {
                    this.isPKICertsEnabled = true;
                    if (workflow.workflowSteps.find(e => e.name === 'PERSO_AND_ISSUANCE') !== undefined) {
                        this.workflowForm.get('issueCertsToCard.tokenIssueCerts').enable();
                        this.workflowForm.get('activation').enable();
                        this.workflowForm
                            .get('certs')
                            .get('contentSign')
                            .enable();
                        this.workflowForm
                            .get('chipEncode')
                            .get('isChipEncode')
                            .enable();
                        this.workflowForm
                            .get('chipPlusVisualIDPrint')
                            .get('isChipPlusVisualIDPrint')
                            .enable();
                    }
                }
                // push this workflow groups to the groups, as in groups we are geting only which dont have wf role
                if (editMode) {
                    this.workflowForm
                        .get('general')
                        .get('wfName')
                        .setValue(workflow.name);

                    this.wfn = workflow.name;
                    workflow.groups.forEach(g => {
                        g.isDisabled = true;
                        this.workflowGroups.push(g);
                    });
                    const groups: Group[] = [];
                    this.workflowGroups.forEach(c => {
                        workflow.groups.forEach(c1 => {
                            if (c1.id === c.id) {
                                groups.push(c);
                            }
                        });
                    });
                    this.workflowForm
                        .get('general')
                        .get('selectedGroups')
                        .setValue(groups);
                }
                this.workflowForm
                    .get('general')
                    .get('description')
                    .setValue(workflow.description);

                this.workflowForm
                    .get('general')
                    .get('defaultWorkflow')
                    .setValue(workflow.defaultWorkflow);
                // set groups in issuance nd meetidentity wallet if that steps are not there
                if (workflow.allowedDevices !== null && workflow.allowedDevices >= 1) {
                    this.isAllowedDevicesSelected = true;
                    this.workflowForm.get('general.maxAllowedDevices.allowedDevices').enable();
                    this.workflowForm
                        .get('general')
                        .get('maxAllowedDevices.isAllowedDevices')
                        .setValue(true);
                    this.workflowForm
                        .get('general')
                        .get('maxAllowedDevices.allowedDevices')
                        .setValue(workflow.allowedDevices);
                }
                this.workflowForm
                    .get('general')
                    .get('months')
                    .setValue(workflow.expirationInMonths);
                this.workflowForm
                    .get('general')
                    .get('days')
                    .setValue(workflow.expirationInDays);
                this.workflowForm
                    .get('general')
                    .get('disableExpiration')
                    .setValue(workflow.disableExpirationEnforcement);
                this.workflowForm
                    .get('general')
                    .get('revokeEncryptionCert')
                    .setValue(workflow.revokeEncryptionCertificate);
                this.requiredEnrollment = workflow.requiredEnrollment;
                this.requiredTokenCardIssuance = workflow.requiredTokenCardIssuance;
                this.requiredMobileIssuance = workflow.requiredMobileIssuance;
                if (this.requiredEnrollment) {
                    this.workflowForm.get('isbiometricsEnabled').setValue(true);
                    this.workflowForm.get('biometricEnrollment.idProofingChecked').enable();
                    this.workflowForm.get('biometricEnrollment.enrollmentFormChecked').enable();
                    if (clone) {
                        this.workflowForm.get('biometricEnrollment.faceChecked').enable();
                        this.workflowForm.get('biometricEnrollment.irisChecked').enable();
                        this.workflowForm.get('biometricEnrollment.fingerprintChecked').enable();
                    }
                    this.workflowForm.get('biometricEnrollment.signatureChecked').enable();
                    this.workflowForm.get('biometricEnrollment.approvalChecked').enable();
                }
                if (this.requiredTokenCardIssuance) {
                    this.workflowForm.get('tokenIssuanceChecked').setValue(true);
                }
                if (this.requiredMobileIssuance) {
                    this.workflowForm.get('mobileId.isMobileIdReq').setValue(true);
                    this.workflowForm.get('mobileId.enforceHardwareBackeKeystrore').enable();
                    this.workflowForm.get('mobileId.onBoardingConfig.isSelfServiceReq').enable();
                    // this.workflowForm.get('mobileId.digitalIdentityIssuance.enableMobileID').enable();
                }
                if (workflow.workflowSteps.find(e => e.name === 'REGISTRATION') === undefined) {
                    this.workflowService
                        .getRegOrgIdentityFields(this.workflowForm.get('general').get('identityModel').value.identityTypeName)
                        .subscribe(idenityFields => {
                            this.regOrgIdentityFields = idenityFields;
                            if (this.regOrgIdentityFields.length > 0) {
                                (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).removeAt(0);
                                this.regOrgIdentityFields.forEach((registrationField: RegOrgIdentityField) => {
                                    this.addFormGroup(registrationField.orgIdentityFieldId, registrationField.mandatory);
                                });
                                (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).disable();
                            }
                        });
                }
                let visualTemplateId;
                if (editMode) {
                    if (workflow.workflowSteps.find(e => e.name === 'PERSO_AND_ISSUANCE') === undefined) {
                        if (this.smartCardVisualTemplates.length > 0) {
                            visualTemplateId = this.smartCardVisualTemplates[0].id;
                        } else {
                            visualTemplateId = '';
                        }
                        for (let i = 0; i < this.workflowForm.get('general').get('selectedGroups').value.length; i++) {
                            const group: string[] = [];
                            group.push(this.workflowForm.get('general').get('selectedGroups').value[i].id);
                            (this.workflowForm.get('cardVisualDesigns') as FormArray).push(
                                this.formBuilder.group({
                                    cardVisualTemplateGroup: [group, [Validators.required]],
                                    visualTemplateID: [visualTemplateId, [Validators.required]]
                                })
                            );
                        }
                        (this.workflowForm.get('cardVisualDesigns') as FormArray).disable();
                    }
                    if (workflow.workflowSteps.find(e => e.name === 'MOBILE_ID_IDENTITY_ISSUANCE') === undefined) {
                        if (this.mobileVisualTemplates.length > 0) {
                            visualTemplateId = this.mobileVisualTemplates[0].id;
                        } else {
                            visualTemplateId = '';
                        }
                        (this.workflowForm.get(
                            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
                        ) as FormArray).controls.forEach(control => {
                            for (let i = 0; i < this.workflowForm.get('general').get('selectedGroups').value.length; i++) {
                                const group: string[] = [];
                                group.push(this.workflowForm.get('general').get('selectedGroups').value[i].id);
                                (control.get('mobileVisualDesigns') as FormArray).push(
                                    this.formBuilder.group({
                                        mobileVisualTemplateGroup: [group, [Validators.required]],
                                        visualTemplateID: [visualTemplateId, [Validators.required]]
                                    })
                                );
                            }
                            (control.get('mobileVisualDesigns') as FormArray).disable();
                        });
                    }
                }
                // Assign Steps
                workflow.workflowSteps.forEach((workflowStep: WorkflowStep) => {
                    if (workflowStep.name === 'ID_PROOFING') {
                        this.workflowForm.get('biometricEnrollment.idProofingChecked').setValue(true);
                        this.selectedSteps.push('ID_PROOFING');
                    }
                    if (workflowStep.name === 'REGISTRATION') {
                        this.workflowForm.get('biometricEnrollment.enrollmentFormChecked').setValue(true);
                        this.showFormFields = true;
                        this.selectedSteps.push('REGISTRATION');

                        // Load Org Reg Identity Fields
                        this.workflowService
                            .getRegOrgIdentityFields(workflow.organizationIdentities.identityTypeName)
                            .subscribe((res: RegOrgIdentityField[]) => {
                                this.regOrgIdentityFields = res;
                                // Set Enrollment Fields
                                const enrollmentFormArray = this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray;
                                if (workflowStep.wfStepRegistrationConfigs !== undefined) {
                                    if (workflowStep.wfStepRegistrationConfigs.length > 0) {
                                        (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).removeAt(0);
                                    }
                                    this.regOrgIdentityFields.forEach((element: RegOrgIdentityField) => {
                                        const regOrgId = workflowStep.wfStepRegistrationConfigs.find(
                                            e => e.orgFieldId === parseInt(element.orgIdentityFieldId, 10)
                                        );
                                        if (regOrgId !== undefined) {
                                            const regOrgFielList: RegOrgIdentityField[] = this.regOrgIdentityFields.filter(
                                                e => parseInt(e.orgIdentityFieldId, 10) === regOrgId.orgFieldId
                                            );
                                            enrollmentFormArray.push(
                                                this.formBuilder.group({
                                                    fieldName: [element.orgIdentityFieldId, Validators.required],
                                                    fieldLabel: [regOrgId.fieldLabel],
                                                    required: [regOrgId.required],
                                                    isMandatory: [element.mandatory],
                                                    regOrgIdentityFieldsList: [regOrgFielList]
                                                })
                                            );
                                        } else {
                                            const regOrgFielList: RegOrgIdentityField[] = [];
                                            regOrgFielList.push(element);
                                            if (this.regOrgIdentityFields.length > workflowStep.wfStepRegistrationConfigs.length) {
                                                this.hideAddEnrollButton = false;
                                                this.deletedOrgFieldList.push(element);
                                                const orgRegIdentityField = new OrgRegIdentityField();
                                                orgRegIdentityField.fieldName = element.orgIdentityFieldId;
                                                orgRegIdentityField.fieldLabel = ''; //element.fieldDisplayName;;
                                                orgRegIdentityField.isMandatory = element.mandatory;
                                                orgRegIdentityField.regOrgIdentityFieldsList = regOrgFielList;
                                                orgRegIdentityField.required = false;
                                                this.deletedFieldList.push(orgRegIdentityField);
                                            }
                                        }
                                    });
                                }
                            });
                    } else if (workflowStep.name === 'FACE_CAPTURE') {
                        this.workflowForm.get('biometricEnrollment.faceChecked').setValue(true);
                        this.workflowForm.get('biometricEnrollment.face').enable();
                        this.selectedSteps.push('FACE_CAPTURE');
                        // Set Config
                        let icaoComplianceConfig = false;
                        let cropSizeConfig = '';
                        let transparentPhotoAllowed = false;
                        if (workflowStep.wfStepGenericConfigs !== undefined) {
                            workflowStep.wfStepGenericConfigs.forEach(genericConfig => {
                                if (genericConfig.name === 'ICAO_COMPLIANCE') {
                                    if (genericConfig.value === 'Y') {
                                        icaoComplianceConfig = true;
                                    }
                                } else if (genericConfig.name === 'CROP_SIZE') {
                                    cropSizeConfig = genericConfig.value;
                                } else if (genericConfig.name === 'TRANSPARENT_PHOTO_ALLOWED') {
                                    if (genericConfig.value === 'Y') {
                                        transparentPhotoAllowed = true;
                                    }
                                }
                            });
                        }
                        (this.workflowForm.get('biometricEnrollment.face') as FormGroup).get('cropSize').setValue(cropSizeConfig);
                        (this.workflowForm.get('biometricEnrollment.face') as FormGroup)
                            .get('icaoCompliance')
                            .setValue(icaoComplianceConfig);
                        (this.workflowForm.get('biometricEnrollment.face') as FormGroup)
                            .get('transparentPhotoAllowed')
                            .setValue(transparentPhotoAllowed);
                    } else if (workflowStep.name === 'IRIS_CAPTURE') {
                        this.workflowForm.get('biometricEnrollment.irisChecked').setValue(true);
                        this.workflowForm.get('biometricEnrollment.iris').enable();
                        this.selectedSteps.push('IRIS_CAPTURE');
                        // Set Config
                        let irisModeConfig = '';
                        if (workflowStep.wfStepGenericConfigs !== undefined) {
                            workflowStep.wfStepGenericConfigs.forEach(genericConfig => {
                                if (genericConfig.name === 'IRIS_MODE') {
                                    irisModeConfig = genericConfig.value;
                                }
                            });
                        }
                        this.workflowForm
                            .get('biometricEnrollment.iris')
                            .get('irisMode')
                            .setValue(irisModeConfig);
                    } else if (workflowStep.name === 'FINGERPRINT_CAPTURE') {
                        this.workflowForm.get('biometricEnrollment.fingerprintChecked').setValue(true);
                        this.workflowForm.get('biometricEnrollment.fingerprint').enable();
                        this.selectedSteps.push('FINGERPRINT_CAPTURE');
                        // Load Threshold Values
                        this.workflowService.getConfigIdValues('FINGERPRINT_MIN_THRESHOLD').subscribe(configIdValues => {
                            this.fingerprintThresholds = configIdValues;
                        });
                        // Set Config
                        let minFingReqConfig = '0';
                        let minFingThresConfig = '';
                        let enableWSQ = false;
                        if (workflowStep.wfStepGenericConfigs !== undefined) {
                            workflowStep.wfStepGenericConfigs.forEach(genericConfig => {
                                if (genericConfig.name === 'MINIMUM_FINGERS_REQUIRED') {
                                    minFingReqConfig = genericConfig.value;
                                } else if (genericConfig.name === 'FINGERPRINT_MIN_THRESHOLD') {
                                    minFingThresConfig = genericConfig.value;
                                } else if (genericConfig.name === 'FINGERPRINT_WSQ') {
                                    if (genericConfig.value === 'Y') {
                                        enableWSQ = true;
                                    }
                                }
                            });
                        }
                        this.workflowService.getFingerByFingerCount(minFingReqConfig).subscribe((result: Finger[]) => {
                            this.fingersByCount = result;
                        });
                        (this.workflowForm.get('biometricEnrollment.fingerprint') as FormGroup)
                            .get('numberOfRequiredFingers')
                            .setValue(minFingReqConfig);
                        (this.workflowForm.get('biometricEnrollment.fingerprint') as FormGroup)
                            .get('minimumFingersThreshhold')
                            .setValue(minFingThresConfig);
                        (this.workflowForm.get('biometricEnrollment.fingerprint') as FormGroup).get('enableWSQ').setValue(enableWSQ);
                    } else if (workflowStep.name === 'FINGERPRINT_ROLL_CAPTURE') {
                        this.selectedSteps.push('FINGERPRINT_ROLL_CAPTURE');
                        if (workflow.workflowSteps.some(c => c.name === 'FINGERPRINT_CAPTURE')) {
                            (this.workflowForm.get('biometricEnrollment.fingerprint') as FormGroup).get('enableRollFinger').setValue(true);
                        }
                    } else if (workflowStep.name === 'SIGNATURE_CAPTURE') {
                        this.workflowForm.get('biometricEnrollment.signatureChecked').setValue(true);
                        this.selectedSteps.push('SIGNATURE_CAPTURE');
                    } else if (workflowStep.name === 'APPROVAL_BEFORE_ISSUANCE') {
                        this.workflowForm.get('biometricEnrollment.approvalChecked').setValue(true);
                        this.workflowForm.get('biometricEnrollment.approval').enable();
                        this.selectedSteps.push('APPROVAL_BEFORE_ISSUANCE');
                        const approvalGroups: Group[] = [];
                        if (workflowStep.wfStepApprovalGroupConfigs !== undefined) {
                            this.groupsList.forEach(c => {
                                workflowStep.wfStepApprovalGroupConfigs.forEach(c1 => {
                                    if (c1.groupId === c.id) {
                                        approvalGroups.push(c);
                                    }
                                });
                            });
                        }
                        this.workflowForm.get('biometricEnrollment.approval.approvalGroup').setValue(approvalGroups);
                    } else if (workflowStep.name === 'PERSO_AND_ISSUANCE') {
                        this.workflowForm.get('tokenIssuanceChecked').enable();
                        if (workflowStep.wfStepAppletLoadingInfo !== null && workflowStep.wfStepAppletLoadingInfo !== undefined) {
                            const WfAppletLoadingInfo = workflowStep.wfStepAppletLoadingInfo;
                            this.showAppletSelection = true;
                            this.workflowForm.get('appletSelectionFormGroup').enable();
                            this.workflowForm
                                .get('appletSelectionFormGroup.enablePivApplet')
                                .setValue(WfAppletLoadingInfo.pivAppletEnabled);
                            this.workflowForm
                                .get('appletSelectionFormGroup.enableFido2Applet')
                                .setValue(WfAppletLoadingInfo.fido2AppletEnabled);

                            if (this.workflowForm.get('appletSelectionFormGroup.enableFido2Applet').value) {
                                this.isFidoAppletSelected = true;
                                this.workflowForm.get('fidoe2AppletDetails').enable();
                                this.workflowForm.get('fidoe2AppletDetails.attestationCert').setValue(WfAppletLoadingInfo.attestationCert);
                                this.workflowForm
                                    .get('fidoe2AppletDetails.attestationCertPvtKey')
                                    .setValue(WfAppletLoadingInfo.attestationCertPrivateKey);
                                this.workflowForm.get('fidoe2AppletDetails.aaguid').setValue(WfAppletLoadingInfo.aaguid);
                            }
                        }
                        this.workflowForm.get('tokenIssuanceChecked').setValue(true);
                        if (this.isPrintEnabled) {
                            this.workflowForm.get('visualIDPrint.isVisualIDPrint').enable();
                        }
                        // set grous with vds, printing groups, ativation types
                        this.selectedSteps.push('PERSO_AND_ISSUANCE');
                        let isContentSignRequired = false;
                        this.workflowForm
                            .get('certs')
                            .get('contentSign')
                            .enable();
                        if (workflowStep.wfStepGenericConfigs !== undefined) {
                            workflowStep.wfStepGenericConfigs.forEach(genericConfig => {
                                if (genericConfig.name === 'CONTENT_SIGNING_CERTIFICATE_REQUIRED') {
                                    if (genericConfig.value === 'Y') {
                                        isContentSignRequired = true;
                                    }
                                }
                            });
                        }
                        this.workflowForm
                            .get('certs')
                            .get('contentSign')
                            .setValue(isContentSignRequired);

                        let enableChipPerso = false;
                        let enableChipPersoVIDPrint = false;
                        let enableVisualIDPrint = false;
                        if (workflowStep.wfStepGenericConfigs !== undefined) {
                            workflowStep.wfStepGenericConfigs.forEach(genericConfig => {
                                if (genericConfig.name === 'ENABLE_CHIP_PERSO') {
                                    if (genericConfig.value === 'Y') {
                                        enableChipPerso = true;
                                        this.workflowForm.get('chipEncode.chipEncodeGroup').enable();
                                    }
                                } else if (genericConfig.name === 'ENABLE_CHIP_PERSO_PLUS_VISUAL_ID_PRINT') {
                                    if (genericConfig.value === 'Y') {
                                        enableChipPersoVIDPrint = true;
                                        this.workflowForm.get('chipPlusVisualIDPrint.chipPlusVisualIDGroup').enable();
                                    }
                                } else if (genericConfig.name === 'ENABLE_VISUAL_ID_PRINT') {
                                    if (genericConfig.value === 'Y') {
                                        enableVisualIDPrint = true;
                                        this.workflowForm.get('visualIDPrint.visualIDPrintGroup').enable();
                                    }
                                }
                            });
                        }
                        this.workflowForm.get('chipEncode.isChipEncode').setValue(enableChipPerso);
                        this.workflowForm.get('chipPlusVisualIDPrint.isChipPlusVisualIDPrint').setValue(enableChipPersoVIDPrint);
                        this.workflowForm.get('visualIDPrint.isVisualIDPrint').setValue(enableVisualIDPrint);
                        if (workflowStep.wfStepChipEncodeGroupConfigs !== undefined) {
                            if (workflowStep.wfStepChipEncodeGroupConfigs.length !== 0 && enableChipPerso) {
                                const chipEncodeGroups: Group[] = [];
                                this.groupsList.forEach(c => {
                                    workflowStep.wfStepChipEncodeGroupConfigs.forEach(c1 => {
                                        if (c1.groupId === c.id) {
                                            chipEncodeGroups.push(c);
                                        }
                                    });
                                });
                                this.workflowForm.get('chipEncode.chipEncodeGroup').setValue(chipEncodeGroups);
                            }
                        }
                        if (workflowStep.wfStepChipEncodeVIDPrintGroupConfigs !== undefined) {
                            if (workflowStep.wfStepChipEncodeVIDPrintGroupConfigs.length !== 0 && enableChipPersoVIDPrint) {
                                const chipEncodeVIDGroups: Group[] = [];
                                this.groupsList.forEach(c => {
                                    workflowStep.wfStepChipEncodeVIDPrintGroupConfigs.forEach(c1 => {
                                        if (c1.groupId === c.id) {
                                            chipEncodeVIDGroups.push(c);
                                        }
                                    });
                                });
                                this.workflowForm.get('chipPlusVisualIDPrint.chipPlusVisualIDGroup').setValue(chipEncodeVIDGroups);
                            }
                        }
                        if (workflowStep.wfStepPrintGroupConfigs !== undefined) {
                            if (workflowStep.wfStepPrintGroupConfigs.length !== 0 && enableVisualIDPrint) {
                                const printGroups: Group[] = [];
                                this.groupsList.forEach(c => {
                                    workflowStep.wfStepPrintGroupConfigs.forEach(c1 => {
                                        if (c1.groupId === c.id) {
                                            printGroups.push(c);
                                        }
                                    });
                                });
                                this.workflowForm.get('visualIDPrint.visualIDPrintGroup').setValue(printGroups);
                            }
                        }
                        const cardVisualDesigns = this.formBuilder.array([]);
                        if (editMode) {
                            workflow.groups.forEach(wfGroup => {
                                const vt: VisualTemplate = this.smartCardVisualTemplates.filter(e => e.id === wfGroup.visualTemplateID)[0];
                                let vtId;
                                if (vt !== undefined && vt !== null) {
                                    vtId = vt.id;
                                } else {
                                    vtId = '';
                                }
                                const groupIds: string[] = [];
                                groupIds.push(wfGroup.id);
                                cardVisualDesigns.push(
                                    this.formBuilder.group({
                                        cardVisualTemplateGroup: [groupIds, [Validators.required]],
                                        visualTemplateID: [vtId, [Validators.required]]
                                    })
                                );
                            });
                            this.workflowForm.setControl('cardVisualDesigns', cardVisualDesigns);
                        }
                        if (enableChipPersoVIDPrint || enableVisualIDPrint) {
                            this.showCardVisualIdSelection = true;
                            (this.workflowForm.get('cardVisualDesigns') as FormArray).enable();
                        } else {
                            (this.workflowForm.get('cardVisualDesigns') as FormArray).disable();
                        }
                    } else if (workflowStep.name === 'PKI_CERTIFICATES') {
                        if (workflowStep.wfStepCertConfigs !== undefined) {
                            this.workflowForm
                                .get('certs')
                                .get('contentSign')
                                .enable();
                            if (workflowStep.wfStepCertConfigs.length > 0) {
                                this.workflowForm.get('certs').enable();
                                this.workflowForm.get('issueCertsToCard.tokenIssueCerts').setValue(true);
                                (this.workflowForm.get('cardCertificates') as FormArray).enable();
                                this.selectedSteps.push('PKI_CERTIFICATES');
                                this.workflowService.getConfigIdValues('NOTIFY_USER_ABOUT_CERT_EXPIRING_IN_DAYS').subscribe(res => {
                                    this.cardCertificateExpiringDays = res;
                                });
                                this.workflowService.getConfigIdValues('EMAIL_NOTIFICATION_FREQUENCY').subscribe(emailNotFreq => {
                                    this.cardNotificationFrequencies = emailNotFreq;
                                });
                                // let isContentSignRequired = false;
                                let emailNotificationFreq = '';
                                let noOfDaysBeforeCertExpiry;
                                if (workflowStep.wfStepGenericConfigs !== undefined) {
                                    workflowStep.wfStepGenericConfigs.forEach(genericConfig => {
                                        if (genericConfig.name === 'CONTENT_SIGNING_CERTIFICATE_REQUIRED') {
                                            if (genericConfig.value === 'Y') {
                                                // isContentSignRequired = true;
                                            }
                                        } else if (genericConfig.name === 'EMAIL_NOTIFICATION_FREQUENCY') {
                                            emailNotificationFreq = genericConfig.value;
                                        } else if (genericConfig.name === 'NOTIFY_USER_ABOUT_CERT_EXPIRING_IN_DAYS') {
                                            noOfDaysBeforeCertExpiry = genericConfig.value;
                                        }
                                    });
                                }
                                this.workflowForm
                                    .get('certs')
                                    .get('email')
                                    .setValue(emailNotificationFreq);
                                this.workflowForm
                                    .get('certs')
                                    .get('noOfDaysBeforeCertExpiry')
                                    .setValue(noOfDaysBeforeCertExpiry);
                                if (workflowStep.wfStepCertConfigs !== undefined) {
                                    for (let i = 0; i < workflowStep.wfStepCertConfigs.length - 1; i++) {
                                        (this.workflowForm.get('cardCertificates') as FormArray).push(
                                            this.formBuilder.group({
                                                id: [''],
                                                certType: ['', [Validators.required]],
                                                certTypeList: [''],
                                                caServer: ['', [Validators.required]],
                                                certProfile: ['', [Validators.required]],
                                                certProfileList: [''],
                                                canChangeCertType: [true],
                                                hideKeyEscrow: [true],
                                                escrow: [false],
                                                revocation: [false],
                                                algorithm: ['', [Validators.required]],
                                                keySize: ['', [Validators.required]],
                                                keySizeList: [''],
                                                subjectDN: ['', [Validators.required]],
                                                subjectDNDisplayValue: [''],
                                                subjectANDisplayValue: [''],
                                                subjectAN: this.formBuilder.group({
                                                    otherNameCheck: [false],
                                                    rfcNameCheck: [false],
                                                    dnsNameCheck: [false],
                                                    x400AddressCheck: [false],
                                                    directoryNameCheck: [false],
                                                    uriCheck: [false],
                                                    ediPartyCheck: [false],
                                                    ediPartyText: [{ value: '', disabled: true }],
                                                    ipAddressCheck: [false],
                                                    regIdCheck: [false],
                                                    regIdText: [{ value: '', disabled: true }]
                                                })
                                            })
                                        );
                                    }

                                    for (let i = 0; i < workflowStep.wfStepCertConfigs.length; i++) {
                                        const workflowCert = workflowStep.wfStepCertConfigs[i];
                                        let hideKeyEscrowCheck = true;
                                        if (workflowCert.certType.toLowerCase().includes('9d')) {
                                            hideKeyEscrowCheck = false;
                                        }
                                        const certificateTypeList = this.certTypes.filter(e => e.value === workflowCert.certType);
                                        this.certTypes = this.certTypes.filter(e => e.value !== workflowCert.certType);
                                        if (workflowCert !== undefined) {
                                            let cardSubAN = new SubjectAN();
                                            if (workflowCert.subjectAN !== '') {
                                                cardSubAN = this.setSubANInEditCase(workflowCert.subjectAN);
                                            }
                                            let selectedCAServer: CAServerConfig;
                                            this.caServers.forEach(caServer => {
                                                if (caServer.id + '' === workflowCert.caServer) {
                                                    let certProfileList: string[] = [];
                                                    selectedCAServer = caServer;
                                                    if (selectedCAServer) {
                                                        let keySizes = [];
                                                        if (workflowCert.algorithm.toLocaleLowerCase().includes('rsa')) {
                                                            keySizes = this.RSAKeySizes;
                                                        } else if (workflowCert.algorithm.toLocaleLowerCase().includes('ecdsa')) {
                                                            if (workflowCert.certType.includes('9A')) {
                                                                keySizes = this.ECCKeySizesOf9A;
                                                            } else if (workflowCert.certType.includes('9E')) {
                                                                keySizes = this.ECCKeySizesOf9E;
                                                            } else if (workflowCert.certType.includes('9C')) {
                                                                keySizes = this.ECCKeySizesOf9C;
                                                            } else if (workflowCert.certType.includes('9D')) {
                                                                keySizes = this.ECCKeySizesOf9D;
                                                            }
                                                        }
                                                        certProfileList = selectedCAServer.certTemplates;
                                                        //  this.workflowService.getCaTemplates(selectedCAServer.id).subscribe(caTemplates => {
                                                        // certProfileList = caTemplates;
                                                        ((this.workflowForm.get('cardCertificates') as FormArray).at(
                                                            i
                                                        ) as FormGroup).setValue({
                                                            id: workflowCert.id,
                                                            certType: certificateTypeList[0],
                                                            certTypeList: certificateTypeList,
                                                            caServer: selectedCAServer,
                                                            certProfile: workflowCert.certTemplate,
                                                            certProfileList: '',
                                                            canChangeCertType: false,
                                                            hideKeyEscrow: hideKeyEscrowCheck,
                                                            escrow: workflowCert.keyEscrow,
                                                            revocation: workflowCert.disableRevoke,
                                                            algorithm: workflowCert.algorithm,
                                                            keySize: workflowCert.keysize + '',
                                                            keySizeList: keySizes,
                                                            subjectDN: workflowCert.subjectDN,
                                                            subjectDNDisplayValue: workflowCert.subjectDN,
                                                            subjectANDisplayValue: workflowCert.subjectAN,
                                                            subjectAN: {
                                                                otherNameCheck: cardSubAN.OtherNameCheck,
                                                                rfcNameCheck: cardSubAN.rfcNameCheck,
                                                                dnsNameCheck: cardSubAN.dnsNameCheck,
                                                                x400AddressCheck: cardSubAN.x400AddressCheck,
                                                                directoryNameCheck: cardSubAN.directoryNameCheck,
                                                                uriCheck: cardSubAN.uriCheck,
                                                                ediPartyCheck: cardSubAN.ediPartyCheck,
                                                                ediPartyText: cardSubAN.ediPartyText,
                                                                ipAddressCheck: cardSubAN.ipAddressCheck,
                                                                regIdCheck: cardSubAN.regIdCheck,
                                                                regIdText: cardSubAN.regIdText
                                                            }
                                                        });
                                                        ((this.workflowForm.get('cardCertificates') as FormArray).at(i) as FormGroup)
                                                            .get('certProfileList')
                                                            .setValue(certProfileList);
                                                        //  });
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    } else if (workflowStep.name === 'POST_PERSO_AND_ISSUANCE') {
                        this.selectedSteps.push('POST_PERSO_AND_ISSUANCE');
                        this.workflowForm.get('activation').setValue(true);

                        let verifyPinConfig = false;
                        let verifyMocConfig = false;
                        if (workflowStep.wfStepGenericConfigs !== undefined) {
                            workflowStep.wfStepGenericConfigs.forEach(genericConfig => {
                                if (genericConfig.name === 'VERIFY_WITH_PIN' && genericConfig.value === 'Y') {
                                    verifyPinConfig = true;
                                } else if (genericConfig.name === 'VERIFY_WITH_MOC_FINGERPRINT' && genericConfig.value === 'Y') {
                                    verifyMocConfig = true;
                                }
                            });
                        }
                        const verificationPolices = this.formBuilder.group({
                            verifyWithPIN: [verifyPinConfig],
                            verifyWithMOCFingerprint: [verifyMocConfig],
                            verifyWithPinFace: [false],
                            verifyWithPinFp: [false]
                        });
                        this.workflowForm.setControl('verificationPolices', verificationPolices);
                    }
                    if (this.requiredMobileIssuance) {
                        //  this.workflowForm.get('mobileId.isMobileIdReq').enable();
                        if (workflowStep.name === 'MOBILE_ID_HARDWARE_BACKED_KEYSTORE') {
                            this.selectedSteps.push('MOBILE_ID_HARDWARE_BACKED_KEYSTORE');
                            this.workflowForm.get('mobileId.enforceHardwareBackeKeystrore').setValue(true);
                        } else if (workflowStep.name === 'MOBILE_ID_ONBOARDING_CONFIG') {
                            this.selectedSteps.push('MOBILE_ID_ONBOARDING_CONFIG');
                            this.workflowForm.get('mobileId.onBoardingConfig.isSelfServiceReq').setValue(true);
                            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerification').enable();
                            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReq').enable();

                            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.trustedIdentitiesAllowed').disable();
                            let onBoardEnrollUsingExistingIds = false;
                            let faceVerification = false;
                            let approvalOrAdjudicationReq = false;
                            let saveTheCapturedFaceForVisualID = false;
                            const onboardingOptions = this.workflowForm.get(
                                'mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions'
                            );
                            if (workflowStep.wfStepGenericConfigs !== undefined) {
                                workflowStep.wfStepGenericConfigs.forEach(genericConfig => {
                                    if (genericConfig.name === 'ONBOARD_ENROLL_USING_EXISTING_IDENTITY' && genericConfig.value === 'Y') {
                                        onBoardEnrollUsingExistingIds = true;
                                        this.workflowForm
                                            .get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsers')
                                            .setValue(true);
                                    }
                                    if (onBoardEnrollUsingExistingIds) {
                                        if (genericConfig.name === 'SCAN_QR_CODE' && genericConfig.value === 'Y') {
                                            onboardingOptions.get('scanQrCode').setValue(true);
                                        }
                                        if (genericConfig.name === 'TAP_AND_READ_THROUGH_NFC' && genericConfig.value === 'Y') {
                                            onboardingOptions.get('nfc').setValue(true);
                                        }
                                        if (genericConfig.name === 'CAPTURE_FRONT_OF_DOCUMENT' && genericConfig.value === 'Y') {
                                            onboardingOptions.get('frontDoc').setValue(true);
                                        }
                                        if (genericConfig.name === 'CAPTURE_BACK_OF_DOCUMENT' && genericConfig.value === 'Y') {
                                            onboardingOptions.get('backDoc').setValue(true);
                                        }
                                    }
                                    if (genericConfig.name === 'FACE_VERIFY_DURING_ONBOARDING_REQUIRED' && genericConfig.value === 'Y') {
                                        faceVerification = true;
                                        this.workflowForm
                                            .get('mobileId.onBoardingConfig.selfServiceOptions.faceVerification')
                                            .setValue(true);
                                        this.workflowForm
                                            .get(
                                                'mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions.saveTheCapturedFaceForVisualID'
                                            )
                                            .enable();
                                    }
                                    if (faceVerification) {
                                        if (genericConfig.name === 'SAVE_CAPTURED_FACE_FOR_VISUAL_ID' && genericConfig.value === 'Y') {
                                            saveTheCapturedFaceForVisualID = true;
                                            this.workflowForm
                                                .get(
                                                    'mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions.saveTheCapturedFaceForVisualID'
                                                )
                                                .setValue(true);
                                            this.workflowForm
                                                .get('mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions.facialCropSize')
                                                .enable();
                                        }
                                    }
                                    if (saveTheCapturedFaceForVisualID) {
                                        if (genericConfig.name === 'CROP_SIZE') {
                                            this.workflowForm
                                                .get('mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions.facialCropSize')
                                                .setValue(genericConfig.value);
                                        }
                                    }
                                    if (genericConfig.name === 'APPROVAL_OR_ADJUDICATION_REQUIRED' && genericConfig.value === 'Y') {
                                        approvalOrAdjudicationReq = true;
                                        this.workflowForm
                                            .get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReq')
                                            .setValue(true);
                                        this.workflowForm
                                            .get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReqOptions')
                                            .enable();
                                    }
                                });
                            }
                            if (onBoardEnrollUsingExistingIds) {
                                this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.trustedIdentitiesAllowed').enable();
                                const trustedExistingIdentities: OrganizationIdentity[] = [];
                                this.workflowService.getOrgTrustedExistingIdentityModels().subscribe(idModels => {
                                    this.trustedExistingIdentityModels = idModels;
                                    if (workflowStep.wfMobileIDStepTrustedIdentities !== undefined) {
                                        this.trustedExistingIdentityModels.forEach(identity => {
                                            workflowStep.wfMobileIDStepTrustedIdentities.forEach(wfidentity => {
                                                if (wfidentity.id === identity.id) {
                                                    trustedExistingIdentities.push(identity);
                                                }
                                            });
                                        });
                                    }
                                    this.workflowForm
                                        .get('mobileId.onBoardingConfig.selfServiceOptions.trustedIdentitiesAllowed')
                                        .setValue(trustedExistingIdentities);
                                });
                            }
                            if (approvalOrAdjudicationReq) {
                                const adjGroups: Group[] = [];
                                this.groupsList.forEach(group => {
                                    workflowStep.wfStepAdjudicationGroupConfigs.forEach(adjGroup => {
                                        if (group.id === adjGroup.id) {
                                            adjGroups.push(group);
                                        }
                                    });
                                });
                                this.workflowForm
                                    .get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReqOptions')
                                    .get('adjudicationGroups')
                                    .setValue(adjGroups);
                            }
                        } else if (workflowStep.name === 'MOBILE_ID_IDENTITY_ISSUANCE') {
                            this.workflowForm.get('mobileId.digitalIdentityIssuance.enableMobileID').setValue(true);
                            this.workflowForm.get('mobileId.digitalIdentityIssuance.bindExpirationToMobileID').enable();
                            if (workflowStep.wfStepGenericConfigs !== undefined) {
                                workflowStep.wfStepGenericConfigs.forEach(genericConfig => {
                                    if (
                                        genericConfig.name === 'BIND_EXPIRATION_WITH_EXISTING_IDENTITY_USER' &&
                                        genericConfig.value === 'Y'
                                    ) {
                                        this.workflowForm.get('mobileId.digitalIdentityIssuance.bindExpirationToMobileID').setValue(true);
                                    }
                                });
                            }
                            this.selectedSteps.push('MOBILE_ID_IDENTITY_ISSUANCE');
                            this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity').enable();
                            (this.workflowForm
                                .get('mobileId.digitalIdentityIssuance.additionalIdentity')
                                .get('identityCredentialsArray') as FormArray).controls.forEach((control: FormGroup) => {
                                (control.get('addCertificatesArray') as FormArray).disable();
                                (control.get('notifyUsers') as FormArray).disable();
                                (control.get('emailFrequency') as FormArray).disable();
                            });
                            if (workflowStep.wfMobileIDStepIdentityConfigs !== undefined) {
                                if (workflowStep.wfMobileIDStepIdentityConfigs.length > 0) {
                                    if (workflowStep.wfMobileIDStepIdentityConfigs.length > 1) {
                                        this.deleteIdentity = true;
                                    }
                                    const identityCredentialsArray = this.formBuilder.array([]);
                                    workflowStep.wfMobileIDStepIdentityConfigs.forEach(mobileIDStepIdentityConfig => {
                                        const identityFormGroup = this.formBuilder.group({});
                                        identityFormGroup.addControl(
                                            'id',
                                            new FormControl(mobileIDStepIdentityConfig.wfMobileIDStepIdentityConfigId)
                                        );
                                        identityFormGroup.addControl(
                                            'friendlyName',
                                            new FormControl(mobileIDStepIdentityConfig.friendlyName, Validators.required)
                                        );
                                        identityFormGroup.addControl('pushVerify', new FormControl(mobileIDStepIdentityConfig.pushVerify));
                                        identityFormGroup.addControl('softOtp', new FormControl(mobileIDStepIdentityConfig.softOTP));
                                        identityFormGroup.addControl('fido2', new FormControl(mobileIDStepIdentityConfig.fido2));
                                        identityFormGroup.addControl('consent', new FormControl(mobileIDStepIdentityConfig.consent));
                                        identityFormGroup.addControl(
                                            'isContentSigning',
                                            new FormControl(mobileIDStepIdentityConfig.isContentSigning)
                                        );
                                        const mobileVisualDesigns = this.formBuilder.array([]);
                                        if (mobileIDStepIdentityConfig.wfMobileIdStepVisualIDGroupConfigs !== undefined) {
                                            if (mobileIDStepIdentityConfig.wfMobileIdStepVisualIDGroupConfigs.length !== 0 && editMode) {
                                                mobileIDStepIdentityConfig.wfMobileIdStepVisualIDGroupConfigs.forEach(mobileIDVisualID => {
                                                    const vt: VisualTemplate = this.mobileVisualTemplates.filter(
                                                        e => e.id === mobileIDVisualID.visualTemplateId
                                                    )[0];
                                                    let vtId;
                                                    if (vt !== undefined && vt !== null) {
                                                        vtId = vt.id;
                                                    } else {
                                                        vtId = '';
                                                    }
                                                    const groupIds: string[] = [];
                                                    groupIds.push(mobileIDVisualID.referenceGroupId);
                                                    mobileVisualDesigns.push(
                                                        this.formBuilder.group({
                                                            mobileVisualTemplateGroup: [groupIds, [Validators.required]],
                                                            visualTemplateID: [vtId, [Validators.required]]
                                                        })
                                                    );
                                                });
                                                identityFormGroup.addControl('mobileVisualDesigns', mobileVisualDesigns);
                                            } else {
                                                identityFormGroup.addControl('mobileVisualDesigns', mobileVisualDesigns);
                                            }
                                        } else {
                                            identityFormGroup.addControl('mobileVisualDesigns', mobileVisualDesigns);
                                        }
                                        const addCertificatesArray = this.formBuilder.array([]);
                                        if (mobileIDStepIdentityConfig.wfMobileIdStepCertificates.length !== 0) {
                                            identityFormGroup.addControl('issueCertificates', new FormControl(true));
                                            this.workflowService
                                                .getConfigIdValues('NOTIFY_USER_ABOUT_CERT_EXPIRING_IN_DAYS')
                                                .subscribe(response => {
                                                    this.mobileCertificateExpiringDays = response;
                                                    this.workflowService
                                                        .getConfigIdValues('EMAIL_NOTIFICATION_FREQUENCY')
                                                        .subscribe(emailNotificationFreqs => {
                                                            this.mobileNotificationFrequencies = emailNotificationFreqs;
                                                        });
                                                });

                                            mobileIDStepIdentityConfig.wfMobileIdStepCertificates.forEach(
                                                (mobileIdStepCert: WFMobileIDStepCertificates) => {
                                                    const certificateTypeList = this.mobileCertTypes.filter(
                                                        e => e.value === mobileIdStepCert.certType
                                                    );
                                                    //   this.mobileCertTypes = this.mobileCertTypes.filter(e => e.value !== mobileIdStepCert.certType);
                                                    let selectedCAServer: CAServerConfig;
                                                    this.caServers.forEach(caServer => {
                                                        if (caServer.id + '' === mobileIdStepCert.caServer) {
                                                            let certProfileList: string[] = [];
                                                            selectedCAServer = caServer;
                                                            if (selectedCAServer) {
                                                                /** if (this.caServerObj.length !== 0) {
                                                          this.caServerObj.forEach(element => {
                                                              if (element.caProviderName === selectedCAServer.caProviderName &&
                                                                 element.caServerName === selectedCAServer.caName) {
                                                                  console.log(element);
                                                              }
                                                          });
                                                      } */
                                                                this.workflowService
                                                                    .getCaTemplates(selectedCAServer.id, 'mobile')
                                                                    .subscribe(caTemplates => {
                                                                        let hideKeyEscrow = true;
                                                                        if (
                                                                            mobileIdStepCert.certTemplate
                                                                                .toLowerCase()
                                                                                .includes('keymanagement')
                                                                        ) {
                                                                            hideKeyEscrow = false;
                                                                        } else {
                                                                            hideKeyEscrow = true;
                                                                        }
                                                                        let keySizeList = [];
                                                                        if (
                                                                            mobileIdStepCert.algorithm.toLocaleLowerCase().includes('rsa')
                                                                        ) {
                                                                            keySizeList = this.RSAKeySizes;
                                                                        } else if (
                                                                            mobileIdStepCert.algorithm.toLocaleLowerCase().includes('ecdsa')
                                                                        ) {
                                                                            keySizeList = this.ECDSAKeySizes;
                                                                        }
                                                                        certProfileList = caTemplates;
                                                                        addCertificatesArray.push(
                                                                            this.formBuilder.group({
                                                                                id: [mobileIdStepCert.wfMobileIdStepCertId],
                                                                                certType: [certificateTypeList[0], Validators.required],
                                                                                certTypeList: [certificateTypeList],
                                                                                caServer: [selectedCAServer, Validators.required],
                                                                                certProfile: [
                                                                                    mobileIdStepCert.certTemplate,
                                                                                    Validators.required
                                                                                ],
                                                                                certProfileList: [certProfileList],
                                                                                hideKeyEscrow: [hideKeyEscrow],
                                                                                escrow: [mobileIdStepCert.keyEscrow],
                                                                                revocation: [mobileIdStepCert.disableRevoke],
                                                                                algorithm: [
                                                                                    mobileIdStepCert.algorithm,
                                                                                    Validators.required
                                                                                ],
                                                                                keySize: [
                                                                                    mobileIdStepCert.keysize + '',
                                                                                    Validators.required
                                                                                ],
                                                                                keySizeList: [keySizeList],
                                                                                subDn: [mobileIdStepCert.subjectDN, Validators.required],
                                                                                subDnDisplayValue: [mobileIdStepCert.subjectDN],
                                                                                subAN: ['']
                                                                            })
                                                                        );
                                                                    });
                                                            }
                                                        }
                                                    });
                                                }
                                            );
                                            const wfMobileIdStepCertConfig = mobileIDStepIdentityConfig.wfMobileIDStepCertConfig;
                                            identityFormGroup.addControl('addCertificatesArray', addCertificatesArray);
                                            identityFormGroup.addControl(
                                                'notifyUsers',
                                                new FormControl(wfMobileIdStepCertConfig.noOfDaysBeforeCertExpiry.value)
                                            );
                                            identityFormGroup.addControl(
                                                'emailFrequency',
                                                new FormControl(wfMobileIdStepCertConfig.emailNotificationFreq.value)
                                            );
                                        } else {
                                            identityFormGroup.addControl('issueCertificates', new FormControl(false));
                                            addCertificatesArray.push(
                                                this.formBuilder.group({
                                                    id: [''],
                                                    certType: ['', Validators.required],
                                                    certTypeList: [''],
                                                    caServer: ['', Validators.required],
                                                    certProfile: ['', Validators.required],
                                                    certProfileList: [''],
                                                    hideKeyEscrow: [true],
                                                    escrow: [false],
                                                    revocation: [false],
                                                    algorithm: ['', Validators.required],
                                                    keySize: ['', Validators.required],
                                                    keySizeList: [''],
                                                    subDn: ['', Validators.required],
                                                    subDnDisplayValue: [''],
                                                    subAN: ['']
                                                })
                                            );
                                            identityFormGroup.addControl('addCertificatesArray', addCertificatesArray);
                                            identityFormGroup.get('addCertificatesArray').disable();
                                            identityFormGroup.addControl('notifyUsers', new FormControl('', Validators.required));
                                            identityFormGroup.get('notifyUsers').disable();
                                            identityFormGroup.addControl('emailFrequency', new FormControl('', Validators.required));
                                            identityFormGroup.get('emailFrequency').disable();
                                        }
                                        identityCredentialsArray.push(identityFormGroup);
                                    });
                                    (this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity') as FormGroup).setControl(
                                        'identityCredentialsArray',
                                        identityCredentialsArray
                                    );
                                }
                            }
                        }
                    }
                });
                this.isLoadingResults = false;
                const pkiCertificates = workflow.workflowSteps.find(e => e.name === 'PKI_CERTIFICATES');
                if (pkiCertificates !== undefined || pkiCertificates !== null) {
                    this.isTokenCertsEnabled = true;
                    if (pkiCertificates.wfStepCertConfigs.length === 4) {
                        this.hideAddCardCert = true;
                    } else {
                        setTimeout(() => {
                            this.hideAddCardCertButton(pkiCertificates.wfStepCertConfigs.length);
                        }, 100);
                    }
                }
            });
        } else {
            this.isLoadingResults = false;
            this.editWorkflow = false;
        }
    }
    setSubANInEditCase(subjectAN: string) {
        const subAN = new SubjectAN();
        if (subjectAN === null) {
            return subAN;
        }
        if (subjectAN.length !== 0) {
            const listOfFields = subjectAN.split(',');
            listOfFields.forEach(field => {
                switch (field) {
                    case SubjectANEnum.OTHER_NAME:
                        subAN.OtherNameCheck = true;
                        break;
                    case SubjectANEnum.RFC822NAME:
                        subAN.rfcNameCheck = true;
                        break;
                    case SubjectANEnum.DNS_NAME:
                        subAN.dnsNameCheck = true;
                        break;
                    case SubjectANEnum.X400ADDRESS:
                        subAN.x400AddressCheck = true;
                        break;
                    case SubjectANEnum.DIRECTORY_NAME:
                        subAN.directoryNameCheck = true;
                        break;
                    case SubjectANEnum.URI:
                        subAN.uriCheck = true;
                        break;
                    case SubjectANEnum.IP_ADDRESS:
                        subAN.ipAddressCheck = true;
                        break;
                }
                if (field.includes(SubjectANEnum.EDI_PARTY_NAME)) {
                    subAN.ediPartyCheck = true;
                    const textValue = field.split('=');
                    if (textValue[1] === null || textValue[1] === undefined) {
                        subAN.ediPartyText = '';
                    } else {
                        subAN.ediPartyText = textValue[1];
                    }
                }
                if (field.includes(SubjectANEnum.REG_ID)) {
                    subAN.regIdCheck = true;
                    const textValue = field.split('=');
                    if (textValue[1] === null || textValue[1] === undefined) {
                        subAN.regIdText = '';
                    } else {
                        subAN.regIdText = textValue[1];
                    }
                }
            });
        }
        return subAN;
    }
    /** =================================================== Workflow Step Selection Change Handling Logic ============================================================*/
    get generalForm() {
        return this.workflowForm.get('general') as FormGroup;
    }
    get regFormFields() {
        return this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray;
    }
    get cardVisualDesignsArray() {
        return this.workflowForm.get('cardVisualDesigns') as FormArray;
    }
    get cardCertificates() {
        return this.workflowForm.get('cardCertificates') as FormArray;
    }
    cardCertificatesAt(i) {
        return (this.workflowForm.get('cardCertificates') as FormArray).at(i) as FormGroup; // .get('subjectAN') as FormGroup;
    }
    cardVisualDesignsArrayAt(i) {
        return (this.workflowForm.get('cardVisualDesigns') as FormArray).at(i) as FormGroup;
    }
    get getIdentityCredentials() {
        return this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray') as FormArray;
    }
    getIdentityCredentialsAt(i) {
        return (this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray') as FormArray).at(
            i
        ) as FormGroup;
    }
    mobileVisualDesignsArray(index) {
        return (this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).controls[index].get('mobileVisualDesigns') as FormArray;
    }
    mobileVisualDesignsArrayAt(i, k) {
        return ((this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).controls[i].get('mobileVisualDesigns') as FormArray).at(k) as FormGroup;
    }
    mobileCertificates(index) {
        return (this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).controls[index].get('addCertificatesArray') as FormArray;
    }
    mobileCertificatesAt(i, c) {
        return ((this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).controls[i].get('addCertificatesArray') as FormArray).at(c) as FormGroup;
    }
    mobileIdCertCheck(index) {
        return (this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).controls[index].get('issueCertificates') as FormControl;
    }
    isSubDNDisable(index) {
        return (this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).controls[index].get('issueCertificates').value;
    }
    enableAllowedDevices() {
        if (this.workflowForm.get('general.maxAllowedDevices.isAllowedDevices').value) {
            this.isAllowedDevicesSelected = true;
            this.workflowForm.get('general.maxAllowedDevices.allowedDevices').enable();
        } else {
            this.isAllowedDevicesSelected = false;
            this.workflowForm.get('general.maxAllowedDevices.allowedDevices').reset();
            this.workflowForm.get('general.maxAllowedDevices.allowedDevices').disable();
        }
    }
    enableBiometrics(event: MatCheckboxChange) {
        if (event.checked) {
            this.requiredEnrollment = true;
            this.workflowForm.get('biometricEnrollment').enable();
            this.workflowForm.get('biometricEnrollment.idProofingChecked').setValue(true);
            this.selectedSteps.push('ID_PROOFING');
            this.workflowForm.get('biometricEnrollment.enrollmentFormChecked').setValue(true);
            this.loadRegOrgIdentityFields();
            this.workflowForm.get('biometricEnrollment.faceChecked').setValue(true);
            this.loadFaceCropSizes();
            this.workflowForm.get('biometricEnrollment.irisChecked').setValue(true);
            this.workflowForm
                .get('biometricEnrollment.iris')
                .get('irisMode')
                .setValue('Dual');
            this.selectedSteps.push('IRIS_CAPTURE');
            this.workflowForm.get('biometricEnrollment.fingerprintChecked').setValue(true);
            this.loadFpThresholds();
            this.workflowForm.get('biometricEnrollment.signatureChecked').setValue(true);
            this.selectedSteps.push('SIGNATURE_CAPTURE');
            // disable approval step, as it s not supporting now
            this.workflowForm.get('biometricEnrollment.approvalChecked').enable();
            this.workflowForm.get('biometricEnrollment.approval').enable();
            this.workflowForm.get('biometricEnrollment.approvalChecked').setValue(true);
            this.selectedSteps.push('APPROVAL_BEFORE_ISSUANCE');
        } else {
            this.requiredEnrollment = false;
            // this.workflowForm.get('biometricEnrollment').reset();
            this.workflowForm.get('biometricEnrollment').disable();
            this.workflowForm.get('biometricEnrollment.idProofingChecked').setValue(false);
            if (this.selectedSteps.indexOf('ID_PROOFING') !== -1) {
                this.selectedSteps.splice(this.selectedSteps.indexOf('ID_PROOFING'), 1);
            }
            this.workflowForm.get('biometricEnrollment.enrollmentFormChecked').setValue(false);
            this.showFormFields = false;
            if (this.selectedSteps.indexOf('REGISTRATION') !== -1) {
                this.selectedSteps.splice(this.selectedSteps.indexOf('REGISTRATION'), 1);
            }
            this.workflowForm.get('biometricEnrollment.face').reset();
            this.workflowForm.get('biometricEnrollment.faceChecked').setValue(false);
            if (this.selectedSteps.indexOf('FACE_CAPTURE') !== -1) {
                this.selectedSteps.splice(this.selectedSteps.indexOf('FACE_CAPTURE'), 1);
            }
            this.workflowForm.get('biometricEnrollment.irisChecked').setValue(false);
            if (this.selectedSteps.indexOf('IRIS_CAPTURE') !== -1) {
                this.selectedSteps.splice(this.selectedSteps.indexOf('IRIS_CAPTURE'), 1);
            }
            this.workflowForm
                .get('biometricEnrollment.iris')
                .get('irisMode')
                .setValue('');
            this.workflowForm.get('biometricEnrollment.fingerprintChecked').setValue(false);
            if (this.selectedSteps.indexOf('FINGERPRINT_CAPTURE') !== -1) {
                this.selectedSteps.splice(this.selectedSteps.indexOf('FINGERPRINT_CAPTURE'), 1);
            }
            this.workflowForm.get('biometricEnrollment.fingerprint').reset();
            this.workflowForm.get('biometricEnrollment.signatureChecked').setValue(false);
            if (this.selectedSteps.indexOf('SIGNATURE_CAPTURE') !== -1) {
                this.selectedSteps.splice(this.selectedSteps.indexOf('SIGNATURE_CAPTURE'), 1);
            }
            this.workflowForm.get('biometricEnrollment.approval').reset();
            this.workflowForm.get('biometricEnrollment.approvalChecked').setValue(false);
            if (this.selectedSteps.indexOf('APPROVAL_BEFORE_ISSUANCE') !== -1) {
                this.selectedSteps.splice(this.selectedSteps.indexOf('APPROVAL_BEFORE_ISSUANCE'), 1);
            }
        }
    }
    onIdProofingChange(idProofing: MatCheckboxChange) {
        if (idProofing.checked) {
            this.selectedSteps.push('ID_PROOFING');
        } else {
            this.selectedSteps.splice(this.selectedSteps.indexOf('ID_PROOFING'), 1);
        }
    }
    onEnrollmentFormChange(enrollmentForm: MatCheckboxChange) {
        if (enrollmentForm.checked) {
            this.loadRegOrgIdentityFields();
        } else {
            this.showFormFields = false;
            (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).disable();
            this.selectedSteps.splice(this.selectedSteps.indexOf('REGISTRATION'), 1);
        }
    }
    loadRegOrgIdentityFields() {
        this.showFormFields = true;
        (this.workflowForm.get('biometricEnrollment.enrollmentForm') as FormArray).enable();
        this.selectedSteps.push('REGISTRATION');
    }
    onFaceChange(face: MatCheckboxChange) {
        if (face.checked) {
            this.loadFaceCropSizes();
        } else {
            this.workflowForm.get('biometricEnrollment.face').reset();
            this.workflowForm.get('biometricEnrollment.face').disable();
            this.selectedSteps.splice(this.selectedSteps.indexOf('FACE_CAPTURE'), 1);
        }
    }
    loadFaceCropSizes() {
        this.workflowForm.get('biometricEnrollment.face').enable();
        this.selectedSteps.push('FACE_CAPTURE');
    }
    onIrisChange(iris: MatCheckboxChange) {
        if (iris.checked) {
            this.workflowForm.get('biometricEnrollment.iris').enable();
            this.workflowForm
                .get('biometricEnrollment.iris')
                .get('irisMode')
                .setValue('Dual');
            this.selectedSteps.push('IRIS_CAPTURE');
        } else {
            this.workflowForm
                .get('biometricEnrollment.iris')
                .get('irisMode')
                .setValue('');
            this.workflowForm.get('biometricEnrollment.iris').disable();
            this.selectedSteps.splice(this.selectedSteps.indexOf('IRIS_CAPTURE'), 1);
        }
    }
    onFingerPrintChange(fingerprint: MatCheckboxChange) {
        if (fingerprint.checked) {
            this.loadFpThresholds();
        } else {
            this.workflowForm.get('biometricEnrollment.fingerprint').reset();
            this.workflowForm.get('biometricEnrollment.fingerprint').disable();
            this.selectedSteps.splice(this.selectedSteps.indexOf('FINGERPRINT_CAPTURE'), 1);
            if (this.selectedSteps.indexOf('FINGERPRINT_ROLL_CAPTURE') !== -1) {
                this.selectedSteps.splice(this.selectedSteps.indexOf('FINGERPRINT_ROLL_CAPTURE'), 1);
            }
        }
    }
    onChangeOfMinFingersRequiredSelection(event: MatSelectChange) {
        this.workflowService.getFingerByFingerCount(event.value).subscribe((result: Finger[]) => {
            this.fingersByCount = result;
        });
    }
    onEnableRollFinger(event: MatCheckboxChange) {
        if (event.checked) {
            this.selectedSteps.push('FINGERPRINT_ROLL_CAPTURE');
        } else {
            this.selectedSteps.splice(this.selectedSteps.indexOf('FINGERPRINT_ROLL_CAPTURE'), 1);
        }
    }

    loadFpThresholds() {
        this.workflowForm.get('biometricEnrollment.fingerprint').enable();
        this.selectedSteps.push('FINGERPRINT_CAPTURE');
        this.workflowService.getConfigIdValues('FINGERPRINT_MIN_THRESHOLD').subscribe(configIdValues => {
            this.fingerprintThresholds = configIdValues;
        });
    }
    onSignatureChange(signature: MatCheckboxChange) {
        if (signature.checked) {
            this.selectedSteps.push('SIGNATURE_CAPTURE');
        } else {
            this.selectedSteps.splice(this.selectedSteps.indexOf('SIGNATURE_CAPTURE'), 1);
        }
    }
    onApprovalChange(approval: MatCheckboxChange) {
        if (approval.checked) {
            this.workflowForm.get('biometricEnrollment.approval').enable();
            this.selectedSteps.push('APPROVAL_BEFORE_ISSUANCE');
        } else {
            this.workflowForm.get('biometricEnrollment.approval').reset();
            this.workflowForm.get('biometricEnrollment.approval').disable();
            this.selectedSteps.splice(this.selectedSteps.indexOf('APPROVAL_BEFORE_ISSUANCE'), 1);
        }
    }
    onDevProfileChange(event: MatOptionSelectionChange) {
        if (event.isUserInput) {
            const deviceProfile: DeviceProfile = event.source.value;
            const changedOption: DeviceProfileConfig = deviceProfile.deviceProfileConfig;
            if (event.source.selected) {
                if (changedOption.isMobileId) {
                    if (!this.workflowForm.get('mobileId.isMobileIdReq').value) {
                        this.workflowForm.get('mobileId.isMobileIdReq').setValue(true);
                        this.onMobileIdReq();
                    }
                } else if (!changedOption.isPlasticId) {
                    this.workflowForm.get('tokenIssuanceChecked').enable();
                    this.isPKICertsEnabled = true;
                    this.isPrintEnabled = true;
                    if (this.workflowForm.get('tokenIssuanceChecked').value) {
                        if (this.workflowForm.get('issueCertsToCard.tokenIssueCerts').disabled) {
                            this.workflowForm.get('issueCertsToCard.tokenIssueCerts').enable();
                            // this.workflowForm.get('certs').enable();
                        }
                        this.workflowForm
                            .get('certs')
                            .get('contentSign')
                            .enable();
                        if (this.workflowForm.get('activation').disabled) {
                            this.workflowForm.get('activation').enable();
                        }
                        this.workflowForm
                            .get('chipEncode')
                            .get('isChipEncode')
                            .enable();
                        this.workflowForm
                            .get('chipPlusVisualIDPrint')
                            .get('isChipPlusVisualIDPrint')
                            .enable();
                    }
                    if (deviceProfile.deviceProfileConfig.isAppletLoadingRequired) {
                        this.showAppletSelection = true;
                        //check if token/ card issuance step enabled or not, if enabled then only enable this form group
                        if (this.workflowForm.get('tokenIssuanceChecked').value) {
                            this.workflowForm.get('appletSelectionFormGroup').enable();
                            // this.workflowForm.get('fidoe2AppletDetails').reset();
                            if (this.isFidoAppletSelected) {
                                // this.workflowForm.get('fidoe2AppletDetails').reset();
                            }
                        }
                    }
                    // enable plastic card option
                    if (this.workflowForm.get('tokenIssuanceChecked').value) {
                        this.workflowForm
                            .get('visualIDPrint')
                            .get('isVisualIDPrint')
                            .enable();
                    }
                } else {
                    this.isPrintEnabled = true;
                    this.workflowForm.get('tokenIssuanceChecked').enable();
                    // enable plastic card option
                    if (this.workflowForm.get('tokenIssuanceChecked').value) {
                        this.workflowForm
                            .get('visualIDPrint')
                            .get('isVisualIDPrint')
                            .enable();
                    }
                }
            } else {
                const selectedDevProfs = this.workflowForm
                    .get('general')
                    .get('selectedDeviceProfiles')
                    .value.filter(e => e.name !== event.source.value.name);

                let isSmartCardDevProfSelected = false;
                let showAppletSelection = false;
                let isPrintDeviceProfileSelected = false;
                let isMobileDevProfSelected = false;
                selectedDevProfs.forEach((selectedDeviceProfile: DeviceProfile) => {
                    if (!selectedDeviceProfile.deviceProfileConfig.isMobileId && !selectedDeviceProfile.deviceProfileConfig.isPlasticId) {
                        isSmartCardDevProfSelected = true;
                    }
                    if (selectedDeviceProfile.deviceProfileConfig.isPlasticId) {
                        isPrintDeviceProfileSelected = true;
                    }
                    if (selectedDeviceProfile.deviceProfileConfig.isAppletLoadingRequired) {
                        showAppletSelection = true;
                    }
                    if (selectedDeviceProfile.deviceProfileConfig.isMobileId) {
                        isMobileDevProfSelected = true;
                    }
                });
                if (changedOption.isMobileId) {
                    if (!isMobileDevProfSelected) {
                        this.workflowForm.get('mobileId').disable();
                        this.workflowForm.get('mobileId').reset();
                        this.requiredMobileIssuance = false;
                    }
                } else if (!changedOption.isPlasticId) {
                    if (this.isPKICertsEnabled) {
                        if (!showAppletSelection) {
                            this.showAppletSelection = false;
                            // disbale forms
                            this.workflowForm.get('appletSelectionFormGroup').reset();
                            this.workflowForm.get('appletSelectionFormGroup').disable();
                            this.workflowForm.get('fidoe2AppletDetails').reset();
                            this.workflowForm.get('fidoe2AppletDetails').disable();
                        }
                        if (!isSmartCardDevProfSelected) {
                            this.isPKICertsEnabled = false;
                            this.workflowForm.get('issueCertsToCard.tokenIssueCerts').reset();
                            this.workflowForm.get('issueCertsToCard.tokenIssueCerts').disable();
                            (this.workflowForm.get('cardCertificates') as FormArray).disable();
                            this.workflowForm.get('certs').reset();
                            this.workflowForm.get('certs').disable();
                            this.workflowForm.get('activation').reset();
                            this.workflowForm.get('activation').disable();
                            this.workflowForm
                                .get('certs')
                                .get('contentSign')
                                .disable();
                            this.workflowForm.get('verificationPolices.verifyWithPIN').reset();
                            this.workflowForm.get('chipEncode').reset();
                            this.workflowForm.get('chipEncode').disable();
                            this.workflowForm.get('chipPlusVisualIDPrint').reset();
                            this.workflowForm.get('chipPlusVisualIDPrint').disable();
                        }
                    }
                }
                // disable plastic card option
                if (this.isPrintEnabled) {
                    if (!isPrintDeviceProfileSelected && !isSmartCardDevProfSelected) {
                        this.isPrintEnabled = false;
                        this.workflowForm.get('visualIDPrint').reset();
                        this.workflowForm.get('visualIDPrint').disable();
                        this.workflowForm.get('tokenIssuanceChecked').setValue(false);
                        this.workflowForm.get('tokenIssuanceChecked').disable();
                        (this.workflowForm.get('cardVisualDesigns') as FormArray).reset();
                        (this.workflowForm.get('cardVisualDesigns') as FormArray).disable();
                    }
                }
            }
        }
    }

    addGroups(event: MatOptionSelectionChange) {
        if (event.isUserInput) {
            let scVisualTemplateId;
            let mobileVisualTemplateId;
            const groups: Array<string> = [];
            if (this.visualTemplates.length === 0) {
                this.workflowService
                    .getVisualTemplates(this.workflowForm.get('general.identityModel').value.identityTypeName)
                    .subscribe(visualTemplates => {
                        this.visualTemplates = visualTemplates;
                        visualTemplates.forEach(vt => {
                            vt.deviceTypes.forEach(device => {
                                if (device.name.includes(DeviceTypeEnum.SMART_CARD)) {
                                    this.smartCardVisualTemplates.push(vt);
                                } else if (device.name.includes(DeviceTypeEnum.MOBILE)) {
                                    this.mobileVisualTemplates.push(vt);
                                }
                            });
                        });
                    });
            } else {
                if (this.smartCardVisualTemplates.length === 0) {
                    scVisualTemplateId = '';
                } else {
                    scVisualTemplateId = this.smartCardVisualTemplates[0].id;
                }
                if (this.mobileVisualTemplates.length === 0) {
                    mobileVisualTemplateId = '';
                } else {
                    mobileVisualTemplateId = this.mobileVisualTemplates[0].id;
                }
            }
            if (event.source.selected) {
                groups.push(event.source.value.id);
                (this.workflowForm.get('cardVisualDesigns') as FormArray).push(
                    this.formBuilder.group({
                        cardVisualTemplateGroup: [groups, [Validators.required]],
                        visualTemplateID: [scVisualTemplateId, [Validators.required]]
                    })
                );
            } else {
                (this.workflowForm.get('cardVisualDesigns') as FormArray).controls.forEach(e => {
                    if (e.get('cardVisualTemplateGroup').value.length > 1) {
                        e.get('cardVisualTemplateGroup').value.forEach(el => {
                            if (el === event.source.value.id) {
                                const index = (this.workflowForm.get('cardVisualDesigns') as FormArray).controls.indexOf(e);
                                if (index !== -1) {
                                    (this.workflowForm.get('cardVisualDesigns') as FormArray)
                                        .at(index)
                                        .get('cardVisualTemplateGroup')
                                        .setValue(e.get('cardVisualTemplateGroup').value.filter(c => c !== event.source.value.id));
                                }
                            }
                        });
                    } else if (e.get('cardVisualTemplateGroup').value.length === 1) {
                        if (e.get('cardVisualTemplateGroup').value[0] === event.source.value.id) {
                            const index = (this.workflowForm.get('cardVisualDesigns') as FormArray).controls.indexOf(e);
                            (this.workflowForm.get('cardVisualDesigns') as FormArray).removeAt(index);
                        }
                    }
                });
            }
            if (this.showCardVisualIdSelection) {
                (this.workflowForm.get('cardVisualDesigns') as FormArray).enable();
            } else {
                (this.workflowForm.get('cardVisualDesigns') as FormArray).disable();
            }
            (this.workflowForm
                .get('mobileId.digitalIdentityIssuance.additionalIdentity')
                .get('identityCredentialsArray') as FormArray).controls.forEach((control: FormGroup) => {
                if (event.source.selected) {
                    (control.get('mobileVisualDesigns') as FormArray).push(
                        this.formBuilder.group({
                            mobileVisualTemplateGroup: [groups, [Validators.required]],
                            visualTemplateID: [mobileVisualTemplateId, [Validators.required]]
                        })
                    );
                } else {
                    (control.get('mobileVisualDesigns') as FormArray).controls.forEach(e => {
                        if (e.get('mobileVisualTemplateGroup').value.length > 1) {
                            e.get('mobileVisualTemplateGroup').value.forEach(el => {
                                if (el === event.source.value.id) {
                                    const index = (control.get('mobileVisualDesigns') as FormArray).controls.indexOf(e);
                                    if (index !== -1) {
                                        (control.get('mobileVisualDesigns') as FormArray)
                                            .at(index)
                                            .get('mobileVisualTemplateGroup')
                                            .setValue(e.get('mobileVisualTemplateGroup').value.filter(c => c !== event.source.value.id));
                                    }
                                }
                            });
                        } else if (e.get('mobileVisualTemplateGroup').value.length === 1) {
                            if (e.get('mobileVisualTemplateGroup').value[0] === event.source.value.id) {
                                const index = (control.get('mobileVisualDesigns') as FormArray).controls.indexOf(e);
                                (control.get('mobileVisualDesigns') as FormArray).removeAt(index);
                            }
                        }
                    });
                }
                if (this.workflowForm.get('mobileId.digitalIdentityIssuance.enableMobileID').value) {
                    (control.get('mobileVisualDesigns') as FormArray).enable();
                } else {
                    (control.get('mobileVisualDesigns') as FormArray).disable();
                }
            });
        }
    }
    onActivationCheckChange(event: MatCheckboxChange) {
        if (event.checked) {
            this.workflowForm
                .get('certs')
                .get('contentSign')
                .enable();
            this.selectedSteps.push('POST_PERSO_AND_ISSUANCE');
            this.workflowForm.get('verificationPolices.verifyWithPIN').setValue(true);
        } else {
            if (this.selectedSteps.indexOf('POST_PERSO_AND_ISSUANCE') !== -1) {
                this.selectedSteps.splice(this.selectedSteps.indexOf('POST_PERSO_AND_ISSUANCE'), 1);
            }
            this.workflowForm.get('verificationPolices').reset();
            this.workflowForm.get('verificationPolices').disable();
        }
    }
    async onTokenIssuanceChange(event: MatCheckboxChange) {
        if (event.checked) {
            this.requiredTokenCardIssuance = true;
            if (this.isPKICertsEnabled) {
                this.workflowForm.get('issueCertsToCard.tokenIssueCerts').enable();
                this.workflowForm.get('activation').enable();
                this.workflowForm
                    .get('certs')
                    .get('contentSign')
                    .enable();
                this.workflowForm.get('chipEncode.isChipEncode').enable();
                this.workflowForm.get('chipPlusVisualIDPrint.isChipPlusVisualIDPrint').enable();
                if (this.workflowForm.get('issueCertsToCard.tokenIssueCerts').value) {
                    (this.workflowForm.get('cardCertificates') as FormArray).enable();
                    this.workflowForm.get('certs').enable();
                }
            }
            if (this.isPrintEnabled) {
                this.workflowForm.get('visualIDPrint.isVisualIDPrint').enable();
            }
            if (this.workflowForm.get('chipEncode.isChipEncode').value) {
                this.workflowForm.get('chipEncode.chipEncodeGroup').enable();
            }
            if (this.workflowForm.get('chipPlusVisualIDPrint.isChipPlusVisualIDPrint').value) {
                this.workflowForm.get('chipPlusVisualIDPrint.chipPlusVisualIDGroup').enable();
                (this.workflowForm.get('cardVisualDesigns') as FormArray).enable();
            }
            if (this.workflowForm.get('visualIDPrint.isVisualIDPrint').value) {
                this.workflowForm.get('visualIDPrint.visualIDPrintGroup').enable();
                (this.workflowForm.get('cardVisualDesigns') as FormArray).enable();
            }
            this.selectedSteps.push('PERSO_AND_ISSUANCE');
            if (this.showAppletSelection) {
                this.workflowForm.get('appletSelectionFormGroup').enable();
            }
        } else {
            this.requiredTokenCardIssuance = false;
            (this.workflowForm.get('cardVisualDesigns') as FormArray).disable();
            (this.workflowForm.get('cardVisualDesigns') as FormArray).reset();
            (this.workflowForm.get('cardCertificates') as FormArray).disable();
            this.workflowForm.get('certs').disable();
            this.workflowForm.get('certs').reset();
            this.workflowForm.get('issueCertsToCard').disable();
            this.workflowForm.get('issueCertsToCard').reset(); // group
            this.workflowForm.get('chipEncode').disable();
            this.workflowForm.get('chipEncode').reset();
            this.workflowForm.get('chipPlusVisualIDPrint').disable();
            this.workflowForm.get('chipPlusVisualIDPrint').reset();
            this.workflowForm.get('visualIDPrint').disable();
            this.workflowForm.get('visualIDPrint').reset();
            this.workflowForm
                .get('certs')
                .get('contentSign')
                .disable();
            this.workflowForm
                .get('certs')
                .get('contentSign')
                .reset();
            this.workflowForm.get('activation').disable();
            this.workflowForm.get('activation').reset();
            if (this.selectedSteps.indexOf('PERSO_AND_ISSUANCE') !== -1) {
                this.selectedSteps.splice(this.selectedSteps.indexOf('PERSO_AND_ISSUANCE'), 1);
            }
            if (this.selectedSteps.indexOf('PKI_CERTIFICATES') !== -1) {
                this.selectedSteps.splice(this.selectedSteps.indexOf('PKI_CERTIFICATES'), 1);
            }
            this.workflowForm.get('appletSelectionFormGroup').disable();
            this.workflowForm.get('fidoe2AppletDetails').disable();
            this.workflowForm.get('fidoe2AppletDetails').reset();
        }
    }
    onFido2AppletSelection(event: MatCheckboxChange) {
        console.log(event);
        if (event.checked) {
            this.isFidoAppletSelected = true;
            this.workflowForm.get('fidoe2AppletDetails').enable();
        } else {
            this.isFidoAppletSelected = false;
            this.workflowForm.get('fidoe2AppletDetails').disable();
            this.workflowForm.get('fidoe2AppletDetails').reset();
        }
    }
    onChipEncodeChange(event: MatCheckboxChange) {
        if (event.checked) {
            this.workflowForm.get('chipEncode.chipEncodeGroup').enable();
        } else {
            this.workflowForm.get('chipEncode.chipEncodeGroup').reset();
            this.workflowForm.get('chipEncode.chipEncodeGroup').disable();
        }
    }
    onChipPlusVisualIDPrintChange(event: MatCheckboxChange) {
        if (event.checked) {
            this.showCardVisualIdSelection = true;
            this.workflowForm.get('chipPlusVisualIDPrint.chipPlusVisualIDGroup').enable();
            (this.workflowForm.get('cardVisualDesigns') as FormArray).enable();
        } else {
            this.workflowForm.get('chipPlusVisualIDPrint.chipPlusVisualIDGroup').reset();
            this.workflowForm.get('chipPlusVisualIDPrint.chipPlusVisualIDGroup').disable();
            if (this.workflowForm.get('visualIDPrint.isVisualIDPrint').value) {
                this.showCardVisualIdSelection = true;
                (this.workflowForm.get('cardVisualDesigns') as FormArray).enable();
            } else {
                this.showCardVisualIdSelection = false;
                (this.workflowForm.get('cardVisualDesigns') as FormArray).disable();
            }
        }
    }
    onVisualIDPrintChange(event: MatCheckboxChange) {
        if (event.checked) {
            this.workflowForm.get('visualIDPrint.visualIDPrintGroup').enable();
            this.showCardVisualIdSelection = true;
            (this.workflowForm.get('cardVisualDesigns') as FormArray).enable();
        } else {
            this.workflowForm.get('visualIDPrint.visualIDPrintGroup').disable();
            this.workflowForm.get('visualIDPrint.visualIDPrintGroup').reset();
            if (this.workflowForm.get('chipPlusVisualIDPrint.isChipPlusVisualIDPrint').value) {
                this.showCardVisualIdSelection = true;
                (this.workflowForm.get('cardVisualDesigns') as FormArray).enable();
            } else {
                this.showCardVisualIdSelection = false;
                (this.workflowForm.get('cardVisualDesigns') as FormArray).disable();
            }
        }
    }
    onCardVisualIdGroupChange(event, indx) {
        if (event.isUserInput) {
            let visualTemplateId;
            if (this.smartCardVisualTemplates.length > 0) {
                visualTemplateId = this.smartCardVisualTemplates[0].id;
            }
            const selectedGroupId = event.source.value;
            if (event.source.selected) {
                let formGroupControl: FormGroup = null;
                (this.workflowForm.get('cardVisualDesigns') as FormArray).controls.forEach((element: FormGroup) => {
                    element.controls['cardVisualTemplateGroup'].value.forEach(group => {
                        if (group === selectedGroupId) {
                            formGroupControl = element;
                        }
                    });
                });
                const index = (this.workflowForm.get('cardVisualDesigns') as FormArray).controls.indexOf(formGroupControl);
                if (index !== -1 && formGroupControl !== null) {
                    const visualTemplateGroup: string[] = (this.workflowForm.get('cardVisualDesigns') as FormArray).at(index).value
                        .cardVisualTemplateGroup;
                    if (visualTemplateGroup.length !== 1) {
                        const i = visualTemplateGroup.indexOf(selectedGroupId);
                        if (i !== -1) {
                            (this.workflowForm.get('cardVisualDesigns') as FormArray)
                                .at(index)
                                .get('cardVisualTemplateGroup')
                                .setValue(visualTemplateGroup.filter(c => c !== selectedGroupId));
                        }
                    } else {
                        (this.workflowForm.get('cardVisualDesigns') as FormArray).removeAt(index);
                    }
                }
            } else {
                const group: Array<String> = [];
                group.push(selectedGroupId);
                const cardVisualDesignGroup = (this.workflowForm.get('cardVisualDesigns') as FormArray).at(indx).value;
                if (cardVisualDesignGroup.cardVisualTemplateGroup.length === 1) {
                    (this.workflowForm.get('cardVisualDesigns') as FormArray).removeAt(indx);
                    (this.workflowForm.get('cardVisualDesigns') as FormArray).push(
                        this.formBuilder.group({
                            cardVisualTemplateGroup: [group, [Validators.required]],
                            visualTemplateID: [visualTemplateId, [Validators.required]]
                        })
                    );
                } else {
                    (this.workflowForm.get('cardVisualDesigns') as FormArray).push(
                        this.formBuilder.group({
                            cardVisualTemplateGroup: [group, [Validators.required]],
                            visualTemplateID: [visualTemplateId, [Validators.required]]
                        })
                    );
                }
            }
        }
    }
    onMobileVisualIdGroupChange(event, index, k) {
        if (event.isUserInput) {
            let visualTemplateId;
            if (this.mobileVisualTemplates.length > 0) {
                visualTemplateId = this.mobileVisualTemplates[0].id;
            }
            const mobileVisualDesignsArray = (this.workflowForm.get(
                'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
            ) as FormArray).controls[index].get('mobileVisualDesigns') as FormArray;
            const selectedGroupId = event.source.value;
            if (event.source.selected) {
                let formGroupControl: FormGroup = null;
                if (mobileVisualDesignsArray !== null) {
                    mobileVisualDesignsArray.controls.forEach((element: FormGroup) => {
                        if (element.controls['mobileVisualTemplateGroup'].value !== null) {
                            element.controls['mobileVisualTemplateGroup'].value.forEach(group => {
                                if (group === selectedGroupId) {
                                    formGroupControl = element;
                                }
                            });
                        }
                    });
                }
                const index2 = mobileVisualDesignsArray.controls.indexOf(formGroupControl);
                if (index2 !== -1 && formGroupControl !== null) {
                    const visualTemplateGroup: string[] = mobileVisualDesignsArray.at(index2).value.mobileVisualTemplateGroup;
                    if (visualTemplateGroup.length !== 1) {
                        const i = visualTemplateGroup.indexOf(selectedGroupId);
                        if (i !== -1) {
                            mobileVisualDesignsArray
                                .at(index2)
                                .get('mobileVisualTemplateGroup')
                                .setValue(visualTemplateGroup.filter(c => c !== selectedGroupId));
                        }
                    } else {
                        mobileVisualDesignsArray.removeAt(index2);
                    }
                }
            } else {
                const group: Array<String> = [];
                group.push(selectedGroupId);
                const mobileVID = mobileVisualDesignsArray.at(k).value;
                if (mobileVID.mobileVisualTemplateGroup.length === 1) {
                    mobileVisualDesignsArray.removeAt(k);
                    mobileVisualDesignsArray.push(
                        this.formBuilder.group({
                            mobileVisualTemplateGroup: [group, [Validators.required]],
                            visualTemplateID: [visualTemplateId, [Validators.required]]
                        })
                    );
                } else {
                    mobileVisualDesignsArray.push(
                        this.formBuilder.group({
                            mobileVisualTemplateGroup: [group, [Validators.required]],
                            visualTemplateID: [visualTemplateId, [Validators.required]]
                        })
                    );
                }
            }
        }
    }
    tokenIssueCheckBox($event: MatCheckboxChange) {
        this.tokenIssueCertCheckboxEvent = $event;
        if ($event.checked) {
            this.onCertificatesChange($event);
        } else {
            if ((this.workflowForm.get('cardCertificates') as FormArray).controls.find(e => e.touched === true)) {
                this.workflowForm.get('issueCertsToCard.tokenIssueCerts').setValue(true);
                $('#resetCert').modal('show');
            } else {
                this.onCertificatesChange($event);
            }
        }
    }
    unSelectCheckbox() {
        this.workflowForm.get('issueCertsToCard.tokenIssueCerts').setValue(false);
        this.onCertificatesChange(this.tokenIssueCertCheckboxEvent);
    }

    async onCertificatesChange(certificates: MatCheckboxChange) {
        if (certificates.checked) {
            this.isTokenCertsEnabled = true;
            // Load CA Servers
            const caProviders: CAProvider[] = await this.workflowService.getCaProviders().toPromise();
            this.cardNotificationFrequencies = await this.workflowService.getConfigIdValues('EMAIL_NOTIFICATION_FREQUENCY').toPromise();
            this.cardCertificateExpiringDays = await this.workflowService
                .getConfigIdValues('NOTIFY_USER_ABOUT_CERT_EXPIRING_IN_DAYS')
                .toPromise();

            if (this.caServers.length === 0) {
                for (let i = 0; i < caProviders.length; i++) {
                    const caServerConfigs: CAServerConfig[] = await this.workflowService.getCaServers(caProviders[i].id).toPromise();
                    caServerConfigs.forEach(caServerConfig => {
                        caServerConfig.caProviderName = caProviders[i].name;
                        this.caServers.push(caServerConfig);
                    });
                }
            }

            (this.workflowForm.get('cardCertificates') as FormArray).enable();
            this.workflowForm
                .get('certs')
                .get('email')
                .enable();
            this.workflowForm
                .get('certs')
                .get('noOfDaysBeforeCertExpiry')
                .enable();
            this.selectedSteps.push('PKI_CERTIFICATES');
        } else {
            this.workflowForm
                .get('certs')
                .get('email')
                .disable();
            this.workflowForm
                .get('certs')
                .get('email')
                .reset();
            this.workflowForm
                .get('certs')
                .get('noOfDaysBeforeCertExpiry')
                .disable();
            this.workflowForm
                .get('certs')
                .get('noOfDaysBeforeCertExpiry')
                .reset();
            (this.workflowForm.get('cardCertificates') as FormArray).controls.forEach((element: FormGroup) => {
                Object.keys(element.controls).forEach(key => {
                    const c = this.cardCertificates.length - 1;
                    if (c >= 0) {
                        this.removeCardCert(c);
                    }
                });
            });
            this.isTokenCertsEnabled = false;
            this.addCardCert(0);
            this.removeCardCert(0);
            (this.workflowForm.get('cardCertificates') as FormArray).disable();
            if (this.selectedSteps.indexOf('PKI_CERTIFICATES') !== -1) {
                this.selectedSteps.splice(this.selectedSteps.indexOf('PKI_CERTIFICATES'), 1);
            }
        }
    }

    onMobileIdReq() {
        if (this.workflowForm.get('mobileId.isMobileIdReq').value) {
            this.requiredMobileIssuance = true;
            this.workflowForm.get('mobileId.enforceHardwareBackeKeystrore').enable();
            this.workflowForm.get('mobileId.onBoardingConfig.isSelfServiceReq').enable();
            // check enable issuance by default
            this.selectedSteps.push('MOBILE_ID_IDENTITY_ISSUANCE');
            this.workflowForm.get('mobileId.digitalIdentityIssuance.enableMobileID').setValue(true);
            this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity').enable();
            this.workflowForm.get('mobileId.digitalIdentityIssuance.bindExpirationToMobileID').enable();
            (this.workflowForm
                .get('mobileId.digitalIdentityIssuance.additionalIdentity')
                .get('identityCredentialsArray') as FormArray).controls.forEach((control: FormGroup) => {
                if (control.get('issueCertificates').value) {
                    (control.get('addCertificatesArray') as FormArray).enable();
                    (control.get('notifyUsers') as FormArray).enable();
                    (control.get('emailFrequency') as FormArray).enable();
                } else {
                    (control.get('addCertificatesArray') as FormArray).disable();
                    (control.get('notifyUsers') as FormArray).disable();
                    (control.get('emailFrequency') as FormArray).disable();
                }
            });
            if (this.workflowForm.get('mobileId.onBoardingConfig.isSelfServiceReq').value) {
                this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReq').enable();
                this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerification').enable();
                this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsers').setValue(true);
                this.workflowForm
                    .get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions')
                    .get('scanQrCode')
                    .setValue(true);
                this.workflowForm
                    .get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions')
                    .get('nfc')
                    .setValue(false);
                this.workflowForm
                    .get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions')
                    .get('frontDoc')
                    .setValue(true);
                this.workflowForm
                    .get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions')
                    .get('backDoc')
                    .setValue(true);
                this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.trustedIdentitiesAllowed').enable();
                if (!this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerification').value) {
                    this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions').disable();
                }
                if (!this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReq').value) {
                    this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReqOptions').disable();
                }
            }
        } else {
            this.requiredMobileIssuance = false;
            this.workflowForm.get('mobileId.enforceHardwareBackeKeystrore').disable();
            this.workflowForm.get('mobileId.enforceHardwareBackeKeystrore').reset();
            this.workflowForm.get('mobileId.onBoardingConfig').disable();
            this.workflowForm.get('mobileId.onBoardingConfig').reset();
            this.workflowForm.get('mobileId.digitalIdentityIssuance.enableMobileID').disable();
            this.workflowForm.get('mobileId.digitalIdentityIssuance.enableMobileID').reset();
            this.workflowForm.get('mobileId.digitalIdentityIssuance.bindExpirationToMobileID').disable();
            this.workflowForm.get('mobileId.digitalIdentityIssuance.bindExpirationToMobileID').reset();
            this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity').disable();
            this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity').reset();
        }
    }
    onEnforceHardwareKeystoreCheck($event) {
        if ($event.checked) {
            this.selectedSteps.push('MOBILE_ID_HARDWARE_BACKED_KEYSTORE');
        } else {
            this.selectedSteps.splice(this.selectedSteps.indexOf('MOBILE_ID_HARDWARE_BACKED_KEYSTORE'), 1);
        }
    }
    enableselfServiceOptions($event) {
        if ($event.checked) {
            this.selectedSteps.push('MOBILE_ID_ONBOARDING_CONFIG');
            if (this.trustedExistingIdentityModels.length === 0) {
                this.workflowService.getOrgTrustedExistingIdentityModels().subscribe(idModels => {
                    this.trustedExistingIdentityModels = idModels;
                });
            }
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerification').enable();
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReq').enable();
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsers').setValue(true);
            this.workflowForm
                .get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions')
                .get('scanQrCode')
                .setValue(true);
            this.workflowForm
                .get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions')
                .get('nfc')
                .setValue(false);
            this.workflowForm
                .get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions')
                .get('frontDoc')
                .setValue(true);
            this.workflowForm
                .get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions')
                .get('backDoc')
                .setValue(true);
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.trustedIdentitiesAllowed').enable();
            if (!this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerification').value) {
                this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions').disable();
            }
            if (!this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReq').value) {
                this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReqOptions').disable();
            }
        } else {
            this.selectedSteps.splice(this.selectedSteps.indexOf('MOBILE_ID_ONBOARDING_CONFIG'), 1);
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions').reset();
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions').disable();
        }
    }
    onboardEnrollUsers($event) {
        if (this.trustedExistingIdentityModels.length === 0) {
            this.workflowService.getOrgTrustedExistingIdentityModels().subscribe(idModels => {
                this.trustedExistingIdentityModels = idModels;
            });
        }
        if (this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsers').value) {
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions').enable();
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.trustedIdentitiesAllowed').enable();
        } else {
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions').reset();
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.onboardEnrollUsersOptions').disable();
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.trustedIdentitiesAllowed').reset();
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.trustedIdentitiesAllowed').disable();
        }
    }
    onFaceVerifyReqCheck(event) {
        if (event.checked) {
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions').enable();
            if (
                !this.workflowForm.get(
                    'mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions.saveTheCapturedFaceForVisualID'
                ).value
            ) {
                this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions.facialCropSize').disable();
            }
        } else {
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions').reset();
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions').disable();
        }
    }
    onCaptureFaceForVIDCheck(event) {
        if (event.checked) {
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions.facialCropSize').enable();
        } else {
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions.facialCropSize').reset();
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.faceVerificationOptions.facialCropSize').disable();
        }
    }
    onApprovalOrAdjReqCheck(event) {
        if (event.checked) {
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReqOptions').enable();
        } else {
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReqOptions').reset();
            this.workflowForm.get('mobileId.onBoardingConfig.selfServiceOptions.approvalOrAdjudicationReqOptions').disable();
        }
    }
    mobileCAServerChange(event: any, i, c) {
        if (event.value !== undefined) {
            const identitiesArray = (this.workflowForm.get(
                'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
            ) as FormArray).at(i);
            const certGroup = (identitiesArray.get('addCertificatesArray') as FormArray).at(c);
            this.workflowService.getCaTemplates(event.value.id, 'mobile').subscribe(caTemplates => {
                certGroup.get('certProfileList').setValue(caTemplates);
            });
        }
    }
    onSelectionOfCertProfile(event: any, i, c) {
        const identitiesArray = (this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).at(i);
        const certGroup = (identitiesArray.get('addCertificatesArray') as FormArray).at(c);
        if (event.value.toLowerCase().includes('keymanagement')) {
            certGroup.get('hideKeyEscrow').setValue(false);
        } else {
            certGroup.get('hideKeyEscrow').setValue(true);
        }
    }
    showKeyEscrow(i, c) {
        const identitiesArray = (this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).at(i);
        const certGroup = (identitiesArray.get('addCertificatesArray') as FormArray).at(c);
        return certGroup.get('hideKeyEscrow').value;
    }
    mobileCertProfiles(i, c) {
        let list = [];
        const certProfileListControl = ((this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).controls[i].get('addCertificatesArray') as FormArray).controls[c].get('certProfileList');
        if (certProfileListControl !== null) {
            list = certProfileListControl.value;
        }
        return list;
    }
    async enableAdditionalIdentity(event) {
        if (this.workflowForm.get('mobileId.digitalIdentityIssuance.enableMobileID').value) {
            this.selectedSteps.push('MOBILE_ID_IDENTITY_ISSUANCE');
            this.workflowForm.get('mobileId.digitalIdentityIssuance.bindExpirationToMobileID').enable();
            this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity').enable();
            (this.workflowForm
                .get('mobileId.digitalIdentityIssuance.additionalIdentity')
                .get('identityCredentialsArray') as FormArray).controls.forEach((control: FormGroup) => {
                (control.get('addCertificatesArray') as FormArray).disable();
                (control.get('notifyUsers') as FormArray).disable();
                (control.get('emailFrequency') as FormArray).disable();
            });
        } else {
            this.workflowForm.get('mobileId.digitalIdentityIssuance.bindExpirationToMobileID').reset();
            this.workflowForm.get('mobileId.digitalIdentityIssuance.bindExpirationToMobileID').disable();
            this.selectedSteps.splice(this.selectedSteps.indexOf('MOBILE_ID_IDENTITY_ISSUANCE'), 1);
            this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity').reset();
            this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity').disable();
        }
    }
    async onChangeMobileCerts(event: MatCheckboxChange, index) {
        const formArray = (this.workflowForm
            .get('mobileId.digitalIdentityIssuance.additionalIdentity')
            .get('identityCredentialsArray') as FormArray).at(index);
        if (event.checked) {
            (formArray.get('addCertificatesArray') as FormArray).enable();
            formArray.get('notifyUsers').enable();
            formArray.get('emailFrequency').enable();
            if (this.mobileCertificateExpiringDays.length === 0) {
                this.mobileCertificateExpiringDays = await this.workflowService
                    .getConfigIdValues('NOTIFY_USER_ABOUT_CERT_EXPIRING_IN_DAYS')
                    .toPromise();
            }
            if (this.mobileNotificationFrequencies.length === 0) {
                this.mobileNotificationFrequencies = await this.workflowService
                    .getConfigIdValues('EMAIL_NOTIFICATION_FREQUENCY')
                    .toPromise();
            }
            if (this.caServers.length === 0) {
                const caProviders: CAProvider[] = await this.workflowService.getCaProviders().toPromise();
                this.caServers = [];
                for (let i = 0; i < caProviders.length; i++) {
                    const caServerConfigs: CAServerConfig[] = await this.workflowService.getCaServers(caProviders[i].id).toPromise();
                    caServerConfigs.forEach(caServerConfig => {
                        caServerConfig.caProviderName = caProviders[i].name;
                        this.caServers.push(caServerConfig);
                    });
                }
            }
        } else {
            (formArray.get('addCertificatesArray') as FormArray).disable();
            (formArray.get('addCertificatesArray') as FormArray).reset();
            formArray.get('notifyUsers').disable();
            formArray.get('notifyUsers').reset();
            formArray.get('emailFrequency').disable();
            formArray.get('emailFrequency').reset();
        }
    }
    // onBoardingConfig - selfServiceOptions - onboardEnrollUsers

    addMobileCertRow(i) {
        (this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray') as FormArray).controls[i][
            'controls'
        ]['addCertificatesArray'].push(
            this.formBuilder.group({
                id: [''],
                certType: ['', Validators.required],
                certTypeList: [''],
                caServer: ['', Validators.required],
                certProfile: ['', Validators.required],
                certProfileList: [''],
                hideKeyEscrow: [true],
                escrow: [false],
                revocation: [false],
                algorithm: ['', Validators.required],
                keySize: ['', Validators.required],
                keySizeList: [''],
                subDn: ['', Validators.required],
                subDnDisplayValue: [''],
                subAN: ['']
            })
        );
    }

    removeMobileCertRow(i, ind) {
        const addCertArray = (this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).controls[i]['controls']['addCertificatesArray'] as FormArray;

        if (addCertArray.length > 1) {
            addCertArray.removeAt(ind);
        }
    }

    addAdditionalIdentity() {
        this.deleteIdentity = true;
        (this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray') as FormArray).push(
            this.formBuilder.group({
                id: [''],
                friendlyName: ['', Validators.required],
                mobileVisualDesigns: this.formBuilder.array([]),
                pushVerify: [false],
                isContentSigning: [false],
                softOtp: [false],
                fido2: [false],
                consent: [false],
                issueCertificates: [false],
                addCertificatesArray: this.formBuilder.array([
                    this.formBuilder.group({
                        id: [''],
                        certType: ['', Validators.required],
                        certTypeList: [''],
                        caServer: ['', Validators.required],
                        certProfile: ['', Validators.required],
                        certProfileList: [''],
                        hideKeyEscrow: [true],
                        escrow: [false],
                        revocation: [false],
                        algorithm: ['', Validators.required],
                        keySize: ['', Validators.required],
                        keySizeList: [''],
                        subDn: ['', Validators.required],
                        subDnDisplayValue: [''],
                        subAN: ['']
                    })
                ]),
                notifyUsers: ['', Validators.required],
                emailFrequency: ['', Validators.required]
            })
        );
        const selectedGroupLength = this.workflowForm.get('general.selectedGroups').value.length;
        const index =
            (this.workflowForm.get('mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray') as FormArray).length - 1;
        const mobileIDIssuuanceFormArray = (this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray).at(index);
        const mobileVisualDesignsFormArray = mobileIDIssuuanceFormArray.get('mobileVisualDesigns') as FormArray;
        (mobileIDIssuuanceFormArray.get('addCertificatesArray') as FormArray).disable();
        mobileIDIssuuanceFormArray.get('notifyUsers').disable();
        mobileIDIssuuanceFormArray.get('emailFrequency').disable();
        if (selectedGroupLength !== 0) {
            let visualTemplateId;
            if (this.mobileVisualTemplates.length > 0) {
                visualTemplateId = this.mobileVisualTemplates[0].id;
            }
            for (let k = 0; k < selectedGroupLength; k++) {
                mobileVisualDesignsFormArray.push(
                    this.formBuilder.group({
                        mobileVisualTemplateGroup: ['', [Validators.required]],
                        visualTemplateID: [visualTemplateId, [Validators.required]]
                    })
                );
            }
            for (let i = 0; i < mobileVisualDesignsFormArray.length; i++) {
                const group: Array<String> = [];
                group.push(this.workflowForm.get('general.selectedGroups').value[i].id);
                (mobileVisualDesignsFormArray['controls'][i] as FormGroup).controls['mobileVisualTemplateGroup'].setValue(group);
            }
        }
    }
    dellAdditionalIdentity(delAdditionalIdentity: any) {
        this.delAdditionalIdentity = delAdditionalIdentity;
    }
    deleteAdditionalIdentity(i) {
        const identityCredArray = this.workflowForm.get(
            'mobileId.digitalIdentityIssuance.additionalIdentity.identityCredentialsArray'
        ) as FormArray;
        if (identityCredArray.length > 1) {
            identityCredArray.removeAt(i);
        }
        if (identityCredArray.length < 2) {
            this.deleteIdentity = false;
        } else {
            this.deleteIdentity = true;
        }
    }
    goHome() {
        this.router.navigate(['workflows/manage-workflows']);
    }
}
export function maxLengthValidator(maxVal: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        if (control.value !== undefined && control.value !== null) {
            const value = control.value;
            if (Math.floor(value) > maxVal) {
                return { maxLengthValidator: true };
            }
        }
        return null;
    };
}
