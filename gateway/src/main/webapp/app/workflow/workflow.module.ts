import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NumberPickerModule } from 'ng-number-picker';
import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from 'app/material/material.module';
import { WORKFLOW_ROUTES } from './workflow.route';
import { ManageWorkflowComponent } from './manage/manage-workflow.component';
import { CreateWorkflowComponent } from './create/create-workflow.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { WorkflowsComponent } from './workflows/workflows.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NumberPickerModule,
    MaterialModule,
    GatewaySharedModule,
    RouterModule.forChild(WORKFLOW_ROUTES),
    AngularSvgIconModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ ManageWorkflowComponent, CreateWorkflowComponent, WorkflowsComponent ]
})
export class WorkflowModule { }
