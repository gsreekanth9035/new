import { DeviceType } from 'app/visual-design/device-type.model';

export class VisualTemplate {
    id: number;
    name: string;
    deviceTypes: Array<DeviceType>;

    constructor() {}
}
