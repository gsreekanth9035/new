export class WFStepCertConfig {
    id: number;
    certType: string;
    caServer: string;
    certTemplate: string;
    keyEscrow: boolean;
    disableRevoke: boolean;
    noOfDaysBeforeCertExpiry: number;
    algorithm: string;
    keysize: number;
    subjectDN: string;
    subjectAN: string;

    constructor() {}
}
