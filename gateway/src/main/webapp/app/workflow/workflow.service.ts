import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Principal } from 'app/core';
import { Workflow } from './workflow.model';
import { Group } from './group.model';
import { VisualTemplate } from './visual-template.model';
import { WorkflowStep } from './workflow-step.model';
import { Page } from 'app/visual-design/page.model';
import { OrganizationIdentity } from './model/org-identity-model';
import { DeviceProfile } from 'app/device-profile/device-profile.model';
import { WFMobileIDStepIdentityConfig } from './model/mobile-id-issuance.model';

@Injectable({
    providedIn: 'root'
})
export class WorkflowService {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name
        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });
    constructor(private httpClient: HttpClient, private principal: Principal) {}

    // Call REST APIs
    createWorkflow(workflow: Workflow): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');

        return this.httpClient.post(
            `${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/workflows`,
            workflow,
            { headers: this.httpHeaders, observe: 'response' }
        );
    }

    editWorkflow(wfName, workflow: Workflow): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');

        return this.httpClient.put(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/workflows/${wfName}`,
            workflow,
            { headers: this.httpHeaders, observe: 'response' }
        );
    }

    deleteWorkflow(workflowName: string): Observable<any> {
        return this.httpClient.delete(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/workflows/${workflowName}`,
            { observe: 'response' }
        );
    }

    getWorkflowByName(workflowName: string): Observable<Workflow> {
        return this.httpClient.get<Workflow>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/workflows/${workflowName}`
        );
    }

    getWorkflows(organization: string, sort: string, order: string, page: number, size: number): Observable<Page> {
        return this.httpClient.get<Page>(
            `${
                window.location.origin
            }/usermanagement/api/v1/organizations/${organization}/workflows?sort=${sort}&order=${order}&page=${page}&size=${size}`
        );
    }

    // REST Calls for Dropdown Values
    getRegOrgIdentityFields(identityTypeName: string): Observable<RegOrgIdentityField[]> {
        const host = window.location.origin;
        const orgName = this.principal.getLoggedInUser().organization;
        const workflowName = this.principal.getLoggedInUser().workflow;
        return this.httpClient.get<RegOrgIdentityField[]>(
            `${host}/usermanagement/api/v1/organizations/${orgName}/workflows/${workflowName}/steps/REGISTRATION/${identityTypeName}/orgIdentityFieldIdNames`
        );
    }
    getFaceCropSizes(identityTypeName: string): Observable<ConfigIdValuePair[]> {
        const host = window.location.origin;
        const orgName = this.principal.getLoggedInUser().organization;
        const identityFieldName = 'Photo';
        return this.httpClient.get<ConfigIdValuePair[]>(
            `${host}/usermanagement/api/v1/organizations/${orgName}/${identityTypeName}/${identityFieldName}/configs`
        );
    }

    getVisualTemplates(identityTypeName): Observable<VisualTemplate[]> {
        return this.httpClient.get<VisualTemplate[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/${identityTypeName}/visual-templates`
        );
    }

    getGroups(): Observable<Group[]> {
        // /workflowGroups
        return this.httpClient.get<Group[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/groups/workflowGroups`
        );
    }
    getAllGroups(): Observable<Group[]> {
        return this.httpClient.get<Group[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/groups`
        );
    }

    getDeviceProfiles(): Observable<DeviceProfile[]> {
        return this.httpClient.get<DeviceProfile[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/device-profiles/list`
        );
    }

    getCaProviders(): Observable<CAProvider[]> {
        const organizationName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<CAProvider[]>(
            `${window.location.origin}/caservice/api/v1/ca/organizations/${organizationName}/providers`
        );
    }

    getCaServers(providerId: number): Observable<CAServerConfig[]> {
        return this.httpClient.get<CAServerConfig[]>(`${window.location.origin}/caservice/api/v1/ca/providers/${providerId}/configs`);
    }

    getCaTemplates(configId: number, requestIsFor: string): Observable<string[]> {
        return this.httpClient.get<string[]>(
            `${window.location.origin}/caservice/api/v1/ca/caServer/${configId}/requestIsFor/${requestIsFor}/catemplates`
        );
    }

    getConfigIdValues(configName: string): Observable<ConfigIdValuePair[]> {
        return this.httpClient.get<ConfigIdValuePair[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/config/${configName}/values`
        );
    }

    getStepsByWorkflow(organizationName, workflowName): Observable<WorkflowStep[]> {
        return this.httpClient.get<WorkflowStep[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organizationName}/workflows/${workflowName}/steps`
        );
    }

    getWFMobileIDStepIdentityConfig(workflowName): Observable<WorkflowStep> {
        const organizationName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<WorkflowStep>(
            `${
                window.location.origin
            }/usermanagement/api/v1/organizations/${organizationName}/workflows/${workflowName}/mobile-issuance-details`
        );
    }

    getOrgIssuingIdentityModels(): Observable<OrganizationIdentity[]> {
        const organizationName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<OrganizationIdentity[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organizationName}/issuingIdentityTypes`
        );
    }
    getOrgTrustedExistingIdentityModels(): Observable<OrganizationIdentity[]> {
        const organizationName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<OrganizationIdentity[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organizationName}/trustedExistingIdentityTypes`
        );
    }
    getFingerByFingerCount(fingerCount): Observable<Finger[]> {
        const organizationName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<Finger[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organizationName}/fingersByCount/${fingerCount}`
        );
    }

    getCertTemplatesByIds(caServerIds: Array<number>, requestIsFor: string): Observable<CertTemplate[]> {
        return this.httpClient.post<CertTemplate[]>(
            `${window.location.origin}/caservice/api/v1/ca/requestIsFor/${requestIsFor}/certTemplates`,
            caServerIds
        );
    }

    checkTransparentPhotoEnabledInSystem(): Observable<any> {
        const organizationName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organizationName}/transparent-image-enabled-check`
        );
    }
}

export class ConfigIdValuePair {
    id: number;
    value: string;
}

export class CAProvider {
    id: number;
    name: string;

    constructor() {}
}

export class CAServerConfig {
    id: number;
    caName: string;
    caProviderName: string;
    caProviderId: number;
    certTemplates: string[] = [];
    constructor() {}
}

export class RegOrgIdentityField {
    orgIdentityFieldId: string;
    fieldDisplayName: string;
    mandatory: boolean;

    constructor() {}
}
export class Finger {
    fingerCount: number;
    fingerConfigValueName: string;
    constructor() {}
}
export class CertTemplate {
    caServerConfigId: number;
    certTemplates: string[] = [];
}
