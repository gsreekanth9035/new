import { Route, Routes } from '@angular/router';
import { ManageWorkflowComponent } from './manage/manage-workflow.component';
import { CreateWorkflowComponent } from './create/create-workflow.component';
import { WorkflowsComponent } from './workflows/workflows.component';

export const WORKFLOW_ROUTES: Routes = [
    {
        path: 'workflows',
        component: WorkflowsComponent,
        children: [
            { path: '', redirectTo: 'manage-workflows', pathMatch: 'full' },
            { path: 'manage-workflows', component: ManageWorkflowComponent },
            { path: 'create-workflow', component: CreateWorkflowComponent },
        ]
    },
    {
        path: 'createworkflow',
        component: CreateWorkflowComponent,
        data: {
            authorities: [],
            pageTitle: 'createWorkflow.title',
        }
    }
];
