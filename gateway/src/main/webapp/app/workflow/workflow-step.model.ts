import { WFStepRegistrationConfig } from './wf-step-registration-config.model';
import { WFStepGenericConfig } from './wf-step-generic-config.model';
import { WFStepCertConfig } from './wf-step-cert-config.model';
import { WFStepApprovalGroupConfig } from './wf-step-approval-group-config';
import { WFMobileIDStepIdentityConfig } from './model/mobile-id-issuance.model';
import { Group } from './group.model';
import { OrganizationIdentity } from './model/org-identity-model';
import { WfStepGroupConfig } from './model/wf-step-group-config.model';
import { WfStepAppletLoadingInfo } from './model/wf-applet-loading-info.model';

export class WorkflowStep {
    name: string;
    description: string;
    displayName: string;
    order: number;

    wfStepGenericConfigs: WFStepGenericConfig[];
    wfStepRegistrationConfigs: WFStepRegistrationConfig[];
    wfStepCertConfigs: WFStepCertConfig[];
    wfStepApprovalGroupConfigs: WFStepApprovalGroupConfig[];
    wfStepChipEncodeGroupConfigs: WfStepGroupConfig[] = [];
    wfStepChipEncodeVIDPrintGroupConfigs: WfStepGroupConfig[] = [];
    wfStepPrintGroupConfigs: WfStepGroupConfig[] = [];
    wfMobileIDStepIdentityConfigs: WFMobileIDStepIdentityConfig[];
    wfStepAdjudicationGroupConfigs: Group[];
    wfMobileIDStepTrustedIdentities: OrganizationIdentity[];
    wfStepAppletLoadingInfo: WfStepAppletLoadingInfo;
    constructor() {}
}
