export class WFStepRegistrationConfig {
    id: number;
    orgFieldId: number;
    fieldName: string;
    fieldLabel: string;
    required: boolean;
    mandatory: boolean;
    fieldMaxSize: number;

    constructor() {}
}
