import { WorkflowStep } from './workflow-step.model';
import { Group } from './group.model';
import { OrganizationIdentity } from './model/org-identity-model';
import { DeviceProfile } from 'app/device-profile/device-profile.model';

export class Workflow {
    name: string;
    description: string;
    defaultWorkflow: boolean;
    organizationIdentities: OrganizationIdentity;
    deviceProfiles: DeviceProfile[];
    groups: Group[];
    allowedDevicesApplicable: boolean;
    allowedDevices: number;
    expirationInMonths: number;
    expirationInDays: number;

    workflowSteps: WorkflowStep[];

    canEdit: boolean;
    canDelete: boolean;

    disableExpirationEnforcement: boolean;
    revokeEncryptionCertificate: boolean;

    requiredEnrollment: boolean;
    requiredTokenCardIssuance: boolean;
    requiredMobileIssuance: boolean;

    constructor() {}
}
