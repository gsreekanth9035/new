import { OnBoardingUserModule } from './onboarding-user/onboarding-user.module';
import { VisualDesignModule } from './visual-design/visual-design.module';
import './vendor.ts';

import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { Ng2Webstorage } from 'ngx-webstorage';
import { JhiEventManager } from 'ng-jhipster';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { GatewaySharedModule } from 'app/shared';
import { GatewayCoreModule } from 'app/core';
import { GatewayAppRoutingModule } from './app-routing.module';
import { GatewayHomeModule } from './home/home.module';
import { StateStorageService } from 'app/core/auth/state-storage.service';
import { EnrollModule } from './enroll/enroll.module';
import { IssuanceModule } from 'app/issuance/issuance.module';
import { ManageDeviceProfileModule } from './device-profile/manage/manage-device-profile.module';
import { AddDeviceProfileModule } from './device-profile/add/add-device-profile.module';
import { JhiMainComponent } from './layouts';
import { SidebarModule } from './layouts/sidebar/sidebar.module';
import { NavbarModule } from './layouts/navbar/navbar.module';
import { FooterModule } from './layouts/footer/footer.module';
import { ManageIdentityDevicesModule } from './manage-identity-devices/manage-identity-devices.module';
import { WorkflowModule } from './workflow/workflow.module';
import { PairMobileDeviceModule } from './pair-mobile-device/pair-mobile-device.module';
import { VehicleRegistrationModule } from './vehicle-registration/vehicle-registration.module';
import { ManageVehiclesModule } from './manage-vehicles/manage-vehicles.module';
import { ManageGroupsModule } from './manage-groups/manage-groups.module';
import { LoginModule } from './layouts/login/login.module';
import { SearchUserModule } from './search-user/search-user.module';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { OrganizationModule } from './organization/organization.module';
import { ReportsModule } from './monitor/reports/reports.module';
import { ExpressEnrollUserModule } from './expressenroll/expressenroll.module';
import { ActivityModule } from './activity/activity.module';
import { MdModule } from './md/md.module';
import { CardPrintersModule } from './card-printers/card-printers.module';
import { ApproveRequestsModule } from './approve-requests/approve-requests.module';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive'; // this includes the core NgIdleModule but includes keepalive providers for easy wireup
import { IframesafePipe } from './iframesafe.pipe';
import { ConnectorService } from './connector.service';
import { PairMobileServiceModule } from './pair-mobile-service/pair-mobile-service.module';
import { RelyingPartyModule } from './relying-party/relying-party.module';
import { IdReaderModule } from './id-reader/id-reader.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatIconModule,
    MatInputModule,
    MatRippleModule,
    MatTooltipModule
} from '@angular/material';
import { FactoryResetModule } from './factory-reset/factory-reset.module';
import { MatDialogModule } from '@angular/material';
import { AgmCoreModule } from '@agm/core';
// import { MomentModule } from 'angular2-moment'; // optional, provides moment-style pipes for date formatting

@NgModule({
    imports: [
        BrowserModule,
        GatewayAppRoutingModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatInputModule,
        FormsModule,
        MatIconModule,
        MatCardModule,
        MatRippleModule,
        MatCheckboxModule,
        MatTooltipModule,
        HttpModule,
        // MomentModule,
        NgIdleKeepaliveModule.forRoot(),
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
        GatewaySharedModule,
        GatewayCoreModule,
        GatewayHomeModule,
        LoginModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        EnrollModule,
        ActivityModule,
        IssuanceModule,
        ManageDeviceProfileModule,
        AddDeviceProfileModule,
        ManageIdentityDevicesModule,
        WorkflowModule,
        PairMobileDeviceModule,
        VisualDesignModule,
        CardPrintersModule,
        VehicleRegistrationModule,
        ManageVehiclesModule,
        ManageGroupsModule,
        OnBoardingUserModule,
        SearchUserModule,
        HttpClientModule,
        AngularSvgIconModule,
        OrganizationModule,
        ReportsModule,
        ExpressEnrollUserModule,
        ApproveRequestsModule,
        MdModule,
        PairMobileServiceModule,
        RelyingPartyModule,
        IdReaderModule,
        FactoryResetModule,
        MatDialogModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBZN7bx3cqAbaDA9eMPZ1PzHpGOTS-x3iQ',
            libraries: ['geometry']
        })
    ],
    declarations: [JhiMainComponent, IframesafePipe],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [StateStorageService, Injector, ConnectorService]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [JhiEventManager]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [Injector]
        },
        ConnectorService
    ],
    bootstrap: [JhiMainComponent]
})
export class GatewayAppModule {}
