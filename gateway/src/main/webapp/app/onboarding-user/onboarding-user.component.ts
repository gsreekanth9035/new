import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { OnboardingUserService } from './onboarding-user.service';
import { Group } from 'app/workflow/group.model';
import { Role } from 'app/workflow/role.model';
import { DataService, Data } from 'app/device-profile/data.service';
import { Principal } from 'app/core';
import { User } from 'app/enroll/user.model';
import { MatDialog, MatOptionSelectionChange } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { SearchUserService } from 'app/search-user/search-user.service';
import { AddIdentityDeviceDialogService } from 'app/manage-identity-devices/add-identity-device-dialog.service';
import { SharedService } from 'app/shared/util/shared-service';
import { Console } from 'console';
import { containsElement } from '@angular/animations/browser/src/render/shared';
import { ValidationByLanguageService, ValidatorsComponent } from 'app/shared/util/validation-by-language.service';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
    selector: 'jhi-onboarding',
    templateUrl: './onboarding-user.component.html',
    styleUrls: ['onboarding-user.scss']
    // encapsulation: ViewEncapsulation.None
})
export class OnboardingComponent implements OnInit {
    // Initialization
    onbordingUser: any;
    onboardingForm: FormGroup = this.formBuilder.group({});
    pinHide = true;
    groups: Group[];
    roles: Role[];
    selectedRoles: string[];
    saveOnboardingUserSuccess;
    isLoadingResults: boolean;
    hasEnrollmentPermission = false;
    hasIssuancePermission = false;
    hasApprovalPermission = false;
    user: User;
    userNameValidationError = false;
    userNameValidationNumberError = false;
    saveError = false;
    errorMsg = '';
    data = new Data();
    editUser = new IdentityProviderUser();
    userName: string;
    editOnboarding = false;
    sendInvitesVia: string[];
    isEnrolled = false;
    approvalRequired = false;
    countryCodes: Country[] = [];
    userNameSpaceValidationError = false;
    validations: ValidatorsComponent;
    subscription: Subscription;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private onboardingUserService: OnboardingUserService,
        private principal: Principal,
        private dataService: DataService,
        private translateService: TranslateService,
        public dialog: MatDialog,
        private searchUserService: SearchUserService,
        private identityDeviceDialogService: AddIdentityDeviceDialogService,
        private sharedService: SharedService,
        private validationByLanguageService: ValidationByLanguageService
    ) {
        this.countryCodes = this.sharedService.getCountryCodes();
        this.subscription = this.validationByLanguageService.cuurentValidationsObj.subscribe(res => {
            this.validations = res;
            this.changeValidations();
        });
        this.buildOnboardForm();
    }

    ngOnInit() {
        this.principal.hasAuthority('Menu_Enrollment').then(res => {
            this.hasEnrollmentPermission = true;
        });
        if (this.principal.getLoggedInUser().authorities.includes('Menu_Issuance')) {
            this.hasIssuancePermission = true;
        }
        if (this.principal.getLoggedInUser().authorities.includes('Menu_Approve_Requests')) {
            this.hasApprovalPermission = true;
        }
        this.data = this.dataService.getCurrentObj();
        this.onboardingUserService.getGroups().subscribe(groups => {
            this.groups = groups;
        });
        this.onboardingUserService.getRolesFromAuthService().subscribe(roles => {
            let rolesByLoggedInUserRole = roles;
            const lowerCaseRoles = [];
            this.principal.getLoggedInUser().roles.forEach(role => {
                lowerCaseRoles.push(role.toLowerCase());
            });
            if (lowerCaseRoles.includes('role_operator')) {
                rolesByLoggedInUserRole = roles.filter(e => e.name.toLowerCase() !== 'role_admin');
            } else if (lowerCaseRoles.includes('role_adjudicator')) {
                rolesByLoggedInUserRole = roles.filter(
                    e => !e.name.toLowerCase().includes('role_admin') && !e.name.toLowerCase().includes('role_operator')
                );
            }
            this.roles = rolesByLoggedInUserRole;
            if (this.data !== null) {
                this.editOnboarding = true;
                this.userName = this.data.userName;
                this.onboardingUserService.getUserOnboardDetails(this.userName).subscribe(identityProviderUser => {
                    this.editUser = identityProviderUser;
                    this.setUserDetails(this.editUser);
                });
            }
        });
        this.sendInvitesVia = ['SMS', 'Email'];
    }
    changeValidations() {
        const firstName = this.onboardingForm.controls['firstName'];
        const lastName = this.onboardingForm.controls['lastName'];
        firstName.clearValidators();
        firstName.setValidators([
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(30),
            this.validations.allowedCharactersAndNumbersAndSpace
        ]);
        firstName.updateValueAndValidity();
        lastName.clearValidators();
        lastName.setValidators([
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(30),
            this.validations.allowedCharactersAndNumbersAndSpace
        ]);
        lastName.updateValueAndValidity();
    }
    buildOnboardForm() {
        const allowedCharactersAndNumbersAndSpace = this.validations.allowedCharactersAndNumbersAndSpace;
        this.onboardingForm = this.formBuilder.group({
            firstName: [
                '',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(30),
                    allowedCharactersAndNumbersAndSpace,
                    customValidators
                ]
            ],
            lastName: [
                '',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(30),
                    allowedCharactersAndNumbersAndSpace,
                    customValidators
                ]
            ],
            email: [
                '',
                [
                    Validators.required,
                    Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')
                ]
            ],
            countryCode: ['', [Validators.pattern('^[0-9 +]+$'), Validators.maxLength(4)]],
            // Validators.minLength(7), Validators.maxLength(12), Validators.pattern('^[0-9]+$')
            contactNumber: ['', [Validators.minLength(7), Validators.maxLength(12), Validators.pattern('^[0-9]+$')]],
            enabled: [true],
            emailInvite: [false],
            smsInvite: [false],
            userName: [
                '',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(30),
                    Validators.pattern('^(?!.*?[_]{2})[a-zA-Z0-9_ ]+$')
                ]
            ],
            credentialType: ['password'],
            credentialTemporary: [true],
            groupId: ['', [Validators.required]],
            groupName: [''],
            authServiceRoleID: ['', [Validators.required, selectedItemsValidator]]
        });
        this.onboardingForm.controls['smsInvite'].disable();
    }
    setUserDetails(user: IdentityProviderUser) {
        this.onboardingForm.controls['firstName'].setValue(user.firstName);
        this.onboardingForm.controls['lastName'].setValue(user.lastName);
        this.onboardingForm.controls['email'].setValue(user.email);
        if (user.countryCode !== null) {
            this.onboardingForm.controls['countryCode'].setValue(user.countryCode);
        }
        this.onboardingForm.controls['contactNumber'].setValue(user.contactNumber);
        this.onboardingForm.controls['userName'].setValue(user.userName);
        this.onboardingForm.controls['userName'].disable();
        this.onboardingForm.controls['enabled'].setValue(user.enabled);
        const group = this.groups.find(e => e.id === user.groupId);
        if (group !== undefined) {
            this.onboardingForm.controls['groupId'].setValue(group);
            this.onboardingForm.controls['groupName'].setValue(group.name);
        }
        const roles: Role[] = [];
        user.roles.forEach(e1 => {
            const role = this.roles.find(e => e.authServiceRoleID === e1.authServiceRoleID);
            roles.push(role);
            this.rolesSelectionValidation(role, true);
        });
        this.onboardingForm.controls['authServiceRoleID'].setValue(roles);
        if (user.userStatus === 'PENDING_ENROLLMENT') {
            this.isEnrolled = false;
        } else if (user.userStatus === 'PENDING_APPROVAL') {
            this.isEnrolled = true;
            this.approvalRequired = true;
            this.onboardingForm.controls['groupId'].disable();
        } else if (user.userStatus === 'READY_FOR_ISSUANCE' || user.userStatus === 'IDENTITY_ISSUED') {
            this.isEnrolled = true;
            this.onboardingForm.controls['groupId'].disable();
        }
    }
    onKeyUp(event: KeyboardEvent) {
        this.userNameValidationError = false;
        this.userNameValidationNumberError = false;
        this.userNameSpaceValidationError = false;
        const length = this.onboardingForm.controls['userName'].value.length;
        if (
            //  this.onboardingForm.controls['userName'].value.charAt(0) === '.' ||
            this.onboardingForm.controls['userName'].value.charAt(0) === '_' ||
            //  this.onboardingForm.controls['userName'].value.charAt(length - 1) === '.' ||
            this.onboardingForm.controls['userName'].value.charAt(length - 1) === '_'
        ) {
            this.userNameValidationError = true;
            this.onboardingForm.controls['userName'].setErrors(Validators.pattern);
        }
        if (
            this.onboardingForm.controls['userName'].value.charAt(0) === ' ' ||
            this.onboardingForm.controls['userName'].value.charAt(length - 1) === ' '
        ) {
            this.userNameSpaceValidationError = true;
            this.onboardingForm.controls['userName'].setErrors(Validators.pattern);
        }
        if (
            this.onboardingForm.controls['userName'].value.charAt(0) >= '0' &&
            this.onboardingForm.controls['userName'].value.charAt(0) <= '9'
        ) {
            this.userNameValidationNumberError = true;
            this.onboardingForm.controls['userName'].setErrors(Validators.pattern);
        }
    }
    // Navigate to Back
    goBack() {
        this.router.navigate([this.data.routeFrom]);
    }
    goSearchPage() {
        if (this.editOnboarding) {
            this.router.navigate(['/dashboard']);
        } else {
            this.router.navigate(['/searchUser']);
        }
    }

    checkRole(event: MatOptionSelectionChange) {
        if (event.isUserInput) {
            const isSelected = event.source.selected;
            const role = event.source.value;
            this.rolesSelectionValidation(role, isSelected);
        }
    }
    rolesSelectionValidation(role: any, isSelected: boolean) {
        this.roles.forEach(element => {
            if (element.name === role.name) {
                if (element.name.toLowerCase() !== 'role_user') {
                    if (element.name.toLowerCase() === 'role_admin') {
                        this.roles.forEach(e => {
                            if (e.name.toLowerCase().includes('role_operator') || e.name.toLowerCase().includes('role_adjudicator')) {
                                if (isSelected) {
                                    e.disabled = true;
                                } else {
                                    e.disabled = false;
                                }
                            }
                        });
                    } else if (element.name.toLowerCase().includes('role_operator')) {
                        this.roles.forEach(e => {
                            if (
                                e.name.toLowerCase().includes('role_admin') ||
                                e.name.toLowerCase() === 'role_adjudicator' ||
                                (e.name.toLowerCase().includes('role_operator') && e.name !== element.name)
                            ) {
                                if (isSelected) {
                                    e.disabled = true;
                                } else {
                                    e.disabled = false;
                                }
                            }
                        });
                    } else if (element.name.toLowerCase() === 'role_adjudicator') {
                        this.roles.forEach(e => {
                            if (e.name.toLowerCase().includes('role_admin') || e.name.toLowerCase().includes('role_operator')) {
                                if (isSelected) {
                                    e.disabled = true;
                                } else {
                                    e.disabled = false;
                                }
                            }
                        });
                    }
                }
            }
        });
    }
    // Save Onboarding User & Error Handling
    saveOnboardingUser() {
        this.saveError = false;
        const user = new IdentityProviderUser();
        user.firstName = this.onboardingForm.controls['firstName'].value;
        user.lastName = this.onboardingForm.controls['lastName'].value;
        user.email = this.onboardingForm.controls['email'].value;
        if (
            this.onboardingForm.controls['countryCode'].value !== undefined &&
            this.onboardingForm.controls['countryCode'].value !== null &&
            this.onboardingForm.controls['contactNumber'].value !== undefined &&
            this.onboardingForm.controls['contactNumber'].value !== null
        ) {
            user.countryCode = this.onboardingForm.controls['countryCode'].value;
            user.contactNumber = this.onboardingForm.controls['contactNumber'].value;
        }
        user.userName = this.onboardingForm.controls['userName'].value;
        user.enabled = this.onboardingForm.controls['enabled'].value;
        // user.sendInvite = this.onboardingForm.controls['sendInvite'].value;
        // if (user.sendInvite) {
        // user.sendInviteVia = this.onboardingForm.controls['sendInviteVia'].value;
        // }
        const emailInvite = this.onboardingForm.controls['emailInvite'].value;
        const smsInvite = this.onboardingForm.controls['smsInvite'].value;
        if (emailInvite || smsInvite) {
            user.sendInvite = true;
            if (emailInvite) {
                user.sendInviteVia = 'Email';
            }
        }
        user.credentialType = this.onboardingForm.controls['credentialType'].value;
        user.credentialTemporary = this.onboardingForm.controls['credentialTemporary'].value;
        user.groupId = this.onboardingForm.controls['groupId'].value.id;
        user.groupName = this.onboardingForm.controls['groupId'].value.name;
        user.orgId = this.principal.getLoggedInUser().orgId;
        user.loggedInUserId = this.principal.getLoggedInUser().userId;
        user.loggedInUser = this.principal.getLoggedInUser().firstName + ' ' + this.principal.getLoggedInUser().lastName;
        const roles: string[] = [];
        this.onboardingForm.controls['authServiceRoleID'].value.forEach(e => {
            roles.push(e.authServiceRoleID);
        });
        user.authServiceRoleID = roles;
        if (this.editOnboarding) {
            user.userKLKId = this.editUser.userKLKId;
        }
        this.isLoadingResults = true;
        if (this.editOnboarding) {
            this.onboardingUserService.editOnboardingUser(user).subscribe(
                res => {
                    this.user = res;
                    this.saveOnboardingUserSuccess = true;
                    this.isLoadingResults = false;
                },
                err => {
                    this.handleErrors(err);
                }
            );
        } else {
            this.onboardingUserService.createOnboardingUser(user).subscribe(
                res => {
                    this.user = res;
                    this.saveOnboardingUserSuccess = true;
                    this.isLoadingResults = false;
                },
                err => {
                    this.handleErrors(err);
                }
            );
        }
    }
    handleErrors(err) {
        this.saveError = true;
        this.saveOnboardingUserSuccess = false;
        if (err.status === 409) {
            this.translateService.get('onboarding.messages.conflict').subscribe(msg => {
                this.errorMsg = msg;
            });
            window.scrollTo(0, 0);
        } else if (err.status === 500) {
            this.translateService.get('onboarding.messages.invalid').subscribe(msg => {
                this.errorMsg = msg;
            });
            window.scrollTo(0, 0);
        } else if (err.status === 503) {
            this.translateService.get('onboarding.messages.serverError').subscribe(msg => {
                this.errorMsg = msg;
            });
            window.scrollTo(0, 0);
        } else {
            this.translateService.get('onboarding.messages.failure').subscribe(msg => {
                this.errorMsg = msg;
            });
            window.scrollTo(0, 0);
        }
        this.isLoadingResults = false;
    }
    navigateToEnrollment() {
        const data = new Data();
        data.userFirstName = this.user.firstName;
        data.userId = this.user.id;
        data.userName = this.user.name;
        data.userLastName = this.user.lastName;
        data.userEmail = this.user.email;
        data.groupId = this.user.groupId;
        data.groupName = this.user.groupName;
        data.isEnrollmentInProgress = this.user.isEnrollmentInProgress;
        this.dataService.changeObj(data);
        this.router.navigate(['/register']);
    }
    navigateToApproval() {
        const data = new Data();
        data.userFirstName = this.user.firstName;
        data.userName = this.user.name;
        data.userLastName = this.user.lastName;
        data.groupId = this.user.groupId;
        data.groupName = this.user.groupName;
        data.isEnrolledUser = true;
        data.routeFrom = this.router.url;
        data.isFederated = this.user.isFederated;
        this.dataService.changeObj(data);
        this.router.navigate(['/approve-requests']);
    }

    issueUser() {
        this.searchUserService.getUserByName(this.user.name).subscribe(user => {
            if (user !== null) {
                if (this.data !== null) {
                    user.identityType = this.data.identityType;
                }
                this.identityDeviceDialogService.openAddIdentityDeviceDialog(user);
            }
        });
    }
}

function selectedItemsValidator(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value !== undefined && control.value !== null) {
        const val = control.value;
        if (val.length > 2) {
            return { selectedItemsValidator: true };
        }
    }
    return null;
}
export class IdentityProviderUser {
    firstName: string;
    lastName: string;
    email: string;
    enabled: boolean;
    sendInvite: boolean;
    sendInviteVia: string;
    countryCode: string;
    contactNumber: string;
    userName: string;
    credentialType: string;
    credentialValue: string;
    credentialTemporary: boolean;
    groupId: string;
    groupName: string;
    authServiceRoleID: Array<string>;
    roles: Role[] = [];
    phoneNumber;
    userKLKId: string;
    orgId: number;
    loggedInUserId: number;
    loggedInUser: string;
    userStatus: string;
}
export class Country {
    flagName;
    countryCode: string;
    constructor() {}
}

function customValidators(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value !== undefined && control.value !== null) {
        const val = control.value;
        const length = control.value.length;
        if (val.charAt(0) >= '0' && val.charAt(0) <= '9') {
            console.log('n');
            return { ValidationNumberError: true };
        }
        if (val.charAt(0) === ' ' || val.charAt(length - 1) === ' ') {
            return { SpaceValidationError: true };
        }
    }
    return null;
}
