import { Principal } from 'app/core';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Group } from '../workflow/group.model';
import { Role } from 'app/workflow/role.model';
import { IdentityProviderUser } from './onboarding-user.component';

@Injectable({
    providedIn: 'root'
})
export class OnboardingUserService {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name,
        uimsUrl: location.origin
        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });

    constructor(private httpClient: HttpClient, private principal: Principal) {}
    // Call REST APIs
    createOnboardingUser(onboardingUser): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.httpClient.post(
            `${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/idp-users`,
            onboardingUser,
            { headers: this.httpHeaders }
        );
    }

    // Call REST APIs
    updateUserRoles(onboardingUser): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.httpClient.post(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/idp-users/roles`,
            onboardingUser
        );
    }

    editOnboardingUser(onboardingUser): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.httpClient.put(
            `${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/idp-users`,
            onboardingUser,
            { headers: this.httpHeaders }
        );
    }
    // Call Groups
    getGroups(): Observable<Group[]> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.httpClient.get<Group[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/groups/groupsWithWorkflow`,
            { headers: this.httpHeaders }
        );
    }

    // Call for Roles
    getRoles(): Observable<string[]> {
        return this.httpClient.get<string[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/roles/names`,
            { headers: this.httpHeaders }
        );
    }
    // Call for Auth Servie Roles
    getRolesFromAuthService(): Observable<Role[]> {
        return this.httpClient.get<Role[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/roles/fromAuthService`,
            { headers: this.httpHeaders }
        );
    }

    getUserOnboardDetails(userName: string): Observable<IdentityProviderUser> {
        return this.httpClient.get<any>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/idp-users/${userName}/onboardUserDetails`
        );
    }
}
