import { OnboardingUserService } from './onboarding-user.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ONBOARDING_ROUTE } from './onboarding-user.route';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GatewaySharedModule } from 'app/shared';
import { OnboardingComponent } from './onboarding-user.component';

@NgModule({
    imports: [
        CommonModule,
        GatewaySharedModule,
        RouterModule.forChild([ONBOARDING_ROUTE]),
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
    ],
    declarations: [OnboardingComponent],
    providers: [ OnboardingUserService ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class OnBoardingUserModule {}
