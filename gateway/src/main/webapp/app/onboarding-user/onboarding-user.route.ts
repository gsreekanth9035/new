import { OnboardingComponent } from './onboarding-user.component';
import { Route } from '@angular/router';

export const ONBOARDING_ROUTE: Route = {
    path: 'onboard',
    component: OnboardingComponent,
    data: {
        authorities: [],
        pageTitle: 'onboarding.title'
    }
};
