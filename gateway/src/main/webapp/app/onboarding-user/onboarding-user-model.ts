export class OnboardingUser {
    firstName: string;
    lastName: string;
    email: string;
    contactNumber: string;
    enabled: boolean;
    sendInvite: boolean;
    sendInviteVia: string;
    userName: string;
    credentialType: string;
    credentialValue: string;
    credentialTemporary: boolean;
    groupId: string;
    groupName: string;
    authServiceRoleID: string[];
}
