import { OnInit, Component, ViewChild, AfterViewInit } from '@angular/core';
import { VisualDesignService } from '../visual-design.service';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material';
import { MatSort } from '@angular/material/sort';
import { VisualDesign } from '../visual-design.model';
import { DataService } from 'app/device-profile/data.service';
import { Principal } from 'app/core';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from 'app/shared/util/shared-service';

@Component({
    selector: 'jhi-manage-visual-design',
    templateUrl: './manage-visual-design.component.html',
    styleUrls: ['manage-visual-design.scss']
})
export class ManageVisualDesignComponent implements OnInit {
    displayedColumns: string[] = ['name', 'deviceType', 'type', 'actions'];
    data: VisualDesign[] = [];

    resultsLength = 0;
    pageSizeOptions = [5, 10, 15, 20];
    parentPageSizeOptions =  [5, 10, 15, 20];
    pageSize;

    message: any;
    visualDesignName: string;
    succesMsg = false;
    delFailure = false;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private router: Router,
        private visualDesignService: VisualDesignService,
        private dataService: DataService,
        private principal: Principal,
        private translateService: TranslateService,
        private sharedService: SharedService
    ) {}

    ngOnInit() {
        this.pageSize = this.pageSizeOptions[0];
        this.getVisualDesignList();
    }

    getVisualDesignList() {
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    if (this.paginator.pageSize === undefined) {
                        this.paginator.pageSize = this.pageSize;
                    }
                    return this.visualDesignService.getVisualDesignList(
                        this.principal.getLoggedInUser().organization,
                        this.sort.active,
                        this.sort.direction,
                        this.paginator.pageIndex,
                        this.paginator.pageSize
                    );
                }),
                map(data => {
                    this.resultsLength = data.totalElements;
                    if (data.totalElements > 0) {
                        this.pageSizeOptions = this.sharedService.getPageSizeOptionsBasedOnTotalElements(data.totalElements, this.parentPageSizeOptions);
                    }
                    return data.content;
                }),
                catchError(() => {
                    return observableOf([]);
                })
            )
            .subscribe(data => (this.data = data));
    }

    addVisualDesign() {
        this.router.navigate(['/visual-design/create-visual-design']);
    }
    editVisualDesign(visualDesignName: string) {
        this.dataService.changeObj(visualDesignName);
        this.router.navigate(['/visual-design/create-visual-design']);
    }
    deleteVD() {
        this.visualDesignService.deleteVisualTemplateByName(this.visualDesignName, this.principal.getLoggedInUser().organization).subscribe(
            result => {
                if (result.status === 204) {
                    this.getVisualDesignList();
                    this.translateService.get('visualDesign.messages.delSuccessMsg', { visualDesignName: `${this.visualDesignName}` }).subscribe(msg => {
                        window.scrollTo(0, 0);
                        this.succesMsg = true;
                        this.message = msg;
                    });
                }
            },
            error => {
                if (error.status === 409) {
                    this.translateService.get('visualDesign.messages.delFailureMsg', { visualDesignName: `${this.visualDesignName}` }).subscribe(msg => {
                        window.scrollTo(0, 0);
                        this.delFailure = true;
                        this.message = msg;
                    });
                }
            }
        );
    }
    deleteVisualDesign(name: string) {
        this.succesMsg = false;
        this.delFailure = false;
        this.visualDesignName = name;
    }
}
