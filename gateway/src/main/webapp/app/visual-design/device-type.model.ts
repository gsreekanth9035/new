export class DeviceType {
    id: number;
    name: string;
    visualApplicable: boolean;
    active: boolean;
    constructor() {}
}
