import { VisualDesignComponent } from './create/visual-design.component';
import { Route, Routes } from '@angular/router';
import { ManageVisualDesignComponent } from './manage/manage-visual-design.component';

export const VISUALDESIGN_ROUTES: Routes = [
    {
        path: 'visual-design',
        component: ManageVisualDesignComponent,
        data: {
            authorities: [],
            pageTitle: 'visualDesign.title',
        }
    },
    {
        path: 'visual-design/create-visual-design',
        component: VisualDesignComponent,
        data: {
            authorities: [],
            pageTitle: 'visualDesign.title',
        }
    }
];
