import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from '../material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VisualDesignComponent } from './create/visual-design.component';
import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SanityPipe } from './sanity.pipe';
import { VISUALDESIGN_ROUTES } from './visual-design.route';
import { ManageVisualDesignComponent } from './manage/manage-visual-design.component';
import { AngularSvgIconModule } from 'angular-svg-icon';

@NgModule({
  imports: [
      RouterModule.forChild(VISUALDESIGN_ROUTES),
      CommonModule,
      BrowserAnimationsModule,
      ReactiveFormsModule,
      HttpClientModule,
      MaterialModule,
      GatewaySharedModule,
      AngularSvgIconModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ VisualDesignComponent, ManageVisualDesignComponent, SanityPipe ]
})

export class VisualDesignModule { }
