import { OrganizationIdentity } from 'app/workflow/model/org-identity-model';
import { DeviceType } from './device-type.model';

export class VisualDesign {
    name: string;
    organizationIdentity: OrganizationIdentity;
    deviceTypes: DeviceType[] = [];
    frontDesign: string;
    backDesign: string;
    frontOrientation = 'Horizontal';
    backOrientation = 'Horizontal';
    attributeMapping: string;
    constructor() {}
}
