import { VisualDesign } from './visual-design.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Principal } from 'app/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Page } from './page.model';
import { DeviceType } from './device-type.model';

@Injectable({
    providedIn: 'root'
})
export class VisualDesignService {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name
        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });
    constructor(private httpClient: HttpClient, private principal: Principal) {}

    // Call REST APIs
    createVisualDesign(visualDesign: VisualDesign, organization): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.httpClient.post(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/visualtemplates`,
            visualDesign,
            { headers: this.httpHeaders, observe: 'response' }
        ); // .catch(this.errorHandler);
    }

    getVisualDesignList(organization, sort: string, order: string, page: number, size: number): Observable<Page> {
        return this.httpClient.get<Page>(
            `${
                window.location.origin
            }/usermanagement/api/v1/organizations/${organization}/visualtemplates?sort=${sort}&order=${order}&page=${page}&size=${size}`
        );
    }

    getVisulaDesignByName(visualTemplateName: string, organization): Observable<VisualDesign> {
        return this.httpClient.get<VisualDesign>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/visualtemplates/${visualTemplateName}`
        );
    }

    updateVisualTemplate(visualTemplateName: string, visualDesign: VisualDesign, organization): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.httpClient.put(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}/visualtemplates/${visualTemplateName}`,
            visualDesign,
            { headers: this.httpHeaders, observe: 'response' }
        );
    }

    deleteVisualTemplateByName(visualTemplateName: string, organization) {
        return this.httpClient.delete(
            `${window.location.origin}/usermanagement/api/v1/organizations/${organization}
    /visualtemplates/${visualTemplateName}`,
            { observe: 'response' }
        );
    }

    getVisualApplicableDeviceTypes(): Observable<Array<DeviceType>> {
        return this.httpClient.get<Array<DeviceType>>(`${window.location.origin}/usermanagement/api/v1/device-types/visual-applicable`);
    }

    errorHandler(error: HttpErrorResponse) {
        console.log(error);
        return Observable.throw('Data is not submitted');
    }
}
