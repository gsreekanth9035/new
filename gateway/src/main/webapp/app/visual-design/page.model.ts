export class Page {
    content = [];
    totalElements: number;
    last: boolean;
    totalPages: number;
    size: number;
    number: number;
    sort: any;
    first: boolean;
    numberOfElements: number;
    constructor() {}
}
