import { Component, OnInit, ViewChild, ElementRef, Input, AfterViewInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VisualDesign } from '../visual-design.model';
import { Router, ActivatedRoute } from '@angular/router';
import { VisualDesignService } from '../visual-design.service';
import { TranslateService } from '@ngx-translate/core';
import { DataService } from 'app/device-profile/data.service';
import { Principal } from 'app/core';
import { WorkflowService } from 'app/workflow/workflow.service';
import { OrganizationIdentity } from 'app/workflow/model/org-identity-model';
import { DeviceType } from '../device-type.model';

@Component({
    selector: 'jhi-visual-design',
    templateUrl: './visual-design.component.html',
    styleUrls: ['visual-design.scss']
})
export class VisualDesignComponent implements OnInit, AfterViewInit, OnDestroy {
    successMsg = false;
    errorMsg = false;
    visualDesignForm: FormGroup;
    frontDesignBase64String: string;
    backDesignBase64String: string;
    attributeMappingBase64String: string;
    frontCardError = true;
    backCardError = true;
    attributeCardError = true;
    public imagePathFront;
    imgURLFront: any;
    public imagePathBack;
    imgURLBack: any;
    editMode = false;
    visualDesign = new VisualDesign();
    message: string;
    devices: Array<DeviceType> = [];
    isLoadingResults = true;
    @ViewChild('frontDesignLabel') frontDesignLabel: ElementRef;
    @ViewChild('backDesignLabel') backDesignLabel: ElementRef;
    @ViewChild('attributeMapping') attributeMappingLabel: ElementRef;
    // get these values from backend
    visualDesignTypes: OrganizationIdentity[] = [];
    /**[
        { value: '', viewValue: 'Select' },
        { value: 'Driving_License', viewValue: 'Driving License' },
        { value: 'PIV_Card', viewValue: 'PIV Card' },
        { value: 'National_ID', viewValue: 'National ID' },
        { value: 'Health_ID', viewValue: 'Health ID' },
        { value: 'Student_ID', viewValue: 'Student ID' },
        { value: 'Employee_ID', viewValue: 'Employee ID' },
        { value: 'Police_ID', viewValue: 'Police ID' },
        { value: 'MLS_ID', viewValue: 'MLS ID' }
    ];  */

    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private visualDesignService: VisualDesignService,
        private route: ActivatedRoute,
        private translateService: TranslateService,
        private dataService: DataService,
        private principal: Principal,
        private workflowService: WorkflowService
    ) {}

    ngOnInit() {
        this.buildVisualDesignForm();
        this.workflowService.getOrgIssuingIdentityModels().subscribe(
            idModels => {
                this.visualDesignTypes = idModels;
                this.visualDesignService.getVisualApplicableDeviceTypes().subscribe(
                    deviceTypes => {
                        this.devices = deviceTypes;
                        const visualTemplateName = this.dataService.getCurrentObj();
                        if (visualTemplateName !== null) {
                            this.editMode = true;
                            this.visualDesignService
                                .getVisulaDesignByName(visualTemplateName, this.principal.getLoggedInUser().organization)
                                .subscribe(
                                    visualTemplate => {
                                        const visualDesign = new VisualDesign();
                                        visualDesign.name = visualTemplate.name;
                                        visualDesign.organizationIdentity = visualTemplate.organizationIdentity;
                                        visualDesign.frontOrientation = visualTemplate.frontOrientation;
                                        visualDesign.backOrientation = visualTemplate.backOrientation;
                                        this.visualDesign = visualDesign;
                                        this.visualDesignForm.get('name').setValue(this.visualDesign.name);
                                        this.visualDesignForm
                                            .get('type')
                                            .setValue(
                                                this.visualDesignTypes.filter(
                                                    idType =>
                                                        idType.identityTypeName === this.visualDesign.organizationIdentity.identityTypeName
                                                )[0]
                                            );
                                        const devices: string[] = [];
                                        this.devices.forEach(d => {
                                            visualTemplate.deviceTypes.forEach(dt => {
                                                if (dt.name === d.name) {
                                                    devices.push(d.name);
                                                }
                                            });
                                        });
                                        this.visualDesignForm.get('device').setValue(devices);
                                        this.visualDesignForm.get('frontOrientation').setValue(this.visualDesign.frontOrientation);
                                        this.visualDesignForm.get('backOrientation').setValue(this.visualDesign.backOrientation);
                                        this.imgURLFront = `data:image/svg+xml;base64,${visualTemplate.frontDesign}`;
                                        this.frontDesignBase64String = this.imgURLFront;
                                        this.imgURLBack = `data:image/svg+xml;base64,${visualTemplate.backDesign}`;
                                        this.backDesignBase64String = this.imgURLBack;
                                        this.attributeMappingBase64String = `data:application/json;base64,${
                                            visualTemplate.attributeMapping
                                        }`;
                                        this.backCardError = false;
                                        this.frontCardError = false;
                                        this.attributeCardError = false;
                                        // ${visualDesign.name}_
                                        this.frontDesignLabel.nativeElement.innerHTML = `frontFile.svg`;
                                        this.backDesignLabel.nativeElement.innerHTML = `backFile.svg`;
                                        this.attributeMappingLabel.nativeElement.innerHTML = `card_fields.json`;
                                        this.isLoadingResults = false;
                                    },
                                    err => {
                                        this.isLoadingResults = false;
                                    }
                                );
                        } else {
                            this.isLoadingResults = false;
                        }
                    },
                    err => {
                        this.isLoadingResults = false;
                    }
                );
            },
            err => {
                this.isLoadingResults = false;
            }
        );
    }
    ngAfterViewInit() {}
    buildVisualDesignForm() {
        this.visualDesignForm = this.formBuilder.group({
            name: ['', Validators.required],
            type: ['', Validators.required],
            device: ['', Validators.required],
            frontOrientation: [this.visualDesign.frontOrientation],
            backOrientation: [this.visualDesign.backOrientation],
            frontDesign: [''],
            backDesign: [''],
            attributeMapping: ['']
        });
    }
    saveVisualDesign() {
        this.errorMsg = false;
        this.successMsg = false;
        if (!this.visualDesignForm.valid || this.frontCardError || this.backCardError || this.attributeCardError) {
            return;
        }
        this.isLoadingResults = true;
        const visualDesign: VisualDesign = new VisualDesign();
        visualDesign.name = this.visualDesignForm.get('name').value;
        visualDesign.organizationIdentity = this.visualDesignForm.get('type').value;
        const deviceTypes: DeviceType[] = [];
        this.visualDesignForm.get('device').value.forEach((deviceType: string) => {
            const deviceProfile: DeviceType = new DeviceType();
            deviceProfile.name = deviceType;

            deviceTypes.push(deviceProfile);
        });
        visualDesign.deviceTypes = deviceTypes;
        visualDesign.frontOrientation = this.visualDesignForm.get('frontOrientation').value;
        visualDesign.backOrientation = this.visualDesignForm.get('backOrientation').value;
        visualDesign.frontDesign = this.frontDesignBase64String.split(',')[1];
        visualDesign.backDesign = this.backDesignBase64String.split(',')[1];
        visualDesign.attributeMapping = this.attributeMappingBase64String.split(',')[1];

        if (this.editMode) {
            this.visualDesignService
                .updateVisualTemplate(this.visualDesign.name, visualDesign, this.principal.getLoggedInUser().organization)
                .subscribe(
                    result => {
                        if (result.status === 201) {
                            this.translateService
                                .get('visualDesign.messages.updateSuccess', { visualDesignName: `${this.visualDesign.name}` })
                                .subscribe(message => {
                                    this.message = message;
                                    this.successMsg = true;
                                    this.errorMsg = false;
                                });
                        }
                        this.isLoadingResults = false;
                    },
                    error => {
                        this.isLoadingResults = false;
                        this.handleError(error);
                    }
                );
            return;
        } else {
            this.visualDesignService.createVisualDesign(visualDesign, this.principal.getLoggedInUser().organization).subscribe(
                response => {
                    if (response.status === 201) {
                        this.translateService.get('visualDesign.messages.success').subscribe(message => {
                            this.message = message;
                            this.successMsg = true;
                        });
                    }
                    this.isLoadingResults = false;
                },
                error => {
                    this.isLoadingResults = false;
                    this.handleError(error);
                }
            );
        }
    }
    handleError(error) {
        window.scrollTo(0, 0);
        if (error.status === 400) {
            this.translateService.get('visualDesign.messages.validationFailed', {visualTemplateName: this.visualDesignForm.get('name').value}).subscribe(message => {
                this.message = message;
            });
        } else if (error.status === 409) {
            this.translateService.get('visualDesign.messages.visualDesignExistsWithName', {visualTemplateName: this.visualDesignForm.get('name').value}).subscribe(message => {
                this.message = message;
            });
        } else if (error.status === 406) {
            this.translateService.get('visualDesign.messages.failure').subscribe(message => {
                this.message = message;
            });
        } else if (error.error.status === 500) {
            this.message = error.error.detail;
        } else {
            this.translateService.get('visualDesign.messages.failure').subscribe(message => {
                this.message = message;
            });
        }
        this.errorMsg = true;
    }

    onFrontDesignChange(event) {
        const fileValue = event.target.files[0];
        if (event.target.files.length > 0) {
            this.frontCardError = false;
        } else {
            this.frontCardError = true;
            document.getElementById('frontDesign').classList.add('hideFileName');
            if (this.editMode) {
                this.frontCardError = false;
            }
            return;
        }
        const reader = new FileReader();
        const [file] = event.target.files;
        reader.readAsDataURL(file);
        reader.onload = () => {
            this.frontDesignBase64String = reader.result.toString();
            this.visualDesignForm.controls['frontDesign'].setValue(fileValue ? fileValue.name : '');
            document.getElementById('frontDesign').classList.remove('hideFileName');
        };
    }

    onBackDesignChange(event) {
        const fileValue = event.target.files[0];
        if (event.target.files.length > 0) {
            this.backCardError = false;
        } else {
            this.backCardError = true;
            document.getElementById('backDesign').classList.add('hideFileName');
            if (this.editMode) {
                this.backCardError = false;
            }
            return;
        }
        const reader = new FileReader();
        const [file] = event.target.files;
        reader.readAsDataURL(file);
        reader.onload = () => {
            this.backDesignBase64String = reader.result.toString();
            this.visualDesignForm.controls['backDesign'].setValue(fileValue ? fileValue.name : '');
            document.getElementById('backDesign').classList.remove('hideFileName');
        };
    }

    onCardAttributeChange(event) {
        const fileValue = event.target.files[0];
        if (event.target.files.length > 0) {
            this.attributeCardError = false;
        } else {
            this.attributeCardError = true;
            document.getElementById('attributeMapping').classList.add('hideFileName');
            if (this.editMode) {
                this.attributeCardError = false;
            }
            return;
        }
        const reader = new FileReader();
        const [file] = event.target.files;
        reader.readAsDataURL(file);
        reader.onload = () => {
            this.attributeMappingBase64String = reader.result.toString();
            this.visualDesignForm.controls['attributeMapping'].setValue(fileValue ? fileValue.name : '');
            document.getElementById('attributeMapping').classList.remove('hideFileName');
        };
    }

    goHome() {
        this.router.navigate(['/visual-design']);
    }

    previewFrontDesign(files) {
        if (files.length === 0) {
            return;
        }

        const mimeType = files[0].type;
        if (mimeType.match(/image\/.*/) == null) {
            return;
        }

        const reader = new FileReader();
        this.imagePathFront = files;
        reader.readAsDataURL(files[0]);
        reader.onload = _event => {
            this.imgURLFront = reader.result;
        };
    }

    previewBackDesign(files) {
        if (files.length === 0) {
            return;
        }

        const mimeType = files[0].type;
        if (mimeType.match(/image\/.*/) == null) {
            return;
        }

        const reader = new FileReader();
        this.imagePathBack = files;
        reader.readAsDataURL(files[0]);
        reader.onload = _event => {
            this.imgURLBack = reader.result;
        };
    }

    ngOnDestroy() {
        this.dataService.changeObj(null);
    }
}
