import { ManageGroupsComponent } from './manage-groups.component';
import { Route, Routes } from '@angular/router';

export const MANAGE_GROUPS_ROUTES: Routes = [
    {
        path: 'groups',
        component: ManageGroupsComponent,
        data: {
            authorities: [],
            pageTitle: 'manageGroups.pageTitle'
        }
    }
];
