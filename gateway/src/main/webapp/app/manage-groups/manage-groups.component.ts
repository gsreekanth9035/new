import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ManageGroupsService } from './manage-groups.service';
import { Group } from 'app/workflow/group.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'jhi-manage-groups',
    templateUrl: './manage-groups.component.html'
})
export class ManageGroupsComponent implements OnInit {
    success = false;
    failure = false;
    groups: Group[];
    isLoadingResults = false;
    groupId: string;
    groupName: string;
    message = '';
    manageGroupsForm: FormGroup;
    editGroupsForm: FormGroup;
    errorMsg: string;
    updateGroup: Group;
    editMode: boolean;
    groupNameValidationError = false;
    editGroupNameValidationError = false;
    groupNameValidationNumberError = false;
    groupNameMaxlength = 30;
    groupNameMinlength = 2;
    @ViewChild('manageFormDirective') private manageGroupFormDirective: NgForm;
    constructor(
        private translateService: TranslateService,
        private router: Router,
        private formBuilder: FormBuilder,
        private manageGroupsService: ManageGroupsService
    ) {}

    ngOnInit() {
        this.manageGroupsForm = this.formBuilder.group({
            groupName: [
                '',
                [
                    Validators.required,
                    Validators.pattern('^[a-zA-Z0-9_ ]+$'),
                    Validators.minLength(this.groupNameMinlength),
                    Validators.maxLength(this.groupNameMaxlength)
                ]
            ]
        });
        this.editGroupsForm = this.formBuilder.group({
            groupedit: [
                '',
                [
                    Validators.required,
                    Validators.pattern('^[a-zA-Z0-9_ ]+$'),
                    Validators.minLength(this.groupNameMinlength),
                    Validators.maxLength(this.groupNameMaxlength)
                ]
            ]
        });

        this.manageGroupsService.getGroups().subscribe(groups => {
            this.groups = groups;
        });
    }
    onKeyUp(event: KeyboardEvent) {
        this.success = false;
        this.failure = false;
        this.groupNameValidationError = false;
        this.editGroupNameValidationError = false;
        this.groupNameValidationNumberError = false;
        if (
            this.manageGroupsForm.controls['groupName'].value.charAt(0) === ' ' ||
            this.manageGroupsForm.controls['groupName'].value.charAt(0) === '_'
        ) {
            this.manageGroupsForm.controls['groupName'].setErrors(Validators.pattern);
            this.groupNameValidationError = true;
        }
        if (
            this.manageGroupsForm.controls['groupName'].value.charAt(0) >= '0' &&
            this.manageGroupsForm.controls['groupName'].value.charAt(0) <= '9'
        ) {
            this.groupNameValidationNumberError = true;
            this.manageGroupsForm.controls['groupName'].setErrors(Validators.pattern);
        }
    }
    onKeyUpForEditGroupForm(event: KeyboardEvent) {
        this.success = false;
        this.failure = false;
        this.groupNameValidationError = false;
        this.editGroupNameValidationError = false;
        this.groupNameValidationNumberError = false;
        if (
            this.editGroupsForm.controls['groupedit'].value.charAt(0) === ' ' ||
            this.editGroupsForm.controls['groupedit'].value.charAt(0) === '_'
        ) {
            this.editGroupsForm.controls['groupedit'].setErrors(Validators.pattern);
            this.editGroupNameValidationError = true;
        }
        if (
            this.editGroupsForm.controls['groupedit'].value.charAt(0) >= '0' &&
            this.editGroupsForm.controls['groupedit'].value.charAt(0) <= '9'
        ) {
            this.groupNameValidationNumberError = true;
            this.editGroupsForm.controls['groupedit'].setErrors(Validators.pattern);
        }
    }
    hideMessages() {
        setTimeout(() => {
            this.success = false;
            this.failure = false;
        }, 10000);
    }
    clearForm() {
        this.manageGroupFormDirective.resetForm();
    }
    editGroupById() {
        const group = new Group();
        group.name = this.editGroupsForm.controls['groupedit'].value;
        group.name = group.name.trim();
        group.id = this.updateGroup.id;
        this.success = false;
        this.failure = false;
        this.manageGroupsService.updateGroup(group).subscribe(
            res => {
                if (res.status === 200 || res.status === 204) {
                    this.editMode = false;
                    this.success = true;
                    this.isLoadingResults = false;
                    this.translateService.get('manageGroups.messages.update').subscribe(msg => {
                        this.message = msg;
                        this.manageGroupsService.getGroups().subscribe(groups => {
                            this.groups = groups;
                        });
                    });
                    this.hideMessages();
                }
            },
            err => {
                this.success = false;
                this.handleErrors(err);
                this.hideMessages();
            }
        );
    }
    goBack() {
        this.editMode = false;
    }
    editGroup(group: Group) {
        this.editMode = true;
        this.updateGroup = group;
        this.success = false;
        this.failure = false;
        this.editGroupsForm.patchValue({
            groupedit: this.updateGroup.name
        });
        window.scrollTo(0, 0);
        this.clearForm();
    }
    saveGroup() {
        this.success = false;
        this.failure = false;
        const group = new Group();
        group.name = this.manageGroupsForm.controls['groupName'].value;
        group.name = group.name.trim();
        this.isLoadingResults = true;
        this.manageGroupsService.saveGroup(group).subscribe(
            res => {
                this.success = true;
                this.clearForm();
                this.isLoadingResults = false;
                this.translateService.get('manageGroups.messages.success').subscribe(msg => {
                    this.message = msg;
                    this.manageGroupsService.getGroups().subscribe(groups => {
                        this.groups = groups;
                    });
                });
                this.hideMessages();
            },
            err => {
                this.handleErrors(err);
                this.hideMessages();
            }
        );
    }
    handleErrors(err) {
        this.failure = true;
        if (err.status === 409) {
            this.translateService.get('manageGroups.messages.conflict').subscribe(msg => {
                this.errorMsg = msg;
            });
            window.scrollTo(0, 0);
        } else if (err.status === 500) {
            this.translateService.get('manageGroups.messages.invalid').subscribe(msg => {
                this.errorMsg = msg;
            });
            window.scrollTo(0, 0);
        } else if (err.status === 503) {
            this.translateService.get('manageGroups.messages.serverError').subscribe(msg => {
                this.errorMsg = msg;
            });
            window.scrollTo(0, 0);
        } else {
            this.translateService.get('manageGroups.messages.failure').subscribe(msg => {
                this.errorMsg = msg;
            });
            window.scrollTo(0, 0);
        }
        this.isLoadingResults = false;
    }
    delGroup() {
        this.success = false;
        this.failure = false;
        this.manageGroupsService.deleteGroupById(this.groupId).subscribe(
            res => {
                if (res.status === 200 || res.status === 204) {
                    this.success = true;
                    this.isLoadingResults = false;
                    this.translateService.get('manageGroups.messages.success2').subscribe(msg => {
                        this.message = msg;
                        this.manageGroupsService.getGroups().subscribe(groups => {
                            this.groups = groups;
                        });
                    });
                    this.hideMessages();
                }
                window.scrollTo(0, 0);
            },
            err => {
                this.handleError(err);
                this.hideMessages();
            }
        );
    }
    handleError(err) {
        if (err.status === 409) {
            this.failure = true;
            this.translateService.get('manageGroups.messages.conflict2').subscribe(msg => {
                this.errorMsg = msg;
            });
            window.scrollTo(0, 0);
        }
        if (err.status === 404) {
            this.failure = true;
            this.translateService.get('manageGroups.messages.deleteGroup').subscribe(msg => {
                this.errorMsg = msg;
            });
            window.scrollTo(0, 0);
        }
        this.isLoadingResults = false;
    }
    deleteGroup(group: Group) {
        this.groupId = group.id;
        this.groupName = group.name;
        this.success = false;
        this.failure = false;
    }
    goHome() {
        this.router.navigate(['/groups']);
    }
}
