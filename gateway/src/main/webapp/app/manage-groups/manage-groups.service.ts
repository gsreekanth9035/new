import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Principal } from 'app/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Group } from 'app/workflow/group.model';

@Injectable({
    providedIn: 'root'
})
export class ManageGroupsService {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name
        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });

    constructor(private httpClient: HttpClient, private principal: Principal) {}
    // Call Groups
    getGroups(): Observable<Group[]> {
        return this.httpClient.get<Group[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/groups`
        );
    }
    updateGroup(group: Group): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.httpClient.put(
            `${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/groups`, 
            group,
            { headers: this.httpHeaders, observe: 'response' }
        );
    }
    saveGroup(group: Group): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');
        return this.httpClient.post(
            `${window.location.origin}/usermanagement/api/v1/organizations/${
                this.principal.getLoggedInUser().organization
            }/groups`,
            group,
            { headers: this.httpHeaders }
        );
    }
    deleteGroupById(groupId: string): Observable<any> {
        return this.httpClient.delete(
            `${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/groups/${groupId}`, { observe: 'response' }
        );
    }
    errorHandler(error: HttpErrorResponse) {
        return Observable.throw('Error: Group Mapping failed');
    }
}
