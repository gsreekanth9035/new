import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from '../material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MANAGE_GROUPS_ROUTES } from './manage-groups.route';
import { ManageGroupsComponent } from './manage-groups.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AngularSvgIconModule } from 'angular-svg-icon';

@NgModule({
    imports: [
        RouterModule.forChild(MANAGE_GROUPS_ROUTES),
        CommonModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MaterialModule,
        GatewaySharedModule,
        AngularSvgIconModule
    ],
    declarations: [ManageGroupsComponent]
})
export class ManageGroupsModule {}
