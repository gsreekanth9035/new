import { MaterialModule } from './../material/material.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MONITOR_ROUTES } from './monitor.route';
import { MonitorComponent } from './monitor.component';
import { GatewaySharedModule } from '../shared';
import { MonitorService } from './monitor.service';

@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        GatewaySharedModule,
        RouterModule.forChild(MONITOR_ROUTES),
        MaterialModule,
        MatProgressSpinnerModule,
        AngularSvgIconModule,
        HttpClientModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations: [MonitorComponent],
    providers: [MonitorService]
})
export class MonitorModule {}
