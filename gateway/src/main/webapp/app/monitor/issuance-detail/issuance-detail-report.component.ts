import { Component, OnInit, OnDestroy, LOCALE_ID, Inject, ViewChild } from '@angular/core';
import { AbstractControl, ValidatorFn, FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Start_Date, End_Date, Organization } from '../reports-constants';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { PagingSpec, GenerateReportRequestBody, MonitorService } from '../monitor.service';
import { MatSort, MatPaginator, MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'app/device-profile/data.service';
import { DatePipe, formatDate } from '@angular/common';
import { Principal } from 'app/core';
import { ReportFilters, Report, ReportContent } from '../model/report-type.model';
import * as _moment from 'moment';
import { merge, of as observableOf } from 'rxjs';
import { COMPLIANCE_RPT_GROUP } from '../reports/reports.component';
import { SharedService } from 'app/shared/util/shared-service';
import { AppDateAdapter, APP_DATE_FORMATS } from 'app/shared/util/date.adapter';

const moment = _moment;
@Component({
    selector: 'jhi-issuance-report',
    templateUrl: './issuance-detail-report.component.html',
    styleUrls: ['issuance-detail-report.scss'],
    providers: [{ provide: DateAdapter, useClass: AppDateAdapter }, { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }]
})
export class IssuanceDetailReportComponent implements OnInit, OnDestroy {
    isLoadingResults = false;
    authorities: string[] = [];
    userName: string;
    orgName: string;
    maxDate = new Date();
    showToolTip = false;
    report: Report;
    reportSearchInterval: number;
    startDateFormat: string;
    endDateFormat: string;
    startDateFilterValue = null;
    endDateFilterValue = null;
    // used in request body
    reportFiltersObj: ReportFilters[] = [];
    generateDataListReportReqBody: GenerateReportRequestBody;
    exportFileTypes: Array<string> = [];
    userStatusForSearch: Array<any> = [];
    deviceType: Array<any> = [];
    filterForm: FormGroup;
    reportFilters: ReportFilters[] = [];
    isAdvancedSelected = false;

    data: ReportContent[] = [];
    resultsLength = 0;
    pageSize = 50;
    browserLanguage;
    // 'index',
    displayedColumns: string[] = [
        'username',
        'fullName',
        'issuedDate',
        'issuedBy',
        'issuedStatus',
        'issuedStatusReason',
        'deviceType',
        'uniqueIdentifier',
        'appType',
        'status'
    ];
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    pageSizeOptions = [50, 75, 100, 150];
    parentPageSizeOptions = [50, 75, 100, 150];
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private principal: Principal,
        private dataService: DataService,
        private monitorService: MonitorService,
        private datePipe: DatePipe,
        @Inject(LOCALE_ID) private locale: string,
        private formBuilder: FormBuilder,
        private sharedService: SharedService
    ) {
        this.browserLanguage = navigator.language;
        this.orgName = this.principal.getLoggedInUser().organization;
        this.userName = this.principal.getLoggedInUser().username;
    }
    ngOnInit() {
        this.isLoadingResults = true;
        this.exportFileTypes = ['CSV', 'PDF'];
        this.userStatusForSearch = [
            { displayName: 'None' },
            { displayName: 'Pending Activation', value: 'Pending Activation' },
            { displayName: 'Active', value: 'Active' },
            { displayName: 'OnHold', value: 'OnHold' },
            { displayName: 'Suspended', value: 'Suspended' },
            { displayName: 'Revoked', value: 'Revoked' },
            { displayName: 'Removed', value: 'Removed' },
            { displayName: 'Expired', value: 'Expired' }
        ];
        this.deviceType = [
            { displayName: 'None    ' },
            { displayName: 'Plastic Card', value: 'Plastic Card' },
            { displayName: 'Idemia', value: 'IDEMIA' },
            { displayName: 'Smart Card', value: 'Smart Card' }
        ];
        this.report = this.dataService.getCurrentObj();
        if (this.report === null) {
            this.router.navigate(['/reports']);
            return;
        }
        this.reportSearchInterval = this.report.reportGroup.reportSearchInterval;
        this.authorities = this.principal.getLoggedInUser().authorities;
        this.monitorService.getReportFilters(this.orgName, this.userName, true, this.report.id).subscribe(
            res => {
                this.reportFilters = res.body;

                this.filterForm = this.formBuilder.group({});
                res.body.forEach(e => {
                    if (e.displayName !== '') {
                        e.validationObj = JSON.parse(e.validation);
                        this.filterForm.addControl(e.name, this.addFormControl(e.validationObj.required));
                        if (e.name === Start_Date) {
                            this.startDateFormat = e.validationObj.format;
                        } else if (e.name === End_Date) {
                            this.endDateFormat = e.validationObj.format;
                        }
                    }
                });
                let startDateControl: AbstractControl;
                let endDateControl: AbstractControl;
                Object.keys(this.filterForm.controls).forEach(fieldName => {
                    if (fieldName === Start_Date) {
                        startDateControl = this.filterForm.controls[fieldName];
                    } else if (fieldName === End_Date) {
                        endDateControl = this.filterForm.controls[fieldName];
                    }
                });
                if (startDateControl !== undefined && endDateControl !== undefined) {
                    startDateControl.setValidators([Validators.required, dateValidator(endDateControl, Start_Date)]);
                    endDateControl.setValidators([Validators.required, dateValidator(startDateControl, End_Date)]);
                    startDateControl.updateValueAndValidity();
                    endDateControl.updateValueAndValidity();
                }
                this.getEnrollmentReports();
            },
            err => {
                this.isLoadingResults = false;
            }
        );
    }
    addFormControl(required: string): FormControl {
        let isRequired = false;
        if (required === '1') {
            isRequired = true;
        }
        if (isRequired) {
            return new FormControl('', Validators.required);
        }
        return new FormControl('');
    }

    getEnrollmentReports() {
        this.generateDataListReportReqBody = this.getInitialReportRequestBody();
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    if (this.paginator.pageSize === undefined) {
                        this.paginator.pageSize = this.pageSize;
                    }
                    this.generateDataListReportReqBody.pagingSpec = new PagingSpec(
                        this.sort.active,
                        this.sort.direction,
                        this.paginator.pageIndex,
                        this.paginator.pageSize
                    );
                    return this.monitorService.generateReport(
                        this.orgName,
                        this.userName,
                        this.report.id,
                        this.generateDataListReportReqBody
                    );
                }),
                map(data => {
                    this.resultsLength = data.body.totalElements;
                    this.isLoadingResults = false;
                    if (data.body.totalElements > 0) {
                        this.pageSizeOptions = this.sharedService.getPageSizeOptionsBasedOnTotalElements(
                            data.body.totalElements,
                            this.parentPageSizeOptions
                        );
                    }
                    return data.body.content;
                }),
                catchError(() => {
                    this.isLoadingResults = false;
                    return observableOf([]);
                })
            )
            .subscribe(data => (this.data = data));
    }
    getInitialReportRequestBody(): GenerateReportRequestBody {
        const generateReportReqBody = new GenerateReportRequestBody();

        this.endDateFilterValue = formatDate(new Date(), this.endDateFormat, this.locale);
        // set date value in ui
        this.filterForm.controls[End_Date].setValue(new Date());
        const strtDate = new Date(new Date().getTime() - this.reportSearchInterval * 24 * 60 * 60 * 1000);
        this.startDateFilterValue = formatDate(strtDate, this.startDateFormat, this.locale);
        this.filterForm.controls[Start_Date].setValue(new Date(strtDate));
        const startDaeFilter = new ReportFilters();
        startDaeFilter.key = Start_Date;
        startDaeFilter.value = this.startDateFilterValue;
        const endDateFilter = new ReportFilters();
        endDateFilter.key = End_Date;
        endDateFilter.value = this.endDateFilterValue;
        const orgFilter = new ReportFilters();
        orgFilter.key = Organization;
        orgFilter.value = this.orgName;
        if (this.reportFilters.find(e => e.name === Organization)) {
            this.reportFiltersObj.push(startDaeFilter, endDateFilter, orgFilter);
        } else {
            this.reportFiltersObj.push(startDaeFilter, endDateFilter);
        }
        generateReportReqBody.fileNamePrefix = '';
        // generateReportReqBody.description = ''; // optional field
        generateReportReqBody.reportLanguage = this.browserLanguage;
        generateReportReqBody.reportId = this.report.reportGroupId;
        generateReportReqBody.reportFilters = this.reportFiltersObj;
        return generateReportReqBody;
    }
    onApplyFilters() {
        this.paginator.pageIndex = 0;
        const generateReportReqBody = new GenerateReportRequestBody();
        const reportFilters: ReportFilters[] = [];
        // add filters to req body
        Object.keys(this.filterForm.controls).forEach(fieldName => {
            if (
                this.filterForm.controls[fieldName].value !== '' &&
                this.filterForm.controls[fieldName].value !== undefined &&
                this.filterForm.controls[fieldName].value.length !== 0
            ) {
                const filter = new ReportFilters();
                filter.key = fieldName;
                if (fieldName === Start_Date) {
                    filter.value = this.startDateFilterValue;
                } else if (fieldName === End_Date) {
                    filter.value = this.endDateFilterValue;
                } else {
                    if (this.reportFilters.find(e => e.name === fieldName).validationObj.type === 'STRING') {
                        filter.value = this.filterForm.controls[fieldName].value;
                    } else if (this.reportFilters.find(e => e.name === fieldName).validationObj.type === 'JSON') {
                        const listObj = [];
                        this.filterForm.controls[fieldName].value.forEach(e => {
                            listObj.push(e);
                        });
                        filter.value = listObj;
                    }
                }
                reportFilters.push(filter);
            }
        });
        const orgFilter = new ReportFilters();
        orgFilter.key = Organization;
        orgFilter.value = this.orgName;
        if (this.reportFilters.find(e => e.name === Organization)) {
            reportFilters.push(orgFilter);
        }
        generateReportReqBody.reportFilters = reportFilters;
        generateReportReqBody.pagingSpec = new PagingSpec(
            this.sort.active,
            this.sort.direction,
            this.paginator.pageIndex,
            this.paginator.pageSize
        );
        this.generateDataListReportReqBody = generateReportReqBody;
        this.monitorService.generateReport(this.orgName, this.userName, this.report.id, generateReportReqBody).subscribe(result => {
            this.resultsLength = result.body.totalElements;
            this.data = result.body.content;
        });
    }
    onClickOfAdvanced() {
        this.isAdvancedSelected = true;
        // show the filters now
    }
    onClickOfBasic() {
        this.generateDataListReportReqBody = this.getInitialReportRequestBody();
        this.isAdvancedSelected = false;
        this.generateDataListReportReqBody.pagingSpec = new PagingSpec(
            this.sort.active,
            this.sort.direction,
            this.paginator.pageIndex,
            this.paginator.pageSize
        );
        this.monitorService
            .generateReport(this.orgName, this.userName, this.report.id, this.generateDataListReportReqBody)
            .subscribe(result => {
                this.resultsLength = result.body.totalElements;
                this.data = result.body.content;
            });
    }
    onClick(uri: string) {
        this.router.navigate([uri]);
    }
    onChangeDate(dateFilter, event: MatDatepickerInputEvent<Date>) {
        if (event.value !== null) {
            if (dateFilter === Start_Date) {
                const date1 = moment(event.value).add({ hours: 0, minutes: 0, seconds: 0 });
                const date = date1.toDate();
                this.startDateFilterValue = formatDate(date, this.startDateFormat, this.locale);
            }
            if (dateFilter === End_Date) {
                const date2 = moment(event.value).add({ hours: 23, minutes: 59, seconds: 59 });
                const selectedEndDate = date2.toDate();
                this.endDateFilterValue = formatDate(selectedEndDate, this.endDateFormat, this.locale);
            }
        } else {
            //  this.filterForm.controls[dateFilter].setValue('');
        }
    }
    onClickOfExport(fileType) {
        this.isLoadingResults = true;
        const generateReportReqBody = new GenerateReportRequestBody();
        const reportFilters: ReportFilters[] = [];
        Object.keys(this.filterForm.controls).forEach(fieldName => {
            if (
                this.filterForm.controls[fieldName].value !== '' &&
                this.filterForm.controls[fieldName].value !== undefined &&
                this.filterForm.controls[fieldName].value.length !== 0
            ) {
                const filter = new ReportFilters();
                filter.key = fieldName;
                if (fieldName === Start_Date) {
                    filter.value = this.startDateFilterValue;
                } else if (fieldName === End_Date) {
                    filter.value = this.endDateFilterValue;
                } else {
                    if (this.reportFilters.find(e => e.name === fieldName).validationObj.type === 'STRING') {
                        filter.value = this.filterForm.controls[fieldName].value;
                    } else if (this.reportFilters.find(e => e.name === fieldName).validationObj.type === 'JSON') {
                        const listObj = [];
                        this.filterForm.controls[fieldName].value.forEach(e => {
                            listObj.push(e);
                        });
                        filter.value = listObj;
                    }
                }
                reportFilters.push(filter);
            }
        });
        const orgFilter = new ReportFilters();
        orgFilter.key = Organization;
        orgFilter.value = this.orgName;
        if (this.reportFilters.find(e => e.name === Organization)) {
            reportFilters.push(orgFilter);
        }
        generateReportReqBody.fileNamePrefix = 'All IssuanceDetails';
        generateReportReqBody.reportFilters = reportFilters;
        // generateReportReqBody.reportLanguage = this.browserLanguage;
        generateReportReqBody.reportLocale = this.browserLanguage;
        if (fileType === 'CSV') {
            generateReportReqBody.exportFormat = 'CSV';
        } else if (fileType === 'PDF') {
            generateReportReqBody.exportFormat = 'PDF';
        }
        generateReportReqBody.reportId = this.report.id;
        this.monitorService.downloadReport(generateReportReqBody, this.orgName, this.userName).subscribe(
            res => {
                const filename = res.headers.get('filename');
                const blob = new Blob([res.body], { type: 'application/text' });
                const downloadURL = window.URL.createObjectURL(blob);
                const link = document.createElement('a');
                link.href = downloadURL;
                link.download = filename;
                link.click();
                this.isLoadingResults = false;
            },
            err => {
                this.isLoadingResults = false;
            }
        );
    }
    ngOnDestroy() {
        this.dataService.changeObj(null);
    }
}
export function dateValidator(otherDateControl: AbstractControl, dateFilterName): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        if (control.value !== undefined && control.value !== null) {
            const value = control.value;
            if (dateFilterName === Start_Date) {
                if (new Date(value) > new Date(otherDateControl.value)) {
                    return { dateValidator: true };
                } else {
                    if (!otherDateControl.hasError('required')) {
                        otherDateControl.setErrors(null);
                    }
                }
            } else if (dateFilterName === End_Date) {
                if (new Date(value) < new Date(otherDateControl.value)) {
                    return { dateValidator: true };
                } else {
                    if (!otherDateControl.hasError('required')) {
                        otherDateControl.setErrors(null);
                    }
                }
            }
        }
        return null;
    };
}
