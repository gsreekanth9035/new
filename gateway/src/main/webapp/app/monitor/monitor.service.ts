import { Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ReportType, ReportGroup, Report, GenerateReportResponse, ReportFilters } from './model/report-type.model';
import { Observable } from 'rxjs';
import { Page } from 'app/visual-design/page.model';

@Inject({
    providedIn: 'root'
})
export class MonitorService {
    url: string;
    constructor(private http: HttpClient) {
        this.url = `${window.location.origin}/reportsmanagement`;
    }
    getReportTypes(organizationName: string, userName: string, activeOnly: boolean): Observable<HttpResponse<Array<ReportType>>> {
        const auth = 'Basic auth';
        const httpHeaders = new HttpHeaders({
            'Content-type': 'application/json',
            organization: organizationName,
            user: userName,
            version: '1',
            // 'Authorization': auth,
            activeOnly: String(activeOnly)
        });
        return this.http.get<ReportType[]>(`${this.url}/config/reporttype`, { headers: httpHeaders, observe: 'response' });
    }
    getReports(
        organizationName: string,
        userName: string,
        activeOnly: boolean,
        reportTypeId: number,
        reportGroupName: string
    ): Observable<HttpResponse<Report[]>> {
        const httpHeaders = new HttpHeaders({
            'Content-type': 'application/json',
            organization: organizationName,
            user: userName,
            version: '1',
            // 'Authorization': auth,
            // 'activeOnly': String(activeOnly),
            reportTypeId: String(reportTypeId),
            reportGroupName: String(reportGroupName)
        });
        return this.http.get<Report[]>(`${this.url}/config/report`, { headers: httpHeaders, observe: 'response' });
    }
    getReportFilters(
        organizationName: string,
        userName: string,
        activeOnly: boolean,
        reportId: number
    ): Observable<HttpResponse<ReportFilters[]>> {
        const httpHeaders = new HttpHeaders({
            'Content-type': 'application/json',
            organization: organizationName,
            user: userName,
            version: '1',
            // 'Authorization': auth,
            activeOnly: String(activeOnly),
            reportId: String(reportId)
        });
        return this.http.get<ReportFilters[]>(`${this.url}/config/reportfilters.`, { headers: httpHeaders, observe: 'response' });
    }
    generateReport(
        organizationName: string,
        userName: string,
        reportId: number,
        requestBody: GenerateReportRequestBody
    ): Observable<HttpResponse<Page>> {
        const httpHeaders = new HttpHeaders({
            'Content-type': 'application/json',
            organization: organizationName,
            user: userName,
            version: '1'
            // 'Authorization': auth,
        });
        requestBody.reportId = reportId;
        return this.http.post<GenerateReportResponse>(`${this.url}/report/generate`, requestBody, {
            headers: httpHeaders,
            observe: 'response'
        });
    }
    downloadReport(requestBody, organizationName: string, userName: string): Observable<any> {
        const httpHeaders = new HttpHeaders({
            'Content-type': 'application/json',
            organization: organizationName,
            user: userName,
            version: '1'
            // 'Authorization': auth,
        });
        return this.http.post(`${this.url}/report/generate/download`, requestBody, {
            headers: httpHeaders,
            observe: 'response',
            responseType: 'blob'
        });
    }
}
export class GenerateReportRequestBody {
    fileNamePrefix: string;
    description: string;
    reportLanguage: string;
    reportLocale: string;
    reportId: number;
    exportFormat: string;
    pagingSpec: PagingSpec;
    reportFilters: ReportFilters[] = [];
}
export class PagingSpec {
    sortBy: string;
    orderBy: string;
    page: number;
    size: number;
    constructor(sortBy, orderBy, page, size) {
        this.sortBy = sortBy;
        this.orderBy = orderBy;
        this.page = page;
        this.size = size;
    }
}
