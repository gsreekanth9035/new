import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService, Data } from '../../device-profile/data.service';
import { Principal } from '../../core';

@Component({
    selector: 'jhi-issued-devices',
    templateUrl: './issued-devices.component.html',
    styles: []
})
export class IssuedDeviceComponent implements OnInit {
    index = 0;
    authorities: string[] = [];

    constructor(private router: Router, private route: ActivatedRoute,
        private principal: Principal, private dataService: DataService) {
    }

    ngOnInit() {
        this.authorities = this.principal.getLoggedInUser().authorities;
    }

    enrollUser(userName: string, userFirstName: string) {
        const data = new Data();
        data.userFirstName = userFirstName;
        data.userLastName = '';
        data.userName = userName;
        this.dataService.changeObj(data);
        this.router.navigate(['/register']);
    }

    onClick(uri: string) {
       // alert(uri);
        this.router.navigate([uri]);
        // this.router.navigate([uri]);
    }
}
