import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { REPORTS_ROUTES } from './reports.route';
import { GatewaySharedModule } from '../../shared';
import { MaterialModule } from '../../material/material.module';
import { ReportsComponent } from './reports.component';
import { EnrollmentReportComponent } from '../enrollment/enrollment-report.component';
import { IssuedDeviceComponent } from '../issued-devices/issued-devices.component';
import { MonitorService } from '../monitor.service';
import { AuthenticationReportComponent } from '../authentication/authentication-report-component';
import { IssuanceDetailReportComponent } from '../issuance-detail/issuance-detail-report.component';

@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        GatewaySharedModule,
        RouterModule.forChild(REPORTS_ROUTES),
        MaterialModule,
        AngularSvgIconModule,
        HttpClientModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations: [
        ReportsComponent,
        EnrollmentReportComponent,
        IssuanceDetailReportComponent,
        IssuedDeviceComponent,
        AuthenticationReportComponent
    ],
    providers: [MonitorService]
})
export class ReportsModule {}
