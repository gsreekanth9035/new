import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService, Data } from '../../device-profile/data.service';
import { Principal } from '../../core';
import { MonitorService } from '../monitor.service';
import { Report, ReportType } from '../model/report-type.model';
import { MatSort } from '@angular/material';

export const DATA_LIST = 'Data list';
export const COMPLIANCE_RPT_GROUP = 'COMPLIANCE_RPT_GROUP';
@Component({
    selector: 'jhi-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['reports.scss']
})
export class ReportsComponent implements OnInit {
    index = 0;
    authorities: string[] = [];
    userName: string;
    orgName: string;
    displayedColumns: string[] = ['index', 'reportName', 'description'];
    data: Report[] = [];

    dataListReportType: ReportType;

    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private principal: Principal,
        private dataService: DataService,
        private monitorService: MonitorService
    ) {
        this.orgName = this.principal.getLoggedInUser().organization;
        this.userName = this.principal.getLoggedInUser().username;
    }

    ngOnInit() {
        this.authorities = this.principal.getLoggedInUser().authorities;
        this.monitorService.getReportTypes(this.orgName, this.userName, true).subscribe(res => {
            if (res.body !== null) {
                this.dataListReportType = res.body[0];
                if (this.dataListReportType !== undefined) {
                    this.getReports();
                }
            }
        });
    }
    getReports() {
        this.monitorService
            .getReports(this.orgName, this.userName, true, this.dataListReportType.id, COMPLIANCE_RPT_GROUP)
            .subscribe(reportGroup => {
                if (reportGroup.body.length !== 0) {
                    this.data = reportGroup.body;
                }
            });
    }
    onClickOfReport(row: Report) {
        this.dataService.changeObj(row);
        this.router.navigate([`../${row.name.toLowerCase().replace(/_/g, '')}`], { relativeTo: this.activatedRoute });
    }

    enrollUser(userName: string, userFirstName: string) {
        const data = new Data();
        data.userFirstName = userFirstName;
        data.userLastName = '';
        data.userName = userName;
        this.dataService.changeObj(data);
        this.router.navigate(['/register']);
    }

    onClick(uri: string) {
        this.router.navigate([uri]);
    }
}
