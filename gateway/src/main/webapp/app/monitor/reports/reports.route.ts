import { Route, Routes } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { EnrollmentReportComponent } from '../enrollment/enrollment-report.component';
import { IssuanceDetailReportComponent } from '../issuance-detail/issuance-detail-report.component';
import { IssuedDeviceComponent } from '../issued-devices/issued-devices.component';
import { AuthenticationReportComponent } from '../authentication/authentication-report-component';

export const REPORTS_ROUTES: Routes = [
    {
        path: 'reports',
        component: ReportsComponent,
        data: {
            authorities: [],
            pageTitle: 'monitor.title'
        }
    },
    {
        path: 'enrollmentreport',
        component: EnrollmentReportComponent,
        data: {
            authorities: [],
            pageTitle: 'monitor.title'
        }
    },
    {
        path: 'authenticationreport',
        component: AuthenticationReportComponent,
        data: {
            authorities: [],
            pageTitle: 'monitor.title'
        }
    },
    {
        path: 'issuancedetailsreport',
        component: IssuanceDetailReportComponent,
        data: {
            authorities: [],
            pageTitle: 'monitor.title'
        }
    },
    {
        path: 'issuedDevicesReport',
        component: IssuedDeviceComponent,
        data: {
            authorities: [],
            pageTitle: 'monitor.title'
        }
    }
];
