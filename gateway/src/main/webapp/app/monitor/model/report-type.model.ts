import { User } from 'app/enroll/user.model';
import { Page } from 'app/visual-design/page.model';

export class ReportType {
    id: number;
    name: string;
    description: string;
    organization: string;
    active: boolean;
    // "createdBy": "ADMIN",
    // "lastModifiedBy": "ADMIN",
    // "createdDate": "2020-07-08T07:16:02Z",
    // "lastModifiedDate": "2020-07-08T07:16:02Z",
    // "baseOnlineLocation": "C:/meetidentity/reportsoutput/datalist/online",
    // "baseArchiveLocation": "C:/meetidentity/reportsoutput/datalist/archive",
    // "baseTemplatesLocation": ""
    // version: number;
    constructor() {}
}
export class ReportGroup {
    id: number;
    name: string;
    organizationName: string;
    // version: number;
    active: boolean;
    description: string;
    displayName: string;
    reportTypeId: number;
    reportOnlineAvailableTime: number;
    reportSearchInterval: number;
    //  "reportOnlineTime": 90,
}
export class Report {
    id: number;
    name: string;
    displayName: string;
    description: string;
    organizationName: string;
    reportVersion: number;
    active: boolean;
    reportGroupId: number;
    reportGroup: ReportGroup;
    // "folerLocation": "enrollment",
    // "mainTemplateName": "maintemplate.jasper",
}

export class ReportFilters {
    name: string;
    displayName: string;
    validation: any;
    validationObj: ReportFilterValidation;
    key: string;
    value: any;
    constructor() {}
}
export class ReportFilterValidation {
    required: string;
    type: string;
    format: string;
}
export class GenerateReportResponse extends Page {
    folderLocation: string;
    generatedStatus: string;
    reportStatus: string;
    reportStatusMessage: string;
}
export class ReportContent {
    id: number;
    name: string;
    referenceName: string;
    status: string;
    organizationName: string;
    approvalStatus: string;
    approvedBy: string;
    approvedDate: Date;
    reason: string;
    enrolledBy: string;
    enrollmentStartDate: Date;
    enrollmentEndDate: Date;
}
