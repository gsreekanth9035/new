import { Route, Routes } from '@angular/router';

import { MonitorComponent } from '.';
import { ReportsComponent } from './reports/reports.component';

export const MONITOR_ROUTES: Routes = [{
    path: 'monitoring',
    component: MonitorComponent,
    data: {
        authorities: [],
        pageTitle: 'monitor.title'
    }
}];
