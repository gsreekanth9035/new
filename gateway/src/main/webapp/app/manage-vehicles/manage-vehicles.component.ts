import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ManageVehiclesService, UserIdNamePair } from './manage-vehicles.service';

@Component({
    selector: 'jhi-manage-vehicles',
    templateUrl: './manage-vehicles.component.html'
})

export class ManageVehiclesComponent implements OnInit {
    saveSuccess = false;
    saveFailure = false;

    userIdNamePairs: UserIdNamePair[];
    manageVehiclesForm: FormGroup;

    constructor(private router: Router, private formBuilder: FormBuilder, private manageVehiclesService: ManageVehiclesService) {}

    ngOnInit() {
        this.manageVehiclesForm = this.formBuilder.group({
            vin: ['', Validators.required],
            userId: ['', Validators.required]
        });

        this.manageVehiclesService.getUserIdNamePairs().subscribe(userIdNames => {
            this.userIdNamePairs = userIdNames;
        });

    }

    assignVehicleToUser() {
        if (!this.manageVehiclesForm.valid) {
            return;
        }

        this.manageVehiclesService.addVehicleToUser(
            this.manageVehiclesForm.get('userId').value, this.manageVehiclesForm.get('vin').value).subscribe(
            response => {
                this.saveSuccess = true;
                this.saveFailure = false;
            },
            error => {
                this.saveSuccess = false;
                this.saveFailure = true;
            }
        );
    }

    goHome() {
        this.router.navigate(['']);
    }

}
