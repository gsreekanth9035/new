import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from './../material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MANAGE_VEHICLES_ROUTES } from './manage-vehicles.route';
import { ManageVehiclesComponent } from './manage-vehicles.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
      RouterModule.forChild(MANAGE_VEHICLES_ROUTES),
      CommonModule,
      BrowserAnimationsModule,
      ReactiveFormsModule,
      HttpClientModule,
      MaterialModule,
      GatewaySharedModule
  ],
  declarations: [ ManageVehiclesComponent ]
})

export class ManageVehiclesModule { }
