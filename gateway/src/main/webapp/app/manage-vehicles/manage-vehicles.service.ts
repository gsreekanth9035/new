import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Principal } from 'app/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable({
    providedIn: 'root'
})
export class ManageVehiclesService {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name
        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });

    constructor(private httpClient: HttpClient, private principal: Principal) {}

    // Call REST APIs
    addVehicleToUser(userId: number, vin: string): Observable<any> {
        this.httpHeaders.append('Content-type', 'application/json');

        const orgName = this.principal.getLoggedInUser().organization;
        const userName = this.principal.getLoggedInUser().username;

        return this.httpClient
            .post(`${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/${userId}/vehicles/${vin}`, null, {
                headers: this.httpHeaders,
                observe: 'response'
            })
            .catch(this.errorHandler);
    }

    getUserIdNamePairs(): Observable<UserIdNamePair[]> {
        const orgName = this.principal.getLoggedInUser().organization;
        return this.httpClient.get<UserIdNamePair[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${orgName}/users/userIdNames`
        );
    }

    errorHandler(error: HttpErrorResponse) {
        return Observable.throw('Error: Vehicle Mapping failed');
    }
}

export class UserIdNamePair {
    private userId: number;
    private userName: string;
}
