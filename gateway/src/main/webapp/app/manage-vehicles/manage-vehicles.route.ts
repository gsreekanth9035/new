import { ManageVehiclesComponent } from './manage-vehicles.component';
import { Route, Routes } from '@angular/router';

export const MANAGE_VEHICLES_ROUTES: Routes = [
    {
        path: 'manage-vehicles',
        component: ManageVehiclesComponent,
        data: {
            authorities: [],
            pageTitle: 'manageVehicles.pageTitle',
        }
    }
];
