import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { APP_DATE_FORMATS, AppDateAdapter } from 'app/shared/util/date.adapter';
import { TableData } from '../md/md-table/md-table.component';
import * as Chartist from 'chartist';
import * as $ from 'jquery';

declare const $: any;

@Component({
    selector: 'jhi-activity',
    templateUrl: './activity.component.html',
    styleUrls: ['activity.scss']
})
export class ActivityComponent implements OnInit, AfterViewInit {
    cardPrinterForm: FormGroup;
    pageSize = 5;
    resultsLength = 0;
    // constructor(private navbarTitleService: NavbarTitleService, private notificationService: NotificationService) { }
    public tableData: TableData;
    startAnimationForLineChart(chart: any) {
        let seq: any, delays: any, durations: any;
        seq = 0;
        delays = 80;
        durations = 500;
        chart.on('draw', function(data: any) {
            if (data.type === 'line' || data.type === 'area') {
                data.element.animate({
                    d: {
                        begin: 600,
                        dur: 700,
                        from: data.path
                            .clone()
                            .scale(1, 0)
                            .translate(0, data.chartRect.height())
                            .stringify(),
                        to: data.path.clone().stringify(),
                        easing: Chartist.Svg.Easing.easeOutQuint
                    }
                });
            } else if (data.type === 'point') {
                seq++;
                data.element.animate({
                    opacity: {
                        begin: seq * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }
        });

        seq = 0;
    }
    startAnimationForBarChart(chart: any) {
        let seq2: any, delays2: any, durations2: any;
        seq2 = 0;
        delays2 = 80;
        durations2 = 500;
        chart.on('draw', function(data: any) {
            if (data.type === 'bar') {
                seq2++;
                data.element.animate({
                    opacity: {
                        begin: seq2 * delays2,
                        dur: durations2,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }
        });

        seq2 = 0;
    }
    // constructor(private navbarTitleService: NavbarTitleService) { }
    public ngOnInit() {
        this.tableData = {
            headerRow: ['ID', 'Name', 'Salary', 'Country', 'City'],
            dataRows: [
                ['10/29/2019 09:25 AM EST', 'Adam', 'PKI', '10.342.23.2', 'US', 'USA', 'Success'],
                ['10/29/2019 09:25 AM EST', 'Smith', 'Push Token', '10.342.23.2', 'DE', 'Germany', 'Success'],
                ['10/29/2019 09:25 AM EST', 'Jane', 'PKI', '10.342.23.2', 'AU', 'Australia', 'Failure'],
                ['10/29/2019 09:25 AM EST', 'Albert', 'PKI', '10.342.23.2', 'GB', 'United Kingdom', 'Success'],
                ['10/29/2019 09:25 AM EST', 'Ali', 'Push Token', '10.342.23.2', 'RO', 'Romania', 'Failure'],
                ['10/29/2019 09:25 AM EST', 'Steve', 'Push Token', '10.342.23.2', 'BR', 'Brasil', 'Success']
            ]
        };
        /* ----------==========     Daily Sales Chart initialization    ==========---------- */

        this.resultsLength = this.tableData.dataRows.length;
        const dataDailySalesChart = {
            labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
            series: [[12, 17, 7, 17, 23, 18, 38]]
        };

        const optionsDailySalesChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0 }
        };

        const dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

        this.startAnimationForLineChart(dailySalesChart);
        /* ----------==========     Completed Tasks Chart initialization    ==========---------- */

        const dataCompletedTasksChart = {
            labels: ['12p', '3p', '6p', '9p', '12p', '3a', '6a', '9a'],
            series: [[230, 750, 450, 300, 280, 240, 200, 190]]
        };

        const optionsCompletedTasksChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better
            // look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0 }
        };

        const completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);

        this.startAnimationForLineChart(completedTasksChart);

        /* ----------==========     Emails Subscription Chart initialization    ==========---------- */

        const dataWebsiteViewsChart = {
            // labels: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
            labels: ['5', '10', '30', '60', '90'],
            series: [[542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]]
        };
        const dataWebsiteViewsChart2 = {
            labels: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
            series: [[542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]]
        };
        const dataWebsiteViewsChart3 = {
            labels: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
            series: [[542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]]
        };
        const optionsWebsiteViewsChart = {
            axisX: {
                showGrid: false
            },
            low: 0,
            high: 1000,
            chartPadding: { top: 0, right: 5, bottom: 0, left: 0 }
        };
        const optionsWebsiteViewsChart2 = {
            axisX: {
                showGrid: false
            },
            low: 0,
            high: 1000,
            chartPadding: { top: 0, right: 5, bottom: 0, left: 0 }
        };
        const optionsWebsiteViewsChart3 = {
            axisX: {
                showGrid: false
            },
            low: 0,
            high: 1000,
            chartPadding: { top: 0, right: 5, bottom: 0, left: 0 }
        };
        const responsiveOptions: any = [
            [
                'screen and (max-width: 640px)',
                {
                    seriesBarDistance: 5
                    // ,
                    //   axisX: {
                    //     labelInterpolationFnc: function(value) {
                    //       return value[0];
                    //     }
                    //   }
                }
            ]
        ];
        const websiteViewsChart = new Chartist.Bar(
            '#websiteViewsChart',
            dataWebsiteViewsChart,
            optionsWebsiteViewsChart,
            responsiveOptions
        );
        const websiteViewsChart2 = new Chartist.Bar(
            '#websiteViewsChart2',
            dataWebsiteViewsChart,
            optionsWebsiteViewsChart,
            responsiveOptions
        );
        const websiteViewsChart3 = new Chartist.Bar(
            '#websiteViewsChart3',
            dataWebsiteViewsChart,
            optionsWebsiteViewsChart,
            responsiveOptions
        );

        this.startAnimationForBarChart(websiteViewsChart);
        this.startAnimationForBarChart(websiteViewsChart2);
        this.startAnimationForBarChart(websiteViewsChart3);

        $('#worldMap').vectorMap({
            map: 'world_en',
            backgroundColor: 'transparent',
            borderColor: '#818181',
            borderOpacity: 0.25,
            borderWidth: 1,
            color: '#b3b3b3',
            enableZoom: true,
            hoverColor: '#eee',
            hoverOpacity: null,
            normalizeFunction: 'linear',
            scaleColors: ['#b6d6ff', '#005ace'],
            selectedColor: '#c9dfaf',
            selectedRegions: null,
            showTooltip: true
            //  ,
            //  onRegionClick: function(element, code, region) {
            //      const message = 'You clicked "'
            //          + region
            //          + '" which has the code: '
            //          + code.toUpperCase();

            //      alert(message);
            //  }
        });
    } // ngOnInit
    ngAfterViewInit() {
        const breakCards = true;
        if (breakCards === true) {
            // We break the cards headers if there is too much stress on them :-)
            $('[data-header-animation="true"]').each(function() {
                const $fix_button = $(this);
                const $card = $(this).parent('.card');
                $card.find('.fix-broken-card').click(function() {
                    const $header = $(this)
                        .parent()
                        .parent()
                        .siblings('.card-header, .card-image');
                    $header.removeClass('hinge').addClass('fadeInDown');

                    $card.attr('data-count', 0);

                    setTimeout(function() {
                        $header.removeClass('fadeInDown animate');
                    }, 480);
                });

                $card.mouseenter(function() {
                    const $this = $(this);
                    const hover_count = parseInt($this.attr('data-count'), 10) + 1 || 0;
                    $this.attr('data-count', hover_count);
                    if (hover_count >= 20) {
                        $(this)
                            .children('.card-header, .card-image')
                            .addClass('hinge animated');
                    }
                });
            });
        }
    }
}
