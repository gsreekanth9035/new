import { ActivityComponent } from './activity.component';
import { Routes } from '@angular/router';

export const ACTIVITY_ROUTES: Routes = [
    {
        path: 'activity',
        component: ActivityComponent,
        data: {
            authorities: [],
            pageTitle: 'monitor.activityTitle'
        }
    }
];
