import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from '../material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ACTIVITY_ROUTES } from './activity.route';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ActivityComponent } from './activity.component';
import { MdModule } from '../md/md.module';

@NgModule({
    imports: [
        RouterModule.forChild(ACTIVITY_ROUTES),
        CommonModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MaterialModule,
        GatewaySharedModule,
        MdModule
    ],
    declarations: [ActivityComponent]
})
export class ActivityModule {}
