import { Routes } from '@angular/router';
import { PairMobileServiceComponent } from './pair-mobile-service.component';

export const PAIRMOBILESERVICE_ROUTES: Routes = [
    {
        path: 'pairmobileservice',
        component: PairMobileServiceComponent,
        data: {
            authorities: [],
            pageTitle: 'pairMobileService.title',
        }
    }
];
