import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/core';
import { PairMobileDeviceConfig } from 'app/pair-mobile-device/pair-mobile-device.service';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { OrganizationBrandingInfo } from 'app/layouts/login/login.component';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs-compat/operator/filter';

@Component({
    selector: 'jhi-pair-mobile-service',
    templateUrl: './pair-mobile-service.component.html',
    styleUrls: ['pair-mobile-service.scss']
})
export class PairMobileServiceComponent implements OnInit {
    errorMsg: string;
    pairMobileDeviceConfig: PairMobileDeviceConfig;
    serviceApp: string;
    constructor(private loginService: LoginService, private titleService: Title, private translateService: TranslateService,
        private activatedRoute: ActivatedRoute  ) {}

    ngOnInit() {
        const brnadingInformation: OrganizationBrandingInfo = this.loginService.getBrandingInfo();
        if (brnadingInformation !== null) {
            this.serviceApp = brnadingInformation.mobileServicesAppName;
            this.activatedRoute.snapshot.data['pageTitle'] = `pairMobileService.${this.serviceApp}`;
            this.translateService.get(`pairMobileService.${this.serviceApp}`).subscribe(msg => {
                this.titleService.setTitle(msg);
            });
        }

        this.loginService.getPairMobileServiceQRCode().subscribe(
            (pairMSConfig: PairMobileDeviceConfig) => {
                this.pairMobileDeviceConfig = pairMSConfig;
                this.errorMsg = undefined;
            },
            err => {
                // alert('error');
            }
        );
    }
}
