import { GatewaySharedModule } from '../shared/shared.module';
import { PAIRMOBILESERVICE_ROUTES } from './pair-mobile-service.route';
import { NgModule } from '@angular/core';
import { PairMobileServiceComponent } from './pair-mobile-service.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatTooltipModule } from '@angular/material';

@NgModule({
    imports: [CommonModule, GatewaySharedModule, RouterModule.forChild(PAIRMOBILESERVICE_ROUTES), MatTooltipModule],

    declarations: [PairMobileServiceComponent]
})
export class PairMobileServiceModule {}
