import { Route } from '@angular/router';
import { IdReaderComponent } from './id-reader.component';

export const ID_READER_ROUTE: Route = {
    path: 'idreader',
    component: IdReaderComponent,
    data: {
        authorities: [],
        pageTitle: 'idreader.title'
    }
};
