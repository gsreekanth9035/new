export class PivMasterBean {
    cccContainerBean: CCContainerBean;
    chuidContainerBean: CHUIDContainerBean;
    piContainerBean: PIContainerBean;
    fpContainerBean: FPContainerBean;
    facialUserBioInfoBean: FacialUserBioInfoBean;
    securityContainerBean: SecurityContainerBean;
    keyHistoryBean: KeyHistoryContainerBean;
    discoveryObjectBean: DiscoveryObjectBean;
    irisContainerBean: IRISContainerBean;
    bitGroupContainerBean: BITGroupContainerBean;
    userCertificatesBean: UserCertificates;
    userCertificates: UserCertificateBean[] = [];
    userRetiredKeyMngCertificates: UserCertificateBean[] = [];
    pairingCodeContainerbean: PairingCodeConatainerBean;
    constructor() { }
  }
  export class CCContainerBean {
    cardIdentifier: string;
    capaContainerVerNo: string;
    capaGrammarVerNo: string;
    appCardURL: string;
    pKCS15: string;
    regDataModelNo: string;
    accessControlRuleTable: string;
    extAppCardURL: string;
    securityObjectBuff: string;
    cardAPDUs: string;
    redirectionTag: string;
    capabilityTuples: string;
    statusTuples: string;
    nextCCC: string;
  
  }
  export class CHUIDContainerBean {
    fascn: string;
    fascnAgencyCode: string;
    fascnSystemCode: string;
    fascnCredentialNumber: string;
    fascnCs: string;
    fascnICI: string;
    fascnPI: string;
    fascnOC: string;
    fascnOI: string;
    fascnPOA: string;
    duns: string;
    guid: string;
    expirationDate: string;
    uUID: string;
    organizationID: string;
    issuerAsymetricSign: string;
    constructor() { }
  }
  export class PIContainerBean {
    name: string;
    empAffiliation: string;
    expirationDate: string;
    agencyCardSerialNo: string;
    issuerID: string;
    orgAffiliation1: string;
    orgAffiliation2: string;
    constructor() { }
  }
  export class FPContainerBean {
    fingerPosition: string;
    viewNumImpressionType: string;
    fingerQuality: string;
    minutiaeCount: string;
    fingerImage: string;
    primaryFingerPresent: boolean;
    secondaryFingerPresent: boolean;
  }
  export class FacialUserBioInfoBean {
    facialImage: string;
    facialData;
    CreationDate: string;
    ExpirationDate: string;
    ValidFromDate: string;
    Creator: string;
    signatureDate: Date;
    featurePoints: string;
    gender: string;
    eyeColor: string;
    hairColor: string;
    featureMask: string;
    imageWidth: string;
    imageHeight: string;
    deviceType: string;
    imageQuality: string;
    // private ContentCertificateBean contentCertificateBean;
    uuid: string;
  }
  export class SecurityContainerBean {
  
  }
  export class KeyHistoryContainerBean {
    keysWithOnCardCerts: string;
    keysWithOffCardCerts: string;
  }
  export class DiscoveryObjectBean {
    pivCardAppAID: string;
    pinUsagePolicy: string;
  }
  export class IRISContainerBean {
    imagesForIris: string;
  }
  export class BITGroupContainerBean {
    NumberOfFingers: string;
    BITforFirstFinger: string;
    BITforSecondFinger: string;
  }
  export class UserCertificates {
    userCertificates: UserCertificateBean[] = [];
    pivAuthentication9ACert: string;
    digSignature9CCert: string;
    keyManagement9DCert: string;
    cardAuthentication9ECert: string;
    retiredKeyMang1: string;
    retiredKeyMang2: string;
    retiredKeyMang3: string;
    retiredKeyMang4: string;
    retiredKeyMang5: string;
    retiredKeyMang6: string;
    retiredKeyMang7: string;
    retiredKeyMang8: string;
    retiredKeyMang9: string;
    retiredKeyMang10: string;
    retiredKeyMang11: string;
    retiredKeyMang12: string;
    retiredKeyMang13: string;
    retiredKeyMang14: string;
    retiredKeyMang15: string;
    retiredKeyMang16: string;
    retiredKeyMang17: string;
    retiredKeyMang18: string;
    retiredKeyMang19: string;
    retiredKeyMang20: string;
    X509CertForContentSign: string;
    secureMsgCertInfo: string;
  }
  export class UserCertificateBean {
    check;
    idName: string;
    name: string;
    x509CertContent: string;
    serialNumber: string;
    commonName: string;
    organizationName: string;
    organizationUnit: string;
    country: string;
    userEmail: string;
    signAlg: string;
    validFrom: Date;
    validTo: Date;
  }
  export class PairingCodeConatainerBean {
    pairingCode: string;
  }
  
  export class RfidMasterBean {
    rfidBasicIDTagBean: RfidBasicIDTagBean;
    rfidCardIDBean: RfidCardIDBean;
    rfidExtCardIDBean: RfidExtCardIDBean;
  }
  export class RfidBasicIDTagBean {
    basicTagLen: string;
    orgName: string;
    cardType: number;
    cardTypeName: string;
    cardSrNo: string;
    issuerAddress: string;
    cardHolderAddress: string;
    cardHolderName: string;
    cardHolderLastName: string;
    cardHolderFirstName: string;
    cardHolderMiddleName: string;
    issueDate: string;
    issueDateDay: string;
    issueDateMonth: string;
    issueDateYear: string;
    expiredDate: string;
    expiredDateDay: string;
    expiredDateMonth: string;
    expiredDateYear: string;
    sex: string;
    bloodGroup: string;
    donar: string;
    issuerContactInfo: string;
    issuerContactInfoMobNo: string;
    issuerContactInfoLanNo: string;
    issuerContactInfoFaxNo: string;
    issuerContactInfoEmailId: string;
    issuerContactInfoWeb: string;
    cardHolderContactInfo: string;
    cardHolderContactInfoMobNo: string;
    cardHolderContactInfoLanNo: string;
    cardHolderContactInfoFaxNo: string;
    cardHolderContactInfoEmailId: string;
    cardHolderContactInfoWeb: string;
  }
  export class RfidCardIDBean {
  
  }
  export class RfidExtCardIDBean {
  
  }
  