import { GatewaySharedModule } from 'app/shared';
import { MaterialModule } from '../material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { ID_READER_ROUTE } from './id-reader.route';
import { IdReaderComponent } from './id-reader.component';

@NgModule({
    imports: [
        RouterModule.forChild([ID_READER_ROUTE]),
        CommonModule,
        GatewaySharedModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        MaterialModule,
        GatewaySharedModule,
        AngularSvgIconModule
    ],
    declarations: [IdReaderComponent]
})
export class IdReaderModule {}
