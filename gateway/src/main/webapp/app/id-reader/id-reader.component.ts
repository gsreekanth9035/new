import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Principal } from 'app/core';
import { IssuanceService } from 'app/issuance/issuance.service';
import { CardReader } from 'app/issuance/model/cardReader.model';
import { DeviceClientService } from 'app/shared/util/device-client.service';
import { throwError, Subscription, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { CmsSyncRequest } from 'app/issuance/model/cmsSyncRequest.model';
import { DataService } from 'app/device-profile/data.service';
import { Device } from 'app/manage-identity-devices/manage-identity-devices.model';
import { DeviceProfile } from 'app/device-profile/device-profile.model';
import { WorkflowService } from 'app/workflow/workflow.service';
import { User } from 'app/enroll/user.model';
import { Commands } from 'app/issuance/model/commands.model';
import {
    CHUIDContainerBean,
    FacialUserBioInfoBean,
    FPContainerBean,
    PIContainerBean,
    PivMasterBean,
    RfidBasicIDTagBean,
    RfidCardIDBean,
    RfidExtCardIDBean,
    RfidMasterBean,
    UserCertificateBean,
    UserCertificates
} from './model/id-reader.model';

@Component({
    selector: 'jhi-id-reader',
    templateUrl: './id-reader.component.html',
    styleUrls: ['id-reader.scss']
})
export class IdReaderComponent implements OnInit, OnDestroy {
    isLoadingResults = false;
    idReaderForm: FormGroup;
    pinHide = true;
    display = false;
    errorMsg = '';
    errorDisplay = false;
    processName;
    identifyProcessResponse: any;
    cardType;
    appLock: string;
    showPIN = false;
    showDeviceProfile = false;
    atr: string;

    cardReaders: CardReader[] = [];
    subscription: Subscription = new Subscription();

    flag = false;
    timer: any;

    selectedCardAtr: string;
    selectedCardName: string;

    device: Device;
    username: string;
    mappedDeviceID;
    orgName;
    workflowName;

    deviceProfiles: DeviceProfile[] = [];
    wfDeviceProfilesList: DeviceProfile[] = [];
    dispalyDetails = false;
    pivMasterBean: PivMasterBean;
    rfidMasterBean: RfidMasterBean;

    userDetails: User;
    piConatinerBean: PIContainerBean;
    chuidContainerBean: CHUIDContainerBean;
    userCertificatesBean: UserCertificates;
    cardSerialNumber;
    fpContainerBean: FPContainerBean;
    facialUserBioInfoBean: FacialUserBioInfoBean;
    userCertificates: UserCertificateBean[] = [];
    userRetiredKeyMngCertificates: UserCertificateBean[] = [];

    rfidBasicIDTagBean: RfidBasicIDTagBean;
    rfidCardIDBean: RfidCardIDBean;
    rfidExtCardIDBean: RfidExtCardIDBean;
    certIdNameCount = 0;
    isPinValidationFalied = false;
    // collapsable button related variables
    showDeviceInfo = false;
    showUserInfo = false;
    showIssuerInfo = false;
    showChuidInfo = false;
    showFascn = false;
    showPrintedInfo = false;
    showCerts = false;
    showUserBiometrics = false;

    constructor(
        private formBuilder: FormBuilder,
        private dataService: DataService,
        private principal: Principal,
        private issuanceService: IssuanceService,
        private translateService: TranslateService,
        private deviceClientService: DeviceClientService,
        private workflowService: WorkflowService
    ) {
        this.orgName = this.principal.getLoggedInUser().organization;
        this.buildIdReaderForm();
    }

    ngOnInit() {
        this.display = true;
        this.device = this.dataService.getCurrentMIdObj();
        console.log(this.device);
        if (this.device !== null) {
            this.username = this.device.userName;
            this.mappedDeviceID = this.device.id;
            if (this.device.userFirstName !== undefined) {
                this.issuanceService.getWorkflowByUsername(this.device.userName).subscribe(workflow => {
                    this.workflowName = workflow;
                });
            } else {
                this.workflowName = this.principal.getLoggedInUser().workflow;
            }
        }
    }

    buildIdReaderForm() {
        this.idReaderForm = this.formBuilder.group({
            identityDevice: ['', Validators.required],
            pin: [null],
            deviceProfile: [null]
        });
        this.getReadersList();
    }

    getReadersList() {
        this.subscription = timer(0, 2000)
            .pipe(switchMap(() => this.deviceClientService.getCardReaders()))
            .subscribe(
                readersList => {
                    const list = readersList.cardReaderList;
                    this.flag = false;
                    let updateList = false;
                    if (readersList.responseCode === 'DS0000') {
                        const selectedReader = this.idReaderForm.controls['identityDevice'].value;
                        const cr = this.idReaderForm.controls['identityDevice'].value;
                        if (cr === null || cr === undefined) {
                            this.cardReaders = list;
                            this.idReaderForm.controls['identityDevice'].setValue(this.cardReaders[0]);
                            this.setCardName(this.cardReaders[0]);
                            return;
                        }
                        if (this.cardReaders.length === list.length || this.cardReaders.length > list.length) {
                            let isFound = false;
                            for (const c of this.cardReaders) {
                                const found = list.find(e => e.cardReaderName === c.cardReaderName);
                                if (found === undefined) {
                                    updateList = true;
                                } else {
                                    if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                    }
                                }
                            }
                            const card1 = list.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (card1 !== undefined) {
                                if (!isFound || this.selectedCardName === '' || card1.atr !== this.selectedCardAtr) {
                                    if (card1 !== undefined) {
                                        this.setCardName(card1);
                                    }
                                }
                            }
                        } else if (this.cardReaders.length < list.length) {
                            let isFound = false;
                            for (const c of list) {
                                const found = this.cardReaders.find(e => e.cardReaderName === c.cardReaderName);
                                if (found === undefined) {
                                    updateList = true;
                                } else {
                                    if (found.atr === this.selectedCardAtr) {
                                        isFound = true;
                                    }
                                }
                            }
                            const card1 = list.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (card1 !== undefined) {
                                if (!isFound || this.selectedCardName === '' || card1.atr !== this.selectedCardAtr) {
                                    if (card1 !== undefined) {
                                        this.setCardName(card1);
                                    }
                                }
                            }
                        }
                        if (updateList) {
                            this.cardReaders = list;
                            const cardReader: CardReader = this.cardReaders.find(e => e.cardReaderName === selectedReader.cardReaderName);
                            if (cardReader !== undefined) {
                                this.idReaderForm.controls['identityDevice'].setValue(cardReader);
                                if (cardReader.atr !== this.selectedCardAtr) {
                                    this.setCardName(cardReader);
                                }
                            } else {
                                this.idReaderForm.controls['identityDevice'].setValue(this.cardReaders[0]);
                                if (this.cardReaders[0].atr !== this.selectedCardAtr) {
                                    this.setCardName(this.cardReaders[0]);
                                }
                            }
                        }
                    } else {
                        this.idReaderForm.controls['identityDevice'].setValue(null);
                        this.cardReaders = [];
                        this.selectedCardAtr = '';
                        this.selectedCardName = '';
                        this.translateService.get('issuance.connectReaderAndCard').subscribe(msg => {
                            this.errorMsg = msg;
                            this.errorDisplay = true;
                        });
                    }
                    this.flag = true;
                },
                error => {
                    if (this.subscription) {
                        this.subscription.unsubscribe();
                    }
                    this.errorDisplay = true;
                    this.translateService.get('enroll.messages.error.deviceService.serviceDown').subscribe(serviceErrMessage => {
                        this.errorMsg = serviceErrMessage;
                        // Retrying
                        this.getReadersList();
                    });
                }
            );
    }
    setCardName(cardReader: CardReader) {
        const atr = cardReader.atr;
        if (atr.length !== 0) {
            if (!this.isPinValidationFalied) {
                this.errorDisplay = false;
            }
            const newAtr = atr.replace(/ /g, '');
            this.atr = newAtr;
            this.issuanceService.getCardNameByAtr(newAtr).subscribe(
                name => {
                    console.log(name);
                    this.errorDisplay = false;
                    console.log(this.selectedCardName, name);
                    if (this.selectedCardName !== name) {
                        this.selectedCardAtr = atr;
                        this.selectedCardName = name;
                        // call identify process and get the card details
                        // this.processName = 'identify';
                        console.log('call identify process:::::');
                        this.idetifyProcess(cardReader, newAtr);
                        this.dispalyDetails = false;
                    }
                },
                err => {}
            );
        } else {
            this.translateService.get('issuance.cardNotFound').subscribe(
                msg => {
                    this.dispalyDetails = false;
                    this.showPIN = false;
                    this.showDeviceProfile = false;
                    this.errorDisplay = true;
                    this.errorMsg = msg;
                    this.selectedCardAtr = '';
                    this.selectedCardName = '';
                },
                err => {
                    this.errorMsg = 'Card not found, Please insert';
                    console.log('insdie card not found:::');
                }
            );
        }
    }

    clearPinAndValidators() {
        const pin = this.idReaderForm.controls.pin;
        pin.clearValidators();
        pin.updateValueAndValidity();
        pin.reset();

        const deviceProfile = this.idReaderForm.controls.deviceProfile;
        deviceProfile.reset();
        deviceProfile.setValidators([Validators.required]);
        deviceProfile.updateValueAndValidity();
    }
    addPinAndValidators() {
        const pin = this.idReaderForm.controls.pin;
        const deviceProfile = this.idReaderForm.controls.deviceProfile;
        deviceProfile.reset();
        deviceProfile.clearValidators();
        pin.setValidators([Validators.required, Validators.pattern('^([0-9]{6,8})$')]);
        deviceProfile.updateValueAndValidity();
        pin.reset();
        pin.updateValueAndValidity();
    }
    loadDevicePRofiles(cardProductName: string) {
        if (this.wfDeviceProfilesList.length === 0) {
            this.workflowService.getDeviceProfiles().subscribe(
                deviceProfiles => {
                    this.wfDeviceProfilesList = deviceProfiles;
                    this.getDeviceProfilesBasedOnDevice(cardProductName);
                },
                err => {
                    this.isLoadingResults = false;
                }
            );
        } else {
            this.getDeviceProfilesBasedOnDevice(cardProductName);
        }
    }
    getDeviceProfilesBasedOnDevice(cardProductName: string) {
        const devProfList: DeviceProfile[] = this.wfDeviceProfilesList.filter(e => e.configValueProductName.value === cardProductName);
        console.log(devProfList);
        if (devProfList.length === 1) {
            this.deviceProfiles = devProfList;
            this.idReaderForm.controls.deviceProfile.setValue(this.deviceProfiles[0]);
        } else if (devProfList.length === 0) {
            this.translateService.get('idreader.noDeviceProfFound').subscribe(msg => {
                this.errorMsg = msg;
                this.errorDisplay = true;
            });
        } else {
            this.deviceProfiles = devProfList;
            this.idReaderForm.controls.deviceProfile.setValue(this.deviceProfiles[0]);
        }
    }
    async idetifyProcess(cardReader: CardReader, atr) {
        const cardReaderName = cardReader.cardReaderName;
        // start identify process
        this.issuanceService.start('identify', atr).subscribe(
            identifyStartResponse => {
                const deviceServiceReq1 = identifyStartResponse.body;
                deviceServiceReq1.state = 0;
                console.log(deviceServiceReq1);
                // execute the commands to card
                this.deviceClientService.execute(deviceServiceReq1, cardReaderName).subscribe(
                    identifyCommandsRes => {
                        this.issuanceService.next(identifyCommandsRes.body).subscribe(
                            identifyNextRes => {
                                let nextResponse = new Commands();
                                nextResponse = identifyNextRes.body;
                                console.log(nextResponse);
                                if (nextResponse.step === 'done') {
                                    this.cardSerialNumber = identifyNextRes.body.entries[0].result.serial;
                                    this.identifyProcessResponse = identifyNextRes;
                                    this.appLock = identifyNextRes.body.entries[0].result.appLock;
                                    this.cardType = identifyNextRes.body.entries[0].result.type;
                                    if (
                                        this.cardType === 'MifareClassic1k' ||
                                        this.cardType === 'MifareClassic4k' ||
                                        this.cardType === 'Desfire'
                                    ) {
                                        // for these cards PIN is not required
                                        console.log('card is rfid::::');
                                        this.showPIN = false;
                                        this.showDeviceProfile = true;
                                        this.clearPinAndValidators();
                                        this.loadDevicePRofiles(identifyNextRes.body.entries[0].result.cardProductName);
                                    } else {
                                        this.showPIN = true;
                                        this.showDeviceProfile = false;
                                        // PIN is required, so enable pin section
                                        this.addPinAndValidators();
                                    }
                                    console.log(nextResponse);
                                } else {
                                    this.selectedCardName = '';
                                    return throwError(nextResponse.entries[0].message);
                                }
                            },
                            error => {
                                console.log(error);
                            }
                        );
                    },
                    error => {
                        console.log(error);
                    }
                );
            },
            error => {
                console.log(error);
            }
        );
    }

    async verifyPinAndGetDetails() {
        this.isPinValidationFalied = false;
        this.isLoadingResults = true;
        this.errorDisplay = false;
        if (this.idReaderForm.controls['identityDevice'].value === null) {
            this.errorDisplay = true;
            this.errorMsg = 'Please connect reader and card!!';
            window.scrollTo(0, 0);
            this.isLoadingResults = false;
            return;
        }
        if (this.appLock === 'true') {
            console.log('inside the applock');
            this.errorDisplay = true;
            this.errorMsg = 'Card/Token is locked.Please Unlock the same to proceed';
            window.scrollTo(0, 0);
            this.isLoadingResults = false;
            return;
        }
        this.deviceClientService
            .getAtrByReaderName(this.idReaderForm.controls.identityDevice.value.urlEncodedcardReaderName)
            .subscribe(async response => {
                const atr1 = response.atr;
                if (atr1 === '') {
                    this.translateService.get('issuance.identityDeviceNotFound').subscribe(msg => {
                        this.errorDisplay = true;
                        this.isLoadingResults = false;
                        this.errorMsg = msg;
                        window.scrollTo(0, 0);
                        return null;
                    });
                } else {
                    if (this.identifyProcessResponse === undefined) {
                        console.log('identify response is null');
                        // removing the white spaces in atr
                        const newAtr = atr1.replace(/ /g, '');
                        await this.idetifyProcess(this.idReaderForm.controls.identityDevice.value.cardReaderName, newAtr);
                    }
                    const cardReaderName = this.idReaderForm.controls['identityDevice'].value.cardReaderName;
                    if (this.showPIN) {
                        this.processName = 'verify-pin';
                    } else {
                        // directly call get card details process
                        this.processName = 'rfidread';
                    }
                    this.startProcess(this.identifyProcessResponse, this.processName, cardReaderName);
                    clearInterval(this.timer);
                }
            });
    }

    startProcess(response, processName, cardReaderName) {
        this.processName = processName;
        const cmsSyncRequest = this.buildCmsSyncRequest(response, processName);
        this.issuanceService.getUUID(processName, cmsSyncRequest).subscribe(
            uuid => {
                // start  process
                this.issuanceService.start(processName, uuid).subscribe(
                    issaunceStartRes => {
                        // send the commands to card
                        issaunceStartRes.body.state = 1;
                        this.deviceClientService.execute(issaunceStartRes.body, cardReaderName).subscribe(
                            result => {
                                this.issuanceService.next(result.body).subscribe(
                                    res => {
                                        this.recurse(res.body, cardReaderName);
                                    },
                                    error => {
                                        console.log(error);
                                        this.isLoadingResults = false;
                                    }
                                );
                            },
                            error => {
                                console.log(error);
                                this.isLoadingResults = false;
                            }
                        );
                    },
                    error => {
                        console.log(error);
                        this.isLoadingResults = false;
                    }
                );
            },
            error => {
                console.log(error);
                this.isLoadingResults = false;
            }
        );
    }

    recurse(res, cardReaderName) {
        if (res.stage !== 'done' && res.step !== 'error') {
            res.state = 1;
            this.deviceClientService.execute(res, cardReaderName).subscribe(
                result1 => {
                    this.issuanceService.next(result1.body).subscribe(
                        response => {
                            if (response.body !== null) {
                                if (res.step === 'done') {
                                    if (this.processName === 'read_piv' || this.processName === 'rfidread') {
                                        this.pivMasterBean = null;
                                        this.rfidMasterBean = null;
                                    }
                                    if (res.pivMasterBean !== null) {
                                        this.pivMasterBean = res.pivMasterBean;
                                        this.rfidMasterBean = res.rfidMasterBean;
                                        console.log('inside piv master bean:::', res.pivMasterBean);
                                    } else if (res.rfidMasterBean !== null) {
                                        console.log('inside rfid master bean:::');
                                        this.rfidMasterBean = res.rfidMasterBean;
                                        this.pivMasterBean = res.pivMasterBean;
                                        console.log(res.rfidMasterBean, res.pivMasterBean);
                                    }
                                }
                                this.recurse(response.body, cardReaderName);
                            } else {
                                this.isLoadingResults = false;
                            }
                        },
                        error => {
                            console.log(error);
                            this.isLoadingResults = false;
                        }
                    );
                },
                error => {
                    console.log(error);
                    this.isLoadingResults = false;
                }
            );
        } else if (res.step === 'error' || res.stage === 'error') {
            this.errorDisplay = true;
            if (res.entries[0] !== null) {
                this.translateService.get('idreader.failed').subscribe(msg => {
                    if (res.entries[0].message.includes('_')) {
                        this.errorMsg = msg + '- (Error Code: ' + res.entries[0].message + ')';
                    } else {
                        this.errorMsg = msg;
                    }
                });
                if (this.errorMsg === undefined || this.errorMsg === null) {
                    this.errorMsg = 'Error while reading card data';
                }
            }
            if (this.processName === 'verify-pin') {
                this.isPinValidationFalied = true;
            }
            this.isLoadingResults = false;
            return throwError(res.step);
        } else if (res.step === 'done' && res.stage === 'done') {
            if (this.processName === 'verify-pin') {
                this.processName = 'read_piv';
                this.startProcess(this.identifyProcessResponse, this.processName, cardReaderName);
            } else if (this.processName === 'rfidread') {
                this.isLoadingResults = false;
                this.getCardDetails(this.pivMasterBean, this.rfidMasterBean);
            } else if (this.processName === 'read_piv') {
                this.isLoadingResults = false;
                this.getCardDetails(this.pivMasterBean, this.rfidMasterBean);
            }
        }
    }

    getCardDetails(pivMasterBean: PivMasterBean, rfidMasterBean: RfidMasterBean) {
        this.resetValues();
        if (pivMasterBean !== null) {
            this.piConatinerBean = pivMasterBean.piContainerBean;
            this.chuidContainerBean = pivMasterBean.chuidContainerBean;
            this.fpContainerBean = pivMasterBean.fpContainerBean;
            this.facialUserBioInfoBean = pivMasterBean.facialUserBioInfoBean;
            this.userCertificatesBean = pivMasterBean.userCertificatesBean;
            pivMasterBean.userCertificates.forEach(e => {
                e.idName = 'cert' + this.certIdNameCount++;
                e.check = 'certCheck' + this.certIdNameCount++;
            });
            pivMasterBean.userRetiredKeyMngCertificates.forEach(e => {
                e.idName = 'cert' + this.certIdNameCount++;
                e.check = 'certCheck' + this.certIdNameCount++;
            });

            this.userCertificates = pivMasterBean.userCertificates;
            this.userRetiredKeyMngCertificates = pivMasterBean.userRetiredKeyMngCertificates;
            this.userRetiredKeyMngCertificates.forEach(e => {
                this.userCertificates.push(e);
            });
            this.userDetails = new User();
            if (this.piConatinerBean !== null) {
                this.userDetails.username = this.piConatinerBean.name;
                this.userDetails.organisationName = this.piConatinerBean.empAffiliation;
            }
        } else if (rfidMasterBean !== null) {
            this.rfidBasicIDTagBean = rfidMasterBean.rfidBasicIDTagBean;
            this.rfidBasicIDTagBean.sex = this.getGender(this.rfidBasicIDTagBean.sex);
            this.rfidBasicIDTagBean.cardTypeName = this.getCardTypename(this.rfidBasicIDTagBean.cardType);
        }
        this.dispalyDetails = true;
    }

    resetValues() {
        this.piConatinerBean = null;
        this.chuidContainerBean = null;
        this.facialUserBioInfoBean = null;
        this.fpContainerBean = null;
        this.userCertificatesBean = null;
        this.rfidBasicIDTagBean = null;
        this.userCertificates = [];
        this.userRetiredKeyMngCertificates = [];
        this.userDetails = new User();
        this.certIdNameCount = 0;
    }
    getGender(gender: string): string {
        if (gender === 'M') {
            return 'Male';
        } else if (gender === 'F') {
            return 'Female';
        } else {
            return 'Others';
        }
    }
    getCardTypename(cardType: number): string {
        if (cardType === 1) {
            return 'National ID';
        } else if (cardType === 2) {
            return 'Permanent Resident Card';
        } else if (cardType === 3) {
            return 'Student ID';
        } else if (cardType === 4) {
            return 'Emloyee ID';
        } else if (cardType === 5) {
            return 'Vehicle Reg. ID';
        } else if (cardType === 6) {
            return 'Driving Licence';
        } else if (cardType === 7) {
            return 'Health Card';
        } else if (cardType === 8) {
            return 'Access Card';
        } else if (cardType === 9) {
            return 'Travel Card';
        }
    }

    buildCmsSyncRequest(identifyNextRes, processName) {
        const cmsSyncRequest = new CmsSyncRequest();
        cmsSyncRequest.processName = processName;
        cmsSyncRequest.deviceType = 'permanent';
        cmsSyncRequest.device = {
            type: identifyNextRes.body.entries[0].result.type,
            serial: identifyNextRes.body.entries[0].result.serial,
            cardType: identifyNextRes.body.entries[0].result.cardType,
            cardProductName: identifyNextRes.body.entries[0].result.cardProductName
        };
        if (this.device !== null) {
            cmsSyncRequest.user = this.username;
            cmsSyncRequest.wfName = this.workflowName;
        }
        if (this.showDeviceProfile) {
            console.log(this.idReaderForm.controls.deviceProfile.value, this.username, this.workflowName);
            cmsSyncRequest.deviceProfileId = this.idReaderForm.controls.deviceProfile.value.id;
            cmsSyncRequest.user = this.username;
            cmsSyncRequest.wfName = this.workflowName;
        }
        cmsSyncRequest.userGroupXrefId = 1;
        cmsSyncRequest.orgName = this.orgName;
        cmsSyncRequest.pin = this.idReaderForm.controls.pin.value;
        cmsSyncRequest.issuedBy = this.principal.getLoggedInUser().username;
        cmsSyncRequest.loggedInUserId = this.principal.getLoggedInUser().userId;
        return cmsSyncRequest;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.dataService.changeMIDObj(null);
        clearInterval(this.timer);
    }
}
