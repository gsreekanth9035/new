import { Principal } from 'app/core';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Group } from '../workflow/group.model';

@Injectable({
    providedIn: 'root'
})
export class ExpressEnrollUserService {
    httpHeaders = new HttpHeaders({
        loggedInUser: this.principal.getLoggedInUser().username,
        loggedInUserGroupID: this.principal.getLoggedInUser().groups[0].id,
        loggedInUserGroupName: this.principal.getLoggedInUser().groups[0].name,
        uimsUrl: location.origin
        // Authroization is added by angular/gateway by default no need to add specifically
        // this.httpHeaders.append('Authorization', 'Bearer my-token');}
    });

    constructor(private httpClient: HttpClient, private principal: Principal) {}

    // Call REST APIs
    expressEnrollUser(groupId: string, formData: FormData, roleIDs: String): Observable<any> {
        // this.httpHeaders.append('Content-type', 'application/json');
        const httpHeaders = new HttpHeaders({
            'Content-type': 'multipart/form-data'
        });
        const orgName = this.principal.getLoggedInUser().organization;
        const lggedInUser = this.principal.getLoggedInUser();
        const uri = '/usermanagement/api/v1/organizations/' + orgName + '/users/express-enroll';
        const url = window.location.origin + uri;
        // alert(orgName);
        // alert(uri);
        // alert(url);
        return this.httpClient.post(`${url}`, formData, { headers: this.httpHeaders, observe: 'response' });
        // return this.httpClient.post(`${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/users/express-enroll`,
        // jsonString, { headers: httpHeaders, observe: 'response' }).catch(this.errorHandler);
    }

    errorHandler(error: HttpErrorResponse) {
        return Observable.throw('Error: Express Enrollment failed');
    }

    // Call Groups
    getGroups(): Observable<Group[]> {
        return this.httpClient.get<Group[]>(
            `${window.location.origin}/usermanagement/api/v1/organizations/${this.principal.getLoggedInUser().organization}/groups`
        );
    }
}
