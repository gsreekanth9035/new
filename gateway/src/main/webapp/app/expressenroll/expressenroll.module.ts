import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { ExpressEnrollUserService } from './expressenroll.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EXPRESSENROLL_ROUTE } from './expressenroll.route';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ExpressEnrollComponent } from './expressenroll.component';
import { GatewaySharedModule } from '../shared';

@NgModule({
    imports: [
        CommonModule,
        GatewaySharedModule,
        RouterModule.forChild([EXPRESSENROLL_ROUTE]),
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
    ],
    declarations: [ExpressEnrollComponent],
    providers: [ ExpressEnrollUserService ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class ExpressEnrollUserModule {}
