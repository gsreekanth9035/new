import { ExpressEnrollComponent } from './expressenroll.component';
import { Route } from '@angular/router';

export const EXPRESSENROLL_ROUTE: Route = {
    path: 'expressenroll',
    component: ExpressEnrollComponent,
    data: {
        authorities: [],
        pageTitle: 'expressenroll.title'
    }
};
