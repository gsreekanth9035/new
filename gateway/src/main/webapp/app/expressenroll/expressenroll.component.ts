import { Principal } from 'app/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ExpressEnrollUserService } from './expressenroll.service';
import { OnboardingUserService } from '../onboarding-user/onboarding-user.service';
import { Group } from 'app/workflow/group.model';
import { Role } from 'app/workflow/role.model';
import { MatOptionSelectionChange } from '@angular/material';

@Component({
    selector: 'jhi-expressenroll',
    templateUrl: './expressenroll.component.html',
    styleUrls: ['expressenroll.scss']
})
export class ExpressEnrollComponent implements OnInit {
    // Initialization
    groups;
    groupId;

    isLoadingResults: boolean;
    successMsg = false;
    errorMsg = false;
    formData = new FormData();
    expressEnrollForm: FormGroup;
    expressEnrollFileString: string;
    expressEnrollFileError = true;
    saveExpressEnrollUserSuccess;
    saveExpressEnrollUserFailure;
    saveExpressEnrollUserConflict;
    saveExpressEnrollUserServiceErr;
    saveExpressEnrollUserInvalid;
    // groups: Group[];
    roles: Role[];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private expressEnrollUserService: ExpressEnrollUserService,
        private principal: Principal,
        private onboardingUserService: OnboardingUserService
    ) {}

    ngOnInit() {
        this.isLoadingResults = false;
        // Form
        this.expressEnrollForm = this.formBuilder.group({
            jsonPath: ['', [Validators.required]],
            groupId: ['', [Validators.required]],
            groupName: [''],
            authServiceRoleID: ['', [Validators.required, selectedItemsValidator]]
        });

        // Load Groups
        if (this.principal.getLoggedInUser().roles.includes('role_admin')) {
            this.expressEnrollUserService.getGroups().subscribe(groups => {
                this.groups = groups;
            });
        } else if (this.principal.getLoggedInUser().roles.includes('role_operator')) {
            this.groups = this.principal.getLoggedInUser().groups;
        }

        this.onboardingUserService.getRolesFromAuthService().subscribe(roles => {
            let rolesByLoggedInUserRole = roles;
            const lowerCaseRoles = [];
            this.principal.getLoggedInUser().roles.forEach(role => {
                lowerCaseRoles.push(role.toLowerCase());
            });
            if (lowerCaseRoles.includes('role_operator')) {
                rolesByLoggedInUserRole = roles.filter(e => e.name.toLowerCase() !== 'role_admin');
            } else if (lowerCaseRoles.includes('role_adjudicator')) {
                rolesByLoggedInUserRole = roles.filter(
                    e => !e.name.toLowerCase().includes('role_admin') && !e.name.toLowerCase().includes('role_operator')
                );
            }
            this.roles = rolesByLoggedInUserRole;
        });
    }
    checkRole(event: MatOptionSelectionChange) {
        if (event.isUserInput) {
            const isSelected = event.source.selected;
            const role = event.source.value.name.toLowerCase();
            this.rolesSelectionValidation(role, isSelected);
        }
    }
    rolesSelectionValidation(role: string, isSelected: boolean) {
        if (role === 'role_admin') {
            this.roles.forEach(e => {
                if (e.name.toLowerCase().includes('role_operator')) {
                    if (isSelected) {
                        e.disabled = true;
                    } else {
                        e.disabled = false;
                    }
                } else if (e.name.toLowerCase() === 'role_adjudicator') {
                    if (isSelected) {
                        e.disabled = true;
                    } else {
                        e.disabled = false;
                    }
                }
            });
        } else if (role.includes('role_operator')) {
            this.roles.forEach(e => {
                if (e.name.toLowerCase() === 'role_admin') {
                    if (isSelected) {
                        e.disabled = true;
                    } else {
                        e.disabled = false;
                    }
                } else if (e.name.toLowerCase() === 'role_adjudicator') {
                    if (isSelected) {
                        e.disabled = true;
                    } else {
                        e.disabled = false;
                    }
                } else {
                    if (role === 'role_operator') {
                        if (
                            e.name.toLowerCase() === 'role_operator_sponsor' ||
                            e.name.toLowerCase() === 'role_operator_registrar' ||
                            e.name.toLowerCase() === 'role_operator_issuer' ||
                            e.name.toLowerCase() === 'role_operator_digital_signatory' ||
                            e.name.toLowerCase() === 'role_operator_approval_authority'
                        ) {
                            if (isSelected) {
                                e.disabled = true;
                            } else {
                                e.disabled = false;
                            }
                        }
                    } else if (role === 'role_operator_sponsor') {
                        if (
                            e.name.toLowerCase() === 'role_operator' ||
                            e.name.toLowerCase() === 'role_operator_registrar' ||
                            e.name.toLowerCase() === 'role_operator_issuer' ||
                            e.name.toLowerCase() === 'role_operator_digital_signatory' ||
                            e.name.toLowerCase() === 'role_operator_approval_authority'
                        ) {
                            if (isSelected) {
                                e.disabled = true;
                            } else {
                                e.disabled = false;
                            }
                        }
                    } else if (role === 'role_operator_registrar') {
                        if (
                            e.name.toLowerCase() === 'role_operator_sponsor' ||
                            e.name.toLowerCase() === 'role_operator' ||
                            e.name.toLowerCase() === 'role_operator_issuer' ||
                            e.name.toLowerCase() === 'role_operator_digital_signatory' ||
                            e.name.toLowerCase() === 'role_operator_approval_authority'
                        ) {
                            if (isSelected) {
                                e.disabled = true;
                            } else {
                                e.disabled = false;
                            }
                        }
                    } else if (role === 'role_operator_issuer') {
                        if (
                            e.name.toLowerCase() === 'role_operator_sponsor' ||
                            e.name.toLowerCase() === 'role_operator_registrar' ||
                            e.name.toLowerCase() === 'role_operator' ||
                            e.name.toLowerCase() === 'role_operator_digital_signatory' ||
                            e.name.toLowerCase() === 'role_operator_approval_authority'
                        ) {
                            if (isSelected) {
                                e.disabled = true;
                            } else {
                                e.disabled = false;
                            }
                        }
                    } else if (role === 'role_operator_digital_signatory') {
                        if (
                            e.name.toLowerCase() === 'role_operator_sponsor' ||
                            e.name.toLowerCase() === 'role_operator_registrar' ||
                            e.name.toLowerCase() === 'role_operator_issuer' ||
                            e.name.toLowerCase() === 'role_operator_digital' ||
                            e.name.toLowerCase() === 'role_operator_approval_authority'
                        ) {
                            if (isSelected) {
                                e.disabled = true;
                            } else {
                                e.disabled = false;
                            }
                        }
                    } else if (role === 'role_operator_approval_authority') {
                        if (
                            e.name.toLowerCase() === 'role_operator_sponsor' ||
                            e.name.toLowerCase() === 'role_operator_registrar' ||
                            e.name.toLowerCase() === 'role_operator_issuer' ||
                            e.name.toLowerCase() === 'role_operator_digital_signatory' ||
                            e.name.toLowerCase() === 'role_operator_approval'
                        ) {
                            if (isSelected) {
                                e.disabled = true;
                            } else {
                                e.disabled = false;
                            }
                        }
                    }
                }
            });
        } else if (role === 'role_adjudicator') {
            this.roles.forEach(e => {
                if (e.name.toLowerCase() === 'role_admin') {
                    if (isSelected) {
                        e.disabled = true;
                    } else {
                        e.disabled = false;
                    }
                } else if (e.name.toLowerCase().includes('role_operator')) {
                    if (isSelected) {
                        e.disabled = true;
                    } else {
                        e.disabled = false;
                    }
                }
            });
        }
    }
    // Navigate to Home
    goHome() {
        this.router.navigate(['/dashboard']);
    }

    goBack() {
        this.router.navigate(['expressenroll']);
    }

    onFormSubmit() {
        this.groupId = this.expressEnrollForm.get('groupId').value;
        this.saveExpressEnrollUser();
    }

    // Save expressEnroll User & Error Handling
    saveExpressEnrollUser() {
        // alert( this.groupId);
        // alert(this.expressEnrollForm.controls['groupId'].value.id);
        // alert(this.expressEnrollForm.controls['groupId'].value.name);
        this.formData.set('groupID', this.expressEnrollForm.controls['groupId'].value.id);
        this.formData.set('groupName', this.expressEnrollForm.controls['groupId'].value.name);
        this.formData.set('enrolledBy', this.principal.getLoggedInUser().username);
        let roleIDs = '';
        this.expressEnrollForm.controls['authServiceRoleID'].value.forEach(e => {
            // this.roles.push(e.authServiceRoleID);
            if (roleIDs.length > 0) {
                roleIDs = roleIDs + ',';
            }
            roleIDs = roleIDs + e.authServiceRoleID;
        });
        this.formData.set('roleIDs', roleIDs);
        // alert(roleIDs);
        this.isLoadingResults = true;
        this.expressEnrollUserService.expressEnrollUser(this.groupId, this.formData, roleIDs).subscribe(
            res => {
                this.saveExpressEnrollUserSuccess = true;
                this.saveExpressEnrollUserFailure = false;
                this.saveExpressEnrollUserConflict = false;
                this.saveExpressEnrollUserServiceErr = false;
                this.saveExpressEnrollUserServiceErr = false;
                this.saveExpressEnrollUserInvalid = false;
                this.expressEnrollForm.reset();
                this.isLoadingResults = false;
            },
            err => {
                console.log(err.status);
                // alert(err.status);
                if (err.status === 409) {
                    this.saveExpressEnrollUserSuccess = false;
                    this.saveExpressEnrollUserFailure = false;
                    this.saveExpressEnrollUserConflict = true;
                    this.saveExpressEnrollUserServiceErr = false;
                    this.saveExpressEnrollUserInvalid = false;
                } else if (err.status === 500) {
                    this.saveExpressEnrollUserSuccess = false;
                    this.saveExpressEnrollUserFailure = false;
                    this.saveExpressEnrollUserConflict = false;
                    this.saveExpressEnrollUserServiceErr = false;
                    this.saveExpressEnrollUserInvalid = true;
                } else if (err.status === 503) {
                    this.saveExpressEnrollUserSuccess = false;
                    this.saveExpressEnrollUserFailure = false;
                    this.saveExpressEnrollUserConflict = false;
                    this.saveExpressEnrollUserServiceErr = true;
                    this.saveExpressEnrollUserInvalid = false;
                } else {
                    this.saveExpressEnrollUserSuccess = false;
                    this.saveExpressEnrollUserFailure = true;
                    this.saveExpressEnrollUserConflict = false;
                    this.saveExpressEnrollUserServiceErr = false;
                    this.saveExpressEnrollUserInvalid = false;
                }
                this.isLoadingResults = false;
            }
        );
    }

    expressEnrollFile($event) {
        // alert(event.target.files);
        const length = $event.target.files.length;
        if (length > 0) {
            this.expressEnrollFileError = false;
        } else {
            this.expressEnrollFileError = true;
        }
        const fileList = $event.target.files;
        for (const file of fileList) {
            this.formData.set('expressFiles', file, file.name);
        }
        // const reader = new FileReader();
        // reader.readAsText(file);
        // reader.onload = () => {
        //     this.expressEnrollFileString = reader.result.toString();
        //     // console.log(this.expressEnrollFileString);
        // };
    }

    // expressEnrollGroup(event: any) {
    //     alert(event.target.value);
    //     this.groupId = event.target.value;
    // }
}

function selectedItemsValidator(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value !== undefined && control.value !== null) {
        const val = control.value;
        if (val.length > 2) {
            return { selectedItemsValidator: true };
        }
    }
    return null;
}
